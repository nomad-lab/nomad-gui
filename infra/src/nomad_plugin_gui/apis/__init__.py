from nomad.config.models.plugins import APIEntryPoint
from nomad.config import config


class GUIAPIEntryPoint(APIEntryPoint):
    def load(self):
        import os
        import os.path
        import glob
        import re
        import hashlib
        import shutil

        from fastapi import FastAPI
        from fastapi.responses import PlainTextResponse, FileResponse
        from starlette.datastructures import Headers
        from starlette.staticfiles import (
            StaticFiles as StarletteStaticFiles,
            NotModifiedResponse,
        )

        from nomad_plugin_gui.gui_config import config_js_string

        config_etag = hashlib.md5(
            config_js_string.encode(), usedforsecurity=False
        ).hexdigest()

        class GUIFiles(StarletteStaticFiles):
            def is_not_modified(
                self, response_headers: Headers, request_headers: Headers
            ) -> bool:
                # The starlette etag implementation is not considering the "..."
                # and W/"..." etag RFC syntax used by browsers.
                etag_re = r'^(W/)?"?([^"]*)"?$'
                try:
                    if_none_match = request_headers['if-none-match']
                    match = re.match(etag_re, if_none_match)
                    if_none_match = match.group(2)
                    etag = response_headers['etag']
                    if if_none_match == etag:
                        return True
                except KeyError:
                    pass

                return super().is_not_modified(response_headers, request_headers)

            async def get_response(self, path: str, scope):
                if path != 'config.js':
                    response = await super().get_response(path, scope)
                else:
                    response = PlainTextResponse(
                        config_js_string,
                        media_type='application/javascript',
                        headers=dict(etag=config_etag),
                    )

                request_headers = Headers(scope=scope)
                if self.is_not_modified(response.headers, request_headers):
                    return NotModifiedResponse(response.headers)
                return response

        root_path = f'{config.services.api_base_path}/{self.prefix}'

        # create a copy of the static gui build that has the default base path
        # replaced with the confiured base path
        static_folder = os.path.abspath(
            os.path.join(os.path.dirname(__file__), 'static')
        )

        run_gui_folder = os.path.join(config.fs.working_directory, 'run', 'gui')
        if not os.path.exists(run_gui_folder):
            os.makedirs(run_gui_folder)

        shutil.rmtree(run_gui_folder, ignore_errors=True)
        shutil.copytree(static_folder, run_gui_folder, dirs_exist_ok=True)

        source_file_globs = [
            '**/*.json',
            '**/*.html',
            '**/*.js',
            '**/*.js.map',
            '**/*.css',
        ]
        for source_file_glob in source_file_globs:
            source_files = glob.glob(
                os.path.join(run_gui_folder, source_file_glob), recursive=True
            )
            for source_file in source_files:
                with open(source_file, 'rt') as f:
                    file_data = f.read()
                file_data = file_data.replace('/prefix-to-replace', root_path)
                with open(source_file, 'wt') as f:
                    f.write(file_data)

        # create the app and mount the static files
        app = FastAPI(
            root_path=root_path, title=self.name, description=self.description
        )
        app.mount('/', GUIFiles(directory=run_gui_folder), name='static')

        # return the index.html for all routes that do not match a file to
        # make the react SPA work
        @app.exception_handler(404)
        async def custom_404_handler(_, __):
            return FileResponse(os.path.join(run_gui_folder, 'index.html'))

        return app


gui_api = GUIAPIEntryPoint(
    name='The NOMAD GUI',
    description='The new NOMAD GUI. This API serves the static files of the build GUI.',
    prefix='ui',
)
