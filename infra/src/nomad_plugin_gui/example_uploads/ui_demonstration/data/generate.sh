#!/bin/sh

cp template.archive.json "CH_3NH_3PbI_3.archive.json"
cp template.archive.json "CsSnI_3.archive.json"
cp template.archive.json "(FAPbI_3)(1-x)(MAPbBr_3)(x).archive.json"
cp template.archive.json "BA_2SnS_4.archive.json"
cp template.archive.json "MAPbBr_3Cl.archive.json"
cp template.archive.json "Cs_2AgBiBr_6.archive.json"
cp template.archive.json "CH_3NH_3Sn(Se_(1-x)S_x)_3.archive.json"
cp template.archive.json "(Cs,MA)_3Sb_2I_9.archive.json"
cp template.archive.json "FAPb(I_(1-x)Br_x)_3.archive.json"
cp template.archive.json "CH_3NH_3Pb(Cl_(1-x)I_x)_3.archive.json"