## UI Demonstration

This provides files that demonstrate the capabilities of the new UI.

This includes examples for ELNs using the new _layout_ system:

- An ELN modeling a [simple lab class](./exercises/exercise_1_resistance.archive.json) about measuring electrical resistance.
- An ELN modeling a [biography](./authors/markus.archive.yaml).

For any questions or feedback, please contact [markus.scheidgen@physik.hu-berlin.de].
