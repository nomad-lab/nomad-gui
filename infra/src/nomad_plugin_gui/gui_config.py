import json

from nomad.config import config


include_filter = {
    'keycloak': {
        'realm_name': '*',
        'client_id': '*',
        'public_server_url': '*',
    },
    'services': '*',
    'ui': {
      'unit_systems': '*'
    },
}

def filter_config(data, filter):
    if filter == '*':
        return data
    if not filter:
        return None
    if isinstance(data, dict):
        next = {}
        for key in filter:
            if key in data:
                next_data = data.get(key)
                if next_data is not None:
                    next[key] = filter_config(next_data, filter[key])
        return next
    if data == filter:
        return data
    return None

config.load_plugins()
config_data = filter_config(config.dict(), include_filter)

# Add a filtered list of app entry points
entry_points = config.plugins.entry_points.filtered_values()
apps = [value.dict(exclude_none=True) for value in entry_points if value.entry_point_type == 'app']
config_data['plugins'] = {'entry_points': {'items': apps}}

# Add a filtered list of unit systems
unit_systems = []
for key, value in config.ui.unit_systems.filtered_items():
    unit_system = value.dict()
    unit_system.update({'id': key})
    unit_systems.append(unit_system)
config_data['ui'] = {'unit_systems': {'items': unit_systems, 'selected': config.ui.unit_systems.selected}}
config_js_string = f'window.nomadConfig = {json.dumps(config_data)};'


if __name__ == '__main__':
    print(config_js_string)
