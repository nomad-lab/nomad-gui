#!/bin/sh

set -e

working_dir=$(pwd)
project_dir=$(dirname $(dirname $(realpath $0)))

cd $project_dir

npm run build -- --base /prefix-to-replace
rm -rf infra/src/nomad_plugin_gui/apis/static/*
touch infra/src/nomad_plugin_gui/apis/static/.empty
cp -r build/* infra/src/nomad_plugin_gui/apis/static
