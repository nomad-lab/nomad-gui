#!/bin/sh

set -e

working_dir=$(pwd)
project_dir=$(dirname $(dirname $(realpath $0)))

cd $project_dir

NOMAD_CONFIG=infra/nomad.yaml python -m nomad_plugin_gui.gui_config > public/config.js
