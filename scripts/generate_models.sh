#!/bin/sh

set -e

working_dir=$(pwd)
project_dir=$(dirname $(dirname $(realpath $0)))

cd $project_dir

python -m nomad.cli dev api-model nomad.app.v1.models.graph.GraphRequest | \
    npx json2ts --additionalProperties false --output src/models/graphRequestModels.d.ts

python -m nomad.cli dev api-model nomad.app.v1.models.graph.GraphResponse | \
    npx json2ts --additionalProperties false --output src/models/graphResponseModels.d.ts

python -m nomad.cli dev api-model nomad.app.v1.routers.entries.EntryEdit | \
    npx json2ts --additionalProperties false --output src/models/entryEditRequestModels.d.ts

python -m nomad.cli dev api-model nomad.app.v1.routers.entries.EntryEditResponse | \
    npx json2ts --additionalProperties false --output src/models/entryEditResponseModels.d.ts

python -m nomad.cli dev api-model nomad.config.models.config.Config | \
    npx json2ts --additionalProperties false --output src/models/config.d.ts"