[![pipeline status](https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-FAIR/badges/develop/pipeline.svg)](https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-gui/commits/develop)
[![coverage report](https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-gui/badges/develop/coverage.svg?job=tests&key_text=coverage&key_width=130)](https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-gui/commits/develop)

# NOMAD GUI V2

## Getting Started

Before you begin, ensure you have the following software and access:

- **Node.js**: Ensure you have Node.js installed. We recommend using the LTS version.
- **npm**: You'll need a package manager for installing project dependencies.

1. Clone this repository to your local machine:

   ```shell
   git clone https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-gui.git
   ```

2. Navigate to the project directory:

   ```shell
   cd nomad-gui
   ```

3. Install project dependencies:

   ```shell
   npm install
   ```

4. Run the development server

   ```shell
   npm run start
   ```

5. Run the storybook server

   ```shell
   npm run storybook
   ```

Read more documentation from the [storybook server](http://localhost:6006/?path=/docs/development--docs)
or the [dev server](http://localhost:3000/dev/docs)

## Run against an actual NOMAD

By default we use a synthetic "fake" api with some made up data.

To set up an actual NOMAD and run the GUI against it, we provide the `infra`
directory with all the necessary Python code, NOMAD plugins, and other artifacts
to facilitate development and end-to-end testing. It provides the GUI
plugin that is used to run the GUI in production, and it also provides you with
example plugins, schemas, data to run the GUI against.

This is how you run NOMAD, upload some data, and start using the GUI with it
in development.

1. Get a python environment with NOMAD installed.
   From the gui project root folder:

   ```sh
   python3.11 -m venv .pyenv
   source .pyenv/bin/activate
   pip install uv
   uv pip install --upgrade pip
   uv pip install -e infra \
      --extra-index-url https://gitlab.mpcdf.mpg.de/api/v4/projects/2187/packages/pypi/simple \
      --pre
   ```

2. (optional) Replace nomad-lab with a local clone, assuming you have cloned
   the [nomad-FAIR](https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-FAIR) project to a
   sibling directory called `nomad`.

   ```sh
   uv pip install -e ../nomad[parsing,infrastructure]
   ```

3. Run the required infrastructure services. Typically you will run them through a `docker-compose.yaml` file that is part of your development setup.


4. Run NOMAD. Run from the `infra` directory it has the necessary `nomad.yaml` file:

   ```sh
   cd infra
   nomad admin run appworker
   ````

5. Upload some data. Again, mind the directory. Run from `infra`.

   ```sh
   cd infra
   nomad client upload --upload-name demo-schema --ignore-path-prefix data/demo-schema
   nomad client upload --upload-name entry-data --ignore-path-prefix  data/entry-data
   nomad client upload --upload-name upload-navigation data/upload-navigation.zip
   ```

6. Run the gui with the right `.env.local`. This one is not part of git. By
   default `.env` is set to use the synthetic "fake" api. Overwrite with your
   `.env.local` to use the real api. This one has to be at the root of
   the project. Therefore, from the root dir you could do:

   ```sh
   echo "VITE_USE_MOCKED_API=false" > .env.local
   npm run start
   ```

   After this you can see the data once you login as the `test` user (the `nomad client` command uses the `test` user by default.)

7. In production, the GUI will load the NOMAD config from the backend. In development,
   this is not possible and the GUI is using a generated config. The file is `public/config.js`.
   It is in Git, but if you change relevant parts of the config, you might need to
   regenerate it (this requires the Python setup from the steps before!):

   ```sh
   ./scripts/generate_config.sh
   ```
