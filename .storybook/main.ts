import type {StorybookConfig} from '@storybook/react-vite'

const vitePluginsToRemove = ['@mdx-js/rollup']
const excludedLibraries = ['material-react-table']
const config: StorybookConfig = {
  stories: [
    '../docs/**/*.mdx',
    '../src/**/*.doc.mdx',
    '../src/**/*.stories.@(js|jsx|mjs|ts|tsx)',
  ],
  staticDirs: ['../public'],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-onboarding',
    '@storybook/addon-interactions',
    '@storybook/addon-themes',
    '@storybook/themes',
    '@storybook/addon-mdx-gfm',
  ],
  framework: {
    name: '@storybook/react-vite',
    options: {},
  },
  docs: {
    autodocs: 'tag',
  },
  core: {disableTelemetry: true},
  viteFinal: (config) => {
    // Some of our own plugins have to be removed, because storybook will
    // put their own versions of them on top.
    config.plugins = config.plugins?.filter((plugin) => {
      if (typeof plugin === 'object') {
        if (vitePluginsToRemove.indexOf((plugin as any).name || '') >= 0) {
          return false
        }
      }
      return true
    })
    return config
  },
  typescript: {
    reactDocgen: 'react-docgen-typescript',
    reactDocgenTypescriptOptions: {
      shouldRemoveUndefinedFromOptional: true,
      propFilter: (prop) => {
        const exclude_prop =
          prop.declarations?.some((declaration) =>
            excludedLibraries.some((excludedLib) =>
              declaration.fileName.includes(excludedLib),
            ),
          ) ?? false
        return !exclude_prop
      },
    },
  },
}
export default config
