import {CssBaseline, ThemeProvider} from '@mui/material'
import {withThemeFromJSXProvider} from '@storybook/addon-themes'
import type {Preview} from '@storybook/react'

import {darkTheme, lightTheme} from '../src/components/theme/themeOptions'

const preview: Preview = {
  parameters: {
    actions: {argTypesRegex: '^on[A-Z].*'},
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
    options: {
      storySort: {
        order: [
          'GettingStarted',
          'ProjectStructure',
          'Guidelines',
          'Routing',
          'APIModels',
          'Values',
          'Layouts',
          'Archive',
          'DevTools',
          'E2ETesting',
        ],
      },
    },
  },

  decorators: [
    withThemeFromJSXProvider({
      themes: {
        light: lightTheme,
        dark: darkTheme,
      },
      defaultTheme: 'light',
      Provider: ThemeProvider,
      GlobalStyles: CssBaseline,
    }),
  ],
}

export default preview
