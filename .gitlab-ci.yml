# default installed image for docker executor is: python:3.6
# using an image that can do git, docker, docker-compose
image: gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-fair/ci-runner:latest

default:
  tags:
    - cloud

variables:
  DOCKER_TAG: ${CI_COMMIT_REF_SLUG}
  CI_DEBUG_SERVICES: 'false' # set to true to enable service container logs
  UV_VERSION: 0.5
  PYTHON_VERSION: 3.11

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH

stages:
  - install
  - build
  - test
  - release
  - deploy

install:
  stage: install
  image: node:20
  script:
    - npm i
  cache:
    paths:
      - node_modules/
  artifacts:
    expire_in: 1 days
    when: on_success
    paths:
      - node_modules/

build gui:
  stage: build
  image: node:20
  dependencies:
    - install
  script:
    - echo '// will be replaced by backend' > public/config.js
    - npm run build -- --base /prefix-to-replace > build.log 2>&1 # Redirect build output to a log file
    - |
      if [ $? -ne 0 ]; then
        echo "Build command failed"
        cat build.log
        exit 2
      fi

      if grep -q "Some chunks are larger than" build.log; then
        echo "Build failed due to some chunks being too large. Consider dynamic imports"
        exit 1
      fi
  artifacts:
    expire_in: 1 days
    when: on_success
    paths:
      - node_modules/
      - build/

build standalone gui:
  stage: build
  image: node:20
  dependencies:
    - install
  script:
    - echo '// will be replaced by backend' > public/config.js
    - VITE_USE_MOCKED_API=true npm run build -- --base /prod/v2/gui > build.log 2>&1 # Redirect build output to a log file
    - |
      if [ $? -ne 0 ]; then
        echo "Build command failed"
        cat build.log
        exit 2
      fi

      if grep -q "Some chunks are larger than" build.log; then
        echo "Build failed due to some chunks being too large. Consider dynamic imports"
        exit 1
      fi
    - mv build/ standalone_build/
  artifacts:
    expire_in: 1 days
    when: on_success
    paths:
      - node_modules/
      - standalone_build/

build python package:
  stage: build
  image: ghcr.io/astral-sh/uv:$UV_VERSION-python$PYTHON_VERSION-bookworm
  variables:
    GIT_DEPTH: 0
  needs:
    - build gui
  script:
    - cp -r build/* infra/src/nomad_plugin_gui/apis/static/
    - cd infra
    - uv build
  artifacts:
    paths:
      - infra/dist/
    expire_in: 1 hour
  rules:
    - when: on_success

release python package:
  stage: release
  dependencies:
    - build python package
  script:
    - python -m venv .venv
    - source .venv/bin/activate
    - python -m pip install twine
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi infra/dist/*
  rules:
    - if: $CI_COMMIT_BRANCH == "develop"
    - if: $CI_COMMIT_BRANCH != "develop"
      when: manual
      allow_failure: true

build e2e-test nomad image:
 stage: build
 image:
   name: gcr.io/kaniko-project/executor:debug
   entrypoint: ['']
 variables:
   GIT_SUBMODULE_STRATEGY: recursive
   GIT_SUBMODULE_DEPTH: 1
   GIT_SUBMODULE_UPDATE_FLAGS: --jobs 4
 before_script:
   - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"},\"$CI_DEPENDENCY_PROXY_SERVER\":{\"auth\":\"$(printf "%s:%s" ${CI_DEPENDENCY_PROXY_USER} "${CI_DEPENDENCY_PROXY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
 script:
   - /kaniko/executor
     --context "${CI_PROJECT_DIR}/infra"
     --dockerfile "${CI_PROJECT_DIR}/infra/Dockerfile"
     --destination "${CI_REGISTRY_IMAGE}:${DOCKER_TAG}-api-e2e"
 rules:
   - when: never
   #  - when: on_success

build standalone gui image:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: ['']
  needs:
    - build standalone gui
  before_script:
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"},\"$CI_DEPENDENCY_PROXY_SERVER\":{\"auth\":\"$(printf "%s:%s" ${CI_DEPENDENCY_PROXY_USER} "${CI_DEPENDENCY_PROXY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - mv ./standalone_build ./build
  script:
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}:${DOCKER_TAG}"
  rules:
    - when: on_success

linting:
  stage: test
  image: node:20
  dependencies:
    - install
  script:
    - npm run check-style
    - npm run lint

tests:
  stage: test
  image: node:20
  coverage: /All files[^|]*\|[^|]*\s+([\d\.]+)/
  dependencies:
    - install
  script:
    - npx vitest run --coverage
    - cp coverage/cobertura-coverage.xml $CI_PROJECT_DIR
  artifacts:
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: cobertura-coverage.xml

e2e_tests:
  stage: test
  image: mcr.microsoft.com/playwright:v1.49.0-jammy
  services:
    - name: ${CI_REGISTRY_IMAGE}:${DOCKER_TAG}-api-e2e
      alias: backend
      command:
        - python
        - -m
        - nomad.cli
        - admin
        - run
        - appworker
    - name: rabbitmq:3.11.5
      alias: rabbitmq
    - name: docker.elastic.co/elasticsearch/elasticsearch:7.17.1
      alias: elastic
      command:
        - bash
        - '-c'
        - ES_JAVA_OPTS="-Xms512m -Xmx512m" docker-entrypoint.sh elasticsearch -Ediscovery.type=single-node -Expack.security.enabled=false
    - name: mongo:5.0.6
      alias: mongo
  variables:
    RABBITMQ_ERLANG_COOKIE: SWQOKODSQALRPCLNMEQG
    RABBITMQ_DEFAULT_USER: rabbitmq
    RABBITMQ_DEFAULT_PASS: rabbitmq
    RABBITMQ_DEFAULT_VHOST: /
    NOMAD_RABBITMQ_HOST: rabbitmq
    NOMAD_ELASTIC_HOST: elastic
    NOMAD_MONGO_HOST: mongo
    NOMAD_NORMALIZE_SPRINGER_DB_PATH: /nomad/fairdi/db/data/springer.msg
    VITE_USE_MOCKED_API: false
    E2E_API_URL: http://localhost:8000/fairdi/nomad/latest/api/v1
  dependencies:
    - install
    - build e2e-test nomad image
  script:
    - sh infra/health_check.sh
    - npm run build
    - npm ci
    - npm run test:e2e
  artifacts:
    expire_in: 1 days
    when: always
    paths:
      - test-results/
  rules:
    - when: never

release standalone gui image:
  stage: release
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: ['']
  variables:
    GIT_STRATEGY: none
  dependencies:
    - build standalone gui image
  script:
    - crane auth login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - crane cp ${CI_REGISTRY_IMAGE}:${DOCKER_TAG} ${CI_REGISTRY_IMAGE}:latest
  rules:
    - when: on_success

deploy standalone gui:
  stage: deploy
  dependencies:
    - release standalone gui image
  environment:
    name: production
    url: https://nomad-lab.eu/prod/v2/gui
  before_script:
    - mkdir ~/.kube/
    - echo ${CI_K8S_CLOUD_CONFIG} | base64 -d > ~/.kube/config
    - chmod g-r ~/.kube/config
  script:
    - helm upgrade nomad-gui chart/nomad-gui
      --install
      --namespace nomad-gui
      --values chart/values.yaml
      --set roll=true
      --timeout=15m
      --wait
    - kubectl rollout restart -n nomad-gui deployment nomad-gui
  rules:
    - if: $CI_COMMIT_BRANCH == "develop"
    - if: $CI_COMMIT_BRANCH != "develop"
      when: manual
      allow_failure: true
