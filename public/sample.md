## Markdown

You can use all _CommonMark_ and _Git-Hub Flavoured_ markdown syntax.

### Contents

### Autolink literals

www.example.com, https://example.com, and contact@example.com.

### Footnote

A note[^1]

[^1]: Big note.

### Strikethrough

~one~ or ~~two~~ tildes.

### Table

| a   | b   |   c |  d  |
| --- | :-- | --: | :-: |
| 1   | 2   |   3 |  4  |

### Tasklist

- [ ] to do
- [x] done

### Code block

\`\`\`tsx
function Hello() {
return <Typography>Hello</Typography>
}
\`\`\`
