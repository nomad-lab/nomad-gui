[![pipeline status](https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-FAIR/badges/develop/pipeline.svg)](https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-gui/commits/develop)
[![coverage report](https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-gui/badges/develop/coverage.svg?job=tests&key_text=coverage&key_width=130)](https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-gui/commits/develop)

# NOMAD GUI V2

## Getting Started

Before you begin, ensure you have the following software and access:

- **Node.js**: Ensure you have Node.js installed. We recommend using the LTS version.
- **npm**: You'll need a package manager for installing project dependencies.

1. Clone this repository to your local machine:

   ```shell
   git clone https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-gui.git
   ```

2. Navigate to the project directory:

   ```shell
   cd nomad-gui
   ```

3. Install project dependencies:

   ```shell
   npm install
   ```

4. Run the development server

   ```shell
   npm run start
   ```

5. Run the storybook server

   ```shell
   npm run storybook
   ```

Read more documentation from the [storybook server](http://localhost:6006/?path=/docs/development--docs)
or the [dev server](http://localhost:3000/dev/docs)
