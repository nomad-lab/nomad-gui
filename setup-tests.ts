// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom'
import * as matchers from '@testing-library/jest-dom/matchers'
import ResizeObserver from 'resize-observer-polyfill'
import {expect, vi} from 'vitest'

global.ResizeObserver = ResizeObserver
expect.extend(matchers)

vi.mock('react-oidc-context', async (importOriginal) => ({
  ...(await importOriginal<typeof import('react-oidc-context')>()),
  useAuth: () => ({
    isAuthenticated: false,
    signinRedirect: () => {},
    signoutRedirect: () => {},
    signinSilent: () => {},
  }),
}))

vi.mock('./src/config', () => ({
  default: {
    keycloak: {
      public_server_url: 'http://localhost/auth',
      realm_name: 'test',
      client_id: 'client_id',
    },
    services: {
      https: false,
      api_host: 'localhost',
      api_port: 8000,
      api_base_path: '/fairdi/nomad/latest',
    },
  },
}))

// This seems to be an issue with jsdom. Range does not have the
// getBoundingClientRect method dispite it being part of the spec.
// This workaround is needed because Lexical is using getBoundingClientRect.
// https://github.com/jsdom/jsdom/issues/3002
Range.prototype.getBoundingClientRect =
  (() => ({})) as typeof Range.prototype.getBoundingClientRect
