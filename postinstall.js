// This script patches the react-grid-layout package to use flushSync from react-dom. See:
// https://github.com/react-grid-layout/react-grid-layout/pull/2043#issuecomment-2154637714
const fs = require('fs');
const path = require('path');

// Path to the GridItem.js file
const gridItemPath = path.join(__dirname, 'node_modules/react-grid-layout/build/GridItem.js');

// Read the content of the file
fs.readFile(gridItemPath, 'utf8', (err, data) => {
  if (err) {
    console.error('Error reading GridItem.js:', err);
    return;
  }

  // Check if flushSync is already required
  const importStatement = "const { flushSync } = require('react-dom');\n";
  if (!data.includes(importStatement)) {
    // Add the import statement at the top of the file
    data = importStatement + data;
  }

  // Wrap all this.setState(...) calls with flushSync from react-dom
  const updatedData = data.replace(/this\.setState\(([^)]+)\)/g, 'flushSync(() => this.setState($1))');

  // Write the updated content back to the file
  fs.writeFile(gridItemPath, updatedData, 'utf8', (err) => {
    if (err) {
      console.error('Error writing to GridItem.js:', err);
      return;
    }

    console.log('GridItem.js has been successfully updated.');
  });
});
