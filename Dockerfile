# Use the official nginx image as the base image
FROM nginx

# Copy the built webapp from the /build directory to the nginx default directory
RUN mkdir -p /usr/share/nginx/html/prod/v2/gui
COPY ./build /usr/share/nginx/html/prod/v2/gui

# Add nginx configuration to redirect to index.html
COPY nginx.conf /etc/nginx/conf.d/default.conf

# Expose port 80 for the nginx server
EXPOSE 80
