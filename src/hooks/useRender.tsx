import {useCallback, useState} from 'react'

/**
 * A hooks that provides a trigger for a rerender. This is useful for hooks
 * and components that keeps state in a ref and want to control when
 * to re-render.
 *
 * @returns A function that triggers a rerender.
 */
export default function useRender() {
  const setState = useState(0)[1]
  return useCallback(() => setState((n) => n + 1), [setState])
}
