import {act, renderHook, waitFor} from '@testing-library/react'
import {DependencyList} from 'react'
import {FunctionReturningPromise} from 'react-use/lib/misc/types'
import {vi} from 'vitest'

import useAsyncConditional from './useAsyncConditional'

export const renderCount = vi.fn()

afterEach(() => {
  vi.restoreAllMocks()
})

type HookProps = {
  fn: FunctionReturningPromise | undefined
  deps: DependencyList
}

function Hook({fn, deps}: HookProps) {
  renderCount()
  return useAsyncConditional(fn, deps)
}

it('does nothing when called without function', () => {
  const {result} = renderHook(Hook, {
    initialProps: {fn: undefined, deps: []} as HookProps,
  })
  expect(result.current).toEqual({loading: false})
  expect(renderCount).toHaveBeenCalledTimes(1)
})

it('loads when called with a function', async () => {
  const {result} = renderHook(Hook, {
    initialProps: {
      fn: () => Promise.resolve('value'),
      deps: [],
    } as HookProps,
  })
  expect(result.current).toEqual({loading: true})
  expect(renderCount).toHaveBeenCalledTimes(1)
  await waitFor(() =>
    expect(result.current).toEqual({loading: false, value: 'value'}),
  )
  expect(renderCount).toHaveBeenCalledTimes(2)
})

it('loads when the function becomes defined', async () => {
  const {result, rerender} = renderHook(Hook, {
    initialProps: {
      fn: undefined,
      deps: [],
    } as HookProps,
  })
  expect(result.current).toEqual({loading: false})
  expect(renderCount).toHaveBeenCalledTimes(1)
  await act(async () => {
    rerender({fn: () => Promise.resolve('value'), deps: []})
  })
  await waitFor(() =>
    expect(result.current).toEqual({loading: false, value: 'value'}),
  )
  expect(renderCount).toHaveBeenCalledTimes(3)
})

it('reloads when the deps change', async () => {
  const {result, rerender} = renderHook(Hook, {
    initialProps: {
      fn: () => Promise.resolve('value1'),
      deps: [1],
    } as HookProps,
  })
  expect(result.current).toEqual({loading: true})
  expect(renderCount).toHaveBeenCalledTimes(1)
  await waitFor(() =>
    expect(result.current).toEqual({loading: false, value: 'value1'}),
  )
  expect(renderCount).toHaveBeenCalledTimes(2)
  await act(async () => {
    rerender({fn: () => Promise.resolve('value2'), deps: [2]})
  })
  expect(result.current).toEqual({loading: false, value: 'value2'})
  expect(renderCount).toHaveBeenCalledTimes(4)
})

it('does not reload on rerender without changes', async () => {
  const {result, rerender} = renderHook(Hook, {
    initialProps: {
      fn: () => Promise.resolve('value'),
      deps: [],
    } as HookProps,
  })
  expect(result.current).toEqual({loading: true})
  expect(renderCount).toHaveBeenCalledTimes(1)
  await act(async () => {
    rerender({fn: () => Promise.resolve('value'), deps: []})
  })
  expect(result.current).toEqual({loading: false, value: 'value'})
  expect(renderCount).toHaveBeenCalledTimes(2)
})

it('return undefined when the function turns undefined', async () => {
  const {result, rerender} = renderHook(Hook, {
    initialProps: {
      fn: () => Promise.resolve('value'),
      deps: [],
    } as HookProps,
  })
  await waitFor(() =>
    expect(result.current).toEqual({loading: false, value: 'value'}),
  )
  expect(renderCount).toHaveBeenCalledTimes(2)
  await act(async () => {
    rerender({fn: undefined, deps: []})
  })
  expect(result.current).toEqual({loading: false})
  expect(renderCount).toHaveBeenCalledTimes(3)
})
