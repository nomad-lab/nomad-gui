import {act, renderHook, waitFor} from '@testing-library/react'
import {MockedFunction, expect, it, vi} from 'vitest'

import {GraphResponse} from '../models/graphResponseModels'
import * as api from '../utils/api'
import {renderCount} from '../utils/test.helper'
import {JSONObject} from '../utils/types'
import useData from './useData'

let mockedApi: MockedFunction<typeof api.graphApi>

beforeEach(() => {
  mockedApi = vi.spyOn(api, 'graphApi') as MockedFunction<typeof api.graphApi>
})

export function runUseDataTests(
  hookUnderTest: typeof useData,
  mockedApiValue: (request: JSONObject) => GraphResponse,
  wrapper?: React.JSXElementConstructor<{children: React.ReactNode}>,
) {
  const request = {q: '*'}

  it('has no effect without request', () => {
    const {result} = renderHook(hookUnderTest, {
      initialProps: {request: undefined},
      wrapper,
    })
    expect(result.current.loading).toBe(false)
  })

  it('initially fetches data', async () => {
    mockedApi.mockResolvedValue(mockedApiValue({q: 'test_value'}))
    const {result} = renderHook(
      () => {
        renderCount()
        return hookUnderTest({request})
      },
      {wrapper},
    )
    expect(result.current.loading).toBe(true)
    expect(result.current.data).toBeUndefined()
    await waitFor(() => expect(result.current.loading).toBe(false))
    expect(result.current.data).toEqual({q: 'test_value'})
    expect(renderCount).toHaveBeenCalledTimes(2)
  })

  it('does not initially fetch data without implicit fetch', async () => {
    mockedApi.mockResolvedValue(mockedApiValue({q: 'test_value'}))
    const {result} = renderHook(hookUnderTest, {
      initialProps: {request, noImplicitFetch: true},
      wrapper,
    })
    expect(result.current.loading).toBe(false)
    expect(result.current.data).toBeUndefined()
  })

  it('returns stale data while fetching changed request', async () => {
    mockedApi.mockResolvedValue(mockedApiValue({q: 'test_value'}))
    const {result, rerender} = renderHook(hookUnderTest, {
      initialProps: {
        request,
      },
      wrapper,
    })
    await waitFor(() => expect(result.current.loading).toBe(false))
    expect(result.current.data).toEqual({q: 'test_value'})

    mockedApi.mockResolvedValue(mockedApiValue({q: 'new_value'}))
    rerender({request: {...request}})
    expect(result.current.loading).toBe(true)
    expect(result.current.data).toEqual({q: 'test_value'})
    await waitFor(() => expect(result.current.loading).toBe(false))
    expect(result.current.data).toEqual({q: 'new_value'})
  })

  it('does not fetch data, if the request did not change', async () => {
    mockedApi.mockResolvedValue(mockedApiValue({q: 'test_value'}))
    const {result, rerender} = renderHook(hookUnderTest, {
      initialProps: {request},
      wrapper,
    })
    await waitFor(() => expect(result.current.loading).toBe(false))
    expect(result.current.data).toEqual({q: 'test_value'})
    rerender({request})
    expect(result.current.loading).toBe(false)
  })

  it('refreshes data when refresh is called', async () => {
    mockedApi.mockResolvedValue(mockedApiValue({q: 'test_value'}))
    const {result} = renderHook(hookUnderTest, {
      initialProps: {request},
      wrapper,
    })
    await waitFor(() => expect(result.current.loading).toBe(false))
    expect(result.current.data).toEqual({q: 'test_value'})

    mockedApi.mockResolvedValue(mockedApiValue({q: 'new_value'}))
    act(() => result.current.fetch())
    await waitFor(() => expect(result.current.loading).toBe(true))
    await waitFor(() => expect(result.current.loading).toBe(false))
    expect(result.current.data).toEqual({q: 'new_value'})
  })

  it('calls onFetch', async () => {
    const onFetch = vi.fn()
    const response = mockedApiValue({q: 'test_value'})
    mockedApi.mockResolvedValue(response)
    const {result} = renderHook(
      () => {
        renderCount()
        return hookUnderTest({request, onFetch})
      },
      {wrapper},
    )
    expect(result.current.loading).toBe(true)
    expect(result.current.data).toBeUndefined()
    await waitFor(() => expect(result.current.loading).toBe(false))
    expect(result.current.data).toEqual({q: 'test_value'})
    expect(onFetch).toHaveBeenCalledWith(
      {
        q: 'test_value',
      },
      response,
    )
    expect(renderCount).toHaveBeenCalledTimes(2)
  })
}
