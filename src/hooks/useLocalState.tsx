import {
  Dispatch,
  MutableRefObject,
  SetStateAction,
  useCallback,
  useMemo,
  useRef,
} from 'react'
import {usePrevious} from 'react-use'

import useRender from './useRender'

export type UseLocalStateOptions<O, I> = {
  /**
   * An optional callback that is called on state update, if
   * the state has changed with respect to value. This
   * is useful to report the change to the controlling
   * parent.
   */
  onChange?: (value: I) => void
  /**
   * An optional transform function, if the internal state should
   * be derived from the value prop. Default is identity.
   */
  transform?: (value: O, current: I | undefined) => I
}

/**
 * Just like react's useState, but the value is updated when the passed value
 * has changed. This can be useful for semi-controlled components that keep
 * an internal derived state that needs to be overwritten from a changing prop.
 * @param value The value to keep in sync with the state, also the initial value
 * @param options Additional options to customize the behavior
 * @returns The current internal state, a setter function, and a ref to the state.
 */
export default function useLocalState<O, I = O>(
  value: O,
  options?: UseLocalStateOptions<O, I>,
): [I, Dispatch<SetStateAction<I>>, MutableRefObject<I>] {
  const {onChange, transform} = options || ({} as UseLocalStateOptions<O, I>)

  const render = useRender()
  const stateRef = useRef<I>()
  const transformedValue = useMemo(() => {
    return transform
      ? transform(value, stateRef.current)
      : (value as unknown as I)
  }, [stateRef, value, transform])
  const previousValue = usePrevious(transformedValue)

  if (previousValue !== transformedValue) {
    stateRef.current = transformedValue
  }

  const state = stateRef.current as I

  const setState: Dispatch<SetStateAction<I>> = useCallback(
    (valueOrSetter) => {
      let changedValue
      if (typeof valueOrSetter === 'function') {
        changedValue = (valueOrSetter as (value: I) => I)(stateRef.current as I)
      } else {
        changedValue = valueOrSetter
      }

      if (stateRef.current !== changedValue) {
        render()
      }
      stateRef.current = changedValue

      if (transformedValue !== changedValue) {
        onChange?.(changedValue)
      }
    },
    [onChange, render, transformedValue],
  )

  return [state, setState, stateRef as MutableRefObject<I>]
}
