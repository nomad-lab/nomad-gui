import lodash from 'lodash'
import {useCallback, useEffect, useRef, useState} from 'react'
import {usePrevious} from 'react-use'

import DevToolsJsonViewer from '../components/devTools/DevToolsJsonViewer'
import useDevTools from '../components/devTools/useDevTools'
import {GraphRequest} from '../models/graphRequestModels'
import {GraphResponse} from '../models/graphResponseModels'
import {graphApi} from '../utils/api'
import {resolveAllMDefs} from '../utils/metainfo'
import {DefaultToObject, JSONObject, JSONValue} from '../utils/types'
import useAuth from './useAuth'
import useRender from './useRender'

/**
 * Returns a new request object that reduces all keys with path separators
 * in the given request. E.g. `{uploads/id:{'*':'*'}}` will become `{uploads:{id:{'*':'*'}}}`.
 */
export function normalizeGraph(request: JSONObject): JSONObject {
  function expandPaths(path: string, obj: JSONValue): JSONObject {
    const pathSegments = path.split('/')
    const result = {} as JSONObject
    pathSegments.reduce(
      (currentRequest: JSONObject, pathSegment: string, index) => {
        const isLast = index === pathSegments.length - 1
        const next = isLast ? (obj as JSONObject) : {}
        currentRequest[pathSegment] = next
        return next
      },
      result,
    )
    return result
  }
  const result = {} as JSONObject
  Object.keys(request).forEach((key) => {
    const value = request[key]
    let normalizedValue: JSONValue
    if (Array.isArray(value)) {
      normalizedValue = value.map((item) =>
        lodash.isObject(item) ? normalizeGraph(item as JSONObject) : item,
      )
    } else if (lodash.isObject(value)) {
      normalizedValue = normalizeGraph(value as JSONObject)
    } else {
      normalizedValue = value
    }
    Object.assign(result, expandPaths(key, normalizedValue))
  })
  return result
}

export type UseDataParams<Request, Response> = {
  /**
   * The request object that is used to fetch data. A different request will
   * trigger a new fetch.
   */
  request?: Request

  /**
   * If false (default), the data will be fetched immediately and if the
   * request changes. If true, the data will only be fetched when
   * fetch is called.
   */
  noImplicitFetch?: boolean

  /**
   * An optional callback that is called when the data was successfully
   * received. This allows to trigger state changes now, instead of needing
   * to wait for the re-render and using an `useEffect` to cause yet another render.
   */
  onFetch?: (
    data: DefaultToObject<Response>,
    fullResponse: GraphResponse,
  ) => void

  /**
   * An optional callback that is called when an error occurred during the fetch.
   */
  onError?: (error: Error) => void
}

export type UseDataResult<Response> = {
  data?: DefaultToObject<Response>
  loading: boolean
  /**
   * A callback that triggers a new fetch when called. This will
   * cause two renders, one with loading=true and one with the new data.
   */
  fetch: () => void
}

/**
 * A hook that fetches data from the API based on a request.
 * @returns The response containing the fetched data or undefined if the data
 *  is not available yet.
 */
export default function useData<
  Request extends object = JSONObject,
  Response extends object = JSONObject,
>({
  request,
  noImplicitFetch = false,
  onFetch,
  onError,
}: UseDataParams<Request, Response>): UseDataResult<Response> {
  const {user} = useAuth()
  const requestRef = useRef<Request | undefined>()
  const normalizedRequestRef = useRef<GraphRequest | undefined>()
  const [data, setData] = useState<GraphResponse | undefined>()
  const render = useRender()

  if (request !== usePrevious(request) && request && !noImplicitFetch) {
    requestRef.current = request
  }

  const fetch = useCallback(() => {
    if (!request) {
      return
    }
    requestRef.current = request
    const fetch = async () => {
      normalizedRequestRef.current = normalizeGraph(request as JSONObject)
      try {
        const result = await graphApi(
          normalizedRequestRef.current,
          user || undefined,
        )

        // TODO Similar to the loader, we need to deal with indexKeys that
        // have been answered with arrays.

        // TODO We should only resolve m_defs in archive data and not in uploads,
        // entries, files, etc. Currently we apply this to all and the complete
        // responses no matter what they are.
        resolveAllMDefs(result as JSONObject)
        if (requestRef.current === request) {
          requestRef.current = undefined
          setData(result as JSONObject)
          onFetch?.(result as DefaultToObject<Response>, result)
        }
      } catch (error) {
        onError?.(error as Error)
      }
    }
    fetch()
    return () => {
      requestRef.current = undefined
    }
  }, [
    requestRef,
    setData,
    normalizedRequestRef,
    request,
    onFetch,
    onError,
    user,
  ])

  useEffect(() => {
    if (!noImplicitFetch) {
      return fetch()
    }
  }, [fetch, noImplicitFetch])

  const fetchAndRender = useCallback(() => {
    fetch()
    render()
  }, [fetch, render])

  const devTools = useDevTools()
  useEffect(() => {
    devTools.registerTool(
      'data',
      <DevToolsJsonViewer
        value={{
          request: normalizedRequestRef.current,
          response: data,
        }}
      />,
    )
    return () => devTools.unregisterTool('routes-data')
  }, [data, requestRef, devTools, normalizedRequestRef])

  return {
    data: data as DefaultToObject<Response> | undefined,
    loading: requestRef.current !== undefined,
    fetch: fetchAndRender,
  }
}
