import {User} from 'oidc-client-ts'
import React, {useMemo} from 'react'

import {ENV} from '../utils/env'
import {ApiContext} from './ApiContext'
import {useIsApiLoading} from './useApi'
import useAuth from './useAuth'

function getHeaders(user: User | null | undefined) {
  const headers: HeadersInit = {
    'Content-Type': 'application/json',
  }
  if (user?.access_token) {
    headers['Authorization'] = `Bearer ${user.access_token}`
  }
  return headers
}

function getUrl(path: string) {
  return `${ENV.API_URL}/${path}`
}

export function ApiProvider({children}: React.PropsWithChildren) {
  const {user} = useAuth()
  const setLoading = useIsApiLoading()[1]

  const api = useMemo(() => {
    return {
      suggestions: async (quantities: string[], input: string) => {
        setLoading(true)
        const url = getUrl('suggestions')
        try {
          const response = await fetch(url, {
            method: 'POST',
            headers: getHeaders(user),
            body: JSON.stringify({input: input, quantities: quantities}),
          })
          const responseData = await response.json()
          return responseData
        } finally {
          setLoading(false)
        }
      },
      query: async (
        searchTarget: string,
        search: unknown,
        config: {returnRequest: boolean},
      ) => {
        setLoading(true)
        const url = getUrl(`${searchTarget}/query`)
        const request: {
          method: string
          path: string
          url: string
          body: unknown
          response?: unknown
        } = {
          method: 'POST',
          path: `${searchTarget}/query`,
          url: url,
          body: search,
        }
        try {
          const response = await fetch(url, {
            method: 'POST',
            headers: getHeaders(user),
            body: JSON.stringify(search),
          })
          const responseData = await response.json()
          if (config.returnRequest) {
            request.response = responseData
            return request
          } else {
            return responseData
          }
        } finally {
          setLoading(false)
        }
      },
      get: async (path: string) => {
        setLoading(true)
        try {
          const response = await fetch(getUrl(path), {
            method: 'GET',
            headers: getHeaders(user),
          })

          if (response.ok) {
            return response.json()
          } else {
            throw new Error(
              `API request failed with status: ${response.status}`,
            )
          }
        } finally {
          setLoading(false)
        }
      },
      post: async (path: string, body: Record<string, unknown>) => {
        setLoading(true)
        try {
          const response = await fetch(getUrl(path), {
            method: 'POST',
            headers: getHeaders(user),
            body: JSON.stringify(body),
          })

          if (response.ok) {
            return response.json()
          } else {
            throw new Error(
              `API request failed with status: ${response.status}`,
            )
          }
        } finally {
          setLoading(false)
        }
      },
    }
  }, [user, setLoading])

  return <ApiContext.Provider value={api}>{children}</ApiContext.Provider>
}
