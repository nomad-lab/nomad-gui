import {act, render, renderHook, screen} from '@testing-library/react'
import {vi} from 'vitest'

import useLocalStateWithoutRenderCounter from './useLocalState'

describe('useLocalState', () => {
  const renderCount = vi.fn()
  const useLocalState: typeof useLocalStateWithoutRenderCounter = (...args) => {
    renderCount()
    return useLocalStateWithoutRenderCounter(...args)
  }
  beforeEach(() => {
    renderCount.mockClear()
  })
  it('provides the initial value', () => {
    const {
      result: {
        current: [state],
      },
    } = renderHook(() => useLocalState(42))
    expect(state).toBe(42)
    expect(renderCount).toHaveBeenCalledTimes(1)
  })
  it('updates with local state change', () => {
    const {result} = renderHook(() => useLocalState(42))

    const [, setState] = result.current
    act(() => {
      setState(43)
      setState(44)
    })
    expect(result.current[0]).toBe(44)
    expect(renderCount).toHaveBeenCalledTimes(2)
  })
  it('does not update on same value setter', () => {
    const {result} = renderHook(() => useLocalState(42))

    const [, setState] = result.current
    act(() => setState(42))
    expect(result.current[0]).toBe(42)
    expect(renderCount).toHaveBeenCalledTimes(1)
  })
  it('updates with local state change and set function', () => {
    const {result} = renderHook(() => useLocalState(42))

    const [, setState] = result.current
    act(() => setState((value) => value + 1))
    expect(result.current[0]).toBe(43)
    expect(renderCount).toHaveBeenCalledTimes(2)
  })
  it('updates with prop change', () => {
    const {result, rerender} = renderHook(
      ({value}: {value: number}) => useLocalState(value),
      {initialProps: {value: 42}},
    )
    rerender({value: 43})
    expect(result.current[0]).toBe(43)
    expect(renderCount).toHaveBeenCalledTimes(2)
  })
  it('does not update on rerender with unchanged prop', () => {
    const {result, rerender} = renderHook(
      ({value}: {value: number}) => useLocalState(value),
      {initialProps: {value: 42}},
    )
    act(() => result.current[1](43))
    rerender({value: 42})
    expect(result.current[0]).toBe(43)
    expect(renderCount).toHaveBeenCalledTimes(3)
  })
  it('transforms the value', () => {
    const {result, rerender} = renderHook(
      ({value}: {value: number}) =>
        useLocalState(value, {transform: (value) => value.toString()}),
      {initialProps: {value: 42}},
    )
    expect(result.current[0]).toBe('42')
    rerender({value: 43})
    expect(result.current[0]).toBe('43')
    expect(renderCount).toHaveBeenCalledTimes(2)
  })
  it('calls onChange on state change', () => {
    const handleChange = vi.fn()

    const {result} = renderHook(() =>
      useLocalState(42, {onChange: handleChange}),
    )
    const [, setState] = result.current
    act(() => setState(43))
    expect(result.current[0]).toBe(43)
    expect(renderCount).toHaveBeenCalledTimes(2)
    expect(handleChange).toHaveBeenCalledTimes(1)
  })
  it('calls onChange on state change only if value has changed', () => {
    const handleChange = vi.fn()

    const {result} = renderHook(() =>
      useLocalState(42, {onChange: handleChange}),
    )
    const [, setState] = result.current
    act(() => setState(43))
    act(() => setState(42))
    expect(result.current[0]).toBe(42)
    expect(renderCount).toHaveBeenCalledTimes(3)
    // only called once during the change from 42 to 43
    // but not on changing it back from 43 to the original 42
    expect(handleChange).toHaveBeenCalledTimes(1)
  })
  it('supports undefined as value in local state change', () => {
    const {result} = renderHook(() =>
      useLocalState<number | undefined>(undefined),
    )
    const [, setState] = result.current
    act(() => setState(42))
    expect(result.current[0]).toBe(42)
    expect(renderCount).toHaveBeenCalledTimes(2)
    act(() => setState(undefined))
    expect(result.current[0]).toBe(undefined)
    expect(renderCount).toHaveBeenCalledTimes(3)
  })
  it('supports nested use', () => {
    function Inner({
      state: controlledState,
      onStateChange,
    }: {
      state: number | undefined
      onStateChange: (value: number | undefined) => void
    }) {
      const [state, setState] = useLocalState(controlledState, {
        onChange: onStateChange,
      })
      return (
        <button onClick={() => setState((state) => (state ? undefined : 42))}>
          {state}
        </button>
      )
    }
    function Outer() {
      const [state, setState] = useLocalState<number | undefined>(undefined)
      return (
        <>
          <Inner state={state} onStateChange={setState} />
          {state ? 'defined' : 'undefined'}
        </>
      )
    }
    render(<Outer />)
    expect(screen.queryByText('42')).not.toBeInTheDocument()
    expect(screen.getByText('undefined')).toBeInTheDocument()
    act(() => {
      screen.getByRole('button').click()
    })
    expect(screen.queryByText('42')).toBeInTheDocument()
    expect(screen.getByText('defined')).toBeInTheDocument()
    act(() => {
      screen.getByRole('button').click()
    })
    expect(screen.queryByText('42')).not.toBeInTheDocument()
    expect(screen.getByText('undefined')).toBeInTheDocument()
  })
})
