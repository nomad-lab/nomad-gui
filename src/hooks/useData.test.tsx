import {expect, it, vi} from 'vitest'

import {GraphResponse} from '../models/graphResponseModels'
import useData, {normalizeGraph} from './useData'
import {runUseDataTests} from './useData.shared'

vi.mock('../utils/api')

afterEach(() => {
  vi.restoreAllMocks()
})

describe('testing request normalization', () => {
  it('does nothing, if not necessary', () => {
    expect(normalizeGraph({a: 'b'})).toEqual({a: 'b'})
  })
  it('replaces keys', () => {
    expect(normalizeGraph({'a/b': 'c'})).toEqual({a: {b: 'c'}})
  })
  it('is recursive', () => {
    expect(normalizeGraph({a: {'b/c': 'd'}})).toEqual({a: {b: {c: 'd'}}})
  })
  it('respects arrays', () => {
    expect(normalizeGraph({m_request: {exclude: ['*']}})).toEqual({
      m_request: {exclude: ['*']},
    })
  })
})

describe('useData', () => {
  runUseDataTests(useData, (request) => request as GraphResponse)
})
