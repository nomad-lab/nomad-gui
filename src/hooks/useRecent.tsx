import {useCallback, useEffect} from 'react'

import useStorage, {Scope} from './useStorage'

function useSortedStorage<T>(
  scope: Scope,
  key: string,
  compareFn: (a: T[keyof T], b: T[keyof T]) => number,
): [value: T, set: (value: T | ((value: T) => T)) => void, reset: () => void] {
  const [storage, setStorage, reset] = useStorage<T>(scope, key)

  const set = useCallback(
    (value: T | ((value: T) => T)) => {
      const update = (value: T) => {
        const storageList = Object.entries(value as object)
        storageList.sort((a, b) => compareFn(a[1], b[1]))
        return Object.fromEntries(storageList) as T
      }
      if (typeof value !== 'function') {
        setStorage(update(value))
      } else {
        setStorage((prev) => update((value as (value: T) => T)(prev)))
      }
    },
    [setStorage, compareFn],
  )

  return [storage, set, reset]
}

type WithTime<T> = {
  [key: string | number]: {
    timestamp: number
    value: T
  }
}

export default function useRecent<T>(
  scope: Scope,
  key: string,
  append?: {[key: string]: T},
): {
  add: (key: string, value: T) => void
  recent: (n: number) => T[]
  remove: (key: string) => void
  reset: () => void
} {
  const [recentStorage, setStorage, reset] = useSortedStorage<WithTime<T>>(
    scope,
    key,
    useCallback((a, b) => b.timestamp - a.timestamp, []),
  )

  const recent = useCallback(
    (n: number) => {
      if (!recentStorage) return []
      const allRecent = Object.values(recentStorage).map(
        (recent) => recent.value,
      ) as T[]
      return allRecent.slice(0, n)
    },
    [recentStorage],
  )

  const add = useCallback(
    (itemKey: string, value: T) => {
      const currentDate = new Date()
      const timestamp = currentDate.getTime()
      const newAct = {timestamp: timestamp, value: value}
      setStorage((recentStorage) => ({...recentStorage, [itemKey]: newAct}))
    },
    [setStorage],
  )

  const remove = useCallback(
    (key: string) => {
      setStorage((recentStorage) => {
        if (key in recentStorage) {
          delete recentStorage[key]
        }
        return recentStorage
      })
    },
    [setStorage],
  )

  useEffect(() => {
    if (append) {
      Object.entries(append).forEach(([key, value]) => add(key, value))
    }
  }, [add, append])

  return {add, recent, remove, reset}
}
