import {act, renderHook} from '@testing-library/react'
import {vi} from 'vitest'

import useRefState from './useRefState'

describe('useRefState', () => {
  it('should return a get and set, the set works immediatly', () => {
    const {
      rerender,
      result: {
        current: [get, set],
      },
    } = renderHook(() => useRefState(0))

    expect(get()).toBe(0)
    set(1)
    expect(get()).toBe(1)
    rerender()
    expect(get()).toBe(1)
  })

  it('should not rerender unless requested to', () => {
    const renderCount = vi.fn()
    const {
      result: {
        current: [get, set],
      },
    } = renderHook(() => {
      renderCount()
      return useRefState(0)
    })

    act(() => {
      set(1, false)
    })
    expect(renderCount).toBeCalledTimes(1)
    expect(get()).toBe(1)

    act(() => {
      set(2, true)
    })
    expect(get()).toBe(2)
    expect(renderCount).toBeCalledTimes(2)
  })
})
