import {useAuth as useOIDCAuth} from 'react-oidc-context'

export default function useAuth() {
  // We provide a minimal mock here to allow storybook to render the components
  // depending on useAuth.
  return (
    useOIDCAuth() || {
      signinSilent: () => {},
    }
  )
}
