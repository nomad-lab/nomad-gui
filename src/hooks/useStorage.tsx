import {useCallback, useMemo, useState} from 'react'

export type Scope = 'global' | 'upload' | 'entry'

type Storage = {
  [key in Scope]: {
    [key: string]: unknown
  }
}

function setUserStorage(key: string, storage: Storage) {
  localStorage.setItem(key, JSON.stringify(storage || {}))
}

export default function useStorage<T>(
  scope: Scope,
  key: string,
): [value: T, set: (value: T | ((value: T) => T)) => void, reset: () => void] {
  const currentUser = 'guest'

  const getStorage = useCallback(() => {
    const userStorageString = localStorage.getItem(currentUser)
    const userStorage = JSON.parse(userStorageString || '{}')
    return userStorage as Storage
  }, [])

  const [value, setValue] = useState<T>(getStorage()?.[scope]?.[key] as T)

  const set = useCallback(
    (value: T | ((value: T) => T)) => {
      const updatedUserStorage = getStorage()
      if (!(scope in updatedUserStorage)) {
        updatedUserStorage[scope] = {}
      }
      function isCallable(
        value: T | ((value: T) => T),
      ): value is (value: T) => T {
        return typeof value === 'function'
      }
      const newValue = isCallable(value)
        ? value(updatedUserStorage[scope][key] as T)
        : (value as T)
      updatedUserStorage[scope][key] = newValue
      setValue(newValue)
      setUserStorage(currentUser, updatedUserStorage)
    },
    [key, scope, getStorage],
  )

  const reset = useCallback(() => {
    const storage = getStorage()
    if (scope in storage && key in storage[scope]) {
      const currentUserStorage = {...storage}
      const currentScopeStorage = currentUserStorage[scope]
      delete currentScopeStorage[key]
      setUserStorage(currentUser, currentUserStorage)
    }
  }, [key, scope, getStorage])

  const response = useMemo<
    [value: T, set: (value: T | ((value: T) => T)) => void, reset: () => void]
  >(() => {
    return [value, set, reset]
  }, [value, set, reset])

  return response
}
