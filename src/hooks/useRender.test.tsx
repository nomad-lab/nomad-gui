import {act, renderHook} from '@testing-library/react'
import {vi} from 'vitest'

import useRender from './useRender'

it('renders when render is called', () => {
  const renderCount = vi.fn()
  const {result} = renderHook(() => {
    renderCount()
    return useRender()
  })
  expect(result.current).toBeInstanceOf(Function)
  expect(renderCount).toHaveBeenCalledTimes(1)
  act(() => result.current())
  expect(renderCount).toHaveBeenCalledTimes(2)
  act(() => result.current())
  expect(renderCount).toHaveBeenCalledTimes(3)
})
