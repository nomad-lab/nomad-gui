import {DependencyList} from 'react'
import {useDebounce} from 'react-use'

export default function useEditDebounce(
  fn: () => unknown,
  deps?: DependencyList,
) {
  useDebounce(fn, 500, deps)
}
