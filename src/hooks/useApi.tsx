import {useContext} from 'react'
import {useRecoilState} from 'recoil'

import {Api, ApiContext, isApiLoadingState} from './ApiContext'

export default function useApi(): Api | undefined {
  return useContext(ApiContext)
}

export function useIsApiLoading() {
  return useRecoilState(isApiLoadingState)
}
