import {useCallback, useRef} from 'react'

import useRender from './useRender'

/**
 * This hook is similar to `useState` from `react` but it returns a getter and
 * a setter and allows to set the value without triggering a rerender.
 *
 * The getter is a function that returns the current value.
 * The setter is a function that updates the current value and optionally
 * triggers a rerender based on the second argument.
 *
 * @param initialState The initial state.
 * @returns An tuple with getter and setter functions.
 */
export default function useRefState<T>(
  initialState: T,
): [() => T, (value: T, rerender?: boolean) => void] {
  const render = useRender()
  const ref = useRef<T>(initialState)
  const get = useCallback(() => ref.current, [ref])
  const set = useCallback(
    (value: T, rerender: boolean = false) => {
      ref.current = value
      if (rerender) {
        render()
      }
    },
    [ref, render],
  )

  return [get, set]
}
