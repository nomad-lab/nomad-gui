import {DependencyList, useEffect, useMemo, useRef} from 'react'
import {useMountedState} from 'react-use'
import {FunctionReturningPromise, PromiseType} from 'react-use/lib/misc/types'
import {AsyncState} from 'react-use/lib/useAsyncFn'

import useRender from './useRender'

/**
 * This hook is similar to `useAsync` from `react-use` but it allows the function
 * to be undefined. The function is only (re-)called
 * if it is defined and the dependencies have changed (i.e. the function has changed).
 *
 * @param fn The async function to call.
 * @param deps The dependency list for the async function.
 * @returns And async state object.
 */
export default function useAsyncConditional<T extends FunctionReturningPromise>(
  fn: T | undefined,
  deps: DependencyList = [],
): AsyncState<PromiseType<ReturnType<T>>> {
  const render = useRender()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const memoizedFn = useMemo(() => fn && (() => fn()), [!!fn, ...deps]) as
    | T
    | undefined
  const isMounted = useMountedState()
  const lastCallId = useRef(0)
  const lastCalledFn = useRef<T | undefined>()
  const doLoad = !!memoizedFn && lastCalledFn.current !== memoizedFn
  const state = useRef<AsyncState<PromiseType<ReturnType<T>>>>({
    loading: doLoad,
  })

  if (doLoad) {
    state.current = {loading: true}
  }

  if (!memoizedFn) {
    lastCalledFn.current = undefined
    state.current = {loading: false}
  }

  useEffect(() => {
    if (!doLoad) {
      return
    }

    lastCalledFn.current = memoizedFn
    const callId = ++lastCallId.current

    memoizedFn?.().then(
      (value) => {
        if (isMounted() && callId === lastCallId.current) {
          state.current = {value, loading: false}
          render()
        }
      },
      (error) => {
        if (isMounted() && callId === lastCallId.current) {
          state.current = {error, loading: false}
          render()
        }
      },
    )
  }, [memoizedFn, lastCalledFn, isMounted, state, doLoad, render])

  return state.current
}
