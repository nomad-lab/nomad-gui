import {createContext} from 'react'
import {atom} from 'recoil'

export type Api = {
  query: (
    searchTarget: string,
    search: unknown,
    config: {returnRequest: boolean},
  ) => Promise<unknown>
  suggestions: (quantities: string[], input: string) => Promise<unknown>
  get: (url: string) => Promise<unknown>
  post: (url: string, body: Record<string, unknown>) => Promise<unknown>
}

export const ApiContext = createContext<Api | undefined>(undefined)

export const isApiLoadingState = atom({
  key: `isApiLoadingState`,
  default: false,
})
