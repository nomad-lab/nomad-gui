import '@fontsource/titillium-web/400.css'
import '@fontsource/titillium-web/700.css'
import {LocalizationProvider} from '@mui/x-date-pickers'
import {AdapterDateFns} from '@mui/x-date-pickers/AdapterDateFnsV3'
import React from 'react'
import ReactDOM from 'react-dom/client'
import {RecoilRoot} from 'recoil'

import '../node_modules/react-grid-layout/css/styles.css'
import '../node_modules/react-resizable/css/styles.css'
import Routes from './Routes'
import {AuthProvider} from './components/app/auth'
import DevTools from './components/devTools/DevTools'
import Theme from './components/theme/Theme'
import UnitProvider from './components/units/UnitProvider'
import {ApiProvider} from './hooks/ApiProvider'
import './index.css'
import {ENV} from './utils/env'

// import reportWebVitals from './reportWebVitals'

async function enableMocking() {
  if (!ENV.USE_MOCKED_API) {
    return
  }
  const {httpWorker} = await import('./mocks/httpWorker')
  const serviceWorker = ENV.MODE === 'production' && {
    url:
      ENV.BASE_URL === '/'
        ? `${ENV.BASE_URL}mockServiceWorker.js`
        : `${ENV.BASE_URL}/mockServiceWorker.js`,
  }
  await httpWorker.start({
    ...(serviceWorker ? {serviceWorker} : {}),
    onUnhandledRequest: (request, print) => {
      if (request.url.toString().startsWith(ENV.API_URL)) {
        print.error()
      }
    },
  })
}

enableMocking().then(() => {
  const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement,
  )
  root.render(
    <React.StrictMode>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <RecoilRoot>
          <Theme>
            <AuthProvider>
              <ApiProvider>
                <UnitProvider>
                  <DevTools enabled>
                    <Routes />
                  </DevTools>
                </UnitProvider>
              </ApiProvider>
            </AuthProvider>
          </Theme>
        </RecoilRoot>
      </LocalizationProvider>
    </React.StrictMode>,
  )
})

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals()
