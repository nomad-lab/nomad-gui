import {Config} from './models/config'

// We use a minial mock here to allow storybook render the components even
// without a full config
const config = (window as unknown as {nomadConfig: Required<Config>})
  .nomadConfig || {keycloak: {}, services: {}}
export default config
