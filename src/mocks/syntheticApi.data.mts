import {faker} from '@faker-js/faker'
import fs from 'fs'
import lodash from 'lodash'
import path from 'path'
import {fileURLToPath} from 'url'

import {DirectoryResponse, FileResponse} from '../models/graphResponseModels'
import {JSONObject, JSONValue} from '../utils/types'

function entryMetadata() {
  return {
    references: faker.helpers.multiple(() => faker.internet.url(), {
      count: {min: 1, max: 5},
    }),
    authors: faker.helpers.multiple(
      () => ({
        name: faker.person.fullName(),
      }),
      {count: {min: 1, max: 5}},
    ),
    datasets: faker.helpers.multiple(
      () => ({
        dataset_name: faker.lorem.sentence({min: 3, max: 15}),
      }),
      {count: {min: 1, max: 5}},
    ),
  }
}

function createEntry(template: JSONObject): JSONObject {
  const metadata = {
    ...entryMetadata(),
    entry_name: faker.lorem.sentence({min: 3, max: 15}),
    entry_type: faker.helpers.arrayElement([
      'VASP calculation',
      'ELN',
      'Other',
    ]),
  }
  return {
    entry_id: faker.git.commitSha({length: 29}),
    metadata,
    complete_time: faker.date.anytime().toISOString(),
    entry_create_time: faker.date.anytime().toISOString(),
    process_status: faker.helpers.arrayElement([
      'PROCESS',
      'READY',
      'FAILED',
      'SUCCESS',
    ]),
    archive: {
      m_def: {
        m_def: '/metainfo/nomad.datamodel/section_definitions/EntryArchive',
      },
      metadata: {
        m_def: {
          m_def: '/metainfo/nomad.datamodel/section_definitions/EntryMetadata',
        },
        ...metadata,
      },
      material: {
        m_def: {
          m_def:
            '/metainfo/nomad.datamodel.material_properties/section_definitions/Material',
        },
        formula: 'GaAs',
      },
      data: {
        m_def: {
          m_def:
            '/metainfo/nomad.plugin.simulation/section_definitions/Simulation',
        },
        my_quantity: 'value',
      },
    },
    ...template,
  }
}

function createEntities(
  count: number,
  factory: (index: number) => [string, JSONObject],
): JSONObject {
  return Array.from(Array(count).keys()).reduce((current, index) => {
    const [key, value] = factory(index)
    return {
      ...current,
      [key]: value,
    }
  }, {})
}

function createDirectory(
  depth: number = 0,
  prototype?: JSONObject,
  directories?: number,
  files?: number,
): JSONObject {
  directories =
    directories === undefined ? faker.number.int({min: 1, max: 5}) : directories
  files = files === undefined ? faker.number.int({min: 1, max: 10}) : files
  return {
    m_is: 'Directory',
    ...(directories && depth > 0
      ? createEntities(directories, () => {
          return [
            faker.system.fileName({extensionCount: 0}),
            createDirectory(depth - 1, prototype),
          ]
        })
      : {}),
    ...(files
      ? createEntities(files, (index) => {
          return [
            index <= 1
              ? faker.system.commonFileName(['json', 'pdf'][index])
              : faker.system.fileName(),
            {m_is: 'File', size: faker.number.int({min: 24, max: 1024 * 1024})},
          ]
        })
      : {}),
    ...(prototype || {}),
  }
}

function uploadMetadata() {
  return {
    main_author: {
      name: faker.person.fullName(),
    },
    coauthors: faker.helpers.multiple(
      () => ({
        name: faker.person.fullName(),
      }),
      {count: {min: 0, max: 5}},
    ),
  }
}

function createUpload(entries: number, template: JSONObject = {}): JSONObject {
  const upload = {
    upload_id: faker.git.commitSha({length: 29}),
    ...template,
  }
  const uploadFiles = createDirectory(3, {}, 3, 10)
  const uploadEntries: {[key: string]: JSONObject} = {}
  function walkFiles(
    fileOrDirectory: DirectoryResponse | FileResponse,
    path: string = '',
  ) {
    if (fileOrDirectory.m_is !== 'Directory') {
      return
    }
    const directory = fileOrDirectory as DirectoryResponse

    Object.entries(directory).forEach(([name, fileOrDirectory], index) => {
      if (
        (fileOrDirectory as FileResponse | DirectoryResponse).m_is === 'File' &&
        path.split('/').length <= 2 &&
        index !== 4 &&
        index !== 5 &&
        index !== 7
      ) {
        const file = fileOrDirectory as FileResponse
        const entry = createEntry({mainfile_path: `${path}/${name}`})
        entry.upload_id = upload.upload_id
        file.entry = entry
        uploadEntries[entry.entry_id as string] = entry
      }
      walkFiles(
        fileOrDirectory as FileResponse | DirectoryResponse,
        `${path}/${name}`,
      )
    })
  }
  walkFiles(uploadFiles)

  return {
    upload_name: faker.lorem.sentence({min: 3, max: 15}),
    published: faker.datatype.boolean(),
    process_status: faker.helpers.arrayElement([
      'PROCESS',
      'READY',
      'FAILED',
      'SUCCESS',
    ]),
    complete_time: faker.date.anytime().toISOString(),
    upload_create_time: faker.date.anytime().toISOString(),
    entries: uploadEntries,
    files: uploadFiles,
    ...uploadMetadata(),
    ...upload,
  }
}

function createExampleUpload(): JSONObject {
  const upload_id = faker.git.commitSha({length: 29})
  const metadata = {
    ...entryMetadata(),
    entry_name: 'CP2K calculation',
    entry_type: 'CP2K calculation',
  }
  const entry1 = createEntry({
    upload_id,
    mainfile_path: '/simulation/cp2k/co2_probe_tip.out',
    complete_time: faker.date.anytime().toISOString(),
    entry_create_time: faker.date.anytime().toISOString(),
    process_status: 'SUCCESS',
    metadata,
    archive: {
      m_def: {
        m_def: '/metainfo/nomad.datamodel/section_definitions/EntryArchive',
      },
      metadata: {
        m_def: {
          m_def: '/metainfo/nomad.datamodel/section_definitions/EntryMetadata',
        },
        ...metadata,
      },
      material: {
        m_def: {
          m_def:
            '/metainfo/nomad.datamodel.material_properties/section_definitions/Material',
        },
        formula: 'CO2Cu255',
      },
      data: {
        m_def: {
          m_def:
            '/metainfo/nomad.plugin.simulation/section_definitions/Simulation',
        },
        description: 'This is a CP2K calculation of CO2 on Cu(110)',
        aux_files: ['./co2_probe_tip.out'],
        method: {
          m_def: {
            m_def:
              '/metainfo/nomad.plugin.simulation/section_definitions/Method',
          },
          program_name: 'CP2K',
          method: 'DFT Simulation',
          workflow: 'optimization',
        },
        system: {
          m_def: {
            m_def:
              '/metainfo/nomad.plugin.simulation/section_definitions/System',
          },
          formula_hill: 'CO2Cu255',
          atoms: {
            m_def: {
              m_def:
                '/metainfo/nomad.plugin.simulation/section_definitions/Atoms',
            },
            atom_labels: ['C', 'O', 'Cu'],
            atom_positions: [
              [0, 0, 0],
              [0, 0, 1.2],
              [0, 0, 2.4],
            ],
          },
        },
        calculation: [
          {
            m_def: {
              m_def:
                '/metainfo/nomad.plugin.simulation/section_definitions/Calculation',
            },
            energy_total: 1.13423e-23,
            log: {
              m_def: {
                m_def:
                  '/metainfo/nomad.plugin.simulation/section_definitions/Log',
              },
            },
          },
          {
            m_def: {
              m_def:
                '/metainfo/nomad.plugin.simulation/section_definitions/Calculation',
            },
            energy_total: 1.13219e-23,
          },
        ],
      },
    },
  })
  const entry2 = createEntry({
    upload_id,
    mainfile_path: '/experiment/afm/co2_cu110_100nm.spm',
    metadata: {
      ...entryMetadata(),
      entry_name: 'AFM scan',
      entry_type: 'AFM scan',
    },
    complete_time: faker.date.anytime().toISOString(),
    entry_create_time: faker.date.anytime().toISOString(),
    process_status: 'SUCCESS',
    archive: {
      m_def: {
        m_def: '/metainfo/nomad.datamodel/section_definitions/EntryArchive',
      },
      material: {
        m_def: {
          m_def:
            '/metainfo/nomad.datamodel.material_properties/section_definitions/Material',
        },
        formula: 'CO2Cu255',
      },
      data: {
        m_def: {
          m_def: '/metainfo/nomad.plugin.afm/section_definitions/AFM',
        },
        device: {
          m_def: {
            m_def: '/metainfo/nomad.plugin.afm/section_definitions/Device',
          },
          manufacturer: 'Manufacturer X',
          model: 'Model Y',
        },
        system: {
          m_def: {
            m_def:
              '/metainfo/nomad.plugin.simulation/section_definitions/System',
          },
          formula_hill: 'CO2Cu255',
        },
        experiment: {
          m_def: {
            m_def: '/metainfo/nomad.plugin.afm/section_definitions/Experiment',
          },
          scan_mode: 'non-contact',
        },
      },
    },
  })

  return {
    upload_id: upload_id,
    upload_name: 'Atomic Force Microscopy Experiment and Simulation on Cu110',
    published: false,
    process_status: 'SUCCESS',
    complete_time: faker.date.anytime().toISOString(),
    upload_create_time: faker.date.anytime().toISOString(),
    entries: {
      [entry1.entry_id as string]: entry1,
      [entry2.entry_id as string]: entry2,
    },
    files: {
      m_is: 'Directory',
      simulation: {
        m_is: 'Directory',
        cp2k: {
          m_is: 'Directory',
          'co2_probe_tip.out': {
            m_is: 'File',
            size: 591952,
            entry: entry1,
          },
          'co2_probe_tip.in': {
            m_is: 'File',
            size: 4991,
          },
          'reftraj.xyz': {
            m_is: 'File',
            size: 474015,
          },
          BASIS_SET: {
            m_is: 'File',
            size: 126074,
          },
          POTENTIAL: {
            m_is: 'File',
            size: 128376,
          },
        },
      },
      experiment: {
        m_is: 'Directory',
        afm: {
          m_is: 'Directory',
          'co2_cu110_100nm.spm': {
            m_is: 'File',
            size: 2492184,
            entry: entry2,
          },
          'co2_cu110_100nm.tif': {
            m_is: 'File',
            size: 993416,
          },
        },
      },
      'README.md': {
        m_is: 'File',
        size: 256317,
      },
    },
    ...uploadMetadata(),
  }
}

function createQuantity(
  name: string,
  typeOrQuantity: string | JSONObject,
  props: object = {},
) {
  let type
  if (typeof typeOrQuantity === 'string') {
    type = typeOrQuantity
  } else {
    type = typeOrQuantity.type as string
    const {type: _, ...rest} = typeOrQuantity
    Object.assign(props, rest as JSONObject)
  }
  let typeKind
  switch (type) {
    case 'str': {
      typeKind = 'python'
      break
    }
    case 'float': {
      typeKind = 'python'
      break
    }
    case 'File': {
      typeKind = 'custom'
      type = 'nomad.metainfo.data_type.File'
      break
    }
    case 'Datetime': {
      typeKind = 'custom'
      type = 'nomad.metainfo.data_type.Datetime'
      break
    }
    default: {
      typeKind = 'reference'
    }
  }
  return {
    m_def: {
      m_def: '/metainfo/nomad.metainfo/section_definitions/Quantity',
    },
    name,
    type: {type_kind: typeKind, type_data: type},
    ...props,
  }
}

function createSubSection(name: string, typeOrProps: string | object) {
  return {
    m_def: {
      m_def: '/metainfo/nomad.metainfo/section_definitions/SubSection',
    },
    name,
    ...(typeof typeOrProps === 'string'
      ? {sub_section: typeOrProps}
      : typeOrProps),
  }
}

function createSection({
  name,
  baseSections = [],
  quantities = {},
  subSections = {},
  annotations = {},
}: {
  name: string
  baseSections?: string[]
  quantities?: {[name: string]: string | JSONObject}
  subSections?: {[name: string]: string | JSONObject}
  annotations?: {[name: string]: JSONValue}
  repeats?: boolean
}) {
  const section = {
    m_def: {
      m_def: '/metainfo/nomad.metainfo/section_definitions/Section',
    },
    m_annotations: annotations,
    name,
    quantities: Object.entries(quantities).map(([name, quantity]) => {
      return createQuantity(name, quantity)
    }),
    sub_sections: Object.entries(subSections).map(([name, type]) => {
      return createSubSection(name, type)
    }),
    base_sections: baseSections,
    all_base_sections: baseSections,
    all_quantities: {},
    all_sub_sections: {},
    all_properties: {},
  }
  section.all_quantities = Object.fromEntries(
    Object.entries(quantities).map(([name, type]) => [
      name,
      createQuantity(name, type),
    ]),
  )
  section.all_sub_sections = Object.fromEntries(
    Object.entries(subSections).map(([name, type]) => [
      name,
      createSubSection(name, type),
    ]),
  )
  section.all_properties = lodash.merge(
    {},
    section.all_quantities,
    section.all_sub_sections,
  )
  return section
}

faker.seed(0)

const data = {
  uploads: [
    createExampleUpload(),
    ...Array.from(Array(9).keys()).map(() => createUpload(10)),
  ].reduce(
    (current, upload) => ({...current, [upload.upload_id as string]: upload}),
    {},
  ),
  metainfo: {
    'nomad.datamodel': {
      section_definitions: {
        EntryArchive: createSection({
          name: 'EntryArchive',
          quantities: {entry_name: 'str', entry_id: 'str'},
          subSections: {
            metadata:
              '/metainfo/nomad.datamodel/section_definitions/EntryMetadata',
            data: '/metainfo/nomad.datamodel/section_definitions/EntryData',
            material:
              '/metainfo/nomad.datamodel.material_properties/section_definitions/Material',
          },
        }),
        EntryMetadata: createSection({
          name: 'EntryMetadata',
          quantities: {
            entry_name: 'str',
            entry_id: 'str',
            entry_type: 'str',
            references: {type: 'str', shape: '*'},
          },
        }),
        EntryData: createSection({name: 'EntryData'}),
      },
    },
    'nomad.datamodel.material_properties': {
      section_definitions: {
        Material: createSection({
          name: 'Material',
          quantities: {formula: 'str'},
        }),
      },
    },
    'nomad.plugin.afm': {
      section_definitions: {
        AFM: createSection({
          name: 'AFM',
          baseSections: [
            '/metainfo/nomad.datamodel/section_definitions/EntryData',
          ],
          subSections: {
            device: '/metainfo/nomad.plugin.afm/section_definitions/Device',
            system:
              '/metainfo/nomad.plugin.simulation/section_definitions/System',
            experiment:
              '/metainfo/nomad.plugin.afm/section_definitions/Experiment',
          },
        }),
        Device: createSection({
          name: 'Device',
          baseSections: [
            '/metainfo/nomad.datamodel/section_definitions/MSection',
          ],
          quantities: {
            manufacturer: 'str',
            model: 'str',
          },
        }),
        System: createSection({
          name: 'System',
          baseSections: [
            '/metainfo/nomad.plugin.simulation/section_definitions/System',
          ],
          quantities: {formula_hill: 'str'},
        }),
        Experiment: createSection({
          name: 'Experiment',
          baseSections: [
            '/metainfo/nomad.datamodel/section_definitions/MSection',
          ],
          quantities: {scan_mode: 'float'},
        }),
      },
    },
    'nomad.plugin.simulation': {
      section_definitions: {
        Simulation: createSection({
          name: 'Simulation',
          baseSections: [
            '/metainfo/nomad.datamodel/section_definitions/EntryData',
          ],
          quantities: {
            description: 'str',
            aux_files: {type: 'File', shape: ['*']},
          },
          subSections: {
            method:
              '/metainfo/nomad.plugin.simulation/section_definitions/Method',
            system:
              '/metainfo/nomad.plugin.simulation/section_definitions/System',
            calculation: {
              sub_section:
                '/metainfo/nomad.plugin.simulation/section_definitions/Calculation',
              repeats: true,
            },
          },
          annotations: {
            layout: {
              type: 'container',
              children: [
                {
                  type: 'richText',
                  xs: 12,
                  property: 'description',
                },
                {
                  type: 'text',
                  xs: 12,
                  content: 'Simulation input',
                  variant: 'h1',
                },
                {
                  type: 'card',
                  xs: 12,
                  md: 4,
                  title: 'Method',
                  children: [
                    {
                      type: 'subSection',
                      property: 'method',
                      layout: {
                        type: 'container',
                        children: [
                          {
                            type: 'quantity',
                            xs: 12,
                            label: 'summary',
                            property: 'method',
                          },
                          {
                            xs: 12,
                            type: 'quantity',
                            label: 'used code',
                            editable: true,
                            property: 'program_name',
                          },
                          {
                            xs: 12,
                            type: 'quantity',
                            editable: true,
                            property: 'workflow',
                          },
                        ],
                      },
                    },
                  ],
                },
                {
                  xs: 12,
                  md: 8,
                  type: 'subSection',
                  property: 'system',
                  layout: {
                    type: 'card',
                    title: 'System',
                    children: [
                      {
                        type: 'container',
                        xs: 12,
                        md: 6,
                        children: [
                          {
                            type: 'quantity',
                            label: 'formula hill',
                            editable: true,
                            property: 'formula_hill',
                          },
                          {
                            type: 'subSection',
                            property: 'atoms',
                            layout: {
                              type: 'container',
                              children: [
                                {
                                  type: 'quantity',
                                  label: 'atom_labels',
                                  editable: true,
                                  property: 'atom_labels',
                                },
                                {
                                  type: 'quantity',
                                  label: 'atom_positions',
                                  displayUnit: 'angstrom',
                                  property: 'atom_positions',
                                },
                              ],
                            },
                          },
                        ],
                      },
                      {
                        type: 'text',
                        xs: 12,
                        md: 6,
                        content:
                          'Here we could add the system viewer ' +
                          'X '.repeat(500),
                      },
                    ],
                  },
                },
                {
                  type: 'card',
                  xs: 12,
                  title: 'Files',
                  children: [
                    {
                      type: 'quantity',
                      xs: 12,
                      label: 'auxiliary files',
                      property: 'aux_files',
                      editable: true,
                      component: {
                        FileReference: {
                          filePattern: '.*\\.xyz',
                        },
                      },
                    },
                  ],
                },
                {
                  type: 'text',
                  xs: 12,
                  md: 12,
                  content: 'Simulation output',
                  variant: 'h1',
                },
                {
                  type: 'subSection',
                  property: 'calculation',
                  xs: 12,
                  layout: {
                    type: 'card',
                    title: 'Calculation',
                    xs: 12,
                    md: 6,
                    children: [
                      {
                        type: 'quantity',
                        xs: 12,
                        label: 'method reference',
                        property: 'method',
                        editable: true,
                      },
                      {
                        type: 'quantity',
                        xs: 12,
                        label: 'total energy',
                        property: 'energy_total',
                        displayUnit: 'eV',
                        editable: true,
                        component: {
                          Numeral: {
                            options: {
                              notation: 'scientific',
                            },
                          },
                        },
                      },
                      {
                        type: 'subSection',
                        property: 'log',
                        layout: {
                          type: 'container',
                          children: [
                            {
                              type: 'quantity',
                              property: 'entry',
                            },
                            {
                              type: 'quantity',
                              property: 'datetime',
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        }),
        Method: createSection({
          name: 'Method',
          baseSections: [
            '/metainfo/nomad.datamodel/section_definitions/MSection',
          ],
          quantities: {program_name: 'str', workflow: 'str', method: 'str'},
        }),
        System: createSection({
          name: 'System',
          baseSections: [
            '/metainfo/nomad.datamodel/section_definitions/MSection',
          ],
          quantities: {formula_hill: 'str'},
          subSections: {
            atoms: {
              sub_section:
                '/metainfo/nomad.plugin.simulation/section_definitions/Atoms',
            },
          },
        }),
        Atoms: createSection({
          name: 'Atoms',
          baseSections: [
            '/metainfo/nomad.datamodel/section_definitions/MSection',
          ],
          quantities: {
            atom_labels: {type: 'str', shape: ['*']},
            atom_positions: {type: 'float', shape: ['*', 3], unit: 'm'},
            lattice_vectors: {type: 'float', shape: [3, 3], unit: 'm'},
          },
        }),
        Calculation: createSection({
          name: 'Calculation',
          baseSections: [
            '/metainfo/nomad.datamodel/section_definitions/MSection',
          ],
          quantities: {
            energy_total: {type: 'float', unit: 'J'},
            method:
              '/metainfo/nomad.plugin.simulation/section_definitions/Method',
          },
          subSections: {
            log: '/metainfo/nomad.plugin.simulation/section_definitions/Log',
          },
        }),
        Log: createSection({
          name: 'Result',
          quantities: {
            entry: 'str',
            datetime: 'Datetime',
          },
        }),
      },
    },
  },
}

fs.writeFileSync(
  path.join(
    path.dirname(fileURLToPath(import.meta.url)),
    'syntheticApi.data.json',
  ),
  JSON.stringify(data, null, 2),
)
