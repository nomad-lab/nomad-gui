import {http} from 'msw'
import {setupWorker} from 'msw/browser'

import {EntryEdit} from '../models/entryEditRequestModels'
import {GraphRequest} from '../models/graphRequestModels'
import {
  EntryResponse,
  GraphResponse,
  UploadResponse,
} from '../models/graphResponseModels'
import {callSyntheticApi, callSyntheticEditApi, getData} from './syntheticApi'

// This configures a request mocking in the browser (=for e2e tests and for
// local development without the actual API). By default, a response is
// auto-generated on the fly.
const handlers = [
  http.post(
    `${import.meta.env.VITE_MOCKED_API_URL}/graph/query`,
    async ({request}) => {
      await new Promise((resolve) =>
        // eslint-disable-next-line no-undef
        setTimeout(
          resolve,
          import.meta.env.VITE_USE_MOCKED_API_LATENCY_MS || 0,
        ),
      )
      const body = await request.json()

      const response = callSyntheticApi(body as GraphRequest)

      return new Response(JSON.stringify(response), {
        headers: {
          'Content-Type': 'application/json',
        },
      })
    },
  ),
  http.post(
    `${import.meta.env.VITE_MOCKED_API_URL}/entries/:entryId/edit`,
    async ({request}) => {
      const regex = /.*\/entries\/([^/]+)\/edit$/
      const matches = request.url.match(regex)

      if (!matches) {
        return new Response('Does not exist', {status: 404})
      }

      const entryId = matches[1]

      await new Promise((resolve) =>
        // eslint-disable-next-line no-undef
        setTimeout(
          resolve,
          import.meta.env.VITE_USE_MOCKED_API_LATENCY_MS || 0,
        ),
      )
      const body = await request.json()

      try {
        const response = callSyntheticEditApi(entryId, body as EntryEdit)
        return new Response(JSON.stringify(response), {
          headers: {
            'Content-Type': 'application/json',
          },
        })
      } catch (e) {
        return new Response((e as Error).message, {
          status: 400,
        })
      }
    },
  ),
  http.get(
    `${import.meta.env.VITE_MOCKED_API_URL}/uploads/:uploadId/raw/*`,
    async ({request}) => {
      const regex = /.*\/uploads\/([^/]+)\/raw\/(.*)$/
      const matches = request.url.match(regex)

      if (!matches) {
        return new Response('Does not exist', {status: 404})
      }

      const sampleRes = async (path: string) => {
        // eslint-disable-next-line no-undef
        const buffer = await fetch(path).then((response) =>
          response.arrayBuffer(),
        )
        return new Response(buffer)
      }

      const remainingPath = matches[2]
      const uploadId = matches[1]
      const prefix =
        import.meta.env.MODE === 'production' ? import.meta.env.BASE_URL : ''
      const data = getData() as GraphResponse
      const upload = data.uploads?.[uploadId] as UploadResponse | undefined
      const entries = upload?.entries || {}
      const entryId = Object.keys(entries).find((entryId) => {
        const entry = entries[entryId] as EntryResponse
        if (entry?.mainfile_path === remainingPath) {
          return true
        }
      })
      const entry = entryId ? (entries[entryId] as EntryResponse) : undefined
      if (entry) {
        return new Response(JSON.stringify({data: entry.archive?.data}), {
          headers: {
            'Content-Type': 'application/json',
          },
        })
      } else if (remainingPath.endsWith('.json')) {
        return sampleRes(`${prefix}/sample.json`)
      } else if (remainingPath.endsWith('.pdf')) {
        return sampleRes(`${prefix}/sample.pdf`)
      }
      return sampleRes(`${prefix}/sample.txt`)
    },
  ),
]
export const httpWorker = setupWorker(...handlers)
