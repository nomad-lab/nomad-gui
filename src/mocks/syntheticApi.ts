import lodash from 'lodash'

import {getIndexedKey} from '../components/routing/loader'
import {EntryEdit} from '../models/entryEditRequestModels'
import {EntryEditResponse} from '../models/entryEditResponseModels'
import {
  DefinitionType,
  DirectiveType,
  GraphRequest,
} from '../models/graphRequestModels'
import {
  EntryResponse,
  GraphResponse,
  Error as MError,
  MSectionResponse,
} from '../models/graphResponseModels'
import {assert} from '../utils/utils'
import {JSONObject, JSONValue, PageBasedPagination} from './../utils/types'
import * as syntheticApiDataNomad from './syntheticApi.nomad.data.json'
import * as syntheticApiDataRandom from './syntheticApi.random.data.json'

let data: JSONObject | undefined

export const getData = () => {
  if (data === undefined) {
    const mockedApiData = import.meta.env.VITE_USE_MOCKED_API_DATA as
      | 'nomad'
      | 'random'
    const pkgData = (mockedApiData === 'nomad'
      ? syntheticApiDataNomad
      : syntheticApiDataRandom) as unknown as {
      metainfo: {[k: string]: JSONObject}
    }
    for (const pkgName of Object.keys(pkgData.metainfo)) {
      if (pkgName.startsWith('m_')) {
        continue
      }
      const pkg = pkgData.metainfo[pkgName]
      if (Array.isArray(pkg.section_definitions)) {
        const sectionDefs = {} as {[k: string]: JSONObject}
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        pkg.section_definitions.forEach((section: any, index: number) => {
          if (section === null) {
            return
          }
          sectionDefs[`${index}`] = section
        })
        pkg.section_definitions = sectionDefs
      }
    }
    data = pkgData as JSONObject
  }
  return data
}

function getEffectivePagination(pagination: PageBasedPagination): {
  page: number
  page_size: number
} {
  const page = pagination?.page
  const page_size = pagination?.page_size
  let effectivePage
  if (typeof page === 'string') {
    effectivePage = parseInt(page)
  } else if (typeof page === 'number') {
    effectivePage = page
  } else {
    effectivePage = 1
  }
  let effectivePageSize
  if (typeof page_size === 'string') {
    effectivePageSize = parseInt(page_size)
  } else if (typeof page_size === 'number') {
    effectivePageSize = page_size
  } else {
    effectivePageSize = 10
  }
  return {
    page: effectivePage,
    page_size: effectivePageSize,
  }
}

type Context = {
  references: string[]
  directive?: DirectiveType
  depth: number
  include_definition?: DefinitionType
}
type GetResponseArgs<D> = {
  request: JSONObject | '*'
  data: D
  context: Context
}
type JSONCollection = {[key: string]: JSONObject}

function isDataKey(key: string) {
  if (key === '*') {
    return false
  }

  return (
    !key.startsWith('m_') || ['m_annotation', 'm_def', 'm_is'].includes(key)
  )
}

function dataKeys(request: JSONObject) {
  return Object.keys(request).filter(isDataKey)
}

function getQueriedKeys(
  data: JSONCollection,
  query: JSONObject = {},
  pagination: PageBasedPagination = {},
  sort?: (a: string, b: string) => number,
): [string[], JSONObject] {
  const order = pagination?.order
  const order_by = pagination?.order_by
  const effectivePagination = getEffectivePagination(pagination)

  // apply the query
  let queriedKeys = Object.keys(data).filter(
    (key) =>
      !key.startsWith('m_') &&
      Object.keys(query).every((requestKey) => {
        let queryValue = query[requestKey]
        if (queryValue === undefined) {
          return true
        }
        if (!Array.isArray(queryValue)) {
          queryValue = [queryValue]
        }
        return (
          (queryValue as JSONValue[]).indexOf(
            (data[key] as JSONObject)[requestKey],
          ) >= 0
        )
      }),
  )
  const total = queriedKeys.length

  // sort
  if (sort) {
    queriedKeys = queriedKeys.sort(sort)
  } else if (
    typeof order_by === 'string' &&
    typeof order === 'string' &&
    ['asc', 'desc'].includes(order)
  ) {
    queriedKeys = queriedKeys.sort((a_key, b_key) => {
      const a_object = data[a_key] as JSONObject
      const b_object = data[b_key] as JSONObject
      const order_num = pagination.order === 'asc' ? 1 : -1
      const a = a_object[pagination.order_by as string]
      const b = b_object[pagination.order_by as string]
      if (a && b) {
        if (typeof a === 'number' && typeof b === 'number') {
          return (a - b) * order_num
        } else if (typeof a === 'string' && typeof b === 'string') {
          return a.localeCompare(b) * order_num
        } else return 1
      } else return 1
    })
  }

  // paginate
  queriedKeys = queriedKeys.slice(
    effectivePagination.page_size * (effectivePagination.page - 1),
    effectivePagination.page_size * effectivePagination.page,
  )

  // create the response
  const m_response = {
    pagination: {
      ...effectivePagination,
      total: total,
      ...(typeof order === 'string' ? {order} : {}),
      ...(typeof order_by === 'string' ? {order_by} : {}),
    },
    query,
  }
  return [queriedKeys, m_response]
}

function getCollectionResponse(
  {request, data, context}: GetResponseArgs<JSONCollection>,
  resolve: (args: GetResponseArgs<JSONObject>) => JSONObject,
  sort?: (a: string, b: string) => number,
): JSONObject {
  if (request === '*') {
    request = {'*': '*'}
  }

  const errors: MError[] = []
  const response: JSONObject = {}

  // potential query
  if (request['*'] !== undefined) {
    const m_request = (request?.m_request || {}) as JSONObject
    const [queriedKeys, m_response] = getQueriedKeys(
      data,
      m_request.query as JSONObject,
      m_request.pagination as PageBasedPagination,
      sort,
    )
    for (const key of queriedKeys) {
      response[key] = resolve({
        request: request['*'] as JSONObject | '*',
        data: data[key],
        context,
      })
      response.m_response = m_response
    }
  }

  // specific keys
  for (const key of dataKeys(request)) {
    if (data[key] === undefined) {
      errors.push({
        message: `The entity with id ${key} does not exist.`,
        error_type: 'DoesNotExist',
      })
      continue
    }

    response[key] = resolve({
      request: request[key] as JSONObject | '*',
      data: data[key],
      context,
    })
  }

  // special keys
  for (const key of Object.keys(data).filter((key) => key.startsWith('m_'))) {
    response[key] = data[key]
  }

  if (errors.length > 0) {
    response.m_errors = errors as unknown as JSONValue
  }

  return response
}

function getObjectResponse(
  {request, data, context}: GetResponseArgs<JSONObject>,
  resolve: (
    key: string,
    args: GetResponseArgs<JSONValue>,
  ) => JSONValue | undefined,
): JSONObject {
  const response: JSONObject = {}
  if (request === '*' && Array.isArray(data)) {
    return data
  }
  if (request === '*') {
    request = {'*': '*'}
  }
  const requestKeys = dataKeys(request)
  const isQuery =
    request['*'] ||
    requestKeys.length === 0 ||
    context.directive !== undefined ||
    (requestKeys.length === 1 && requestKeys[0] === 'm_def')
  const keys = [
    ...new Set([
      ...requestKeys.filter((key) => key !== '*'),
      ...(isQuery ? Object.keys(data) : []),
    ]),
  ]
  const m_request = (request?.m_request || {}) as JSONObject

  const next = (key: string, args: GetResponseArgs<JSONValue>) => {
    const response = resolve(key, args)
    if (response === undefined) {
      if (lodash.isObject(args.data)) {
        return getObjectResponse(
          args as GetResponseArgs<JSONObject>,
          () => undefined,
        )
      }
      return args.data
    }
    return response
  }

  for (const fullKey of keys) {
    const [key, index] = getIndexedKey(fullKey)

    const fullValue = data[key]
    if (fullValue === undefined) {
      continue
    }

    if (index !== undefined && !Array.isArray(fullValue)) {
      throw new Error('You cannot use an key with index for non array values.')
    }

    const value =
      index !== undefined ? (fullValue as JSONValue[])[index] : fullValue
    const valueRequest = (request[fullKey] || '*') as JSONObject | '*'
    let responseValue: JSONValue | undefined
    const nextContext = {
      ...context,
      depth: (m_request.depth as number) || context.depth - 1,
    }
    if (m_request.directive) {
      nextContext.directive = m_request.directive as DirectiveType
    }

    if (index === undefined && Array.isArray(fullValue)) {
      responseValue = fullValue.map((value) =>
        next(key, {
          request: valueRequest,
          data: value as JSONObject,
          context: nextContext,
        }),
      )
    } else {
      responseValue = next(key, {
        request: valueRequest,
        data: value as JSONObject,
        context: nextContext,
      })
    }

    if (responseValue === undefined) {
      if (valueRequest !== '*') {
        throw new Error(`Complex request is not supported for ${fullKey}.`)
      }
      responseValue = value
    }

    if (index !== undefined) {
      if (response[key] === undefined) {
        response[key] = []
      }
      const fullResponse = response[key] as JSONValue[]
      fullResponse[index] = responseValue
      for (let index = 0; index < fullResponse.length; index++) {
        if (fullResponse[index] === undefined) {
          fullResponse[index] = null
        }
      }
    } else {
      response[key] = responseValue
    }
  }

  return response
}

function getArchiveResponse({
  request,
  data,
  context,
}: GetResponseArgs<JSONObject>): JSONValue {
  return getObjectResponse({request, data, context}, (key, args) => {
    if (lodash.isObject(args.data)) {
      if (args.context.depth === 0 && key !== 'm_def') {
        return '__internal__:*'
      }
      return getArchiveResponse(args as GetResponseArgs<JSONObject>)
    } else {
      if (key === 'm_def') {
        context.references.push(args.data as string)
      }
    }
  })
}

function getMetainfoResponse(args: GetResponseArgs<JSONObject>) {
  return getObjectResponse(args, () => undefined)
}

function getFilesResponse(args: GetResponseArgs<JSONCollection>): JSONObject {
  return getCollectionResponse(
    args,
    ({request, data, context}) => {
      if (data.m_is === 'File') {
        return getObjectResponse({request, data, context}, (key, args) => {
          if (key === 'entry') {
            return getEntryResponse(args as GetResponseArgs<JSONObject>)
          }
          return args.data
        })
      } else {
        if (request === '*') {
          // End the recursion, emulate depth: 1
          return {
            m_is: 'Directory',
          }
        }
        return getFilesResponse({
          request,
          data: data as JSONCollection,
          context,
        })
      }
    },
    (a, b) => {
      const data = args.data
      if (data[a].m_is === data[b].m_is) {
        return a.localeCompare(b)
      }
      if (data[a].m_is === 'File') {
        return 1
      } else {
        return -1
      }
    },
  )
}

function getEntryResponse(args: GetResponseArgs<JSONObject>): JSONObject {
  return getObjectResponse(args, (key, args) => {
    if (key === 'archive') {
      return getArchiveResponse(args as GetResponseArgs<JSONObject>)
    }
  })
}

export function callSyntheticApi(
  request: GraphRequest,
  data?: JSONObject,
): GraphResponse {
  const usedData = data || getData()
  const references: string[] = []
  const response = getObjectResponse(
    {
      request: request as JSONObject,
      data: usedData,
      context: {references, depth: -1},
    },
    (key, args) => {
      if (key === 'uploads') {
        return getCollectionResponse(
          args as GetResponseArgs<JSONCollection>,
          (args) =>
            getObjectResponse(args, (key, args) => {
              if (key === 'entries') {
                if (args.request === '*') {
                  return '__internal__:*'
                }
                return getCollectionResponse(
                  args as GetResponseArgs<JSONCollection>,
                  getEntryResponse,
                )
              }
              if (key === 'files') {
                return getFilesResponse(args as GetResponseArgs<JSONCollection>)
              }
            }),
        )
      }
    },
  )

  for (const reference of references) {
    const referenceRequest = reference
      .split('/')
      .filter((segment) => segment !== '')
      .reverse()
      .reduce((current: JSONObject | undefined, next) => {
        return {
          [next]: current || '*',
        }
      }, undefined) as JSONObject
    lodash.merge(
      response,
      getMetainfoResponse({
        request: referenceRequest,
        data: usedData,
        context: {references, depth: -1},
      }),
    )
  }
  return response
}

export function callSyntheticEditApi(
  entryId: string,
  request: EntryEdit,
  data?: JSONObject,
) {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const usedData: any = data || getData()

  function getEntry() {
    for (const upload of Object.keys(usedData.uploads)) {
      if (upload.startsWith('m_')) {
        continue
      }
      for (const entry of Object.keys(usedData.uploads[upload].entries)) {
        if (entry === entryId) {
          return usedData.uploads[upload].entries[entry] as EntryResponse
        }
      }
    }
  }

  const entry = getEntry()

  if (!entry) {
    throw new Error('Entry not found')
  }

  const archive = entry.archive as MSectionResponse

  function toKey(key: string) {
    return isNaN(parseInt(key)) ? key : parseInt(key)
  }

  for (const change of request.changes) {
    const pathSegments = change.path.split('/')
    let sectionData = archive
    let nextKey: string | number = ''

    for (let pathIndex = 0; pathIndex < pathSegments.length - 1; pathIndex++) {
      const pathSegment = pathSegments[pathIndex]
      nextKey = toKey(pathSegments[pathIndex + 1])
      const key = toKey(pathSegment)
      const isRepeatedSubSection = typeof nextKey === 'number'

      const nextValue = isRepeatedSubSection ? [] : {}

      if (Array.isArray(sectionData)) {
        assert(typeof key === 'number')
        if (sectionData[key] === null) {
          sectionData[key] = nextValue
        }
        sectionData = sectionData[key]
      } else {
        sectionData = (sectionData[key] || {}) as MSectionResponse
      }

      if (Array.isArray(sectionData)) {
        assert(typeof nextKey === 'number')
        if (sectionData.length <= nextKey) {
          sectionData.push(null)
        }
      }
    }

    if (change.action === 'remove') {
      assert(Array.isArray(sectionData))
      assert(typeof nextKey === 'number')
      sectionData.splice(nextKey, 1)
    } else {
      sectionData[nextKey] = change.new_value
    }
  }

  return {} as EntryEditResponse
}
