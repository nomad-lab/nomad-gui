import {vi} from 'vitest'

import {
  DirectoryRequest,
  GraphRequest,
  MSectionRequest,
  UploadsRequest,
} from '../models/graphRequestModels'
import {
  DirectoryResponse,
  EntryResponse,
  FileResponse,
  GraphResponse,
  MSectionResponse,
  UploadResponse,
  UploadsResponse,
} from '../models/graphResponseModels'
import {archiveRequest} from '../pages/entry/entryRoute'
import {JSONObject} from '../utils/types'
import {callSyntheticApi as actualCallSyntheticApi} from './syntheticApi'

const getApiData = vi.fn()
const callSyntheticApi = (request: GraphRequest) =>
  actualCallSyntheticApi(request, getApiData())

describe('collections', () => {
  const apiData = {
    uploads: {
      p0: {
        upload_name: 'value',
        entries: {
          e0: {},
        },
      },
      p1: {},
      p2: {},
      p3: {},
      p4: {},
      p5: {},
      p6: {},
      p7: {},
      p8: {},
      p9: {},
    },
  }
  Object.keys(apiData.uploads).forEach((key) => {
    const upload = apiData.uploads[
      key as keyof typeof apiData.uploads
    ] as JSONObject
    upload.upload_id = key
  })

  function api(request: UploadsRequest) {
    const response = callSyntheticApi({uploads: request})
    return response.uploads as UploadsResponse
  }

  beforeEach(() => getApiData.mockReturnValue(apiData))

  it('queries', () => {
    const response = api({'*': '*'})
    expect(response).toBeTruthy()
    expect(response).toHaveProperty('m_response')
    expect(response).toHaveProperty('p1')
  })

  it('queries without conditions', () => {
    const response = api({
      '*': {upload_name: '*'},
      m_request: {query: {}},
    })
    expect(response).toBeTruthy()
    expect(response).toHaveProperty('m_response')
    expect(response).toHaveProperty('p1')
  })

  it('queries with conditions', () => {
    const response = api({
      m_request: {
        query: {
          upload_name: ['value'],
        },
      },
      '*': '*',
    })
    expect(response.m_response?.pagination).toMatchObject({
      total: 1,
    })
    expect(response).toHaveProperty('p0')
    expect(response).not.toHaveProperty('p1')
  })

  it('paginatates with defaults', () => {
    const response = api({
      '*': {upload_name: '*'},
      m_request: {pagination: {page: 1, page_size: 10}},
    })
    expect(response).toBeTruthy()
    expect(response.m_response?.pagination?.total).toBe(10)
    expect(
      Object.keys(response).filter((key) => key !== 'm_response').length,
    ).toBe(10)
    expect(response).toHaveProperty('m_response')
    expect(response).toHaveProperty('p1')
  })

  it('paginates', () => {
    const response = api({
      m_request: {
        pagination: {
          page: 2,
          page_size: 2,
        },
      },
      '*': '*',
    })
    expect(response).toBeTruthy()
    expect(response).toHaveProperty('m_response')
    expect(response.m_response?.pagination).toMatchObject({
      page: 2,
      page_size: 2,
      total: 10,
    })
    expect(response).not.toHaveProperty('p1')
    expect(response).toHaveProperty('p2')
    expect(response).toHaveProperty('p3')
    expect(response).not.toHaveProperty('p4')
  })

  it('orders', () => {
    const response = api({
      m_request: {
        pagination: {
          page: 1,
          page_size: 1,
          order_by: 'upload_id',
          order: 'desc',
        },
      },
      '*': '*',
    })
    expect(response).toBeTruthy()
    expect(response).not.toHaveProperty('p0')
    expect(response).not.toHaveProperty('p8')
    expect(response).toHaveProperty('p9')
  })

  it('gets', () => {
    const response = api({
      p0: '*',
    })
    expect(response).not.toHaveProperty('p1')
    expect(response.p0).toHaveProperty('upload_name')
    expect(response.p0).toHaveProperty('entries')
  })

  it('gets with error', () => {
    const response = api({
      doesNotExist: '*',
    })
    expect(response).not.toHaveProperty('p1')
    expect(response).toHaveProperty('m_errors')
    expect(response.m_errors).toEqual([
      {
        error_type: 'DoesNotExist',
        message: 'The entity with id doesNotExist does not exist.',
      },
    ])
  })

  it('does not resolve unrequested properties', () => {
    const response = api({
      p0: '*',
    })
    expect(response.p0).toHaveProperty('entries')
    expect((response.p0 as JSONObject)?.entries).toBe('__internal__:*')
  })

  it('does not get unrequest parent properties', () => {
    const response = api({
      p0: {entries: {'*': '*'}},
    })
    expect(response).not.toHaveProperty('p1')
    expect(response.p0).not.toHaveProperty('upload_name')
    expect(response.p0).toHaveProperty('entries')
  })
})

describe('files', () => {
  const apiData = {
    uploads: {
      p0: {
        files: {
          m_is: 'Directory',
          f0: {
            m_is: 'File',
            size: 100,
            entry: {
              entry_id: 'e0',
            },
          },
          d0: {
            m_is: 'Directory',
            f00: {
              m_is: 'File',
              size: 100,
            },
          },
          f1: {
            m_is: 'File',
            size: 100,
          },
          f2: {
            m_is: 'File',
            size: 100,
          },
          f3: {
            m_is: 'File',
            size: 100,
          },
        },
      },
    },
  }

  function api(request: DirectoryRequest) {
    const response = callSyntheticApi({
      uploads: {
        p0: {files: request},
      },
    })
    const files = (response.uploads?.p0 as UploadResponse).files
    expect(files).toBeDefined()
    return files as DirectoryResponse
  }

  beforeEach(() => getApiData.mockReturnValue(apiData))

  it('queries', () => {
    const response = api({
      m_request: {pagination: {page_size: 2}},
      '*': '*',
    })
    expect(response.m_response?.pagination).toMatchObject({
      page: 1,
      total: 5,
      page_size: 2,
    })
    expect(response).toHaveProperty('m_response')
    expect(response).toHaveProperty('m_is')
    const keys = Object.keys(response).filter((key) => !key.startsWith('m_'))
    expect(keys).toEqual(['d0', 'f0'])
  })

  it('resolves entries', () => {
    const response = api({
      '*': '*',
    })
    expect(response.f0).toHaveProperty('entry')
    expect((response.f0 as FileResponse).entry).toMatchObject({entry_id: 'e0'})
  })

  it('queries with depth 1', () => {
    const response = api({
      '*': '*',
    })
    expect(response.d0).toMatchObject({m_is: 'Directory'})
    expect(response.f1).toMatchObject({m_is: 'File', size: 100})
  })

  it('gets', () => {
    const response = api({
      f1: '*',
    })
    expect(response.f1).toMatchObject({m_is: 'File', size: 100})
    expect(Object.keys(response)).toEqual(['f1', 'm_is'])
  })

  it('gets with error', () => {
    const response = api({
      doesNotExist: '*',
    })
    expect(response).toHaveProperty('m_errors')
    expect(Object.keys(response).toSorted()).toEqual(['m_errors', 'm_is'])
  })
})

describe('archive', () => {
  const m_def = {
    m_def: 'metainfo/package/section_definitions/Section',
  }

  const sub_section = {
    m_def,
    quantity: 'value',
    sub_section: {
      m_def,
      quantity: 'value',
      sub_section: {
        m_def,
      },
    },
  }

  beforeEach(() =>
    getApiData.mockReturnValue({
      uploads: {
        p0: {
          entries: {
            e0: {
              archive: {
                m_def,
                quantity: 'value',
                matrix: [
                  [1, 2],
                  [3, 4],
                ],
                sub_section,
                repeated_sub_section: [sub_section, sub_section],
              },
            },
          },
        },
      },
      metainfo: {
        package: {
          section_definitions: {
            Section: {
              name: 'Section',
            },
          },
        },
      },
    }),
  )

  function api(
    request: MSectionRequest | '*',
  ): [MSectionResponse, GraphResponse] {
    const response = callSyntheticApi({
      uploads: {
        p0: {
          entries: {
            e0: {
              archive: request as MSectionRequest,
            },
          },
        },
      },
    })
    const upload = response.uploads?.p0 as UploadResponse
    const entry = upload.entries?.e0 as EntryResponse
    const archive = entry.archive
    expect(archive).toBeDefined()
    return [archive as MSectionResponse, response]
  }

  it('gets quantity', () => {
    const [response] = api({quantity: '*'})
    expect(response).toMatchObject({quantity: 'value'})
  })

  it('gets matrix', () => {
    const [response] = api({matrix: '*'})
    expect(response).toMatchObject({
      matrix: [
        [1, 2],
        [3, 4],
      ],
    })
  })

  it('gets sub section', () => {
    const [response] = api({sub_section: {quantity: '*'}})
    expect(response).toMatchObject({sub_section: {quantity: 'value'}})
  })

  it('gets repeated sub sections', () => {
    const [response] = api({repeated_sub_section: {quantity: '*'}})
    expect(response).toMatchObject({
      repeated_sub_section: [{quantity: 'value'}, {quantity: 'value'}],
    })
  })

  it('gets repeated sub sections with indexed key', () => {
    const [response] = api({'repeated_sub_section[0]': {quantity: '*'}})
    expect(response).toMatchObject({
      repeated_sub_section: [{quantity: 'value'}],
    })
  })

  it('gets repeated sub sections with offset indexed key', () => {
    const [response] = api({'repeated_sub_section[1]': {quantity: '*'}})
    expect(response).toMatchObject({
      repeated_sub_section: [null, {quantity: 'value'}],
    })
  })

  it('queries with depth 1', () => {
    const [response] = api({m_request: {depth: 1}})
    expect(response.sub_section).toMatchObject({
      m_def: {
        m_def: 'metainfo/package/section_definitions/Section',
      },
      quantity: 'value',
      sub_section: '__internal__:*',
    })
  })

  it.each([
    ['direct', {m_request: {depth: 2}}],
    ['indirect', {sub_section: {m_request: {depth: 1}}}],
  ])('gets with variable depth (%s)', (description, request) => {
    const [response] = api(request)
    expect(response.sub_section).toMatchObject({
      m_def,
      quantity: 'value',
      sub_section: {
        m_def,
        quantity: 'value',
        sub_section: '__internal__:*',
      },
    })
  })

  it('gets and queries', () => {
    const [response] = api({
      m_request: {depth: 1},
      '*': '*',
      sub_section: {m_request: {depth: 1}},
    })
    expect(response).toMatchObject({
      m_def,
      quantity: 'value',
      sub_section: {
        m_def: {
          m_def: 'metainfo/package/section_definitions/Section',
        },
        quantity: 'value',
        sub_section: {
          m_def,
          quantity: 'value',
          sub_section: '__internal__:*',
        },
      },
      repeated_sub_section: [
        {m_def, quantity: 'value', sub_section: '__internal__:*'},
        {m_def, quantity: 'value', sub_section: '__internal__:*'},
      ],
    })
  })

  it.each([
    ['*'],
    [{'*': '*'}], // this is currently not supported by the API
    [{}],
    [{m_request: {}}],
  ])('queries with %s', (request) => {
    const [response] = api(request as MSectionRequest)
    expect(response).toHaveProperty('m_def')
    expect(response).toHaveProperty('quantity')
    expect(response).toHaveProperty('sub_section')
  })

  it('resolves and includes m_def referneces', () => {
    const [archive, response] = api(archiveRequest)
    expect(archive).toHaveProperty('m_def')
    expect(archive.m_def?.m_def).toBe(
      'metainfo/package/section_definitions/Section',
    )
    expect(response).toHaveProperty('metainfo')
    expect(response.metainfo).toEqual({
      package: {
        section_definitions: {
          Section: {
            name: 'Section',
          },
        },
      },
    })
  })
})
