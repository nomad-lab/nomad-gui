import {expect, test as setup} from '@playwright/test'
import path from 'path'

const authFile = path.join(__dirname, '../playwright/.auth/user.json')

// see https://playwright.dev/docs/auth
setup('authenticate', async ({page}) => {
  await page.goto('http://localhost:3000/')
  await page.getByRole('button', {name: 'Login'}).click()
  await page.getByLabel('Username or email').fill('test')
  await page.getByLabel('Password', {exact: true}).fill('password')
  await page.getByRole('button', {name: 'Sign In'}).click()
  await expect(page.getByRole('button', {name: 'Profile'})).toBeVisible()

  await page.context().storageState({path: authFile})
})
