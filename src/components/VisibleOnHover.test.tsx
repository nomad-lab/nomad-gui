import {act, fireEvent, render, screen} from '@testing-library/react'
import {vi} from 'vitest'

import VisibleOnHover from './VisibleOnHover'

describe('VisibleOnHover', () => {
  it.each([
    ['can hover', true, false],
    ['cannot hover', false, false],
    ['can hover, but disabled', true, true],
  ])(
    'renders visible on hover with devices that %s',
    async (description, canHover, disabled) => {
      vi.spyOn(window, 'matchMedia').mockImplementation(
        () =>
          ({
            matches: canHover,
            addEventListener: () => {},
            removeEventListener: () => {},
          } as unknown as MediaQueryList),
      )

      render(
        <VisibleOnHover container>
          <VisibleOnHover disabled={disabled}>visible-on-hover</VisibleOnHover>
          container
        </VisibleOnHover>,
      )

      const container = screen.getByText('container')
      if (canHover && !disabled) {
        expect(screen.getByText('visible-on-hover')).not.toBeVisible()
      } else {
        expect(screen.getByText('visible-on-hover')).toBeVisible()
      }
      act(() => fireEvent.mouseEnter(container))
      expect(screen.getByText('visible-on-hover')).toBeVisible()
      act(() => fireEvent.mouseLeave(container))
      if (canHover && !disabled) {
        expect(screen.getByText('visible-on-hover')).not.toBeVisible()
      } else {
        expect(screen.getByText('visible-on-hover')).toBeVisible()
      }
    },
  )

  it('renders visible without container', () => {
    render(<VisibleOnHover>visible</VisibleOnHover>)
    expect(screen.getByText('visible')).toBeVisible()
  })
})
