import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {vi} from 'vitest'

import Numeral from '../values/primitives/Numeral'
import Text from '../values/primitives/Text'
import RichTableEditor from './RichTableEditor'
import {cell} from './RichTableEditor.helper'

describe('RichTableEditor', () => {
  const columns = [
    {
      key: 'foo',
      component: Text,
      props: {
        editable: true,
      },
    },
    {
      key: 'bar',
      component: Text,
      props: {
        editable: true,
      },
    },
    {
      key: 'numeric',
      component: Numeral,
      props: {
        unit: 'm',
        editable: true,
      },
    },
    {
      key: 'noProps',
      component: Text,
    },
  ]
  const data = [
    {
      foo: 'one',
      bar: 'two',
      numeric: 1,
      noProps: 'noProps',
    },
    {
      foo: 'three',
      bar: 'four',
      numeric: 0,
      noProps: 'noProps',
    },
  ]

  it.each([[true], [false]])(
    'renders table cells with editable %s',
    (editable) => {
      render(
        <RichTableEditor
          columns={columns.map((column) => ({
            ...column,
            props: {
              ...column.props,
              editable,
            },
          }))}
          data={data}
        />,
      )

      expect(screen.getByRole('table')).toBeInTheDocument()
      expect(screen.getByText('foo')).toBeInTheDocument()

      if (editable) {
        expect(screen.getByDisplayValue('one')).toBeInTheDocument()
      } else {
        expect(screen.getByText('one')).toBeInTheDocument()
      }
    },
  )

  it('allows to navigate the table', async () => {
    render(<RichTableEditor columns={columns} data={data} />)

    await userEvent.click(cell('one'))
    expect(cell('one')).toHaveFocus()
    expect(cell('two')).not.toHaveFocus()
    await userEvent.keyboard('{arrowright}')
    expect(cell('one')).not.toHaveFocus()
    expect(cell('two')).toHaveFocus()
    expect(cell('four')).not.toHaveFocus()
    await userEvent.keyboard('{arrowdown}')
    expect(cell('two')).not.toHaveFocus()
    expect(cell('three')).not.toHaveFocus()
    expect(cell('four')).toHaveFocus()
    await userEvent.keyboard('{arrowleft}')
    expect(cell('one')).not.toHaveFocus()
    expect(cell('three')).toHaveFocus()
    expect(cell('four')).not.toHaveFocus()
    await userEvent.keyboard('{arrowup}')
    expect(cell('one')).toHaveFocus()
    expect(cell('two')).not.toHaveFocus()
    expect(cell('three')).not.toHaveFocus()
    await userEvent.keyboard('{tab}')
    expect(cell('one')).not.toHaveFocus()
    expect(cell('two')).toHaveFocus()
  })

  it.each([['enter'], ['escape']])(
    'allows to edit cells with enter and exiting with %s',
    async (button) => {
      const onChange = vi.fn()
      const onAbort = vi.fn()
      render(
        <RichTableEditor
          columns={columns}
          data={data}
          onChange={onChange}
          onAbort={onAbort}
        />,
      )

      await userEvent.click(cell('one'))
      await userEvent.keyboard(`{enter}changed{${button}}`)
      expect(cell('changed')).toHaveFocus()
      expect(onChange).toHaveBeenCalledTimes(1)
      expect(onChange).toHaveBeenCalledWith('foo', 0, 'changed')
      if (button === 'escape') {
        expect(onAbort).toHaveBeenCalled()
      }
      if (button === 'enter') {
        expect(onAbort).not.toHaveBeenCalled()
      }
    },
  )

  it('allows to edit cells by typing and existing with arrowkey', async () => {
    const onChange = vi.fn()
    render(
      <RichTableEditor columns={columns} data={data} onChange={onChange} />,
    )

    await userEvent.click(cell('one'))
    await userEvent.keyboard(`changed{arrowright}`)
    expect(cell('two')).toHaveFocus()
    expect(onChange).toHaveBeenCalledWith('foo', 0, 'changed')
  })

  it('allows to edit with double click', async () => {
    const onChange = vi.fn()
    render(
      <RichTableEditor columns={columns} data={data} onChange={onChange} />,
    )

    await userEvent.dblClick(cell('one'))
    await userEvent.keyboard('changed{enter}')
    expect(cell('changed')).toHaveFocus()
    expect(onChange).toHaveBeenCalledWith('foo', 0, 'changed')
  })

  it('shows errors when cell editor has error', async () => {
    const onChange = vi.fn()
    render(
      <RichTableEditor columns={columns} data={data} onChange={onChange} />,
    )
    await userEvent.click(cell('1'))
    await userEvent.keyboard('{enter}x{enter}')
    expect(screen.getByText('Unit "x" not found.')).toBeInTheDocument()
    expect(onChange).not.toHaveBeenCalled()

    await userEvent.keyboard('{enter}1 cm{enter}')
    expect(screen.queryByText('Unit "x" not found.')).not.toBeInTheDocument()
    expect(onChange).toHaveBeenCalledWith('numeric', 0, 0.01)
  })

  it('allows to change column units', async () => {
    const onChange = vi.fn()
    render(
      <RichTableEditor columns={columns} data={data} onChange={onChange} />,
    )
    const unitInput = screen.getByDisplayValue('m') as HTMLInputElement
    await userEvent.type(unitInput, `{backspace}cm{enter}`)
    expect(onChange).not.toHaveBeenCalled()
    expect(screen.getByDisplayValue('100')).toBeInTheDocument()
  })

  it('renders and allows to use delete button', async () => {
    const onDelete = vi.fn()
    render(
      <RichTableEditor columns={columns} data={data} onDelete={onDelete} />,
    )
    await userEvent.click(screen.getAllByTestId('DeleteIcon')[1])
    expect(onDelete).toHaveBeenCalledWith(1)
    expect(screen.queryByText('three')).not.toBeInTheDocument()
  })

  it.each([[true], [false]])(
    'renders and allows to use add button with label %s',
    async (withLabel) => {
      const onAdd = vi.fn()
      render(
        <RichTableEditor
          columns={columns}
          data={data}
          onAdd={onAdd}
          itemLabel={withLabel ? 'item' : undefined}
        />,
      )
      await userEvent.click(
        screen.getByText('Add' + (withLabel ? ' item' : '')),
      )
      expect(onAdd).toHaveBeenCalled()
    },
  )

  it('does not render add or delete button by default', () => {
    render(<RichTableEditor columns={columns} data={data} />)
    expect(screen.queryByText('Add')).not.toBeInTheDocument()
    expect(screen.queryByTestId('DeleteIcon')).not.toBeInTheDocument()
  })
})
