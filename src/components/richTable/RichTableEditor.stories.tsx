import {LocalizationProvider} from '@mui/x-date-pickers'
import {AdapterDateFns} from '@mui/x-date-pickers/AdapterDateFnsV3'
import {Meta, StoryObj} from '@storybook/react'

import CopyToClipboard from '../values/actions/CopyToClipboard'
import Datetime from '../values/primitives/Datetime'
import EnumSelect from '../values/primitives/EnumSelect'
import Link from '../values/primitives/Link'
import Numeral from '../values/primitives/Numeral'
import Text from '../values/primitives/Text'
import RichTableEditor, {RichTableEditorColumn} from './RichTableEditor'

const meta = {
  title: 'components/richTable/RichTableEditor',
  component: RichTableEditor,
  decorators: [
    (Story) => (
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <Story />
      </LocalizationProvider>
    ),
  ],
  tags: ['autodocs'],
} satisfies Meta<typeof RichTableEditor>

export default meta

type Story = StoryObj<typeof meta>
export const Root: Story = {
  args: {
    data: [1, 2, 3, 4, 5].map((current) => ({
      id: `meassure ${current}`,
      current,
      voltage: current * 2,
    })),
    columns: [
      {
        key: 'id',
        component: Text,
      },
      {
        key: 'current',
        component: Numeral,
        props: {
          unit: 'A',
          actions: <CopyToClipboard />,
          editable: true,
        },
      },
      {
        key: 'voltage',
        component: Numeral,
        props: {
          unit: 'V',
          editable: true,
        },
      },
      {
        key: 'time',
        component: Datetime,
        props: {
          variant: 'date',
          editable: true,
        },
      },
      {
        key: 'link',
        component: Link,
      },
      {
        key: 'automated',
        component: EnumSelect,
        props: {
          values: ['yes', 'no'],
          editable: true,
        },
      },
    ] as RichTableEditorColumn[],
  },
}
