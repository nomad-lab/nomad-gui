import {screen} from '@testing-library/react'

import {assert} from '../../utils/utils'

export function cell(text: string) {
  const element = screen.getByDisplayValue(text).closest('td')
  assert(element)
  return element
}
