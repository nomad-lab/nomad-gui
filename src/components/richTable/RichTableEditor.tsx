import {Delete} from '@mui/icons-material'
import {
  Box,
  BoxProps,
  Button,
  Divider,
  IconButton,
  Paper,
  Popover,
  Table,
  TableBody,
  TableCell,
  TableCellProps,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material'
import React, {
  FocusEvent,
  FocusEventHandler,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react'

import useLocalState from '../../hooks/useLocalState'
import ContainerProps from '../values/containers/ContainerProps'
import PrimitiveProps from '../values/primitives/PrimitiveProps'
import {TextProps} from '../values/primitives/Text'
import Unit from '../values/utils/Unit'

export type RichTableEditorColumn = {
  key: string
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  component: React.FunctionComponent<PrimitiveProps<any>>
  props?: Partial<
    Pick<
      ContainerProps,
      'unit' | 'label' | 'placeholder' | 'displayUnit' | 'editable'
    >
  >
}

export type RichtTableEditorProps = {
  data: {[key: string]: unknown}[]
  itemLabel?: string
  columns: RichTableEditorColumn[]
  onChange?: (columnKey: string, rowIndex: number, value: unknown) => void
  onAdd?: () => void
  onDelete?: (rowIndex: number) => void
  // The cell might have been edited and onChange called, but the user
  // pressed escape to "abort" the edit. Parent components need to manage
  // the state and restore the old value.
  onAbort?: (columnKey: string, rowIndex: number) => void
}

const TableCellWithPrimitive = React.forwardRef(function TabelCellInput(
  {
    value,
    valueIndex,
    onChange,
    component: Component,
    componentProps,
    onBlur,
    onFocus,
    onDoubleClick,
    ...props
  }: Omit<PrimitiveProps, 'onBlur' | 'onFocus'> &
    Pick<TableCellProps, 'onBlur' | 'onFocus'> &
    Pick<BoxProps, 'onDoubleClick'> & {
      valueIndex: number
      component: React.FunctionComponent<PrimitiveProps & {valueIndex: number}>
      componentProps?: Partial<PrimitiveProps>
    },
  tableCellRef,
) {
  const [localValue, setLocalValue] = useLocalState(value, {onChange})
  const [error, setError] = React.useState<string | undefined>(undefined)
  const [hasFocus, setHasFocus] = React.useState(false)

  const handleInputFocus = useCallback(
    (event: FocusEvent<HTMLInputElement>) => {
      event.target.select()
      setHasFocus(true)
    },
    [],
  )
  const handleInputBlur = useCallback(() => {
    setHasFocus(false)
  }, [])

  const handleCellFocus = useCallback(
    (event: FocusEvent<HTMLTableCellElement>) => {
      setHasFocus(true)
      onFocus?.(event)
    },
    [onFocus],
  )

  const handleCellBlur = useCallback(
    (event: FocusEvent<HTMLTableCellElement>) => {
      setHasFocus(false)
      onBlur?.(event)
    },
    [onBlur],
  )

  const handleError = useCallback((error: string | undefined) => {
    setError(error)
  }, [])

  const anchorRef = useRef<HTMLDivElement>(null)

  return (
    <TableCell
      padding='none'
      ref={tableCellRef}
      tabIndex={0}
      onBlur={handleCellBlur}
      onFocus={handleCellFocus}
      sx={{
        outline: 'none',
        display: 'relative',
        '&:focus-within': {
          boxShadow: (theme) =>
            `inset 0 0 0 2px ${
              error ? theme.palette.error.main : theme.palette.primary.main
            }`,
        },
      }}
      onDoubleClick={onDoubleClick}
    >
      <Popover
        tabIndex={-1}
        open={!!error && hasFocus}
        anchorEl={anchorRef.current}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        disableAutoFocus
        disableEnforceFocus
        disableRestoreFocus
      >
        <Typography color='error' sx={{padding: 1}}>
          {error}
        </Typography>
      </Popover>
      <Box ref={anchorRef}>
        <Component
          valueIndex={valueIndex}
          {...componentProps}
          table
          fullWidth
          value={localValue as string}
          onChange={setLocalValue}
          onFocus={handleInputFocus}
          onBlur={handleInputBlur}
          {...props}
          error={!!error}
          onError={handleError}
          hiddenUnit
          {...({
            inputProps: {
              sx: {
                userSelect: 'none',
                pointerEvents: 'none',
                '& button': {
                  pointerEvents: 'all !important',
                },
                width: '100%',
                padding: 2,
                color: error && ((theme) => theme.palette.error.main),
                '&:focus-within': {
                  boxShadow: (theme) =>
                    `inset 0 0 0 2px ${
                      error
                        ? theme.palette.error.main
                        : theme.palette.primary.main
                    }, 0 0 10px ${
                      error
                        ? theme.palette.error.main
                        : theme.palette.primary.main
                    }`,
                },
              },
              inputProps: {tabIndex: -1},
            } satisfies TextProps['inputProps'],
            // dynamically we only allow PrimitiveProps that are also TextProps,
            // put its too hard to express this in types and we hive the TextProps['inputProps']
            // via casts.
          } as unknown as PrimitiveProps)}
        />
      </Box>
    </TableCell>
  )
})

export default function RichTableEditor({
  data,
  columns: initialColumns,
  onAbort,
  onChange,
  onAdd,
  onDelete,
  itemLabel,
}: RichtTableEditorProps) {
  const cellRefs = useRef<HTMLElement[]>([])
  const editorRefs = useRef<HTMLInputElement[]>([])
  const focusedCellIndexRef = useRef<number | null>(null)
  const explicitEditRef = useRef<boolean>(false)

  const [columns, setColumns] = useState(initialColumns)

  const handleUnitChange = useCallback(
    (displayUnit: string, colIndex: number) => {
      setColumns((columns) => {
        const oldColumn = columns[colIndex]
        const newColummns = [...columns]
        newColummns[colIndex] = {
          ...oldColumn,
          props: {...oldColumn?.props, displayUnit},
        }
        return newColummns
      })
    },
    [],
  )

  const handleCellFocus = useCallback<FocusEventHandler<HTMLElement>>(
    (event) => {
      let index = cellRefs.current.indexOf(event.target)
      if (index === -1) {
        index = editorRefs.current.indexOf(event.target as HTMLInputElement)
      }
      if (index !== -1) {
        focusedCellIndexRef.current = index
      } // else something else was focused, like a button or a popover
      // within the cell
    },
    [],
  )

  const handleCellBlur = useCallback(() => {
    focusedCellIndexRef.current = null
  }, [])

  const handleDoubleClick = useCallback(() => {
    if (focusedCellIndexRef.current !== null) {
      explicitEditRef.current = true
      editorRefs.current[focusedCellIndexRef.current].focus()
    }
  }, [])

  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      if (focusedCellIndexRef.current === null) {
        return
      }

      const index = focusedCellIndexRef.current
      const colIndex = index % columns.length
      const rowIndex = Math.floor(index / columns.length)
      let preventDefault = true

      function applyArrowNavigation() {
        if (e.key === 'ArrowRight') {
          if (colIndex !== columns.length - 1) {
            cellRefs.current[index + 1].focus()
          }
        } else if (e.key === 'ArrowLeft') {
          if (colIndex !== 0) {
            cellRefs.current[index - 1].focus()
          }
        } else if (e.key === 'ArrowDown') {
          if (rowIndex !== data.length - 1) {
            cellRefs.current[index + columns.length].focus()
          }
        } else if (e.key === 'ArrowUp') {
          if (rowIndex !== 0) {
            cellRefs.current[index - columns.length].focus()
          }
        } else {
          return false
        }
        return true
      }

      if (editorRefs.current[index] === document.activeElement) {
        if (e.key === 'Escape') {
          // Leave and restore old value
          onAbort?.(columns[colIndex].key, rowIndex)
          cellRefs.current[index].focus()
        } else if (e.key === 'Enter') {
          // Leave and save new value
          cellRefs.current[index].focus()
        } else if (explicitEditRef.current) {
          preventDefault = false
        } else if (!applyArrowNavigation()) {
          preventDefault = false
        }
      } else if (!applyArrowNavigation()) {
        if (e.key === 'Enter') {
          explicitEditRef.current = true
          editorRefs.current[index].focus()
        } else if (e.key.length === 1) {
          // Only "printable" characters have a length of 1. All
          // control keys have a spoken name like "Enter" or "ArrowRight".
          explicitEditRef.current = false
          editorRefs.current[index].focus()
          preventDefault = false
        } else {
          preventDefault = false
        }
      }

      if (preventDefault) {
        e.preventDefault()
      }
      e.stopPropagation()
    }
    document.addEventListener('keydown', handleKeyDown, true)
    return () => document.removeEventListener('keydown', handleKeyDown, true)
  }, [columns, columns.length, data.length, onAbort])

  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            {columns.map((column, colIndex) => {
              const {displayUnit, unit, label} = column.props || {}
              return (
                <TableCell key={column.key}>
                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                      gap: 1,
                    }}
                  >
                    {label || column.key}
                    {(displayUnit || unit) && (
                      <>
                        &nbsp;[
                        <Unit
                          unit={unit}
                          displayUnit={displayUnit || unit}
                          onDisplayUnitChange={(unit) =>
                            handleUnitChange(unit, colIndex)
                          }
                          displayUnitLocked={undefined}
                          unitSelectProps={{
                            textFieldProps: {
                              variant: 'standard',
                              size: 'small',
                              margin: 'none',
                            },
                          }}
                        />
                        ]
                      </>
                    )}
                  </Box>
                </TableCell>
              )
            })}
            {onDelete && <TableCell />}
          </TableRow>
        </TableHead>
        <TableBody sx={{backgroundColor: 'background.default'}}>
          {data.map((row, rowIndex) => (
            <TableRow
              key={rowIndex}
              sx={{'&:last-child td, &:last-child th': {border: 0}}}
            >
              {columns.map((column, colIndex) => (
                <TableCellWithPrimitive
                  valueIndex={rowIndex}
                  onDoubleClick={handleDoubleClick}
                  onFocus={handleCellFocus}
                  onBlur={handleCellBlur}
                  key={column.key}
                  ref={(element) => {
                    cellRefs.current[rowIndex * columns.length + colIndex] =
                      element as HTMLElement
                  }}
                  component={column.component}
                  componentProps={column.props}
                  value={row[column.key]}
                  onChange={(value) => onChange?.(column.key, rowIndex, value)}
                  inputRef={(element: HTMLInputElement | null) => {
                    editorRefs.current[rowIndex * columns.length + colIndex] =
                      element as HTMLInputElement
                  }}
                />
              ))}
              {onDelete && (
                <TableCell sx={{width: '64px', paddingY: 0}}>
                  <IconButton onClick={() => onDelete(rowIndex)} size='small'>
                    <Delete />
                  </IconButton>
                </TableCell>
              )}
            </TableRow>
          ))}
        </TableBody>
      </Table>
      {onAdd && (
        <>
          <Divider />
          <Box sx={{margin: 1, display: 'flex', flexDirection: 'row-reverse'}}>
            <Button variant='contained' onClick={onAdd}>
              {itemLabel ? `Add ${itemLabel}` : 'Add'}
            </Button>
          </Box>
        </>
      )}
    </TableContainer>
  )
}
