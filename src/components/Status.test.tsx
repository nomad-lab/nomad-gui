import {render, screen} from '@testing-library/react'

import Status from './Status'
import {Public} from './icons'

describe('Status', () => {
  it('renders with label', () => {
    render(<Status label='test' icon={<Public />} />)
    expect(screen.getByText('test')).toBeInTheDocument()
  })
  it('renders without label', () => {
    render(<Status variant='icon' label='test' icon={<Public />} />)
    expect(screen.queryByText('test')).not.toBeInTheDocument()
  })
})
