import {Quantity} from '../../utils/metainfo'
import {JSONValue} from '../../utils/types'
import {getPropertyLabel} from '../editor/PropertyEditor'
import DynamicValue from '../values/utils/DynamicValue'
import {createDynamicComponentSpec} from '../values/utils/dynamicComponents'

type QuantityProps = {
  value: JSONValue | undefined
  onChange?: (value: JSONValue) => void
  quantityDef: Quantity
  editable?: boolean
  label?: string
}

export default function QuantityValue({
  quantityDef,
  ...valueProps
}: QuantityProps) {
  const component = createDynamicComponentSpec(quantityDef)

  return (
    <DynamicValue
      component={component}
      label={getPropertyLabel(quantityDef.name)}
      placeholder='no value'
      fullWidth
      {...valueProps}
    />
  )
}
