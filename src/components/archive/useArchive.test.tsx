import {act, renderHook} from '@testing-library/react'
import {vi} from 'vitest'

import {ArchiveChangeAction} from '../../models/entryEditRequestModels'
import {MSectionRequest} from '../../models/graphRequestModels'
import {MSectionResponse} from '../../models/graphResponseModels'
import {archiveRequest} from '../../pages/entry/entryRoute'
import {assert} from '../../utils/utils'
import * as useRouteData from '../routing/useRouteData'
import {addDefinitions, mockArchive, removeDefinitions} from './archive.helper'
import useArchive, {
  Archive,
  allArchives,
  useArchiveChanges,
  useArchiveProperty,
} from './useArchive'

describe('useArchive', () => {
  describe('Archive', () => {
    let archive: Archive

    beforeEach(() => {
      archive = new Archive()
    })

    // TODO tests about changing repeating sections as a whole. Currently
    // there is only a test for adding or removing the whole list. But
    // nothing about adding a sub-section, removing one, etc.
    it.each([
      // quantities
      ['adding a Quantity', {}, {q: '*'}, {q: 1}, {q: 1}, [['q', 1]]],
      ['removing a Quantity', {q: 1}, {q: '*'}, {}, {}, [['q', undefined]]],
      ['removing a non existing Quantity', {}, {q: '*'}, {}, {}, []],
      ['updating a Quantity', {q: 1}, {q: '*'}, {q: 2}, {q: 2}, [['q', 2]]],
      ['ignoring a Quantity', {q: 1}, {}, {}, {q: 1}, []],
      // sub sections
      ['adding a SubSection', {}, {s: '*'}, {s: {}}, {s: {}}, [['s', {}]]],
      ['removing a SubSection', {s: {}}, {s: '*'}, {}, {}, [['s', undefined]]],
      ['removing a non existing SubSection', {}, {s: '*'}, {}, {}, []],
      ['updating a SubSection', {s: {}}, {s: '*'}, {s: {}}, {s: {}}, []],
      ['ignoring a SubSection', {s: {}}, {}, {}, {s: {}}, []],
      // repeating sub sections as a whole
      [
        'adding a repeating SubSection',
        {},
        {r: '*'},
        {r: [{}]},
        {r: [{}]},
        [['r', [{}]]],
      ],
      [
        'adding to an existing repeating SubSection',
        {r: []},
        {r: '*'},
        {r: [{}]},
        {r: [{}]},
        [['r', [{}]]],
      ],
      [
        'removing a repeating SubSection',
        {r: [{}]},
        {r: '*'},
        {},
        {},
        [['r', undefined]],
      ],
      [
        'removing from an existing repeating SubSection',
        {r: [{}]},
        {r: '*'},
        {r: []},
        {r: []},
        [['r', []]],
      ],
      [
        'updating an existing repeating SubSection',
        {r: [{}]},
        {r: '*'},
        {r: [{}]},
        {r: [{}]},
        [],
      ],
      ['ignoring a repeating SubSection', {r: [{}]}, {}, {}, {r: [{}]}, []],
      // a sub section in a repeating sub section
      [
        'index adding to a repeating SubSection',
        {},
        {'r[0]': '*'},
        {r: [{}]},
        {r: [{}]},
        [['r', [{}]]],
      ],
      [
        'index removing from a repeating SubSection',
        {r: [{}]},
        {'r[0]': '*'},
        {},
        {r: []},
        [['r', undefined]],
      ],
      [
        'index removing from a non existing repeating SubSection',
        {},
        {'r[0]': '*'},
        {},
        {},
        [],
      ],
      [
        'index removing from a repeating SubSection a Section that does not exist',
        {r: []},
        {'r[0]': '*'},
        {},
        {r: []},
        [],
      ],
      [
        'index updating in a repeating SubSection',
        {r: [{}]},
        {'r[0]': '*'},
        {r: [{}]},
        {r: [{}]},
        [],
      ],
      [
        'ignoring a SubSection in a repeating SubSection',
        {r: [{}]},
        {},
        {},
        {r: [{}]},
        [],
      ],
      // recurse into sub sections
      [
        'adding a Quantity in a SubSection',
        {s: {}},
        {s: {q: '*'}},
        {s: {q: 1}},
        {s: {q: 1}},
        [['s/q', 1]],
      ],
      [
        'adding a Quantity in a repeated SubSection',
        {s: [{}]},
        {'s[0]': {q: '*'}},
        {s: [{q: 1}]},
        {s: [{q: 1}]},
        [['s/0/q', 1]],
      ],
    ])(
      'updates when %s',
      (
        description,
        initialArchive: MSectionResponse,
        request: MSectionRequest,
        response: MSectionResponse,
        expectedArchive,
        expectedUpdates,
      ) => {
        Object.assign(request, {
          ...archiveRequest,
        })
        assert(request.m_request)
        request.m_request.exclude = ['*']

        // add definitions to make the function work
        addDefinitions(response, {q: 1, s: {}, r: []})

        archive.archive = initialArchive
        const updates = archive.updateArchive(request, response, {})
        expect(archive.archive.m_def).not.toBeUndefined()

        // remove definitions to make the comparison simpler
        removeDefinitions(archive.archive)

        expect(archive.archive).toEqual(expectedArchive)
        expect(updates).toEqual(expectedUpdates)
      },
    )
  })

  const renderCounter = vi.fn()
  function useArchiveWithRenderCounter(path: string) {
    renderCounter()
    return useArchiveProperty(path)
  }

  let archive: Archive

  beforeEach(() => {
    archive = mockArchive()
    archive.loading = false
    renderCounter.mockClear()
  })

  it('registers a new archive', () => {
    vi.spyOn(useRouteData, 'default').mockReturnValue({
      entry_id: 'otherEntryId',
    })
    renderHook(() => useArchiveProperty('q'))
    expect(allArchives.has('otherEntryId')).toBe(true)
  })

  it('removes element when unmounted', () => {
    const {unmount} = renderHook(() => useArchiveProperty('q'))
    expect(archive.elements.map.get('q')?.length).toBe(1)
    act(() => unmount())
    expect(archive.elements.map.get('q')?.length).toBe(0)
  })

  it('re-renders on update', () => {
    const {result} = renderHook(() => useArchiveWithRenderCounter('q'))
    expect(result.current.value).toEqual(undefined)
    act(() => archive.updateArchive({q: '*'}, addDefinitions({q: 1}), {}))
    expect(result.current.value).toEqual(1)
    expect(renderCounter).toHaveBeenCalledTimes(2)
  })

  it('does not re-render if something else updates', () => {
    const {result} = renderHook(() => useArchiveWithRenderCounter('q'))
    expect(result.current.value).toEqual(undefined)
    act(() => archive.updateArchive({s: '*'}, addDefinitions({s: {}}), {}))
    expect(result.current.value).toEqual(undefined)
    expect(renderCounter).toHaveBeenCalledTimes(1)
  })

  it.each([
    ['root section', 'q', 1, {}, {q: 1}],
    ['sub section', 's/q', 1, {s: {}}, {s: {q: 1}}],
  ])(
    're-renders if it submits change to %s',
    (descriptions, path, value, initialArchive, expectedArchive) => {
      archive.archive = initialArchive
      const {result} = renderHook(() => useArchiveWithRenderCounter(path))
      expect(result.current.value).toEqual(undefined)
      act(() => result.current.change('upsert', value))
      expect(result.current.value).toEqual(value)
      expect(archive).toBeDefined()
      expect(archive.archive).toEqual(expectedArchive)
      expect(renderCounter).toHaveBeenCalledTimes(2)
    },
  )

  it('re-renders all components listening to the same element', () => {
    const {result: result1} = renderHook(() => useArchiveWithRenderCounter('q'))
    const {result: result2} = renderHook(() => useArchiveWithRenderCounter('q'))

    act(() => result1.current.change('upsert', 1))
    expect(result1.current.value).toEqual(1)
    expect(result2.current.value).toEqual(1)
    expect(renderCounter).toHaveBeenCalledTimes(4)
  })

  it('allows to resolve references in the archive', () => {
    vi.spyOn(useRouteData, 'default').mockReturnValue({
      entry_id: 'otherEntryId',
    })
    const {result, rerender} = renderHook(() => useArchive())
    result.current.updateArchive(
      {data: {ref: '*'}},
      addDefinitions({data: {ref: '/uploads/1/entries/1/archive/data'}}),
      {
        uploads: {
          1: {entries: {1: {archive: addDefinitions({data: {q: 'test'}})}}},
        },
      },
    )
    rerender(() => useArchive())
    const typedArchive = result.current.archive as {data: {ref: string}}
    expect(typedArchive.data.ref).toEqual('/uploads/1/entries/1/archive/data')
    expect(result.current.resolveRef(typedArchive.data.ref)).toMatchObject({
      q: 'test',
    })
  })
})

describe('useArchiveProperty', () => {
  beforeEach(() => {
    mockArchive({
      data: addDefinitions({
        quantity: 'test_value',
        sub_section: {
          quantity: 'test_value',
          nested_sub_section: {quantity: 'test_value'},
        },
        repeating_sub_section: [{quantity: 'test_value'}],
      }),
    })
  })

  it.each([
    'quantity',
    'sub_section/quantity',
    'repeating_sub_section/0/quantity',
    'sub_section/nested_sub_section/quantity',
    'sub_section',
    'sub_section/nested_sub_section',
  ])('returns the value of %s', (path) => {
    const {result} = renderHook(() => useArchiveProperty(path))
    if (path.endsWith('quantity')) {
      expect(result.current.value).toEqual('test_value')
    } else {
      expect(result.current.value).toHaveProperty('quantity')
      expect(result.current.value).toMatchObject({quantity: 'test_value'})
    }
  })

  it('returns value of empty path', () => {
    const {result} = renderHook(() => useArchiveProperty(''))
    expect(result.current.value).toMatchObject({quantity: 'test_value'})
  })

  it.each([
    'quantity',
    'sub_section/quantity',
    'repeating_sub_section/0/quantity',
    'sub_section/nested_sub_section/quantity',
    'sub_section',
    'repeating_sub_section',
    'sub_section/nested_sub_section',
  ])('returns the definition of %s', (path) => {
    const {result} = renderHook(() => useArchiveProperty(path))
    const propertyDefinition = result.current.getDefinition()
    expect(propertyDefinition).not.toBeUndefined()
    expect(propertyDefinition?.name).toEqual(path.split('/').pop())
  })

  it('returns the definition of empty path', () => {
    const {result} = renderHook(() => useArchiveProperty(''))
    const propertyDefinition = result.current.getDefinition()
    expect(propertyDefinition).toBeUndefined()
  })

  it.each([
    'quantity',
    'sub_section/quantity',
    'repeating_sub_section/0/quantity',
    'sub_section/nested_sub_section/quantity',
  ])('allows to change %s', (path) => {
    const {result, rerender} = renderHook(() => useArchiveProperty(path))
    act(() => result.current.change('upsert', 'new_value'))
    rerender()
    expect(result.current.value).toEqual('new_value')
  })

  it.each([
    ['add sub section', 'sub_section', 'upsert', {}, undefined],
    ['remove sub section', 'sub_section', 'remove', undefined, undefined],
    ['add repeated sub section', 'repeated_sub_section', 'upsert', [{}], 0],
    ['remove repeated sub section', 'repeated_sub_section', 'remove', [], 0],
  ])('allows to %s', (description, path, action, newValue, index) => {
    const {result, rerender} = renderHook(() => useArchiveProperty(path))
    act(() =>
      result.current.change(action as ArchiveChangeAction, newValue, index),
    )
    rerender()
    expect(result.current.value).toEqual(newValue)
  })

  it('accumulates the latest change', () => {
    const {result, rerender} = renderHook(() => useArchiveProperty('quantity'))
    act(() => result.current.change('upsert', 'newValue'))
    rerender()
    act(() => result.current.change('upsert', 'nextValue'))
    rerender()
    expect(result.current.value).toEqual('nextValue')

    const {result: useArchiveResult} = renderHook(() => useArchive())
    expect(useArchiveResult.current.changeStack.length).toEqual(1)
  })

  it('does not accumulate unrelated changes', () => {
    const {result, rerender} = renderHook(() => useArchiveProperty('quantity'))
    act(() => result.current.change('upsert', 'newValue'))
    rerender()
    act(() => result.current.change('remove', undefined))
    rerender()
    expect(result.current.value).toEqual(undefined)

    const {result: useArchiveResult} = renderHook(() => useArchive())
    expect(useArchiveResult.current.changeStack.length).toEqual(2)
  })

  it('allows to resolve references in the archive', () => {
    vi.spyOn(useRouteData, 'default').mockReturnValue({
      entry_id: 'otherEntryId',
    })
    const {result} = renderHook(() => useArchive())
    result.current.updateArchive(
      {data: {ref: '*'}},
      addDefinitions({data: {ref: '/uploads/1/entries/1/archive/data'}}),
      {
        uploads: {
          1: {entries: {1: {archive: addDefinitions({data: {q: 'test'}})}}},
        },
      },
    )

    const {
      result: {
        current: {value, resolveRef},
      },
    } = renderHook(() => useArchiveProperty('data/ref'))
    expect(value).toEqual('/uploads/1/entries/1/archive/data')
    expect(resolveRef(value as string)).toMatchObject({q: 'test'})
  })
})

describe('useArchiveChanges', () => {
  let archive: Archive
  beforeEach(() => {
    archive = mockArchive({
      data: addDefinitions({
        quantity: 'test_value',
      }),
    })
  })

  it('has submitted changes', () => {
    const {result} = renderHook(() => useArchiveChanges())
    act(() => archive.submitElementChange('quantity', 'upsert', 'new_value'))
    expect(result.current.changeStack.length).toBe(1)
  })

  it('clears changes', () => {
    const {result} = renderHook(() => useArchiveChanges())
    act(() => archive.submitElementChange('quantity', 'upsert', 'new_value'))
    expect(result.current.changeStack.length).toBe(1)
    act(() => result.current.clearChangeStack())
    expect(result.current.changeStack.length).toBe(0)
  })
})
