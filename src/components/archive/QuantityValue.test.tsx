import {LocalizationProvider} from '@mui/x-date-pickers'
import {AdapterDateFns} from '@mui/x-date-pickers/AdapterDateFnsV3'
import {render, screen} from '@testing-library/react'
import dayjs from 'dayjs'
import {PropsWithChildren} from 'react'

import {CustomType, PythonType, Quantity} from '../../utils/metainfo'
import QuantityValue from './QuantityValue'

describe('QuantityValue', () => {
  const defaultQuantity: Omit<Quantity, 'type'> = {
    m_def: 'Quantity',
    name: 'test_quantity',
    shape: [],
  }

  function pythonType(typeData: PythonType['type_data']): PythonType {
    return {type_kind: 'python', type_data: typeData}
  }

  const renderOptions = {
    wrapper: ({children}: PropsWithChildren) => (
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        {children}
      </LocalizationProvider>
    ),
  }

  it.each([
    [
      'type string',
      {
        ...defaultQuantity,
        type: pythonType('str'),
      },
      'test',
      'test',
    ],
    [
      'type float',
      {
        ...defaultQuantity,
        type: pythonType('float'),
      },
      1.87,
      '1.87',
    ],
    [
      'type int',
      {
        ...defaultQuantity,
        type: pythonType('int'),
      },
      42,
      '42',
    ],
    [
      'type bool',
      {
        ...defaultQuantity,
        type: pythonType('bool'),
      },
      true,
      'true',
    ],
    [
      'type datetime',
      {
        ...defaultQuantity,
        type: {
          type_kind: 'custom',
          type_data: 'nomad.metainfo.data_type.Datetime',
        } as CustomType,
      },
      '2021-01-01T10:00:00Z',
      dayjs('2021-01-01T10:00:00Z').format('DD/MM/YYYY hh:mm A'),
    ],
  ])(
    'renders quantity with %s',
    (description, quantity: Quantity, value, expected) => {
      render(
        <QuantityValue quantityDef={quantity} value={value} />,
        renderOptions,
      )
      expect(screen.getByText(expected)).toBeInTheDocument()
    },
  )
})
