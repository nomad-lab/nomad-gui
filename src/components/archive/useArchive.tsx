import {useCallback, useEffect, useMemo, useState} from 'react'
import {usePrevious} from 'react-use'

import useRender from '../../hooks/useRender'
import {ArchiveChange} from '../../models/entryEditRequestModels'
import {MSectionRequest} from '../../models/graphRequestModels'
import {
  EntryResponse,
  GraphResponse,
  MDefResponse,
  MSectionResponse,
  UploadResponse,
} from '../../models/graphResponseModels'
import entryRoute from '../../pages/entry/entryRoute'
import {Property, Section, resolveRef} from '../../utils/metainfo'
import {assert} from '../../utils/utils'
import {getIndexedKey} from '../routing/loader'
import useRouteData from '../routing/useRouteData'

class MultiMap<K, V> {
  map = new Map<K, V[]>()

  add(key: K, value: V) {
    if (!this.map.has(key)) {
      this.map.set(key, [])
    }
    this.map.get(key)?.push(value)
  }

  shift(key: K) {
    this.map.get(key)?.shift()
  }

  length(key: K) {
    return this.map.get(key)?.length ?? 0
  }

  get(key: K) {
    return this.map.get(key) || []
  }

  delete(key: K, value: V) {
    const values = this.map.get(key)
    assert(values, 'Cannot unregister an unregistered element')
    const index = values.indexOf(value)
    values.splice(index, 1)
  }
}

type ArchivePropertyState<T> = {
  value: T | undefined
  loading: boolean
}

type ArchivePropertyDispatch = React.Dispatch<ArchivePropertyState<unknown>>

/**
 * A class to manage the client-side state of an entry's archive. See
 * `useArchive` for the hook that provides access it.
 */
export class Archive {
  elements: MultiMap<string, ArchivePropertyDispatch>
  changeListener: (() => void)[]
  archive: MSectionResponse
  changeStack: ArchiveChange[]
  loading: boolean
  responsesWithReferencedArchives: MultiMap<string, GraphResponse>

  constructor() {
    this.elements = new MultiMap<string, ArchivePropertyDispatch>()
    this.changeListener = []
    this.archive = {}
    this.changeStack = []
    this.responsesWithReferencedArchives = new MultiMap()

    // TODO loading needs to be integrated with routing and the use of
    // useDataForRoute
    this.loading = true
  }

  registerElement(path: string, dispatch: ArchivePropertyDispatch) {
    this.elements.add(path, dispatch)
    return () => {
      this.elements.delete(path, dispatch)
    }
  }

  registerChangeListener(listener: () => void) {
    this.changeListener.push(listener)
    return () => {
      this.changeListener = this.changeListener.filter((l) => l !== listener)
    }
  }

  clearChangeStack() {
    while (this.changeStack.length > 0) {
      this.changeStack.pop()
    }
    this.changeListener.forEach((listener) => listener())
  }

  /**
   * Submits a change to the archive. This will update the archive,
   * dispatch the change to all registered elements, and keep track
   * of the change in the change stack.
   *
   * @param path The path to the property that should be changed.
   * @param action The action to perform on the property. Either 'upsert'
   * or 'remove'.
   * @param value The new value of the property.
   * @param index If the property is a repeating sub-section and index must be provided.
   */
  submitElementChange(
    path: string,
    action: 'upsert' | 'remove',
    value: unknown,
    index?: number,
  ) {
    // apply the value to the archive
    let current = this.archive
    const parts = path.split('/')
    const subSections = parts.slice(0, -1)
    for (const key of subSections) {
      current = current[key] as MSectionResponse
    }
    const valueIndex = parts[parts.length - 1]
    current[valueIndex] = value

    // dispatch change
    for (const dispatch of this.elements.get(path)) {
      dispatch({value, loading: false})
    }

    // translate the change into an ArchiveChange and keep the change in
    // this.changes and this.changeStack
    let change
    if (index !== undefined) {
      assert(Array.isArray(value), 'Value must be an array for indexed changes')
      change = {
        path: `${path}/${index}`,
        new_value: action === 'remove' ? undefined : {...value[index]},
        action,
      } satisfies ArchiveChange
    } else {
      change = {
        path,
        new_value: action === 'remove' ? undefined : value,
        action,
      } satisfies ArchiveChange

      if (this.changeStack.length > 0) {
        const lastChange = this.changeStack[this.changeStack.length - 1]
        if (
          lastChange.path === change.path &&
          lastChange.action === change.action
        ) {
          lastChange.new_value = change.new_value
          return
        }
      }
    }

    this.changeStack.push(change)
    this.changeListener.forEach((listener) => listener())
  }

  /**
   * Returns the value of the property at the given path if it exists in
   * the current archive.
   */
  getValue<T = unknown>(path: string) {
    let current = this.archive
    if (path === '') {
      return current as T
    }
    for (const key of path.split('/')) {
      if (current[key] === undefined) {
        return undefined
      }
      current = current[key] as MSectionResponse
    }
    return current as T | undefined
  }

  getState<T = unknown>(path: string) {
    if (this.loading) {
      return {loading: true} as ArchivePropertyState<T>
    } else {
      return {
        loading: false,
        value: this.getValue<T>(path),
      } as ArchivePropertyState<T>
    }
  }

  /**
   * Resolves a given reference within the archive and other archives
   * that have been loaded (e.g. via directive=resolved) in the context
   * of the current entry.
   */
  resolveRef(ref: string) {
    const match = ref.match(
      /^\/uploads\/[a-zA-Z0-9_-]+\/entries\/([a-zA-Z0-9]+)\/archive\//,
    )
    const entryId = match?.[1]
    assert(entryId, `Invalid archive reference ${ref}`)
    let resolved: MSectionResponse | undefined
    this.responsesWithReferencedArchives
      .get(entryId)
      .reverse()
      .find((apiResponse) => {
        resolved = resolveRef(ref, apiResponse)
        return resolved !== undefined
      })
    return resolved
  }

  /**
   * Merges new archive data represented as a request/response pair into the
   * archive. It detects a collection of changes and dispatches them to the
   * registered elements.
   */
  updateArchive(
    request: MSectionRequest,
    response: MSectionResponse,
    apiResponse: GraphResponse,
    path?: string,
  ) {
    const changes: [string, unknown][] = []

    // collect referenced archives (this might include the current one for
    // intra-entry references)
    Object.keys(apiResponse.uploads || []).forEach((key) => {
      const upload = apiResponse.uploads?.[key] as UploadResponse
      Object.keys(upload.entries || []).forEach((key) => {
        const entry = upload.entries?.[key] as EntryResponse
        if (entry.archive) {
          this.responsesWithReferencedArchives.add(key, apiResponse)
          // Keep the list of referenced archives short. Technically, it is
          // possible that different archives are referenced in subsequent
          // updates and we need to keep them all, but it is also very unlikely.
          // If we always keep all referenced archives, we might run into memory
          // issues, e.g. when an entry is edited and saved (this incl. update
          // after save) many times.
          // TODO Refine the implementation to merge the referenced archives
          // instead of keeping them all.
          if (this.responsesWithReferencedArchives.length(key) > 5) {
            this.responsesWithReferencedArchives.shift(key)
          }
        }
      })
    })

    // traverse
    const visit = (
      path: string,
      archive: MSectionResponse,
      originalRequest: MSectionRequest,
      response: MSectionResponse,
    ) => {
      const m_def = response.m_def as Section
      assert(m_def, `MSectionResponse for ${path} must come with a definition`)
      // TODO Is this good? Do we need to merge something here? What if
      // the definition changes?
      archive.m_def = m_def as MDefResponse

      // For requests that do not specify keys, the semantics is that
      // all properties are requested.
      let request = originalRequest
      const requestKeys = Object.keys(request)
      if (
        requestKeys.length <= 2 &&
        requestKeys.every((key) => ['m_request', 'm_def'].includes(key))
      ) {
        request = {...originalRequest}
        Object.keys(m_def.all_properties)
          .filter((key) => {
            return !(
              request.m_request?.exclude?.includes('*') ||
              request.m_request?.exclude?.includes(key)
            )
          })
          .forEach((key) => {
            request[key] = '*'
          })
      }

      for (const [requestKey, requestValue] of Object.entries(request)) {
        if (['m_request', 'm_def'].includes(requestKey)) {
          continue
        }

        const [key, index] = getIndexedKey(requestKey)
        const pathForCurrentKey = path ? `${path}/${key}` : key
        const propertyDefinition = m_def.all_properties[key]
        const childIsSubSection = propertyDefinition?.m_def === 'SubSection'
        const childIsRepeatingSubSection =
          childIsSubSection && propertyDefinition?.repeats
        const indexed = index !== undefined && childIsSubSection

        if (isNaN(Number(key))) {
          // key might be the index within a repeating sub-section
          // TODO I removed the assertion for now. The graph API
          // currently does not seem to include inherited properties in
          // all_properties. This causes issues, e.g. with figure from FigureSection.
          // assert(
          //   propertyDefinition,
          //   `MSectionResponse property ${key} in "${path}" without definition.`,
          // )
        }

        if (response[key] === undefined) {
          // delete
          if (!archive[key]) {
            continue
          }

          if (indexed) {
            const list = archive[key] as unknown[]
            if (list[index] === undefined) {
              continue
            }
            list[index] = undefined

            const firstEmptyIndex = list.findLastIndex(Boolean) + 1
            list.splice(firstEmptyIndex, list.length - firstEmptyIndex)
          } else {
            delete archive[key]
          }

          changes.push([pathForCurrentKey, response[key]])
        } else if (archive[key] === undefined) {
          // add
          if (indexed) {
            const archiveList =
              (archive[key] as unknown as MSectionResponse[]) || []
            archive[key] = archiveList
            const responseList = response[key] as MSectionResponse[]
            archiveList[index] = responseList[index]
          } else {
            archive[key] = response[key]
          }

          changes.push([pathForCurrentKey, response[key]])
        } else {
          // update
          if (!childIsSubSection) {
            archive[key] = response[key]
            changes.push([pathForCurrentKey, response[key]])
          } else if (childIsRepeatingSubSection) {
            // The semantics is that the sections with the same index are
            // the same. Even if a section is removed from the middle of the
            // list.
            // The recursion will update the sections themsleves.
            // Here, we only remove/add at the end of the list.
            const archiveList = archive[key] as MSectionResponse[]
            archive[key] = archiveList
            const responseList = response[key] as MSectionResponse[]
            if (responseList.length !== archiveList.length) {
              if (responseList.length > archiveList.length) {
                for (
                  let index = archiveList.length;
                  index < responseList.length;
                  index++
                ) {
                  archiveList.push(responseList[index])
                }
              }
              if (responseList.length < archiveList.length) {
                archiveList.splice(
                  responseList.length,
                  archiveList.length - responseList.length,
                )
              }
              changes.push([pathForCurrentKey, response[key]])
            }
          } // else { nothing to do, the recursion will update the section }
        }

        // recurse
        if (typeof response[key] === 'object' && childIsSubSection) {
          if (indexed) {
            const archiveList = archive[key] as MSectionResponse[]
            const responseList = response[key] as MSectionResponse[]
            visit(
              `${pathForCurrentKey}/${index}`,
              archiveList[index],
              requestValue as MSectionRequest,
              responseList[index],
            )
          } else if (childIsRepeatingSubSection) {
            const archiveList = archive[key] as MSectionResponse[]
            const responseList = response[key] as MSectionResponse[]
            for (let index = 0; index < responseList.length; index++) {
              visit(
                `${pathForCurrentKey}/${index}`,
                archiveList[index],
                requestValue as MSectionRequest,
                responseList[index],
              )
            }
          } else {
            visit(
              pathForCurrentKey,
              archive[key] as MSectionResponse,
              requestValue as MSectionRequest,
              response[key] as MSectionResponse,
            )
          }
        }
      }
    }

    if (path) {
      const section = this.getValue(path) as MSectionResponse
      assert(section && section?.m_def, `No section for path ${path}`)
      visit(path, section, request, response)
    } else {
      visit('', this.archive, request, response)
    }

    // TODO the loading state needs to be properly handled.
    if (this.loading) {
      this.loading = false
      for (const [path, elements] of this.elements.map.entries()) {
        for (const dispatch of elements) {
          dispatch?.({
            value: this.getValue(path),
            loading: false,
          })
        }
      }
    } else {
      for (const [path, value] of changes) {
        for (const dispatch of this.elements.get(path)) {
          dispatch?.({
            value,
            loading: false,
          })
        }
      }
    }

    return changes
  }

  /**
   * Provides the definition of the property at the given path.
   * Returns a tuple with the containing section definition, and the
   * property definition.
   */
  getDefinition(path: string): [Section, Property | undefined] | undefined {
    if (path === '') {
      return [this.archive.m_def as Section, undefined]
    }
    const pathMatch = path.match(/^(.*)(\/\d+)?$/)
    const pathWithoutIndex = pathMatch?.[1]
    assert(pathWithoutIndex, `Invalid property path ${path}`)

    let section = this.archive
    const parts = path.split('/')
    const subSections = parts.slice(0, -1)
    const property = parts[parts.length - 1]
    for (const key of subSections) {
      section = section[key] as MSectionResponse
    }
    const sectionDef = section.m_def as Section
    const propertyDef = sectionDef?.all_properties?.[property]

    assert(
      sectionDef && propertyDef,
      `No definition for property ${path}: ${sectionDef?.name}`,
    )
    return [sectionDef, propertyDef]
  }
}

export const allArchives = new Map<string, Archive>()

export function getArchive(entry_id: string) {
  if (!allArchives.has(entry_id)) {
    allArchives.set(entry_id, new Archive())
  }
  return allArchives.get(entry_id) as Archive
}

/**
 * A hook that returns the _archive_ for the current entry. Here _archive_
 * is an `Archive` instance. A local copy of the archive and manager
 * the client-side state of the archive. This includes the archive data
 * updated from the latest API requests and user changes, the stack of
 * unsaved changes, and a registry for components listening to properties
 * of the archive via `useArhiveProperty`.
 */
export default function useArchive() {
  const {entry_id} = useRouteData(entryRoute)
  assert(entry_id, 'UseArchive only works within an entry route.')
  return getArchive(entry_id)
}

export type ArchiveChanges = {
  changeStack: ArchiveChange[]
  clearChangeStack: () => void
  // undo: () => void,
  // redo: () => void,
}

/**
 * A hook that returns the lasted change stack of the current entry's archive.
 * The hook causes a re-render, if there are new changes.
 */
export function useArchiveChanges() {
  const archive = useArchive()
  const [changeStack, setChangeStack] = useState<ArchiveChange[]>([
    ...archive.changeStack,
  ])

  const handleChange = useCallback(() => {
    setChangeStack([...archive.changeStack])
  }, [archive])

  useMemo(() => {
    return archive.registerChangeListener(handleChange)
  }, [archive, handleChange])

  const clearChangeStack = useCallback(() => {
    archive.clearChangeStack()
  }, [archive])

  return {
    changeStack,
    clearChangeStack,
  } satisfies ArchiveChanges
}

export type ArchiveProperty<Type, DefinitionType extends Property> = {
  /**
   * Archive path that identifies the property within an archive.
   */
  path: string

  /**
   * The current value of the property.
   */
  value?: Type

  /**
   * Will submit a value change to the archive. This will cause a re-render.
   */
  change: (action: 'upsert' | 'remove', value: Type, index?: number) => void

  /**
   * True if the value is still loading or there is a running request
   * that might update the value.
   */
  loading: boolean

  /**
   * Gets the definition of the property. The definition will
   * only be available, if the property has a value. The function
   * will throw, if the definition is not available.
   */
  getDefinition: () => DefinitionType | undefined

  /**
   * Resolves a given reference within the archive and other archives
   * that have been loaded (e.g. via directive=resolved) in the context
   * of the current entry.
   */
  resolveRef: (ref: string) => MSectionResponse | undefined
}

/**
 * A hook that interacts with the archive of the current entry. It allows
 * to get, set, and react to changes of individual properties in the archive.
 *
 * An property can be a quantity, a sub-section, or a
 * repeating sub-section. Properties are identified with a path from archive
 * root to the property. The paths use `/` as separator and align with the syntax
 * of the graph API or archive references.
 *
 * What it means that an propery changes depends on the
 * kind of property. A quantity value changes if the value changes, a (repeating)
 * sub-section changes if a/the section is added, removed, or replaced.
 *
 * The components using `useArchiveProperty` will be re-rendered
 * if the property changes. Changes can be triggered by using the returned
 * setter, another client of `useArchiveProperty` using the setter on the same element,
 * a route change causing an archive update, another component causing an archive
 * update via `useArchive`.
 */
export function useArchiveProperty<T = unknown, D extends Property = Property>(
  path: string,
): ArchiveProperty<T, D> {
  const archive = useArchive()

  const previousPath = usePrevious(path)
  assert(
    previousPath === path || previousPath === undefined,
    'Archive path must not change',
  )

  // TODO the value in the archive might have changed for the same path,
  // but we keep relying on the old and wrong localState.
  const propertyState = archive.getState(path) as ArchivePropertyState<T>
  const render = useRender()

  useEffect(() => {
    return archive.registerElement(path, render)
  }, [path, render, archive])

  const change = useCallback(
    (action: 'upsert' | 'remove', value: T, index?: number) => {
      archive.submitElementChange(path, action, value, index)
    },
    [path, archive],
  )

  const resolveRef = useCallback(
    (ref: string) => archive.resolveRef(ref),
    [archive],
  )

  const getDefinition = useCallback(() => {
    return archive.getDefinition(path)?.[1] as D | undefined
  }, [path, archive])

  return {
    ...propertyState,
    change,
    path,
    getDefinition,
    resolveRef,
  }
}
