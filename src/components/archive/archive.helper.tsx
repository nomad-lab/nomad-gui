import {useEffect, useState} from 'react'
import {vi} from 'vitest'

import {UseDataResult} from '../../hooks/useData'
import {ArchiveChange} from '../../models/entryEditRequestModels'
import {MDefResponse, MSectionResponse} from '../../models/graphResponseModels'
import {Quantity, Section, SubSection} from '../../utils/metainfo'
import {DefaultToObject} from '../../utils/types'
import * as useDataForRoute from '../routing/useDataForRoute'
import * as useRouteData from '../routing/useRouteData'
import {Archive, allArchives} from './useArchive'

export type MockArchiveArgs = {
  entryId?: string
  data?: MSectionResponse
  changes?: ArchiveChange[]
}

/**
 * A mock that clears all existing archives and creates a new one
 * for the given `entryId`. NOTE: this will replace the `useRouteData`
 * implementation.
 */
export function mockArchive({
  entryId = 'entryId',
  data,
  changes = [],
}: MockArchiveArgs = {}) {
  allArchives.clear()
  const archive = new Archive()
  if (data) {
    archive.archive = data
    archive.loading = false
  }
  allArchives.set(entryId, archive)
  archive.changeStack.push(...changes)
  vi.spyOn(useRouteData, 'default').mockReturnValue({
    entry_id: entryId,
  })
  return archive
}

/**
 * A mock for the `useDataForRoute` hook that returns the given `mockedData`.
 * It mimicks the one render with `loading` and one with the new data behavior.
 * The `withLoading` parameter can be used to skip the second step and
 * make it always return without data and `loading` set to `true`.
 */
export function mockDataForRoute<Response>(
  mockedData: Response,
  withLoading: boolean = false,
) {
  function useDataForRouteMocked({
    onFetch,
  }: useDataForRoute.UseDataForRouteParams<
    unknown,
    Response
  >): UseDataResult<Response> {
    const [data, setData] = useState<Response | undefined>(undefined)
    useEffect(() => {
      if (withLoading) {
        return
      }
      onFetch?.(mockedData as DefaultToObject<Response>, {})
      setData(mockedData)
    }, [setData, onFetch])
    return {
      data: data as DefaultToObject<Response>,
      loading: data === undefined,
      fetch: vi.fn(),
    }
  }

  vi.spyOn(useDataForRoute, 'default').mockImplementation(
    useDataForRouteMocked as (
      args: useDataForRoute.UseDataForRouteParams,
    ) => UseDataResult<unknown>,
  )
}

/**
 * Recursively adds `m_def`s to the given response. Based on the data
 * in the response it will guess the correct definitions.
 * NOTE: this only works if all quantities are strings or numbers.
 *
 * The `extra` parameter can be used to assume values (values) at
 * given paths (keys). This is useful, if you your response does not
 * contain a value, but the definition is still needed.
 */
export function addDefinitions(
  data: MSectionResponse,
  extra: {[key: string]: unknown} = {},
) {
  const visit = (data: MSectionResponse, path: string) => {
    const all_sub_sections: {[key: string]: SubSection} = {}
    const all_quantities: {[key: string]: Quantity} = {}

    const entries = Object.entries(data)

    for (const [key, value] of Object.entries(extra)) {
      if (
        key.match(/^(.+)\/\w+$/)?.[1] === path ||
        key.match(/^()\w+$/)?.[1] === path
      ) {
        entries.push([key.match(/(\w+)$/)?.[1] as string, value])
      }
    }

    for (const [key, value] of entries) {
      if (['m_request', 'm_def'].includes(key)) {
        continue
      }

      const childPath = path ? `${path}/${key}` : key

      if (Array.isArray(value)) {
        if (value.length === 0 || typeof value[0] === 'object') {
          // repeating sub section
          const sectionDefs = value.map((item, index) =>
            visit(item, `${childPath}/${index}`),
          )
          all_sub_sections[key] = {
            m_def: 'SubSection',
            name: key,
            repeats: true,
            sub_section: sectionDefs[0] || 'some-ref',
          } satisfies SubSection
        } else {
          // list quantity
          all_quantities[key] = {
            m_def: 'Quantity',
            name: key,
            type: {
              type_kind: 'python',
              type_data: typeof value[0] === 'string' ? 'str' : 'float',
            },
            shape: ['*'],
          } satisfies Quantity
        }
      } else if (typeof value === 'object') {
        // non repeating sub section
        const sectionDef = visit(value as MSectionResponse, childPath)
        all_sub_sections[key] = {
          m_def: 'SubSection',
          name: key,
          repeats: false,
          sub_section: sectionDef,
        } satisfies SubSection
      } else {
        // quantity
        all_quantities[key] = {
          m_def: 'Quantity',
          name: key,
          type: {
            type_kind: 'python',
            type_data: typeof value === 'string' ? 'str' : 'float',
          },
          shape: [],
        } satisfies Quantity
      }
    }

    const sectionDef = {
      name: 'Unknown',
      m_ref: 'some-ref',
      m_raw: {},
      all_properties: {
        ...all_quantities,
        ...all_sub_sections,
      },
      all_sub_sections,
      all_quantities,
      sub_sections: Object.values(all_sub_sections),
      quantities: Object.values(all_quantities),
      base_sections: [],
      all_base_sections: [],
      m_annotations: {},
      m_resolved: true,
    } satisfies Section

    data.m_def = sectionDef as MDefResponse

    return sectionDef
  }

  visit(data, '')

  return data
}

/**
 * Recursively removes `m_def`s from the given response. This is useful
 * if you want to compare responses with manually written expected responses.
 */
export function removeDefinitions(data: MSectionResponse) {
  delete data.m_def
  for (const value of Object.values(data)) {
    if (Array.isArray(value)) {
      value.forEach((item) => removeDefinitions(item))
    } else if (typeof value === 'object') {
      removeDefinitions(value as MSectionResponse)
    }
  }

  return data
}
