import {Button} from '@mui/material'
import {useAsyncFn} from 'react-use'

import useAuth from '../../hooks/useAuth'
import {MSectionResponse} from '../../models/graphResponseModels'
import entryRoute from '../../pages/entry/entryRoute'
import {archiveEditApi} from '../../utils/api'
import {assert} from '../../utils/utils'
import useRoute from '../routing/useRoute'
import useRouteData from '../routing/useRouteData'
import {useArchiveChanges} from './useArchive'

export default function EditStatus() {
  const {user} = useAuth()
  const {reload} = useRoute()
  const {entry_id} = useRouteData(entryRoute)
  const {changeStack, clearChangeStack} = useArchiveChanges()

  const [saving, save] = useAsyncFn(async () => {
    const apiChanges = changeStack.map((change) => {
      const apiChange = {
        ...change,
      }
      const newValue = change.new_value as MSectionResponse

      if (newValue?.m_def) {
        apiChange.new_value = {
          ...newValue,
        }
        delete (apiChange.new_value as MSectionResponse)['m_def']
      }

      return apiChange
    })

    assert(user, 'User must be defined at this point')
    await archiveEditApi(entry_id as string, apiChanges, user)
    clearChangeStack()
    reload()
  }, [changeStack, clearChangeStack, reload])

  return (
    <Button
      onClick={save}
      variant='contained'
      disabled={changeStack.length === 0 || saving.loading}
    >
      {saving.loading
        ? 'saving ...'
        : changeStack.length === 0
        ? 'no changes to save'
        : `save changes`}
    </Button>
  )
}
