import type {Meta, StoryObj} from '@storybook/react'

import Visibility from './Visibility'

const meta = {
  title: 'components/Visibility',
  component: Visibility,
  tags: ['autodocs'],
} satisfies Meta<typeof Visibility>

export default meta
type Story = StoryObj<typeof meta>

export const Published: Story = {
  args: {
    upload: {
      published: true,
    },
  },
}

export const Shared: Story = {
  args: {
    upload: {
      published: false,
      writers: [{}, {}], // me and someone else
    },
  },
}

export const Private: Story = {
  args: {
    upload: {
      published: false,
    },
  },
}
