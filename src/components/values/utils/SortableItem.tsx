import DragHandleIcon from '@mui/icons-material/DragHandle'
import {Box} from '@mui/material'
import {PropsWithChildren, useRef} from 'react'
import {useDrag, useDrop} from 'react-dnd'

export type SortableItemProps = {
  index: number
  onHover: (draggedIndex: number, droppedIndex: number) => void
  onDropped: (draggedIndex: number, droppedIndex: number) => void
  onCanceled: (draggedIndex: number) => void
} & PropsWithChildren

export default function SortableItem({
  index,
  onHover,
  onCanceled,
  onDropped,
  children,
}: SortableItemProps) {
  const dragRef = useRef<HTMLDivElement>(null)
  const dropRef = useRef<HTMLDivElement>(null)

  const [, drag, preview] = useDrag(
    () => ({
      type: 'sortable',
      item: () => ({index}),
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
      }),
      end: ({index: droppedIndex}, monitor) => {
        const didDrop = monitor.didDrop()
        if (!didDrop) {
          onCanceled(droppedIndex)
        } else {
          onDropped(droppedIndex, index)
        }
      },
    }),
    [index, onCanceled, onDropped],
  )

  const [{isOver}, drop] = useDrop(
    () => ({
      accept: 'sortable',
      hover: ({index: draggedIndex}: {index: number}) => {
        onHover(draggedIndex, index)
      },
      collect: (monitor) => ({
        isOver: monitor.isOver({shallow: true}),
      }),
    }),
    [index, onHover],
  )

  drag(dragRef)
  drop(preview(dropRef))

  return (
    <Box
      ref={dropRef}
      sx={{
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        opacity: isOver ? 0 : 1,
      }}
    >
      <Box
        ref={dragRef}
        sx={{
          cursor: 'move',
          color: 'action.active',
          display: 'flex',
          alignItems: 'baseline',
          marginRight: 1,
        }}
      >
        <DragHandleIcon />
      </Box>
      {children}
    </Box>
  )
}
