import {Box, Tooltip, Typography, TypographyProps} from '@mui/material'
import {useLayoutEffect, useRef, useState} from 'react'
import {useWindowSize} from 'react-use'

import {assert} from '../../../utils/utils'

export type DisplayStringProps = {
  /**
   * Determines where the ellipsis should be placed.
   */
  ellipsis?: 'left' | 'right'

  /**
   * A placeholder to show when there is only an empty string.
   */
  placeholder?: string

  /**
   * The optional tooltip. By default, it shows the full string if
   * the component is overflowing.
   */
  tooltip?: string

  /**
   * The optional end adornment renders at the end of the string.
   */
  endAdornment?: React.ReactNode

  /**
   * The optional href if component is a respective Link component
   */
  href?: string

  /**
   * The optional to if component is a respective Link component
   */
  to?: string
} & Pick<TypographyProps, 'variant' | 'sx' | 'children' | 'color' | 'component'>

/**
 * A allows to display a simple unwrapped string with ellipsis and tooltip.
 */
export default function DisplayString({
  ellipsis = 'right',
  placeholder = '',
  tooltip,
  sx = {},
  endAdornment,
  children,
  ...typographyProps
}: DisplayStringProps) {
  const typographyRef = useRef<HTMLElement>(null)

  const [isOverflowing, setIsOverflowing] = useState(false)
  const windowSize = useWindowSize()

  useLayoutEffect(() => {
    const el = typographyRef.current
    assert(el !== null, 'Typography ref has to be set')

    setIsOverflowing(
      el.clientWidth < el.scrollWidth || el.clientHeight < el.scrollHeight,
    )
  }, [windowSize.width, windowSize.height, setIsOverflowing, typographyRef])

  const showPlaceholder = children === '' || children === undefined

  const typographyElement = (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        gap: 1,
        alignItems: 'center',
        width: '100%',
        ...sx,
      }}
    >
      <Typography
        ref={typographyRef}
        noWrap
        {...typographyProps}
        sx={{
          ...(showPlaceholder && {color: 'text.disabled'}),
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          ...(ellipsis === 'left' && {
            direction: 'rtl',
            textAlign: 'left',
            '&::before': {
              content: '"‎"',
            },
          }),
        }}
      >
        {showPlaceholder ? placeholder : children}
      </Typography>
      {endAdornment && (
        <Box sx={{display: 'flex', flexDirection: 'row', flexWrap: 'nowrap'}}>
          {endAdornment}
        </Box>
      )}
    </Box>
  )

  if (tooltip || isOverflowing) {
    return <Tooltip title={tooltip || children}>{typographyElement}</Tooltip>
  } else {
    return typographyElement
  }
}
