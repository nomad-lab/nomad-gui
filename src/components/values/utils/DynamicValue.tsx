import {useMemo} from 'react'

import ContainerProps from '../containers/ContainerProps'
import {
  Containers,
  DynamicComponentSpec,
  createElement,
  mergeComponentSpecs,
  normalizeComponentSpec,
} from './dynamicComponents'

/**
 * A *value* instance based on a JSON serializable dynamic
 * component spec. Such dynamic component specs are used in schemas
 * to describe how a quantity value should be displayed and edited.
 *
 * Here is an example:
 * ```json
 * {
 *  "component": {
 *    "Value": {
 *      "valueComponent": {
 *        "Text": {
 *          "label": "Name",
 *          "actions": {
 *            "CopyToClipboard": {}
 *          }
 *        }
 *      }
 *    }
 *  }
 *  "editable": true,
 *  "value": "John Doe"
 * }
 * ```
 *
 * There are short hand notations allowing to reduce verbosity. More specifically
 * one can leave out the default `value` container and use strings for
 * components without any props.
 *
 * Here is the same example:
 * ```json
 * {
 *  "component": {
 *    "Text": {
 *      "label": "Name",
 *      "actions": "CopyToClipboard"
 *    }
 *  }
 *  "editable": true,
 *  "value": "John Doe"
 * }
 * ```
 *
 * During the creation process, this will check keys that denote components.
 * However, it will not validate
 * any props.
 */
export default function DynamicValue<Value = unknown>({
  component,
  defaultComponent,
  ...containerProps
}: {
  component: DynamicComponentSpec
  defaultComponent?: Containers
} & ContainerProps<Value>) {
  const normalizedComponent = useMemo(() => {
    return normalizeComponentSpec(component)
  }, [component])

  return createElement(
    mergeComponentSpecs(normalizedComponent, defaultComponent),
    containerProps,
  )
}
