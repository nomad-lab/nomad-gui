import {Box, IconButton} from '@mui/material'
import {Unit as MathjsUnit} from 'mathjs'
import {useCallback, useMemo} from 'react'

import {assert} from '../../../utils/utils'
import {Linked, Unlinked} from '../../icons'
import UnitSelect, {UnitSelectProps} from '../../units/UnitSelect'
import * as units from '../../units/units'
import ContainerProps from '../containers/ContainerProps'
import useProps from './useProps'

export type UnitProps = {
  variant?: 'edit' | 'standard'
  unitSelectProps?: Partial<UnitSelectProps>
} & ContainerProps<unknown>

/**
 * A unit select action for numeral values.
 */
export default function Unit({variant = 'edit', ...props}: UnitProps) {
  const {
    label,
    error,
    onError,
    displayUnitLocked,
    onDisplayUnitLockedChange,
    displayUnit,
    onDisplayUnitChange,
    onBlur,
    onFocus,
    unitSelectProps,
  } = useProps(props)

  const unitValue = useMemo(() => {
    assert(!!displayUnit, 'unit or displayUnit must be defined to use Unit')
    return units.unit(displayUnit)
  }, [displayUnit])

  const autocompleteProps = useMemo(
    () => ({
      onBlur,
      onFocus,
      sx: {
        width: 80,
      },
    }),
    [onBlur, onFocus],
  )

  const handleChange = useCallback(
    (unit: MathjsUnit) => {
      onDisplayUnitChange(unit.formatUnits())
    },
    [onDisplayUnitChange],
  )

  const handleLockedClicked = useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => {
      event.stopPropagation()
      onDisplayUnitLockedChange?.(!displayUnitLocked)
    },
    [displayUnitLocked, onDisplayUnitLockedChange],
  )

  const lockedButton = displayUnitLocked !== undefined && (
    <IconButton size='small' onClick={handleLockedClicked} aria-label='lock'>
      {displayUnitLocked ? <Linked /> : <Unlinked />}
    </IconButton>
  )

  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        flexWrap: 'nowrap',
      }}
    >
      {lockedButton}
      <UnitSelect
        textFieldProps={{
          size: 'small',
          margin: 'none',
          label: label && 'unit',
          hiddenLabel: !label,
          variant: variant === 'edit' ? 'filled' : 'standard',
        }}
        {...unitSelectProps}
        unit={unitValue}
        onChange={handleChange}
        error={error}
        onError={onError}
        autocompleteProps={autocompleteProps}
      />
    </Box>
  )
}
