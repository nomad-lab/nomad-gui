import {act, fireEvent, render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {vi} from 'vitest'

import ContainerProps from '../containers/ContainerProps'
import {TestContainer} from '../containers/testComponents.helper'
import Numeral from '../primitives/Numeral'

describe('Unit', () => {
  const handleChange = vi.fn()
  const handleError = vi.fn()
  const handleDisplayUnitChange = vi.fn()

  function TestComponent(props: ContainerProps<number>) {
    const {displayUnitLocked} = props
    return (
      <TestContainer
        {...props}
        value={1}
        editable
        onDisplayUnitChange={handleDisplayUnitChange}
        displayUnitLocked={displayUnitLocked}
        onError={handleError}
        unit='m'
        onChange={handleChange}
      >
        <Numeral />
      </TestContainer>
    )
  }

  beforeEach(() => {
    handleChange.mockClear()
    handleError.mockClear()
    handleDisplayUnitChange.mockClear()
  })

  it.each([
    ['unit and no displayUnit', 'm', undefined, '1'],
    ['unit and the same displayUnit', 'm', 'm', '1'],
    ['unit and a different displayUnit', 'm', 'cm', '100'],
  ])('displays with %s', (description, unit, displayUnit, result) => {
    // non editable
    render(<Numeral value={1} unit={unit} displayUnit={displayUnit} />)
    expect(screen.getByText(result)).toBeInTheDocument()

    // editable
    render(<Numeral value={1} unit={unit} displayUnit={displayUnit} editable />)
    // trigger a validation to that nothing changes
    const inputElement = screen.getByRole('textbox')
    act(() => fireEvent.blur(inputElement))
    expect(screen.getByDisplayValue(result)).toBeInTheDocument()
  })

  it.each([
    ['no unit', '', true, '1', 1],
    ['the same unit and unit locked', 'm', true, '1', 1],
    ['another unit and unit locked', 'cm', true, '0.01', 0.01],
    ['the same unit and unit unlocked', 'm', false, '1', 1],
    ['another unit and unit unlocked', 'cm', false, '1', 0.01],
    ['a wrong unit', 'xx', true, '1xx', 'error'],
  ])(
    'allows to type a unit with %s',
    async (
      description,
      input,
      displayUnitLocked,
      resultDisplayValue,
      resultValue,
    ) => {
      render(<TestComponent displayUnitLocked={displayUnitLocked} />)
      const inputElement = screen.getByDisplayValue(1) as HTMLInputElement
      await userEvent.type(inputElement, `${input}{enter}`)
      expect(screen.getByDisplayValue(resultDisplayValue)).toBeInTheDocument()
      if (resultValue === 'error') {
        expect(handleError).toHaveBeenLastCalledWith('Unit "xx" not found.')
        expect(handleChange).not.toHaveBeenCalled()
      } else if (input === 'cm') {
        expect(handleError).not.toHaveBeenCalled()
        expect(handleChange).toHaveBeenLastCalledWith(resultValue)
      } else {
        expect(handleError).not.toHaveBeenCalled()
        expect(handleChange).not.toHaveBeenCalled()
      }

      if (!displayUnitLocked && input !== 'm') {
        expect(handleDisplayUnitChange).toHaveBeenLastCalledWith(input)
      } else {
        expect(handleDisplayUnitChange).not.toHaveBeenCalled()
      }
    },
  )
  it.each([
    ['locked', 'cm', true, '100', 1],
    ['unlocked', 'cm', false, '1', 0.01],
    ['wrong unit', 'xx', true, '1', 'error'],
  ])(
    'allows to change the display unit with unit %s',
    async (
      description,
      input,
      displayUnitLocked,
      resultDisplayValue,
      resultValue,
    ) => {
      render(<TestComponent displayUnitLocked={displayUnitLocked} />)
      const inputElement = screen.getByDisplayValue('m') as HTMLInputElement
      await userEvent.type(inputElement, `{backspace}${input}{enter}`)
      expect(screen.getByDisplayValue(resultDisplayValue)).toBeInTheDocument()
      if (resultValue === 1) {
        expect(handleError).not.toHaveBeenCalled()
        expect(handleChange).not.toHaveBeenCalled()
      } else if (resultValue === 'error') {
        expect(handleError).toHaveBeenLastCalledWith('Unit "xx" not found.')
        expect(handleChange).not.toHaveBeenCalled()
      } else {
        expect(handleError).not.toHaveBeenCalled()
        expect(handleChange).toHaveBeenLastCalledWith(resultValue)
      }
    },
  )
  it('allows to change display unit locked', () => {
    const handleDisplayUnitLockedChange = vi.fn()
    render(
      <TestComponent
        onDisplayUnitLockedChange={handleDisplayUnitLockedChange}
      />,
    )
    const lockButton = screen.getByRole('button', {name: 'lock'})
    expect(lockButton).toBeInTheDocument()
    expect(handleDisplayUnitLockedChange).not.toHaveBeenCalled()
    act(() => fireEvent.click(lockButton))
    expect(handleDisplayUnitLockedChange).toHaveBeenLastCalledWith(false)
  })
})
