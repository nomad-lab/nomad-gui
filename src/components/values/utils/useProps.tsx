import {createContext, useContext} from 'react'

export const propsContext = createContext<unknown>({})

/**
 * Allows to access props from a `PropsProvider`. It takes props as an
 * argument and returns all props from the argument mixed with props
 * from the `PropsProvider`. The argument props are prioritized.
 */
export default function useProps<PropsType = unknown>(
  props?: Partial<PropsType>,
): PropsType {
  const contextProps = useContext(propsContext) as PropsType
  return {
    ...contextProps,
    ...props,
  }
}
