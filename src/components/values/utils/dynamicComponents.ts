import _ from 'lodash'
import React from 'react'

import {Quantity, Type, metainfoDataTypePackage} from '../../../utils/metainfo'
import {JSONObject, JSONValue} from '../../../utils/types'
import {assert} from '../../../utils/utils'
import {DynamicActionProps} from '../actions/Action'
import CopyToClipboard, {
  DynamicCopyToClipboardProps,
} from '../actions/CopyToClipboard'
import EditToggle from '../actions/EditToggle'
import Help, {DynamicHelpProps} from '../actions/Help'
import ContainerProps from '../containers/ContainerProps'
import List, {DynamicListProps} from '../containers/List'
import Matrix, {DynamicMatrixProps} from '../containers/Matrix'
import Value, {DynamicValueProps} from '../containers/Value'
import Boolean, {DynamicBooleanProps} from '../primitives/Boolean'
import Datetime, {DynamicDatetimeProps} from '../primitives/Datetime'
import EnumRadio, {DynamicEnumProps} from '../primitives/EnumRadio'
import EnumSelect, {DynamicEnumSelectProps} from '../primitives/EnumSelect'
import FileReference, {
  DynamicFileReferenceProps,
} from '../primitives/FileReference'
import Id, {DynamicIdProps} from '../primitives/Id'
import JsonData, {DynamicJsonDataProps} from '../primitives/JsonData'
import Link, {DynamicLinkProps} from '../primitives/Link'
import Numeral, {DynamicNumeralProps} from '../primitives/Numeral'
import RelativeTime, {
  DynamicRelativeTimeProps,
} from '../primitives/RelativeTime'
import SectionReference, {
  DynamicSectionReferenceProps,
} from '../primitives/SectionReference'
import Text, {DynamicTextProps} from '../primitives/Text'

function getPrimitiveOrPrimitiveKey(
  type: Quantity['type'],
): Primitives | PrimitiveKey {
  if (type.type_kind === 'python') {
    if (type.type_data === 'str') {
      return 'Text'
    }
    if (type.type_data === 'int') {
      return 'Numeral'
    }
    if (type.type_data === 'float') {
      return 'Numeral'
    }
    if (type.type_data === 'bool') {
      return 'Boolean'
    }
    throw new Error(`Unknown quantity python type ${type.type_data}`)
  }
  if (type.type_kind === 'numpy') {
    return 'Numeral'
  }
  if (type.type_kind === 'custom') {
    if (type.type_data === `${metainfoDataTypePackage}.Datetime`) {
      return {
        Datetime: {
          variant: 'datetime',
        },
      }
    }
    if (type.type_data === `${metainfoDataTypePackage}.URL`) {
      return 'Link'
    }
    if (type.type_data === `${metainfoDataTypePackage}.File`) {
      return 'FileReference'
    }
    if (type.type_data === `${metainfoDataTypePackage}.Capitalized`) {
      // TODO
      return 'Text'
    }
    if (type.type_data === `${metainfoDataTypePackage}.Bytes`) {
      // TODO
      return 'Text'
    }
    if (type.type_data === `${metainfoDataTypePackage}.JSON`) {
      return 'JsonData'
    }
    if (type.type_data === `${metainfoDataTypePackage}.Unit`) {
      // TODO
      return 'Text'
    }
    if (type.type_data === `${metainfoDataTypePackage}.Dimension`) {
      // TODO
      return 'Text'
    }
    throw new Error(`Unknown quantity custom type ${type.type_data}`)
  }
  if (type.type_kind === 'enum') {
    return type.type_data.length <= 8
      ? {
          EnumRadio: {
            row: type.type_data.length <= 3,
            values: type.type_data,
          },
        }
      : {
          EnumSelect: {
            values: type.type_data,
          },
        }
  }
  if (type.type_kind === 'reference') {
    return {
      SectionReference: {
        sectionDefinition: type.type_data.m_ref,
      },
    }
  }

  throw new Error(`Unknown quantity type kind ${(type as Type).type_kind}`)
}

function getPrimitive(type: Quantity['type']): Primitives {
  const primitive = getPrimitiveOrPrimitiveKey(type)
  if (typeof primitive === 'string') {
    return {
      [primitive]: {},
    }
  }
  return primitive
}

function getContainer(quantity: Quantity): Containers {
  const {type, shape = []} = quantity
  const primitive = getPrimitive(type)

  const isNumpy = type.type_kind === 'numpy'
  const forceMatrix = (isNumpy && shape.length >= 1) || shape.length >= 2
  const unitProps = {...(quantity.unit && {unit: quantity.unit})}

  if (forceMatrix) {
    return {
      Matrix: {
        valueComponent: primitive,
        ...unitProps,
      },
    }
  }

  Object.assign(
    primitive[Object.keys(primitive)[0] as PrimitiveKey] as object,
    {
      actions: {
        CopyToClipboard: {},
      },
    },
  )

  if (shape.length >= 1) {
    return {
      List: {
        valueComponent: primitive,
        ...unitProps,
      },
    }
  }
  return {
    Value: {
      valueComponent: primitive,
      ...unitProps,
    },
  }
}

export function createDynamicComponentSpec(quantity: Quantity): Containers {
  return getContainer(quantity)
}

const primitiveComponents = {
  Text,
  Link,
  Numeral,
  Boolean,
  EnumRadio,
  EnumSelect,
  Datetime,
  RelativeTime,
  Id,
  JsonData,
  FileReference,
  SectionReference,
}

export type PrimitiveKey = keyof typeof primitiveComponents

const actionComponents = {
  CopyToClipboard,
  EditToggle,
  Help,
}

export type ActionKey = keyof typeof actionComponents

const containerComponents = {
  Value,
  List,
  Matrix,
}

export type ContainerKey = keyof typeof containerComponents

export type Actions = Partial<{
  CopyToClipboard: DynamicCopyToClipboardProps
  EditToggle: DynamicActionProps
  Help: DynamicHelpProps
}>

export type Primitives = Partial<{
  Text: DynamicTextProps & DynamicValueProps
  Numeral: DynamicNumeralProps & DynamicValueProps
  Boolean: DynamicBooleanProps & DynamicValueProps
  EnumRadio: DynamicEnumProps & DynamicValueProps
  EnumSelect: DynamicEnumSelectProps & DynamicValueProps
  Id: DynamicIdProps & DynamicValueProps
  Datetime: DynamicDatetimeProps & DynamicValueProps
  RelativeTime: DynamicRelativeTimeProps & DynamicValueProps
  Link: DynamicLinkProps & DynamicValueProps
  JsonData: DynamicJsonDataProps & DynamicValueProps
  FileReference: DynamicFileReferenceProps & DynamicValueProps
  SectionReference: DynamicSectionReferenceProps & DynamicValueProps
}>

export type Containers = Partial<{
  Value: DynamicValueProps
  List: DynamicListProps
  Matrix: DynamicMatrixProps
}>

export type DynamicComponentSpec =
  | Containers
  | ContainerKey
  | Primitives
  | PrimitiveKey

export function createElement<Value = unknown>(
  component: Primitives | Containers | Actions | React.ReactNode,
  props: ContainerProps<Value> = {},
  isParent: boolean = false,
): React.ReactNode {
  if (React.isValidElement(component)) {
    return component
  }
  component = component as Primitives | Containers | Actions

  const elements = Object.entries(component).map(
    ([name, {...componentProps}], index) => {
      const component =
        primitiveComponents[name as PrimitiveKey] ||
        actionComponents[name as ActionKey] ||
        containerComponents[name as ContainerKey]
      const {actions, labelActions} = componentProps

      if (actions !== undefined) {
        componentProps.actions = createElement(actions)
      }

      if (labelActions !== undefined) {
        componentProps.labelActions = createElement(labelActions)
      }

      const isContainer = !!containerComponents[name as ContainerKey]
      let finalProps
      if (isContainer) {
        const {
          emptyValue,
          valueComponent = {
            Text: {},
          },
          ...currentProps
        } = componentProps
        finalProps = {
          key: index,
          ...currentProps,
          ...props,
          ...(emptyValue !== undefined
            ? {createEmptyValue: () => emptyValue}
            : {}),
          renderValue: () => createElement(valueComponent),
        }
      } else {
        finalProps = {
          key: index,
          ...(isParent ? props : {}),
          ...componentProps,
        }
      }

      return React.createElement(
        component as React.ComponentType<typeof finalProps>,
        finalProps,
      )
    },
  )

  return elements
}

/**
 * Normalizes a dynamic component spec. More specifically this replaces
 * `string` and `string[]` short hand notation for the `componentSpec` itself
 * and children keys `valueComponent`, `labelActions`, and `actions`.
 *
 */
export function normalizeComponentSpec(
  componentSpec: DynamicComponentSpec,
): Containers {
  function normalizeActionsComponentSpec(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    componentSpec: any,
  ): Actions | undefined {
    if (typeof componentSpec === 'string') {
      assert(
        actionComponents[componentSpec as keyof Actions] !== undefined,
        `Unknown component: ${componentSpec}`,
      )
      return {
        [componentSpec]: {},
      }
    }

    if (Array.isArray(componentSpec)) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      return componentSpec.reduce<any>((actions, actionKey) => {
        assert(typeof actionKey === 'string', 'Action must be a string.')
        assert(
          actionComponents[actionKey as keyof Actions],
          `Unknown component: ${actionKey}`,
        )
        actions[actionKey as ActionKey] = {}
        return actions
      }, {})
    }

    // Usually we only support dynamic components here. This is an exception
    // for some build-in components using react elements as actions so that
    // we do not have to wrap exotic components into dynamic components.
    if (React.isValidElement(componentSpec)) {
      return componentSpec as Actions
    }

    assert(
      _.isPlainObject(componentSpec),
      'JSON component must be string or an object.',
    )
    Object.keys(componentSpec).forEach((componentKey) => {
      assert(
        actionComponents[componentKey as keyof Actions] !== undefined,
        `Unknown component: ${componentKey}`,
      )
      assert(
        _.isPlainObject(componentSpec[componentKey]),
        'Component must be an object.',
      )
    })
    return componentSpec as Actions
  }

  function normalizePrimitiveComponentSpec(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    componentSpec: any,
  ): Primitives {
    assert(componentSpec !== undefined, 'A value component must be specified.')
    if (typeof componentSpec === 'string') {
      assert(
        primitiveComponents[componentSpec as keyof Primitives] !== undefined,
        `Unknown component: ${componentSpec}`,
      )
      return {
        [componentSpec]: {},
      }
    }

    assert(
      _.isPlainObject(componentSpec),
      'JSON component must be string or an object.',
    )
    assert(
      Object.keys(componentSpec).length === 1,
      'JSON component must have exactly one key.',
    )
    const componentKey = Object.keys(componentSpec)[0] as keyof Primitives

    assert(
      primitiveComponents[componentKey] !== undefined,
      `Unknown component: ${componentKey}`,
    )
    assert(
      _.isPlainObject(componentSpec[componentKey]),
      'Component must be an object.',
    )
    const componentValue = componentSpec[componentKey]

    const component = {...componentValue} as Primitives[keyof Primitives]
    assert(component, 'Typescript assertion')
    if (componentValue.actions) {
      component.actions = normalizeActionsComponentSpec(componentValue.actions)
    }
    if (componentValue.labelActions) {
      component.labelActions = normalizeActionsComponentSpec(
        componentValue.labelActions,
      )
    }

    return {
      [componentKey]: component,
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  function normalizeContainerComponentSpec(componentSpec: any): Containers {
    if (typeof componentSpec === 'string') {
      const isPrimitive =
        primitiveComponents[componentSpec as PrimitiveKey] !== undefined
      if (isPrimitive) {
        return {
          Value: {
            valueComponent: normalizePrimitiveComponentSpec(componentSpec),
          },
        }
      }

      assert(
        containerComponents[componentSpec as keyof Containers] !== undefined,
        `Unknown component: ${componentSpec}`,
      )
      return {
        Value: {
          valueComponent: {[componentSpec]: {}},
        },
      }
    }
    assert(
      _.isPlainObject(componentSpec),
      'JSON component must be string or an object.',
    )
    componentSpec = componentSpec as Record<string, JSONValue>

    assert(
      Object.keys(componentSpec as object).length === 1,
      'JSON component must have exactly one key.',
    )
    const componentKey = Object.keys(componentSpec)[0] as
      | ContainerKey
      | PrimitiveKey

    const isPrimitive =
      primitiveComponents[componentKey as PrimitiveKey] !== undefined
    if (isPrimitive) {
      return {
        Value: {
          valueComponent: normalizePrimitiveComponentSpec(componentSpec),
        },
      }
    }

    assert(
      containerComponents[componentKey as ContainerKey] !== undefined,
      `Unknown component: ${componentKey}`,
    )
    assert(
      _.isPlainObject(componentSpec[componentKey]),
      'Component must be an object.',
    )
    const componentValue = componentSpec[componentKey] as JSONObject

    const component = {...componentValue} as Containers[keyof Containers]
    assert(component, 'Typescript assertion')
    component.valueComponent = normalizePrimitiveComponentSpec(
      componentValue.valueComponent,
    )
    if (componentValue.labelActions) {
      component.labelActions = normalizeActionsComponentSpec(
        componentValue.labelActions,
      )
    }
    if (componentValue.actions) {
      component.actions = normalizeActionsComponentSpec(componentValue.actions)
    }

    return {
      [componentKey]: component,
    }
  }

  return normalizeContainerComponentSpec(componentSpec)
}

/**
 * Overwrites the first key of the target with the first key of the source.
 * If they match the props are merged giving the source priority.
 */
function mergeFirstKey(
  target: JSONObject,
  source: JSONObject,
  additional?: (target: JSONObject, source: JSONObject) => JSONObject,
) {
  const sourceKey = Object.keys(source)[0]
  const targetKey = Object.keys(target)[0]
  const sourceComponent = source[sourceKey] as JSONObject
  const targetComponent = target[targetKey] as JSONObject

  if (targetKey !== sourceKey) {
    return {
      [sourceKey]: {
        ...sourceComponent,
        ...(targetComponent.actions &&
          !Object.keys(sourceComponent).includes('actions') && {
            actions: targetComponent.actions,
          }),
        ...(additional?.(targetComponent, sourceComponent) ?? {}),
      },
    }
  } else {
    return {
      [targetKey]: {
        ...targetComponent,
        ...sourceComponent,
        ...(additional?.(targetComponent, sourceComponent) ?? {}),
      },
    }
  }
}

function mergePrimitiveComponentSpecs(
  component: Primitives,
  defaultComponent: Primitives,
) {
  // We prioritize the component to allow users to overwrite the default
  // primitive.
  return mergeFirstKey(defaultComponent as JSONObject, component as JSONObject)
}

/**
 * Merges the given `defaultComponent` spec with the `component` spec.
 * This will be used to take a user provided component spec fill it with
 * information from default generated component specs to reduce the need
 * for versbose component specs.
 */
export function mergeComponentSpecs(
  component: Containers,
  defaultComponent: Containers | undefined,
): Containers {
  if (!defaultComponent) {
    return component
  }
  // If they do not match, we assume that the user did not specify a container
  // and use the additional container.
  // TODO what if the user wants to override a List with a Matix?! Ideally we
  // would know what happened during normalization.
  return mergeFirstKey(
    component as JSONObject,
    defaultComponent as JSONObject,
    (target, source) =>
      ({
        valueComponent: mergePrimitiveComponentSpecs(
          target.valueComponent as Primitives,
          source.valueComponent as Primitives,
        ),
      } as unknown as JSONObject),
  )
}
