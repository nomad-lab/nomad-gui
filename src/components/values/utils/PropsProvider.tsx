import {PropsWithChildren} from 'react'

import useProps, {propsContext} from './useProps'

export type PropsProviderProps<Props> = {
  /**
   * If true, propagates props from a parent PropsProvider, otherwise
   * creates a whole new props context that blocks the parent context.
   */
  propagate?: boolean
} & PropsWithChildren &
  Props

/**
 * This is the provider counterpart to `useProps`. It allows to
 * pass props to children without needing to drill down actual props.
 * Any `useProps` in side a `PropsProvider` will get the props.
 */
export default function PropsProvider<Props>({
  propagate = false,
  children,
  ...props
}: PropsProviderProps<Props>) {
  const childProps = useProps(props as Partial<Props>)
  const value = propagate ? childProps : props
  return <propsContext.Provider value={value}>{children}</propsContext.Provider>
}
