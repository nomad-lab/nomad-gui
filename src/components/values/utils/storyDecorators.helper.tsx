import {Box, Typography} from '@mui/material'
import {Parameters, StoryFn} from '@storybook/react'
import _ from 'lodash'
import React, {useMemo, useState} from 'react'
import {useAsync} from 'react-use'

import uploadsRoute from '../../../pages/uploads/uploadsRoute'
import {assert} from '../../../utils/utils'
import MemoryRouter from '../../routing/MemoryRouter'
import loader from '../../routing/loader'
import useRoute from '../../routing/useRoute'
import ContainerProps from '../containers/ContainerProps'
import PropsProvider from './PropsProvider'

export function baseDecorator(Story: StoryFn) {
  return (
    <Box
      sx={{
        width: 400,
        margin: 1,
        padding: 1,
        border: 1,
        borderColor: 'grey.300',
        display: 'flex',
        position: 'relative',
      }}
    >
      <Story />
    </Box>
  )
}

/**
 * Provides state and controls value container props like value, display unit,
 * and error.
 */
export function editorDecorator<Value>(
  Story: StoryFn,
  {
    args,
    parameters,
  }: {
    args: ContainerProps<Value>
    parameters: Parameters
  },
) {
  const {
    value: parameterValue,
    render = (props: ContainerProps, story: React.ReactNode) => (
      <PropsProvider {...props}>{story}</PropsProvider>
    ),
  } = parameters
  const {
    value: storyValue,
    onChange,
    onError,
    onBlur,
    onFocus,
    onDisplayUnitChange,
    onDisplayUnitLockedChange,
    ...storyArgs
  } = args
  function Component() {
    const [value, setValue] = useState(parameterValue || storyValue)
    const story = useMemo(() => <Story args={storyArgs} />, [])
    return (
      <>
        <Box sx={{position: 'absolute', left: 432, width: 200}}>
          <Typography>{value?.toString() || ''}</Typography>
        </Box>
        {render(
          {
            value,
            onChange: setValue,
          },
          story,
        )}
      </>
    )
  }
  return <Component />
}

/**
 * Creates a decorator that renders the story as the component of a route
 * with the given uploadId and entryId. Uses a `MemoryRouter` to run
 * the whole app based on the synthetic API. The given ids have to exist
 * in the synthetic API data.
 *
 * This is useful for stories that require the routing to be setup.
 */
export function createRouteDecorator(uploadId: string, entryId: string) {
  return function Story(Story: StoryFn) {
    import.meta.env.VITE_USE_MOCKED_API_LATENCY_MS = 0

    const state = useAsync(async () => {
      const {httpWorker} = await import('../../../mocks/httpWorker')
      await httpWorker.start({
        onUnhandledRequest: () => {},
      })
    }, [])

    function Component() {
      const {isLoading} = useRoute()
      if (isLoading) {
        return <Typography>loading...</Typography>
      }
      return <Story />
    }

    const rootRoute = {
      path: '',
      children: [_.cloneDeep(uploadsRoute)],
    }
    const entryRoute = rootRoute?.children
      ?.find((child) => child.path === 'uploads')
      ?.children?.find((child) => child.path === ':uploadId')
      ?.children?.find((child) => child.path === 'entries')
      ?.children?.find((child) => child.path === ':entryId')
    assert(entryRoute !== undefined, 'entryRoute not found')
    entryRoute.component = Component

    if (state.loading) {
      return <Typography>loading...</Typography>
    }

    return (
      <MemoryRouter
        route={rootRoute}
        loader={loader}
        url={`/uploads/${uploadId}/entries/${entryId}`}
      />
    )
  }
}

export default {}
