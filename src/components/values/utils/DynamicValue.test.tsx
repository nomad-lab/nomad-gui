import {act, fireEvent, render, screen} from '@testing-library/react'

import {TestContainer} from '../containers/testComponents.helper'
import DynamicValue from './DynamicValue'
import {
  ActionKey,
  Actions,
  Containers,
  PrimitiveKey,
  Primitives,
} from './dynamicComponents'

describe('DynamicValue', () => {
  it('renders from primitive key', () => {
    render(<DynamicValue component='Text' value='text' />)
    expect(screen.getByText('text')).toBeInTheDocument()
  })
  it('renders from primitive dynamic component', () => {
    render(
      <DynamicValue
        component={{
          Text: {
            actions: 'CopyToClipboard',
          },
        }}
        value='Text'
        editable
      />,
    )
    expect(screen.getByDisplayValue('Text')).toBeInTheDocument()
    expect(screen.getByRole('button')).toBeInTheDocument()
  })
  it('renders from container key', () => {
    render(<DynamicValue component='List' value={['a', 'b']} />)
    expect(screen.getByText('a')).toBeInTheDocument()
    expect(screen.getByText('b')).toBeInTheDocument()
  })
  it('renders from container dynamic component with value key', () => {
    render(
      <DynamicValue
        component={{
          List: {
            valueComponent: 'Numeral',
          },
        }}
        value={[1, 101102]}
      />,
    )
    expect(screen.getByText('1')).toBeInTheDocument()
    expect(screen.getByText('101,102')).toBeInTheDocument()
  })
  it('renders from container dynamic component with dynamic component value', () => {
    render(
      <DynamicValue
        component={{
          List: {
            valueComponent: {
              Numeral: {
                options: {
                  notation: 'scientific',
                },
              },
            },
          },
        }}
        value={[1, 101102]}
      />,
    )
    expect(screen.getByText('1E0')).toBeInTheDocument()
    expect(screen.getByText('1.011E5')).toBeInTheDocument()
  })
  it.each([
    ['with empty value', true],
    ['without empty value', false],
  ])('renders list %s', (description, provideEmptyValue) => {
    const element = (
      <DynamicValue
        component={{
          List: {
            valueComponent: 'Numeral',
            emptyValue: provideEmptyValue ? 0 : undefined,
          },
        }}
        editable
      />
    )
    render(<TestContainer value={[1, 2]}>{element}</TestContainer>)
    expect(screen.getByDisplayValue('1')).toBeInTheDocument()
    expect(screen.getByDisplayValue('2')).toBeInTheDocument()
    const addButton = screen.getByRole('button', {name: 'add item'})
    expect(addButton).toBeInTheDocument()
    act(() => fireEvent.click(addButton))
    if (provideEmptyValue) {
      expect(screen.getByDisplayValue('0')).toBeInTheDocument()
    }
  })
  it.each([
    ['from string', 'CopyToClipboard'],
    ['from list', ['CopyToClipboard']],
    ['from object', {CopyToClipboard: {}}],
  ])('renders actions %s', (description, actions) => {
    render(
      <DynamicValue
        component={{
          Text: {
            actions: actions as Actions | ActionKey | ActionKey[],
          },
        }}
        value='value'
        editable
      />,
    )
    expect(screen.getByDisplayValue('value')).toBeInTheDocument()
    expect(screen.getByRole('button')).toBeInTheDocument()
  })
  it('renders actions on container', () => {
    render(
      <DynamicValue
        component={{
          Value: {
            actions: 'CopyToClipboard',
            valueComponent: 'Text',
          },
        }}
        value='value'
        editable
      />,
    )
    expect(screen.getByDisplayValue('value')).toBeInTheDocument()
    expect(screen.getByRole('button')).toBeInTheDocument()
  })
  it.each([
    ['from string', 'CopyToClipboard'],
    ['from list', ['CopyToClipboard']],
    ['from object', {CopyToClipboard: {}}],
  ])('renders label adornments %s', (description, actions) => {
    render(
      <DynamicValue
        component={{
          Value: {
            label: 'label',
            labelActions: actions as Actions | ActionKey | ActionKey[],
            valueComponent: 'Text',
          },
        }}
        value='value'
        editable
      />,
    )
    expect(screen.getByDisplayValue('value')).toBeInTheDocument()
    expect(screen.getByRole('button')).toBeInTheDocument()
  })
  it.each([
    ['plain primitive', 'Text' as PrimitiveKey],
    ['primitive', {Text: {}} as Primitives],
    ['different primitive', {Link: {}} as Primitives],
    ['primitive with props', {Text: {displayValue: 'text'}} as Primitives],
    ['container', {Value: {valueComponent: {Text: {}}}} as Containers],
    ['different container', {List: {valueComponent: {Text: {}}}} as Containers],
  ])('merges with default component spec and %s', (description, component) => {
    render(
      <DynamicValue
        defaultComponent={{
          Value: {
            valueComponent: {
              Text: {
                actions: {
                  CopyToClipboard: {},
                },
              },
            },
          },
        }}
        component={component}
        value='value'
        editable
      />,
    )
    expect(screen.getByDisplayValue('value')).toBeInTheDocument()
    expect(screen.getByRole('button', {name: /clipboard/})).toBeInTheDocument()
  })
})
