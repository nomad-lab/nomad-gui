import {
  CustomType,
  PythonType,
  Quantity,
  Section,
  metainfoDataTypePackage,
} from '../../../utils/metainfo'
import {
  ContainerKey,
  Primitives,
  createDynamicComponentSpec,
} from './dynamicComponents'

describe('createDynamicComponentSpec', () => {
  it.each([
    [
      'str',
      {type_kind: 'python', type_data: 'str'} satisfies Quantity['type'],
      'Text',
    ],
    [
      'bool',
      {type_kind: 'python', type_data: 'bool'} satisfies Quantity['type'],
      'Boolean',
    ],
    ...['float', 'int'].map((type) => {
      return [
        type,
        {
          type_kind: 'python',
          type_data: type as PythonType['type_data'],
        } satisfies Quantity['type'],
        'Numeral',
      ] as [string, PythonType, Primitives]
    }),
    [
      'numpy',
      {type_kind: 'numpy', type_data: 'float64'} satisfies Quantity['type'],
      'Numeral',
    ],
    [
      'Datetime',
      {
        type_kind: 'custom',
        type_data: `${metainfoDataTypePackage}.Datetime`,
      } satisfies Quantity['type'],
      'Datetime',
    ],
    [
      'URL',
      {
        type_kind: 'custom',
        type_data: `${metainfoDataTypePackage}.URL`,
      } satisfies Quantity['type'],
      'Link',
    ],
    [
      'JSON',
      {
        type_kind: 'custom',
        type_data: `${metainfoDataTypePackage}.JSON`,
      } satisfies Quantity['type'],
      'JsonData',
    ],
    ...['Capitalized', 'Bytes', 'Unit', 'Dimension'].map((type) => {
      return [
        type,
        {
          type_kind: 'custom',
          type_data: `${metainfoDataTypePackage}.${type}`,
        } satisfies Quantity['type'],
        'Text',
      ] as [string, CustomType, Primitives]
    }),
    [
      'Enum',
      {
        type_kind: 'enum',
        type_data: ['a', 'b', 'c'],
      } satisfies Quantity['type'],
      'EnumRadio',
    ],
    [
      'Enum',
      {
        type_kind: 'enum',
        type_data: Array(10).map((_, index) => `${index}`),
      } satisfies Quantity['type'],
      'EnumSelect',
    ],
    [
      'Reference',
      {
        type_kind: 'reference',
        type_data: {
          m_def: '/metainfo/...',
        } as unknown as Section,
      } satisfies Quantity['type'],
      'SectionReference',
    ],
    [
      'File',
      {
        type_kind: 'custom',
        type_data: `${metainfoDataTypePackage}.File`,
      } satisfies Quantity['type'],
      'FileReference',
    ],
  ])(
    'returns value with primitive for %s',
    (description, type, expectedPrimitive) => {
      const spec = createDynamicComponentSpec({
        m_def: 'Quantity',
        shape: [],
        name: 'test',
        type,
      } satisfies Quantity)
      expect(Object.keys(spec.Value?.valueComponent as Primitives)[0]).toBe(
        expectedPrimitive,
      )
    },
  )

  it('throws for unknown type kind', () => {
    expect(() =>
      createDynamicComponentSpec({
        m_def: 'Quantity',
        shape: [],
        name: 'test',
        type: {type_kind: 'unknown', type_data: 'unknown'},
      } as unknown as Quantity),
    ).toThrow('Unknown quantity type kind unknown')
  })

  it('throws for unknown quantity python type', () => {
    expect(() =>
      createDynamicComponentSpec({
        m_def: 'Quantity',
        shape: [],
        name: 'test',
        type: {type_kind: 'python', type_data: 'unknown'},
      } as unknown as Quantity),
    ).toThrow('Unknown quantity python type unknown')
  })

  it('throws for unknown quantity custom type', () => {
    expect(() =>
      createDynamicComponentSpec({
        m_def: 'Quantity',
        shape: [],
        name: 'test',
        type: {type_kind: 'custom', type_data: 'unknown'},
      } as unknown as Quantity),
    ).toThrow('Unknown quantity custom type unknown')
  })

  it.each([
    ['python Value', [], 'python', 'Value'],
    ['numpy Value', [], 'numpy', 'Value'],
    ['List', ['*'], 'python', 'List'],
    ['Matrix', [2, 2], 'numpy', 'Matrix'],
    ['Vector', ['*'], 'numpy', 'Matrix'],
  ])(
    'returns the right container for the right shape %s',
    (description, shape, type, expectedContainerKey) => {
      const spec = createDynamicComponentSpec({
        m_def: 'Quantity',
        shape,
        name: 'test',
        type:
          type === 'python'
            ? {type_kind: 'python', type_data: 'float'}
            : {type_kind: 'numpy', type_data: 'float64'},
      } satisfies Quantity)
      expect(spec[expectedContainerKey as ContainerKey]).toBeDefined()
    },
  )
})
