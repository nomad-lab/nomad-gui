import {Box} from '@mui/material'
import type {Meta, StoryObj} from '@storybook/react'

import DisplayString from './DisplayString'

const meta = {
  title: 'components/values/utils/DisplayString',
  component: DisplayString,
  decorators: [
    (Story) => {
      return (
        <Box
          sx={{
            width: 'fit-content',
            margin: 1,
            padding: 1,
            border: 1,
            borderColor: 'grey.300',
          }}
        >
          <Story />
        </Box>
      )
    },
  ],
  argTypes: {
    children: {
      control: 'text',
    },
    tooltip: {
      control: 'text',
    },
  },
  tags: ['autodocs'],
} satisfies Meta<typeof DisplayString>

export default meta

const defaultArgs = {
  children: 'This is a longer value string',
  tooltip: undefined,
}

type Story = StoryObj<typeof meta>
export const Plain: Story = {
  args: {
    ...defaultArgs,
  },
}

export const EllipsisRight: Story = {
  decorators: [
    (Story) => (
      <Box sx={{width: 100}}>
        <Story />
      </Box>
    ),
  ],
  args: {
    ...defaultArgs,
    ellipsis: 'right',
  },
}

export const EllipsisLeft: Story = {
  decorators: [
    (Story) => (
      <Box sx={{width: 100}}>
        <Story />
      </Box>
    ),
  ],
  args: {
    ...defaultArgs,
    ellipsis: 'left',
  },
}

export const Placeholder: Story = {
  args: {
    ...defaultArgs,
    placeholder: 'This is a placeholder',
    children: undefined,
  },
}

export const CustomTooltip: Story = {
  args: {
    ...defaultArgs,
    tooltip: 'This tooltip is custom and not the value',
  },
}
