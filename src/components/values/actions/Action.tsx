import {PropsWithChildren} from 'react'

import VisibleOnHover from '../../VisibleOnHover'

export type ActionProps = {
  /**
   * The action will only be visible when the user hovers over the parent element.
   * Will always be visible on devices that do not support hover. Default
   * is true.
   */
  visibleOnHover?: boolean
}

export type DynamicActionProps = ActionProps

export default function Action({
  children,
  visibleOnHover = true,
}: ActionProps & PropsWithChildren) {
  return (
    <VisibleOnHover disabled={!visibleOnHover} sx={{marginY: -1}}>
      {children}
    </VisibleOnHover>
  )
}
