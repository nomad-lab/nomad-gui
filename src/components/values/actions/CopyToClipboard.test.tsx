import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {describe, expect, it, vi} from 'vitest'

import Value from '../containers/Value'
import CopyToClipboard from './CopyToClipboard'

describe('CopyToClipboard', () => {
  it.each([['value'], [undefined]])(
    'renders and allows to copy %s to clipboard',
    async (value) => {
      const writeText = vi.fn().mockImplementation(() => Promise.resolve())
      Object.assign(window.navigator, {
        clipboard: {
          writeText,
        },
      })
      render(
        <Value value={value}>
          <CopyToClipboard tooltipProps={{enterDelay: 0, leaveDelay: 0}} />
        </Value>,
      )
      const clipBoardButton = screen.getByRole('button')
      expect(clipBoardButton).toBeInTheDocument()
      await userEvent.hover(clipBoardButton)
      expect(screen.getByText('Copy to clipboard')).toBeInTheDocument()
      await userEvent.click(clipBoardButton)
      expect(writeText).toHaveBeenCalledWith(value === undefined ? '' : value)
      expect(screen.getByText('Copied!')).toBeInTheDocument()
    },
  )
  it('copies custom text', async () => {
    const writeText = vi.fn().mockImplementation(() => Promise.resolve())
    Object.assign(window.navigator, {
      clipboard: {
        writeText,
      },
    })
    render(
      <Value value='test'>
        <CopyToClipboard
          tooltipProps={{enterDelay: 0, leaveDelay: 0}}
          createClipboardValue={(value) => `${value}-modified`}
        />
      </Value>,
    )
    const clipBoardButton = screen.getByRole('button')
    await userEvent.click(clipBoardButton)
    expect(writeText).toHaveBeenCalled()
    expect(writeText).toHaveBeenCalledWith('test-modified')
  })
})
