import {act, fireEvent, render, screen, waitFor} from '@testing-library/react'

import Help from './Help'

describe('Help', () => {
  it('renders and shows the help content', async () => {
    render(<Help content={'**content**'} visibleOnHover={false} />)
    expect(screen.queryByText('content')).not.toBeInTheDocument()
    const helpButton = screen.getByRole('button')
    expect(helpButton).toBeInTheDocument()
    act(() => fireEvent.click(helpButton))
    expect(screen.getByText('content')).toBeInTheDocument()
    const closeButton = screen.getByRole('button', {name: 'close'})
    act(() => fireEvent.click(closeButton))
    await waitFor(() =>
      expect(screen.queryByText('content')).not.toBeInTheDocument(),
    )
  })
})
