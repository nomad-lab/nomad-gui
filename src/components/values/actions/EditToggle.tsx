import {IconButton} from '@mui/material'
import {useCallback} from 'react'

import {Edit, View} from '../../icons'
import PrimitiveProps from '../primitives/PrimitiveProps'
import useProps from '../utils/useProps'
import Action, {ActionProps} from './Action'

export default function EditToggle(actionProps: ActionProps) {
  const {editable, edit, onEditChange} = useProps<PrimitiveProps>()
  const handleClick = useCallback(() => {
    onEditChange?.(!edit)
  }, [edit, onEditChange])

  if (!editable) {
    return
  }

  return (
    <Action {...actionProps}>
      <IconButton onClick={handleClick} size='small'>
        {edit ? <View /> : <Edit />}
      </IconButton>
    </Action>
  )
}
