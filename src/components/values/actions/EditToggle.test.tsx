import {act, fireEvent, render, screen} from '@testing-library/react'
import {vi} from 'vitest'

import {TestContainer} from '../containers/testComponents.helper'
import EditToggle from './EditToggle'

describe('EditToggle', () => {
  it.each([
    ['edit', true],
    ['not edit', false],
  ])('renders and changes edit change with %s', async (description, edit) => {
    const handleEditChange = vi.fn()
    render(
      <TestContainer editable onEditChange={handleEditChange} edit={edit}>
        <EditToggle />
      </TestContainer>,
    )
    const helpButton = screen.getByRole('button')
    expect(helpButton).toBeInTheDocument()
    act(() => fireEvent.click(helpButton))
    expect(handleEditChange).toHaveBeenCalled()
  })
  it('does not render if not editable', async () => {
    render(
      <TestContainer editable={false}>
        <EditToggle />
      </TestContainer>,
    )
    expect(screen.queryByRole('button')).not.toBeInTheDocument()
  })
})
