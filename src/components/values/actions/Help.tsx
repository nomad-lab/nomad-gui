import {
  Button,
  DialogActions,
  DialogContent,
  IconButton,
  Popover,
} from '@mui/material'
import {useState} from 'react'

import {Help as HelpIcon} from '../../icons'
import Markdown from '../../markdown/Markdown'
import Action, {ActionProps, DynamicActionProps} from './Action'

export type HelpProps = {
  content: string
} & ActionProps

export type DynamicHelpProps = DynamicActionProps & Pick<HelpProps, 'content'>

export default function Help({content, ...actionProps}: HelpProps) {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null)

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const open = Boolean(anchorEl)

  return (
    <Action {...actionProps}>
      <IconButton size='small' onClick={handleClick}>
        <HelpIcon />
      </IconButton>

      <Popover
        transitionDuration={0}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        disableAutoFocus
        disableEnforceFocus
        disableRestoreFocus
      >
        <DialogContent>
          <Markdown content={content} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>close</Button>
        </DialogActions>
      </Popover>
    </Action>
  )
}
