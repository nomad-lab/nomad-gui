import {Tooltip, TooltipProps} from '@mui/material'
import IconButton from '@mui/material/IconButton'
import {useCallback, useMemo, useState} from 'react'
import {useMountedState} from 'react-use'

import {Clipboard} from '../../icons'
import PrimitiveProps from '../primitives/PrimitiveProps'
import useProps from '../utils/useProps'
import Action, {ActionProps, DynamicActionProps} from './Action'

export type CopyToClipboardProps<Value = unknown> = {
  createClipboardValue?: (value?: Value) => string
  tooltipProps?: Partial<TooltipProps>
  copiedDelay?: number
} & PrimitiveProps<Value> &
  ActionProps

export type DynamicCopyToClipboardProps = DynamicActionProps &
  Pick<CopyToClipboardProps, 'copiedDelay'>

/**
 * An layout action that allows users to copy a value to the clipboard.
 */
export default function CopyToClipboard<Value = unknown>({
  createClipboardValue,
  tooltipProps,
  copiedDelay = 200,
  visibleOnHover,
  ...primitiveProps
}: CopyToClipboardProps<Value>) {
  const {value} = useProps<PrimitiveProps<Value>>(primitiveProps)
  const [isCopied, setIsCopied] = useState(false)
  const isMounted = useMountedState()
  const clipboardValue = useMemo(() => {
    if (!value) {
      return ''
    } else if (createClipboardValue) {
      return createClipboardValue(value)
    } else {
      return value.toString()
    }
  }, [value, createClipboardValue])

  const handleClick = useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => {
      event.preventDefault()
      event.stopPropagation()
      navigator.clipboard.writeText(clipboardValue).then(() => {
        setIsCopied(true)
      })
    },
    [clipboardValue],
  )

  const handleMouseLeave = useCallback(() => {
    setTimeout(() => {
      if (isMounted()) {
        setIsCopied(false)
      }
    }, copiedDelay)
  }, [copiedDelay, isMounted])

  return (
    <Action visibleOnHover={visibleOnHover}>
      <Tooltip
        title={isCopied ? 'Copied!' : 'Copy to clipboard'}
        onClose={handleMouseLeave}
        {...tooltipProps}
      >
        <IconButton
          tabIndex={-1}
          onClick={handleClick}
          size='small'
          onMouseDown={(event) => event.preventDefault()}
        >
          <Clipboard />
        </IconButton>
      </Tooltip>
    </Action>
  )
}
