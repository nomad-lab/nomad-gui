import {act, fireEvent, render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {vi} from 'vitest'

import Numeral from '../primitives/Numeral'
import Matrix from './Matrix'

describe('Matrix', () => {
  vi.mock('react-virtualized-auto-sizer', () => ({
    default: ({
      children,
    }: {
      children: ({height, width}: {height: number; width: number}) => void
    }) => children({height: 100, width: 100}),
  }))
  it('renders a placeholder', () => {
    render(
      <Matrix
        renderValue={() => <Numeral />}
        value={undefined}
        placeholder='placeholder'
      />,
    )
    expect(screen.getByText('placeholder')).toBeInTheDocument()
  })
  it('renders a vector', () => {
    render(<Matrix value={[0, 1]} renderValue={() => <Numeral />} />)
    expect(screen.getByText('0')).toBeInTheDocument()
    expect(screen.getByText('1')).toBeInTheDocument()
  })
  it('renders a transposed vector', () => {
    render(<Matrix value={[0, 1]} transpose renderValue={() => <Numeral />} />)
    expect(screen.getByText('0')).toBeInTheDocument()
    expect(screen.getByText('1')).toBeInTheDocument()
  })
  it('renders a matrix', () => {
    render(<Matrix value={[[0], [1]]} renderValue={() => <Numeral />} />)
    expect(screen.getByText('0')).toBeInTheDocument()
    expect(screen.getByText('1')).toBeInTheDocument()
  })
  it('renders a tensor', async () => {
    render(
      <Matrix
        value={[
          [[8], [8]],
          [[9], [9]],
        ]}
        renderValue={() => <Numeral />}
      />,
    )
    expect(screen.getAllByText('8')).toHaveLength(2)
    expect(screen.getByText('1')).toBeInTheDocument()
    expect(screen.getByText('2')).toBeInTheDocument()

    await userEvent.click(screen.getByText('2'))
    expect(screen.getAllByText('9')).toHaveLength(2)
  })
  it('allows to change unit', async () => {
    const handleDisplayUnitChange = vi.fn()
    render(
      <Matrix
        value={[[1]]}
        unit='m'
        renderValue={() => <Numeral />}
        onDisplayUnitChange={handleDisplayUnitChange}
      />,
    )
    const inputElement = screen.getByDisplayValue('m') as HTMLInputElement
    await userEvent.type(inputElement, `{backspace}cm{enter}`)
    expect(handleDisplayUnitChange).toHaveBeenCalledWith('cm')
    expect(screen.getByText('100')).toBeInTheDocument()
    expect(screen.queryByRole('button', {name: 'lock'})).not.toBeInTheDocument()
  })
  it('shows focus with label color', async () => {
    render(
      <Matrix
        value={[[1]]}
        unit={'m'}
        renderValue={() => <Numeral />}
        label='Label'
      />,
    )
    const label = screen.getByText('Label')
    const unit = screen.getByDisplayValue('m')
    act(() => fireEvent.focus(unit))
    expect(label).toHaveClass('Mui-focused')
    act(() => fireEvent.blur(unit))
    expect(label).not.toHaveClass('Mui-focused')
  })
})
