import {
  Box,
  FormControl,
  FormControlProps,
  FormHelperText,
  InputLabel,
  Stack,
  styled,
  useTheme,
} from '@mui/material'
import {FocusEvent, PropsWithChildren, useCallback} from 'react'

import useLocalState from '../../../hooks/useLocalState'
import PropsProvider from '../utils/PropsProvider'
import useProps from '../utils/useProps'
import ContainerProps, {ControlProps} from './ContainerProps'

const LabelContainer = styled(Box)(({theme}) => ({
  position: 'absolute',
  marginTop: theme.spacing(0.5),
  height: theme.spacing(2),
  width: '100%',
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'nowrap',
  flexFlow: 'flex-start',
  alignItems: 'center',
}))

const StaticInputLabel = styled(InputLabel)(() => ({
  position: 'static', // normal "shrink" will not work anymore but was already disabled
  transform: 'none', // overrides shrink transformations to help position label actions
  overflow: 'visible', // otherwise label is clipped for short inputs
  textOverflow: 'ellipsis',
  fontSize: '0.75rem',
}))

const LabelActionsContainer = styled(Box)(({theme}) => ({
  position: 'static',
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  '& .MuiIconButton-root': {},
  '& .MuiChip-root': {
    marginLeft: theme.spacing(0.5),
    height: 'fit-content',
  },
}))

export type ValueFormControlProps = ControlProps &
  FormControlProps &
  PropsWithChildren & {
    edit?: boolean
  }

export function ValueFormControl({...props}: ValueFormControlProps) {
  const {
    children,
    label,
    helperText,
    focused,
    labelActions,
    fullWidth,
    error,
    edit,
    ...formControlProps
  } = props
  const {inputId, editable} = useProps<ContainerProps>()
  const theme = useTheme()

  return (
    <FormControl
      fullWidth={fullWidth}
      variant='standard'
      error={error}
      focused={focused}
      size='small'
      {...formControlProps}
    >
      {label && (
        <LabelContainer>
          <StaticInputLabel
            shrink
            id={editable ? undefined : inputId && `${inputId}-label`}
            htmlFor={editable ? inputId : undefined}
            style={{marginLeft: editable ? theme.spacing(1.5) : 0}}
          >
            {label}
          </StaticInputLabel>
          <LabelActionsContainer>{labelActions}</LabelActionsContainer>
        </LabelContainer>
      )}
      <Stack direction='row' width='100%'>
        {children}
      </Stack>
      {(edit || error) && helperText && (
        <FormHelperText>{helperText}</FormHelperText>
      )}
    </FormControl>
  )
}

function useControlledProps<Value>(
  props: Partial<ContainerProps<Value>>,
): ContainerProps<Value> {
  const {
    editable,
    edit: controlledEdit,
    onEditChange,
    error: controlledError,
    helperText,
    onError,
    focused: controlledFocused,
    onFocus,
    onBlur,
    ...containerProps
  } = useProps(props)
  const [edit, setEdit] = useLocalState(
    editable ? (controlledEdit === undefined ? true : controlledEdit) : false,
    {
      onChange: onEditChange,
    },
  )

  const [error, setError] = useLocalState(
    controlledError ? helperText : undefined,
    {
      onChange: (error: string) => {
        onError?.(error)
      },
    },
  )
  const [focused, setFocused] = useLocalState(false)
  const handleFocus = useCallback(
    (event: FocusEvent<HTMLInputElement>) => {
      setFocused(true)
      onFocus?.(event)
    },
    [onFocus, setFocused],
  )
  const handleBlur = useCallback(
    (event: FocusEvent<HTMLInputElement>) => {
      setFocused(false)
      onBlur?.(event)
    },
    [onBlur, setFocused],
  )

  return {
    ...containerProps,
    editable,
    edit,
    onEditChange: setEdit,
    error: !!error,
    helperText: error || helperText,
    onError: setError as (error: string | undefined) => void,
    focused,
    onBlur: handleBlur,
    onFocus: handleFocus,
  }
}

export type Controllable = 'label'

export type ContainerControlProps<Value = unknown> = ContainerProps<Value> &
  PropsWithChildren & {
    /**
     * Determines if the label is shown in the control or passed to the
     * children to show.
     */
    showLabel?: boolean
  }

/**
 * Renders a form control with a label and helper text, depending on the
 * given props. It provides the given props to children using `useProps`.
 * It further provides control for the error and focused state of the control.
 */
export default function ContainerControl<Value = unknown>({
  children,
  showLabel = false,
  ...props
}: ContainerControlProps<Value>) {
  const {label, labelActions, focused, helperText, ...commonProps} =
    useControlledProps(props)

  const controlProps: ValueFormControlProps = {
    fullWidth: commonProps.fullWidth,
    edit: commonProps.edit,
    error: commonProps.error,
    helperText,
    focused,
  }
  const containerProps: ContainerProps<Value> = {...commonProps}

  if (showLabel) {
    controlProps.label = label
    controlProps.labelActions = labelActions
  } else {
    containerProps.label = label
    containerProps.labelActions = labelActions
  }

  return (
    <PropsProvider {...containerProps}>
      <ValueFormControl {...controlProps}>{children}</ValueFormControl>
    </PropsProvider>
  )
}
