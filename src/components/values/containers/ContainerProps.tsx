import React from 'react'

import PrimitiveProps, {
  DynamicPrimitiveProps,
} from '../primitives/PrimitiveProps'
import {
  ActionKey,
  Actions,
  PrimitiveKey,
  Primitives,
} from '../utils/dynamicComponents'

export type ControlProps = {
  /**
   * Optional helper text to display below the value(s).
   */
  helperText?: string
  /**
   * Optional label to display above the value(s).
   */
  label?: string
  /**
   * Optional adornments shown right to the label
   */
  labelActions?: React.ReactNode
  /**
   * Whether the value is focused.
   */
  focused?: boolean

  error?: boolean
  fullWidth?: boolean

  inputId?: string
}

type ContainerProps<Value = unknown> = PrimitiveProps<Value> &
  ControlProps & {
    /**
     * Function that renders a single value(s) in the container.
     */
    renderValue?: () => React.ReactNode
  }

export type DynamicContainerProps = DynamicPrimitiveProps &
  Pick<
    PrimitiveProps,
    'unit' | 'displayUnit' | 'displayUnitLocked' | 'hiddenUnit'
  > &
  Pick<ContainerProps, 'helperText' | 'label'> & {
    labelActions?: Actions | ActionKey | ActionKey[]
  } & {
    valueComponent?: Primitives | PrimitiveKey
  }

export type NonEditableContainerProps<Value = unknown> = Omit<
  ContainerProps<Value>,
  'onBlur' | 'onFocus' | 'onError' | 'onChange'
> & {
  editable?: false
}

export default ContainerProps
