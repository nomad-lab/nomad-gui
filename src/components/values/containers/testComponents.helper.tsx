import {PropsWithChildren, useEffect} from 'react'

import useLocalState from '../../../hooks/useLocalState'
import PrimitiveProps from '../primitives/PrimitiveProps'
import PropsProvider from '../utils/PropsProvider'
import useProps from '../utils/useProps'
import ContainerProps from './ContainerProps'

export function TestContainer<Props extends ContainerProps>({
  value,
  onChange,
  children,
  ...containerProps
}: Props & PropsWithChildren) {
  const [localValue, setLocalValue] = useLocalState(value, {onChange})
  return (
    <PropsProvider
      value={localValue}
      onChange={setLocalValue}
      {...containerProps}
    >
      {children}
    </PropsProvider>
  )
}

export function TestError(props: PrimitiveProps<never>) {
  const {onError} = useProps(props)
  useEffect(() => {
    onError?.('error')
  }, [onError])
  return 'nothing'
}
