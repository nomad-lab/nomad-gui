import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {vi} from 'vitest'

import Text from '../primitives/Text'
import Value from './Value'

describe('Value', () => {
  it('renders non editable', () => {
    render(
      <Value value={'value'}>
        <Text />
      </Value>,
    )
    expect(screen.getByText('value')).toBeInTheDocument()
  })
  it('renders placeholder without value', () => {
    render(
      <Value placeholder='placeholder'>
        <Text />
      </Value>,
    )
    expect(screen.getByText('placeholder')).toBeInTheDocument()
  })
  it('renders not placeholder with value', () => {
    render(
      <Value value='value' placeholder='placeholder'>
        <Text />
      </Value>,
    )
    expect(screen.queryByText('placeholder')).not.toBeInTheDocument()
  })
  it.each([['with error'], ['without error']])(
    'renders all control elements %s',
    (description) => {
      render(
        <Value
          label='Label'
          helperText='Helper text'
          error={description === 'with error'}
          value='value'
          editable
        >
          <Text />
        </Value>,
      )

      expect(screen.getByDisplayValue('value'))
      if (description === 'with error') {
        expect(screen.getByText('Label')).toHaveClass('Mui-error')
        expect(screen.getByText('Helper text')).toHaveClass('Mui-error')
      } else {
        expect(screen.getByText('Label')).not.toHaveClass('Mui-error')
        expect(screen.getByText('Helper text')).not.toHaveClass('Mui-error')
      }
    },
  )
  it('shows label, but not helper text on non editable', () => {
    render(
      <Value label='Label' helperText='Helper text' value='value'>
        <Text />
      </Value>,
    )
    expect(screen.getByText('Label')).toBeInTheDocument()
    expect(screen.queryByText('Helper text')).not.toBeInTheDocument()
  })
  it('allows to change the value', async () => {
    const handleChange = vi.fn()
    render(
      <Value
        value='value'
        onChange={handleChange}
        editable
        placeholder='placeholder'
      >
        <Text />
      </Value>,
    )
    const input = screen.getByDisplayValue('value')
    await userEvent.type(input, '+{enter}')
    expect(handleChange).toHaveBeenCalledWith('value+')
  })
  it('allows to get the editable value from a label', () => {
    render(
      <Value value='value' editable label='Label'>
        <Text />
      </Value>,
    )
    const element = screen.getByLabelText('Label')
    expect(element).toHaveValue('value')
  })
  it('allows to get the non-editable value from a label', () => {
    render(
      <Value value='value' label='Label'>
        <Text />
      </Value>,
    )
    const element = screen.getByLabelText('Label')
    expect(element).toHaveTextContent('value')
  })
})
