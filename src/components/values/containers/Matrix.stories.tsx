import type {Meta, StoryObj} from '@storybook/react'

import Numeral from '../primitives/Numeral'
import {baseDecorator} from '../utils/storyDecorators.helper'
import Matrix from './Matrix'

const meta = {
  title: 'components/values/containers/Matrix',
  component: Matrix,
  decorators: [baseDecorator],
  tags: ['autodocs'],
  argTypes: {
    value: {
      table: {
        disable: true,
      },
    },
  },
} satisfies Meta<typeof Matrix<number>>

export default meta

function generateValue(n: number, m?: number, v?: number) {
  m = m || n
  const value: number[][] = []
  for (let i: number = 0; i < m; i++) {
    value[i] = []
    for (let j: number = 0; j < n; j++) {
      value[i][j] = v === undefined ? i + j : v
    }
  }
  return value
}

const defaultArgsWithoutValue = {
  fullWidth: true,
  renderValue: () => <Numeral />,
  placeholder: 'placeholder',
}

const defaultArgs = {
  value: generateValue(50),
  ...defaultArgsWithoutValue,
}

defaultArgs.value[1][1] = 203112.3457

type Story = StoryObj<typeof meta>
export const FullWidth: Story = {
  args: {
    ...defaultArgs,
  },
}

export const NoValue: Story = {
  args: {
    ...defaultArgs,
    value: undefined,
    placeholder: 'No value',
  },
}

export const CustomWidth: Story = {
  args: {
    ...defaultArgs,
    fullWidth: false,
    width: 100,
  },
}

export const RowVector: Story = {
  args: {
    ...defaultArgs,
    fullWidth: false,
    columnWidth: 40,
    value: [1, 2, 3],
  },
}

export const ColumnVector: Story = {
  args: {
    ...defaultArgs,
    fullWidth: false,
    columnWidth: 40,
    value: [1, 2, 3],
    transpose: true,
  },
}

export const _4DTensor: Story = {
  args: {
    ...defaultArgs,
    value: Array(3)
      .fill(0)
      .map((_, i) =>
        Array(6)
          .fill(0)
          .map((_, j) => generateValue(10, 3, i + j)),
      ),
  },
}

export const WithUnit: Story = {
  args: {
    ...defaultArgs,
    unit: 'm',
    label: 'label',
    columnWidth: 100,
  },
}

export const LargeMatrix: Story = {
  args: {
    ...defaultArgs,
    value: generateValue(1000),
  },
}

export const FormControl: Story = {
  args: {
    ...defaultArgs,
    helperText: 'Helper text',
    label: 'label',
  },
}

export const FormControlWithError: Story = {
  args: {
    ...defaultArgs,
    helperText: 'Invalid value',
    label: 'label',
    error: true,
  },
}
