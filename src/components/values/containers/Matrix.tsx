import {Box, Pagination, Stack} from '@mui/material'
import {get} from 'lodash'
import React, {PropsWithChildren, useMemo, useState} from 'react'
import AutoSizer from 'react-virtualized-auto-sizer'
import {FixedSizeGrid as Grid, GridChildComponentProps} from 'react-window'

import useLocalState from '../../../hooks/useLocalState'
import {assert} from '../../../utils/utils'
import PrimitiveProps from '../primitives/PrimitiveProps'
import Text from '../primitives/Text'
import Unit from '../utils/Unit'
import useProps from '../utils/useProps'
import ContainerControl from './ContainerControl'
import {
  DynamicContainerProps,
  NonEditableContainerProps,
} from './ContainerProps'
import Value from './Value'

export type Tensor<Value = unknown> = Value | Tensor<Value>[]

function getShape<Value>(data: Tensor<Value> | undefined): number[] {
  if (data === undefined) {
    return []
  }
  if (!Array.isArray(data)) {
    return []
  }
  const subDimensionSizes = getShape(data[0])
  return [data.length, ...subDimensionSizes]
}

export type MatrixProps<Value = unknown> = Omit<
  NonEditableContainerProps<Tensor<Value>>,
  'edit' | 'editable'
> & {
  /**
   * Determines how to render each item value.
   */
  renderValue: (props: PrimitiveProps<Value>) => React.ReactNode

  /**
   * Displays the matrix transposed. This only affects the displayed
   * matrix (i.e. the last two dimensions) and not the paginated
   * dimensions.
   */
  transpose?: boolean

  /**
   * The height of the matrix. By default the hight of a five
   * row matrix of text set numbers is calculated.
   */
  height?: number
  /**
   * The width of the matrix. With fullWidth, the matrix
   * will fill the available space.
   */
  width?: number
  /**
   * The height reserved for each row.
   */
  rowHeight?: number
  /**
   * The width reserved for each column.
   */
  columnWidth?: number
  /**
   * If no explicit height is set, the matrix will show at most
   * this many rows. Default is 5.
   */
  displayRows?: number
  /**
   * If not fullWidth and no explicit width is set, the matrix
   * will show at most this many columns. Default is 5.
   */
  displayColumns?: number
} & PropsWithChildren

export type DynamicMatrixProps = Omit<
  DynamicContainerProps,
  'edit' | 'editable'
> &
  Pick<
    MatrixProps,
    | 'transpose'
    | 'displayRows'
    | 'displayColumns'
    | 'rowHeight'
    | 'columnWidth'
    | 'height'
    | 'width'
  >

/**
 * A container that allows to display a matrix of values. Can be
 * used for vector, matrix, and tensor values of arbitrary many
 * dimensions.
 *
 * For large matrices, only a small portion of the values are
 * shown. The rest is hidden and virtualized. Use the
 * hight, width, rowHeight, and columnWidth props to control
 * the size of the matrix.
 *
 * Matrix is a readonly container and does not support editing.
 */
export default function Matrix<Value = unknown>({
  renderValue,
  transpose = false,
  height,
  width,
  rowHeight = 32,
  columnWidth = 60,
  displayColumns = 5,
  displayRows = 5,
  children,
  ...props
}: MatrixProps<Value>) {
  props = useProps(props)

  const {
    value,
    label,
    fullWidth,
    displayUnit: controlledDisplayUnit,
    unit,
    onDisplayUnitChange,
  } = props
  const valueShape = useMemo(() => getShape(value), [value])
  const [pages, setPages] = useState<number[]>(
    new Array(Math.max(0, valueShape.length - 2)).fill(0),
  )
  const [displayUnit, setDisplayUnit] = useLocalState(
    controlledDisplayUnit || unit,
    {onChange: onDisplayUnitChange},
  )
  const containerProps = {
    ...props,
    displayUnit,
    onDisplayUnitChange: setDisplayUnit,
    displayUnitLocked: undefined, // never show the unit locked on a matrix
  }

  const matrixValue = useMemo(() => {
    let result: Value[][] | undefined = undefined
    if (valueShape.length === 0) {
      return undefined
    } else if (valueShape.length === 1) {
      result = [value as Value[]]
    } else if (valueShape.length === 2) {
      result = value as Value[][]
    } else {
      result = get(value, pages)
    }
    assert(result !== undefined, `Unsupported value shape ${valueShape}`)
    if (transpose) {
      result = result[0].map((_, i) =>
        (result as Value[][]).map((row) => row[i]),
      )
    }
    return result
  }, [value, pages, valueShape, transpose])

  // This is used to force an update on the grid. The solution to use forceUpdate
  // proposed in the react-virtualized docs does not work.
  const gridKey = useMemo(() => pages.reduce((n, m) => n * m + n, 1), [pages])

  const cellRenderer = ({
    columnIndex,
    rowIndex,
    style,
  }: GridChildComponentProps) => {
    return (
      <div
        style={{...style, paddingLeft: 4, paddingRight: 4, textAlign: 'center'}}
      >
        <Value value={matrixValue?.[rowIndex][columnIndex]} hiddenUnit>
          {renderValue?.({})}
        </Value>
      </div>
    )
  }

  const matrixElement =
    matrixValue === undefined ? (
      <Text value={undefined} />
    ) : (
      <>
        <Box
          sx={{
            borderLeft: 2,
            borderRight: 2,
            paddingX: 0.5,
            ...(fullWidth
              ? {width: '100%'}
              : {
                  width:
                    width ||
                    Math.min(
                      displayColumns * columnWidth + 12,
                      matrixValue[0]?.length * columnWidth + 12,
                    ),
                }),
            height:
              height ||
              Math.min(displayRows * rowHeight, matrixValue.length * rowHeight),
          }}
        >
          <AutoSizer key={gridKey}>
            {({height, width}) => (
              <Grid
                columnCount={matrixValue[0].length}
                columnWidth={columnWidth}
                height={height}
                rowCount={matrixValue.length}
                rowHeight={rowHeight}
                width={width}
              >
                {cellRenderer}
              </Grid>
            )}
          </AutoSizer>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-end',
            marginTop: 0.5,
          }}
        >
          {pages.map((page, index) => (
            <Pagination
              key={index}
              count={valueShape[index]}
              page={page + 1}
              size='small'
              showFirstButton
              showLastButton
              onChange={(_, page) => {
                const changedPages = [...pages]
                changedPages[index] = page - 1
                setPages(changedPages)
              }}
            />
          ))}
          {(unit || displayUnit) && <Unit variant='standard' />}
        </Box>
      </>
    )

  return (
    <ContainerControl showLabel {...containerProps}>
      <Stack width='100%' marginTop={label ? 3 : undefined}>
        {matrixElement}
      </Stack>
    </ContainerControl>
  )
}
