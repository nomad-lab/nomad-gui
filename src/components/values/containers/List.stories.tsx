import type {Meta, StoryObj} from '@storybook/react'

import CopyToClipboard from '../actions/CopyToClipboard'
import Text from '../primitives/Text'
import {baseDecorator, editorDecorator} from '../utils/storyDecorators.helper'
import List from './List'

const meta = {
  title: 'components/values/containers/List',
  component: List,
  decorators: [editorDecorator, baseDecorator],
  tags: ['autodocs'],
} satisfies Meta<typeof List<string>>

export default meta

const defaultArgs = {
  value: ['one', 'two', 'three'],
  editable: true,
  fullWidth: true,
  placeholder: 'Enter a value ...',
  createEmptyValue: () => '',
  renderValue: () => (
    <>
      <Text />
      <CopyToClipboard />
    </>
  ),
}

type Story = StoryObj<typeof meta>
export const Editable: Story = {
  args: {
    ...defaultArgs,
  },
}

export const FixedLengthEditable: Story = {
  args: {
    ...defaultArgs,
    createEmptyValue: undefined,
  },
}

export const NotEditable: Story = {
  args: {
    ...defaultArgs,
    editable: false,
  },
}

export const InlineEditable: Story = {
  args: {
    ...defaultArgs,
    variant: 'inline',
    fullWidth: false,
    editable: true,
    renderValue: () => <Text sx={{width: 100}} />,
  },
}

export const InlineNotEditable: Story = {
  args: {
    ...defaultArgs,
    value: [
      'one',
      'two',
      'three',
      'four',
      'five',
      'six',
      'seven',
      'eight',
      'nine',
      'ten',
    ],
    variant: 'inline',
    fullWidth: false,
    editable: false,
    renderValue: () => <Text />,
  },
}

export const InitialWidth: Story = {
  args: {
    ...defaultArgs,
    fullWidth: false,
  },
}

export const FormControl: Story = {
  args: {
    ...defaultArgs,
    helperText: 'Helper text',
    label: 'label',
    editable: false,
  },
}

export const EditableFormControl: Story = {
  args: {
    ...defaultArgs,
    helperText: 'Helper text',
    label: 'label',
  },
}

export const FormControlWithError: Story = {
  args: {
    ...defaultArgs,
    helperText: 'Invalid value',
    label: 'label',
    error: true,
  },
}
