import {Chip} from '@mui/material'
import type {Meta, StoryObj} from '@storybook/react'

import CopyToClipboard from '../actions/CopyToClipboard'
import Text from '../primitives/Text'
import {baseDecorator, editorDecorator} from '../utils/storyDecorators.helper'
import Value from './Value'

const meta = {
  title: 'components/values/containers/Value',
  component: Value,
  decorators: [editorDecorator, baseDecorator],
  tags: ['autodocs'],
  args: {
    value: 'Hello world',
    editable: true,
    fullWidth: true,
    placeholder: undefined,
    helperText: undefined,
    error: undefined,
    label: undefined,
    actions: <CopyToClipboard />,
    children: <Text />,
  },
  parameters: {
    controls: {exclude: ['actions', 'children']},
  },
} satisfies Meta<typeof Value<string>>

export default meta

type Story = StoryObj<typeof meta>
export const Editable: Story = {}

export const NotEditable: Story = {
  args: {
    editable: false,
  },
}

export const InitialWidth: Story = {
  args: {
    fullWidth: false,
  },
}

export const InitialWidthWithShortValueAndLongLabel: Story = {
  args: {
    fullWidth: false,
    value: 'X',
    editable: false,
    label: 'label',
    labelActions: (
      <>
        <Chip label='small' size='small' />
        <Chip label='medium' size='medium' />
        <CopyToClipboard />
      </>
    ),
  },
}

export const FormControl: Story = {
  args: {
    helperText: 'Helper text',
    label: 'label',
    labelActions: (
      <>
        <Chip label='important' size='small' />
        <CopyToClipboard value='label' />
      </>
    ),
    editable: false,
  },
}

export const EditableFormControl: Story = {
  args: {
    helperText: 'Helper text',
    label: 'label',
    labelActions: (
      <>
        <Chip label='important' size='small' />
        <CopyToClipboard value='label' />
      </>
    ),
  },
}

export const FormControlWithError: Story = {
  args: {
    helperText: 'Invalid value',
    label: 'label',
    error: true,
  },
}
