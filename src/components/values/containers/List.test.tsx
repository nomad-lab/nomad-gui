import {act, fireEvent, render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {vi} from 'vitest'

import PrimitiveProps from '../primitives/PrimitiveProps'
import Text from '../primitives/Text'
import useProps from '../utils/useProps'
import List, {ListProps} from './List'
import {TestError} from './testComponents.helper'

describe('List', () => {
  describe.each([['block'], ['inline']])('%s', (variant) => {
    it('renders all items non editable', () => {
      render(
        <List
          variant={variant as ListProps['variant']}
          value={['one', 'two']}
          renderValue={() => <Text />}
        />,
      )
      expect(screen.getByText('one')).toBeInTheDocument()
      expect(screen.getByText('two')).toBeInTheDocument()
    })
    it('renders without value', () => {
      render(<List value={undefined} renderValue={() => <Text />} />)
    })
    it.each([['with error'], ['without error']])(
      'renders all control elements %s',
      (description) => {
        render(
          <List
            variant={variant as ListProps['variant']}
            label='Label'
            helperText='Helper text'
            error={description === 'with error'}
            value={[]}
            renderValue={() => <Text />}
            editable
          />,
        )

        if (description === 'with error') {
          expect(screen.getByText('Label')).toHaveClass('Mui-error')
          expect(screen.getByText('Helper text')).toHaveClass('Mui-error')
        } else {
          expect(screen.getByText('Label')).not.toHaveClass('Mui-error')
          expect(screen.getByText('Helper text')).not.toHaveClass('Mui-error')
        }
      },
    )
    it('shows label, but not helper text on non editable', () => {
      render(
        <List
          variant={variant as ListProps['variant']}
          label='Label'
          helperText='Helper text'
          value={[]}
          renderValue={() => <Text />}
        />,
      )
      expect(screen.getByText('Label')).toBeInTheDocument()
      expect(screen.queryByText('Helper text')).not.toBeInTheDocument()
    })
    it('shows helper text, but does not pass it to items', () => {
      render(
        <List
          variant={variant as ListProps['variant']}
          label='Label'
          helperText='Helper text'
          editable
          value={['test']}
          renderValue={() => <Text />}
        />,
      )
      expect(screen.getByText('Label')).toBeInTheDocument()
      expect(screen.getByText('Helper text')).toBeInTheDocument()
    })
    it('allows to change values', async () => {
      const handleChange = vi.fn()
      render(
        <List
          variant={variant as ListProps['variant']}
          value={['one', 'two']}
          onChange={handleChange}
          editable
          renderValue={() => <Text />}
          placeholder='placeholder'
        />,
      )
      const input = screen.getByDisplayValue('one')
      await userEvent.type(input, '+{enter}')
      expect(handleChange).toHaveBeenCalledWith(['one+', 'two'])
    })
    it('allows to add values', async () => {
      const handleChange = vi.fn()
      render(
        <List
          variant={variant as ListProps['variant']}
          value={['one', 'two']}
          onChange={handleChange}
          editable
          renderValue={() => <Text />}
          createEmptyValue={() => ''}
          placeholder='placeholder'
        />,
      )
      const addButton = screen.getByRole('button', {name: 'add item'})
      await userEvent.click(addButton)
      expect(handleChange).toHaveBeenCalledWith(['one', 'two', ''])
    })
    it('allows to remove values', async () => {
      const handleChange = vi.fn()
      render(
        <List
          variant={variant as ListProps['variant']}
          value={['one', 'two']}
          onChange={handleChange}
          editable
          renderValue={() => <Text />}
          createEmptyValue={() => ''}
          placeholder='placeholder'
        />,
      )
      const removeButton = screen.getAllByRole('button')[0]
      await userEvent.click(removeButton)
      expect(handleChange).toHaveBeenCalledWith(['two'])
    })
    it('allows to shuffle values', async () => {
      const handleChange = vi.fn()
      render(
        <List
          variant={variant as ListProps['variant']}
          value={['one', 'two']}
          onChange={handleChange}
          editable
          renderValue={() => <Text />}
        />,
      )
      const secondHandle = screen.getAllByTestId('DragHandleIcon')[1]
      const firstValue = screen.getByDisplayValue('one')
      expect(secondHandle).toBeInTheDocument()

      act(() => fireEvent.dragStart(secondHandle))
      act(() => fireEvent.dragEnter(firstValue))
      act(() => fireEvent.dragEnd(firstValue))
      expect(handleChange).not.toHaveBeenCalled()

      act(() => fireEvent.dragStart(secondHandle))
      act(() => fireEvent.drop(secondHandle))
      expect(handleChange).not.toHaveBeenCalled()

      act(() => fireEvent.dragStart(secondHandle))
      act(() => fireEvent.dragEnter(firstValue))
      act(() => fireEvent.drop(firstValue))
      expect(handleChange).toHaveBeenCalledWith(['two', 'one'])
    })
  })
  it('re-renders only the changed item', async () => {
    const hasRendered = vi.fn()
    function Value() {
      const {value} = useProps<PrimitiveProps<string>>()
      hasRendered(value)
      return ''
    }
    const renderValue = () => <Value />
    const {rerender} = render(<List value={[1, 2]} renderValue={renderValue} />)
    expect(hasRendered).toHaveBeenCalledTimes(2)
    hasRendered.mockClear()

    rerender(<List value={[1, 3]} renderValue={renderValue} />)
    expect(hasRendered).toHaveBeenCalledTimes(1)
    expect(hasRendered).toHaveBeenCalledWith(3)
  })
  it('shows errors', async () => {
    render(
      <List
        editable
        value={['one']}
        renderValue={() => <TestError />}
        label='Label'
      />,
    )
    // Ideally the label would have `Mui-error` class. But, we cannot
    // separate error from helperText. Therefore, we either have the right
    // label and an unwanted repeated helperText (on the FormControl of the value
    // an the FormControl of the list), or we don't have the
    // repeated helperText but the label without the error class.
    expect(screen.getByText('Label')).not.toHaveClass('Mui-error')
    expect(screen.getByText('error')).toHaveClass('Mui-error')
  })
  it('shows focus with label color', async () => {
    render(
      <List
        editable
        value={['one']}
        renderValue={() => <Text />}
        label='Label'
      />,
    )
    const label = screen.getByText('Label')
    const text = screen.getByDisplayValue('one')
    expect(label).not.toHaveClass('Mui-focused')
    act(() => fireEvent.focus(text))
    expect(label).toHaveClass('Mui-focused')
    act(() => fireEvent.blur(text))
    expect(label).not.toHaveClass('Mui-focused')
  })
})
