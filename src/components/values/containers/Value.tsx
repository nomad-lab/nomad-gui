import {Stack} from '@mui/material'
import useId from '@mui/material/utils/useId'
import {PropsWithChildren} from 'react'

import {assert} from '../../../utils/utils'
import VisibleOnHover from '../../VisibleOnHover'
import ContainerControl from './ContainerControl'
import ContainerProps, {DynamicContainerProps} from './ContainerProps'

export type ValueProps<Value = unknown> = ContainerProps<Value> &
  PropsWithChildren

export type DynamicValueProps = DynamicContainerProps

/**
 * A container that can edit or display a single primitive value.
 */
export default function Value<Value = unknown>({
  children,
  renderValue,
  ...props
}: ValueProps<Value>) {
  const inputId = useId()
  assert(inputId, 'inputId is required')
  return (
    <VisibleOnHover container>
      <ContainerControl {...props} inputId={inputId}>
        <Stack
          direction='row'
          spacing={1}
          alignItems='center'
          sx={{width: '100%'}}
        >
          {renderValue?.()}
          {children}
        </Stack>
      </ContainerControl>
    </VisibleOnHover>
  )
}
