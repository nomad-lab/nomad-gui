import {Box, Button, IconButton, Stack} from '@mui/material'
import React, {
  PropsWithChildren,
  useCallback,
  useMemo,
  useRef,
  useState,
} from 'react'
import {DndProvider} from 'react-dnd'
import {HTML5Backend} from 'react-dnd-html5-backend'

import {SxProps} from '../../../utils/types'
import {assert} from '../../../utils/utils'
import {Remove} from '../../icons'
import PrimitiveProps from '../primitives/PrimitiveProps'
import DisplayString from '../utils/DisplayString'
import SortableItem, {SortableItemProps} from '../utils/SortableItem'
import useProps, {propsContext} from '../utils/useProps'
import ContainerControl from './ContainerControl'
import ContainerProps, {DynamicContainerProps} from './ContainerProps'
import Value from './Value'

type ListItemProps<Value> = {
  index: number
  onValueChange: (value: Value, index: number) => void
  onRemove?: (index: number) => void
  renderValue: () => React.ReactNode
} & PrimitiveProps<Value> &
  SortableItemProps

function UnMemoizedListItem<Value>({
  index,
  onValueChange,
  onRemove,
  renderValue,
  onHover,
  onDropped,
  onCanceled,
  ...primitiveProps
}: ListItemProps<Value>) {
  const {edit} = primitiveProps
  primitiveProps.onChange = (value) => onValueChange(value, index)
  return (
    <propsContext.Provider value={primitiveProps}>
      {edit ? (
        <Stack direction='row' alignItems='center'>
          <SortableItem
            index={index}
            onHover={onHover}
            onDropped={onDropped}
            onCanceled={onCanceled}
          >
            <Value>{renderValue()}</Value>
            {edit && onRemove && (
              <IconButton size='small' onClick={() => onRemove(index)}>
                <Remove />
              </IconButton>
            )}
          </SortableItem>
        </Stack>
      ) : (
        <Value>{renderValue()}</Value>
      )}
    </propsContext.Provider>
  )
}

const MemoizedListItem = React.memo(
  UnMemoizedListItem,
) as typeof UnMemoizedListItem

function ListItem<Value>(props: ListItemProps<Value>) {
  props = useProps(props)
  return <MemoizedListItem {...props} />
}

function ListContainer({
  variant,
  sx,
  children,
}: PropsWithChildren & SxProps & {variant: ListProps['variant']}) {
  const {edit, fullWidth} = useProps<ListProps>()
  return (
    <Box
      sx={{
        ...sx,
        width: fullWidth ? '100%' : undefined,
        display: 'flex',
        rowGap: edit ? 1 : 0,
        ...(variant === 'block'
          ? {flexDirection: 'column'}
          : {
              flexDirection: 'row',
              flexWrap: 'wrap',
              columnGap: 1,
              '& > div:after': {
                alignSelf: 'flex-end',
                paddingY: 0.5,
                content: '", "',
              },
              '& > div:last-child:after': {
                content: '""',
              },
            }),
      }}
    >
      {children}
    </Box>
  )
}

function ListActions({
  variant,
  children,
  onAddItem,
}: PropsWithChildren & {onAddItem: () => void; variant: ListProps['variant']}) {
  const {edit} = useProps<ListProps>()
  return (
    edit && (
      <Box
        sx={{
          alignSelf: 'flex-start',
          ...(variant === 'block' && {marginTop: 1}),
          display: 'flex',
          flexDirection: 'row',
          width: '100%',
        }}
      >
        {children}
        <Box sx={{flexGrow: 10}} />
        <Button
          color='primary'
          variant='contained'
          size='small'
          onClick={onAddItem}
        >
          add item
        </Button>
      </Box>
    )
  )
}

export type ListProps<Value = unknown> = ContainerProps<Value[]> & {
  /**
   * Determines how to render each item value.
   */
  renderValue: (props: PrimitiveProps<Value>) => React.ReactNode
  /**
   * Determines how to create an empty value for added list items.
   * Without this, the list will not have an add or remove item
   * functionality even when edit is true.
   */
  createEmptyValue?: () => Value
  /**
   * The variant of the list. The block variant will show items
   * in a column, the inline variant will show items in a wrapping row.
   */
  variant?: 'block' | 'inline'
} & PropsWithChildren

export type DynamicListProps = DynamicContainerProps &
  Pick<ListProps, 'variant'> & {
    emptyValue?: unknown
  }

/**
 * A value container that allows to display and edit
 * values that are lists of items. The items can be edited, reordered,
 * added and removed.
 */
export default function List<Value = unknown>({
  createEmptyValue,
  renderValue,
  variant = 'block',
  children,
  ...props
}: ListProps<Value>) {
  props = useProps(props)
  const [hover, setHover] = useState({dragged: 0, dropped: 0})
  const {value, onChange, label} = props

  const valueRef = useRef<Value[]>(value || [])
  valueRef.current = value || []

  const handleChangeItemValue = useCallback(
    (changedItemValue: Value, index: number) => {
      const newValue = [...valueRef.current]
      newValue[index] = changedItemValue
      onChange?.(newValue)
    },
    [onChange, valueRef],
  )

  const handleRemoveItem = useCallback(
    (index: number) => {
      const changedValue = [...valueRef.current]
      changedValue.splice(index, 1)
      onChange?.(changedValue)
    },
    [onChange, valueRef],
  )

  const handleAddItem = useCallback(() => {
    const changedValue = [...valueRef.current, createEmptyValue?.() as Value]
    onChange?.(changedValue)
  }, [onChange, createEmptyValue, valueRef])

  const handleHover = useCallback(
    (draggedIndex: number, droppedIndex: number) => {
      setHover((hover) => {
        if (hover.dragged === draggedIndex && hover.dropped === droppedIndex) {
          return hover
        }
        return {
          dragged: draggedIndex,
          dropped: droppedIndex,
        }
      })
    },
    [],
  )

  // shuffledValue is the value that has an dragged items already reshuffled
  // before they are dropped and before onChange is called to change the
  // actual value.
  const shuffledValue = useMemo(() => {
    const shuffledValue = [...(value || [])]
    if (shuffledValue.length === 0) {
      return shuffledValue
    }
    const movedValueItem = shuffledValue[hover.dragged]
    shuffledValue.splice(hover.dragged, 1)
    shuffledValue.splice(hover.dropped, 0, movedValueItem)
    return shuffledValue
  }, [hover.dragged, hover.dropped, value])
  const sortedValueRef = useRef(shuffledValue)
  sortedValueRef.current = shuffledValue

  const handleDropped = useCallback(() => {
    if (
      valueRef.current.some(
        (value, index) => value !== sortedValueRef.current[index],
      )
    ) {
      onChange?.(sortedValueRef.current)
    }
    setHover({dragged: 0, dropped: 0})
  }, [onChange, sortedValueRef, valueRef])

  const handleCanceled = useCallback(() => {
    setHover({dragged: 0, dropped: 0})
  }, [])

  return (
    <>
      <ContainerControl showLabel {...props}>
        <ListContainer
          variant={variant}
          sx={{marginTop: label ? (variant === 'block' ? 3 : 2) : 0}}
        >
          {value?.length === 0 && (
            <DisplayString
              placeholder={
                props.placeholder ||
                (props.label && `no ${props.label}`) ||
                'no items'
              }
            />
          )}
          <DndProvider backend={HTML5Backend} context={window}>
            {shuffledValue?.map((value, index) => {
              const item = (
                <ListItem
                  key={index}
                  value={value as Value}
                  onValueChange={handleChangeItemValue}
                  index={index}
                  onRemove={handleRemoveItem}
                  onHover={handleHover}
                  onDropped={handleDropped}
                  onCanceled={handleCanceled}
                  renderValue={renderValue}
                  onError={undefined} // do not receive errors from individual items
                />
              )
              assert(
                variant === 'block' || variant === 'inline',
                'variant must be "block" or "inline"',
              )
              if (variant === 'block') {
                return item
              } else {
                return (
                  <Box
                    key={index}
                    sx={{
                      display: 'flex',
                      flexDirection: 'row',
                      flexWrap: 'nowrap',
                      alignItems: 'baseline',
                    }}
                  >
                    {item}
                  </Box>
                )
              }
            })}
          </DndProvider>
          <ListActions variant={variant} onAddItem={handleAddItem}>
            {children}
          </ListActions>
        </ListContainer>
      </ContainerControl>
    </>
  )
}
