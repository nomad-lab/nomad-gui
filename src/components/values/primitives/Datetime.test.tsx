import {LocalizationProvider} from '@mui/x-date-pickers'
import {AdapterDateFns} from '@mui/x-date-pickers/AdapterDateFnsV3'
import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {PropsWithChildren} from 'react'
import {describe, expect, it, vi} from 'vitest'

import Datetime, {DatetimeVariant} from './Datetime'

describe('Datetime', () => {
  const renderOptions = {
    wrapper: ({children}: PropsWithChildren) => (
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        {children}
      </LocalizationProvider>
    ),
  }
  it.each([
    ['date', '09/06/2019'],
    ['time', '04:06 PM'],
    ['datetime', '09/06/2019 04:06 PM'],
  ])('renders %s', (variant, expectedValue) => {
    render(
      <Datetime
        variant={variant as DatetimeVariant}
        value='2019-09-06T16:06:36'
      />,
      renderOptions,
    )
    expect(screen.getByText(expectedValue)).toBeInTheDocument()
  })
  it('renders invalid value', () => {
    render(<Datetime value='this is not a datetime' />, renderOptions)
    expect(screen.getByText('Invalid Date')).toBeInTheDocument()
  })
  it('allows to edit invalid value', async () => {
    const handleChange = vi.fn()
    const handleError = vi.fn()
    const testValue = 'this is not a datetime'
    render(
      <Datetime
        variant='date'
        editable
        value={testValue}
        onChange={handleChange}
        onError={handleError}
      />,
      renderOptions,
    )
    const input = screen.getByRole('textbox') as HTMLInputElement

    await userEvent.type(input, '04/06/2024')
    expect(handleChange).toHaveBeenCalled()
    expect(handleError).toHaveBeenCalledWith(undefined)
  })

  it('allows undefined as value and allows editing undefined value', async () => {
    const handleChange = vi.fn()
    const handleError = vi.fn()
    render(
      <Datetime
        variant='date'
        editable
        value={undefined}
        onChange={handleChange}
        onError={handleError}
      />,
      renderOptions,
    )
    const input = screen.getByRole('textbox') as HTMLInputElement

    await userEvent.type(input, '04/06/2024')
    expect(handleChange).toHaveBeenCalled()
    expect(handleError).toHaveBeenCalledWith(undefined)
  })
})
