import entryRoute from '../../../pages/entry/entryRoute'
import {SectionSelectOptions} from '../../navigation/useSelect'
import useRouteData from '../../routing/useRouteData'
import Reference, {DynamicReferenceProps, ReferenceProps} from './Reference'

export type SectionReferenceProps = ReferenceProps &
  Omit<SectionSelectOptions, 'entity'>

export type DynamicSectionReferenceProps = DynamicReferenceProps &
  Omit<SectionSelectOptions, 'entity'>

/**
 * A primitive for displaying and editing references to sections.
 */
export default function SectionReference({
  sectionDefinition,
  ...referenceProps
}: SectionReferenceProps) {
  const {upload_id, entry_id} = useRouteData(entryRoute)

  const selectOptions = {
    entity: 'section',
    sectionDefinition,
  } satisfies SectionSelectOptions

  const initialPath = `/uploads/${upload_id}/entries/${entry_id}/archive`

  return (
    <Reference
      {...referenceProps}
      selectOptions={selectOptions}
      initialPath={initialPath}
    />
  )
}
