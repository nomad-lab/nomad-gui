import React, {FocusEventHandler} from 'react'

import {ActionKey, Actions} from '../utils/dynamicComponents'

type PrimitiveProps<Value = unknown> = {
  /**
   * The value to display. Editable values need to be controlled.
   */
  value?: Value
  /**
   * The change callback to control the value.
   */
  onChange?: (value: Value) => void
  /**
   * Show the primitive in error state. For primitives errors have to be
   * controlled, for containers they can be controlled.
   */
  error?: boolean
  /**
   * A callback to control the error state. The callback is called
   * with an error message when the primitives's internal validation of
   * user provided values fails. It is called with undefined when
   * the validation passes.
   */
  onError?: (helperText: string | undefined) => void
  /**
   * Whether the value is editable and shown as a primitive specific
   * input or displayed as a read-only value by default.
   */
  editable?: boolean
  /**
   * The current edit state. If `editable` the default is true. For
   * Non `editable` values this can never be true.
   * For primitives this has to be controlled, for containers this can be controlled.
   */
  edit?: boolean
  /**
   * The change callback to control the edit state.
   */
  onEditChange?: (edit: boolean) => void
  /**
   * A placeholder that is shown when the value is empty.
   */
  placeholder?: string
  /**
   * Forces the primitive to fill all available horizontal space.
   */
  fullWidth?: boolean
  /**
   * A callback that is called when the editable value loses focus.
   */
  onBlur?: FocusEventHandler<HTMLInputElement>
  /**
   * A callback that is called when the editable value gains focus.
   */
  onFocus?: FocusEventHandler<HTMLInputElement>
  /**
   * Optional ref object that is passed to the underlying input element.
   */
  inputRef?: React.Ref<HTMLInputElement>
  /**
   * Optional actions like <CopyToClipboard />.
   */
  actions?: React.ReactNode | React.ReactNode[]
  /**
   * The unit that the value comes in.
   */
  unit?: string
  /**
   * The unit used to display the value in. This is independent
   * of the unit that might be attached to the value. This value unit
   * will never be changed.
   * For primitives this has to be controlled, for containers this can be controlled.
   */
  displayUnit?: string
  /**
   * To hide the unit, only use unit and displayUnit to show the right
   * value.
   */
  hiddenUnit?: boolean
  /**
   * The change callback to control the display unit.
   */
  onDisplayUnitChange?: (unit: string) => void
  /**
   * Whether the display unit is locked and changes to the unit will
   * change the value or whether the unit is unlocked and changes
   * to the unit, will not effect the value.
   */
  displayUnitLocked?: boolean
  /**
   * The change callback to control the display unit lock state.
   * For primitives this has to be controlled, for containers this can be controlled.
   */
  onDisplayUnitLockedChange?: (locked: boolean) => void

  /**
   * This is only set by the RichTableEditor. The component will render as a
   * variant that is suitable in a table cell.
   */
  table?: boolean
}

export type DynamicPrimitiveProps = Pick<
  PrimitiveProps<never>,
  'error' | 'edit' | 'editable' | 'placeholder' | 'fullWidth'
> & {actions?: Actions | ActionKey | ActionKey[]}

export type NonEditablePrimitiveProps<Value = unknown> = Omit<
  PrimitiveProps<Value>,
  'onBlur' | 'onFocus' | 'onError' | 'onChange'
> & {
  editable?: false
}

export default PrimitiveProps
