import {act, fireEvent, render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {describe, expect, it, vi} from 'vitest'

import {Entity} from '../../navigation/useSelect'
import {createSelectMock, renderOptions} from './Reference.helper'

describe('Reference', async () => {
  const testValue = './entity'
  createSelectMock('/parent/selectedEntity')
  const {default: Reference} = await import('./Reference')
  const referenceProps = {
    selectOptions: {entity: 'testEntity' as Entity},
    initialPath: '/parent',
  }

  it('displays value', async () => {
    render(<Reference value={testValue} {...referenceProps} />, renderOptions)
    const text = screen.getByText(testValue)
    expect(text.closest('a')).toHaveAttribute('href', testValue)
  })

  it('displays value editable and edit false', () => {
    render(
      <Reference value={testValue} editable edit={false} {...referenceProps} />,
      renderOptions,
    )
    const text = screen.getByText(testValue)
    expect(text.closest('a')).toHaveAttribute('href', testValue)
  })

  it('allows to edit', async () => {
    const handleChange = vi.fn()
    const handleError = vi.fn()
    render(
      <Reference
        editable
        value={testValue}
        onChange={handleChange}
        onError={handleError}
        {...referenceProps}
      />,
      renderOptions,
    )
    const input = screen.getByDisplayValue(testValue) as HTMLInputElement
    await userEvent.type(
      input,
      '{backspace}'.repeat(testValue.length) + 'editedEntity{enter}',
    )
    expect(handleChange).toHaveBeenCalledWith('editedEntity')

    await userEvent.type(
      input,
      '{backspace}'.repeat('editedEntity'.length) + '{enter}',
    )
    expect(handleChange).toHaveBeenCalledWith(undefined)

    expect(handleError).not.toHaveBeenCalled()
  })

  it.each([
    ['./value', '/parent/value'],
    ['/value', '/value'],
  ])('allows to open with path %s', (path, expectedUrl) => {
    const open = vi.spyOn(window.history, 'pushState')
    open.mockImplementation(() => null)
    render(
      <Reference value={path} editable {...referenceProps} />,
      renderOptions,
    )
    act(() => fireEvent.click(screen.getByRole('button', {name: 'Open'})))
    expect(open).toHaveBeenCalledWith(null, '', expectedUrl)
  })

  it('allows to select an entity', () => {
    const handleChange = vi.fn()
    const handleError = vi.fn()
    render(
      <Reference
        editable
        value={testValue}
        onChange={handleChange}
        onError={handleError}
        {...referenceProps}
      />,
      renderOptions,
    )
    act(() => fireEvent.click(screen.getByRole('button', {name: 'Select'})))
    expect(handleChange).toHaveBeenCalledWith('./selectedEntity')
    expect(handleError).not.toHaveBeenCalled()
  })
})
