import type {Meta, StoryObj} from '@storybook/react'
import {useCallback} from 'react'

import CopyToClipboard from '../actions/CopyToClipboard'
import List, {ListProps} from '../containers/List'
import Value, {ValueProps} from '../containers/Value'
import {baseDecorator, editorDecorator} from '../utils/storyDecorators.helper'
import Link from './Link'

const meta = {
  title: 'components/values/primitives/Link',
  component: Link,
  decorators: [editorDecorator, baseDecorator],
  tags: ['autodocs'],
} satisfies Meta<typeof Link>

export default meta

const defaultArgsWithoutValue = {
  editable: true,
  placeholder: 'placeholder',
  fullWidth: true,
}

const defaultArgs = {
  value: 'https://example.com',
  ...defaultArgsWithoutValue,
}

type Story = StoryObj<typeof meta>
export const Editable: Story = {
  args: {
    ...defaultArgs,
  },
}

export const NotEditable: Story = {
  args: {
    ...defaultArgs,
    editable: false,
  },
}

export const Error: Story = {
  args: {
    ...defaultArgs,
    error: true,
  },
}

export const InitialWidth: Story = {
  args: {
    ...defaultArgs,
    fullWidth: false,
  },
}

export const InValue: Story = {
  parameters: {
    render: (props: ValueProps, story: React.ReactNode) => (
      <Value
        helperText='Helper text'
        label='label'
        {...props}
        {...defaultArgsWithoutValue}
      >
        {story}
      </Value>
    ),
    value: defaultArgs.value,
  },
  args: {
    actions: <CopyToClipboard />,
  },
}

export const InList: Story = {
  parameters: {
    render: function Render(props: ListProps, story: React.ReactNode) {
      const renderValue = useCallback(() => story, [story])
      return (
        <List
          helperText='Helper text'
          label='label'
          {...props}
          renderValue={renderValue}
          createEmptyValue={() => 0}
          {...defaultArgsWithoutValue}
        />
      )
    },
    value: [
      'https://example.org',
      'https://nomad-lab.eu',
      'https://fairmat.eu',
    ],
  },
  args: {
    actions: <CopyToClipboard />,
  },
}
