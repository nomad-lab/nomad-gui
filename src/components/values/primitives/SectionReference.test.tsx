import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {describe, expect, it, vi} from 'vitest'

import {
  createSelectMock,
  createUseRouteDataMock,
  renderOptions,
} from './Reference.helper'

describe('SectionReference', async () => {
  const testValue = './section'

  createSelectMock('/uploads/uploadId/entries/entryId/archive/selectedSection')
  createUseRouteDataMock({
    upload_id: 'uploadId',
    entry_id: 'entryId',
  })
  const {default: SectionReference} = await import('./SectionReference')

  it('allows to select a file', async () => {
    const handleChange = vi.fn()
    const handleError = vi.fn()
    render(
      <SectionReference
        editable
        value={testValue}
        onChange={handleChange}
        onError={handleError}
      />,
      renderOptions,
    )
    await userEvent.click(screen.getByRole('button', {name: 'Select'}))
    expect(handleChange).toHaveBeenCalledWith('./selectedSection')
    expect(handleError).not.toHaveBeenCalled()
  })
})
