import {LocalizationProvider} from '@mui/x-date-pickers'
import {AdapterDateFns} from '@mui/x-date-pickers/AdapterDateFnsV3'
import type {Meta, StoryObj} from '@storybook/react'

import {baseDecorator} from '../utils/storyDecorators.helper'
import RelativeTime from './RelativeTime'

const meta = {
  title: 'components/values/primitives/RelativeTime',
  component: RelativeTime,
  decorators: [
    baseDecorator,
    (Story) => (
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <Story />
      </LocalizationProvider>
    ),
  ],
  tags: ['autodocs'],
} satisfies Meta<typeof RelativeTime>

export default meta

type Story = StoryObj<typeof meta>
export const WithSuffix: Story = {
  args: {
    value: '1978-04-08',
  },
}

export const WithoutSuffix: Story = {
  args: {
    value: '1978-04-08',
    withoutSuffix: true,
  },
}
