import {act, fireEvent, render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {describe, expect, it, vi} from 'vitest'

import EnumRadio from './EnumRadio'

describe('EnumRadio', () => {
  const values = ['one', 'two', 'three']
  it.each([
    ['one', 'one'],
    [undefined, 'placeholder'],
  ])('displays value %s', (testValue, expected) => {
    render(
      <EnumRadio values={values} value={testValue} placeholder='placeholder' />,
    )
    expect(screen.getByText(expected)).toBeInTheDocument()
  })
  it('displays value editable and edit false', () => {
    render(<EnumRadio values={values} value='one' editable edit={false} />)
    expect(screen.getByText('one')).toBeInTheDocument()
  })
  it.each([
    ['allowNone and undefined', true, undefined],
    ['allowNone and one', true, 'one'],
    ['value', false, 'one'],
    ['undefined', false, undefined],
  ])('allows to edit with %s', async (description, allowNone, value) => {
    const handleChange = vi.fn()
    const handleError = vi.fn()
    render(
      <EnumRadio
        allowNone={allowNone}
        values={values}
        placeholder='placeholder'
        editable
        onChange={handleChange}
        onError={handleError}
        value={value}
      />,
    )
    const oneLabel = screen.getByText('one').closest('label') as HTMLElement
    const twoLabel = screen.getByText('two').closest('label') as HTMLElement
    const noValueLabel = screen
      .queryByText('placeholder')
      ?.closest('label') as HTMLElement
    if (allowNone) {
      expect(noValueLabel).toBeInTheDocument()
    }

    if (value === 'one') {
      expect(oneLabel?.firstChild).toHaveClass('Mui-checked')
    } else {
      expect(oneLabel?.firstChild).not.toHaveClass('Mui-checked')
    }
    expect(twoLabel?.firstChild).not.toHaveClass('Mui-checked')
    if (allowNone) {
      if (value === undefined) {
        expect(noValueLabel?.firstChild).toHaveClass('Mui-checked')
      } else {
        expect(noValueLabel?.firstChild).not.toHaveClass('Mui-checked')
      }
    }

    act(() => fireEvent.click(twoLabel))
    expect(handleChange).toHaveBeenCalledWith('two')

    if (allowNone) {
      handleChange.mockClear()
      await userEvent.click(noValueLabel)
      act(() => fireEvent.click(noValueLabel))
      if (value === undefined) {
        expect(handleChange).not.toHaveBeenCalled()
      } else {
        expect(handleChange).toHaveBeenCalledWith(undefined)
      }
    }

    expect(handleError).not.toHaveBeenCalled()
  })
})
