import type {Meta, StoryObj} from '@storybook/react'
import {useCallback} from 'react'

import apiData from '../../../mocks/syntheticApi.random.data.json'
import CopyToClipboard from '../actions/CopyToClipboard'
import List, {ListProps} from '../containers/List'
import Value, {ValueProps} from '../containers/Value'
import {
  baseDecorator,
  createRouteDecorator,
  editorDecorator,
} from '../utils/storyDecorators.helper'
import FileReference from './FileReference'

const uploadId = Object.values(apiData.uploads)[0].upload_id
const entryId = Object.values(Object.values(apiData.uploads)[0].entries)[0]
  .entry_id

const meta = {
  title: 'components/values/primitives/FileReference',
  component: FileReference,
  decorators: [
    editorDecorator,
    createRouteDecorator(uploadId, entryId),
    baseDecorator,
  ],
  tags: ['autodocs'],
} satisfies Meta<typeof FileReference>

export default meta

const defaultArgsWithoutValue = {
  editable: true,
  placeholder: 'placeholder',
  fullWidth: true,
}

const defaultArgs = {
  value: 'example.txt',
  ...defaultArgsWithoutValue,
}

type Story = StoryObj<typeof meta>
export const Editable: Story = {
  args: {
    ...defaultArgs,
  },
}

export const NotEditable: Story = {
  args: {
    ...defaultArgs,
    editable: false,
  },
}

export const Error: Story = {
  args: {
    ...defaultArgs,
    error: true,
  },
}

export const InitialWidth: Story = {
  args: {
    ...defaultArgs,
    fullWidth: false,
  },
}

export const InValue: Story = {
  parameters: {
    render: (props: ValueProps, story: React.ReactNode) => (
      <Value
        helperText='Helper text'
        label='label'
        {...props}
        {...defaultArgsWithoutValue}
      >
        {story}
      </Value>
    ),
    value: defaultArgs.value,
  },
  args: {
    actions: <CopyToClipboard />,
  },
}

export const InList: Story = {
  parameters: {
    render: function Render(props: ListProps, story: React.ReactNode) {
      const renderValue = useCallback(() => story, [story])
      return (
        <List
          helperText='Helper text'
          label='label'
          {...props}
          renderValue={renderValue}
          createEmptyValue={() => 0}
          {...defaultArgsWithoutValue}
        />
      )
    },
    value: ['example.txt', 'nomad-lab.txt', 'fairmat.txt'],
  },
  args: {
    actions: <CopyToClipboard />,
  },
}
