import {Button} from '@mui/material'
import {PropsWithChildren} from 'react'
import {vi} from 'vitest'

import {JSONObject} from '../../../utils/types'
import Router from '../../routing/Router'
import {Route} from '../../routing/types'

export function createSelectMock(selectPath: string) {
  vi.doMock('../../navigation/Select', () => ({
    SelectDialogButton: ({onSelect}: {onSelect: (path: string) => void}) => {
      return (
        <Button title='Select' onClick={() => onSelect(selectPath)}>
          Select
        </Button>
      )
    },
  }))
}

export function createUseRouteDataMock(routeData: JSONObject) {
  vi.doMock('../../routing/useRouteData', () => ({
    default: () => routeData,
  }))
}

export const renderOptions = {
  wrapper: ({children}: PropsWithChildren) => {
    const recursiveRoute: Route = {
      path: ':id',
      component: () => children,
    }
    recursiveRoute.children = [recursiveRoute]
    const route = {
      path: '',
      children: [recursiveRoute],
    }
    return <Router route={route} />
  },
}
