import {LocalizationProvider} from '@mui/x-date-pickers'
import {AdapterDateFns} from '@mui/x-date-pickers/AdapterDateFnsV3'
import {render, screen} from '@testing-library/react'
import {PropsWithChildren} from 'react'
import {describe, expect, it} from 'vitest'

import RelativeTime from './RelativeTime'

describe('RelativeTime', () => {
  const renderOptions = {
    wrapper: ({children}: PropsWithChildren) => (
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        {children}
      </LocalizationProvider>
    ),
  }
  it('renders valid date', () => {
    render(
      <RelativeTime
        value='2019-09-06T16:06:36'
        referenceTimeValue='2019-09-14'
      />,
      renderOptions,
    )
    expect(screen.getByText('7 days ago')).toBeInTheDocument()
  })
  it('renders from now', () => {
    render(<RelativeTime value='2019-09-06T16:06:36' />, renderOptions)
    expect(screen.getByText(/ago/)).toBeInTheDocument()
  })
})
