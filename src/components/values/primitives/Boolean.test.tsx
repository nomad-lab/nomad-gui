import {act, fireEvent, render, screen} from '@testing-library/react'
import {describe, expect, it, vi} from 'vitest'

import Boolean from './Boolean'

describe('Boolean', () => {
  it.each([
    [true, 'true'],
    [false, 'false'],
    [undefined, 'placeholder'],
  ])('displays value %s', (testValue, expected) => {
    render(<Boolean value={testValue} placeholder='placeholder' />)
    expect(screen.getByText(expected)).toBeInTheDocument()
  })
  it('displays value editable and edit false', () => {
    render(<Boolean value={true} editable edit={false} />)
    expect(screen.getByText('true')).toBeInTheDocument()
  })
  it('allows to edit invalid and undefined values', async () => {
    const handleChange = vi.fn()
    const handleError = vi.fn()
    render(
      <Boolean
        placeholder='placeholder'
        allowNone
        editable
        value={true}
        onChange={handleChange}
        onError={handleError}
      />,
    )
    const trueLabel = screen.getByText('true').closest('label') as HTMLElement
    const falseLabel = screen.getByText('false').closest('label') as HTMLElement
    const noValueLabel = screen
      .getByText('placeholder')
      .closest('label') as HTMLElement

    expect(trueLabel?.firstChild).toHaveClass('Mui-checked')
    expect(falseLabel?.firstChild).not.toHaveClass('Mui-checked')
    expect(noValueLabel?.firstChild).not.toHaveClass('Mui-checked')

    act(() => fireEvent.click(falseLabel))
    expect(handleChange).toHaveBeenCalledWith(false)

    act(() => fireEvent.click(noValueLabel))
    expect(handleChange).toHaveBeenCalledWith(undefined)

    expect(handleError).not.toHaveBeenCalled()
  })
})
