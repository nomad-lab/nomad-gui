import {
  FormControlLabel,
  Radio,
  RadioGroup,
  RadioGroupProps,
} from '@mui/material'
import {ChangeEvent, useCallback} from 'react'

import {assert} from '../../../utils/utils'
import {ValueFormControl} from '../containers/ContainerControl'
import {ControlProps} from '../containers/ContainerProps'
import useProps from '../utils/useProps'
import PrimitiveProps, {DynamicPrimitiveProps} from './PrimitiveProps'
import Text from './Text'

export type EnumRadioProps = {
  /**
   * If true, the editable component will offer a `placeholder`, e.g.
   * "no value" option.
   */
  allowNone?: boolean

  /**
   * The possible values for the enum.
   */
  values: string[]
} & Pick<RadioGroupProps, 'row'> &
  PrimitiveProps<string | undefined>

export type DynamicEnumProps = DynamicPrimitiveProps &
  Pick<EnumRadioProps, 'allowNone' | 'values' | 'row'>

export default function EnumRadio(props: EnumRadioProps) {
  const {
    row,
    allowNone,
    values,
    value,
    onChange,
    edit,
    editable,
    onError,
    inputId,
    ...controlProps
  } = useProps<EnumRadioProps & ControlProps>(props)
  const {label, placeholder} = controlProps
  const showEdit = edit === undefined ? editable : edit

  let stringValue
  if (value === undefined) {
    if (allowNone && showEdit) {
      stringValue = '__noValue__'
    } else {
      stringValue = undefined
    }
  } else {
    stringValue = value
  }

  const handleChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>, value: string) => {
      if (value === '__noValue__') {
        onChange?.(undefined)
        return
      }
      assert(values.includes(value), `Invalid value: ${value}`)
      onChange?.(value)
    },
    [onChange, values],
  )

  if (showEdit) {
    return (
      <ValueFormControl variant='standard' {...controlProps}>
        <RadioGroup
          sx={{marginTop: label ? 2 : 0}}
          row={row}
          value={stringValue}
          onChange={handleChange}
          id={inputId}
        >
          {values.map((value) => (
            <FormControlLabel
              key={value}
              value={value}
              control={<Radio />}
              label={value}
            />
          ))}
          {allowNone && (
            <FormControlLabel
              sx={{color: (theme) => theme.palette.action.active}}
              value='__noValue__'
              control={<Radio />}
              label={placeholder || 'no value'}
            />
          )}
        </RadioGroup>
      </ValueFormControl>
    )
  } else {
    return <Text value={stringValue} {...controlProps} />
  }
}
