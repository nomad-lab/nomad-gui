import {LocalizationProvider} from '@mui/x-date-pickers'
import {AdapterDateFns} from '@mui/x-date-pickers/AdapterDateFnsV3'
import type {Meta, StoryObj} from '@storybook/react'
import {useCallback} from 'react'

import CopyToClipboard from '../actions/CopyToClipboard'
import List, {ListProps} from '../containers/List'
import Value, {ValueProps} from '../containers/Value'
import {baseDecorator, editorDecorator} from '../utils/storyDecorators.helper'
import Datetime from './Datetime'

const meta = {
  title: 'components/values/primitives/Datetime',
  component: Datetime,
  decorators: [
    editorDecorator,
    baseDecorator,
    (Story) => (
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <Story />
      </LocalizationProvider>
    ),
  ],
  tags: ['autodocs'],
} satisfies Meta<typeof Datetime>

export default meta

const defaultArgsWithoutValue = {
  editable: true,
  error: undefined,
  fullWidth: true,
}

const defaultArgs = {
  value: new Date().toISOString(),
  ...defaultArgsWithoutValue,
}

type Story = StoryObj<typeof meta>
export const EditableDate: Story = {
  args: {
    ...defaultArgs,
  },
}

export const EditableDatetime: Story = {
  args: {
    ...defaultArgs,
    variant: 'datetime',
  },
}

export const NotEditableDate: Story = {
  args: {
    ...defaultArgs,
    editable: false,
  },
}

export const NotEditableDatetime: Story = {
  args: {
    ...defaultArgs,
    editable: false,
    variant: 'datetime',
  },
}

export const Error: Story = {
  args: {
    ...defaultArgs,
    error: true,
  },
}

export const Placeholder: Story = {
  args: {
    ...defaultArgs,
    value: undefined,
  },
}

export const Ellipsis: Story = {
  args: {
    ...defaultArgs,
    editable: false,
    fullWidth: true,
  },
}

export const InitialWidth: Story = {
  args: {
    ...defaultArgs,
    fullWidth: false,
  },
}

export const InValue: Story = {
  parameters: {
    render: (props: ValueProps, story: React.ReactNode) => (
      <Value
        helperText='Helper text'
        label='label'
        {...props}
        {...defaultArgsWithoutValue}
      >
        {story}
      </Value>
    ),
    value: defaultArgs.value,
  },
  args: {
    actions: <CopyToClipboard />,
  },
}

export const InList: Story = {
  parameters: {
    render: function Render(props: ListProps, story: React.ReactNode) {
      const renderValue = useCallback(() => story, [story])
      return (
        <List
          helperText='Helper text'
          label='label'
          {...props}
          renderValue={renderValue}
          {...defaultArgsWithoutValue}
        />
      )
    },
    value: [new Date().toISOString(), new Date().toISOString()],
  },
  args: {
    actions: <CopyToClipboard />,
  },
}
