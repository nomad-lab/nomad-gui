import {render, screen, waitFor, within} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {describe, expect, it} from 'vitest'

import Id from './Id'

describe('Id', () => {
  it('renders', () => {
    render(<Id value='Aii9iGX1Z42VS3SF3' />)
    expect(screen.getByText('Aii9iGX1Z42VS3SF3')).toBeInTheDocument()
  })

  it('renders abbreviated value with tooltip', async () => {
    render(<Id value='Aii9iGX1Z42VS3SF3' abbreviated />)
    const text = screen.getByText('Aii9iGX1')
    expect(text).toBeInTheDocument()
    await userEvent.hover(text)
    await waitFor(() =>
      expect(screen.queryByRole('tooltip')).toBeInTheDocument(),
    )
    const tooltip = screen.getByRole('tooltip')
    expect(within(tooltip).getByText('Aii9iGX1Z42VS3SF3')).toBeInTheDocument()
  })
})
