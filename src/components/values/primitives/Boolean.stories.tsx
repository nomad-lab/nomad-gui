import type {Meta, StoryObj} from '@storybook/react'

import CopyToClipboard from '../actions/CopyToClipboard'
import Value, {ValueProps} from '../containers/Value'
import {baseDecorator, editorDecorator} from '../utils/storyDecorators.helper'
import Boolean from './Boolean'

const meta = {
  title: 'components/values/primitives/Boolean',
  component: Boolean,
  decorators: [editorDecorator, baseDecorator],
  tags: ['autodocs'],
  parameters: {
    controls: {exclude: ['actions']},
  },
} satisfies Meta<typeof Boolean>

export default meta

const defaultArgsWithoutValue = {
  editable: true,
  error: false,
  placeholder: 'placeholder',
  fullWidth: true,
}

const defaultArgs = {
  value: true,
  ...defaultArgsWithoutValue,
}

type Story = StoryObj<typeof meta>
export const Editable: Story = {
  args: {
    ...defaultArgs,
  },
}

export const NotEditable: Story = {
  args: {
    ...defaultArgs,
    editable: false,
  },
}

export const EditableAllowNone: Story = {
  args: {
    ...defaultArgs,
    allowNone: true,
    placeholder: 'no value',
  },
}

export const Error: Story = {
  args: {
    ...defaultArgs,
    error: true,
  },
}

export const Placeholder: Story = {
  args: {
    ...defaultArgs,
    value: undefined,
  },
}

export const InitialWidth: Story = {
  args: {
    ...defaultArgs,
    fullWidth: false,
  },
}

export const InValue: Story = {
  parameters: {
    render: (props: ValueProps, story: React.ReactNode) => (
      <Value
        helperText='Helper text'
        label='label'
        {...props}
        {...defaultArgsWithoutValue}
      >
        {story}
      </Value>
    ),
    value: defaultArgs.value,
  },
  args: {
    actions: <CopyToClipboard />,
  },
}
