import {act, fireEvent, render, screen} from '@testing-library/react'
import {describe, expect, it, vi} from 'vitest'

import {
  createSelectMock,
  createUseRouteDataMock,
  renderOptions,
} from './Reference.helper'

describe('FileReference', async () => {
  const testValue = './testFile.txt'

  createSelectMock('/uploads/uploadId/files/testFile.png')
  createUseRouteDataMock({
    upload_id: 'uploadId',
    mainfile_path: '/mainfile',
  })
  const {default: FileReference} = await import('./FileReference')

  it('allows to select a file', () => {
    const handleChange = vi.fn()
    const handleError = vi.fn()
    render(
      <FileReference
        editable
        value={testValue}
        onChange={handleChange}
        onError={handleError}
      />,
      renderOptions,
    )
    act(() => fireEvent.click(screen.getByRole('button', {name: 'Select'})))
    expect(handleChange).toHaveBeenCalledWith('./testFile.png')
    expect(handleError).not.toHaveBeenCalled()
  })

  it.each([
    ['./value', '/uploads/uploadId/files/value'],
    ['/value', '/value'],
  ])('uses the right open url for %s', (path, expectedUrl) => {
    const open = vi.spyOn(window.history, 'pushState')
    open.mockImplementation(() => null)
    render(<FileReference value={path} editable />, renderOptions)
    act(() => fireEvent.click(screen.getByRole('button', {name: 'Open'})))
    expect(open).toHaveBeenCalledWith(null, '', expectedUrl)
  })
})
