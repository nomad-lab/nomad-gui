import type {Meta, StoryObj} from '@storybook/react'
import {useCallback} from 'react'

import CopyToClipboard from '../actions/CopyToClipboard'
import List, {ListProps} from '../containers/List'
import Value, {ValueProps} from '../containers/Value'
import {baseDecorator, editorDecorator} from '../utils/storyDecorators.helper'
import Text from './Text'

const meta = {
  title: 'components/values/primitives/Text',
  component: Text,
  decorators: [editorDecorator, baseDecorator],
  tags: ['autodocs'],
  parameters: {
    controls: {exclude: ['actions']},
  },
} satisfies Meta<typeof Text>

export default meta

const defaultArgsWithoutValue = {
  editable: true,
  error: false,
  placeholder: 'placeholder',
  fullWidth: true,
}

const defaultArgs = {
  value: 'Hello World',
  ...defaultArgsWithoutValue,
}

type Story = StoryObj<typeof meta>
export const Editable: Story = {
  args: {
    ...defaultArgs,
  },
}

export const NotEditable: Story = {
  args: {
    ...defaultArgs,
    editable: false,
  },
}

export const Error: Story = {
  args: {
    ...defaultArgs,
    error: true,
  },
}

export const Placeholder: Story = {
  args: {
    ...defaultArgs,
    value: '',
  },
}

export const Ellipsis: Story = {
  args: {
    ...defaultArgs,
    value:
      'This is a longer, longer, longer, longer, longer, longer value string',
    editable: false,
    fullWidth: true,
  },
}

export const InitialWidth: Story = {
  args: {
    ...defaultArgs,
    fullWidth: false,
  },
}

export const InValue: Story = {
  parameters: {
    render: (props: ValueProps, story: React.ReactNode) => (
      <Value
        helperText='Helper text'
        label='label'
        {...props}
        {...defaultArgsWithoutValue}
      >
        {story}
      </Value>
    ),
    value: defaultArgs.value,
  },
  args: {
    actions: <CopyToClipboard />,
  },
}

export const InList: Story = {
  parameters: {
    render: function Render(props: ListProps, story: React.ReactNode) {
      const renderValue = useCallback(() => <>{story}</>, [story])
      return (
        <List
          helperText='Helper text'
          label='label'
          {...props}
          renderValue={renderValue}
          createEmptyValue={() => ''}
          {...defaultArgsWithoutValue}
        />
      )
    },
    value: ['one', 'two', 'three'],
  },
  args: {
    actions: <CopyToClipboard />,
  },
}
