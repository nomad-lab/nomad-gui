import {Box, TextField} from '@mui/material'
import {FocusEvent, useCallback} from 'react'

import useLocalState from '../../../hooks/useLocalState'
import {JSONValue} from '../../../utils/types'
import {assert} from '../../../utils/utils'
import JsonViewer from '../../fileviewer/JsonViewer'
import {ControlProps} from '../containers/ContainerProps'
import useProps from '../utils/useProps'
import PrimitiveProps, {DynamicPrimitiveProps} from './PrimitiveProps'

export type JsonDataProps = PrimitiveProps<JSONValue | undefined>

export type DynamicJsonDataProps = DynamicPrimitiveProps

export default function JsonData({...props}: JsonDataProps) {
  const {
    edit,
    editable,
    error,
    onError,
    onChange,
    placeholder,
    value,
    fullWidth,
    onBlur,
    onFocus,
    actions,
    inputId,
  } = useProps<JsonDataProps & ControlProps>(props)
  const showEdit = edit === undefined ? editable : edit
  const toInputString = useCallback((data: JSONValue | undefined) => {
    if (data === undefined) {
      return ''
    }
    return JSON.stringify(data, null, 2)
  }, [])
  const [inputValue, setInputValue, inputValueRef] = useLocalState(value, {
    transform: toInputString,
  })
  const handleInputChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setInputValue(event.target.value)
    },
    [setInputValue],
  )
  const validate = useCallback(
    (inputValue: string) => {
      if (inputValue === '') {
        onChange?.(undefined)
        onError?.(undefined)
      }
      try {
        const changedValue = JSON.parse(inputValue)
        onChange?.(changedValue)
        onError?.(undefined)
        setInputValue(toInputString(changedValue))
      } catch (error) {
        assert(error instanceof Error, 'Invalid JSON')
        onError?.(error.message)
      }
    },
    [onError, onChange, setInputValue, toInputString],
  )

  const handleBlur = useCallback(
    (event: FocusEvent<HTMLInputElement>) => {
      validate(inputValueRef.current)
      onBlur?.(event)
    },
    [onBlur, validate, inputValueRef],
  )

  const handleKeyDown = useCallback(
    (event: React.KeyboardEvent<HTMLInputElement>) => {
      if (event.key === 'Enter' && event.shiftKey === true) {
        event.stopPropagation()
        event.preventDefault()
        validate(inputValueRef.current)
      }
    },
    [validate, inputValueRef],
  )

  let element
  if (showEdit) {
    element = (
      <TextField
        sx={{
          width: '100%',
          display: 'flex',
          flexDirection: 'row',
          '& .MuiInputBase-root': {
            flexGrow: 1,
          },
        }}
        margin='none'
        hiddenLabel
        multiline
        variant='filled'
        error={error}
        fullWidth={fullWidth}
        value={inputValue}
        placeholder={placeholder}
        onBlur={handleBlur}
        onFocus={onFocus}
        onKeyDown={handleKeyDown}
        onChange={handleInputChange}
        inputProps={{
          style: {
            width: '100%',
            fontFamily: 'monospace',
            textWrap: 'nowrap',
            whiteSpace: 'pre',
            overflow: 'auto',
          },
          id: inputId,
        }}
      />
    )
  } else {
    element = (
      <Box
        sx={{
          width: fullWidth ? '100%' : undefined,
          marginY: 1,
          overflow: 'auto',
        }}
      >
        <JsonViewer value={value} />
      </Box>
    )
  }
  return (
    <Box sx={{position: 'relative', width: '100%'}}>
      {element}
      <Box sx={{position: 'absolute', top: 16, right: 8}}>{actions}</Box>
    </Box>
  )
}
