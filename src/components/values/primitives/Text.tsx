import {FilledInput, InputBase, InputProps, styled} from '@mui/material'
import React, {FocusEvent, useCallback} from 'react'
import {useDebounce, useLatest} from 'react-use'

import useLocalState from '../../../hooks/useLocalState'
import {SxProps} from '../../../utils/types'
import {sharedThemeOptions} from '../../theme/themeOptions'
import {ValueFormControl} from '../containers/ContainerControl'
import {ControlProps} from '../containers/ContainerProps'
import DisplayString, {DisplayStringProps} from '../utils/DisplayString'
import useProps from '../utils/useProps'
import PrimitiveProps, {DynamicPrimitiveProps} from './PrimitiveProps'

export type TextProps = PrimitiveProps<string | undefined> & {
  /**
   * An optional display value that is displayed instead
   * of the actual value. This might be useful to display simplified
   * values, while still passing the real values to actions like
   * `CopyToClipboard`.
   */
  displayValue?: string
  /**
   * Optional props to pass to the DisplayString component when not editable.
   */
  displayStringProps?: Partial<DisplayStringProps>
  /**
   * Optional props to pass to the TextField component when editable.
   */
  inputProps?: Partial<InputProps>
  /**
   * Text is only calling `onChange` when the user submits the input via blur
   * or pressing enter. This callback is called on every input change.
   */
  onInputChange?: (value: string) => void
  /**
   * Text is only calling `onChange` when the user submits the input via blur
   * or pressing enter. This callback is called when the user submits the input.
   */
  onSubmit?: (value: string) => void
  /**
   * Also submit directly on typing after the given debounce (in ms).
   * Default is 500. Set to -1 for no submit directly after typing.
   */
  submitOnTypingDelay?: number

  variant?: 'h1' | 'h2' | 'standard'
} & SxProps

const variants = {
  standard: FilledInput,
  h1: styled(FilledInput)(({theme}) => ({
    color: theme.palette.primary.main,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ...(sharedThemeOptions as any).typography.h1,
    lineHeight: '1em',
  })),
  h2: styled(FilledInput)(({theme}) => ({
    color: theme.palette.primary.main,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ...(sharedThemeOptions as any).typography.h2,
    lineHeight: '1em',
  })),
}

export type DynamicTextProps = DynamicPrimitiveProps &
  Pick<TextProps, 'displayValue'>

/**
 * A primitive to display and edit simple string values. Displays unwrapped
 * strings and allows editing in an input field.
 */
export default function Text({
  variant = 'standard',
  displayValue,
  displayStringProps,
  inputRef,
  inputProps,
  submitOnTypingDelay = 500,
  sx,
  table,
  ...props
}: TextProps) {
  const {
    edit,
    editable,
    error,
    onChange,
    placeholder,
    value,
    fullWidth,
    onBlur,
    onFocus,
    onInputChange,
    onSubmit,
    label,
    labelActions,
    actions,
    inputId,
  } = useProps<TextProps & ControlProps>(props)
  const showEdit = edit === undefined ? editable : edit
  const latestValue = useLatest(value)
  const [inputValue, setInputValue, inputValueRef] = useLocalState(value, {
    onChange: onInputChange,
  })
  const handleInputChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setInputValue(event.target.value)
    },
    [setInputValue],
  )

  const handleSubmit = useCallback(() => {
    const value = inputValueRef.current
    if (inputValueRef.current !== latestValue.current) {
      onChange?.(value === '' ? undefined : value)
    }
    onSubmit?.(value || '')
  }, [inputValueRef, latestValue, onChange, onSubmit])

  const handleBlur = useCallback(
    (event: FocusEvent<HTMLInputElement>) => {
      handleSubmit()
      onBlur?.(event)
    },
    [onBlur, handleSubmit],
  )

  const handleKeyDown = useCallback(
    (event: React.KeyboardEvent<HTMLInputElement>) => {
      if (event.key === 'Enter') {
        handleSubmit()
      }
    },
    [handleSubmit],
  )

  useDebounce(
    () => {
      if (submitOnTypingDelay >= 0 && inputValue !== latestValue.current) {
        handleSubmit()
      }
    },
    submitOnTypingDelay <= 0 ? (2 ^ 31) - 1 : submitOnTypingDelay,
    [inputValue, latestValue, handleSubmit],
  )

  const InputComponent = table ? InputBase : variants[variant]

  const primitiveElement = showEdit ? (
    <InputComponent
      value={inputValue || ''}
      onChange={handleInputChange}
      placeholder={placeholder}
      onKeyDown={handleKeyDown}
      fullWidth={fullWidth}
      sx={sx}
      size='small'
      endAdornment={actions}
      inputRef={inputRef}
      id={inputId}
      {...inputProps}
    />
  ) : (
    <DisplayString
      variant={variant === 'standard' ? 'body1' : (variant as 'h1' | 'h2')}
      placeholder={placeholder}
      tooltip={displayValue && value}
      {...displayStringProps}
      sx={{
        marginTop: label ? 2 : 0,
        padding: '4px 0 4px',
        ...sx,
      }}
      color={error ? 'error' : 'text.primary'}
      endAdornment={actions}
      aria-labelledby={inputId && `${inputId}-label`}
    >
      {displayValue || value || ''}
    </DisplayString>
  )

  return (
    <ValueFormControl
      variant={showEdit ? 'filled' : 'standard'}
      fullWidth={fullWidth}
      hiddenLabel={label === undefined}
      error={error}
      onBlur={handleBlur}
      onFocus={onFocus}
      size='small'
      margin='none'
      label={label}
      labelActions={labelActions}
    >
      {primitiveElement}
    </ValueFormControl>
  )
}
