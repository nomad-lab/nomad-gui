import useProps from '../utils/useProps'
import Text, {DynamicTextProps, TextProps} from './Text'

export type IdProps = {
  /**
   * Display an abbreviated id.
   */
  abbreviated?: boolean
} & TextProps

export type DynamicIdProps = DynamicTextProps & Pick<IdProps, 'abbreviated'>

/**
 * A primitive to display and edit for ids.
 */
export default function Id({abbreviated, ...props}: IdProps) {
  const {value} = useProps(props)
  const displayValue = abbreviated ? value?.substring(0, 8) : value
  return (
    <Text
      {...props}
      displayValue={displayValue}
      displayStringProps={{variant: 'monospace'}}
    />
  )
}
