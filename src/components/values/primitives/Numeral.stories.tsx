import type {Meta, StoryObj} from '@storybook/react'
import {useCallback} from 'react'

import CopyToClipboard from '../actions/CopyToClipboard'
import List, {ListProps} from '../containers/List'
import Value, {ValueProps} from '../containers/Value'
import {baseDecorator, editorDecorator} from '../utils/storyDecorators.helper'
import Numeral from './Numeral'

const meta = {
  title: 'components/values/primitives/Numeral',
  component: Numeral,
  decorators: [editorDecorator, baseDecorator],
  tags: ['autodocs'],
} satisfies Meta<typeof Numeral>

export default meta

const defaultArgsWithoutValue = {
  editable: true,
  placeholder: 'placeholder',
  fullWidth: true,
}

const defaultArgs = {
  value: 102103.104,
  ...defaultArgsWithoutValue,
}

type Story = StoryObj<typeof meta>
export const Editable: Story = {
  args: {
    ...defaultArgs,
  },
}

export const Standard: Story = {
  args: {
    ...defaultArgs,
    editable: false,
    options: {
      notation: 'standard',
    },
  },
}

export const Scientific: Story = {
  args: {
    ...defaultArgs,
    editable: false,
    options: {
      notation: 'scientific',
      maximumSignificantDigits: 12,
    },
  },
}

export const Compact: Story = {
  args: {
    ...defaultArgs,
    editable: false,
    options: {
      notation: 'compact',
    },
  },
}

export const Unit: Story = {
  args: {
    ...defaultArgs,
    value: 1.87,
    unit: 'm',
    editable: false,
  },
}

export const UnitEditable: Story = {
  args: {
    ...defaultArgs,
    value: 1.87,
    unit: 'm',
  },
}

export const Error: Story = {
  args: {
    ...defaultArgs,
    error: true,
  },
}

export const InitialWidth: Story = {
  args: {
    ...defaultArgs,
    fullWidth: false,
  },
}

export const InValue: Story = {
  parameters: {
    render: (props: ValueProps, story: React.ReactNode) => (
      <Value
        helperText='Helper text'
        label='label'
        {...props}
        {...defaultArgsWithoutValue}
      >
        {story}
      </Value>
    ),
    value: defaultArgs.value,
  },
  args: {
    actions: <CopyToClipboard />,
  },
}

export const InValueWithUnit: Story = {
  parameters: {
    render: (props: ValueProps, story: React.ReactNode) => (
      <Value
        helperText='Helper text'
        label='label'
        unit='m'
        {...props}
        {...defaultArgsWithoutValue}
      >
        {story}
      </Value>
    ),
    value: 1.87,
  },
  args: {
    actions: <CopyToClipboard />,
  },
}

export const InList: Story = {
  parameters: {
    render: function Render(props: ListProps, story: React.ReactNode) {
      const renderValue = useCallback(() => story, [story])
      return (
        <List
          helperText='Helper text'
          label='label'
          {...props}
          renderValue={renderValue}
          {...defaultArgsWithoutValue}
        />
      )
    },
    value: [1.2, 1.3, 1.4],
  },
  args: {
    actions: <CopyToClipboard />,
  },
}

export const InListWithUnit: Story = {
  parameters: {
    render: function Render(props: ListProps, story: React.ReactNode) {
      const renderValue = useCallback(() => story, [story])
      return (
        <List
          helperText='Helper text'
          label='label'
          unit='m'
          {...props}
          renderValue={renderValue}
          {...defaultArgsWithoutValue}
        />
      )
    },
    value: [1.2, 1.3, 1.4],
  },
  args: {
    actions: <CopyToClipboard />,
  },
}
