import {IconButton} from '@mui/material'
import {useCallback, useEffect} from 'react'

import useLocalState from '../../../hooks/useLocalState'
import {assert} from '../../../utils/utils'
import {Edit, Open} from '../../icons'
import {SelectDialogButton} from '../../navigation/Select'
import {SelectOptions} from '../../navigation/useSelect'
import Link from '../../routing/Link'
import useRoute from '../../routing/useRoute'
import PropsProvider from '../utils/PropsProvider'
import useProps from '../utils/useProps'
import PrimitiveProps from './PrimitiveProps'
import Text, {DynamicTextProps, TextProps} from './Text'

export type ReferenceProps = PrimitiveProps<string | undefined> & TextProps

export type DynamicReferenceProps = DynamicTextProps

export type GenericReferenceProps<S extends SelectOptions = SelectOptions> =
  ReferenceProps & {
    /**
     * The entity label that is used in select dialog title, description, buttos. etc.
     * The default is `entity`.
     */
    label?: string

    /**
     * The entity that can be edited with this reference.
     */
    selectOptions: S

    /**
     * The initial path that is used in the select dialog or to compute the relative paths.
     */
    initialPath: string
  }

/**
 * A generic reference primitive component that can be composed in more specific
 * reference primitives for files, sections, etc.
 */
export default function Reference<S extends SelectOptions = SelectOptions>({
  label,
  selectOptions,
  initialPath,
  ...props
}: GenericReferenceProps<S>) {
  label = label || selectOptions.entity
  const {value, onChange, onError, actions, ...textProps} = useProps(props)
  const {navigate, relativePath} = useRoute()
  const {error} = textProps
  const [textValue, setTextValue] = useLocalState(value)

  useEffect(() => {
    // TODO validate value
  }, [])

  const validate = useCallback(
    (textValue: string | undefined) => {
      if (!textValue) {
        onChange?.(undefined)
      } else {
        // TODO validate value
        onChange?.(textValue)
      }
    },
    [onChange],
  )

  const handleChange = useCallback(
    (textValue: string | undefined) => {
      setTextValue(textValue || '')
      validate(textValue)
    },
    [validate, setTextValue],
  )

  const handleOpen = useCallback(() => {
    assert(value !== undefined, 'Open should only be possible with value.')
    if (value.startsWith('/')) {
      navigate({path: value})
    } else {
      navigate({path: `${initialPath}/${value}`})
    }
  }, [value, navigate, initialPath])

  const handleSelect = useCallback(
    (selectedPath: string) => {
      selectedPath = relativePath(selectedPath, initialPath)
      setTextValue(selectedPath)
      onChange?.(selectedPath)
    },
    [setTextValue, onChange, relativePath, initialPath],
  )

  return (
    <Text
      value={textValue}
      onChange={handleChange}
      {...textProps}
      inputProps={{
        ...textProps.inputProps,
        endAdornment: (
          <>
            {actions}
            <IconButton
              size='small'
              onClick={handleOpen}
              disabled={error || !value}
              title='Open'
            >
              <Open />
            </IconButton>
            {/* This empty PropsProvider is necessary to not accidentaly
            prooagate props to values used in the select */}
            <PropsProvider>
              <SelectDialogButton
                selectOptions={selectOptions}
                initialPath={initialPath}
                component={IconButton}
                title={`Select ${label}`}
                description={`Select the ${label} that you want to reference.`}
                selectButtonLabel={`Select ${label}`}
                onSelect={handleSelect}
                size='small'
              >
                <Edit />
              </SelectDialogButton>
            </PropsProvider>
          </>
        ),
      }}
      displayStringProps={
        error && value !== undefined
          ? undefined
          : {
              component: Link,
              to: value,
            }
      }
      submitOnTypingDelay={-1}
    />
  )
}
