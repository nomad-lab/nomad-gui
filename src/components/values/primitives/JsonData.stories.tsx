import type {Meta, StoryObj} from '@storybook/react'

import Value, {ValueProps} from '../containers/Value'
import {baseDecorator, editorDecorator} from '../utils/storyDecorators.helper'
import JsonData from './JsonData'

const meta = {
  title: 'components/values/primitives/JsonData',
  component: JsonData,
  decorators: [editorDecorator, baseDecorator],
  tags: ['autodocs'],
} satisfies Meta<typeof JsonData>

export default meta

const defaultArgsWithoutValue = {
  editable: true,
  error: false,
  placeholder: 'placeholder',
  fullWidth: true,
}

const defaultArgs = {
  value: {
    'this is': {
      some: 'json data',
    },
  },
  ...defaultArgsWithoutValue,
}

type Story = StoryObj<typeof meta>
export const Editable: Story = {
  args: {
    ...defaultArgs,
  },
}

export const NotEditable: Story = {
  args: {
    ...defaultArgs,
    editable: false,
  },
}

export const InValue: Story = {
  parameters: {
    render: (props: ValueProps, story: React.ReactNode) => (
      <Value
        helperText='Helper text'
        label='label'
        {...props}
        {...defaultArgsWithoutValue}
      >
        {story}
      </Value>
    ),
    value: defaultArgs.value,
  },
}
