import {InputAdornment, TextField, TextFieldProps} from '@mui/material'
import {
  DatePicker,
  DateTimePicker,
  DateTimeValidationError,
  TimePicker,
} from '@mui/x-date-pickers'
import {format, isValid, parseISO} from 'date-fns'
import React, {Ref, useCallback} from 'react'

import useLocalState from '../../../hooks/useLocalState'
import ContainerProps from '../containers/ContainerProps'
import useProps from '../utils/useProps'
import Text, {DynamicTextProps, TextProps} from './Text'

const components = {
  date: DatePicker,
  time: TimePicker,
  datetime: DateTimePicker,
}

export type DatetimeVariant = keyof typeof components

const formats: {[key in DatetimeVariant]: string} = {
  date: 'MM/dd/yyyy',
  time: 'hh:mm a',
  datetime: 'MM/dd/yyyy hh:mm a',
}

export type DatetimeProps = {
  variant?: DatetimeVariant
} & Omit<TextProps, 'variant'>

export type DynamicDatetimeProps = DynamicTextProps &
  Pick<DatetimeProps, 'variant'>

const DatetimeTextField = React.forwardRef<HTMLDivElement>(
  function DatetimeTextField(
    {InputProps, ...props}: TextFieldProps,
    ref: Ref<HTMLDivElement>,
  ) {
    const {actions} = useProps<DatetimeProps>()

    return (
      <TextField
        inputRef={props?.inputProps?.ref || ref}
        {...props}
        slotProps={{
          input: {
            ...InputProps,
            endAdornment: (
              <>
                <InputAdornment position='end' sx={{marginRight: -1}}>
                  {actions}
                </InputAdornment>
                {InputProps?.endAdornment}
              </>
            ),
          },
        }}
      />
    )
  },
)

/**
 * A primitive to display and edit for dates, times, and date times.
 *
 * You can only use this primitive within a LocalizationProvider.
 */
export default function Datetime({
  variant = 'date',
  inputRef,
  ...props
}: DatetimeProps) {
  const {
    edit,
    editable,
    value,
    error,
    onChange,
    fullWidth,
    onError,
    placeholder,
    onFocus,
    onBlur,
    inputProps,
    table,
  } = useProps(props)
  const showEdit = edit === undefined ? editable : edit
  const {label, inputId} = useProps<ContainerProps>()
  const toPickerValue = useCallback(
    (value: string | undefined) =>
      value === undefined ? undefined : parseISO(value),
    [],
  )
  const [pickerValue, setPickerValue] = useLocalState(value, {
    transform: toPickerValue,
  })

  const handleError = (error: DateTimeValidationError | null) => {
    onError?.(error === null ? undefined : 'Invalid value')
  }

  const handleChange = (date: Date | null) => {
    setPickerValue(date || undefined)
    try {
      onChange?.(date?.toISOString())
    } catch {
      // pass
    }
  }

  const Component = components[variant]

  if (showEdit) {
    return (
      <Component
        value={pickerValue || null}
        onError={handleError}
        onChange={handleChange}
        slots={{
          textField: DatetimeTextField as typeof TextField,
        }}
        inputRef={inputRef}
        slotProps={{
          textField: {
            label: label,
            hiddenLabel: !label,
            fullWidth: fullWidth,
            variant: table ? 'standard' : 'filled',
            size: 'small',
            error: error,
            onBlur,
            onFocus,
            ...(placeholder && {placeholder: placeholder}),
            InputProps: {
              id: inputId,
              disableUnderline: table,
              ...inputProps,
            },
          },
          openPickerButton: {size: 'small'},
        }}
      />
    )
  } else {
    const parsed = value && parseISO(value)
    return (
      <Text
        value={
          isValid(parsed) ? format(parsed, formats[variant]) : 'Invalid Date'
        }
      />
    )
  }
}
