import {render, screen, waitFor} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {useDebounce} from 'react-use'
import {vi} from 'vitest'

import * as useEditDebounce from '../../../hooks/useEditDebounce'
import Numeral from './Numeral'

describe('Numeral', () => {
  it.each([
    ['standard', {notation: 'standard'}, '102,103.104'],
    [
      'scientific',
      {notation: 'scientific', maximumSignificantDigits: 6},
      '1.02103E5',
    ],
    ['compact', {notation: 'compact'}, '102K'],
    ['no options', undefined, '102,103.104'],
  ])('displays %s', (description, options, expectedValue) => {
    render(
      <Numeral
        value={102103.104}
        options={options as Intl.NumberFormatOptions}
      />,
    )
    expect(screen.getByText(expectedValue)).toBeInTheDocument()
  })
  it('allows to change the value', async () => {
    const handleChange = vi.fn()
    render(<Numeral editable value={12} onChange={handleChange} />)
    const input = screen.getByDisplayValue('12')
    await userEvent.type(input, '3{enter}')
    expect(handleChange).toHaveBeenLastCalledWith(123)
  })
  it.each([
    ['standard', 1e6, '1000000'],
    ['scientific', 1e13, '1E13'],
  ])('it formats editable value', (description, value, result) => {
    render(<Numeral editable value={value} />)
    expect(screen.getByDisplayValue(result)).toBeInTheDocument()
  })
  it('allows to edit invalid and undefined values', async () => {
    const handleChange = vi.fn()
    const handleError = vi.fn()
    render(
      <Numeral
        editable
        value={12}
        onChange={handleChange}
        onError={handleError}
      />,
    )
    const input = screen.getByDisplayValue('12') as HTMLInputElement
    await userEvent.type(input, 'E{enter}')
    expect(handleChange).not.toHaveBeenCalled()
    expect(handleError).toHaveBeenCalled()

    handleError.mockClear()
    await userEvent.type(input, '10{enter}')
    expect(handleChange).toHaveBeenLastCalledWith(12e10)
    expect(handleError).not.toHaveBeenCalled()

    handleChange.mockClear()
    await userEvent.type(
      input,
      '{backspace}{backspace}{backspace}{backspace}{backspace}{enter}',
    )
    expect(handleChange).toHaveBeenLastCalledWith(undefined)
    expect(handleError).not.toHaveBeenCalled()
  })
  it('allows undefined as value', () => {
    render(<Numeral editable value={undefined} />)
    expect(screen.getByDisplayValue('')).toBeInTheDocument()
  })
  it('allows to edit to undefined value', async () => {
    const handleChange = vi.fn()
    render(<Numeral editable value={1} onChange={handleChange} />)
    const input = screen.getByDisplayValue('1') as HTMLInputElement
    await userEvent.type(input, '{backspace}{enter}')
    expect(handleChange).toHaveBeenLastCalledWith(undefined)
  })
  it('calls onChange after debounce', async () => {
    vi.spyOn(useEditDebounce, 'default').mockImplementation((fn, deps) => {
      useDebounce(fn, 0, deps)
    })
    const handleChange = vi.fn()
    render(<Numeral editable value={1} onChange={handleChange} />)
    const input = screen.getByDisplayValue('1') as HTMLInputElement
    await userEvent.type(input, '2')
    await waitFor(() => expect(handleChange).toHaveBeenCalled())
  })
  it.each(['1', '1 ', 'NaN'])(
    'does not call onChange after debounce for invalid or unchange value "%s"',
    async (edit) => {
      vi.spyOn(useEditDebounce, 'default').mockImplementation((fn, deps) => {
        useDebounce(fn, 0, deps)
      })
      const handleChange = vi.fn()
      render(<Numeral editable value={1} onChange={handleChange} />)
      const input = screen.getByDisplayValue('1') as HTMLInputElement
      await userEvent.clear(input)
      await userEvent.type(input, edit)
      await expect(
        waitFor(() => expect(handleChange).toHaveBeenCalled(), {
          timeout: 1,
        }),
      ).rejects.toThrow()
    },
  )
})
