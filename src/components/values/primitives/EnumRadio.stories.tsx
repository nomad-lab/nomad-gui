import type {Meta, StoryObj} from '@storybook/react'

import CopyToClipboard from '../actions/CopyToClipboard'
import Value, {ValueProps} from '../containers/Value'
import {baseDecorator, editorDecorator} from '../utils/storyDecorators.helper'
import EnumRadio from './EnumRadio'

const meta = {
  title: 'components/values/primitives/EnumRadio',
  component: EnumRadio,
  decorators: [editorDecorator, baseDecorator],
  tags: ['autodocs'],
  parameters: {
    controls: {exclude: ['actions']},
  },
} satisfies Meta<typeof EnumRadio>

export default meta

const defaultArgsWithoutValue = {
  editable: true,
  error: false,
  placeholder: 'placeholder',
  fullWidth: true,
  values: ['one', 'two', 'three'],
}

const defaultArgs = {
  value: 'one',
  ...defaultArgsWithoutValue,
}

type Story = StoryObj<typeof meta>
export const Editable: Story = {
  args: {
    ...defaultArgs,
  },
}

export const NotEditable: Story = {
  args: {
    ...defaultArgs,
    editable: false,
  },
}

export const EditableAllowNone: Story = {
  args: {
    ...defaultArgs,
    allowNone: true,
    placeholder: 'no value',
  },
}

export const EditableRow: Story = {
  args: {
    ...defaultArgs,
    row: true,
    allowNone: true,
    placeholder: 'no value',
  },
}

export const Error: Story = {
  args: {
    ...defaultArgs,
    error: true,
  },
}

export const Placeholder: Story = {
  args: {
    ...defaultArgs,
    value: undefined,
  },
}

export const InitialWidth: Story = {
  args: {
    ...defaultArgs,
    fullWidth: false,
  },
}

export const InValue: Story = {
  parameters: {
    render: (props: ValueProps, story: React.ReactNode) => (
      <Value
        helperText='Helper text'
        label='label'
        {...props}
        {...defaultArgsWithoutValue}
      >
        {story}
      </Value>
    ),
    value: defaultArgs.value,
  },
  args: {
    values: defaultArgs.values,
    actions: <CopyToClipboard />,
  },
}
