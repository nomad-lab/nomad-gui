import dayjs from 'dayjs'
import 'dayjs/locale/en'
import relativeTime from 'dayjs/plugin/relativeTime'

import {assert} from '../../../utils/utils'
import useProps from '../utils/useProps'
import {
  DynamicPrimitiveProps,
  NonEditablePrimitiveProps,
} from './PrimitiveProps'
import Text from './Text'

dayjs.extend(relativeTime)

export type RelativeTimeProps = {
  /**
   * An optional reference time value that is used instead of now.
   */
  referenceTimeValue?: string
  /**
   * Omit the "ago" suffix.
   */
  withoutSuffix?: boolean
} & NonEditablePrimitiveProps<string | undefined>

export type DynamicRelativeTimeProps = Omit<
  DynamicPrimitiveProps,
  'edit' | 'editable'
> &
  Pick<RelativeTimeProps, 'referenceTimeValue' | 'withoutSuffix'>

/**
 * A primitive that displays the time spend from the given
 * timestamp to now, e.g. "21 days ago".
 *
 * This primitive is read only and does not support editing.
 */
export default function RelativeTime({
  referenceTimeValue,
  withoutSuffix = false,
  ...props
}: RelativeTimeProps) {
  const {editable, value} = useProps(props)

  assert(!editable, '')
  const stringValue = referenceTimeValue
    ? dayjs(value).from(referenceTimeValue, withoutSuffix)
    : dayjs(value).fromNow(withoutSuffix)

  return <Text value={stringValue} />
}
