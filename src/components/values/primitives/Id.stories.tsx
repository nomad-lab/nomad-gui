import type {Meta, StoryObj} from '@storybook/react'
import {useCallback} from 'react'

import CopyToClipboard from '../actions/CopyToClipboard'
import List, {ListProps} from '../containers/List'
import Value, {ValueProps} from '../containers/Value'
import {baseDecorator, editorDecorator} from '../utils/storyDecorators.helper'
import Id from './Id'

const meta = {
  title: 'components/values/primitives/Id',
  component: Id,
  decorators: [editorDecorator, baseDecorator],
  tags: ['autodocs'],
} satisfies Meta<typeof Id>

export default meta

const defaultArgsWithoutValue = {
  editable: false,
  error: false,
  placeholder: 'placeholder',
  fullWidth: true,
}

const defaultArgs = {
  value: 'mamubknsr2kb7XpHrBsLa_iU58k5r2kb7',
  ...defaultArgsWithoutValue,
}

type Story = StoryObj<typeof meta>

export const NotEditable: Story = {
  args: {
    ...defaultArgs,
  },
}

export const Abbreviated: Story = {
  args: {
    ...defaultArgs,
    abbreviated: true,
  },
}

export const Editable: Story = {
  args: {
    ...defaultArgs,
    editable: true,
  },
}

export const Error: Story = {
  args: {
    ...defaultArgs,
    error: true,
  },
}

export const Placeholder: Story = {
  args: {
    ...defaultArgs,
    value: '',
  },
}

export const InitialWidth: Story = {
  args: {
    ...defaultArgs,
    fullWidth: false,
  },
}

export const InValue: Story = {
  parameters: {
    render: (props: ValueProps, story: React.ReactNode) => (
      <Value
        helperText='Helper text'
        label='label'
        {...props}
        {...defaultArgsWithoutValue}
      >
        {story}
      </Value>
    ),
    value: defaultArgs.value,
  },
  args: {
    actions: <CopyToClipboard />,
  },
}

export const InList: Story = {
  parameters: {
    render: function Render(props: ListProps, story: React.ReactNode) {
      const renderValue = useCallback(() => story, [story])
      return (
        <List
          helperText='Helper text'
          label='label'
          {...props}
          renderValue={renderValue}
          {...defaultArgsWithoutValue}
        />
      )
    },
    value: [defaultArgs.value, defaultArgs.value, defaultArgs.value],
  },
  args: {
    actions: <CopyToClipboard />,
  },
}
