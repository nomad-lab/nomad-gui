import {Autocomplete, RadioGroupProps, TextField} from '@mui/material'
import {useCallback} from 'react'

import {assert} from '../../../utils/utils'
import {ValueFormControl} from '../containers/ContainerControl'
import {ControlProps} from '../containers/ContainerProps'
import useProps from '../utils/useProps'
import PrimitiveProps, {DynamicPrimitiveProps} from './PrimitiveProps'
import Text, {TextProps} from './Text'

export type EnumSelectProps = {
  /**
   * If true, the editable component will offer a `placeholder`, e.g.
   * "no value" option.
   */
  allowNone?: boolean

  /**
   * If true, users can enter values that are not in `values`.
   */
  allowCustom?: boolean

  /**
   * The possible values for the enum.
   */
  values: string[]
} & Pick<RadioGroupProps, 'row'> &
  PrimitiveProps<string | undefined> &
  Pick<TextProps, 'inputProps' | 'inputRef'>

export type DynamicEnumSelectProps = DynamicPrimitiveProps &
  Pick<EnumSelectProps, 'allowNone' | 'allowCustom' | 'values'>

export default function EnumSelect(props: EnumSelectProps) {
  const {
    allowCustom,
    allowNone,
    values,
    value,
    onChange,
    edit,
    editable,
    onError,
    table,
    inputRef,
    inputProps,
    inputId,
    ...textProps
  } = useProps<EnumSelectProps & ControlProps>(props)
  const {placeholder} = textProps
  const {label, ...controlProps} = textProps
  const showEdit = edit === undefined ? editable : edit

  const handleChange = useCallback(
    (event: React.SyntheticEvent, value: string | null) => {
      if (value === null) {
        onChange?.(undefined)
        return
      }
      assert(allowCustom || values.includes(value), `Invalid value: ${value}`)
      onChange?.(value)
    },
    [onChange, values, allowCustom],
  )

  if (showEdit) {
    return (
      <ValueFormControl variant='standard' {...controlProps}>
        <Autocomplete
          disableClearable={!allowNone}
          freeSolo={allowCustom}
          fullWidth
          options={values}
          autoHighlight
          value={value || null}
          onChange={handleChange}
          renderInput={(params) => (
            <TextField
              {...params}
              InputLabelProps={{shrink: true, ...params.InputLabelProps}}
              placeholder={placeholder}
              variant={table ? 'standard' : 'filled'}
              size='small'
              margin='none'
              label={label}
              hiddenLabel={!label}
              InputProps={{
                ...params.InputProps,
                disableUnderline: table,
                id: inputId,
              }}
              // InputProps={{
              //   ...inputProps,
              //   ...params.InputProps,
              // }}
            />
          )}
        />
      </ValueFormControl>
    )
  } else {
    return <Text value={value} {...textProps} />
  }
}
