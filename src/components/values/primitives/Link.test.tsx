import {render, screen, waitFor} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {useDebounce} from 'react-use'
import {describe, expect, it, vi} from 'vitest'

import * as useEditDebounce from '../../../hooks/useEditDebounce'
import Link from './Link'

describe('Link', () => {
  const testValue = 'https://nomad-lab.eu'
  it('displays value', async () => {
    render(<Link value={testValue} />)
    const text = screen.getByText(testValue)
    expect(text.closest('a')).toHaveAttribute('href', testValue)
  })
  it('displays value editable and edit false', () => {
    render(<Link value={testValue} editable edit={false} />)
    const text = screen.getByText(testValue)
    expect(text.closest('a')).toHaveAttribute('href', testValue)
  })
  it('allows to edit invalid and undefined values', async () => {
    const handleChange = vi.fn()
    const handleError = vi.fn()
    render(
      <Link
        editable
        value={testValue}
        onChange={handleChange}
        onError={handleError}
      />,
    )
    const input = screen.getByDisplayValue(testValue) as HTMLInputElement
    await userEvent.type(input, '%{enter}')
    expect(handleError).toHaveBeenCalled()
    expect(handleChange).not.toHaveBeenCalled()

    handleChange.mockClear()
    handleError.mockClear()
    await userEvent.type(input, '{backspace}{enter}')
    expect(handleChange).toHaveBeenCalledWith(testValue)
    expect(handleError).toHaveBeenCalledWith(undefined)

    handleChange.mockClear()
    handleError.mockClear()
    await userEvent.type(
      input,
      '{backspace}'.repeat(testValue.length) + '{enter}',
    )
    expect(handleChange).toHaveBeenCalledWith(undefined)
    expect(handleError).not.toHaveBeenCalled()

    handleChange.mockClear()
    handleError.mockClear()
    await userEvent.type(input, 'wrongProtocol://nomad-lab.eu{enter}')
    expect(handleChange).not.toHaveBeenCalled()
    expect(handleError).toHaveBeenCalled()
  })
  it('calls onChange after debounce', async () => {
    vi.spyOn(useEditDebounce, 'default').mockImplementation((fn, deps) => {
      useDebounce(fn, 0, deps)
    })
    const handleChange = vi.fn()
    render(<Link editable value={testValue} onChange={handleChange} />)
    const input = screen.getByDisplayValue(testValue) as HTMLInputElement
    await userEvent.type(input, '{backspace}{backspace}de')
    await waitFor(() => expect(handleChange).toHaveBeenCalled())
  })
  it('does not call onChange after debounce for invalid values', async () => {
    vi.spyOn(useEditDebounce, 'default').mockImplementation((fn, deps) => {
      useDebounce(fn, 0, deps)
    })
    const handleChange = vi.fn()
    render(<Link editable value={testValue} onChange={handleChange} />)
    const input = screen.getByDisplayValue(testValue) as HTMLInputElement
    await userEvent.clear(input)
    await userEvent.type(input, '§')
    await expect(
      waitFor(() => expect(handleChange).toHaveBeenCalled(), {
        timeout: 1,
      }),
    ).rejects.toThrow()
  })
  it('allows to open the URL', async () => {
    const open = vi.spyOn(window, 'open')
    open.mockImplementation(() => null)
    render(<Link value={testValue} editable />)
    await userEvent.click(screen.getByRole('button'))
    expect(open).toHaveBeenCalledWith(testValue, '_blank')
  })
})
