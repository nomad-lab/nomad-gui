import {useCallback} from 'react'

import {assert} from '../../../utils/utils'
import {ControlProps} from '../containers/ContainerProps'
import useProps from '../utils/useProps'
import EnumRadio, {EnumRadioProps} from './EnumRadio'
import PrimitiveProps, {DynamicPrimitiveProps} from './PrimitiveProps'

export type BooleanProps = PrimitiveProps<boolean | undefined> &
  Pick<EnumRadioProps, 'allowNone'>

export type DynamicBooleanProps = DynamicPrimitiveProps &
  Pick<BooleanProps, 'allowNone'>

export default function Boolean(props: BooleanProps) {
  const {value, onChange, ...enumProps} = useProps<BooleanProps & ControlProps>(
    props,
  )

  assert(value === undefined || typeof value === 'boolean', 'Invalid value')
  const enumValue = value === undefined ? undefined : value ? 'true' : 'false'

  const handleChange = useCallback(
    (value: string | undefined) => {
      if (value === undefined) {
        onChange?.(undefined)
      } else {
        onChange?.(value === 'true')
      }
    },
    [onChange],
  )

  return (
    <EnumRadio
      row
      values={['true', 'false']}
      value={enumValue}
      onChange={handleChange}
      {...enumProps}
    />
  )
}
