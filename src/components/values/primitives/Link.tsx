import {IconButton, Link as MuiLink} from '@mui/material'
import React, {useCallback} from 'react'

import useEditDebounce from '../../../hooks/useEditDebounce'
import useLocalState from '../../../hooks/useLocalState'
import {OpenInNew} from '../../icons'
import useProps from '../utils/useProps'
import PrimitiveProps from './PrimitiveProps'
import Text, {DynamicTextProps, TextProps} from './Text'

export type LinkProps = {
  linkComponent?: React.ComponentType<{href: string}>
} & PrimitiveProps<string | undefined> &
  Omit<TextProps, 'value' | 'onChange' | 'onSubmit' | 'onInputChange'>

export type DynamicLinkProps = DynamicTextProps

function isValidUrl(stringValue: string) {
  let urlValue
  try {
    urlValue = new URL(stringValue)
  } catch {
    return false
  }
  if (urlValue.protocol !== 'http:' && urlValue.protocol !== 'https:') {
    return false
  }
  return true
}

/**
 * A primitive for displaying and editing URLs that are displayed as links.
 */
export default function Link({linkComponent = MuiLink, ...props}: LinkProps) {
  const {value, onChange, onError, actions, ...textProps} = useProps(props)
  const {error} = textProps
  const [textValue, setTextValue] = useLocalState(value)

  const validate = useCallback(
    (textValue: string | undefined) => {
      if (!textValue) {
        onChange?.(undefined)
      } else {
        if (textValue && !isValidUrl(textValue)) {
          onError?.('Invalid URL')
        } else {
          onError?.(undefined)
          onChange?.(textValue)
        }
      }
    },
    [onChange, onError],
  )

  const handleTextInputChange = useCallback(
    (textValue: string) => {
      setTextValue(textValue)
    },
    [setTextValue],
  )

  const handleTextSubmit = useCallback(
    (textValue: string) => {
      validate(textValue)
    },
    [validate],
  )

  useEditDebounce(() => {
    if (textValue !== value && textValue && isValidUrl(textValue)) {
      validate(textValue)
    }
  }, [validate, textValue, value])

  const handleOpen = useCallback(() => {
    window.open(value, '_blank')?.focus()
  }, [value])

  return (
    <Text
      value={textValue}
      onSubmit={handleTextSubmit}
      onInputChange={handleTextInputChange}
      {...textProps}
      inputProps={{
        ...textProps.inputProps,
        endAdornment: (
          <>
            {actions}
            <IconButton
              size='small'
              onClick={handleOpen}
              disabled={error || !textValue || textValue.trim() === ''}
            >
              <OpenInNew />
            </IconButton>
          </>
        ),
      }}
      displayStringProps={
        isValidUrl(value)
          ? {
              component: linkComponent,
              href: value,
            }
          : undefined
      }
      submitOnTypingDelay={-1}
    />
  )
}
