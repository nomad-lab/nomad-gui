import {Stack} from '@mui/material'
import {useCallback, useMemo} from 'react'
import {useLatest} from 'react-use'

import useEditDebounce from '../../../hooks/useEditDebounce'
import useLocalState from '../../../hooks/useLocalState'
import {assert} from '../../../utils/utils'
import * as units from '../../units/units'
import {parseUnitString} from '../../units/units'
import Unit from '../utils/Unit'
import useProps from '../utils/useProps'
import PrimitiveProps, {DynamicPrimitiveProps} from './PrimitiveProps'
import Text, {TextProps} from './Text'

export type NumeralProps = {
  /**
   * The options to format the number for display. If edit/editable is true,
   * the number with be formatted with a predefined format
   * that uses scientific format for numbers smaller than 1e-12 and larger
   * than 1e12. The editable number will be formatted with a maximum of
   * 16 significant digits.
   */
  options?: Intl.NumberFormatOptions
} & PrimitiveProps<number | undefined> &
  Omit<TextProps, 'value' | 'onChange' | 'onSubmit' | 'onInputChange'>

export type DynamicNumeralProps = DynamicPrimitiveProps &
  Pick<
    PrimitiveProps,
    'unit' | 'displayUnit' | 'displayUnitLocked' | 'hiddenUnit'
  > &
  Pick<NumeralProps, 'options'>

const scientificEditableFormat = Intl.NumberFormat(undefined, {
  notation: 'scientific',
  useGrouping: false,
  maximumSignificantDigits: 16,
}).format
const standardEditableFormat = Intl.NumberFormat(undefined, {
  useGrouping: false,
  maximumSignificantDigits: 16,
}).format

/**
 * A primitive to display and edit numbers (with units).
 */
export default function Numeral({options, ...props}: NumeralProps) {
  const {
    value,
    onChange,
    fullWidth,
    unit,
    hiddenUnit,
    displayUnit: controlledDisplayUnit,
    onDisplayUnitChange,
    displayUnitLocked: controlledDisplayUnitLocked,
    onDisplayUnitLockedChange,
    ...textProps
  } = useProps(props)
  const {onError, editable, edit, error} = textProps
  const showEdit = edit === undefined ? editable : edit
  const errorLatest = useLatest(error)
  const [displayUnit, setDisplayUnit] = useLocalState(
    controlledDisplayUnit || unit,
    {
      onChange: onDisplayUnitChange,
    },
  )
  const [displayUnitLocked, setDisplayUnitLocked] = useLocalState(
    controlledDisplayUnitLocked === undefined
      ? true
      : controlledDisplayUnitLocked,
    {
      onChange: onDisplayUnitLockedChange,
    },
  )

  const handleDisplayUnitChange = useCallback(
    (changedDisplayUnit: string) => {
      assert(unit !== undefined, 'unit must be a unit')
      assert(displayUnit !== undefined, 'displayUnit must be defined')

      if (!displayUnitLocked) {
        onChange?.(
          units
            .unit(
              units
                .unit(value as number, unit)
                .to(displayUnit)
                .toNumber(),
              changedDisplayUnit,
            )
            .to(unit)
            .toNumber(),
        )
      }
      setDisplayUnit?.(changedDisplayUnit)
    },
    [displayUnitLocked, value, onChange, unit, displayUnit, setDisplayUnit],
  )

  const displayFormat = useMemo(
    () =>
      Intl.NumberFormat(undefined, options || {maximumSignificantDigits: 12})
        .format,
    [options],
  )
  const format = useCallback(
    (value: number | undefined, displayUnit?: string) => {
      if (value === undefined) {
        return ''
      }
      if (displayUnit) {
        assert(unit !== undefined, 'unit must be defined to use displayUnit')
        value = units.unit(value, unit).to(displayUnit).toNumber()
      }
      if (showEdit) {
        const absValue = Math.abs(value)
        if (absValue !== 0 && (absValue < 1e-12 || absValue > 1e12)) {
          return scientificEditableFormat(value)
        } else {
          return standardEditableFormat(value)
        }
      }
      return displayFormat(value)
    },
    [displayFormat, showEdit, unit],
  )
  const transformToTextValue = useCallback(
    (value: number | undefined) => {
      return format(value, displayUnit)
    },
    [format, displayUnit],
  )

  const handleError = useCallback(
    (changedError: string | undefined) => {
      if (errorLatest.current || changedError !== undefined) {
        onError?.(changedError)
      }
    },
    [onError, errorLatest],
  )

  const [textValue, setTextValue] = useLocalState(value, {
    transform: transformToTextValue,
  })
  const validate = useCallback(
    (textValue: string | undefined) => {
      if (!textValue) {
        onChange?.(undefined)
        handleError(undefined)
        return
      }
      if (unit) {
        const {
          value: unitValue,
          error,
          containsUnit,
        } = parseUnitString(textValue, displayUnit)
        if (error || unitValue === undefined) {
          handleError(error)
          return
        }
        let changedUnit
        if (containsUnit) {
          changedUnit = unitValue.formatUnits()
          if (!displayUnitLocked) {
            handleDisplayUnitChange(changedUnit)
          }
        }
        handleError(undefined)
        const changedValue = unitValue.to(unit).toNumber()
        onChange?.(changedValue)
        setTextValue(format(changedValue, changedUnit || displayUnit))
      } else {
        const numberValue = Number(textValue)
        if (Number.isNaN(numberValue)) {
          handleError('Invalid number')
        } else {
          handleError(undefined)
          onChange?.(numberValue)
        }
      }
    },
    [
      handleError,
      onChange,
      unit,
      displayUnit,
      displayUnitLocked,
      handleDisplayUnitChange,
      format,
      setTextValue,
    ],
  )

  const handleTextInputChange = useCallback(
    (textValue: string) => {
      setTextValue(textValue)
    },
    [setTextValue],
  )

  const handleTextSubmit = useCallback(
    (textValue: string) => {
      validate(textValue)
    },
    [validate],
  )

  useEditDebounce(() => {
    // We only change the value directly on typing, if the user just changed
    // a number into another valid number. This typing should not cause any
    // errors or helperText and still cause an immediate change on
    // most "save worthy" edits. We still run the edit through the validate
    // function just in case.
    if (textValue !== transformToTextValue(value)) {
      if (textValue.trim() !== textValue) {
        return
      }
      const numberValue = Number(textValue)
      if (Number.isNaN(numberValue)) {
        return
      }
      if (textValue !== transformToTextValue(numberValue)) {
        return
      }

      validate(textValue)
    }
  }, [validate, textValue, transformToTextValue, value])

  const showUnit = (unit || displayUnit) && !hiddenUnit

  return (
    <Stack
      direction='row'
      width={fullWidth ? '100%' : undefined}
      alignItems='center'
      spacing={showEdit ? 0 : 1}
    >
      <Text
        fullWidth={showEdit}
        value={textValue}
        onInputChange={handleTextInputChange}
        onSubmit={handleTextSubmit}
        {...textProps}
        submitOnTypingDelay={-1}
      />
      {showUnit && (
        <Unit
          unit={unit}
          displayUnit={displayUnit}
          displayUnitLocked={showEdit ? displayUnitLocked : undefined}
          onDisplayUnitChange={handleDisplayUnitChange}
          onDisplayUnitLockedChange={setDisplayUnitLocked}
          onError={handleError}
          variant={showEdit ? 'edit' : 'standard'}
        />
      )}
    </Stack>
  )
}
