import {act, fireEvent, render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {vi} from 'vitest'

import {createMatchMedia} from '../../../utils/test.helper'
import JsonData from './JsonData'

describe('JsonData', () => {
  beforeAll(() => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
  })
  it('displays value', () => {
    render(<JsonData value={{key: 'value'}} />)
    expect(screen.getByText(/key/)).toBeInTheDocument()
    expect(screen.getByText(/value/)).toBeInTheDocument()
  })
  it('displays value editable and edit false', () => {
    render(<JsonData value={{key: 'value'}} editable edit={false} />)
    expect(screen.getByText(/key/)).toBeInTheDocument()
    expect(screen.getByText(/value/)).toBeInTheDocument()
  })
  it('displays value editable', () => {
    render(<JsonData value={{key: 'value'}} editable />)
    const input = screen.getByDisplayValue(/"key": "value"/)
    expect(input).toBeInTheDocument()
    expect(input.closest('textarea')).toBeInTheDocument()
  })
  it('allows to change the value', async () => {
    const handleChange = vi.fn()
    const handleError = vi.fn()
    render(
      <JsonData
        value={{key: 'value'}}
        editable
        onChange={handleChange}
        onError={handleError}
      />,
    )
    const input = screen.getByDisplayValue(/"key": "value"/)
    await userEvent.clear(input)
    await userEvent.type(input, '"value"{Shift>}{Enter}{/Shift}')
    expect(handleChange).toHaveBeenLastCalledWith('value')
  })
  it('allows to edit invalid and undefined values', async () => {
    const handleChange = vi.fn()
    const handleError = vi.fn()
    render(
      <JsonData
        value={{key: 'value'}}
        editable
        onChange={handleChange}
        onError={handleError}
      />,
    )
    const input = screen.getByDisplayValue(/"key": "value"/)
    await userEvent.type(input, 'E{Shift>}{Enter}{/Shift}')
    expect(handleChange).not.toHaveBeenCalled()
    expect(handleError).toHaveBeenCalled()

    handleError.mockClear()
    await userEvent.clear(input)
    await userEvent.type(input, '{Shift>}{Enter}{/Shift}')
    expect(handleChange).toHaveBeenLastCalledWith(undefined)
    expect(handleError).toHaveBeenCalledWith(undefined)

    handleChange.mockClear()
    await userEvent.type(input, '"value"{Shift>}{Enter}{/Shift}')
    expect(handleChange).toHaveBeenLastCalledWith('value')
    expect(handleError).toHaveBeenCalledWith(undefined)
  })
  it('allows undefined as value', () => {
    render(<JsonData editable value={undefined} />)
    expect(screen.getByDisplayValue('')).toBeInTheDocument()
  })
  it('allows to edit to undefined value', async () => {
    const handleChange = vi.fn()
    render(<JsonData editable value={1} onChange={handleChange} />)
    const input = screen.getByDisplayValue('1') as HTMLInputElement
    await userEvent.type(input, '{backspace}')
    act(() => fireEvent.blur(input))
    expect(handleChange).toHaveBeenLastCalledWith(undefined)
  })
})
