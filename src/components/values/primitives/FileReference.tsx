import entryRoute from '../../../pages/entry/entryRoute'
import {FileSelectOptions} from '../../navigation/useSelect'
import useRouteData from '../../routing/useRouteData'
import Reference, {DynamicReferenceProps, ReferenceProps} from './Reference'

export type FileReferenceProps = ReferenceProps &
  Omit<FileSelectOptions, 'entity'>

export type DynamicFileReferenceProps = DynamicReferenceProps &
  Omit<FileSelectOptions, 'entity'>

/**
 * A primitive for displaying and editing references to files.
 */
export default function FileReference({
  filePattern,
  ...referenceProps
}: FileReferenceProps) {
  const {upload_id, mainfile_path} = useRouteData(entryRoute)

  const selectOptions = {
    entity: 'file',
    filePattern,
  } satisfies FileSelectOptions

  const initialPath = `/uploads/${upload_id}/files${mainfile_path?.replace(
    /\/[^/]+$/,
    '',
  )}`

  return (
    <Reference
      {...referenceProps}
      selectOptions={selectOptions}
      initialPath={initialPath}
    />
  )
}
