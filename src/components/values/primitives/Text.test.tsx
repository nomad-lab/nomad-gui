import {InputBase} from '@mui/material'
import {fireEvent, render, screen, waitFor} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {act} from 'react'
import {vi} from 'vitest'

import Text, {TextProps} from './Text'

describe('Text', () => {
  it('renders', () => {
    render(<Text value='value' />)
    expect(screen.getByText('value')).toBeInTheDocument()
  })
  it.each([['standard'], ['h1'], ['h2']])('renders editable %s', (variant) => {
    render(
      <Text value='value' editable variant={variant as TextProps['variant']} />,
    )
    expect(screen.getByDisplayValue('value')).toBeInTheDocument()
  })
  it('renders with error', async () => {
    render(<Text value='value' error />)
    expect(screen.getByText('value')).toHaveStyle('color: rgb(211, 47, 47)')
  })
  it('allows to change the value pressing enter', async () => {
    const handleChange = vi.fn()
    render(<Text editable value='value' onChange={handleChange} />)
    const input = screen.getByDisplayValue('value')
    await act(() => userEvent.type(input, '+{enter}'))
    expect(handleChange).toHaveBeenCalledWith('value+')
  })
  it('test type', async () => {
    const handleChange = vi.fn()
    render(<InputBase value='value' onChange={handleChange} />)
    const input = screen.getByDisplayValue('value')
    await userEvent.type(input, '+')
    expect(handleChange).toHaveBeenCalled()
  })
  it('allows to change the value via blur', async () => {
    const handleChange = vi.fn()
    render(<Text editable value='value' onChange={handleChange} />)
    const input = screen.getByDisplayValue('value')
    await userEvent.type(input, '+')
    act(() => fireEvent.blur(input))
    expect(handleChange).toHaveBeenCalledWith('value+')
  })
  it('does not fire onChange if value did not change', async () => {
    const handleChange = vi.fn()
    render(<Text editable value='value' onChange={handleChange} />)
    const input = screen.getByDisplayValue('value')
    act(() => fireEvent.blur(input))
    await userEvent.type(input, '{enter}')
    expect(handleChange).not.toHaveBeenCalled()
  })
  it('does fire onChange after debounce', async () => {
    const handleChange = vi.fn()
    render(
      <Text
        editable
        value='value'
        onChange={handleChange}
        submitOnTypingDelay={5}
      />,
    )
    const input = screen.getByDisplayValue('value')
    await userEvent.type(input, '+')
    await waitFor(() => {
      expect(handleChange).toHaveBeenCalledWith('value+')
    })
  })
  it('does not fire onChange after debounce with no delay', async () => {
    const handleChange = vi.fn()
    render(
      <Text
        editable
        value='value'
        onChange={handleChange}
        submitOnTypingDelay={-1}
      />,
    )
    const input = screen.getByDisplayValue('value')
    await userEvent.type(input, '+')
    await waitFor(
      () => {
        expect(handleChange).not.toHaveBeenCalledWith('value+')
      },
      {timeout: 100},
    )
  })
})
