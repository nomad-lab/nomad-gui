import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {describe, expect, it, vi} from 'vitest'

import EnumSelect from './EnumSelect'

describe('Enum', () => {
  const values = ['one', 'two', 'three']
  it.each([
    ['one', 'one'],
    [undefined, 'placeholder'],
  ])('displays value %s', (testValue, expected) => {
    render(
      <EnumSelect
        values={values}
        value={testValue}
        placeholder='placeholder'
      />,
    )
    expect(screen.getByText(expected)).toBeInTheDocument()
  })
  it('displays value editable and edit false', () => {
    render(<EnumSelect values={values} value='one' editable edit={false} />)
    expect(screen.getByText('one')).toBeInTheDocument()
  })
  it.each([['allowNone'], ['allowCustom']])(
    'allows to edit values with %s',
    async (allowProp) => {
      const allowProps = {[allowProp]: true}
      const handleChange = vi.fn()
      const handleError = vi.fn()
      render(
        <EnumSelect
          values={values}
          placeholder='placeholder'
          {...allowProps}
          editable
          value={'one'}
          onChange={handleChange}
          onError={handleError}
        />,
      )

      const inputElement = screen.getByDisplayValue('one')
      expect(inputElement).toBeInTheDocument()

      await userEvent.clear(inputElement)
      if (allowProp === 'allowNone') {
        expect(handleChange).toHaveBeenCalledWith(undefined)
      } else {
        expect(handleChange).not.toHaveBeenCalled()
      }

      await userEvent.type(inputElement, 'two{enter}')
      expect(handleChange).toHaveBeenCalledWith('two')

      await userEvent.clear(inputElement)
      handleChange.mockClear()
      await userEvent.type(inputElement, 'custom{enter}')
      if (allowProp === 'allowCustom') {
        expect(handleChange).toHaveBeenCalledWith('custom')
      } else {
        expect(handleChange).not.toHaveBeenCalled()
      }

      expect(handleError).not.toHaveBeenCalled()
    },
  )
})
