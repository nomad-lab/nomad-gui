import {useContext} from 'react'

import {ToolbarContext, ToolbarContextType} from './toolbarContext'

export default function useToolbar(): ToolbarContextType | undefined {
  return useContext(ToolbarContext)
}
