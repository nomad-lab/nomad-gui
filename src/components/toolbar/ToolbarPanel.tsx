import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import {Box, Button, Collapse, Popover, Stack, Tooltip} from '@mui/material'
import React, {PropsWithChildren} from 'react'

import {SxProps} from '../../utils/types'
import useToolbar from './useToolbar'

export type ToolbarPanelProps = PropsWithChildren<{
  icon?: React.ReactNode
  render?: React.ReactNode
  tooltip?: string
  index: number
}> &
  SxProps

export default function ToolbarPanel({
  icon,
  render,
  tooltip,
  index,
  sx,
  children,
}: ToolbarPanelProps) {
  const toolbarContext = useToolbar()
  const [anchorEl, setAnchorEl] = React.useState<
    (EventTarget & HTMLButtonElement) | null
  >(null)

  if (!toolbarContext) {
    throw new Error('ToolbarPanel must be used within a toolbar')
  }

  const {collapsed} = toolbarContext

  return (
    <Stack direction='row' spacing={0} sx={{display: 'inline-flex', ...sx}}>
      {!!collapsed?.[index] && render}
      {icon && !render && !!collapsed?.[index] && (
        <Box
          display={'inline-flex'}
          data-testid={
            tooltip
              ? `collapsed-toolbar-panel-${tooltip}`
              : 'collapsed-toolbar-panel'
          }
        >
          <Tooltip title={tooltip}>
            <Button
              onClick={(event) => setAnchorEl(event.currentTarget)}
              size={'small'}
              color={'inherit'}
              sx={{
                paddingTop: 0,
                paddingBottom: 0,
                height: 'inherit',
                '& .css-cstir9-MuiButton-startIcon': {margin: 0},
                '& .css-jcxoq4-MuiButton-endIcon': {margin: 0},
              }}
              startIcon={icon}
              endIcon={<KeyboardArrowDownIcon />}
            />
          </Tooltip>
          <Popover
            open={Boolean(anchorEl)}
            anchorEl={anchorEl}
            onClose={() => setAnchorEl(null)}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
          >
            {children}
          </Popover>
        </Box>
      )}
      <Collapse
        orientation='horizontal'
        in={!collapsed?.[index]}
        sx={{
          '& .MuiCollapse-wrapperInner': {
            display: 'inline-flex',
            alignItems: 'center',
          },
        }}
      >
        {children}
      </Collapse>
    </Stack>
  )
}
