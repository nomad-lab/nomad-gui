import FormatBoldIcon from '@mui/icons-material/FormatBold'
import FormatColorFillIcon from '@mui/icons-material/FormatColorFill'
import FormatColorTextIcon from '@mui/icons-material/FormatColorText'
import {Box, IconButton, Typography} from '@mui/material'
import {render, screen, waitFor} from '@testing-library/react'
import {act} from '@testing-library/react'
import React, {useEffect, useState} from 'react'
import {Mock, vi} from 'vitest'

import Toolbar from './Toolbar'
import ToolbarPanel from './ToolbarPanel'

const ResizableComponent = () => {
  const [width, setWidth] = useState(window.innerWidth)

  useEffect(() => {
    const handleResize = () => setWidth(window.innerWidth)
    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  }, [])

  return (
    <Box width={width} maxWidth={width}>
      <Toolbar sx={{width: width}}>
        <Typography>{`Width: ${width}`}</Typography>
        <ToolbarPanel
          index={0}
          icon={<FormatColorTextIcon />}
          tooltip={'formats'}
        >
          <IconButton size='small'>
            <FormatColorTextIcon />
          </IconButton>
          <IconButton size='small'>
            <FormatColorTextIcon />
          </IconButton>
          <IconButton size='small'>
            <FormatColorTextIcon />
          </IconButton>
          <IconButton size='small'>
            <FormatColorTextIcon />
          </IconButton>
          <IconButton size='small'>
            <FormatColorTextIcon />
          </IconButton>
        </ToolbarPanel>
        <ToolbarPanel
          index={2}
          icon={<FormatColorFillIcon />}
          tooltip={'colors'}
        >
          <IconButton size='small'>
            <FormatColorFillIcon />
          </IconButton>
          <IconButton size='small'>
            <FormatColorFillIcon />
          </IconButton>
          <IconButton size='small'>
            <FormatColorFillIcon />
          </IconButton>
          <IconButton size='small'>
            <FormatColorFillIcon />
          </IconButton>
          <IconButton size='small'>
            <FormatColorFillIcon />
          </IconButton>
        </ToolbarPanel>
        <ToolbarPanel index={1} icon={<FormatBoldIcon />} tooltip={'insert'}>
          <IconButton size='small'>
            <FormatBoldIcon />
          </IconButton>
          <IconButton size='small'>
            <FormatBoldIcon />
          </IconButton>
          <IconButton size='small'>
            <FormatBoldIcon />
          </IconButton>
          <IconButton size='small'>
            <FormatBoldIcon />
          </IconButton>
        </ToolbarPanel>
      </Toolbar>
    </Box>
  )
}

let onResize: Mock

describe('Toolbar', () => {
  let originalWidth: number

  beforeEach(() => {
    originalWidth = window.innerWidth
    window.ResizeObserver = vi.fn().mockImplementation(() => ({
      observe: vi.fn(),
      unobserve: vi.fn(),
      disconnect: vi.fn(),
    }))
    vi.mock('react-resize-detector', () => {
      return {
        useResizeDetector: (props: {onResize: Mock}) => {
          onResize = props.onResize
          return {
            onResize: props.onResize,
          }
        },
      }
    })
  })

  afterEach(() => {
    Object.defineProperty(window, 'innerWidth', {
      writable: true,
      configurable: true,
      value: originalWidth,
    })
  })

  const resizeWindow = (width: number) => {
    const lastChild = screen.getByTestId('toolbar-item-3')
    const parent = screen.getByTestId('toolbar')
    Object.defineProperty(parent, 'clientWidth', {
      value: width,
      configurable: true,
      writable: true,
    })
    Object.defineProperty(lastChild, 'offsetLeft', {
      value: 0,
      configurable: true,
      writable: true,
    })
    Object.defineProperty(lastChild, 'clientWidth', {
      value: 500,
      configurable: true,
      writable: true,
    })

    Object.defineProperty(window, 'innerWidth', {
      writable: true,
      configurable: true,
      value: width,
    })
    act(() => {
      window.dispatchEvent(new Event('resize'))
    })
    act(() => {
      onResize({width: width})
    })
  }

  it('updates width on window resize', async () => {
    render(<ResizableComponent />)
    expect(screen.getByText(`Width: ${originalWidth}`)).toBeInTheDocument()

    await waitFor(() =>
      expect(
        screen.queryByTestId('collapsed-toolbar-panel-formats'),
      ).not.toBeInTheDocument(),
    )
    await waitFor(() =>
      expect(
        screen.queryByTestId('collapsed-toolbar-panel-colors'),
      ).not.toBeInTheDocument(),
    )
    await waitFor(() =>
      expect(
        screen.queryByTestId('collapsed-toolbar-panel-insert'),
      ).not.toBeInTheDocument(),
    )

    resizeWindow(400)
    expect(screen.getByText('Width: 400')).toBeInTheDocument()

    await waitFor(() =>
      expect(
        screen.getByTestId('collapsed-toolbar-panel-colors'),
      ).toBeInTheDocument(),
    )
    await waitFor(() =>
      expect(
        screen.getByTestId('collapsed-toolbar-panel-formats'),
      ).toBeInTheDocument(),
    )
    await waitFor(() =>
      expect(
        screen.getByTestId('collapsed-toolbar-panel-insert'),
      ).toBeInTheDocument(),
    )

    resizeWindow(600)

    await waitFor(() =>
      expect(
        screen.queryByTestId('collapsed-toolbar-panel-formats'),
      ).not.toBeInTheDocument(),
    )
    await waitFor(() =>
      expect(
        screen.queryByTestId('collapsed-toolbar-panel-colors'),
      ).not.toBeInTheDocument(),
    )
    await waitFor(() =>
      expect(
        screen.queryByTestId('collapsed-toolbar-panel-insert'),
      ).not.toBeInTheDocument(),
    )
  })
})
