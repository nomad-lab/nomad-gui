import FormatBoldIcon from '@mui/icons-material/FormatBold'
import FormatColorFillIcon from '@mui/icons-material/FormatColorFill'
import FormatColorTextIcon from '@mui/icons-material/FormatColorText'
import {Divider, IconButton} from '@mui/material'
import type {Meta, StoryObj} from '@storybook/react'
import React from 'react'

import Toolbar from './Toolbar'
import ToolbarPanel from './ToolbarPanel'

const meta = {
  title: 'components/toolbar',
  component: Toolbar,
  tags: ['autodocs'],
  args: {
    children: [
      <ToolbarPanel
        key={0}
        index={0}
        icon={<FormatColorTextIcon />}
        tooltip={'formats'}
      >
        <IconButton size='small'>
          <FormatColorTextIcon />
        </IconButton>
        <IconButton size='small'>
          <FormatColorTextIcon />
        </IconButton>
        <IconButton size='small'>
          <FormatColorTextIcon />
        </IconButton>
        <IconButton size='small'>
          <FormatColorTextIcon />
        </IconButton>
        <IconButton size='small'>
          <FormatColorTextIcon />
        </IconButton>
      </ToolbarPanel>,
      <Divider key={'d1'} orientation='vertical' flexItem variant='middle' />,
      <ToolbarPanel
        key={1}
        index={2}
        icon={<FormatColorFillIcon />}
        tooltip={'colors'}
      >
        <IconButton size='small'>
          <FormatColorFillIcon />
        </IconButton>
        <IconButton size='small'>
          <FormatColorFillIcon />
        </IconButton>
        <IconButton size='small'>
          <FormatColorFillIcon />
        </IconButton>
        <IconButton size='small'>
          <FormatColorFillIcon />
        </IconButton>
        <IconButton size='small'>
          <FormatColorFillIcon />
        </IconButton>
      </ToolbarPanel>,
      <Divider key={'d2'} orientation='vertical' flexItem variant='middle' />,
      <ToolbarPanel
        key={2}
        index={1}
        icon={<FormatBoldIcon />}
        tooltip={'insert'}
      >
        <IconButton size='small'>
          <FormatBoldIcon />
        </IconButton>
        <IconButton size='small'>
          <FormatBoldIcon />
        </IconButton>
        <IconButton size='small'>
          <FormatBoldIcon />
        </IconButton>
        <IconButton size='small'>
          <FormatBoldIcon />
        </IconButton>
      </ToolbarPanel>,
    ],
  },
} satisfies Meta<typeof Toolbar>

export default meta

type Story = StoryObj<typeof meta>
export const Editable: Story = {}
