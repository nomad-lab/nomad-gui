import {PropsWithChildren} from 'react'

import {ToolbarContext, ToolbarContextType} from './toolbarContext'

export default function ToolbarProvider({
  collapsed = [],
  children,
}: PropsWithChildren<ToolbarContextType>) {
  return (
    <ToolbarContext.Provider value={{collapsed}}>
      {children}
    </ToolbarContext.Provider>
  )
}
