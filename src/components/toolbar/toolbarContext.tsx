import {createContext} from 'react'

export type ToolbarContextType = {
  collapsed: boolean[]
}

export const ToolbarContext = createContext<ToolbarContextType | undefined>(
  undefined,
)
