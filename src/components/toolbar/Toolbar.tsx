import {Box, Stack} from '@mui/material'
import React, {
  Children,
  PropsWithChildren,
  ReactNode,
  isValidElement,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react'
import {useResizeDetector} from 'react-resize-detector'

import {SxProps} from '../../utils/types'
import ToolbarPanel, {ToolbarPanelProps} from './ToolbarPanel'
import ToolbarProvider from './ToolbarProvider'

function isToolbarPanel(node: ReactNode): boolean {
  if (React.isValidElement(node)) {
    return (
      (React.isValidElement(node) && node.type === ToolbarPanel) ||
      (typeof node.type === 'function' && node.type.name === 'ToolbarPanel')
    )
  }
  return false
}

function countToolbarPanels(nodes: ReactNode): number {
  let count = 0
  Children.forEach(nodes, (node) => {
    if (React.isValidElement(node)) {
      if (isToolbarPanel(node)) {
        count += 1
      }
      if (node.props.children) {
        count += countToolbarPanels(node.props.children)
      }
    }
  })
  return count
}

export type ToolbarProps = PropsWithChildren<{
  actions?: React.ReactNode
}> &
  SxProps

export default function Toolbar({actions, children, sx}: ToolbarProps) {
  const arrayChildren = Children.toArray(children)
  const parentRef = useRef<HTMLDivElement>(null)
  const lastChildRef = useRef<HTMLDivElement>(null)
  const [collapsed, setCollapsed] = useState<boolean[]>([])
  const prevWidth = useRef(0)

  const checkOverflow = useCallback(() => {
    setCollapsed((collapsed) => {
      if (
        lastChildRef.current &&
        parentRef.current &&
        lastChildRef.current.offsetLeft +
          lastChildRef.current.clientWidth -
          parentRef.current.clientWidth >
          0 &&
        collapsed.includes(false)
      ) {
        const panels: boolean[] = [...collapsed.reverse()]
        for (const [index] of panels.entries()) {
          if (!panels?.[index]) {
            panels[index] = true
            return panels.reverse()
          }
        }
      }
      return collapsed
    })
  }, [])

  // TODO this implementation is not ideal
  //   - its prone to infinite loops
  //   - it overruns in tests, causing unpredictable behavior
  useEffect(() => {
    if (collapsed.length > 0) {
      setTimeout?.(() => {
        checkOverflow()
      }, 500)
    }
  }, [collapsed, checkOverflow])

  const nToolbarPanels = useMemo(() => {
    return countToolbarPanels(arrayChildren)
  }, [arrayChildren])

  useEffect(() => {
    setCollapsed(Array(nToolbarPanels).fill(false))
  }, [nToolbarPanels])

  const handleResize = useCallback(
    ({width}: {width: number | null}) => {
      if (width !== null) {
        const widthChange = Math.abs(width - prevWidth.current)
        if (widthChange >= 10) {
          setCollapsed(Array(nToolbarPanels).fill(false))
          prevWidth.current = width
        }
      }
    },
    [nToolbarPanels],
  )

  useResizeDetector({
    targetRef: parentRef,
    handleHeight: false,
    refreshMode: 'debounce',
    refreshRate: 200,
    onResize: handleResize,
  })

  return (
    <ToolbarProvider collapsed={collapsed}>
      <Stack
        direction='row'
        spacing={1}
        ref={parentRef}
        sx={sx}
        data-testid={'toolbar'}
        width={'500'}
      >
        {Children.map(arrayChildren, (child, index) => {
          if (isValidElement<ToolbarPanelProps>(child)) {
            return (
              <Box
                ref={lastChildRef}
                width={'fit-content'}
                display={'inline-flex'}
                data-testid={`toolbar-item-${index}`}
              >
                {child}
              </Box>
            )
          }
        })}
        <Box sx={{flexGrow: 1}} />
        {actions}
      </Stack>
    </ToolbarProvider>
  )
}
