import {Button} from '@mui/material'
import {act, render, screen} from '@testing-library/react'
import {vi} from 'vitest'

import {archiveRoute} from '../../pages/entry/entryRoute'
import {filesRoute} from '../../pages/upload/uploadRoute'
import Select, {SelectDialog, SelectDialogButton} from './Select'
import {Entity, SelectOptions} from './useSelect'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const navigateData: any = {}
const onRender = vi.fn()
vi.mock('../routing/MemoryRouter', () => ({
  default: ({
    url,
    onNavigate,
  }: {
    url: string
    onNavigate: (data: typeof navigateData) => void
  }) => {
    onRender(url)
    return (
      <a
        role='button'
        title='Navigate'
        onClick={() => onNavigate(navigateData)}
      />
    )
  },
}))

beforeEach(() => {
  navigateData.location = ''
  navigateData.fullMatch = [
    {
      route: {},
      response: {},
    },
  ]
  vi.resetAllMocks()
})

describe('Select', () => {
  it('renders', () => {
    render(<Select initialPath='/' />)
    expect(onRender).toHaveBeenCalledWith('/')
    expect(onRender).toHaveBeenCalledTimes(1)
  })
  it.each([
    ['file', {route: filesRoute, response: {m_is: 'File'}}, '/foo'],
    ['directory', {route: filesRoute, response: {m_is: 'Directory'}}, '/foo'],
    ['file', {route: filesRoute, response: {m_is: 'Directory'}}, undefined],
    ['nonExistedEntity', {}, undefined],
  ])('allows to navigate to %s', (entity, matchData, expectedChange) => {
    const handleChange = vi.fn()
    render(
      <Select
        initialPath='/'
        selectOptions={{entity: entity as Entity}}
        onChange={handleChange}
      />,
    )

    navigateData.location = '/foo'
    navigateData.fullMatch[0] = matchData

    act(() => screen.getByRole('button', {name: 'Navigate'}).click())
    expect(handleChange).toHaveBeenCalledWith(expectedChange)
  })
})

describe('SelectDialog', () => {
  it('renders open', () => {
    render(<SelectDialog initialPath='/' open />)
    expect(onRender).toHaveBeenCalledWith('/')
    expect(onRender).toHaveBeenCalledTimes(1)
  })

  it('renders with title and description', () => {
    render(
      <SelectDialog
        initialPath='/'
        open
        title='testTitle'
        description='testDescription'
      />,
    )
    expect(screen.getByText('testTitle')).toBeInTheDocument()
    expect(screen.getByText('testDescription')).toBeInTheDocument()
  })

  it('renders close', () => {
    render(<SelectDialog initialPath='/' open={false} />)
    expect(onRender).not.toHaveBeenCalled()
  })

  it('allows to select', () => {
    const handleSelect = vi.fn()
    render(
      <SelectDialog
        initialPath='/'
        selectOptions={{entity: 'file'}}
        open
        onSelect={handleSelect}
      />,
    )

    navigateData.location = '/foo'
    navigateData.fullMatch[0] = {
      route: filesRoute,
      response: {m_is: 'File'},
    }

    act(() => screen.getByRole('button', {name: 'Navigate'}).click())
    act(() => screen.getByRole('button', {name: 'Select'}).click())
    expect(handleSelect).toHaveBeenCalledWith('/foo')
  })

  it.each([
    [
      'just file',
      {entity: 'file'} as SelectOptions,
      {path: 'file', route: filesRoute, response: {m_is: 'File'}},
      true,
    ],
    [
      'file with pattern',
      {entity: 'file', filePattern: '.xyz'} as SelectOptions,
      {path: 'file.xyz', route: filesRoute, response: {m_is: 'File'}},
      true,
    ],
    [
      'file with pattern negative',
      {entity: 'file', filePattern: '.xyz'} as SelectOptions,
      {path: 'file.yz', route: filesRoute, response: {m_is: 'File'}},
      false,
    ],
    [
      'just section',
      {entity: 'section'} as SelectOptions,
      {route: archiveRoute, response: {m_def: {m_def: 'TestSection'}}},
      true,
    ],
    [
      'section with definition',
      {entity: 'section', sectionDefinition: 'TestSection'} as SelectOptions,
      {
        route: archiveRoute,
        response: {
          m_def: {
            m_ref: 'TestSection',
            all_base_sections: [],
          },
        },
      },
      true,
    ],
    [
      'section with definition matching base section',
      {entity: 'section', sectionDefinition: 'TestSection'} as SelectOptions,
      {
        route: archiveRoute,
        response: {
          m_def: {
            m_ref: 'OtherSection',
            all_base_sections: [
              {
                m_ref: 'TestSection',
              },
            ],
          },
        },
      },
      true,
    ],
    [
      'section with definition negative',
      {entity: 'section', sectionDefinition: 'TestSection'} as SelectOptions,
      {
        route: archiveRoute,
        response: {
          m_def: {
            m_ref: 'OtherSection',
            all_base_sections: [],
          },
        },
      },
      false,
    ],
  ])(
    'disables select button %s',
    (description, selectOptions, testNavigateData, expectedEnabled) => {
      render(
        <SelectDialog initialPath='/' selectOptions={selectOptions} open />,
      )

      Object.assign(navigateData.fullMatch[0], testNavigateData)
      act(() => screen.getByRole('button', {name: 'Navigate'}).click())

      const buttonElement = screen.getByRole('button', {name: 'Select'})
      if (expectedEnabled) {
        expect(buttonElement).not.toBeDisabled()
      } else {
        expect(buttonElement).toBeDisabled()
      }
    },
  )

  it('can be cancled', () => {
    const handleCancel = vi.fn()
    render(<SelectDialog initialPath='/' open onCancel={handleCancel} />)
    act(() => screen.getByRole('button', {name: 'Cancel'}).click())
    expect(handleCancel).toHaveBeenCalled()
  })
})

describe('SelectDialogButton', () => {
  it('renders', () => {
    render(
      <SelectDialogButton initialPath='/' component={Button}>
        testButton
      </SelectDialogButton>,
    )
    expect(screen.getByText('testButton')).toBeInTheDocument()
    expect(
      screen.queryByRole('button', {name: 'Navigate'}),
    ).not.toBeInTheDocument()
  })

  it('opens the dialog', () => {
    render(
      <SelectDialogButton initialPath='/' component={Button}>
        testButton
      </SelectDialogButton>,
    )
    act(() => screen.getByText('testButton').click())
    expect(screen.getByRole('button', {name: 'Navigate'})).toBeInTheDocument()
  })

  it('handles cancel', () => {
    const handleSelect = vi.fn()
    render(
      <SelectDialogButton
        initialPath='/'
        component={Button}
        onSelect={handleSelect}
      >
        testButton
      </SelectDialogButton>,
    )
    act(() => screen.getByText('testButton').click())
    act(() => screen.getByRole('button', {name: 'Cancel'}).click())
    expect(
      screen.queryByRole('button', {name: 'Navigate'}),
    ).not.toBeInTheDocument()
    expect(handleSelect).not.toHaveBeenCalled()
  })

  it('handles select', () => {
    const handleSelect = vi.fn()
    navigateData.location = '/foo'
    navigateData.fullMatch[0] = {
      route: filesRoute,
      response: {m_is: 'File'},
    }

    render(
      <SelectDialogButton
        initialPath='/'
        component={Button}
        onSelect={handleSelect}
        selectOptions={{entity: 'file'}}
      >
        testButton
      </SelectDialogButton>,
    )
    act(() => screen.getByText('testButton').click())
    act(() => screen.getByRole('button', {name: 'Navigate'}).click())
    act(() => screen.getByRole('button', {name: 'Select'}).click())
    expect(handleSelect).toHaveBeenCalledWith('/foo')
    expect(
      screen.queryByRole('button', {name: 'Navigate'}),
    ).not.toBeInTheDocument()
  })
})
