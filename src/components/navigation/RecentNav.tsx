import {useMemo} from 'react'

import useRecent from '../../hooks/useRecent'
import {Scope} from '../../hooks/useStorage'
import {SxProps} from '../../utils/types'
import Link from '../routing/Link'
import NavItem from './NavItem'
import useSelect from './useSelect'

export type RecentPage = {
  /**
   * A unique identifier of the page.
   */
  id: string

  /**
   * The name of the page, will be shown to the user
   * to select a recent page.
   */
  name: string

  /**
   * The URL of the page. Will be used during page navigation.
   */
  url: string

  /**
   * The URL of the page. Will be used during select navigation.
   */
  selectUrl: string
}

type RecentNavProps = {
  /**
   * The label of navigation bar
   */
  label: string
  /**
   * The recent items stored in the specified scope
   */
  scope: Scope
  /**
   * The recent items stored in the specified key in the scope
   */
  storageKey: string
}

/**
 * A component to display the recently visited pages
 */
export default function RecentNav(props: RecentNavProps & SxProps) {
  const {label, scope, storageKey, sx} = props
  const {recent} = useRecent<RecentPage>(scope, storageKey)
  const recentPages = useMemo(() => recent(5), [recent])
  const {isPage} = useSelect()

  if (recentPages.length === 0) {
    return ''
  }

  return (
    <NavItem expandable expanded label={label} sx={sx}>
      {recentPages.map((recent) => (
        <NavItem
          key={recent.id}
          variant='treeNode'
          label={recent.name || recent.id}
          component={Link}
          to={isPage ? recent.url : recent.selectUrl}
        />
      ))}
    </NavItem>
  )
}
