import {act, fireEvent, render, screen} from '@testing-library/react'
import {vi} from 'vitest'

import NavItem from './NavItem'

it('renders', () => {
  render(
    <NavItem label='hello' icon='icon'>
      content
    </NavItem>,
  )
  expect(screen.getByRole('button')).toBeInTheDocument()
  expect(screen.getByText('hello')).toBeInTheDocument()
  expect(screen.queryByTestId('toggle')).not.toBeInTheDocument()
  expect(screen.queryByText('content')).not.toBeInTheDocument()
})

it('renders with expandable', () => {
  const handleToggle = vi.fn()
  render(
    <NavItem label='hello' expandable onToggle={handleToggle}>
      content
    </NavItem>,
  )
  expect(screen.getByTestId('toggle')).toBeInTheDocument()
  expect(screen.getByText('content')).not.toBeVisible()
})

it('renders expanded', () => {
  render(
    <NavItem label='hello' expandable expanded>
      content
    </NavItem>,
  )
  expect(screen.getByText('content')).toBeVisible()
})

it('renders select', () => {
  render(<NavItem label='hello' selected />)
  const button = screen.getByRole('button')
  expect(window.getComputedStyle(button).backgroundColor).not.toBe(
    'transparent',
  )
})

it('calls onSelected', async () => {
  const handleSelect = vi.fn()
  render(<NavItem label='hello' onSelect={handleSelect} />)
  const button = screen.getByRole('button')
  act(() => fireEvent.click(button))
  expect(handleSelect).toHaveBeenCalled()
})

it('calls onToggle', async () => {
  const handleToggle = vi.fn()
  render(<NavItem label='hello' expandable onToggle={handleToggle} />)
  const button = screen.getByTestId('toggle')
  act(() => fireEvent.click(button))
  expect(handleToggle).toHaveBeenCalled()
})
