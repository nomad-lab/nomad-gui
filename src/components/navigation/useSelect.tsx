import React from 'react'

import {PageProps} from '../page/Page'

export type Entity = 'file' | 'directory' | 'section'

export type SelectOptions = {
  entity?: Entity
}

export type SectionSelectOptions = SelectOptions & {
  entity: 'section'
  sectionDefinition?: string
}

export type FileSelectOptions = SelectOptions & {
  entity: 'file'
  filePattern?: string
}

export type SelectContextValue = {
  isPage: boolean
  isSelect: boolean
  pageVariant: PageProps['variant']
  navigateToEntry: boolean
} & SelectOptions

export function createSelectContextValue(
  options: SelectOptions,
): SelectContextValue {
  const isPage = options.entity === undefined
  const entity = options.entity as string
  return {
    isPage,
    isSelect: !isPage,
    pageVariant: isPage ? 'page' : 'dialog',
    navigateToEntry: isPage || !['file', 'directory'].includes(entity),
    ...options,
  }
}

const emptySelectContextValue = createSelectContextValue({})

export const selectContext = React.createContext<SelectContextValue>(
  emptySelectContextValue,
)

export default function useSelect() {
  return React.useContext(selectContext)
}
