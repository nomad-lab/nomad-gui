import {Typography} from '@mui/material'
import {useCallback, useEffect, useMemo, useRef, useState} from 'react'
import {useFirstMountState, usePrevious} from 'react-use'

import {JSONObject} from '../../utils/types'
import {getResponseForPath} from '../routing/loader'
import useDataForRoute from '../routing/useDataForRoute'
import useRoute from '../routing/useRoute'
import {useAvailableRouteData} from '../routing/useRouteData'
import NavItem, {NavItemProps} from './NavItem'

export type TreeNavProps = {
  /**
   * The request object that is used to fetch children.
   */
  request: JSONObject
  /**
   * Determines if available route data is sufficient to render the tree item
   * or if the data has to be fetched. By default route data will always be
   * sufficient. This might be necessary, if route data is influenced by
   * pagination.
   */
  dataIsSufficient?: (data: JSONObject, key: string) => boolean
  /**
   * Filters the children keys of the node. By default children for all keys are
   * shown.
   */
  filterChildKeys?: (data: JSONObject, key: string) => boolean
  /**
   * Provides additional NavMenuItemProps like label or icons that
   * are used to render the child tree items.
   *
   * @param data - The data of the node with all the children.
   * @param key - The key of the child within its parent.
   */
  getChildProps?: (
    data: JSONObject,
    key: string,
    path: string,
  ) => Partial<TreeNavProps>
  /**
   * Comparison function that is used to determine the order of children.
   * @param data - The data of the node with all the children
   * @param a - The key of the first child
   * @param b - The key of the second child
   */
  orderCompareFn?: (data: JSONObject, a: string, b: string) => number
  /**
   * The initial expanded state of the tree item, default is false.
   */
  defaultExpanded?: boolean
  /**
   * The path of the item relative the route of the rendering component.
   * The path has to be compatible with both the routing and API. API keys
   * that diverge from routes is not implemented.
   */
  path: string
  /**
   * The depth of the tree item, default is 0. Only used for the internal
   * recursive application of the component.
   */
  depth?: number

  expandable?: boolean | ((data: JSONObject) => boolean)
} & Partial<Omit<NavItemProps, 'expandable'>>

/**
 * An extension of the NavItem component that produces a tree of child nav items.
 * The children are fetched from the API or available route data. How the
 * children are acquired and presented is determined by the `request`, `getChildProps`, and
 * `orderCompareFn`, `dataIsSufficient` props. The component will control the
 * `expanded` and `selected` if now `expanded` or `selected` values are provided.
 * Typically, clients will control these state on the top level of the tree and
 * the component will control this for all children through out the tree.
 */
export default function TreeNav({
  path,
  request,
  dataIsSufficient = () => true,
  filterChildKeys = () => true,
  getChildProps = () => ({}),
  orderCompareFn,
  depth = 0,
  children,
  expanded: controlledExpanded,
  onToggle,
  defaultExpanded,
  expandable: isExpandable = false,
  ...navItemProps
}: TreeNavProps) {
  const expandedIsControlled = controlledExpanded !== undefined

  const previousRequest = usePrevious(request)
  if (previousRequest !== request && previousRequest) {
    // eslint-disable-next-line no-console
    console.warn(
      'Request must not change. Might happen on hot update, otherwise: ' +
        'did you forget to memoize it?',
    )
  }
  const splitPath = useMemo(() => path.split('/'), [path])
  const key = useMemo(() => splitPath.at(-1) || 'no path', [splitPath])

  const useDataForRouteParams = useMemo(
    () => ({request: {[path]: request}, noImplicitFetch: true}),
    [request, path],
  )
  const dataForRouteResult = useDataForRoute(useDataForRouteParams)
  const {fetch, loading} = dataForRouteResult
  const fetchedData =
    dataForRouteResult.data && getResponseForPath(path, dataForRouteResult.data)
  const hasFetchedData = !!fetchedData

  const {index, fullMatch, navigate} = useRoute()
  const routePath = useMemo(
    () =>
      fullMatch
        .slice(index + 1)
        .map((match) => match.path)
        .join('/'),
    [fullMatch, index],
  )
  const leastChildKey = useMemo(() => {
    return fullMatch[index + splitPath.length + 1]?.path
  }, [fullMatch, index, splitPath])
  const inRoute = routePath.substring(0, path.length) === path
  const selected = path === routePath
  const justGotInRoute = usePrevious(inRoute) !== inRoute && inRoute
  const justSelected = usePrevious(selected) !== selected && selected

  const availableRouteData = useAvailableRouteData<JSONObject, JSONObject>()
  const currentAvailableRouteData = useMemo(
    () =>
      availableRouteData && getResponseForPath(path, availableRouteData, false),
    [availableRouteData, path],
  )
  const availableRouteDataIsSufficient =
    currentAvailableRouteData &&
    dataIsSufficient(currentAvailableRouteData, key)

  const currentData = useRef<JSONObject | undefined>()
  if (fetchedData && !loading) {
    currentData.current = fetchedData
  } else if (currentAvailableRouteData && availableRouteDataIsSufficient) {
    currentData.current = currentAvailableRouteData
  }

  const expandable =
    typeof isExpandable === 'function'
      ? isExpandable(currentData.current as JSONObject)
      : isExpandable

  const [expandedByToggle, setExpandedByToggle] = useState(false)
  const [internalExpanded, setInternalExpanded] = useState(
    !!defaultExpanded && expandable,
  )
  const expanded = expandedIsControlled ? controlledExpanded : internalExpanded
  const setControlledExpanded = useCallback(
    (value: boolean) => {
      if (onToggle && value !== controlledExpanded) {
        onToggle()
      }
    },
    [controlledExpanded, onToggle],
  )
  const setExpanded = expandedIsControlled
    ? setControlledExpanded
    : setInternalExpanded

  const handleSelect = useCallback(
    // Empty search will reset potential pagination/sorting search params
    () => navigate({path, search: {}}),
    [navigate, path],
  )

  const handleExpand = useCallback(() => {
    if (expandable) {
      setExpanded(true)
      return fetch()
    }
  }, [setExpanded, fetch, expandable])

  const handleCollapse = useCallback(() => {
    setExpanded(false)
    setExpandedByToggle(false)
  }, [setExpanded])

  const handleToggle = useCallback(() => {
    if (expanded) {
      handleCollapse()
    } else {
      handleExpand()
      setExpandedByToggle(true)
    }
  }, [expanded, handleExpand, handleCollapse])

  const isFirstMountState = useFirstMountState()

  useEffect(() => {
    if (isFirstMountState && defaultExpanded) {
      return handleExpand()
    }
    if (!justGotInRoute || !expandable) {
      return
    }
    setExpanded(true)
    if (availableRouteDataIsSufficient) {
      return
    }
    if (!hasFetchedData || (justSelected && !expanded)) {
      return fetch()
    }
  }, [
    justGotInRoute,
    availableRouteDataIsSufficient,
    fetch,
    justSelected,
    expanded,
    expandable,
    hasFetchedData,
    setExpanded,
    defaultExpanded,
    handleExpand,
    isFirstMountState,
  ])

  const renderData = currentData.current
  const renderExpanded = expanded || (justGotInRoute && expandable)
  const renderSelected = selected || (inRoute && (!renderExpanded || loading))

  return (
    <NavItem
      expanded={renderExpanded}
      selected={renderSelected}
      highlighted={selected}
      onToggle={handleToggle}
      onSelect={handleSelect}
      expandable={expandable}
      label={key}
      sx={{
        '&>.NavItem-button': {
          paddingLeft: depth === 0 ? undefined : (depth - 1) * 2,
        },
        '&.NavItem-root': {
          position: 'relative',
        },
        ...(depth > 0 && {
          '&>.NavItem-content::before': {
            content: '""',
            position: 'absolute',
            top: 42,
            bottom: -4,
            left: 16 + depth * 16,
            borderLeftWidth: 1,
            borderLeftStyle: 'solid',
            borderLeftColor: 'divider',
          },
        }),
      }}
      {...navItemProps}
    >
      {renderExpanded && (
        <>
          {children}
          {!loading ? (
            renderData &&
            Object.keys(renderData)
              .filter((key) => !key.startsWith('m_'))
              .filter((key) => filterChildKeys(renderData, key))
              .toSorted((a, b) =>
                orderCompareFn
                  ? orderCompareFn(renderData, a, b)
                  : a.localeCompare(b),
              )
              .map((key) => {
                return (
                  <TreeNav
                    key={key}
                    path={`${path}/${key}`}
                    expandable={expandable}
                    request={request}
                    dataIsSufficient={dataIsSufficient}
                    getChildProps={getChildProps}
                    orderCompareFn={orderCompareFn}
                    {...getChildProps(renderData as JSONObject, key, path)}
                    depth={depth + 1}
                  />
                )
              })
          ) : (
            <>
              <Typography sx={{marginLeft: depth * 2 + 4, marginY: 1}}>
                loading...
              </Typography>
              {/* Even when loading, at least render the children from the route. */}
              {leastChildKey &&
                !expandedByToggle &&
                currentAvailableRouteData?.[leastChildKey] && (
                  <TreeNav
                    key={leastChildKey}
                    path={`${path}/${leastChildKey}`}
                    expandable={expandable}
                    request={request}
                    dataIsSufficient={dataIsSufficient}
                    getChildProps={getChildProps}
                    orderCompareFn={orderCompareFn}
                    {...getChildProps(
                      currentAvailableRouteData as JSONObject,
                      leastChildKey,
                      path,
                    )}
                    depth={depth + 1}
                  />
                )}
            </>
          )}
        </>
      )}
    </NavItem>
  )
}
