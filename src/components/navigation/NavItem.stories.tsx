import DirectoryIcon from '@mui/icons-material/FolderOutlined'
import FileIcon from '@mui/icons-material/InsertDriveFileOutlined'
import {Box} from '@mui/material'
import type {Meta, StoryObj} from '@storybook/react'

import NavItem from './NavItem'

const meta = {
  title: 'components/navigation/NavItem',
  component: NavItem,
  decorators: [
    (Story) => (
      <Box
        sx={{
          width: '390px',
          border: 1,
          borderColor: 'grey.300',
          margin: 1,
          paddingY: 1,
        }}
      >
        <Story />
      </Box>
    ),
  ],
  tags: ['autodocs'],
} satisfies Meta<typeof NavItem>

export default meta

type Story = StoryObj<typeof meta>
export const Root: Story = {
  args: {
    selected: true,
    expanded: true,
    highlighted: true,
    label: 'Files',
    expandable: true,
    children: (
      <>
        <NavItem
          label='child 2'
          alignWithExpandIcons
          expandable
          variant='treeNode'
          icon={<DirectoryIcon />}
        >
          HelloWorld
        </NavItem>
        <NavItem
          label='child 1'
          alignWithExpandIcons
          selected
          variant='treeNode'
          icon={<FileIcon />}
        />
      </>
    ),
  },
}

export const WithChildren: Story = {
  args: {
    selected: false,
    label: 'Click me',
    children: 'Hello World',
  },
}

export const Selected: Story = {
  args: {
    selected: true,
    label: 'Click me',
  },
}

export const Expanded: Story = {
  args: {
    expanded: true,
    selected: true,
    expandable: true,
    label: 'Click me',
    children: 'Hello World',
  },
}

export const LongLabel: Story = {
  args: {
    label: 'This is a very long label that does not fit into the menu',
  },
}
