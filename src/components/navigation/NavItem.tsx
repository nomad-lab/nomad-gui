import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import {
  ButtonBase,
  Collapse,
  IconButton,
  IconButtonTypeMap,
  Typography,
  generateUtilityClasses,
  styled,
} from '@mui/material'
import {OverridableComponent} from '@mui/material/OverridableComponent'
import React, {ReactNode, useCallback} from 'react'

import {SxProps} from '../../utils/types'

type NavItemOwnerState = NavItemProps

const classes = generateUtilityClasses('NavItem', [
  'root',
  'button',
  'icon',
  'label',
  'toggleButton',
  'content',
])

const NavItemToggleButton = styled(IconButton, {
  name: 'NavItem',
  slot: 'toggleButton',
})<{ownerState: NavItemOwnerState}>(({theme, ownerState}) => {
  const {expandable, expanded, variant = 'menuItem'} = ownerState
  return {
    marginY: -theme.spacing(1),
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(variant === 'treeNode' ? 1 : 0),
    order: variant === 'menuItem' ? 1 : 0,
    padding: 0,
    color: theme.palette.action.active,
    '&.Mui-disabled': {
      color: expandable ? theme.palette.action.active : 'rgba(0, 0, 0, 0)',
    },
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    ...(expanded
      ? {
          transform: 'rotate(0deg)',
        }
      : {
          transform: 'rotate(-90deg)',
        }),
  }
}) as OverridableComponent<IconButtonTypeMap<{ownerState: NavItemOwnerState}>>

const NavItemRoot = styled('div', {name: 'NavItem', slot: 'root'})(() => ({}))

const NavItemButton = styled(ButtonBase, {
  name: 'NavItem',
  slot: 'button',
})<{ownerState: NavItemOwnerState}>(
  ({
    theme,
    ownerState: {selected = false, disabled = false, highlighted = false},
  }) => ({
    borderRadius: theme.spacing(1),
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: `calc(100% - ${theme.spacing(2)})`,
    height: 42,
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    paddingRight: theme.spacing(1),
    paddingLeft: theme.spacing(2),
    justifyContent: 'start',
    ...(selected && {
      backgroundColor: theme.palette.action.selected,
    }),
    color: disabled ? theme.palette.action.disabled : undefined,
    '&:hover': {
      backgroundColor: theme.palette.action.selected,
    },
    ...(highlighted && {
      '&::before': {
        display: 'block',
        content: '""',
        position: 'absolute',
        left: 4,
        top: 6,
        bottom: 6,
        borderRadius: 3,
        backgroundColor: theme.palette.primary.main,
        width: 6,
      },
    }),
  }),
)

const NavItemIcon = styled('span', {name: 'NavItem', slot: 'icon'})(
  ({theme}) => ({
    marginRight: theme.spacing(1),
    paddingTop: 2,
    color: theme.palette.action.active,
  }),
)

const NavItemLabel = styled(Typography, {name: 'NavItem', slot: 'label'})<{
  ownerState: NavItemOwnerState
}>(({ownerState: {variant = 'menuItem'}}) => ({
  flexGrow: 1,
  textAlign: 'left',
  overflow: 'clip',
  textOverflow: 'ellipsis',
  width: '100%',
  whiteSpace: 'nowrap',
  ...(variant === 'menuItem' && {
    textTransform: 'uppercase',
    fontSize: 14,
    fontWeight: 700,
  }),
}))

const NavItemContent = styled(Collapse, {name: 'NavItem', slot: 'content'})(
  ({theme}) => ({
    '& .MuiCollapse-wrapperInner': {
      display: 'flex',
      flexDirection: 'column',
      gap: theme.spacing(1),
      marginTop: theme.spacing(1),
    },
  }),
)

export type NavItemProps = {
  /**
   * Determines if the nav item is expandable and might have children.
   */
  expandable?: boolean
  /**
   * Determines if the nav item is expanded and shows its children.
   */
  expanded?: boolean
  /**
   * Called when the users clicks the collapse/expand button.
   */
  onToggle?: () => void
  /**
   * Determines if the nav item is selected.
   */
  selected?: boolean
  /**
   * When the user clicks on the nav item.
   */
  onSelect?: () => void
  /**
   * Determines if the nav item is highlighted with the primary color.
   */
  highlighted?: boolean
  /**
   * The component to render the nav item as. Default is button.
   */
  component?: React.ElementType
  /**
   * This prop is passed to the underlying component.
   */
  to?: string
  /**
   * An optional icon to display before the label.
   */
  icon?: ReactNode
  /**
   * A label for the nav item.
   */
  label: string
  /**
   * Will layout the nav item to align with the expand icons of siblings, even
   * if the nav item is not expandable.
   */
  alignWithExpandIcons?: boolean
  /**
   * The children of the nav item. They are only shown if the nav item is
   * `expandable` and `expanded`.
   */
  children?: ReactNode
  /**
   * The variant of the nav item. Default is `menuItem` and is used to produce
   * a top-level nav item menu. The `treeNode` variant is used to create trees.
   * The `treeNode` variant has the expand icon button on the left, the `menuItem`
   * on the right.
   */
  variant?: 'menuItem' | 'treeNode'
  /**
   * The item is disabled. The appreance is different and all actions are
   * disabled.
   */
  disabled?: boolean
}

/**
 * Nav item component to populate the left-hand navigation of a page. It has two
 * controlled states `selected` and `expanded` (`onSelect`, `onToggle`).
 * Select determines if the nav item is highlighted. Expanded determines if the
 * children are shown or not. The `expandable` prop determines if the nav item
 * is expandable in principle; even nav items without children can be expandable.
 */
export default function NavItem(props: NavItemProps & SxProps) {
  const {
    expanded = false,
    expandable = false,
    highlighted,
    onSelect,
    onToggle,
    icon,
    label,
    alignWithExpandIcons = false,
    children,
    sx = [],
    ...buttonBaseProps
  } = props
  const handleItemClick = useCallback(() => onSelect?.(), [onSelect])
  const handleExpandClick = useCallback(
    (event: React.MouseEvent<HTMLElement>) => {
      event.stopPropagation()
      event.nativeEvent.stopImmediatePropagation()
      onToggle?.()
    },
    [onToggle],
  )

  return (
    <NavItemRoot className={classes.root} sx={sx}>
      <NavItemButton
        className={classes.button}
        ownerState={props}
        onClick={handleItemClick}
        disabled={!onSelect && !buttonBaseProps.to}
        {...buttonBaseProps}
      >
        {((expandable && onToggle) || alignWithExpandIcons) && (
          <NavItemToggleButton
            data-testid='toggle'
            className={classes.toggleButton}
            ownerState={props}
            size='small'
            component='span'
            disabled={!expandable}
            onClick={handleExpandClick}
          >
            <ExpandMoreIcon />
          </NavItemToggleButton>
        )}
        {icon && <NavItemIcon className={classes.icon}>{icon}</NavItemIcon>}
        <NavItemLabel className={classes.label} ownerState={props}>
          {label}
        </NavItemLabel>
      </NavItemButton>
      {expandable && children && (
        <NavItemContent
          className={classes.content}
          orientation='vertical'
          in={expanded}
          timeout={0}
        >
          {children}
        </NavItemContent>
      )}
    </NavItemRoot>
  )
}
