import {styled} from '@mui/material'

const Nav = styled('div', {name: 'Nav', slot: 'root'})(({theme}) => ({
  display: 'flex',
  flexDirection: 'column',
  gap: theme.spacing(1),
  marginTop: theme.spacing(1),
  marginBottom: theme.spacing(1),
}))

export default Nav
