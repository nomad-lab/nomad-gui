import ArrowBackIcon from '@mui/icons-material/ChevronLeft'
import {Box, Button, Typography, styled} from '@mui/material'
import {PropsWithChildren} from 'react'

import Link from '../../components/routing/Link'
import Text from '../values/primitives/Text'
import useSelect from './useSelect'

export type NavAboutProps = {
  label: string
  title: string
  overviewUrl?: string
  selectUrl?: string
} & PropsWithChildren

const NavAboutRoot = styled(Box, {
  name: 'NavAbout',
  slot: 'root',
})(({theme}) => ({
  padding: theme.spacing(1),
  paddingLeft: theme.spacing(2),
  paddingRight: theme.spacing(2),
  marginLeft: theme.spacing(1),
  marginRight: theme.spacing(1),
  borderRadius: theme.spacing(1),
}))

export default function NavAbout({
  label,
  title,
  overviewUrl,
  selectUrl,
  children,
}: NavAboutProps) {
  const {isPage} = useSelect()

  return (
    <NavAboutRoot>
      <Typography variant='overline'>{label}</Typography>
      <Box sx={{marginBottom: 1, marginTop: 0.5}}>
        <Text
          value={title}
          fullWidth
          sx={{
            color: (theme) => theme.palette.primary.main,
            fontWeight: 'bold',
            fontSize: '1.2rem',
          }}
        />
      </Box>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          flexDirection: 'row',
          gap: 1,
        }}
      >
        {children}
      </Box>
      {overviewUrl && selectUrl && (
        <Button
          sx={{marginTop: 2}}
          startIcon={<ArrowBackIcon />}
          component={Link}
          to={isPage ? overviewUrl : selectUrl}
          variant='contained'
        >{`go to ${label}${isPage ? ' overview' : ''}`}</Button>
      )}
    </NavAboutRoot>
  )
}
