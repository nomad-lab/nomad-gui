import {act, render, screen} from '@testing-library/react'
import React, {useMemo} from 'react'
import {MockedFunction, it, vi} from 'vitest'

import * as api from '../../utils/api'
import {JSONObject} from '../../utils/types'
import Outlet from '../routing/Outlet'
import Router from '../routing/Router'
import loader from '../routing/loader'
import {Route} from '../routing/types'
import useRoute from '../routing/useRoute'
import TreeNav, {TreeNavProps} from './TreeNav'

function renderPageWithTreeNav(
  treeNavProps: Partial<TreeNavProps> = {},
  url: string = '/parent/else',
  withRouteRequest: boolean = false,
) {
  const testContext = React.createContext(treeNavProps)
  const recursiveSubRoute: Route = {
    path: ':data',
    component: function Component() {
      const {match, index} = useRoute()
      return <div>{`page-${match[index].path}`}</div>
    },
    onlyRender: '',
    request: withRouteRequest ? () => ({'*': '*'}) : undefined,
  }

  recursiveSubRoute.children = [recursiveSubRoute]

  const route: Route = {
    path: '',
    component: () => <Outlet />,
    children: [
      {
        path: 'parent',
        component: function Component() {
          const treeNavProps = React.useContext(testContext)
          const request = useMemo(() => ({'*': '*'}), [])
          return (
            <>
              <TreeNav
                request={request}
                path='data'
                label='nav-data'
                expandable
                getChildProps={(_, path) => ({
                  expandable: path === 'data',
                  label: `nav-${path}`,
                })}
                {...treeNavProps}
              />
              <Outlet />
            </>
          )
        },
        children: [
          {
            ...recursiveSubRoute,
            path: 'data',
          },
          {
            path: 'else',
            component: () => <div>page-else</div>,
          },
        ],
      },
    ],
  }

  window.history.replaceState(null, '', url)
  // We need to act (and later await) here, because an initial effect will cause
  // another render with the same results and we need to wait for that to finish.
  return act(async () =>
    render(
      <testContext.Provider value={treeNavProps}>
        <Router route={route} loader={loader} />,
      </testContext.Provider>,
    ),
  )
}

let mockedApi: MockedFunction<(request: JSONObject) => JSONObject>

beforeEach(() => {
  mockedApi = vi.spyOn(api, 'graphApi') as unknown as MockedFunction<
    (request: JSONObject) => JSONObject
  >
})

afterEach(() => {
  vi.restoreAllMocks()
})

function clickToggleButton(navItemLabel: string) {
  const toggleButton = screen
    .getByText(navItemLabel)
    .closest('.NavItem-root')
    ?.querySelector('.NavItem-toggleButton') as HTMLButtonElement
  expect(toggleButton).not.toBeNull()
  return toggleButton.click()
}

it('does not fetch data when collapsed', async () => {
  await renderPageWithTreeNav()
  expect(screen.getByText('page-else')).toBeInTheDocument()
  expect(screen.getByText('nav-data')).toBeInTheDocument()
  expect(mockedApi).not.toHaveBeenCalled()
})

it('fetches data when expanded', async () => {
  mockedApi.mockResolvedValue({parent: {data: {child: {}}}})
  await renderPageWithTreeNav({defaultExpanded: true})
  expect(screen.getByText('nav-child')).toBeInTheDocument()
  expect(screen.queryByText(/loading/)).not.toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)
})

it('forgets and shows no data when collapsed', async () => {
  mockedApi.mockResolvedValue({parent: {data: {child: {}}}})
  await renderPageWithTreeNav({defaultExpanded: true})
  expect(await screen.findByText('nav-child')).toBeInTheDocument()
  act(() => clickToggleButton('nav-data'))
  expect(screen.queryByText('nav-child')).not.toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)
})

it('re-fetches data when re-expanded', async () => {
  mockedApi.mockResolvedValue({parent: {data: {child: {}}}})
  await renderPageWithTreeNav({defaultExpanded: true})
  expect(await screen.findByText('nav-child')).toBeInTheDocument()
  act(() => clickToggleButton('nav-data'))
  mockedApi.mockResolvedValue({parent: {data: {newChild: {}}}})
  act(() => clickToggleButton('nav-data'))
  expect(await screen.findByText('nav-newChild')).toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(2)
})

it('fetches data when url points to collapsed item', async () => {
  await renderPageWithTreeNav({}, '/parent/else')
  mockedApi.mockResolvedValue({parent: {data: {newChild: {}}}})
  act(() => window.history.pushState(null, '', '/parent/data'))
  expect(await screen.findByText('nav-newChild')).toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)
})

it('re-fretches data when urls points to expanded item', async () => {
  mockedApi.mockResolvedValue({parent: {data: {child: {}}}})
  await renderPageWithTreeNav({defaultExpanded: true})
  expect(await screen.findByText('nav-child')).toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)
  act(() => clickToggleButton('nav-data'))
  expect(screen.queryByText('nav-child')).not.toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)
  mockedApi.mockResolvedValue({parent: {data: {newChild: {}}}})
  act(() => window.history.pushState(null, '', '/parent/data'))
  expect(screen.getByText(/loading/)).toBeInTheDocument()
  expect(await screen.findByText('nav-newChild')).toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(2)
})

it('fetches data when url points to a child of collapsed item', async () => {
  mockedApi.mockResolvedValue({parent: {data: {child: {}}}})
  await renderPageWithTreeNav({}, '/parent/data/child')
  expect(screen.getByText('nav-child')).toBeInTheDocument()
  expect(screen.queryByText(/loading/)).not.toBeInTheDocument()
  expect(screen.getByText('page-child')).toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)
})

it('does not re-fetch when url points to a child of an already expanded item', async () => {
  mockedApi.mockResolvedValue({parent: {data: {child: {}}}})
  await renderPageWithTreeNav({defaultExpanded: true})
  expect(await screen.findByText('nav-child')).toBeInTheDocument()

  mockedApi.mockResolvedValue({parent: {data: {newChild: {}}}})
  act(() => window.history.pushState(null, '', '/parent/data/child'))
  expect(await screen.findByText('page-child')).toBeInTheDocument()
  expect(screen.getByText('nav-child')).toBeInTheDocument()
  expect(screen.queryByText('nav-newChild')).not.toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)
})

it('shows stale data when url points away', async () => {
  mockedApi.mockResolvedValue({parent: {data: {child: {}}}})
  await renderPageWithTreeNav({}, '/parent/data/child')
  expect(await screen.findByText('nav-child')).toBeInTheDocument()

  act(() => window.history.pushState(null, '', '/parent/else'))
  expect(screen.queryByText('nav-child')).toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)
})

it('shows stale data when only search params change', async () => {
  mockedApi.mockResolvedValue({parent: {data: {child: {}}}})
  await renderPageWithTreeNav({}, '/parent/data/child')
  expect(await screen.findByText('nav-child')).toBeInTheDocument()

  act(() => window.history.pushState(null, '', '/parent/data/child?search=1'))
  expect(screen.queryByText('nav-child')).toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)
})

it('does not fetch when route data is suffcient', async () => {
  mockedApi.mockResolvedValue({parent: {data: {m_response: {}, child: {}}}})
  await renderPageWithTreeNav({}, '/parent/data', true)
  expect(await screen.findByText('page-data')).toBeInTheDocument()
  expect(await screen.findByText('nav-child')).toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)
})

it('fetches data when route data is not suffcient', async () => {
  mockedApi.mockResolvedValue({parent: {data: {child: {}}}})
  await renderPageWithTreeNav(
    {defaultExpanded: true, dataIsSufficient: () => false},
    '/parent/else',
    true,
  )
  expect(screen.getByText('page-else')).toBeInTheDocument()
  expect(await screen.findByText('nav-child')).toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)
})

it('fetches data for current and parent nodes when route data is insufficient', async () => {
  mockedApi.mockResolvedValue({parent: {data: {child: {child: {}}}}})
  await renderPageWithTreeNav(
    {
      dataIsSufficient: () => false,
      getChildProps: (_, key) => ({
        expandable: true,
        label: `nav-${key}`,
      }),
    },
    '/parent/data/child/child',
    true,
  )
  expect(screen.getByText('page-child')).toBeInTheDocument()
  expect(screen.getAllByText('nav-child').length).toBe(2)
  // called for the route, for the parent, and for the two children = x4
  expect(mockedApi).toHaveBeenCalledTimes(4)
})

it('fetches when re-expanded even if route data is suffcient', async () => {
  mockedApi.mockResolvedValue({parent: {data: {child: {}}}})
  await renderPageWithTreeNav(
    {dataIsSufficient: () => true},
    '/parent/data',
    true,
  )
  expect(screen.getByText('nav-child')).toBeInTheDocument()

  act(() => clickToggleButton('nav-data'))
  expect(screen.queryByText('nav-child')).not.toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)

  act(() => clickToggleButton('nav-data'))
  expect(screen.getByText(/loading/)).toBeInTheDocument()
  expect(await screen.findByText('nav-child')).toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(2)
})

it('does not update children with insufficient route data', async () => {
  mockedApi.mockResolvedValue({parent: {data: {m_sufficient: true, child: {}}}})
  await renderPageWithTreeNav(
    {dataIsSufficient: (data) => !!data.m_sufficient},
    '/parent/data',
    true,
  )
  expect(await screen.findByText('page-data')).toBeInTheDocument()
  expect(await screen.findByText('nav-child')).toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)

  act(() => window.history.pushState(null, '', '/parent/else'))
  expect(await screen.findByText('page-else')).toBeInTheDocument()
  expect(screen.getByText('nav-child')).toBeInTheDocument()
  expect(mockedApi).toHaveBeenCalledTimes(1)
})
