import HomeIcon from '@mui/icons-material/Home'
import {IconButton, Link, Breadcrumbs as MUIBreadcrumbs} from '@mui/material'

import RouterLink from '../routing/Link'
import useRoute from '../routing/useRoute'
import useSelect from './useSelect'

export default function Breadcrumbs() {
  const {fullMatch} = useRoute()
  const {isSelect} = useSelect()
  return (
    <MUIBreadcrumbs aria-label='breadcrumb'>
      {fullMatch.map((item, index) => {
        if (index === 0) {
          if (isSelect) {
            return ''
          }
          return (
            <IconButton
              component={RouterLink}
              key={index}
              to='/'
              size='small'
              color='primary'
            >
              <HomeIcon sx={{fontSize: '1em'}} />
            </IconButton>
          )
        }

        const isLast = fullMatch
          .slice(index + 1)
          .every((match) => match.path === '')

        // TODO this is very hack and should be specified in the
        // route configuration
        const linkOnlyInPage = [':entryId', ':uploadId'].includes(
          item.route.path,
        )

        const content =
          item.route.breadcrumb === undefined
            ? item.path
            : typeof item.route.breadcrumb === 'function'
            ? item.route.breadcrumb(item)
            : item.route.breadcrumb

        return isLast || (isSelect && linkOnlyInPage) ? (
          <span key={index}>{content}</span>
        ) : (
          <RouterLink
            key={index}
            to={fullMatch
              .slice(0, index + 1)
              .map((match) => match.path)
              .map(encodeURIComponent)
              .join('/')}
          >
            <Link>{content}</Link>
          </RouterLink>
        )
      })}
    </MUIBreadcrumbs>
  )
}
