import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogProps,
  DialogTitle,
} from '@mui/material'
import {PropsWithChildren, useCallback, useState} from 'react'

import {archiveRoute} from '../../pages/entry/entryRoute'
import {filesRoute} from '../../pages/upload/uploadRoute'
import uploadsRoute from '../../pages/uploads/uploadsRoute'
import {Section} from '../../utils/metainfo'
import {assert} from '../../utils/utils'
import {LoadingIndicator} from '../app/App'
import ErrorMessage from '../app/ErrorMessage'
import Page from '../page/Page'
import MemoryRouter from '../routing/MemoryRouter'
import Outlet from '../routing/Outlet'
import loader from '../routing/loader'
import {RouteData} from '../routing/types'
import useRouteError from '../routing/useRouteError'
import {
  FileSelectOptions,
  SectionSelectOptions,
  SelectOptions,
  createSelectContextValue,
  selectContext,
} from './useSelect'

const rootRoute = {
  path: '',
  component: function Component() {
    return (
      <Box sx={{height: '70vh', position: 'relative'}}>
        <LoadingIndicator />
        <Outlet />
      </Box>
    )
  },

  errorComponent: function Component() {
    const error = useRouteError()
    return (
      <Box sx={{height: '70vh', position: 'relative'}}>
        <Page fullwidth variant='dialog'>
          <ErrorMessage error={error} />
        </Page>
      </Box>
    )
  },

  children: [uploadsRoute],
}

export type SelectDialogButtonProps<
  T extends React.ComponentType,
  S extends SelectOptions = SelectOptions,
> = {
  component: T
} & Omit<React.ComponentProps<T>, 'onSelect'> &
  PropsWithChildren &
  Omit<SelectDialogProps<S>, 'onCancel' | 'open'>

/**
 * A button that opens a `SelectDialog` when clicked.
 */
export function SelectDialogButton<
  T extends React.ComponentType,
  S extends SelectOptions = SelectOptions,
>({
  children,
  component,
  initialPath,
  title,
  description,
  selectButtonLabel,
  onSelect,
  selectOptions,
  ...buttonProps
}: SelectDialogButtonProps<T, S>) {
  const Component = component as React.ComponentType<
    PropsWithChildren<typeof buttonProps> & {onClick: () => void}
  >
  const [open, setOpen] = useState(false)
  const handleClick = useCallback(() => setOpen(true), [setOpen])
  const handleClose = useCallback(() => setOpen(false), [setOpen])
  const handleSelect = useCallback(
    (path: string) => {
      setOpen(false)
      onSelect?.(path)
    },
    [setOpen, onSelect],
  )
  return (
    <>
      <Component {...buttonProps} title={title} onClick={handleClick}>
        {children}
      </Component>
      <SelectDialog
        open={open}
        onCancel={handleClose}
        onSelect={handleSelect}
        selectOptions={selectOptions}
        initialPath={initialPath}
        title={title}
        description={description}
        selectButtonLabel={selectButtonLabel}
      />
    </>
  )
}

export type SelectDialogProps<S extends SelectOptions = SelectOptions> = {
  selectOptions?: S
  initialPath: string
  title?: string
  description?: string
  selectButtonLabel?: string
  onSelect?: (selectedPath: string) => void
  onCancel?: () => void
} & Omit<DialogProps, 'onSelect' | 'onClose'>

/**
 * A MUI dialog that allows to browse NOMAD entities and select a path to
 * a specific entity like a file, directory, entry, section, etc. Think
 * of it as the file picker dialog on your os.
 *
 * This component is ment to be composed in specialized components for
 * selecting specific entities.
 */
export function SelectDialog<S extends SelectOptions = SelectOptions>({
  selectOptions,
  initialPath,
  title,
  description,
  selectButtonLabel = 'Select',
  onSelect,
  onCancel,
  open,
  ...dialogProps
}: SelectDialogProps<S>) {
  const [selectedPath, setSelectedPath] = useState<string | undefined>(
    undefined,
  )
  const handleClose = useCallback(() => {
    onCancel?.()
  }, [onCancel])

  const handleSelect = useCallback(() => {
    assert(
      selectedPath !== undefined,
      'SelectedPath must be defined when select button is clicked.',
    )
    onSelect?.(selectedPath)
  }, [onSelect, selectedPath])

  // Force unmount the dialog when it is closed, Otherwise the
  // select state will be preserved when the dialog is reopened, what we
  // do not want.
  if (!open) {
    return null
  }

  return (
    <Dialog
      open={true}
      onClose={handleClose}
      fullWidth
      maxWidth='xl'
      {...dialogProps}
    >
      {title && <DialogTitle>{title}</DialogTitle>}
      {description && (
        <DialogContent>
          <DialogContentText>{description}</DialogContentText>
        </DialogContent>
      )}
      <DialogContent
        sx={(theme) => ({
          padding: 0,
          borderWidth: 1,
          borderStyle: 'solid',
          borderColor: theme.palette.divider,
          borderLeft: 'none',
          borderRight: 'none',
          backgroundColor: theme.palette.background.default,
        })}
      >
        <Select
          selectOptions={selectOptions}
          initialPath={initialPath}
          onChange={setSelectedPath}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} title='Cancel'>
          Cancel
        </Button>
        <Button
          onClick={handleSelect}
          disabled={selectedPath === undefined}
          title='Select'
        >
          {selectButtonLabel}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export type SelectProps<S extends SelectOptions = SelectOptions> = {
  initialPath: string
  onChange?: (selectedPath: string | undefined) => void
  selectOptions?: S
}

/**
 * A component that allows to browse NOMAD entities and select a path to
 * a specific entity like a file, directory, entry, section, etc.
 */
export default function Select<S extends SelectOptions = SelectOptions>({
  selectOptions = {} as S,
  initialPath,
  onChange,
}: SelectProps<S>) {
  const [path, setPath] = useState(initialPath)
  const selectContextValue = createSelectContextValue(selectOptions)
  const {entity} = selectOptions

  const handleNavigate = useCallback(
    ({location, fullMatch}: Pick<RouteData, 'location' | 'fullMatch'>) => {
      const leafMatch = fullMatch[fullMatch.length - 1]
      setPath(location)
      let matchesEntity = false
      if (entity === 'file') {
        matchesEntity = leafMatch.route.routeId === filesRoute.routeId
        matchesEntity &&= leafMatch.response?.m_is === 'File'
        const {filePattern} = selectOptions as unknown as FileSelectOptions
        if (filePattern) {
          const fileName = leafMatch.path
          matchesEntity &&= fileName.match(filePattern) !== null
        }
      } else if (entity === 'directory') {
        matchesEntity = leafMatch.route.routeId === filesRoute.routeId
        matchesEntity &&= leafMatch.response?.m_is === 'Directory'
      } else if (entity === 'section') {
        matchesEntity = leafMatch.route.routeId === archiveRoute.routeId
        matchesEntity &&= leafMatch.response?.m_def !== undefined
        const {sectionDefinition} =
          selectOptions as unknown as SectionSelectOptions
        if (sectionDefinition) {
          const m_def = leafMatch.response?.m_def as Section
          matchesEntity &&=
            m_def.m_ref === sectionDefinition ||
            m_def.all_base_sections.some((s) => s.m_ref === sectionDefinition)
        }
      }

      onChange?.(matchesEntity ? location : undefined)
    },
    [setPath, onChange, entity, selectOptions],
  )

  return (
    <selectContext.Provider value={selectContextValue}>
      <MemoryRouter
        route={rootRoute}
        loader={loader}
        url={path}
        onNavigate={handleNavigate}
      />
    </selectContext.Provider>
  )
}
