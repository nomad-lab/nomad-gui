import {render, screen, waitFor} from '@testing-library/react'
import {describe, expect, it, vi} from 'vitest'

import {createMatchMedia} from '../../utils/test.helper'
import MarkdownFileViewer from './MarkdownFileViewer'

vi.mock('./useFileContents', () => ({
  default: vi.fn().mockReturnValue('# Test Value'),
}))

describe('MarkdownFileViewer', () => {
  it('renders', async () => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    render(<MarkdownFileViewer file={'./sample'} />)
    await waitFor(() =>
      expect(
        screen.getByRole('heading', {name: /test value/i}),
      ).toBeInTheDocument(),
    )
  })
})
