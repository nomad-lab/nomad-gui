import {render, screen} from '@testing-library/react'
import {describe, expect, it, vi} from 'vitest'

import {createMatchMedia} from '../../utils/test.helper'
import JsonViewer from './JsonViewer'

describe('JsonValue', () => {
  it('renders', async () => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    render(<JsonViewer value={{testkey: 'testvalue'}} />)
    const keyElement = await screen.findByText(/testkey/i)
    const valueElement = await screen.findByText(/testvalue/i)
    expect(keyElement).toBeInTheDocument()
    expect(valueElement).toBeInTheDocument()
  })

  it('URL parsed correctly', async () => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    render(<JsonViewer value={{testkey: 'http://example.com'}} />)
    const link = await screen.findByRole('link')
    expect(link).toBeInTheDocument()
  })
})
