import {useTheme} from '@mui/material/styles'
import type {PDFDocumentProxy} from 'pdfjs-dist'
import {useLayoutEffect, useState} from 'react'
import {Document, Page, pdfjs} from 'react-pdf'
import 'react-pdf/dist/esm/Page/AnnotationLayer.css'
import 'react-pdf/dist/esm/Page/TextLayer.css'

pdfjs.GlobalWorkerOptions.workerSrc = new URL(
  'pdfjs-dist/build/pdf.worker.min.js',
  import.meta.url,
).toString()

interface PdfFileViewerProps {
  file: string | File | null
}

export default function PdfFileViewer({file}: PdfFileViewerProps) {
  const [containerRef, setContainerRef] = useState<HTMLElement | null>(null)
  const [numPages, setNumPages] = useState<number>()
  const [pageWidth, setPageWidth] = useState<number>()
  const theme = useTheme()
  const defaultMaxWidth = theme.breakpoints.values.md

  useLayoutEffect(() => {
    if (containerRef) {
      setPageWidth(containerRef.clientWidth)
    }
  }, [containerRef])

  function handleDocumentLoadSuccess({numPages}: PDFDocumentProxy) {
    setNumPages(numPages)
  }

  return (
    <div ref={setContainerRef}>
      <Document file={file} onLoadSuccess={handleDocumentLoadSuccess}>
        {numPages &&
          pageWidth &&
          Array.from(new Array(numPages), (_, index) => (
            <Page
              key={`page_${index + 1}`}
              pageNumber={index + 1}
              width={
                pageWidth
                  ? Math.min(pageWidth, defaultMaxWidth)
                  : defaultMaxWidth
              }
            />
          ))}
      </Document>
    </div>
  )
}
