import {render, screen} from '@testing-library/react'
import {describe, expect, it, vi} from 'vitest'

import {createMatchMedia} from '../../utils/test.helper'
import TextFileViewer from './TextFileViewer'

vi.mock('./useFileContents', () => ({
  default: vi.fn().mockReturnValue('testvalue'),
}))

describe('TextFileViewer', () => {
  it('renders', async () => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    render(<TextFileViewer file={'./sample'} />)
    const element = await screen.findByText(/testvalue/i)
    expect(element).toBeInTheDocument()
  })
})
