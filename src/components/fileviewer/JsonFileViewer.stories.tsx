import type {Meta, StoryObj} from '@storybook/react'

import JsonFileViewer from './JsonFileViewer'

const meta = {
  title: 'Components/fileviewer/JsonFileViewer',
  component: JsonFileViewer,
  tags: ['autodocs'],
  parameters: {
    controls: {expanded: false},
  },
} satisfies Meta<typeof JsonFileViewer>

export default meta
type Story = StoryObj<typeof meta>

export const Example: Story = {
  args: {
    file: './sample.json',
  },
}
