import {Box} from '@mui/material'

export type TextViewerProps = {
  text: string
}

export default function TextViewer({text}: TextViewerProps) {
  return (
    <Box
      sx={{
        width: '100%',
        overflowX: 'auto',
      }}
    >
      <Box sx={{width: 0}}>
        <pre>{text}</pre>
      </Box>
    </Box>
  )
}
