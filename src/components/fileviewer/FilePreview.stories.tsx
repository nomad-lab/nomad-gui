import type {Meta, StoryObj} from '@storybook/react'

import FilePreview from './FilePreview'

const meta = {
  title: 'Components/fileviewer/FilePreview',
  component: FilePreview,
  tags: ['autodocs'],
  parameters: {
    controls: {expanded: false},
  },
} satisfies Meta<typeof FilePreview>

export default meta
type Story = StoryObj<typeof meta>

const files = {
  pdf: './public/sample.pdf',
  json: './public/sample.json',
  markdown: './public/sample.md',
}
export const JSON: Story = {
  args: {
    file: files.json,
    size: 100,
  },
}
export const PDF: Story = {
  args: {
    file: files.pdf,
    size: 400,
  },
}
export const Markdown: Story = {
  args: {
    file: files.markdown,
    size: 400,
  },
}
