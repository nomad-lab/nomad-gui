import JsonFileViewer from './JsonFileViewer'
import MarkdownFileViewer from './MarkdownFileViewer'
import PdfFileViewer from './PdfFileViewer'
import TextFileViewer from './TextFileViewer'

interface FilePreviewProps {
  file: string
  size: number
}

export default function FilePreview({file, size}: FilePreviewProps) {
  const fileType = file.split('.').pop()
  if (fileType === 'pdf' && size < pdfViewerConfig.maxSizeAutoPreview) {
    return <PdfFileViewer file={file} {...pdfViewerConfig} />
  } else if (
    fileType === 'json' &&
    size < jsonViewerConfig.maxSizeAutoPreview
  ) {
    return <JsonFileViewer file={file} {...jsonViewerConfig} />
  } else if (fileType === 'md') {
    return <MarkdownFileViewer file={file} />
  } else {
    return <TextFileViewer file={file} />
  }
}

const jsonViewerConfig = {
  maxSizeAutoPreview: 10e6,
  width: 'fit-content',
}

const pdfViewerConfig = {
  maxSizeAutoPreview: 10e6,
  maxWidths: 400,
}
