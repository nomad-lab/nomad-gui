import type {Meta, StoryObj} from '@storybook/react'

import JsonViewer from './JsonViewer'

const meta = {
  title: 'Components/fileviewer/JsonViewer',
  component: JsonViewer,
  tags: ['autodocs'],
  parameters: {
    controls: {expanded: false},
  },
} satisfies Meta<typeof JsonViewer>

export default meta
type Story = StoryObj<typeof meta>

export const Example: Story = {
  args: {
    value: {
      foo: 'bar',
      baz: 123,
      qux: true,
      quux: null,
      corge: [1, 2, 3],
      grault: {garply: 'waldo'},
    },
  },
}
