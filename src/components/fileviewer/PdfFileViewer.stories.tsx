import type {Meta, StoryObj} from '@storybook/react'

import PdfFileViewer from './PdfFileViewer'

const meta = {
  title: 'Components/fileviewer/PDFViewer',
  component: PdfFileViewer,
  tags: ['autodocs'],
  parameters: {
    controls: {expanded: false},
  },
} satisfies Meta<typeof PdfFileViewer>

export default meta
type Story = StoryObj<typeof meta>
const file = '/static/sample.pdf'
export const Example: Story = {
  args: {
    file: file,
  },
}
