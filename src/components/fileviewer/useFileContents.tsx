import {useEffect, useState} from 'react'

import useAuth from '../../hooks/useAuth'

export default function useFileContents(file: string) {
  const {user} = useAuth()
  const [data, setData] = useState<string | null>(null)

  useEffect(() => {
    const controller = new AbortController()
    const {signal} = controller

    const headers: HeadersInit | undefined = user?.access_token
      ? {
          Authorization: `Bearer ${user.access_token}`,
        }
      : undefined

    const getData = async () => {
      try {
        const response = await fetch(file, {signal, headers})
        const responseData = await response.text()
        setData(responseData)
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      } catch (error: any) {
        if (error.name !== 'AbortError') {
          // eslint-disable-next-line no-console
          console.error(error)
        }
      }
    }

    getData()

    return () => {
      controller.abort()
    }
  }, [file, user])

  return data
}
