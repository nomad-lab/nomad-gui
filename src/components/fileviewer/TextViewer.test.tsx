import {render, screen} from '@testing-library/react'
import {describe, expect, it, vi} from 'vitest'

import {createMatchMedia} from '../../utils/test.helper'
import TextViewer from './TextViewer'

describe('TextValue', () => {
  it('renders', async () => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    render(<TextViewer text={'hello world'} />)
    const element = await screen.findByText(/hello world/)
    expect(element).toBeInTheDocument()
  })
})
