import {render, screen} from '@testing-library/react'
import {describe, expect, it, vi} from 'vitest'

import {createMatchMedia} from '../../utils/test.helper'
import FilePreview from './FilePreview'

vi.mock('./PdfFileViewer', () => ({
  default: () => {
    return <div>PDF viewer</div>
  },
}))

vi.mock('./TextFileViewer', () => ({
  default: () => {
    return <div>Text viewer</div>
  },
}))

vi.mock('./JsonFileViewer', () => ({
  default: () => {
    return <div>Json viewer</div>
  },
}))

vi.mock('./MarkdownFileViewer', () => ({
  default: () => {
    return <div>Markdown viewer</div>
  },
}))

describe('FilePreview', () => {
  it('renders PdfFileViewer for a small Pdf', async () => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    render(<FilePreview file={'./sample.pdf'} size={100} />)
    const element = await screen.findByText('PDF viewer')
    expect(element).toBeInTheDocument()
  })
  it('renders TextFileViewer for a large Pdf', async () => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    render(<FilePreview file={'./sample.pdf'} size={10e8} />)
    const element = await screen.findByText('Text viewer')
    expect(element).toBeInTheDocument()
  })
  it('renders JsonFileViewer for a small Json file', async () => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    render(<FilePreview file={'./sample.json'} size={100} />)
    const element = await screen.findByText('Json viewer')
    expect(element).toBeInTheDocument()
  })
  it('renders TextFileViewer for a large Json', async () => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    render(<FilePreview file={'./sample.json'} size={10e8} />)
    const element = await screen.findByText('Text viewer')
    expect(element).toBeInTheDocument()
  })
  it('renders MarkdownViewer for a markdown file', async () => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    render(<FilePreview file={'./sample.md'} size={100} />)
    const element = await screen.findByText('Markdown viewer')
    expect(element).toBeInTheDocument()
  })
  it('renders TextFileViewer for text files', async () => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    render(<FilePreview file={'./sample.text'} size={100} />)
    const element = await screen.findByText('Text viewer')
    expect(element).toBeInTheDocument()
  })
  it('renders TextFileViewer for unknown files', async () => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    render(<FilePreview file={'./sample.zip'} size={100} />)
    const element = await screen.findByText('Text viewer')
    expect(element).toBeInTheDocument()
  })
})
