import {renderHook, waitFor} from '@testing-library/react'
import {beforeEach, describe, expect, it, vi} from 'vitest'

import useFileContents from './useFileContents'

const mockFetch = vi.fn()
// Mock the global fetch API
global.fetch = mockFetch

describe('useFileContents', () => {
  beforeEach(() => {
    mockFetch.mockClear()
  })

  it('should return null initially', () => {
    mockFetch.mockResolvedValueOnce({
      text: async () => 'some content',
    })
    const {result} = renderHook(() => useFileContents('test.txt'))
    expect(result.current).toBeNull()
    waitFor(() => expect(result.current).toBe('some content'))
  })

  it('should fetch and return file contents', async () => {
    const fileContent = 'Hello, World!'
    const filename = 'test.txt'
    mockFetch.mockResolvedValueOnce({
      text: async () => fileContent,
    })

    const {result} = renderHook(() => useFileContents(filename))

    await waitFor(() => expect(result.current).toBe(fileContent))
    expect(mockFetch).toHaveBeenCalledWith(filename, expect.any(Object))
  })

  it('should handle fetch errors', async () => {
    // eslint-disable-next-line no-console
    console.error = vi.fn() // Mock console.error to suppress error messages in test output
    mockFetch.mockRejectedValueOnce(new Error('Failed to fetch'))

    const {result} = renderHook(() => useFileContents('test.txt'))

    await waitFor(() =>
      expect(mockFetch).toHaveBeenCalledWith('test.txt', expect.any(Object)),
    )
    expect(result.current).toBeNull()
    // eslint-disable-next-line no-console
    expect(console.error).toHaveBeenCalledWith(expect.any(Error))
  })

  it('should abort fetch when component unmounts', async () => {
    const abortSpy = vi.spyOn(AbortController.prototype, 'abort')
    mockFetch.mockImplementation(
      () =>
        new Promise((resolve) => {
          setTimeout(() => resolve({text: async () => 'Hello, World!'}), 1000)
        }),
    )

    const {result, unmount} = renderHook(() => useFileContents('test.txt'))

    unmount()

    expect(abortSpy).toHaveBeenCalled()
    expect(result.current).toBeNull()
  })
})
