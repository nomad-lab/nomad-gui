import {Link, useTheme} from '@mui/material'
import {
  JsonViewerProps,
  JsonViewer as TexteaJsonViewer,
} from '@textea/json-viewer'
import {defineDataType} from '@textea/json-viewer'

function isValidURL(value: string) {
  try {
    new URL(value)
    return true
  } catch {
    return false
  }
}

const urlType = defineDataType<URL | string>({
  is: (value) =>
    value instanceof URL || (typeof value === 'string' && isValidURL(value)),
  Component: (props) => {
    const url = props.value.toString()
    return (
      <Link href={url} target='_blank' rel='noopener noreferrer'>
        {url}
      </Link>
    )
  },
})

export default function JsonViewer(props: JsonViewerProps) {
  const {valueTypes, ...otherProps} = props
  const theme = useTheme()
  return (
    <TexteaJsonViewer
      rootName={false}
      {...otherProps}
      theme={theme.palette.mode}
      sx={{background: 'none'}}
      valueTypes={[urlType, ...(valueTypes ?? [])]}
    />
  )
}
