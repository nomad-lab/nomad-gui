import TextViewer, {TextViewerProps} from './TextViewer'
import useFileContents from './useFileContents'

type TextFileViewerProps = Omit<TextViewerProps, 'text'> & {
  file: string
}

export default function TextFileViewer({file}: TextFileViewerProps) {
  const text = useFileContents(file)
  return <TextViewer text={text ?? ''} />
}
