import {render, screen} from '@testing-library/react'
import {describe, expect, it, vi} from 'vitest'

import {createMatchMedia} from '../../utils/test.helper'
import JsonFileViewer from './JsonFileViewer'

vi.mock('./useFileContents', () => ({
  default: vi.fn().mockReturnValue(JSON.stringify({testkey: 'testvalue'})),
}))

describe('JsonFileViewer', () => {
  it('renders', async () => {
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    render(<JsonFileViewer file={'./sample'} />)
    const keyElement = await screen.findByText(/testkey/i)
    const valueElement = await screen.findByText(/testvalue/i)
    expect(keyElement).toBeInTheDocument()
    expect(valueElement).toBeInTheDocument()
  })
})
