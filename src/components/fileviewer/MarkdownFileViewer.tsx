import Markdown, {MarkdownProps} from '../markdown/Markdown'
import useFileContents from './useFileContents'

type MarkdownFileViewerProps = Omit<MarkdownProps, 'content'> & {
  file: string
}

export default function MarkdownFileViewer({file}: MarkdownFileViewerProps) {
  const text = useFileContents(file)
  return <Markdown content={text || ''} />
}
