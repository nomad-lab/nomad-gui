import {JsonViewerProps} from '@textea/json-viewer'

import JsonViewer from './JsonViewer'
import useFileContents from './useFileContents'

type JsonFileViewerProps = Omit<JsonViewerProps, 'value'> & {
  file: string
}
export default function JsonFileViewer({file, ...props}: JsonFileViewerProps) {
  const data = useFileContents(file)
  const json = data && JSON.parse(data)
  return <JsonViewer {...props} value={json} />
}
