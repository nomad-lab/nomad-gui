import {UploadResponse} from '../models/graphResponseModels'
import Status, {StatusProps} from './Status'
import {Private, Public, Shared} from './icons'

export type VisibilityProps = {
  upload: Pick<
    UploadResponse,
    'published' | 'with_embargo' | 'writers' | 'viewers'
  >
  variant?: 'default' | 'icon'
}

export default function Visibility({
  upload,
  variant = 'default',
}: VisibilityProps) {
  const isPublic = upload.published && !upload.with_embargo
  const isShared =
    upload?.writers?.length || 0 > 0 || upload?.viewers?.length || 0 > 0

  const statusProps = {variant} as StatusProps
  if (isPublic) {
    statusProps.label = 'published'
    statusProps.icon = <Public />
    statusProps.color = 'primary'
  } else if (isShared) {
    statusProps.label = 'shared'
    statusProps.icon = <Shared />
    statusProps.color = 'default'
  } else {
    statusProps.label = 'private'
    statusProps.icon = <Private />
    statusProps.color = 'default'
  }

  return <Status {...statusProps} />
}
