import {useCallback, useMemo, useState} from 'react'

import ControlledDataTable, {
  BaseTableData,
  Pagination,
  TableProps,
} from './ControlledDataTable'

export type UncontrolledTableProps<TData extends BaseTableData> = Omit<
  TableProps<TData>,
  'paginationType'
>

export default function PlainDataTable<TData extends BaseTableData>({
  data,
  ...tableProps
}: UncontrolledTableProps<TData>) {
  const [pagination, setPagination] = useState({
    page_size: data.length,
    total: data.length,
    page: 1,
  } as Pagination)

  const handlePaginationChange = useCallback(
    (update: Partial<Pagination>) => {
      setPagination((prev) => ({...prev, ...update}))
    },
    [setPagination],
  )

  const sortedData = useMemo(() => {
    const orderBy = pagination.order_by
    if (orderBy === undefined) {
      return data
    }

    return data.toSorted((a, b) => {
      const aValue = a[orderBy]
      const bValue = b[orderBy]
      const order = pagination.order === 'asc' ? -1 : 1
      return aValue.toString().localeCompare(bValue.toString()) * order
    })
  }, [pagination.order_by, pagination.order, data])

  return (
    <ControlledDataTable
      paginationType='none'
      data={sortedData}
      pagination={pagination}
      onPaginationChange={handlePaginationChange}
      {...tableProps}
    />
  )
}
