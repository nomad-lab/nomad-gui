import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import {MenuItem} from '@mui/material'
import type {Meta, StoryObj} from '@storybook/react'

import PlainDataTable from './PlainDataTable'

const meta = {
  title: 'components/table/PlainDataTable',
  component: PlainDataTable,
  tags: ['autodocs'],
} satisfies Meta<typeof PlainDataTable>

export default meta

type Story = StoryObj<typeof meta>

const columns = [
  {
    header: 'Column 1',
    accessorKey: 'column1',
    grow: 1,
  },
  {
    header: 'Column 2',
    accessorKey: 'column2',
  },
]

const data = [
  {column1: 'row1', column2: 'row1'},
  {column1: 'row2', column2: 'row2'},
  {column1: 'row3', column2: 'row3'},
]

export const BasicTable: Story = {
  args: {
    columns: columns,
    data: data,
  },
}

export const TableWithRowActions: Story = {
  args: {
    columns: columns,
    data: data,
    rowContextMenuItems: (row) => [
      <MenuItem key='edit' onClick={() => confirm(`${row.id} Edit`)}>
        <EditIcon sx={{mr: 1}} />
        Edit
      </MenuItem>,
      <MenuItem key='delete' onClick={() => confirm('Delete')}>
        <DeleteIcon sx={{mr: 1}} />
        Delete
      </MenuItem>,
    ],
  },
}
