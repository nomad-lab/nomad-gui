import {act, fireEvent, render, screen} from '@testing-library/react'
import {describe, expect, it, vi} from 'vitest'

import {ColumnDef} from './ControlledDataTable'
import RoutingTable from './RoutingTable'

const setPaginate = vi.fn()
vi.mock('../routing/usePagination', () => ({
  default: () => [
    {
      page: 1,
      per_page: 5,
      page_size: 50,
      total: 100,
    },
    setPaginate,
  ],
}))

type Data = {
  upload_id: string
  upload_name: string
}

const mockData: Data[] = Array.from({length: 12}, (_, index) => ({
  upload_id: `${index + 1}`,
  upload_name: `Upload ${index + 1}`,
}))

const columns: ColumnDef<Data>[] = [
  {
    accessorKey: 'upload_id',
    header: 'ID',
    size: 150,
  },
  {
    accessorKey: 'upload_name',
    header: 'Name',
    size: 150,
  },
]

afterEach(() => {
  vi.restoreAllMocks()
})

describe('RoutingTable Component', () => {
  it('should render the table with data', () => {
    render(
      <RoutingTable
        columns={columns}
        paginationType='page'
        data={mockData.slice(0, 5)}
      />,
    )

    const table = screen.getByRole('table')
    expect(table).toBeInTheDocument()
  })
  it('next page should trigger url change ', async () => {
    render(
      <RoutingTable
        columns={columns}
        paginationType='page'
        data={mockData.slice(0, 5)}
      />,
    )

    const table = screen.getByRole('table')
    expect(table).toBeInTheDocument()

    const nextPageButton = screen.getByText('Load More')
    expect(nextPageButton).toBeInTheDocument()
    act(() => fireEvent.click(nextPageButton))
    expect(setPaginate).toBeCalledWith({page_size: 100})
  })
  it('sorting change should trigger navigation', () => {
    render(
      <RoutingTable
        columns={columns}
        paginationType='page'
        data={mockData.slice(0, 5)}
      />,
    )

    const table = screen.getByRole('table')
    expect(table).toBeInTheDocument()

    const elements = screen.getAllByRole('button', {
      name: 'Sort by ID ascending',
    })

    act(() => fireEvent.click(elements[0]))
    expect(setPaginate).toHaveBeenCalledWith({
      order_by: 'upload_id',
      order: 'asc',
    })
  })
  it('pagination should be disabled', () => {
    render(
      <RoutingTable
        columns={columns}
        paginationType='none'
        data={mockData.slice(0, 5)}
      />,
    )

    const table = screen.getByRole('table')
    expect(table).toBeInTheDocument()
    const nextPageElement = screen.queryByTitle('Go to next page')
    expect(nextPageElement).toBeNull()
    const rowsPerPageElement = screen.queryByText('Rows per page')
    expect(rowsPerPageElement).toBeNull()
  })
})
