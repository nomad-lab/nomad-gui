import usePagination from '../routing/usePagination'
import ControlledDataTable, {
  BaseTableData,
  TableProps,
} from './ControlledDataTable'

export type RoutingTableProps<TData extends BaseTableData> = TableProps<TData>

function RoutingTable<TData extends BaseTableData>(
  props: RoutingTableProps<TData>,
) {
  const [pagination, setPagination] = usePagination()
  return (
    <ControlledDataTable
      {...props}
      pagination={pagination}
      onPaginationChange={setPagination}
    />
  )
}

export default RoutingTable
