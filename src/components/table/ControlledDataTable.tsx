import MoreVertIcon from '@mui/icons-material/MoreVert'
import {Box, Button, TableCellProps, Typography, useTheme} from '@mui/material'
import {SortingState} from '@tanstack/react-table'
import {
  type MRT_ColumnDef,
  MRT_GlobalFilterTextField,
  MRT_LinearProgressBar,
  MRT_ShowHideColumnsButton,
  MRT_TableInstance,
  MRT_TablePagination,
  MRT_ToggleFullScreenButton,
  MRT_ToolbarAlertBanner,
  MRT_ToolbarDropZone,
  MRT_ToolbarInternalButtons,
  MRT_Updater,
  MaterialReactTable,
  useMaterialReactTable,
} from 'material-react-table'
import {ReactNode, useCallback, useMemo} from 'react'

import {ENV} from '../../utils/env'
import {parseFromValuesOrFunc} from '../../utils/utils'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function format<TData extends Record<string, any>>({
  grow = false,
  size = undefined,
  center = false,
  ...column
}: ColumnDef<TData>): MRT_ColumnDef<TData> {
  const sx: React.CSSProperties = {
    minWidth: 'initial',
    flex: `${typeof grow === 'boolean' ? (grow ? 1 : 0) : grow} 1 auto`,
  }

  if (size) {
    sx[grow ? 'minWidth' : 'width'] = size
  }

  const cellProps: TableCellProps = {
    sx,
    align: center ? 'center' : 'left',
  }

  return {
    muiTableHeadCellProps: cellProps,
    muiTableBodyCellProps: cellProps,
    ...column,
  }
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type BaseTableData = Record<string, any>

export type ColumnDef<TData extends BaseTableData> = MRT_ColumnDef<TData> & {
  grow?: boolean | number
  center?: boolean
  size?: number
}

export type TableProps<TData extends BaseTableData> = {
  columns: ColumnDef<TData>[]
  data: TData[]
  // TODO: implement load-more and inifinite pagination
  paginationType: 'none' | 'page'
  /** Called when a row is clicked. */
  onRowClick?: (data: TData) => void
  /**
   * If defined, rows are disabled if the function returns true. Disabled
   * rows cannot be clicked and their context menu is disabled.
   */
  isDisabledRow?: (data: TData) => boolean
  /** The items for the row's context menu. */
  rowContextMenuItems?: (data: TData) => ReactNode[]
  /** Additional row actions, rendered in front of the rows context menu button. */
  rowActions?: (data: TData) => ReactNode
  /** General actions for the whole table rendered on the left of the top toolbar. */
  toolbarCustomActions?: (table: MRT_TableInstance<TData>) => ReactNode
  /** General actions for the whole table rendered on the right of the top toolbar. */
  toolbarActions?: (table: MRT_TableInstance<TData>) => ReactNode
  /** Actions rendered in the top toolbar if one or many rows of the table are selected. */
  toolbarSelectActions?: (table: MRT_TableInstance<TData>) => ReactNode
  /** The label for the table's data. */
  entityLabel?: string
  /** Row id getter, if unset the index is used. */
  getRowId?: (data: TData) => string
}

export type Pagination = {
  order_by?: string
  order?: 'asc' | 'desc'
  page_size: number
  page: number
  total: number
}

export type DataTableProps<TData extends BaseTableData> = TableProps<TData> & {
  pagination: Pagination
  onPaginationChange?: (update: Partial<Pagination>) => void
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default function ControlledDataTable<TData extends Record<string, any>>({
  columns,
  data,
  rowContextMenuItems,
  onRowClick,
  entityLabel,
  getRowId,
  isDisabledRow = () => false,
  onPaginationChange,
  pagination,
  toolbarCustomActions,
  toolbarSelectActions,
  toolbarActions,
  paginationType,
}: DataTableProps<TData>) {
  const theme = useTheme()
  const mrtColumns = useMemo(() => columns.map(format), [columns])
  const state = {
    sorting: pagination.order_by
      ? [{id: pagination.order_by, desc: pagination.order === 'desc'}]
      : [],
  }

  const handlePaginationChange = useCallback(() => {
    onPaginationChange?.({
      page_size: pagination.page_size + ENV.LOAD_MORE_PAGE_SIZE,
    })
  }, [pagination, onPaginationChange])

  const handleSortingChange = useCallback(
    (updater: MRT_Updater<SortingState>) => {
      const newSorting =
        updater instanceof Function ? updater(state.sorting) : updater
      onPaginationChange?.({
        order_by: newSorting[0]?.id,
        order:
          newSorting[0]?.desc === undefined
            ? undefined
            : newSorting[0].desc
            ? 'desc'
            : 'asc',
      })
    },
    [onPaginationChange, state.sorting],
  )

  const renderBottomToolbar = useCallback(() => {
    return (
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          width: '100%',
          justifyContent: 'space-between',
          padding: theme.spacing(1),
        }}
      >
        <Box sx={{display: 'flex', alignItems: 'center'}}>
          <Typography>
            {Math.min(pagination.page_size, pagination.total)} of{' '}
            {pagination.total}
          </Typography>
        </Box>
        <Button
          onClick={handlePaginationChange}
          disabled={pagination.total <= pagination.page_size}
          variant='contained'
        >
          Load More
        </Button>
      </Box>
    )
  }, [pagination, handlePaginationChange, theme])

  const table = useMaterialReactTable({
    columns: mrtColumns,
    data,
    positionToolbarAlertBanner: 'top',
    layoutMode: 'grid',
    enableColumnFilters: false,
    enableRowSelection: !!toolbarSelectActions,
    enableRowActions: !!rowContextMenuItems,
    enableGlobalFilter: false,
    enableColumnActions: false,
    manualPagination: true,
    enablePagination: false,
    manualSorting: true,
    positionActionsColumn: 'last',
    enableStickyHeader: true,
    selectAllMode: 'page',
    paginationDisplayMode: 'pages',
    onSortingChange: handleSortingChange,
    rowCount: pagination.total,
    state: state,
    initialState: {density: 'comfortable'},
    muiToolbarAlertBannerProps: {
      sx: {
        color: 'background.paper',
        bgcolor: 'primary.main',
        border: 'none',
      },
    },
    getRowId: getRowId,
    localization: {
      selectedCountOfRowCountRowsSelected: `{selectedCount} of {rowCount} ${
        entityLabel ?? 'row'
      }(s) selected`,
    },
    displayColumnDefOptions: {
      // don't display actions column header
      'mrt-row-actions': {
        header: '',
        size: 0,
      },
      'mrt-row-select': {
        size: 0,
      },
    },
    icons: {
      // override the horiz icon to a vert icon
      MoreHorizIcon: () => <MoreVertIcon />,
    },
    muiTablePaperProps: {
      sx: {
        backgroundColor: theme.palette.background.paper,
        '& .MuiTableContainer-root': {
          maxHeight: 'initial',
        },
        '& .MuiTableRow-root': {
          backgroundColor: `${theme.palette.background.paper} !important`,
        },
      },
    },
    renderTopToolbar: ({table}) => {
      // We needed to get arround the "isStacked" limitation and this is more or
      // less a copy&page from
      // https://github.com/KevinVandy/material-react-table/blob/v2/packages/material-react-table/src/toolbar/MRT_TopToolbar.tsx
      const {
        getState,
        options: {
          enableGlobalFilter,
          enablePagination,
          enableToolbarInternalActions,
          muiTopToolbarProps,
          positionGlobalFilter,
          positionPagination,
          positionToolbarAlertBanner,
          positionToolbarDropZone,
          renderTopToolbarCustomActions,
        },
        refs: {topToolbarRef},
      } = table

      const {isFullScreen} = getState()
      const toolbarProps = parseFromValuesOrFunc(muiTopToolbarProps, {table})

      return (
        <Box
          {...toolbarProps}
          ref={(ref: HTMLDivElement) => {
            topToolbarRef.current = ref
            if (toolbarProps?.ref) {
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              ;(toolbarProps.ref as any).current = ref
            }
          }}
          sx={(theme) => ({
            alignItems: 'flex-start',
            // backgroundColor: getMRTTheme(table, theme).baseBackgroundColor,
            display: 'grid',
            flexWrap: 'wrap-reverse',
            minHeight: '3.5rem',
            overflow: 'hidden',
            transition: 'all 150ms ease-in-out',
            zIndex: 1,
            position: isFullScreen ? 'sticky' : 'relative',
            top: isFullScreen ? '0' : undefined,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            ...(parseFromValuesOrFunc(toolbarProps?.sx, theme) as any),
          })}
        >
          {positionToolbarAlertBanner === 'top' && (
            <MRT_ToolbarAlertBanner table={table} />
          )}
          {['both', 'top'].includes(positionToolbarDropZone ?? '') && (
            <MRT_ToolbarDropZone table={table} />
          )}
          <Box
            sx={{
              alignItems: 'flex-start',
              boxSizing: 'border-box',
              display: 'flex',
              justifyContent: 'space-between',
              p: '0.5rem',
              position: 'absolute',
              right: 0,
              top: 0,
              width: '100%',
            }}
          >
            {enableGlobalFilter && positionGlobalFilter === 'left' && (
              <MRT_GlobalFilterTextField table={table} />
            )}
            {renderTopToolbarCustomActions?.({table})}
            {<span />}
            {enableToolbarInternalActions ? (
              <Box
                sx={{
                  alignItems: 'center',
                  display: 'flex',
                  flexWrap: 'wrap-reverse',
                  justifyContent: 'flex-end',
                }}
              >
                {enableGlobalFilter && positionGlobalFilter === 'right' && (
                  <MRT_GlobalFilterTextField table={table} />
                )}
                <MRT_ToolbarInternalButtons
                  table={table}
                  sx={{width: '100%'}}
                />
              </Box>
            ) : (
              enableGlobalFilter &&
              positionGlobalFilter === 'right' && (
                <MRT_GlobalFilterTextField table={table} />
              )
            )}
          </Box>
          {enablePagination &&
            ['both', 'top'].includes(positionPagination ?? '') && (
              <MRT_TablePagination position='top' table={table} />
            )}
          <MRT_LinearProgressBar isTopToolbar table={table} />
        </Box>
      )
    },
    renderTopToolbarCustomActions: ({table}) => {
      return toolbarCustomActions?.(table)
    },
    renderBottomToolbar: paginationType === 'page' && renderBottomToolbar,
    renderToolbarInternalActions: ({table}) => {
      const isRowSelected =
        table.getIsAllRowsSelected() || table.getIsSomeRowsSelected()
      const color = isRowSelected ? 'background.paper' : 'default'

      return (
        <Box
          sx={{
            display: 'flex',
            color: color,
          }}
        >
          {isRowSelected ? (
            toolbarSelectActions?.(table)
          ) : (
            <>
              {toolbarActions?.(table)}
              <MRT_ShowHideColumnsButton sx={{color: color}} table={table} />
              <MRT_ToggleFullScreenButton sx={{color: color}} table={table} />
            </>
          )}
        </Box>
      )
    },
    muiTopToolbarProps: {
      sx: {
        borderTopRightRadius: 8,
        borderTopLeftRadius: 8,
      },
    },
    muiBottomToolbarProps: {
      sx: {
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        boxShadow: 0,
      },
    },
    renderRowActionMenuItems:
      rowContextMenuItems &&
      ((props) => rowContextMenuItems(props.row.original)),
    muiTableBodyCellProps: {
      sx: {
        flex: '0 1 auto',
        width: 64,
        minWidth: 'initial',
        maxWidth: 'initial',
        '&>.MuiIconButton-root': {
          opacity: 1,
          transition: 'initial',
        },
      },
    },
    muiTableHeadCellProps: {
      sx: {
        flex: '0 1 auto',
        width: 64,
        minWidth: 'initial',
        maxWidth: 'initial',
      },
    },
    muiTableBodyRowProps: ({row}) => ({
      onClick:
        onRowClick && !isDisabledRow(row.original)
          ? () => onRowClick(row.original)
          : undefined,
      sx: {
        '&>.MuiTableCell-root': {
          color: (theme) =>
            isDisabledRow(row.original)
              ? theme.palette.action.disabled
              : 'inherit',
        },
        cursor:
          onRowClick && !isDisabledRow(row.original) ? 'pointer' : 'default',
      },
    }),
    muiTableHeadRowProps: {
      sx: {
        boxShadow: 'inherit',
      },
    },
  })

  return <MaterialReactTable table={table} />
}
