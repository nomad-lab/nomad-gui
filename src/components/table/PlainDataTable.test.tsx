import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import PlainDataTable from './PlainDataTable'

describe('PlainDataTable', () => {
  const data = [{name: 'B'}, {name: 'A'}]
  const columns = [
    {
      enableSorting: true,
      accessorKey: 'name',
      header: 'Name',
      grow: 1,
    },
  ]
  it('renders the table', () => {
    render(<PlainDataTable data={data} columns={columns} />)
    expect(screen.getByText('A')).toBeInTheDocument()
    expect(screen.getByText('B')).toBeInTheDocument()
  })
  it('sorts', async () => {
    render(<PlainDataTable data={data} columns={columns} />)
    await userEvent.click(screen.getByText('Name'))
    expect(
      screen.getByText('A').compareDocumentPosition(screen.getByText('B')),
    ).toBe(2) // 2 means the second before the first
    await userEvent.click(screen.getByText('Name'))
    expect(
      screen.getByText('A').compareDocumentPosition(screen.getByText('B')),
    ).toBe(4) // 4 means the first before the second
  })
})
