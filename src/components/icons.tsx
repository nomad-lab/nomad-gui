import AccountCircleIcon from '@mui/icons-material/AccountCircle'
import AddIcon from '@mui/icons-material/Add'
import OpenIcon from '@mui/icons-material/ArrowForward'
import AutorenewIcon from '@mui/icons-material/Autorenew'
import Brightness4Icon from '@mui/icons-material/Brightness4'
import Brightness7Icon from '@mui/icons-material/Brightness7'
import CancelIcon from '@mui/icons-material/Cancel'
import CheckIcon from '@mui/icons-material/Check'
import ClearIcon from '@mui/icons-material/Clear'
import CodeIcon from '@mui/icons-material/Code'
import ContentPaste from '@mui/icons-material/ContentPaste'
import DatasetIcon from '@mui/icons-material/Dataset'
import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import GroupIcon from '@mui/icons-material/Group'
import HelpIcon from '@mui/icons-material/Help'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'
import LinkIcon from '@mui/icons-material/Link'
import LinkOffIcon from '@mui/icons-material/LinkOff'
import LockIcon from '@mui/icons-material/Lock'
import LockOpenIcon from '@mui/icons-material/LockOpen'
import LoginIcon from '@mui/icons-material/Login'
import LogoutIcon from '@mui/icons-material/Logout'
import MenuIcon from '@mui/icons-material/Menu'
import OpenInNewIcon from '@mui/icons-material/OpenInNew'
import PieChartIcon from '@mui/icons-material/PieChart'
import PublicIcon from '@mui/icons-material/Public'
import PushPinIcon from '@mui/icons-material/PushPin'
import QuestionMarkIcon from '@mui/icons-material/QuestionMark'
import SearchIcon from '@mui/icons-material/Search'
import UploadIcon from '@mui/icons-material/Upload'
import VisibilityIcon from '@mui/icons-material/Visibility'
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff'
import {useTheme} from '@mui/material'

export const ArrowUp = KeyboardArrowUpIcon
export const Close = ClearIcon
export const Remove = DeleteIcon
export const ExpandMore = ExpandMoreIcon
export const Menu = MenuIcon
export const Manage = UploadIcon
export const Search = SearchIcon
export const Explore = SearchIcon
export const Analyze = PieChartIcon
export const Logout = LogoutIcon
export const Login = LoginIcon
export const Profile = AccountCircleIcon
export const Dev = CodeIcon
export const Public = PublicIcon
export const Shared = VisibilityIcon
export const Private = VisibilityOffIcon
export const Success = CheckIcon
export const Processing = AutorenewIcon
export const Failed = CancelIcon
export const Unknown = QuestionMarkIcon
export const Add = AddIcon
export const Clipboard = ContentPaste
export const OpenInNew = OpenInNewIcon
export const Open = OpenIcon
export const Locked = LockIcon
export const Unlocked = LockOpenIcon
export const Linked = LinkIcon
export const Unlinked = LinkOffIcon
export const Edit = EditIcon
export const View = VisibilityIcon
export const Help = HelpIcon
export const Dataset = DatasetIcon
export const Pin = PushPinIcon
export const Group = GroupIcon

export function ToggleColorMode() {
  const theme = useTheme()
  return theme.palette.mode === 'dark' ? (
    <Brightness7Icon />
  ) : (
    <Brightness4Icon />
  )
}
