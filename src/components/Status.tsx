import {Avatar, Chip, Tooltip} from '@mui/material'
import React from 'react'

export type StatusProps = {
  label: string
  icon: React.ReactElement
  color?:
    | 'default'
    | 'primary'
    | 'secondary'
    | 'error'
    | 'info'
    | 'success'
    | 'warning'
  variant?: 'default' | 'icon'
}

export default function Status({
  label,
  icon,
  color = 'default',
  variant = 'default',
}: StatusProps) {
  if (variant === 'icon') {
    return (
      <Tooltip title={label}>
        <Avatar
          color={color}
          sx={{
            width: 24,
            height: 24,
            backgroundColor: `${color}.main`,
            color: `${color}.contrastText`,
            '& .MuiSvgIcon-root': {fontSize: '18px'},
          }}
        >
          {icon}
        </Avatar>
      </Tooltip>
    )
  } else {
    return <Chip size='small' label={label} color={color} icon={icon} />
  }
}
