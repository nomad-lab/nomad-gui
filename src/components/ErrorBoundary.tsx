import {Card} from '@mui/material'
import {PropsWithChildren} from 'react'
import {ErrorBoundary as ReactErrorBoundary} from 'react-error-boundary'

function DefaultErrorMessage({
  error,
  message,
}: {
  error: Error
  message?: string
}) {
  return (
    <div>
      {message && <span>{message}: </span>}
      <span>{error.message}</span>
    </div>
  )
}

export type ErrorBoundaryProps = {
  message?: string
  renderError?: (error: Error) => React.ReactNode
} & PropsWithChildren

export default function ErrorBoundary(props: ErrorBoundaryProps) {
  const {
    renderError = (error) => (
      <DefaultErrorMessage error={error} message={props.message} />
    ),
    children,
  } = props
  return (
    <ReactErrorBoundary
      fallbackRender={({error, resetErrorBoundary}) => (
        <Card
          onClick={() => resetErrorBoundary()}
          sx={{
            borderColor: (theme) => theme.palette.error.main,
            padding: 1,
            overflow: 'auto',
          }}
        >
          {renderError(error)}
        </Card>
      )}
    >
      {children}
    </ReactErrorBoundary>
  )
}
