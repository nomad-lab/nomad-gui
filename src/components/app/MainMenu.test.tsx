import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import React from 'react'
import {vi} from 'vitest'
import {LinkProps} from 'wouter'

import MainMenu from './MainMenu'

vi.mock('../routing/useRoute', () => ({
  default: () => ({
    matches: () => false,
  }),
}))

vi.mock('../routing/Link', () => ({
  default: React.forwardRef<HTMLAnchorElement, LinkProps>(function Link(
    {children},
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    ref,
  ) {
    return children
  }),
}))

describe('MainMenu', () => {
  it('renders', () => {
    render(<MainMenu />)
    expect(screen.getByText('Home')).not.toBeVisible()
  })

  it('opens the drawer', async () => {
    render(<MainMenu />)
    await userEvent.click(screen.getByRole('button', {name: 'Collapse'}))
    expect(screen.getByText('Home')).toBeVisible()
  })
})
