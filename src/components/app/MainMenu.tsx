import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import {List} from '@mui/material'
import Box from '@mui/material/Box'
import Divider from '@mui/material/Divider'
import Drawer from '@mui/material/Drawer'
import ListItem from '@mui/material/ListItem'
import ListItemButton, {ListItemButtonProps} from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import {CSSObject, Theme, styled} from '@mui/material/styles'
import * as React from 'react'

import {SxProps} from '../../utils/types'
import useDevTools from '../devTools/useDevTools'
import {
  Analyze,
  Dev,
  Explore,
  Group,
  Manage,
  Menu,
  ToggleColorMode,
} from '../icons'
import Link from '../routing/Link'
import useRoute from '../routing/useRoute'
import {Logo} from '../theme/Theme'
import useThemeSettings from '../theme/useThemeSettings'
import {LoginMainMenuItem} from './auth'

const mainMenuWidth = 160

const mixin = (theme: Theme, open?: boolean): CSSObject => ({
  background: theme.palette.primary.dark,
  border: 'none',
  overflowX: 'hidden',
  width: open ? mainMenuWidth : theme.spacing(8),
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: open
      ? theme.transitions.duration.enteringScreen
      : theme.transitions.duration.leavingScreen,
  }),
})

const MainMenuDrawer = styled(Drawer, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({theme, open}) => ({
  zIndex: 0,
  width: mainMenuWidth,
  flexShrink: 0,
  whiteSpace: 'nowrap',
  boxSizing: 'border-box',
  ...mixin(theme, open),
  '& .MuiDrawer-paper': mixin(theme, open),
  '& .MuiDivider-root': {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    borderColor: theme.palette.primary.contrastText,
  },
}))

export type MainMenuItemProps = React.PropsWithChildren<{
  /**
   * Label shown in expanded menu
   */
  label: string
  /**
   * Whether the menu is expanded
   */
  open?: boolean
  /**
   * Icon to show in collapsed menu, only shown if there are no
   * children.
   */
  icon?: React.ReactNode
  /**
   * Url to navigate to on click, might only make sense with `Link` component.
   */
  to?: string
  /**
   * Whether the item is active and should be highlighted
   */
  active?: boolean
}> &
  ListItemButtonProps &
  SxProps

export const MainMenuItem = React.forwardRef<HTMLElement, MainMenuItemProps>(
  function MainMenuItem(
    {
      label,
      open,
      icon,
      active = false,
      children,
      sx = {},
      ...listItemButtonProps
    },
    ref,
  ) {
    return (
      <ListItem
        ref={ref as React.Ref<HTMLLIElement>}
        key={label}
        disablePadding
        sx={{
          display: 'block',
          '& .Mui-selected': {
            backgroundColor: 'rgba(255,255,255,0.2) !important',
          },
          '& .MuiListItemButton-root:hover': {
            backgroundColor: 'rgba(255,255,255,0.12) !important',
          },
        }}
      >
        <ListItemButton
          selected={active}
          sx={{
            minHeight: 48,
            justifyContent: open ? 'initial' : 'center',
            px: 2.5,
            '& .MuiListItemIcon-root': {
              color: 'primary.contrastText',
            },
            '& .MuiListItemText-root': {
              color: 'primary.contrastText',
            },
            ...sx,
          }}
          {...listItemButtonProps}
        >
          {children && (
            <Box
              sx={{
                minWidth: 0,
                mr: open ? 2 : 'auto',
                justifyContent: 'center',
              }}
            >
              {children}
            </Box>
          )}
          {icon && (
            <ListItemIcon
              sx={{
                minWidth: 0,
                mr: open ? 3 : 'auto',
                justifyContent: 'center',
              }}
            >
              {icon}
            </ListItemIcon>
          )}
          <ListItemText primary={label} sx={{opacity: open ? 1 : 0}} />
        </ListItemButton>
      </ListItem>
    )
  },
)

export default function MainMenu() {
  const {matches} = useRoute()
  const {handleToggleColorMode, colorMode} = useThemeSettings()
  const {handleOpenClose: handleDevToolsOpenClose, open: devToolsOpen} =
    useDevTools()
  const [open, setOpen] = React.useState(false)

  const handleDrawerChange = () => {
    setOpen((prev) => !prev)
  }

  return (
    <MainMenuDrawer variant='permanent' open={open}>
      <List>
        <MainMenuItem
          label='Home'
          open={open}
          component={Link}
          to='/'
          sx={{px: 1.5}}
        >
          <Logo />
        </MainMenuItem>
      </List>
      <Divider />
      <List>
        <MainMenuItem
          label='Collapse'
          open={open}
          icon={open ? <ChevronLeftIcon /> : <Menu />}
          onClick={handleDrawerChange}
        />
      </List>
      <Divider />
      <List sx={{flexGrow: 1}}>
        <MainMenuItem
          label='Manage'
          open={open}
          icon={<Manage />}
          component={Link}
          to='/uploads'
          active={matches('/uploads')}
        />
        <MainMenuItem
          label='Explore'
          open={open}
          icon={<Explore />}
          component={Link}
          to='/explore'
          active={matches('/exlore')}
        />
        <MainMenuItem
          label='Analyze'
          open={open}
          icon={<Analyze />}
          component={Link}
          to='/analyze'
          active={matches('/analyze')}
        />
      </List>
      <List>
        <MainMenuItem
          label='Groups'
          open={open}
          icon={<Group />}
          component={Link}
          to='/groups'
          active={matches('/groups')}
        />
        <Divider />
        <MainMenuItem
          label='Dev tools'
          open={open}
          icon={<Dev />}
          onClick={handleDevToolsOpenClose}
          active={devToolsOpen}
        />
        <MainMenuItem
          label={colorMode === 'dark' ? 'Light' : 'Dark'}
          open={open}
          icon={<ToggleColorMode />}
          onClick={handleToggleColorMode}
        />
        <LoginMainMenuItem open={open} />
      </List>
    </MainMenuDrawer>
  )
}
