import {render, screen} from '@testing-library/react'

import ErrorMessage from './ErrorMessage'

describe('ErrorMessage', () => {
  it.each([
    ['RouteError', 'Ops, this does not exist!'],
    ['DoesNotExist', 'Data not available'],
    ['else', 'Unexpected error'],
  ])('renders error for %s', (errorName, errorMessage) => {
    render(<ErrorMessage error={{name: errorName, message: 'error message'}} />)
    expect(screen.getByText(errorMessage)).toBeInTheDocument()
  })

  it('renders error with stack', () => {
    render(
      <ErrorMessage
        error={{
          name: 'Exception',
          message: 'error message',
          stack: 'stack',
        }}
      />,
    )
    expect(screen.getByText('stack')).toBeInTheDocument()
  })
})
