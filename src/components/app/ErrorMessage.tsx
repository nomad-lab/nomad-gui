import {Typography} from '@mui/material'

export type ErrorMessageProps = {error: Error}

export default function ErrorMessage({error}: ErrorMessageProps) {
  switch (error.name) {
    case 'RouteError':
      return (
        <Typography variant='h1' color='error'>
          Ops, this does not exist!
        </Typography>
      )
    case 'DoesNotExist':
      return (
        <>
          <Typography variant='h1' color='error'>
            Data not available
          </Typography>
          <Typography color='error'>
            The requested object does not exist or is not visible to you.
          </Typography>
        </>
      )
    default:
      return (
        <>
          <Typography variant='h1' color='error'>
            Unexpected error
          </Typography>
          <Typography color='error'>{error.message}</Typography>
          {error.stack && (
            <Typography component='div'>
              <pre>{error.stack}</pre>
            </Typography>
          )}
        </>
      )
  }
}
