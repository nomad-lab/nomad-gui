import {LinearProgress} from '@mui/material'
import Box from '@mui/material/Box'
import CssBaseline from '@mui/material/CssBaseline'
import * as React from 'react'

import {useIsApiLoading} from '../../hooks/useApi'
import Outlet from '../routing/Outlet'
import useRoute from '../routing/useRoute'

export function LoadingIndicator() {
  const {isLoading} = useRoute()
  const loading = useIsApiLoading()[0]
  if (!isLoading && !loading) {
    return null
  }
  return (
    <LinearProgress sx={{position: 'absolute', top: 0, left: 0, right: 0}} />
  )
}

export default function App({children}: React.PropsWithChildren) {
  return (
    <>
      <CssBaseline />
      <LoadingIndicator />
      <Box component='main' sx={{height: '100%'}}>
        {children || <Outlet />}
      </Box>
    </>
  )
}
