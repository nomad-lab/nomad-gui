import {
  ClickAwayListener,
  Divider,
  Grow,
  ListItemIcon,
  ListItemText,
  MenuItem,
  MenuList,
  Paper,
  Popper,
} from '@mui/material'
import React, {
  PropsWithChildren,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react'
import {
  AuthProvider as OIDCAuthProvider,
  AuthProviderProps as OIDCAuthProviderProps,
  hasAuthParams,
} from 'react-oidc-context'

import config from '../../config'
import useAuth from '../../hooks/useAuth'
import DevToolsJsonViewer from '../devTools/DevToolsJsonViewer'
import useDevTools from '../devTools/useDevTools'
import {Login, Logout, Profile} from '../icons'
import {MainMenuItem, MainMenuItemProps} from './MainMenu'

const oidcRedirectUriParameters = ['code', 'state', 'session_state', 'iss']

const oidcConfig = {
  authority: `${config.keycloak.public_server_url?.replace(/\/$/, '')}/realms/${
    config.keycloak.realm_name
  }`,
  client_id: config.keycloak.client_id as string,
  redirect_uri: window.location.toString(),
  onSigninCallback: () => {
    // this removes the OIDC specific parameters from the URL after getting
    // back from the OIDC provider
    const url = new URL(window.location.href)
    const params = new URLSearchParams(url.search)
    oidcRedirectUriParameters.forEach((param) => params.delete(param))
    url.search = params.toString()
    window.history.replaceState({}, document.title, url)
  },
} satisfies OIDCAuthProviderProps

export function AuthDevTool() {
  const auth = useAuth()
  return <DevToolsJsonViewer value={auth} />
}

export function AuthProvider({children}: PropsWithChildren) {
  return <OIDCAuthProvider {...oidcConfig}>{children}</OIDCAuthProvider>
}

export function LoginMainMenuItem(props: Partial<MainMenuItemProps>) {
  const {
    signinRedirect,
    signoutRedirect,
    isAuthenticated,
    isLoading,
    user,
    activeNavigator,
    signinSilent,
  } = useAuth()
  const [hasTriedSignin, setHasTriedSignin] = useState(false)

  const [menuOpen, setMenuOpen] = useState(false)
  const anchorRef = useRef<HTMLElement>(null)

  const signin = useCallback(() => {
    if (
      !hasAuthParams() &&
      !isAuthenticated &&
      !activeNavigator &&
      !isLoading
    ) {
      signinRedirect({
        redirectMethod: 'replace',
        redirect_uri: window.location.toString(),
      })
    }
  }, [activeNavigator, isAuthenticated, isLoading, signinRedirect])

  const handleClick = useCallback(() => {
    if (isAuthenticated) {
      setMenuOpen((prevOpen) => !prevOpen)
    } else {
      signin()
    }
  }, [setMenuOpen, isAuthenticated, signin])

  const handleLogoutClick = useCallback(() => {
    setMenuOpen(false)
    signoutRedirect({
      redirectMethod: 'replace',
      post_logout_redirect_uri: window.location.toString(),
    })
  }, [signoutRedirect, setMenuOpen])

  const handleMenuClose = (event: Event | React.SyntheticEvent) => {
    if (
      anchorRef.current &&
      anchorRef.current.contains(event.target as HTMLElement)
    ) {
      return
    }

    setMenuOpen(false)
  }

  useEffect(() => {
    if (
      !hasAuthParams() &&
      !isAuthenticated &&
      !activeNavigator &&
      !isLoading &&
      !hasTriedSignin
    ) {
      signinSilent()
      setHasTriedSignin(true)
    }
  }, [
    activeNavigator,
    hasTriedSignin,
    isAuthenticated,
    isLoading,
    signinSilent,
  ])

  const devTools = useDevTools()

  useEffect(() => {
    devTools.registerTool('auth', <AuthDevTool />)
    return () => devTools.unregisterTool('auth')
  }, [devTools])

  const userHandle = useMemo(() => {
    if (!user) {
      return null
    }

    if (user.profile.preferred_username) {
      return user.profile.preferred_username
    }
  }, [user])

  return (
    <>
      <MainMenuItem
        ref={anchorRef}
        label={isAuthenticated ? 'Profile' : 'Login'}
        {...props}
        onClick={handleClick}
        disabled={isLoading}
        icon={isAuthenticated ? <Profile /> : <Login />}
      />
      <Popper
        open={menuOpen}
        anchorEl={anchorRef.current}
        role={undefined}
        placement='right-end'
        transition
      >
        {({TransitionProps}) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin: 'right end',
            }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleMenuClose}>
                <MenuList autoFocusItem={menuOpen}>
                  <MenuItem disabled>
                    <ListItemText>
                      You are logged in as {userHandle}
                    </ListItemText>
                  </MenuItem>
                  <Divider />
                  <MenuItem
                    href={`${config.keycloak.public_server_url}/${config.keycloak.realm_name}/account`}
                  >
                    <ListItemIcon>
                      <Profile fontSize='small' />
                    </ListItemIcon>
                    <ListItemText>Profile</ListItemText>
                  </MenuItem>
                  <MenuItem onClick={handleLogoutClick}>
                    <ListItemIcon>
                      <Logout fontSize='small' />
                    </ListItemIcon>
                    <ListItemText>Logout</ListItemText>
                  </MenuItem>
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </>
  )
}
