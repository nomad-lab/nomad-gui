import {render, screen} from '@testing-library/react'
import React, {PropsWithChildren} from 'react'
import {RecoilRoot} from 'recoil'
import {vi} from 'vitest'

import {RouteData} from '../routing/types'
import * as useRoute from '../routing/useRoute'
import App from './App'

export const renderOptions = {
  wrapper: ({children}: PropsWithChildren) => {
    return <RecoilRoot>{children}</RecoilRoot>
  },
}

describe('App', () => {
  it('renders without loading indictor', () => {
    vi.spyOn(useRoute, 'default').mockImplementation(
      () =>
        ({
          isLoading: false,
        } as RouteData),
    )
    render(<App>Hello</App>, renderOptions)
    expect(screen.getByText('Hello')).toBeInTheDocument()
    expect(screen.queryByRole('progressbar')).not.toBeInTheDocument()
  })

  it('renders with loading indicator', () => {
    vi.spyOn(useRoute, 'default').mockImplementation(
      () =>
        ({
          isLoading: true,
        } as RouteData),
    )
    render(<App>Hello</App>, renderOptions)
    expect(screen.getByText('Hello')).toBeInTheDocument()
    expect(screen.getByRole('progressbar')).toBeInTheDocument()
  })
})
