import {render, screen} from '@testing-library/react'

import ProcessStatus from './ProcessStatus'

describe('ProcessStatus', () => {
  it.each([
    ['SUCCESS', 'processed'],
    ['PROCESS', 'processing'],
    ['READY', 'processing'],
    ['FAILED', 'failed'],
    ['else', 'state unknown'],
  ])('renders %s', (status, label) => {
    render(<ProcessStatus entity={{process_status: status}} />)
    expect(screen.getByText(label)).toBeInTheDocument()
  })
})
