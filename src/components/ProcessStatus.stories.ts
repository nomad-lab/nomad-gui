import type {Meta, StoryObj} from '@storybook/react'

import ProcessStatus from './ProcessStatus'

const meta = {
  title: 'components/ProcessStatus',
  component: ProcessStatus,
  tags: ['autodocs'],
} satisfies Meta<typeof ProcessStatus>

export default meta
type Story = StoryObj<typeof meta>

export const SUCCESS: Story = {
  args: {
    entity: {process_status: 'SUCCESS'},
  },
}

export const PROCESS: Story = {
  args: {
    entity: {process_status: 'PROCESS'},
  },
}

export const FAILED: Story = {
  args: {
    entity: {process_status: 'FAILED'},
  },
}
