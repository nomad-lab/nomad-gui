import {render, screen} from '@testing-library/react'

import {UploadResponse} from '../models/graphResponseModels'
import Visibility from './Visibility'

describe('Visibility', () => {
  it.each([
    ['published', {published: true, with_embargo: false}],
    [
      'shared',
      {published: true, with_embargo: true, writers: [1], viewers: [1]},
    ],
    ['private', {published: false, writers: [], viewers: []}],
  ])('renders %s', (label, upload) => {
    render(<Visibility upload={upload as UploadResponse} />)
    expect(screen.getByText(label)).toBeInTheDocument()
  })
})
