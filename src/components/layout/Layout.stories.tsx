import {Meta, StoryObj} from '@storybook/react'

import {Layout} from './Layout'

const meta = {
  title: 'components/layout/Layout',
  component: Layout,
  tags: ['autodocs'],
} satisfies Meta<typeof Layout>

export default meta

type Story = StoryObj<typeof meta>
export const Root: Story = {
  args: {
    layout: {
      type: 'container',
      children: [
        {
          type: 'container',
          xs: 12,
          md: 5,
          children: [
            {
              type: 'text',
              xs: 12,
              content:
                'This demonstrates a text layout, e.g. for documentation.',
            },
            {
              type: 'divider',
              xs: 12,
            },
            {
              type: 'text',
              xs: 12,
              content: 'And some more text.',
            },
          ],
        },
        {
          type: 'card',
          title: 'Example card',
          xs: 12,
          md: 7,
          children: [
            {
              type: 'value',
              xs: 6,
              label: 'test quantity',
              value: 'Hello World', // this should be replaced with a selector on the archive
            },
            {
              type: 'value',
              xs: 6,
              label: 'test quantity',
              value: 1.23, // this should be replaced with a selector on the archive
            },
            {
              type: 'divider',
              xs: 12,
              content: 'more quantities',
              textAlign: 'left',
            },
            {
              type: 'value',
              xs: 6,
              label: 'test quantity',
              value: 'Hello World', // this should be replaced with a selector on the archive
            },
            {
              type: 'value',
              xs: 6,
              label: 'test quantity',
              value: 1.23, // this should be replaced with a selector on the archive
            },
          ],
        },
        {
          type: 'card',
          xs: 12,
          children: [
            {
              type: 'text',
              content: 'This is only visible when expanded.',
            },
          ],
          collapsedChildren: [
            {
              type: 'text',
              content:
                'This is a collapsed card. If collapsed, only this text is shown.',
            },
          ],
        },
        {
          type: 'card',
          xs: 12,
          title: 'Another card',
          expandedByDefault: true,
          children: [
            {
              type: 'text',
              content: 'This is only visible when expanded.',
            },
          ],
          collapsedChildren: [
            {
              type: 'text',
              content:
                'This is a collapsed card. If collapsed, only this text is shown.',
            },
          ],
        },
      ],
    },
  },
}
