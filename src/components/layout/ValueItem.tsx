import {DynamicContainerProps} from '../values/containers/ContainerProps'
import DynamicValue from '../values/utils/DynamicValue'
import {DynamicComponentSpec} from '../values/utils/dynamicComponents'
import {AbstractItem, LayoutItem} from './Layout'

export type Value<Value = unknown> = AbstractItem & {
  type: 'value'
  component?: DynamicComponentSpec
  value: Value
} & Pick<
    DynamicContainerProps,
    | 'label'
    | 'helperText'
    | 'placeholder'
    | 'editable'
    | 'displayUnit'
    | 'displayUnitLocked'
  >

export default function ValueItem({layout}: {layout: LayoutItem}) {
  const {
    value,
    type,
    component: componentOrUndefined,
    ...valueProps
  } = layout as Value

  const component = componentOrUndefined || 'Text'

  return (
    <DynamicValue
      fullWidth
      component={component}
      value={value}
      {...valueProps}
    />
  )
}
