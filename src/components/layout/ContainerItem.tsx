import {Box, BoxProps} from '@mui/material'
import Grid from '@mui/material/Grid2'

import {AbstractContainer, Item, Layout, LayoutItem} from './Layout'
import useLayoutProps from './useLayoutProps'

export type Container = AbstractContainer & {
  type: 'container'
  variant?: 'grid' | 'flex'
} & Pick<Partial<BoxProps>, 'sx'>

export default function ContainerItem({layout}: {layout: LayoutItem}) {
  const container = layout as Container
  const {type, children, variant = 'grid', ...boxProps} = container
  const {action} = useLayoutProps()

  let contents
  if (variant === 'flex') {
    contents = (
      <Box
        {...boxProps}
        sx={{
          display: 'flex',
          flexDirection: 'row',
          gap: 2,
          flexGrow: 1,
          ...boxProps?.sx,
        }}
      >
        {children.map((child, index) => (
          <Box key={index} sx={{...(child.grow && {flexGrow: 1})}}>
            <Layout layout={child} />
          </Box>
        ))}
      </Box>
    )
  } else {
    contents = (
      <Grid container rowSpacing={2} columnSpacing={2} sx={{flexGrow: 1}}>
        {children.map((child, index) => (
          <Item layout={child} key={index}>
            <Layout layout={child} />
          </Item>
        ))}
      </Grid>
    )
  }

  if (action) {
    return (
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'flexStart',
          gap: 1,
        }}
      >
        {contents}
        {action}
      </Box>
    )
  } else {
    return contents
  }
}
