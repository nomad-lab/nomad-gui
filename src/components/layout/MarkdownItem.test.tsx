import {render, screen} from '@testing-library/react'

import {Layout} from './Layout'

describe('MarkdownItem', () => {
  it('renders a layout with MarkdownItem', () => {
    render(<Layout layout={{type: 'markdown', content: '# test_content'}} />)
    expect(screen.getByText('test_content')).toBeInTheDocument()
  })
})
