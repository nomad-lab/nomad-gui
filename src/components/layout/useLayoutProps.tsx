import React from 'react'

type LayoutPropsContextValue = {
  action?: React.ReactNode
}

/**
 * This context allows to pass additional props to the layout components.
 */
export const layoutPropsContext =
  React.createContext<LayoutPropsContextValue | null>(null)

/**
 * @returns The current layout context value or an empty object.
 */
export default function useLayoutProps(): Partial<LayoutPropsContextValue> {
  return React.useContext(layoutPropsContext) || {}
}
