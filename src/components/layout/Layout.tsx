import Grid, {Grid2Props as GridProps, GridSize} from '@mui/material/Grid2'
import React, {PropsWithChildren, useCallback} from 'react'

import ErrorBoundary from '../ErrorBoundary'
import ElementItem, {Element} from '../editor/ElementItem'
import ImagePreviewItem, {ImagePreview} from '../editor/ImagePreviewItem'
import PlotEditor, {Plot} from '../editor/PlotEditor'
import QuantityEditor, {Quantity} from '../editor/QuantityEditor'
import RichTextEditor, {RichText} from '../editor/RichTextEditor'
import SubSectionEditor, {SubSection} from '../editor/SubSectionEditor'
import SubSectionTableEditor, {
  SubSectionTable,
} from '../editor/SubSectionTableEditor'
import CardItem, {Card} from './CardItem'
import ContainerItem, {Container} from './ContainerItem'
import DividerItem, {Divider} from './DividerItem'
import ImageItem, {Image} from './ImageItem'
import MarkdownItem, {Markdown} from './MarkdownItem'
import TextItem, {Text} from './TextItem'
import ValueItem, {Value} from './ValueItem'

type RegularBreakpoints = {
  lg?: GridSize
  md?: GridSize
  sm?: GridSize
  xl?: GridSize
  xs?: GridSize
}

const components = {
  text: TextItem,
  markdown: MarkdownItem,
  image: ImageItem,
  container: ContainerItem,
  card: CardItem,
  value: ValueItem,
  divider: DividerItem,
  quantity: QuantityEditor,
  element: ElementItem,
  richText: RichTextEditor,
  subSection: SubSectionEditor,
  table: SubSectionTableEditor,
  imagePreview: ImagePreviewItem,
  plot: PlotEditor,
}

export type LayoutItem =
  | Text
  | Markdown
  | Image
  | Container
  | Card
  | Value
  | Divider
  | Quantity
  | Element
  | RichText
  | SubSection
  | SubSectionTable
  | ImagePreview
  | Plot

export type AbstractItem = {
  type: string
  grow?: boolean
} & RegularBreakpoints

export type AbstractContainer = AbstractItem & {
  children: LayoutItem[]
}

export function Item({
  layout,
  children,
  ...gridProps
}: {layout: LayoutItem} & GridProps & PropsWithChildren) {
  const breakpointProps = {
    xs: layout.xs || 12,
    sm: layout.sm,
    md: layout.md,
    lg: layout.lg,
    xl: layout.xl,
  }
  return (
    <Grid sx={{flexGrow: 1}} size={breakpointProps} {...gridProps}>
      {children}
    </Grid>
  )
}

function RenderedLayout({layout}: LayoutProps) {
  const type = layout.type
  const component = components[type]
  if (!component) {
    throw new Error(`Unknown layout type: ${type}`)
  }
  return React.createElement(component, {layout})
}

export type LayoutProps = {
  /**
   * The root layout of the layout to render.
   */
  layout: LayoutItem
}

/**
 * Renders the given data with the layout given by its root layout.
 */
export function Layout({layout}: LayoutProps) {
  const renderError = useCallback(
    (error: Error) => {
      let layoutString
      try {
        layoutString = JSON.stringify(layout, null, 2)
      } catch {
        layoutString = 'Could not stringify layout'
      }
      return (
        <div>
          Error rendering layout:
          <pre>{layoutString}</pre>
          <br />
          {error.message}
        </div>
      )
    },
    [layout],
  )
  return (
    <ErrorBoundary renderError={renderError}>
      <RenderedLayout layout={layout} />
    </ErrorBoundary>
  )
}
