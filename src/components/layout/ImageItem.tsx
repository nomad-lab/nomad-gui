import {AbstractItem, LayoutItem} from '../layout/Layout'

export type Image = AbstractItem & {
  type: 'image'
  src: string
  alt?: string
}

export default function ImagePreviewItem({layout}: {layout: LayoutItem}) {
  const image = layout as Image
  const {src, alt} = image
  return <img src={src} alt={alt} width={'100%'} />
}
