import {render, screen} from '@testing-library/react'

import {Layout} from './Layout'

describe('ContainerItem', () => {
  it('renders a layout with ContainerItem', () => {
    render(
      <Layout
        layout={{
          type: 'container',
          children: [
            {type: 'text', content: 'text_content_1'},
            {type: 'text', content: 'text_content_2'},
          ],
        }}
      />,
    )
    expect(screen.getByText('text_content_1')).toBeInTheDocument()
    expect(screen.getByText('text_content_2')).toBeInTheDocument()
  })
})
