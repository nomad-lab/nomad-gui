import {
  Box,
  CardContent,
  CardHeader,
  CardProps,
  Collapse,
  Card as MUICard,
  styled,
} from '@mui/material'
import React, {useCallback, useMemo, useState} from 'react'

import ExpandMore from '../ExpandMore'
import {layoutPropsContext} from '../layout/useLayoutProps'
import ContainerItem, {Container} from './ContainerItem'
import {AbstractContainer, LayoutItem} from './Layout'
import useLayoutProps from './useLayoutProps'

export type Card = AbstractContainer & {
  type: 'card'
  /**
   * The optional title of the card
   */
  title?: string
  /**
   * If this property is given, the card is expandable.
   * An optional array of children that are displayed even when the
   * card is collapsed.
   */
  collapsedChildren?: LayoutItem[]
  /**
   * If `collapsedChildren` is given, this flag determines if the
   * the card is expanded by default.
   */
  expandedByDefault?: boolean
  expandTransitionTimeout?: number
  variant?: 'standard' | 'highlighted'
} & Pick<Partial<CardProps>, 'raised' | 'sx'>

const CardItemRoot = styled(MUICard, {
  name: 'CardItem',
  slot: 'root',
  overridesResolver: ({ownerState}, styles) => {
    return [
      styles.root,
      ownerState.variant === 'standard' && styles.standard,
      ownerState.variant === 'highlighted' && styles.highlighted,
    ]
  },
})<{
  ownerState: CardItemOwnerState
}>(() => ({
  width: '100%',
  height: '100%',
}))

export type CardItemProps = {
  layout: LayoutItem
}

export type CardItemOwnerState = {
  variant: Card['variant']
}

export default function CardItem({layout}: CardItemProps) {
  const card = layout as Card
  const {
    type,
    title,
    collapsedChildren,
    expandedByDefault,
    expandTransitionTimeout,
    variant = 'standard',
    ...cardProps
  } = card
  const [expanded, setExpanded] = useState(
    !collapsedChildren || expandedByDefault || false,
  )
  const collapsedLayout =
    collapsedChildren &&
    ({
      type: 'container',
      children: collapsedChildren,
    } as Container)
  const {action} = useLayoutProps()
  const handleExpand = useCallback(() => {
    setExpanded((prev) => !prev)
  }, [setExpanded])
  const actions = useMemo(() => {
    const actions: React.ReactNode[] = []
    if (collapsedChildren) {
      actions.push(
        <ExpandMore
          key='expand'
          size='small'
          expanded={expanded}
          onClick={handleExpand}
        />,
      )
    }
    if (action) {
      actions.push(<React.Fragment key='action'>{action}</React.Fragment>)
    }
    return actions
  }, [collapsedChildren, action, handleExpand, expanded])
  return (
    <layoutPropsContext.Provider value={{}}>
      <CardItemRoot ownerState={{variant: variant}} {...cardProps}>
        {title && (
          <CardHeader
            title={title}
            action={actions.length > 0 ? actions : undefined}
          />
        )}
        {collapsedLayout && (
          <CardContent
            sx={{
              display: 'flex',
              padding: 2,
              '&:last-child': {paddingBottom: 2},
            }}
          >
            <Box sx={{flexGrow: 1, width: '100%'}}>
              <ContainerItem layout={collapsedLayout} />
            </Box>
            {!title && actions.length > 0 && (
              <Box sx={{marginLeft: 1, marginRight: -1, marginTop: -1}}>
                {actions}
              </Box>
            )}
          </CardContent>
        )}
        <Collapse in={expanded} unmountOnExit timeout={expandTransitionTimeout}>
          <CardContent
            sx={{
              display: 'flex',
              marginTop: collapsedLayout ? -2 : 0,
              padding: 2,
              '&:last-child': {paddingBottom: 2},
            }}
          >
            <Box sx={{flexGrow: 1, width: '100%'}}>
              <ContainerItem layout={card} />
            </Box>
            {!title && actions.length > 0 && !collapsedLayout && (
              <Box sx={{marginRight: -1, marginTop: -1, marginLeft: 1}}>
                {actions}
              </Box>
            )}
          </CardContent>
        </Collapse>
      </CardItemRoot>
    </layoutPropsContext.Provider>
  )
}
