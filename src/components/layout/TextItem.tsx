import {Typography, TypographyProps} from '@mui/material'

import {AbstractItem, LayoutItem} from './Layout'

export type Text = AbstractItem & {
  type: 'text'
  content: string
} & Pick<Partial<TypographyProps>, 'variant' | 'align' | 'color' | 'sx'>

export default function TextItem({layout}: {layout: LayoutItem}) {
  const {content, type, ...typographyProps} = layout as Text
  return <Typography {...typographyProps}>{content}</Typography>
}
