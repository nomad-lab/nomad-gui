import {DividerProps, Divider as MUIDivider} from '@mui/material'

import {AbstractItem, LayoutItem} from './Layout'

export type Divider = AbstractItem & {
  type: 'divider'
  content?: string
} & Pick<
    Partial<DividerProps>,
    'absolute' | 'orientation' | 'variant' | 'textAlign' | 'sx'
  >

export default function DividerItem({layout}: {layout: LayoutItem}) {
  const {type, content, ...dividerProps} = layout as Divider
  return <MUIDivider {...dividerProps}>{content}</MUIDivider>
}
