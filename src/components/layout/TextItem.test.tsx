import {render, screen} from '@testing-library/react'

import {Layout} from './Layout'

describe('TextItem', () => {
  it('renders a layout with TextItem', () => {
    render(<Layout layout={{type: 'text', content: 'test_content'}} />)
    expect(screen.getByText('test_content')).toBeInTheDocument()
  })
})
