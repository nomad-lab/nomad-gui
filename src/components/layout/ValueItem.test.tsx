import {render, screen} from '@testing-library/react'

import {Layout} from './Layout'

describe('ValueItem', () => {
  it('renders a layout with ValueItem', () => {
    render(<Layout layout={{type: 'value', value: 'test_content'}} />)
    expect(screen.getByText('test_content')).toBeInTheDocument()
  })
})
