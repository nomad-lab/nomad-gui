import {SxProps} from '../../utils/types'
import MarkdownComponent from '../markdown/Markdown'
import {AbstractItem, LayoutItem} from './Layout'

export type Markdown = AbstractItem & {
  type: 'markdown'
  content: string
} & SxProps

export default function MarkdownItem({layout}: {layout: LayoutItem}) {
  const {content, type, ...sxProps} = layout as Markdown
  return <MarkdownComponent content={content} {...sxProps} />
}
