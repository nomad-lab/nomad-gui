import {fireEvent, render, screen} from '@testing-library/react'
import {vi} from 'vitest'

import {Layout} from './Layout'
import {Text} from './TextItem'

describe('Layout', () => {
  it('renders a layout with TextItem', () => {
    render(<Layout layout={{type: 'text', content: 'test_content'}} />)
    expect(screen.getByText('test_content')).toBeInTheDocument()
  })

  it('renders an error boundary', () => {
    vi.spyOn(console, 'error').mockImplementation(() => null)
    render(
      <Layout
        layout={{type: 'wrong', content: 'test_content'} as unknown as Text}
      />,
    )
    expect(screen.getByText(/Unknown layout type/)).toBeInTheDocument()
    expect(screen.queryByText('test_content')).not.toBeInTheDocument()
  })

  it('allows to reset an error boundary', () => {
    vi.spyOn(console, 'error').mockImplementation(() => null)
    const {rerender} = render(
      <Layout
        layout={{type: 'wrong', content: 'test_content'} as unknown as Text}
      />,
    )
    expect(screen.getByText(/Unknown layout type/)).toBeInTheDocument()
    expect(screen.queryByText('test_content')).not.toBeInTheDocument()

    rerender(<Layout layout={{type: 'text', content: 'test_content'}} />)
    expect(screen.getByText(/Unknown layout type/)).toBeInTheDocument()
    expect(screen.queryByText('test_content')).not.toBeInTheDocument()

    fireEvent.click(screen.getByText(/Unknown layout type/))
    expect(screen.queryByText(/Unknown layout type/)).not.toBeInTheDocument()
    expect(screen.getByText('test_content')).toBeInTheDocument()
  })
})
