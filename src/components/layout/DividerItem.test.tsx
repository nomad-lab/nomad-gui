import {render, screen} from '@testing-library/react'

import {Divider} from './DividerItem'
import {Layout} from './Layout'

describe('DividerItem', () => {
  it('renders a layout with DividerItem', () => {
    render(
      <Layout
        layout={{
          type: 'container',
          children: [
            {type: 'text', content: 'text_content_above'},
            {
              type: 'divider',
              'data-testid': 'divider',
            } as unknown as Divider,
            {type: 'text', content: 'text_content_below'},
          ],
        }}
      />,
    )
    expect(screen.getByText('text_content_above')).toBeInTheDocument()
    expect(screen.getByTestId('divider')).toBeInTheDocument()
    expect(screen.getByText('text_content_below')).toBeInTheDocument()
  })
})
