import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import {Layout} from './Layout'
import {layoutPropsContext} from './useLayoutProps'

describe('CardItem', () => {
  it.each([
    ['without title', false],
    ['with title', true],
  ])('renders a card %s', (description, withTitle) => {
    render(
      <Layout
        layout={{
          type: 'card',
          ...(withTitle && {title: 'test_title'}),
          children: [{type: 'text', content: 'test_content'}],
        }}
      />,
    )
    expect(screen.getByText('test_content')).toBeInTheDocument()
    expect(screen.queryByText('test_title')).toBe(
      withTitle ? screen.getByText('test_title') : null,
    )
  })

  it.each([
    ['expanded by default', true, false],
    ['collapsed by default', false, false],
    ['expanded by default with title', true, true],
  ])(
    'renders a expandable card %s',
    (description, expandedByDefault, withTitle) => {
      render(
        <Layout
          layout={{
            type: 'card',
            ...(withTitle && {title: 'test_title'}),
            expandedByDefault,
            collapsedChildren: [{type: 'text', content: 'collapsed_content'}],
            children: [{type: 'text', content: 'test_content'}],
          }}
        />,
      )
      expect(screen.getByText('collapsed_content')).toBeInTheDocument()
      expect(screen.queryByText('test_title')).toBe(
        withTitle ? screen.getByText('test_title') : null,
      )
      expect(screen.queryByText('test_content')).toBe(
        expandedByDefault ? screen.getByText('test_content') : null,
      )
    },
  )

  it('expands and collapses', async () => {
    render(
      <Layout
        layout={{
          type: 'card',
          collapsedChildren: [{type: 'text', content: 'collapsed_content'}],
          children: [{type: 'text', content: 'test_content'}],
          expandTransitionTimeout: 0,
        }}
      />,
    )
    expect(screen.getByText('collapsed_content')).toBeInTheDocument()
    expect(screen.queryByText('test_content')).not.toBeInTheDocument()

    await userEvent.click(screen.getByRole('button'))
    expect(screen.getByText('test_content')).toBeInTheDocument()

    await userEvent.click(screen.getByRole('button'))
    expect(screen.queryByText('test_content')).not.toBeInTheDocument()
  })

  it.each([
    ['and expandable', true],
    ['and not expandable', false],
  ])('renders a layout with remove button %s', (description, expandable) => {
    render(
      <layoutPropsContext.Provider value={{action: 'action'}}>
        <Layout
          layout={{
            type: 'card',
            ...(expandable && {
              collapsedChildren: [{type: 'text', content: 'collapsed_content'}],
            }),
            children: [{type: 'text', content: 'test_content'}],
          }}
        />
      </layoutPropsContext.Provider>,
    )
    expect(screen.getByText('action')).toBeInTheDocument()
  })
})
