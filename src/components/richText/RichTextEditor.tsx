import {$generateHtmlFromNodes, $generateNodesFromDOM} from '@lexical/html'
import LexicalClickableLinkPlugin from '@lexical/react/LexicalClickableLinkPlugin'
import {LexicalComposer} from '@lexical/react/LexicalComposer'
import {useLexicalComposerContext} from '@lexical/react/LexicalComposerContext'
import {ContentEditable as LexicalContentEditable} from '@lexical/react/LexicalContentEditable'
import LexicalErrorBoundary from '@lexical/react/LexicalErrorBoundary'
import {HistoryPlugin} from '@lexical/react/LexicalHistoryPlugin'
import {HorizontalRulePlugin} from '@lexical/react/LexicalHorizontalRulePlugin'
import {ListPlugin} from '@lexical/react/LexicalListPlugin'
import {OnChangePlugin} from '@lexical/react/LexicalOnChangePlugin'
import {RichTextPlugin} from '@lexical/react/LexicalRichTextPlugin'
import {TablePlugin} from '@lexical/react/LexicalTablePlugin'
import {mergeRegister} from '@lexical/utils'
import DevIcon from '@mui/icons-material/Code'
import {
  Box,
  Card,
  CardActions,
  CardContent,
  Divider,
  IconButton,
  Typography,
} from '@mui/material'
import {
  $getRoot,
  $insertNodes,
  $setSelection,
  BLUR_COMMAND,
  COMMAND_PRIORITY_LOW,
  EditorState,
  FOCUS_COMMAND,
  LexicalEditor,
} from 'lexical'
import {useCallback, useEffect, useRef, useState} from 'react'
import {useCounter} from 'react-use'

import useEditDebounce from '../../hooks/useEditDebounce'
import Nodes from './nodes/Nodes'
import AutoLinkPlugin from './plugins/AutoLinkPlugin'
import CodeHighlightPlugin from './plugins/CodeHighlightPlugin'
import EquationsPlugin from './plugins/EquationsPlugin'
import FloatingLinkEditorPlugin from './plugins/FloatingLinkEditorPlugin'
import FloatingTextFormatToolbarPlugin from './plugins/FloatingTextFormatToolbarPlugin'
import ImagesPlugin from './plugins/ImagesPlugin'
import InlineImagePlugin from './plugins/InlineImagePlugin'
import LinkPlugin from './plugins/LinkPlugin'
import MarkdownShortcutPlugin from './plugins/MarkdownShortcutPlugin'
import MentionsPlugin from './plugins/MentionsPlugin'
import ReferencePlugin from './plugins/ReferencePlugin'
import TableActionMenuPlugin from './plugins/TableActionMenuPlugin'
import TableCellResizerPlugin from './plugins/TableCellResizer'
import ToolbarPlugin from './plugins/ToolbarPlugin'
import TreeViewPlugin from './plugins/TreeViewPlugin'
import Theme, {contentEditableSx} from './theme'

function Placeholder({value}: {value?: string}) {
  return (
    <Typography
      sx={{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        paddingX: 2,
        paddingTop: 1,
        fontStyle: 'italic',
        pointerEvents: 'none',
      }}
    >
      {value || 'Type something here...'}
    </Typography>
  )
}

function ContentEditable() {
  return (
    <Box
      sx={{
        ...contentEditableSx,
      }}
    >
      <LexicalContentEditable className={'ContentEditable__root'} />
    </Box>
  )
}

const editorConfig = {
  namespace: 'nomad',
  nodes: Nodes,
  onError(error: Error) {
    throw error
  },
  theme: Theme,
}

function Editor({
  onChange,
  placeholder,
  initialValue,
  readonly,
}: RichTextEditorProps) {
  const [editor] = useLexicalComposerContext()

  const [showDev, setShowDev] = useState(false)
  const [focus, setFocus] = useState(false)
  const [floatingAnchorElem, setFloatingAnchorElem] =
    useState<HTMLDivElement | null>(null)

  const onRef = (anchorElem: HTMLDivElement) => {
    if (anchorElem !== null) {
      setFloatingAnchorElem(anchorElem)
    }
  }

  useEffect(() => {
    return mergeRegister(
      editor.registerCommand(
        BLUR_COMMAND,
        () => {
          setFocus(false)
          return false
        },
        COMMAND_PRIORITY_LOW,
      ),
      editor.registerCommand(
        FOCUS_COMMAND,
        () => {
          setFocus(true)
          return false
        },
        COMMAND_PRIORITY_LOW,
      ),
    )
  }, [editor])

  useEffect(() => {
    if (!initialValue) {
      return
    }

    editor.update(
      () => {
        const parser = new DOMParser()
        const dom = parser.parseFromString(initialValue, 'text/html')
        const nodes = $generateNodesFromDOM(editor, dom)

        // Select the root
        $getRoot().clear()
        $getRoot().select()
        $insertNodes(nodes)
        $setSelection(null)
      },
      {tag: 'initializeValue'},
    )
  }, [editor, initialValue])

  const handleClickDev = useCallback(() => {
    setShowDev((prev) => !prev)
  }, [setShowDev])

  const readHTMLRef = useRef<ReadHTMLFn | null>(null)
  const currentHTMLRef = useRef<string | null>(null)
  const [counter, {inc}] = useCounter(0)

  const handleChange = useCallback(
    (state: EditorState, editor: LexicalEditor, tags: Set<string>) => {
      if (tags.has('focus') || tags.has('initializeValue')) {
        return
      }
      readHTMLRef.current = (setHTML: (html: string) => void) => {
        state.read(() => {
          const html = $generateHtmlFromNodes(editor)
          setHTML(html)
        })
      }
      inc()
    },
    [readHTMLRef, inc],
  )

  useEditDebounce(() => {
    if (readHTMLRef.current) {
      readHTMLRef.current((html) => {
        if (currentHTMLRef.current === html) {
          return
        }
        currentHTMLRef.current = html
        onChange?.(html)
      })
    }
  }, [readHTMLRef, onChange, counter])

  return (
    <Card sx={{borderColor: focus ? 'primary.main' : undefined}}>
      {!readonly && (
        <CardContent sx={{padding: 1}}>
          <ToolbarPlugin
            withFormats
            withAlignments
            withFontColor
            withInsertNodes
            withTableActions
            actions={
              <IconButton
                onClick={handleClickDev}
                size='small'
                color={showDev ? 'primary' : 'default'}
              >
                <DevIcon />
              </IconButton>
            }
          />
        </CardContent>
      )}
      {!readonly && <Divider />}
      <CardContent
        sx={{
          position: 'relative',
          '[contenteditable]': {outline: '0px solid transparent'},
          paddingY: 0,
          '&:last-child': {
            paddingBottom: 0,
          },
        }}
      >
        <MarkdownShortcutPlugin />
        <ListPlugin />
        <LexicalClickableLinkPlugin />
        <LinkPlugin />
        <AutoLinkPlugin />
        <TablePlugin hasCellMerge hasCellBackgroundColor />
        <TableCellResizerPlugin />
        <TableActionMenuPlugin
          anchorElem={floatingAnchorElem || undefined}
          cellMerge={true}
        />
        <FloatingLinkEditorPlugin
          anchorElem={floatingAnchorElem || undefined}
        />
        <FloatingTextFormatToolbarPlugin
          setIsLinkEditMode={() => {}}
          anchorElem={floatingAnchorElem || undefined}
        />
        <ImagesPlugin />
        <InlineImagePlugin />
        <EquationsPlugin />
        <HorizontalRulePlugin />
        <ReferencePlugin />
        <CodeHighlightPlugin />
        <MentionsPlugin />
        <RichTextPlugin
          contentEditable={
            <div className='editor' ref={onRef}>
              <ContentEditable />
            </div>
          }
          placeholder={(isEditable) =>
            !isEditable || focus ? null : <Placeholder value={placeholder} />
          }
          ErrorBoundary={LexicalErrorBoundary}
        />
      </CardContent>
      <HistoryPlugin />
      {!readonly && (
        <OnChangePlugin onChange={handleChange} ignoreSelectionChange />
      )}
      {showDev && (
        <CardActions
          sx={{
            fontFamily: 'monospace',
            fontSize: 10,
            color: 'black',
            background: 'white',
            overflowX: 'auto',
          }}
        >
          <TreeViewPlugin />
        </CardActions>
      )}
    </Card>
  )
}

type ReadHTMLFn = (setHTML: (html: string) => void) => void

export type RichTextEditorProps = {
  onChange?: (html: string) => void
  placeholder?: string
  initialValue?: string
  readonly?: boolean
}

export default function RichTextEditor(props: RichTextEditorProps) {
  const {readonly} = props
  return (
    <LexicalComposer
      initialConfig={{...editorConfig, editable: !readonly}}
      key={`${!readonly}`}
    >
      <Editor {...props} />
    </LexicalComposer>
  )
}
