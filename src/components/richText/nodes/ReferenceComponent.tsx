import {useLexicalComposerContext} from '@lexical/react/LexicalComposerContext'
import {mergeRegister} from '@lexical/utils'
import {
  Button,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@mui/material'
import {
  $getNodeByKey,
  $getSelection,
  $isNodeSelection,
  COMMAND_PRIORITY_HIGH,
  KEY_ESCAPE_COMMAND,
  NodeKey,
  SELECTION_CHANGE_COMMAND,
} from 'lexical'
import * as React from 'react'
import {useCallback, useEffect, useRef, useState} from 'react'
import {ErrorBoundary} from 'react-error-boundary'

import Matrix from '../../values/containers/Matrix'
import Numeral from '../../values/primitives/Numeral'
import ReferenceEditor from './ReferenceEditor'
import {$isReferenceNode} from './ReferenceNode'

type ReferenceComponentProps = {
  reference: string
  inline: boolean
  nodeKey: NodeKey
}

export default function ReferenceComponent({
  reference,
  inline,
  nodeKey,
}: ReferenceComponentProps): JSX.Element {
  const [editor] = useLexicalComposerContext()
  const [referenceValue, setReferenceValue] = useState(reference)
  const [showReferenceEditor, setShowReferenceEditor] = useState<boolean>(false)
  const [showFullView, setShowFullView] = React.useState(false)
  const inputRef = useRef(null)

  const onHide = useCallback(
    (restoreSelection?: boolean) => {
      setShowReferenceEditor(false)
      editor.update(() => {
        const node = $getNodeByKey(nodeKey)
        if ($isReferenceNode(node)) {
          node.setReference(referenceValue)
          if (restoreSelection) {
            node.selectNext(0, 0)
          }
        }
      })
    },
    [editor, referenceValue, nodeKey],
  )

  useEffect(() => {
    if (!showReferenceEditor && referenceValue !== reference) {
      setReferenceValue(reference)
    }
  }, [showReferenceEditor, reference, referenceValue])

  useEffect(() => {
    if (showReferenceEditor) {
      return mergeRegister(
        editor.registerCommand(
          SELECTION_CHANGE_COMMAND,
          () => {
            const activeElement = document.activeElement
            const inputElem = inputRef.current
            if (inputElem !== activeElement) {
              onHide()
            }
            return false
          },
          COMMAND_PRIORITY_HIGH,
        ),
        editor.registerCommand(
          KEY_ESCAPE_COMMAND,
          () => {
            const activeElement = document.activeElement
            const inputElem = inputRef.current
            if (inputElem === activeElement) {
              onHide(true)
              return true
            }
            return false
          },
          COMMAND_PRIORITY_HIGH,
        ),
      )
    } else {
      return editor.registerUpdateListener(({editorState}) => {
        const isSelected = editorState.read(() => {
          const selection = $getSelection()
          return (
            $isNodeSelection(selection) &&
            selection.has(nodeKey) &&
            selection.getNodes().length === 1
          )
        })
        if (isSelected) {
          setShowReferenceEditor(true)
        }
      })
    }
  }, [editor, nodeKey, onHide, showReferenceEditor])

  if (showReferenceEditor) {
    return (
      <ReferenceEditor
        reference={referenceValue}
        setReference={setReferenceValue}
        ref={inputRef}
      />
    )
  }

  if (inline) {
    return (
      <React.Fragment>
        <Chip
          label={'Matrix 3x6'}
          size={'small'}
          onClick={(event) =>
            event.detail === 2
              ? setShowReferenceEditor(true)
              : setShowFullView(true)
          }
        />
        <Dialog open={showFullView} onClose={() => setShowFullView(false)}>
          <DialogTitle style={{cursor: 'move'}} id='draggable-dialog-title'>
            {referenceValue}
          </DialogTitle>
          <DialogContent>
            <ErrorBoundary onError={(e) => editor._onError(e)} fallback={null}>
              <Matrix
                width={400}
                renderValue={() => <Numeral />}
                value={Array(3)
                  .fill(0)
                  .map((_, i) =>
                    Array(6)
                      .fill(0)
                      .map((_, j) => i + j),
                  )}
              />
            </ErrorBoundary>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={() => setShowFullView(false)}>
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    )
  }

  return (
    <ErrorBoundary onError={(e) => editor._onError(e)} fallback={null}>
      <Matrix
        label={referenceValue}
        width={400}
        renderValue={() => <Numeral />}
        value={Array(3)
          .fill(0)
          .map((_, i) =>
            Array(6)
              .fill(0)
              .map((_, j) => i + j),
          )}
      />
    </ErrorBoundary>
  )
}
