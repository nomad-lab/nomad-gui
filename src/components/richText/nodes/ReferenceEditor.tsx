import {Input} from '@mui/material'
import type {Ref, RefObject} from 'react'
import * as React from 'react'
import {ChangeEvent, forwardRef} from 'react'

type ReferenceEditorProps = {
  reference: string
  setReference: (reference: string) => void
}

const ReferenceEditor = forwardRef<
  HTMLInputElement | HTMLTextAreaElement,
  ReferenceEditorProps
>(function ReferenceEditor(
  {reference, setReference}: ReferenceEditorProps,
  forwardedRef: Ref<HTMLInputElement | HTMLTextAreaElement>,
): JSX.Element {
  const onChange = (event: ChangeEvent) => {
    setReference((event.target as HTMLInputElement).value)
  }

  return (
    <Input
      value={reference}
      onChange={onChange}
      ref={forwardedRef as RefObject<HTMLTextAreaElement>}
    />
  )
})

export default ReferenceEditor
