import type {
  DOMConversionMap,
  DOMConversionOutput,
  LexicalNode,
  NodeKey,
  SerializedLexicalNode,
  Spread,
} from 'lexical'
import {$applyNodeReplacement, DOMExportOutput, DecoratorNode} from 'lexical'
import * as React from 'react'
import {Suspense} from 'react'

import ReferenceComponent from './ReferenceComponent'

type SerializedReferenceNode = Spread<
  {
    reference: string
    inline: boolean
  },
  SerializedLexicalNode
>

function $convertReferenceElement(
  domNode: HTMLElement,
): null | DOMConversionOutput {
  const reference = domNode.getAttribute('data-lexical-reference')
  const inline = domNode.getAttribute('data-lexical-inline') === 'true'
  if (reference) {
    const node = $createReferenceNode(reference, inline)
    return {node}
  }

  return null
}

export class ReferenceNode extends DecoratorNode<JSX.Element> {
  __reference: string
  __inline: boolean

  static getType(): string {
    return 'reference'
  }

  static clone(node: ReferenceNode): ReferenceNode {
    return new ReferenceNode(node.__reference, node.__inline, node.__key)
  }

  constructor(reference: string, inline?: boolean, key?: NodeKey) {
    super(key)
    this.__reference = reference
    this.__inline = inline ?? false
  }

  static importJSON(serializedNode: SerializedReferenceNode): ReferenceNode {
    const node = $createReferenceNode(
      serializedNode.reference,
      serializedNode.inline,
    )
    return node
  }

  exportJSON(): SerializedReferenceNode {
    return {
      reference: this.getReference(),
      inline: this.__inline,
      type: 'reference',
      version: 1,
    }
  }

  createDOM(): HTMLElement {
    const element = document.createElement('span')
    element.className = 'editor-reference'
    return element
  }

  exportDOM(): DOMExportOutput {
    const element = document.createElement('span')
    element.setAttribute('data-lexical-reference', this.__reference)
    element.setAttribute('data-lexical-inline', `${this.__inline}`)
    element.textContent = this.__reference
    return {element}
  }

  static importDOM(): DOMConversionMap | null {
    return {
      div: (domNode: HTMLElement) => {
        if (!domNode.hasAttribute('data-lexical-reference')) {
          return null
        }
        return {
          conversion: $convertReferenceElement,
          priority: 2,
        }
      },
      span: (domNode: HTMLElement) => {
        if (!domNode.hasAttribute('data-lexical-reference')) {
          return null
        }
        return {
          conversion: $convertReferenceElement,
          priority: 1,
        }
      },
    }
  }

  updateDOM(prevNode: ReferenceNode): boolean {
    return this.__inline !== prevNode.__inline
  }

  getTextContent(): string {
    return this.__reference
  }

  getReference(): string {
    return this.__reference
  }

  setReference(reference: string): void {
    const writable = this.getWritable()
    writable.__reference = reference
  }

  decorate(): JSX.Element {
    return (
      <Suspense fallback={null}>
        <ReferenceComponent
          reference={this.__reference}
          inline={this.__inline}
          nodeKey={this.__key}
        />
      </Suspense>
    )
  }
}

export function $createReferenceNode(
  reference = '',
  inline = false,
): ReferenceNode {
  const referenceNode = new ReferenceNode(reference, inline)
  return $applyNodeReplacement(referenceNode)
}

export function $isReferenceNode(
  node: LexicalNode | null | undefined,
): node is ReferenceNode {
  return node instanceof ReferenceNode
}
