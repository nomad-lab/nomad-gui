/**
 * Copyright (c) Meta Platforms, Inc. and affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
import {AutoFocusPlugin} from '@lexical/react/LexicalAutoFocusPlugin'
import {useLexicalComposerContext} from '@lexical/react/LexicalComposerContext'
import {LexicalErrorBoundary} from '@lexical/react/LexicalErrorBoundary'
import {LexicalNestedComposer} from '@lexical/react/LexicalNestedComposer'
import {RichTextPlugin} from '@lexical/react/LexicalRichTextPlugin'
import {useLexicalNodeSelection} from '@lexical/react/useLexicalNodeSelection'
import {mergeRegister} from '@lexical/utils'
import EditIcon from '@mui/icons-material/Edit'
import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material'
import type {BaseSelection, LexicalEditor, NodeKey} from 'lexical'
import {
  $getNodeByKey,
  $getSelection,
  $isNodeSelection,
  $setSelection,
  CLICK_COMMAND,
  COMMAND_PRIORITY_LOW,
  DRAGSTART_COMMAND,
  KEY_BACKSPACE_COMMAND,
  KEY_DELETE_COMMAND,
  KEY_ENTER_COMMAND,
  KEY_ESCAPE_COMMAND,
  SELECTION_CHANGE_COMMAND,
} from 'lexical'
import {debounce} from 'lodash-es'
import * as React from 'react'
import {Suspense, useCallback, useEffect, useRef, useState} from 'react'

import LinkPlugin from '../../plugins/LinkPlugin'
import ContentEditable from '../../ui/ContentEditable'
import Placeholder from '../../ui/Placeholder'
import type {Position} from './InlineImageNode'
import {$isInlineImageNode, InlineImageNode} from './InlineImageNode'
import './InlineImageNode.css'

const imageCache = new Set()

function useSuspenseImage(src: string) {
  if (!imageCache.has(src)) {
    throw new Promise((resolve) => {
      const img = new Image()
      img.src = src
      img.onload = () => {
        imageCache.add(src)
        resolve(null)
      }
    })
  }
}

function LazyImage({
  altText,
  className,
  imageRef,
  src,
  width,
  height,
  position,
}: {
  altText: string
  className: string | null
  height: 'inherit' | number
  imageRef: {current: null | HTMLImageElement}
  src: string
  width: 'inherit' | number
  position: Position
}): JSX.Element {
  useSuspenseImage(src)
  return (
    <img
      className={className || undefined}
      src={src}
      alt={altText}
      ref={imageRef}
      data-position={position}
      style={{
        display: 'block',
        height,
        width,
      }}
      draggable='false'
    />
  )
}

export function UpdateInlineImageDialog({
  open,
  activeEditor,
  nodeKey,
  onClose,
}: {
  open: boolean
  activeEditor: LexicalEditor
  nodeKey: NodeKey
  onClose: () => void
}): JSX.Element {
  const editorState = activeEditor.getEditorState()
  const node = editorState.read(() => $getNodeByKey(nodeKey) as InlineImageNode)
  const [showCaption, setShowCaption] = useState(node.getShowCaption())
  const [position, setPosition] = useState<Position>(node.getPosition())

  const handleShowCaptionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setShowCaption(e.target.checked)
  }

  const handlePositionChange = (e: SelectChangeEvent) => {
    setPosition(e.target.value as Position)
  }

  const handleOnConfirm = () => {
    const payload = {altText: node.getAltText(), position, showCaption}
    if (node) {
      activeEditor.update(() => {
        node.update(payload)
      })
    }
    onClose()
  }

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>{'Edit Image'}</DialogTitle>
      <DialogContent>
        <FormControl fullWidth sx={{marginTop: 1}}>
          <InputLabel id='demo-simple-select-label'>Position</InputLabel>
          <Select
            sx={{marginBottom: '1em', width: 250}}
            value={position}
            label='Position'
            name='position'
            id='position-select'
            onChange={handlePositionChange}
          >
            <MenuItem value='left'>Left</MenuItem>
            <MenuItem value='right'>Right</MenuItem>
            <MenuItem value='full'>Full Width</MenuItem>
          </Select>
          <FormControlLabel
            className='Input__wrapper'
            control={
              <Checkbox
                checked={showCaption}
                onChange={handleShowCaptionChange}
              />
            }
            label='Show Caption'
          />
        </FormControl>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => handleOnConfirm()}>Confirm</Button>
      </DialogActions>
    </Dialog>
  )
}

export default function InlineImageComponent({
  src,
  altText,
  nodeKey,
  width,
  height,
  showCaption,
  caption,
  position,
}: {
  altText: string
  caption: LexicalEditor
  height: 'inherit' | number
  nodeKey: NodeKey
  showCaption: boolean
  src: string
  width: 'inherit' | number
  position: Position
}): JSX.Element {
  const imageRef = useRef<null | HTMLImageElement>(null)
  const buttonRef = useRef<HTMLButtonElement | null>(null)
  const [isSelected, setSelected, clearSelection] =
    useLexicalNodeSelection(nodeKey)
  const [editor] = useLexicalComposerContext()
  const [selection, setSelection] = useState<BaseSelection | null>(null)
  const activeEditorRef = useRef<LexicalEditor | null>(null)
  const [openEditDialog, setOpenEditDialog] = useState(false)
  const [showEditIcon, setShowEditIcon] = useState(false)

  // eslint-disable-next-line
  const setDebounceShowEditIcon = useCallback(
    debounce((value: boolean) => {
      setShowEditIcon(value)
    }, 700),
    [],
  )

  const $onDelete = useCallback(
    (payload: KeyboardEvent) => {
      if (isSelected && $isNodeSelection($getSelection())) {
        const event: KeyboardEvent = payload
        event.preventDefault()
        const node = $getNodeByKey(nodeKey)
        if ($isInlineImageNode(node)) {
          node.remove()
          return true
        }
      }
      return false
    },
    [isSelected, nodeKey],
  )

  const $onEnter = useCallback(
    (event: KeyboardEvent) => {
      const latestSelection = $getSelection()
      const buttonElem = buttonRef.current
      if (
        isSelected &&
        $isNodeSelection(latestSelection) &&
        latestSelection.getNodes().length === 1
      ) {
        if (showCaption) {
          // Move focus into nested editor
          $setSelection(null)
          event.preventDefault()
          caption.focus()
          return true
        } else if (
          buttonElem !== null &&
          buttonElem !== document.activeElement
        ) {
          event.preventDefault()
          buttonElem.focus()
          return true
        }
      }
      return false
    },
    [caption, isSelected, showCaption],
  )

  const $onEscape = useCallback(
    (event: KeyboardEvent) => {
      if (
        activeEditorRef.current === caption ||
        buttonRef.current === event.target
      ) {
        $setSelection(null)
        editor.update(() => {
          setSelected(true)
          const parentRootElement = editor.getRootElement()
          if (parentRootElement !== null) {
            parentRootElement.focus()
          }
        })
        return true
      }
      return false
    },
    [caption, editor, setSelected],
  )

  useEffect(() => {
    let isMounted = true
    if (isSelected) {
      setDebounceShowEditIcon(isSelected)
    } else {
      setShowEditIcon(isSelected)
    }
    const unregister = mergeRegister(
      editor.registerUpdateListener(({editorState}) => {
        if (isMounted) {
          setSelection(editorState.read(() => $getSelection()))
        }
      }),
      editor.registerCommand(
        SELECTION_CHANGE_COMMAND,
        (_, activeEditor) => {
          activeEditorRef.current = activeEditor
          return false
        },
        COMMAND_PRIORITY_LOW,
      ),
      editor.registerCommand<MouseEvent>(
        CLICK_COMMAND,
        (payload) => {
          const event = payload
          if (event.target === imageRef.current) {
            if (event.shiftKey) {
              setSelected(!isSelected)
            } else {
              clearSelection()
              setSelected(true)
            }
            return true
          }

          return false
        },
        COMMAND_PRIORITY_LOW,
      ),
      editor.registerCommand(
        DRAGSTART_COMMAND,
        (event) => {
          if (event.target === imageRef.current) {
            // TODO This is just a temporary workaround for FF to behave like other browsers.
            // Ideally, this handles drag & drop too (and all browsers).
            event.preventDefault()
            return true
          }
          return false
        },
        COMMAND_PRIORITY_LOW,
      ),
      editor.registerCommand(
        KEY_DELETE_COMMAND,
        $onDelete,
        COMMAND_PRIORITY_LOW,
      ),
      editor.registerCommand(
        KEY_BACKSPACE_COMMAND,
        $onDelete,
        COMMAND_PRIORITY_LOW,
      ),
      editor.registerCommand(KEY_ENTER_COMMAND, $onEnter, COMMAND_PRIORITY_LOW),
      editor.registerCommand(
        KEY_ESCAPE_COMMAND,
        $onEscape,
        COMMAND_PRIORITY_LOW,
      ),
    )
    return () => {
      isMounted = false
      unregister()
    }
  }, [
    clearSelection,
    editor,
    isSelected,
    nodeKey,
    $onDelete,
    $onEnter,
    $onEscape,
    setSelected,
    setDebounceShowEditIcon,
  ])

  const draggable = isSelected && $isNodeSelection(selection)
  const isFocused = isSelected
  return (
    <Suspense fallback={null}>
      <>
        <span draggable={draggable}>
          {showEditIcon && (
            <IconButton
              className='image-edit-button'
              size={'small'}
              ref={buttonRef}
              onClick={() => setOpenEditDialog(true)}
            >
              <EditIcon />
            </IconButton>
          )}
          <LazyImage
            className={
              isFocused
                ? `focused ${$isNodeSelection(selection) ? 'draggable' : ''}`
                : null
            }
            src={src}
            altText={altText}
            imageRef={imageRef}
            width={width}
            height={height}
            position={position}
          />
        </span>
        {showCaption && (
          <span className='image-caption-container'>
            <LexicalNestedComposer initialEditor={caption}>
              <AutoFocusPlugin />
              <LinkPlugin />
              <RichTextPlugin
                contentEditable={
                  <ContentEditable className='InlineImageNode__contentEditable' />
                }
                placeholder={
                  <Placeholder className='InlineImageNode__placeholder'>
                    Enter a caption...
                  </Placeholder>
                }
                ErrorBoundary={LexicalErrorBoundary}
              />
            </LexicalNestedComposer>
          </span>
        )}
      </>
      <UpdateInlineImageDialog
        activeEditor={editor}
        nodeKey={nodeKey}
        open={openEditDialog}
        onClose={() => setOpenEditDialog(false)}
      />
    </Suspense>
  )
}
