import {useLexicalComposerContext} from '@lexical/react/LexicalComposerContext'
import {$wrapNodeInElement} from '@lexical/utils'
import 'katex/dist/katex.css'
import {
  $createParagraphNode,
  $insertNodes,
  $isRootOrShadowRoot,
  COMMAND_PRIORITY_EDITOR,
} from 'lexical'
import {useEffect} from 'react'

import {$createReferenceNode, ReferenceNode} from '../nodes/ReferenceNode'
import {
  INSERT_REFERENCE_COMMAND,
  ReferenceCommandPayload,
} from './ReferenceCreateCommand'

export default function ReferencePlugin(): JSX.Element | null {
  const [editor] = useLexicalComposerContext()

  useEffect(() => {
    if (!editor.hasNodes([ReferenceNode])) {
      throw new Error('ReferencePlugin: ReferenceNode not registered on editor')
    }

    return editor.registerCommand<ReferenceCommandPayload>(
      INSERT_REFERENCE_COMMAND,
      (payload) => {
        const {reference, inline} = payload
        const referenceNode = $createReferenceNode(reference, inline)

        $insertNodes([referenceNode])
        if ($isRootOrShadowRoot(referenceNode.getParentOrThrow())) {
          $wrapNodeInElement(referenceNode, $createParagraphNode).selectEnd()
        }

        return true
      },
      COMMAND_PRIORITY_EDITOR,
    )
  }, [editor])

  return null
}
