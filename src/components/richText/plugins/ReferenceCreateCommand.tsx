import {LexicalCommand, createCommand} from 'lexical'

export type ReferenceCommandPayload = {
  reference: string
  inline: boolean
}

export const INSERT_REFERENCE_COMMAND: LexicalCommand<ReferenceCommandPayload> =
  createCommand('INSERT_REFERENCE_COMMAND')
