import {LexicalCommand, createCommand} from 'lexical'

export type EquationCommandPayload = {
  equation: string
  inline: boolean
}

export const INSERT_EQUATION_COMMAND: LexicalCommand<EquationCommandPayload> =
  createCommand('INSERT_EQUATION_COMMAND')
