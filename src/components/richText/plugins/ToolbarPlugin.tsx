import {$isCodeNode, CODE_LANGUAGE_MAP} from '@lexical/code'
import {$isLinkNode, TOGGLE_LINK_COMMAND} from '@lexical/link'
import {$isListNode, ListNode} from '@lexical/list'
import {useLexicalComposerContext} from '@lexical/react/LexicalComposerContext'
import {$isHeadingNode} from '@lexical/rich-text'
import {
  $getSelectionStyleValueForProperty,
  $patchStyleText,
} from '@lexical/selection'
import {$isTableNode} from '@lexical/table'
import {
  $deleteTableColumn__EXPERIMENTAL,
  $deleteTableRow__EXPERIMENTAL,
  $insertTableColumn__EXPERIMENTAL,
  $insertTableRow__EXPERIMENTAL,
} from '@lexical/table'
import {
  $findMatchingParent,
  $getNearestNodeOfType,
  mergeRegister,
} from '@lexical/utils'
import AddIcon from '@mui/icons-material/Add'
import CodeIcon from '@mui/icons-material/Code'
import FormatAlignCenterIcon from '@mui/icons-material/FormatAlignCenter'
import FormatAlignJustifyIcon from '@mui/icons-material/FormatAlignJustify'
import FormatAlignLeftIcon from '@mui/icons-material/FormatAlignLeft'
import FormatAlignRightIcon from '@mui/icons-material/FormatAlignRight'
import FormatBoldIcon from '@mui/icons-material/FormatBold'
import FormatColorFillIcon from '@mui/icons-material/FormatColorFill'
import FormatColorTextIcon from '@mui/icons-material/FormatColorText'
import FormatItalicIcon from '@mui/icons-material/FormatItalic'
import FormatStrikethroughIcon from '@mui/icons-material/FormatStrikethrough'
import FormatUnderlinedIcon from '@mui/icons-material/FormatUnderlined'
import FunctionsIcon from '@mui/icons-material/Functions'
import ImageIcon from '@mui/icons-material/Image'
import LinkIcon from '@mui/icons-material/Link'
import RedoIcon from '@mui/icons-material/Redo'
import RemoveIcon from '@mui/icons-material/Remove'
import TableRowsIcon from '@mui/icons-material/TableRows'
import TextFormatIcon from '@mui/icons-material/TextFormat'
import UndoIcon from '@mui/icons-material/Undo'
import ViewColumnIcon from '@mui/icons-material/ViewColumn'
import {Divider, IconButton} from '@mui/material'
import {
  $getSelection,
  $isElementNode,
  $isRangeSelection,
  $isRootOrShadowRoot,
  CAN_REDO_COMMAND,
  CAN_UNDO_COMMAND,
  FORMAT_ELEMENT_COMMAND,
  FORMAT_TEXT_COMMAND,
  NodeKey,
  REDO_COMMAND,
  SELECTION_CHANGE_COMMAND,
  UNDO_COMMAND,
} from 'lexical'
import React, {useCallback, useEffect, useState} from 'react'

import Toolbar from '../../toolbar/Toolbar'
import ToolbarPanel from '../../toolbar/ToolbarPanel'
import AdditionalFormats from '../ui/AdditionalFormats'
import Alignments from '../ui/Alignments'
import {alignmentTypes} from '../ui/AlignmentsOptions'
import BlockFormats from '../ui/BlockFormats'
import {blockTypes, rootTypes} from '../ui/BlockFormatsOptions'
import DropdownColorPicker from '../ui/DropdownColorPicker'
import FontFamily from '../ui/FontFamily'
import FontSize from '../ui/FontSize'
import InsertNodes from '../ui/InsertNodes'
import SelectLanguage from '../ui/SelectLanguage'
import ComposedIcon from '../utils/composedIcon'
import {getSelectedNode} from '../utils/getSelectedNode'
import {sanitizeUrl} from '../utils/url'
import {INSERT_EQUATION_COMMAND} from './EquationCreateCommand'
import {InsertImageDialog} from './ImagesPlugin'

const LowPriority = 1

type ToolbarPluginProps = {
  withFormats?: boolean
  withFontFamily?: boolean
  withFontSize?: boolean
  withAlignments?: boolean
  withFontColor?: boolean
  withInsertNodes?: boolean
  withTableActions?: boolean
  actions?: React.ReactNode
}

export default function ToolbarPlugin({
  actions,
  withFormats,
  withFontFamily,
  withFontSize,
  withAlignments,
  withFontColor,
  withInsertNodes,
  withTableActions,
}: ToolbarPluginProps) {
  const [editor] = useLexicalComposerContext()
  const [canUndo, setCanUndo] = useState(false)
  const [canRedo, setCanRedo] = useState(false)
  const [isBold, setIsBold] = useState(false)
  const [isItalic, setIsItalic] = useState(false)
  const [isUnderline, setIsUnderline] = useState(false)
  const [isStrikethrough, setIsStrikethrough] = useState(false)
  const [blockType, setBlockType] =
    useState<keyof typeof blockTypes>('paragraph')
  const [rootType, setRootType] = useState<keyof typeof rootTypes>('root')
  const [isLink, setIsLink] = useState(false)
  const [isCode, setIsCode] = useState(false)
  const [selectedElementKey, setSelectedElementKey] = useState<NodeKey | null>(
    null,
  )
  const [codeLanguage, setCodeLanguage] = useState<string>('')
  const [fontSize, setFontSize] = useState<string>('')
  const [fontColor, setFontColor] = useState<string>('')
  const [backGroundColor, setBackGroundColor] = useState<string>('')
  const [fontFamily, setFontFamily] = useState<string | undefined>()
  const [alignment, setAlignment] = useState<keyof typeof alignmentTypes | ''>()
  const [openImageDialog, setOpenImageDialog] = useState(false)

  const $updateToolbar = useCallback(() => {
    const selection = $getSelection()
    if ($isRangeSelection(selection)) {
      // Update text format
      setIsBold(selection.hasFormat('bold'))
      setIsItalic(selection.hasFormat('italic'))
      setIsUnderline(selection.hasFormat('underline'))
      setIsStrikethrough(selection.hasFormat('strikethrough'))
      setIsCode(selection.hasFormat('code'))
      setFontColor($getSelectionStyleValueForProperty(selection, 'color'))
      setBackGroundColor(
        $getSelectionStyleValueForProperty(selection, 'background-color'),
      )
      setFontFamily(
        $getSelectionStyleValueForProperty(selection, 'font-family'),
      )
      setFontSize($getSelectionStyleValueForProperty(selection, 'font-size'))

      const node = getSelectedNode(selection)
      const parent = node.getParent()
      if ($isLinkNode(parent) || $isLinkNode(node)) {
        setIsLink(true)
      } else {
        setIsLink(false)
      }

      const tableNode = $findMatchingParent(node, $isTableNode)
      if ($isTableNode(tableNode)) {
        setRootType('table')
      } else {
        setRootType('root')
      }

      const anchorNode = selection.anchor.getNode()
      let element =
        anchorNode.getKey() === 'root'
          ? anchorNode
          : $findMatchingParent(anchorNode, (e) => {
              const parent = e.getParent()
              return parent !== null && $isRootOrShadowRoot(parent)
            })
      if (element === null) {
        element = anchorNode.getTopLevelElementOrThrow()
      }
      const elementKey = element.getKey()
      const elementDOM = editor.getElementByKey(elementKey)
      if (elementDOM !== null) {
        setSelectedElementKey(elementKey)
        if ($isListNode(element)) {
          const parentList = $getNearestNodeOfType<ListNode>(
            anchorNode,
            ListNode,
          )
          const type = parentList
            ? parentList.getListType()
            : element.getListType()
          setBlockType(type)
        } else {
          const type = $isHeadingNode(element)
            ? element.getTag()
            : element.getType()
          if (type in blockTypes) {
            setBlockType(type as keyof typeof blockTypes)
          }
          if ($isCodeNode(element)) {
            const language =
              element.getLanguage() as keyof typeof CODE_LANGUAGE_MAP
            setCodeLanguage(
              language ? CODE_LANGUAGE_MAP[language] || language : '',
            )
            return
          }
        }
      }

      let matchingParent
      if ($isLinkNode(parent)) {
        matchingParent = $findMatchingParent(
          node,
          (parentNode) => $isElementNode(parentNode) && !parentNode.isInline(),
        )
      }
      setAlignment(
        $isElementNode(matchingParent)
          ? matchingParent.getFormatType()
          : $isElementNode(node)
          ? node.getFormatType()
          : (parent?.getFormatType() as keyof typeof alignmentTypes | ''),
      )
    }
  }, [editor])

  useEffect(() => {
    return mergeRegister(
      editor.registerUpdateListener(({editorState}) => {
        editorState.read(() => {
          $updateToolbar()
        })
      }),
      editor.registerCommand(
        SELECTION_CHANGE_COMMAND,
        () => {
          $updateToolbar()
          return false
        },
        LowPriority,
      ),
      editor.registerCommand(
        CAN_UNDO_COMMAND,
        (payload) => {
          setCanUndo(payload)
          return false
        },
        LowPriority,
      ),
      editor.registerCommand(
        CAN_REDO_COMMAND,
        (payload) => {
          setCanRedo(payload)
          return false
        },
        LowPriority,
      ),
    )
  }, [editor, $updateToolbar])

  const applyStyleText = useCallback(
    (styles: Record<string, string>) => {
      editor.update(() => {
        const selection = $getSelection()
        if (selection !== null) {
          $patchStyleText(selection, styles)
        }
      }, {})
    },
    [editor],
  )

  const insertLink = useCallback(() => {
    if (!isLink) {
      editor.dispatchCommand(TOGGLE_LINK_COMMAND, {
        url: sanitizeUrl('https://'),
      })
    } else {
      editor.dispatchCommand(TOGGLE_LINK_COMMAND, null)
    }
  }, [editor, isLink])

  return (
    <Toolbar actions={actions}>
      <IconButton
        disabled={!canUndo}
        onClick={() => editor.dispatchCommand(UNDO_COMMAND, undefined)}
        size='small'
        aria-label='Undo'
      >
        <UndoIcon />
      </IconButton>
      <IconButton
        disabled={!canRedo}
        onClick={() => editor.dispatchCommand(REDO_COMMAND, undefined)}
        size='small'
        aria-label='Redo'
      >
        <RedoIcon />
      </IconButton>
      <Divider orientation='vertical' flexItem variant='middle' />
      <BlockFormats editor={editor} blockType={blockType} />
      {blockType !== 'code' && (
        <>
          {withFormats && !(rootType === 'table' && withTableActions) && (
            <ToolbarPanel index={0}>
              <Divider orientation='vertical' flexItem variant='middle' />
              <ToolbarPanel index={2} icon={<TextFormatIcon />}>
                <IconButton
                  onClick={() =>
                    editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'bold')
                  }
                  color={isBold ? 'primary' : 'default'}
                  size='small'
                  aria-label='Format Bold'
                >
                  <FormatBoldIcon />
                </IconButton>
                <IconButton
                  onClick={() =>
                    editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'italic')
                  }
                  color={isItalic ? 'primary' : 'default'}
                  size='small'
                  aria-label='Format Italics'
                >
                  <FormatItalicIcon />
                </IconButton>
                <IconButton
                  onClick={() =>
                    editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'underline')
                  }
                  color={isUnderline ? 'primary' : 'default'}
                  size='small'
                  aria-label='Format Underline'
                >
                  <FormatUnderlinedIcon />
                </IconButton>
                <IconButton
                  onClick={() =>
                    editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'strikethrough')
                  }
                  color={isStrikethrough ? 'primary' : 'default'}
                  size='small'
                  aria-label='Format Strikethrough'
                >
                  <FormatStrikethroughIcon />
                </IconButton>
                <AdditionalFormats editor={editor} />
              </ToolbarPanel>
            </ToolbarPanel>
          )}
          {withTableActions && rootType === 'table' && (
            <ToolbarPanel index={0}>
              <IconButton
                onClick={() => {
                  editor.update(() => {
                    $insertTableColumn__EXPERIMENTAL(true)
                  })
                }}
                size='small'
              >
                <ComposedIcon
                  position='bottom-end'
                  size='small'
                  icon={<ViewColumnIcon />}
                  extraIcon={<AddIcon />}
                />
              </IconButton>
              <IconButton
                onClick={() => {
                  editor.update(() => {
                    $deleteTableColumn__EXPERIMENTAL()
                  })
                }}
                size='small'
              >
                <ComposedIcon
                  position='bottom-end'
                  size='small'
                  icon={<ViewColumnIcon />}
                  extraIcon={<RemoveIcon />}
                />
              </IconButton>
              <IconButton
                onClick={() => {
                  editor.update(() => {
                    $insertTableRow__EXPERIMENTAL(true)
                  })
                }}
                size='small'
              >
                <ComposedIcon
                  position='bottom-end'
                  size='small'
                  icon={<TableRowsIcon />}
                  extraIcon={<AddIcon />}
                />
              </IconButton>
              <IconButton
                onClick={() => {
                  editor.update(() => {
                    $deleteTableRow__EXPERIMENTAL()
                  })
                }}
                size='small'
              >
                <ComposedIcon
                  position='bottom-end'
                  size='small'
                  icon={<TableRowsIcon />}
                  extraIcon={<RemoveIcon />}
                />
              </IconButton>
            </ToolbarPanel>
          )}
          {(withFontFamily || withFontSize) && (
            <Divider orientation='vertical' flexItem variant='middle' />
          )}
          {withFontSize && (
            <FontSize
              fontSize={parseInt(fontSize, 10)}
              onChange={(fontSize: number | undefined) =>
                applyStyleText({
                  'font-size': fontSize ? `${fontSize}px` : '15px',
                })
              }
            />
          )}
          {withFontFamily && (
            <FontFamily
              fontFamily={fontFamily}
              onChange={(fontFamily) =>
                applyStyleText({'font-family': fontFamily || 'Arial'})
              }
            />
          )}
          {withAlignments && (
            <ToolbarPanel index={1}>
              <ToolbarPanel
                index={3}
                render={
                  <Alignments
                    editor={editor}
                    alignment={alignment || 'left'}
                    onChange={(alignment) => setAlignment(alignment)}
                  />
                }
              >
                <Divider orientation='vertical' flexItem variant='middle' />
                <IconButton
                  onClick={() => {
                    editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'left')
                    setAlignment('left')
                  }}
                  color={alignment == 'left' ? 'primary' : 'default'}
                  size={'small'}
                  aria-label='Left Align'
                >
                  <FormatAlignLeftIcon />
                </IconButton>
                <IconButton
                  onClick={() => {
                    editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'center')
                    setAlignment('center')
                  }}
                  color={alignment == 'center' ? 'primary' : 'default'}
                  size={'small'}
                  aria-label='Center Align'
                >
                  <FormatAlignCenterIcon />
                </IconButton>
                <IconButton
                  onClick={() => {
                    editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'right')
                    setAlignment('right')
                  }}
                  color={alignment == 'right' ? 'primary' : 'default'}
                  size={'small'}
                  aria-label='Right Align'
                >
                  <FormatAlignRightIcon />
                </IconButton>
                <IconButton
                  onClick={() => {
                    editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'justify')
                    setAlignment('justify')
                  }}
                  color={alignment == 'justify' ? 'primary' : 'default'}
                  size={'small'}
                  aria-label='Justify Align'
                >
                  <FormatAlignJustifyIcon />
                </IconButton>
              </ToolbarPanel>
            </ToolbarPanel>
          )}
          <ToolbarPanel index={4}>
            {withFontColor && (
              <>
                <Divider orientation='vertical' flexItem variant='middle' />
                <DropdownColorPicker
                  title={'Font Color'}
                  color={fontColor}
                  onChange={(color) => applyStyleText({color: color})}
                >
                  <FormatColorTextIcon />
                </DropdownColorPicker>
                <DropdownColorPicker
                  title={'Background Color'}
                  color={backGroundColor}
                  onChange={(color) =>
                    applyStyleText({'background-color': color})
                  }
                >
                  <FormatColorFillIcon />
                </DropdownColorPicker>
              </>
            )}
            <IconButton
              onClick={insertLink}
              color={isLink ? 'primary' : 'default'}
              size='small'
              aria-label='Format Link'
            >
              <LinkIcon />
            </IconButton>
            <IconButton
              onClick={() =>
                editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'code')
              }
              color={isCode ? 'primary' : 'default'}
              size='small'
              aria-label='Format Code'
            >
              <CodeIcon />
            </IconButton>
            <IconButton
              onClick={() => {
                setOpenImageDialog(true)
              }}
              size='small'
            >
              <ImageIcon />
            </IconButton>
            <IconButton
              onClick={() => {
                editor.dispatchCommand(INSERT_EQUATION_COMMAND, {
                  equation: 'y = e^x',
                  inline: true,
                })
              }}
              size='small'
            >
              <FunctionsIcon />
            </IconButton>
          </ToolbarPanel>
        </>
      )}
      {blockType === 'code' && (
        <SelectLanguage
          editor={editor}
          language={codeLanguage}
          elementKey={selectedElementKey}
        />
      )}
      {blockType !== 'code' && withInsertNodes && (
        <>
          <Divider orientation='vertical' flexItem variant='middle' />
          <InsertNodes editor={editor} />
        </>
      )}
      <InsertImageDialog
        open={openImageDialog}
        activeEditor={editor}
        onClose={() => setOpenImageDialog(false)}
      />
    </Toolbar>
  )
}
