import {SxProps} from '@mui/material'
import {EditorThemeClasses} from 'lexical'
import _ from 'lodash'

import './theme.css'

type ThemeObject<T> = {[key: string]: T}

// eslint-disable-next-line @typescript-eslint/no-empty-object-type
interface ThemeNode extends ThemeObject<ThemeNode | SxProps> {}

/**
 * A specification for the theme of the editor. We convert this spec into
 * a lexical theme. Those only assign class names to the nodes. We also convert
 * this to a sx object for the content editable that assigns specific styles
 * to the class names.
 *
 * There is a limitation in the current implementation. The style objects
 * must not comprise solely of values that are objects. Otherwise, we cannot
 * distinguish between a style object and a nested node.
 */
const theme: EditorThemeClasses = {
  autocomplete: 'editor_autocomplete',
  blockCursor: 'editor_blockCursor',
  characterLimit: 'editor_characterLimit',
  code: 'editor_code',
  codeHighlight: {
    atrule: 'editor_tokenAttr',
    attr: 'editor_tokenAttr',
    boolean: 'editor_tokenProperty',
    builtin: 'editor_tokenSelector',
    cdata: 'editor_tokenComment',
    char: 'editor_tokenSelector',
    class: 'editor_tokenFunction',
    className: 'editor_tokenFunction',
    comment: 'editor_tokenComment',
    constant: 'editor_tokenProperty',
    deleted: 'editor_tokenProperty',
    doctype: 'editor_tokenComment',
    entity: 'editor_tokenOperator',
    function: 'editor_tokenFunction',
    important: 'editor_tokenVariable',
    inserted: 'editor_tokenSelector',
    keyword: 'editor_tokenAttr',
    namespace: 'editor_tokenVariable',
    number: 'editor_tokenProperty',
    operator: 'editor_tokenOperator',
    prolog: 'editor_tokenComment',
    property: 'editor_tokenProperty',
    punctuation: 'editor_tokenPunctuation',
    regex: 'editor_tokenVariable',
    selector: 'editor_tokenSelector',
    string: 'editor_tokenSelector',
    symbol: 'editor_tokenProperty',
    tag: 'editor_tokenProperty',
    url: 'editor_tokenOperator',
    variable: 'editor_tokenVariable',
  },
  embedBlock: {
    base: 'editor_embedBlock',
    focus: 'editor_embedBlockFocus',
  },
  hashtag: 'editor_hashtag',
  heading: {
    h1: 'editor_h1',
    h2: 'editor_h2',
    h3: 'editor_h3',
    h4: 'editor_h4',
    h5: 'editor_h5',
    h6: 'editor_h6',
  },
  hr: 'editor_hr',
  image: 'editor-image',
  indent: 'editor_indent',
  inlineImage: 'inline-editor-image',
  layoutContainer: 'editor_layoutContainer',
  layoutItem: 'editor_layoutItem',
  link: 'editor_link',
  list: {
    checklist: 'editor_checklist',
    listitem: 'editor_listItem',
    listitemChecked: 'editor_listItemChecked',
    listitemUnchecked: 'editor_listItemUnchecked',
    nested: {
      listitem: 'editor_nestedListItem',
    },
    olDepth: [
      'editor_ol1',
      'editor_ol2',
      'editor_ol3',
      'editor_ol4',
      'editor_ol5',
    ],
    ul: 'editor_ul',
  },
  ltr: 'editor_ltr',
  mark: 'editor_mark',
  markOverlap: 'editor_markOverlap',
  paragraph: 'editor_paragraph',
  quote: 'editor_quote',
  rtl: 'editor_rtl',
  table: 'editor_table',
  tableAddColumns: 'editor_tableAddColumns',
  tableAddRows: 'editor_tableAddRows',
  tableCell: 'editor_tableCell',
  tableCellActionButton: 'editor_tableCellActionButton',
  tableCellActionButtonContainer: 'editor_tableCellActionButtonContainer',
  tableCellEditing: 'editor_tableCellEditing',
  tableCellHeader: 'editor_tableCellHeader',
  tableCellPrimarySelected: 'editor_tableCellPrimarySelected',
  tableCellResizer: 'editor_tableCellResizer',
  tableCellSelected: 'editor_tableCellSelected',
  tableCellSortedIndicator: 'editor_tableCellSortedIndicator',
  tableResizeRuler: 'editor_tableCellResizeRuler',
  tableSelected: 'editor_tableSelected',
  tableSelection: 'editor_tableSelection',
  text: {
    bold: 'editor_textBold',
    code: 'editor_textCode',
    italic: 'editor_textItalic',
    strikethrough: 'editor_textStrikethrough',
    subscript: 'editor_textSubscript',
    superscript: 'editor_textSuperscript',
    underline: 'editor_textUnderline',
    underlineStrikethrough: 'editor_textUnderlineStrikethrough',
  },
}

function generateContentEditableSx() {
  const result: {[key: string]: SxProps} = {}
  const traverse: (node: ThemeNode, name: string) => void = (node, name) => {
    const isSxProps = Object.values(node).some(
      (value) => typeof value !== 'object',
    )
    if (isSxProps) {
      result[`& .${name}`] = node
    } else {
      for (const key in node) {
        const value = node[key]
        const childName = `${name}-${key}`
        if (typeof value === 'object' && !_.isEmpty(value)) {
          traverse(value as ThemeNode, childName)
        }
      }
    }
  }
  traverse(theme, 'editor')
  return result
}

export const contentEditableSx = generateContentEditableSx()

export default theme
