import {fireEvent, render, screen, within} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {useDebounce} from 'react-use'
import {vi} from 'vitest'

import * as useEditDebounce from '../../hooks/useEditDebounce'
import RichTextEditor from './RichTextEditor'

describe('RichTextEditor', async () => {
  vi.spyOn(useEditDebounce, 'default').mockImplementation((fn, deps) => {
    useDebounce(fn, 0, deps)
  })

  it('renders the editor with initial value', async () => {
    render(<RichTextEditor initialValue='<p>Initial Value</p>' />)
    expect(await screen.findByText('Initial Value')).toBeInTheDocument()
  })
  it('shows placeholder when not focused', async () => {
    render(<RichTextEditor placeholder='test_placeholder' />)
    expect(await screen.findByText(/test_placeholder/)).toBeInTheDocument()
    const editor = screen.getByRole('textbox')
    await editor.blur()

    expect(editor).toBeInTheDocument()
    expect(screen.queryByText(/test_placeholder/)).toBeInTheDocument()

    fireEvent.focus(editor)
    expect(screen.queryByText(/test_placeholder/)).not.toBeInTheDocument()
  })
  it('does not call onChange when not edited', async () => {
    const handleChange = vi.fn()
    const initialValue = 'initial value'
    render(
      <RichTextEditor initialValue={initialValue} onChange={handleChange} />,
    )
    expect(await screen.findByText(initialValue)).toBeInTheDocument()
    expect(handleChange).not.toHaveBeenCalled()
  })
  it.each([
    ['initial value', 'initial value'],
    // TODO: This test is failing. It is probably not related to the editor
    // itself but to the way the test is written. The keyboard event seems not
    // to reach the editor, if it does not already have a value.
    // ['no initial value', undefined],
  ])(
    'calls onChange when edited with %s',
    async (description, initialValue) => {
      const handleChange = vi.fn()
      render(
        <RichTextEditor
          placeholder='test_placeholder'
          initialValue={initialValue ? `<p>${initialValue}</p>` : undefined}
          onChange={handleChange}
        />,
      )
      if (initialValue) {
        expect(await screen.findByText(initialValue)).toBeInTheDocument()
      } else {
        expect(await screen.findByText('test_placeholder')).toBeInTheDocument()
      }
      const editor = screen.getByRole('textbox')
      await userEvent.tripleClick(editor)
      await userEvent.keyboard('typed')
      expect(handleChange).toHaveBeenCalledWith(
        `<p class="editor_paragraph" dir="ltr"><span style="white-space: pre-wrap;">typed</span></p>`,
      )
      expect(screen.queryByText(/typed/)).toBeInTheDocument()
    },
  )
  it('Link format', async () => {
    const handleChange = vi.fn()
    const initialValue = `<p>abc</p>`
    render(
      <RichTextEditor initialValue={initialValue} onChange={handleChange} />,
    )
    const editor = await screen.findByRole('textbox')
    await userEvent.dblClick(editor)

    const linkIconButtons = screen.getAllByRole('button', {
      name: /Format Link/i,
    })
    expect(linkIconButtons).toHaveLength(2)
    const linkIconButton = linkIconButtons[0]

    await userEvent.click(linkIconButton)

    expect(handleChange).toHaveBeenCalledWith(
      `<p class="editor_paragraph" dir="ltr"><a href="https://" rel="noreferrer" class="editor_link"><span style="white-space: pre-wrap;">abc</span></a></p>`,
    )

    await userEvent.click(linkIconButton)

    expect(handleChange).toHaveBeenCalledWith(
      `<p class="editor_paragraph" dir="ltr"><span style="white-space: pre-wrap;">abc</span></p>`,
    )
  })
  it('Link editor', async () => {
    const handleChange = vi.fn()
    const initialValue = `<p class="editor_paragraph" dir="ltr"><a href="https://" rel="noreferrer" class="editor_link"><span style="white-space: pre-wrap;">abc</span></a></p>`
    render(
      <RichTextEditor initialValue={initialValue} onChange={handleChange} />,
    )
    const editor = await screen.findByRole('textbox')
    await userEvent.click(editor)

    const linkEditor = screen.getByTestId('float-link-editor')

    const editLink = within(linkEditor).getByRole('button', {
      name: /Edit Link/i,
    })
    within(linkEditor).getByRole('button', {
      name: /Remove Link/i,
    })
    await userEvent.click(editLink)

    const input = within(linkEditor).getByRole('textbox')
    within(linkEditor).getByRole('button', {
      name: /Cancel/i,
    })
    const submitButton = within(linkEditor).getByRole('button', {
      name: /Submit/i,
    })

    await userEvent.click(input)
    await userEvent.keyboard('google.com')
    await userEvent.click(submitButton)

    expect(handleChange).toHaveBeenCalledWith(
      `<p class="editor_paragraph" dir="ltr"><a href="https://google.com" rel="noreferrer" class="editor_link"><span style="white-space: pre-wrap;">abc</span></a></p>`,
    )
  })
  it('Bullet Block', async () => {
    const handleChange = vi.fn()
    const initialValue = `<p>abc</p>`
    render(
      <RichTextEditor initialValue={initialValue} onChange={handleChange} />,
    )
    const editor = await screen.findByRole('textbox')
    await userEvent.dblClick(editor)

    const blockTypesButton = screen.getByTestId('block-types-button')
    await userEvent.click(blockTypesButton)
    expect(screen.queryByTestId('block-types-menu')).toBeInTheDocument()

    await userEvent.click(
      within(screen.getByTestId('block-types-menu')).getByText('Bullet List'),
    )

    expect(handleChange).toHaveBeenCalledWith(
      `<ul class="editor_ul"><li value="1" class="editor_listItem"><span style="white-space: pre-wrap;">abc</span></li></ul>`,
    )

    expect(screen.queryByTestId('block-types-menu')).not.toBeInTheDocument()
    await userEvent.click(blockTypesButton)

    expect(screen.queryByTestId('block-types-menu')).toBeInTheDocument()
    await userEvent.click(
      within(screen.getByTestId('block-types-menu')).getByText('Bullet List'),
    )

    expect(handleChange).toHaveBeenCalledWith(
      `<p class="editor_paragraph" dir="ltr"><span style="white-space: pre-wrap;">abc</span></p>`,
    )
  })
  it('Number Block', async () => {
    const handleChange = vi.fn()
    const initialValue = `<p>abc</p>`
    render(
      <RichTextEditor initialValue={initialValue} onChange={handleChange} />,
    )
    const editor = await screen.findByRole('textbox')
    await userEvent.dblClick(editor)

    const blockTypesButton = screen.getByTestId('block-types-button')
    await userEvent.click(blockTypesButton)
    expect(screen.queryByTestId('block-types-menu')).toBeInTheDocument()
    await userEvent.click(
      within(screen.getByTestId('block-types-menu')).getByText('Numbered List'),
    )

    expect(handleChange).toHaveBeenCalledWith(
      `<ol class="editor_ol1"><li value="1" class="editor_listItem"><span style="white-space: pre-wrap;">abc</span></li></ol>`,
    )

    expect(screen.queryByTestId('block-types-menu')).not.toBeInTheDocument()
    await userEvent.click(blockTypesButton)

    expect(screen.queryByTestId('block-types-menu')).toBeInTheDocument()
    await userEvent.click(
      within(screen.getByTestId('block-types-menu')).getByText('Numbered List'),
    )

    expect(handleChange).toHaveBeenCalledWith(
      `<p class="editor_paragraph" dir="ltr"><span style="white-space: pre-wrap;">abc</span></p>`,
    )
  })
  it('Quote Block', async () => {
    const handleChange = vi.fn()
    const initialValue = `<p>abc</p>`
    render(
      <RichTextEditor initialValue={initialValue} onChange={handleChange} />,
    )
    const editor = await screen.findByRole('textbox')
    await userEvent.dblClick(editor)

    const blockTypesButton = screen.getByTestId('block-types-button')
    await userEvent.click(blockTypesButton)
    expect(screen.queryByTestId('block-types-menu')).toBeInTheDocument()
    await userEvent.click(
      within(screen.getByTestId('block-types-menu')).getByText('Quote'),
    )

    expect(handleChange).toHaveBeenCalledWith(
      `<blockquote class="editor_quote" dir="ltr"><span style="white-space: pre-wrap;">abc</span></blockquote>`,
    )

    expect(screen.queryByTestId('block-types-menu')).not.toBeInTheDocument()
    await userEvent.click(blockTypesButton)

    expect(screen.queryByTestId('block-types-menu')).toBeInTheDocument()
    await userEvent.click(
      within(screen.getByTestId('block-types-menu')).getByText('Quote'),
    )

    expect(handleChange).toHaveBeenCalledWith(
      `<p class="editor_paragraph" dir="ltr"><span style="white-space: pre-wrap;">abc</span></p>`,
    )
  })
  it('Code Block', async () => {
    const handleChange = vi.fn()
    const initialValue = `<p>abc</p>`
    render(
      <RichTextEditor initialValue={initialValue} onChange={handleChange} />,
    )
    const editor = screen.getByRole('textbox')
    await userEvent.dblClick(editor)
    const blockTypesButton = screen.getByTestId('block-types-button')
    await userEvent.click(blockTypesButton)
    expect(screen.queryByTestId('block-types-menu')).toBeInTheDocument()
    await userEvent.click(
      within(screen.getByTestId('block-types-menu')).getByText('Code'),
    )

    expect(handleChange).toHaveBeenCalledWith(
      `<pre class="editor_code" spellcheck="false" data-language="javascript" data-highlight-language="javascript"><span style="white-space: pre-wrap;">abc</span></pre>`,
    )
    expect(screen.queryByTestId('block-types-menu')).not.toBeInTheDocument()

    await userEvent.click(blockTypesButton)

    expect(screen.queryByTestId('block-types-menu')).toBeInTheDocument()
    await userEvent.click(
      within(screen.getByTestId('block-types-menu')).getByText('Code'),
    )

    expect(handleChange).toHaveBeenCalledWith(
      `<p class="editor_paragraph" dir="ltr"><span style="white-space: pre-wrap;">abc</span></p>`,
    )
  })
  it('Code Highlight', async () => {
    const handleChange = vi.fn()
    const initialValue = `<p>import library</p>`
    render(
      <RichTextEditor initialValue={initialValue} onChange={handleChange} />,
    )

    const editor = screen.getByRole('textbox')
    await userEvent.tripleClick(editor)
    const blockTypesButton = screen.getByTestId('block-types-button')
    await userEvent.click(blockTypesButton)
    expect(screen.queryByTestId('block-types-menu')).toBeInTheDocument()
    await userEvent.click(
      within(screen.getByTestId('block-types-menu')).getByText('Code'),
    )
    expect(handleChange).toHaveBeenCalledWith(
      `<pre class="editor_code" spellcheck="false" data-language="javascript" data-highlight-language="javascript"><span class="editor_tokenAttr" style="white-space: pre-wrap;">import</span><span style="white-space: pre-wrap;"> library</span></pre>`,
    )

    const codeLanguageIconButton = screen.getByRole('button', {
      name: /Code Language/i,
    })
    await userEvent.click(codeLanguageIconButton)
    const markdownLanguage = screen.getByTestId(/Markdown/i)
    await userEvent.click(markdownLanguage)
    expect(handleChange).toHaveBeenCalledWith(
      `<pre class="editor_code" spellcheck="false" data-language="markdown" data-highlight-language="markdown"><span style="white-space: pre-wrap;">import library</span></pre>`,
    )
  })
  it('Undo and Redo', async () => {
    const handleChange = vi.fn()
    const initialValue = `<p>a</p>`
    render(
      <RichTextEditor initialValue={initialValue} onChange={handleChange} />,
    )
    const editor = screen.getByRole('textbox')
    await userEvent.tripleClick(editor)
    await userEvent.keyboard('ab')
    await userEvent.keyboard('c')
    expect(handleChange).toHaveBeenCalledWith(
      `<p class="editor_paragraph" dir="ltr"><span style="white-space: pre-wrap;">abc</span></p>`,
    )

    const undoIconButton = screen.getByRole('button', {
      name: /Undo/i,
    })
    handleChange.mockClear()
    await userEvent.click(undoIconButton)
    expect(handleChange).toHaveBeenCalledWith(
      '<p class="editor_paragraph" dir="ltr"><span style="white-space: pre-wrap;">ab</span></p>',
    )

    const redoIconButton = screen.getByRole('button', {
      name: /Redo/i,
    })
    handleChange.mockClear()
    await userEvent.click(redoIconButton)
    expect(handleChange).toHaveBeenCalledWith(
      '<p class="editor_paragraph" dir="ltr"><span style="white-space: pre-wrap;">abc</span></p>',
    )
  })
  it.each([
    ['h1', '# ', `<h1 class="editor_h1"><br></h1>`],
    ['h2', '## ', `<h2 class="editor_h2"><br></h2>`],
    ['h3', '### ', `<h3 class="editor_h3"><br></h3>`],
    [
      'bullet',
      '- ',
      `<ul class="editor_ul"><li value="1" class="editor_listItem"></li></ul>`,
    ],
    [
      'number',
      '1. ',
      `<ol class="editor_ol1"><li value="1" class="editor_listItem"></li></ol>`,
    ],
    [
      'equation',
      '$x^2$',
      `<p class="editor_paragraph"><span data-lexical-equation="eF4y" data-lexical-inline="true"><span class="katex"><span class="katex-html" aria-hidden="true"><span class="base"><span class="strut" style="height: 0.8141em;"></span><span class="mord"><span class="mord mathnormal">x</span><span class="msupsub"><span class="vlist-t"><span class="vlist-r"><span class="vlist" style="height: 0.8141em;"><span class="" style="top: -3.063em; margin-right: 0.05em;"><span class="pstrut" style="height: 2.7em;"></span><span class="sizing reset-size6 size3 mtight"><span class="mord mtight">2</span></span></span></span></span></span></span></span></span></span></span></span></p>`,
    ],
  ])('Markdown shortcut for %s', async (name, shortcut, html) => {
    const handleChange = vi.fn()
    const initialValue = `<p>a</p>`
    render(
      <RichTextEditor initialValue={initialValue} onChange={handleChange} />,
    )
    const editor = await screen.findByRole('textbox')
    await userEvent.dblClick(editor)
    await userEvent.keyboard(shortcut)

    expect(handleChange).toHaveBeenCalledWith(html)
  })
  it.each([
    [
      'Horizontal Rule',
      `<p class="editor_paragraph" dir="ltr"><span style="white-space: pre-wrap;">a</span></p><hr><p class="editor_paragraph"><br></p>`,
    ],
    [
      'Inline Equation',
      `<p class="editor_paragraph"><span data-lexical-equation="eSA9IGVeeA==" data-lexical-inline="true"><span class="katex"><span class="katex-html" aria-hidden="true"><span class="base"><span class="strut" style="height: 0.625em; vertical-align: -0.1944em;"></span><span style="margin-right: 0.0359em;" class="mord mathnormal">y</span><span class="mspace" style="margin-right: 0.2778em;"></span><span class="mrel">=</span><span class="mspace" style="margin-right: 0.2778em;"></span></span><span class="base"><span class="strut" style="height: 0.6644em;"></span><span class="mord"><span class="mord mathnormal">e</span><span class="msupsub"><span class="vlist-t"><span class="vlist-r"><span class="vlist" style="height: 0.6644em;"><span class="" style="top: -3.063em; margin-right: 0.05em;"><span class="pstrut" style="height: 2.7em;"></span><span class="sizing reset-size6 size3 mtight"><span class="mord mathnormal mtight">x</span></span></span></span></span></span></span></span></span></span></span></span></p>`,
    ],
  ])('Insert %s', async (name, html) => {
    const handleChange = vi.fn()
    const initialValue = `<p>a</p>`
    render(
      <RichTextEditor initialValue={initialValue} onChange={handleChange} />,
    )
    const editor = screen.getByRole('textbox')
    await userEvent.tripleClick(editor)
    const insertIconButton = screen.getByRole('button', {
      name: /Insert/i,
    })

    await userEvent.click(insertIconButton)
    await userEvent.click(screen.getByText(name))
    expect(handleChange).toHaveBeenCalledWith(html)
  })
  it.each([
    [
      'Insert row above',
      `<table class="editor_table"><colgroup><col><col><col></colgroup><tbody><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th></tr><tr><td class="editor_tableCell" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start;"><p class="editor_paragraph"><br></p></td><td class="editor_tableCell" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start;"><p class="editor_paragraph"><br></p></td><td class="editor_tableCell" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start;"><p class="editor_paragraph"><br></p></td></tr><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><td style="width: 75px; border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell"><p class="editor_paragraph" style="text-align: start;"><br></p></td><td style="width: 75px; border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell"><p class="editor_paragraph" style="text-align: start;"><br></p></td></tr></tbody></table>`,
    ],
    [
      'Insert row below',
      `<table class="editor_table"><colgroup><col><col><col></colgroup><tbody><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th></tr><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><td style="width: 75px; border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell"><p class="editor_paragraph" style="text-align: start;"><br></p></td><td style="width: 75px; border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell"><p class="editor_paragraph" style="text-align: start;"><br></p></td></tr><tr><td class="editor_tableCell" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start;"><p class="editor_paragraph"><br></p></td><td class="editor_tableCell" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start;"><p class="editor_paragraph"><br></p></td><td class="editor_tableCell" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start;"><p class="editor_paragraph"><br></p></td></tr></tbody></table>`,
    ],
    [
      'Insert column left',
      `<table class="editor_table"><colgroup><col><col><col><col></colgroup><tbody><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th class="editor_tableCell editor_tableCellHeader" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start; background-color: rgb(242, 243, 245);"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th></tr><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><td style="width: 75px; border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell"><p class="editor_paragraph" style="text-align: start;"><br></p></td><td class="editor_tableCell" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start;"><p class="editor_paragraph"><br></p></td><td style="width: 75px; border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell"><p class="editor_paragraph" style="text-align: start;"><br></p></td></tr></tbody></table>`,
    ],
    [
      'Insert column right',
      `<table class="editor_table"><colgroup><col><col><col><col></colgroup><tbody><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th class="editor_tableCell editor_tableCellHeader" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start; background-color: rgb(242, 243, 245);"><p class="editor_paragraph"><br></p></th></tr><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><td style="width: 75px; border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell"><p class="editor_paragraph" style="text-align: start;"><br></p></td><td style="width: 75px; border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell"><p class="editor_paragraph" style="text-align: start;"><br></p></td><td class="editor_tableCell" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start;"><p class="editor_paragraph"><br></p></td></tr></tbody></table>`,
    ],
    [
      'Delete column',
      `<table class="editor_table"><colgroup><col><col></colgroup><tbody><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th></tr><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><td style="width: 75px; border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell"><p class="editor_paragraph" style="text-align: start;"><br></p></td></tr></tbody></table>`,
    ],
    [
      'Delete row',
      `<table class="editor_table"><colgroup><col><col><col></colgroup><tbody><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th></tr></tbody></table>`,
    ],
    [
      'Add row header',
      `<table class="editor_table"><colgroup><col><col><col></colgroup><tbody><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th></tr><tr><td style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell"><p class="editor_paragraph"><br></p></td><th style="width: 75px; border: 1px solid black; vertical-align: top; text-align: start; background-color: rgb(242, 243, 245);" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph" style="text-align: start;"><br></p></th><th style="width: 75px; border: 1px solid black; vertical-align: top; text-align: start; background-color: rgb(242, 243, 245);" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph" style="text-align: start;"><br></p></th></tr></tbody></table>`,
    ],
    [
      'Add column header',
      `<table class="editor_table"><colgroup><col><col><col></colgroup><tbody><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th></tr><tr><th style="width: 75px; background-color: rgb(242, 243, 245); border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph"><br></p></th><td style="width: 75px; border: 1px solid black; vertical-align: top; text-align: start;" class="editor_tableCell"><p class="editor_paragraph" style="text-align: start;"><br></p></td><th style="width: 75px; border: 1px solid black; vertical-align: top; text-align: start; background-color: rgb(242, 243, 245);" class="editor_tableCell editor_tableCellHeader"><p class="editor_paragraph" style="text-align: start;"><br></p></th></tr></tbody></table>`,
    ],
  ])('Table action %s', async (name, html) => {
    const handleChange = vi.fn()
    const initialValue = `<table class="editor_table"><colgroup><col><col><col></colgroup><tbody><tr><th class="editor_tableCell editor_tableCellHeader" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start; background-color: rgb(242, 243, 245);"><p class="editor_paragraph"><br></p></th><th class="editor_tableCell editor_tableCellHeader" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start; background-color: rgb(242, 243, 245);"><p class="editor_paragraph"><br></p></th><th class="editor_tableCell editor_tableCellHeader" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start; background-color: rgb(242, 243, 245);"><p class="editor_paragraph"><br></p></th></tr><tr><th class="editor_tableCell editor_tableCellHeader" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start; background-color: rgb(242, 243, 245);"><p class="editor_paragraph"><br></p></th><td class="editor_tableCell" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start;"><p class="editor_paragraph"><br></p></td><td class="editor_tableCell" style="border: 1px solid black; width: 75px; vertical-align: top; text-align: start;"><p class="editor_paragraph"><br></p></td></tr></tbody></table>`
    render(
      <RichTextEditor initialValue={initialValue} onChange={handleChange} />,
    )
    const editor = await screen.findByRole('textbox')
    await userEvent.click(editor)
    const element = editor.querySelector('.editor_tableCell')

    await userEvent.pointer({
      keys: '[MouseRight>]',
      target: element || undefined,
    })

    expect(screen.queryByText('Cell background color')).toBeInTheDocument()

    const actionButton = screen.getByText(name)

    await userEvent.click(actionButton)
    expect(handleChange).toHaveBeenCalledWith(html)
  })
})
