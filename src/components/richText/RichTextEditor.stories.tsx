import {Meta, StoryObj} from '@storybook/react'

import RichTextEditor from './RichTextEditor'

const meta = {
  title: 'components/richText/RichTextEditor',
  component: RichTextEditor,
  tags: ['autodocs'],
} satisfies Meta<typeof RichTextEditor>

export default meta

type Story = StoryObj<typeof meta>
export const Root: Story = {
  args: {
    initialValue: undefined,
  },
}
