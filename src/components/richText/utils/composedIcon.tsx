import {Paper} from '@mui/material'
import React, {cloneElement} from 'react'

type Size = 'small' | 'medium' | 'large'
type Position = 'top-start' | 'top-end' | 'bottom-start' | 'bottom-end'
type Color =
  | 'inherit'
  | 'disabled'
  | 'action'
  | 'primary'
  | 'secondary'
  | 'error'
  | 'info'
  | 'success'
  | 'warning'

interface ComposedIconProps {
  icon: React.ReactElement
  extraIcon: React.ReactElement
  size?: Size
  color?: Color
  position?: Position
  disabled?: boolean
}

const sizesMap: Record<Size, {size: number; extraSize: number}> = {
  small: {size: 17, extraSize: 10},
  medium: {size: 25, extraSize: 13},
  large: {size: 25, extraSize: 15},
}

const ComposedIcon: React.FC<ComposedIconProps> = ({
  icon,
  extraIcon,
  size = 'small',
  color = 'inherit',
  position = 'bottom-end',
  disabled = false,
}) => {
  const enhancedIcon = cloneElement(icon, {
    ...{
      color: color,
      style: {
        fontSize: sizesMap[size].size,
        width: '1.25em',
      },
    },
  })

  const enhancedExtraIcon = cloneElement(extraIcon, {
    ...{
      color: color,
      style: {
        fontSize: sizesMap[size].extraSize,
        padding: '0px',
        color: disabled ? 'rgba(0, 0, 0, 0.35)' : undefined,
      },
    },
  })

  return (
    <div style={{position: 'relative', cursor: 'default'}}>
      <div style={{lineHeight: '0px'}}>{enhancedIcon}</div>
      <Paper
        style={{
          lineHeight: '0px',
          position: 'absolute',
          textShadow:
            '0.75px 0px 0.5px #FFF, 0px 0.75px 0.5px #FFF, -0.75px 0px 0.5px #FFF, 0px -0.75px 0.5px #FFF',
          bottom: position.includes('bottom') ? '-1px' : undefined,
          top: position.includes('top') ? '-1px' : undefined,
          left: position.includes('start') ? '-1px' : undefined,
          right: position.includes('end') ? '-1px' : undefined,
          borderRadius: '25px',
          borderWidth: 0,
        }}
      >
        {enhancedExtraIcon}
      </Paper>
    </div>
  )
}

export default ComposedIcon
