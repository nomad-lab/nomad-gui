import {Autocomplete, TextField} from '@mui/material'
import React from 'react'

const fontFamilies = [
  'Arial',
  'Arial Black',
  'Comic Sans MS',
  'Courier New',
  'Georgia',
  'Impact',
  'Lucida Sans Unicode',
  'Tahoma',
  'Times New Roman',
  'Trebuchet MS',
  'Verdana',
  'Helvetica',
  'Gill Sans',
  'Palatino',
  'Garamond',
  'Bookman',
  'Comic Sans',
  'Candara',
  'Geneva',
  'Optima',
  'Futura',
  'Franklin Gothic',
  'Baskerville',
  'Big Caslon',
  'Didot',
  'American Typewriter',
  'Andale Mono',
]

interface FontFamilyProps {
  fontFamily?: string | null
  onChange: (fontFamily?: string) => void
}

function FontFamily({fontFamily, onChange}: FontFamilyProps) {
  return (
    <Autocomplete
      value={fontFamily || 'Arial'}
      onChange={(event, value) => onChange(value || undefined)}
      options={fontFamilies}
      disableClearable
      renderInput={(params) => (
        <TextField
          {...params}
          variant='outlined'
          size={'small'}
          sx={{
            paddingTop: 0,
            paddingBottom: 0,
            height: 'inherit',
            width: '120px',
          }}
        />
      )}
      renderOption={(props, option) => (
        <li {...props} style={{fontFamily: option}}>
          {option}
        </li>
      )}
    />
  )
}

export default FontFamily
