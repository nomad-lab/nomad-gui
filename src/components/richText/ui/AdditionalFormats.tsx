import {mergeRegister} from '@lexical/utils'
import FormatClearIcon from '@mui/icons-material/FormatClear'
import SubscriptIcon from '@mui/icons-material/Subscript'
import SuperscriptIcon from '@mui/icons-material/Superscript'
import {IconButton} from '@mui/material'
import {
  $getSelection,
  $isRangeSelection,
  COMMAND_PRIORITY_LOW,
  FORMAT_TEXT_COMMAND,
  LexicalEditor,
  SELECTION_CHANGE_COMMAND,
} from 'lexical'
import React, {useCallback, useEffect, useState} from 'react'

import {clearFormat} from '../utils/clearFormat'

function AdditionalFormats({editor}: {editor: LexicalEditor}) {
  const [isSubscript, setIsSubscript] = useState(false)
  const [isSuperscript, setIsSuperscript] = useState(false)

  const updateFormats = useCallback(() => {
    const selection = $getSelection()
    if ($isRangeSelection(selection)) {
      setIsSubscript(selection.hasFormat('subscript'))
      setIsSuperscript(selection.hasFormat('superscript'))
    }
  }, [])

  useEffect(() => {
    editor.getEditorState().read(() => {
      updateFormats()
    })
    return mergeRegister(
      editor.registerUpdateListener(({editorState}) => {
        editorState.read(() => {
          updateFormats()
        })
      }),

      editor.registerCommand(
        SELECTION_CHANGE_COMMAND,
        () => {
          updateFormats()
          return false
        },
        COMMAND_PRIORITY_LOW,
      ),
    )
  }, [editor, updateFormats])

  const handleClearFormat = useCallback(() => {
    editor.update(() => {
      const selection = $getSelection()
      clearFormat(selection)
    })
  }, [editor])

  return (
    <>
      <IconButton
        onClick={() => {
          editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'subscript')
        }}
        color={isSubscript ? 'primary' : 'default'}
        size='small'
        aria-label='Format Subscript'
      >
        <SubscriptIcon />
      </IconButton>
      <IconButton
        onClick={() => {
          editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'superscript')
        }}
        color={isSuperscript ? 'primary' : 'default'}
        size='small'
        aria-label='Format Superscript'
      >
        <SuperscriptIcon />
      </IconButton>
      <IconButton
        onClick={handleClearFormat}
        size='small'
        aria-label='Clear Format'
      >
        <FormatClearIcon />
      </IconButton>
    </>
  )
}

export default AdditionalFormats
