import FormatAlignCenterIcon from '@mui/icons-material/FormatAlignCenter'
import FormatAlignJustifyIcon from '@mui/icons-material/FormatAlignJustify'
import FormatAlignLeftIcon from '@mui/icons-material/FormatAlignLeft'
import FormatAlignRightIcon from '@mui/icons-material/FormatAlignRight'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import {Button, ListItemIcon, Menu, MenuItem} from '@mui/material'
import {FORMAT_ELEMENT_COMMAND, LexicalEditor} from 'lexical'
import React, {useState} from 'react'

import {alignmentTypes} from './AlignmentsOptions'

function Alignments({
  editor,
  alignment,
  onChange,
}: {
  editor: LexicalEditor
  alignment?: keyof typeof alignmentTypes
  onChange: (alignment?: keyof typeof alignmentTypes) => void
}): JSX.Element {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const openMenu = Boolean(anchorEl)

  return (
    <React.Fragment>
      <Button
        onClick={(event) => setAnchorEl(event.currentTarget)}
        size={'small'}
        color={'inherit'}
        sx={{
          paddingTop: 0,
          paddingBottom: 0,
          height: 'inherit',
          '& .css-cstir9-MuiButton-startIcon': {margin: 0},
          '& .css-jcxoq4-MuiButton-endIcon': {margin: 0},
        }}
        startIcon={alignmentTypes[alignment || 'left']}
        endIcon={<KeyboardArrowDownIcon />}
      />
      <Menu
        anchorEl={anchorEl}
        open={openMenu}
        onClose={() => {
          setAnchorEl(null)
        }}
      >
        <MenuItem
          onClick={() => {
            editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'left')
            onChange('left')
          }}
          aria-label='Left Align'
        >
          <ListItemIcon>
            <FormatAlignLeftIcon />
          </ListItemIcon>
          {'Left'}
        </MenuItem>
        <MenuItem
          onClick={() => {
            editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'center')
            onChange('center')
          }}
          aria-label='Center Align'
        >
          <ListItemIcon>
            <FormatAlignCenterIcon />
          </ListItemIcon>
          {'Center'}
        </MenuItem>
        <MenuItem
          onClick={() => {
            editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'right')
            onChange('right')
          }}
          aria-label='Right Align'
        >
          <ListItemIcon>
            <FormatAlignRightIcon />
          </ListItemIcon>
          {'Right'}
        </MenuItem>
        <MenuItem
          onClick={() => {
            editor.dispatchCommand(FORMAT_ELEMENT_COMMAND, 'justify')
            onChange('justify')
          }}
          aria-label='Justify Align'
        >
          <ListItemIcon>
            <FormatAlignJustifyIcon />
          </ListItemIcon>
          {'Justify'}
        </MenuItem>
      </Menu>
    </React.Fragment>
  )
}

export default Alignments
