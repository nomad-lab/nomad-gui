/**
 * Copyright (c) Meta Platforms, Inc. and affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
import {Card, TextareaAutosize} from '@mui/material'
import {useTheme} from '@mui/material/styles'
import type {Ref, RefObject} from 'react'
import * as React from 'react'
import {ChangeEvent, forwardRef} from 'react'

import './EquationEditor.css'

type BaseEquationEditorProps = {
  equation: string
  inline: boolean
  setEquation: (equation: string) => void
}

const EquationEditor = forwardRef<
  HTMLInputElement | HTMLTextAreaElement,
  BaseEquationEditorProps
>(function EquationEditor(
  {equation, setEquation, inline}: BaseEquationEditorProps,
  forwardedRef: Ref<HTMLInputElement | HTMLTextAreaElement>,
): JSX.Element {
  const onChange = (event: ChangeEvent) => {
    setEquation((event.target as HTMLInputElement).value)
  }
  const theme = useTheme()

  return inline ? (
    <Card className='EquationEditor_inputBackground'>
      <span className='EquationEditor_dollarSign'>$</span>
      <input
        className='EquationEditor_inlineEditor'
        value={equation}
        onChange={onChange}
        autoFocus={true}
        ref={forwardedRef as RefObject<HTMLInputElement>}
        style={{color: theme.palette.text.primary}}
      />
      <span className='EquationEditor_dollarSign'>$</span>
    </Card>
  ) : (
    <Card className='EquationEditor_inputBackground'>
      <span className='EquationEditor_dollarSign'>$</span>
      <TextareaAutosize
        className='EquationEditor_blockEditor'
        value={equation}
        onChange={onChange}
        ref={forwardedRef as RefObject<HTMLTextAreaElement>}
        style={{color: theme.palette.text.primary}}
      />
      <span className='EquationEditor_dollarSign_end'>$</span>
    </Card>
  )
})

export default EquationEditor
