import {INSERT_TABLE_COMMAND} from '@lexical/table'
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  Paper,
  Stack,
  TextField,
  Typography,
} from '@mui/material'
import Grid from '@mui/material/Grid'
import {LexicalEditor} from 'lexical'
import {useCallback, useEffect, useState} from 'react'
import * as React from 'react'

export function InsertTableDialog({
  open,
  activeEditor,
  onClose,
}: {
  open: boolean
  activeEditor: LexicalEditor
  onClose: () => void
}): JSX.Element {
  const [column, setColumn] = useState<string>('4')
  const [row, setRow] = useState<string>('4')
  const [columnError, setColumnError] = useState<string>('')
  const [rowError, setRowError] = useState<string>('')

  useEffect(() => {
    const i = Number(row)
    const j = Number(column)
    if (i < 2) {
      setRowError('Number of rows should be more than 2')
    } else if (i > 10) {
      setRowError('The maximum number of rows is 10')
    } else {
      setRowError('')
    }
    if (j < 2) {
      setColumnError('Number of columns should be more than 2')
    } else if (j > 10) {
      setColumnError('The maximum number of columns is 10')
    } else {
      setColumnError('')
    }
  }, [row, column])

  const getCellColor = useCallback(
    (i: number, j: number) => {
      if (i <= Number(row) && j <= Number(column)) {
        return '#0288d1'
      } else {
        return '#FFFFFF'
      }
    },
    [row, column],
  )

  return (
    <Dialog open={open} PaperProps={{sx: {width: 550}}}>
      <DialogTitle>Add Table</DialogTitle>
      <DialogContent>
        <Typography variant='body1'>Select the size of the table</Typography>
        <Stack
          direction='row'
          justifyContent='center'
          alignItems='center'
          sx={{width: '100%', marginTop: 1, marginBottom: 1}}
        >
          <Box maxWidth={300} maxHeight={300}>
            <Grid container spacing={0.5} columns={{xs: 10, sm: 10, md: 10}}>
              {Array.from(Array(100)).map((_, index) => {
                const i = Math.floor(index / 10) + 1
                const j = (index % 10) + 1
                return (
                  <Grid item xs={1} sm={1} md={1} key={index}>
                    <Paper
                      sx={{
                        width: '100%',
                        height: 25,
                        borderColor: 'gray',
                        background: getCellColor(i, j),
                      }}
                      onClick={() => {
                        setRow(`${i < 2 ? 2 : i}`)
                        setColumn(`${j < 2 ? 2 : j}`)
                      }}
                    />
                  </Grid>
                )
              })}
            </Grid>
          </Box>
        </Stack>
        <FormControl fullWidth margin='dense'>
          <TextField
            label={'Columns'}
            placeholder={'Number of coloums'}
            type='number'
            value={column}
            inputProps={{min: 2, max: 10}}
            onChange={(event) => setColumn(event.target.value)}
            error={!!columnError}
            helperText={columnError ? columnError : undefined}
            fullWidth
          />
        </FormControl>
        <FormControl fullWidth margin='dense'>
          <TextField
            label={'Rows'}
            placeholder={'Number of rows'}
            type='number'
            inputProps={{min: 2, max: 10}}
            value={row}
            onChange={(event) => setRow(event.target.value)}
            error={!!rowError}
            helperText={rowError ? rowError : undefined}
            fullWidth
          />
        </FormControl>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => onClose()} color='primary'>
          Cancel
        </Button>
        <Button
          variant='contained'
          color='primary'
          disabled={!!(columnError || rowError)}
          onClick={() => {
            activeEditor.dispatchCommand(INSERT_TABLE_COMMAND, {
              columns: column,
              rows: row,
            })
            onClose()
          }}
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  )
}
