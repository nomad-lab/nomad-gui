import {$createCodeNode} from '@lexical/code'
import {
  INSERT_CHECK_LIST_COMMAND,
  INSERT_ORDERED_LIST_COMMAND,
  INSERT_UNORDERED_LIST_COMMAND,
} from '@lexical/list'
import {
  $createHeadingNode,
  $createQuoteNode,
  HeadingTagType,
} from '@lexical/rich-text'
import {$setBlocksType} from '@lexical/selection'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import {Button, ListItemIcon, Menu, MenuItem} from '@mui/material'
import {
  $createParagraphNode,
  $getSelection,
  $isRangeSelection,
  LexicalEditor,
} from 'lexical'
import React, {useState} from 'react'

import {blockTypes} from './BlockFormatsOptions'

function BlockFormats({
  editor,
  blockType,
}: {
  editor: LexicalEditor
  blockType: keyof typeof blockTypes
}): JSX.Element {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const openMenu = Boolean(anchorEl)

  const formatParagraph = () => {
    editor.update(() => {
      const selection = $getSelection()
      if ($isRangeSelection(selection)) {
        $setBlocksType(selection, () => $createParagraphNode())
      }
    })
  }

  const formatHeading = (headingSize: HeadingTagType) => {
    if (blockType !== headingSize) {
      editor.update(() => {
        const selection = $getSelection()
        $setBlocksType(selection, () => $createHeadingNode(headingSize))
      })
    }
  }

  const formatBulletList = () => {
    if (blockType !== 'bullet') {
      editor.dispatchCommand(INSERT_UNORDERED_LIST_COMMAND, undefined)
    } else {
      formatParagraph()
    }
  }

  const formatNumberedList = () => {
    if (blockType !== 'number') {
      editor.dispatchCommand(INSERT_ORDERED_LIST_COMMAND, undefined)
    } else {
      formatParagraph()
    }
  }

  const formatCheckList = () => {
    if (blockType !== 'check') {
      editor.dispatchCommand(INSERT_CHECK_LIST_COMMAND, undefined)
    } else {
      formatParagraph()
    }
  }

  const formatQuote = () => {
    if (blockType !== 'quote') {
      editor.update(() => {
        const selection = $getSelection()
        $setBlocksType(selection, () => $createQuoteNode())
      })
    } else {
      formatParagraph()
    }
  }

  const formatCode = () => {
    if (blockType !== 'code') {
      editor.update(() => {
        let selection = $getSelection()

        if (selection !== null) {
          if (selection.isCollapsed()) {
            $setBlocksType(selection, () => $createCodeNode())
          } else {
            const textContent = selection.getTextContent()
            const codeNode = $createCodeNode()
            selection.insertNodes([codeNode])
            selection = $getSelection()
            if ($isRangeSelection(selection)) {
              selection.insertRawText(textContent)
            }
          }
        }
      })
    } else {
      formatParagraph()
    }
  }

  return (
    <React.Fragment>
      <Button
        onClick={(event) => setAnchorEl(event.currentTarget)}
        size={'small'}
        color={'inherit'}
        sx={{
          padding: 0,
          height: 'inherit',
          '& .css-cstir9-MuiButton-startIcon': {margin: 0},
          '& .css-jcxoq4-MuiButton-endIcon': {margin: 0},
        }}
        endIcon={<KeyboardArrowDownIcon />}
        data-testid={'block-types-button'}
      >
        {blockTypes[blockType].label}
      </Button>
      <Menu
        anchorEl={anchorEl}
        open={openMenu}
        onClose={() => {
          setAnchorEl(null)
        }}
        data-testid={'block-types-menu'}
      >
        <MenuItem
          disableRipple
          color={blockType == 'paragraph' ? 'primary' : 'inherit'}
          onClick={() => {
            formatParagraph()
            setAnchorEl(null)
          }}
        >
          <ListItemIcon>{blockTypes['paragraph'].icon}</ListItemIcon>
          Normal text
        </MenuItem>
        <MenuItem
          disableRipple
          color={blockType == 'h1' ? 'primary' : 'inherit'}
          onClick={() => {
            formatHeading('h1')
            setAnchorEl(null)
          }}
        >
          <ListItemIcon>{blockTypes['h1'].icon}</ListItemIcon>
          Heading 1
        </MenuItem>
        <MenuItem
          disableRipple
          color={blockType == 'h2' ? 'primary' : 'inherit'}
          onClick={() => {
            formatHeading('h2')
            setAnchorEl(null)
          }}
        >
          <ListItemIcon>{blockTypes['h2'].icon}</ListItemIcon>
          Heading 2
        </MenuItem>
        <MenuItem
          disableRipple
          color={blockType == 'h3' ? 'primary' : 'inherit'}
          onClick={() => {
            formatHeading('h3')
            setAnchorEl(null)
          }}
        >
          <ListItemIcon>{blockTypes['h3'].icon}</ListItemIcon>
          Heading 3
        </MenuItem>
        <MenuItem
          color={blockType == 'bullet' ? 'primary' : 'inherit'}
          onClick={() => {
            formatBulletList()
            setAnchorEl(null)
          }}
          aria-label='bullet'
        >
          <ListItemIcon>{blockTypes['bullet'].icon}</ListItemIcon>
          Bullet List
        </MenuItem>
        <MenuItem
          color={blockType == 'number' ? 'primary' : 'inherit'}
          onClick={() => {
            formatNumberedList()
            setAnchorEl(null)
          }}
        >
          <ListItemIcon>{blockTypes['number'].icon}</ListItemIcon>
          Numbered List
        </MenuItem>
        <MenuItem
          color={blockType == 'check' ? 'primary' : 'inherit'}
          onClick={() => {
            formatCheckList()
            setAnchorEl(null)
          }}
        >
          <ListItemIcon>{blockTypes['check'].icon}</ListItemIcon>
          Check List
        </MenuItem>
        <MenuItem
          color={blockType == 'quote' ? 'primary' : 'inherit'}
          onClick={() => {
            formatQuote()
            setAnchorEl(null)
          }}
        >
          <ListItemIcon>{blockTypes['quote'].icon}</ListItemIcon>
          Quote
        </MenuItem>
        <MenuItem
          onClick={() => {
            formatCode()
            setAnchorEl(null)
          }}
        >
          <ListItemIcon>{blockTypes['code'].icon}</ListItemIcon>
          Code
        </MenuItem>
      </Menu>
    </React.Fragment>
  )
}

export default BlockFormats
