import FormatAlignCenterIcon from '@mui/icons-material/FormatAlignCenter'
import FormatAlignJustifyIcon from '@mui/icons-material/FormatAlignJustify'
import FormatAlignLeftIcon from '@mui/icons-material/FormatAlignLeft'
import FormatAlignRightIcon from '@mui/icons-material/FormatAlignRight'

export const alignmentTypes = {
  left: <FormatAlignLeftIcon fontSize={'inherit'} />,
  right: <FormatAlignRightIcon fontSize={'inherit'} />,
  center: <FormatAlignCenterIcon fontSize={'inherit'} />,
  justify: <FormatAlignJustifyIcon fontSize={'inherit'} />,
  start: <FormatAlignLeftIcon fontSize={'inherit'} />,
  end: <FormatAlignRightIcon fontSize={'inherit'} />,
}
