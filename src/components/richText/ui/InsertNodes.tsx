import {INSERT_HORIZONTAL_RULE_COMMAND} from '@lexical/react/LexicalHorizontalRuleNode'
import {
  $getSelectionStyleValueForProperty,
  $patchStyleText,
} from '@lexical/selection'
import AddIcon from '@mui/icons-material/Add'
import CodeIcon from '@mui/icons-material/Code'
import FormatColorFillIcon from '@mui/icons-material/FormatColorFill'
import FormatColorTextIcon from '@mui/icons-material/FormatColorText'
import FunctionsIcon from '@mui/icons-material/Functions'
import GridOnIcon from '@mui/icons-material/GridOn'
import HorizontalRuleIcon from '@mui/icons-material/HorizontalRule'
import ImageIcon from '@mui/icons-material/Image'
import ShortcutIcon from '@mui/icons-material/Shortcut'
import {
  Divider,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Tooltip,
} from '@mui/material'
import {
  $getSelection,
  $isRangeSelection,
  FORMAT_TEXT_COMMAND,
  LexicalEditor,
} from 'lexical'
import React, {useCallback, useState} from 'react'

import {INSERT_EQUATION_COMMAND} from '../plugins/EquationCreateCommand'
import {InsertImageDialog} from '../plugins/ImagesPlugin'
import {INSERT_REFERENCE_COMMAND} from '../plugins/ReferenceCreateCommand'
import DropdownColorPicker from './DropdownColorPicker'
import {InsertTableDialog} from './InsertTableDialog'

function InsertNodes({editor}: {editor: LexicalEditor}) {
  const [insertMenuAnchorEl, setInsertMenuAnchorEl] =
    useState<null | HTMLElement>(null)
  const [openImageDialog, setOpenImageDialog] = useState<{
    open: boolean
    inline?: boolean
  }>({open: false})
  const [openTableDialog, setOpenTableDialog] = useState<boolean>(false)
  const [fontColor, setFontColor] = useState<string>('')
  const [backGroundColor, setBackGroundColor] = useState<string>('')
  const openInsertMenu = Boolean(insertMenuAnchorEl)

  const applyStyleText = useCallback(
    (styles: Record<string, string>) => {
      editor.update(() => {
        const selection = $getSelection()
        if (selection !== null) {
          $patchStyleText(selection, styles)
        }
        if ($isRangeSelection(selection)) {
          setFontColor($getSelectionStyleValueForProperty(selection, 'color'))
          setBackGroundColor(
            $getSelectionStyleValueForProperty(selection, 'background-color'),
          )
        }
      }, {})
    },
    [editor],
  )

  return (
    <React.Fragment>
      <Tooltip title={'Insert'}>
        <IconButton
          size={'small'}
          onClick={(event) => setInsertMenuAnchorEl(event.currentTarget)}
          aria-label='Insert'
        >
          <AddIcon />
        </IconButton>
      </Tooltip>
      <Menu
        anchorEl={insertMenuAnchorEl}
        open={openInsertMenu}
        onClose={() => {
          setInsertMenuAnchorEl(null)
        }}
      >
        <MenuItem
          onClick={() => {
            editor.dispatchCommand(INSERT_HORIZONTAL_RULE_COMMAND, undefined)
            setInsertMenuAnchorEl(null)
          }}
          disableRipple
        >
          <ListItemIcon>
            <HorizontalRuleIcon />
          </ListItemIcon>
          Horizontal Rule
        </MenuItem>
        <MenuItem
          onClick={() => editor.dispatchCommand(FORMAT_TEXT_COMMAND, 'code')}
          aria-label='Format Code'
        >
          <ListItemIcon>
            <CodeIcon />
          </ListItemIcon>
          Code
        </MenuItem>
        <MenuItem
          onClick={() => {
            setOpenTableDialog(true)
            setInsertMenuAnchorEl(null)
          }}
          disableRipple
        >
          <ListItemIcon>
            <GridOnIcon />
          </ListItemIcon>
          Table
        </MenuItem>
        <Divider sx={{my: 0.5}} />
        <MenuItem
          onClick={() => {
            setOpenImageDialog({open: true, inline: true})
            setInsertMenuAnchorEl(null)
          }}
          disableRipple
        >
          <ListItemIcon>
            <ImageIcon />
          </ListItemIcon>
          Inline Image
        </MenuItem>
        <MenuItem
          onClick={() => {
            setOpenImageDialog({open: true, inline: false})
            setInsertMenuAnchorEl(null)
          }}
          disableRipple
        >
          <ListItemIcon>
            <ImageIcon />
          </ListItemIcon>
          Image
        </MenuItem>
        <Divider sx={{my: 0.5}} />
        <MenuItem
          onClick={() => {
            editor.dispatchCommand(INSERT_EQUATION_COMMAND, {
              equation: 'y = e^x',
              inline: true,
            })
            setInsertMenuAnchorEl(null)
          }}
          disableRipple
        >
          <ListItemIcon>
            <FunctionsIcon />
          </ListItemIcon>
          Inline Equation
        </MenuItem>
        <MenuItem
          onClick={() => {
            editor.dispatchCommand(INSERT_EQUATION_COMMAND, {
              equation: 'y = e^x',
              inline: false,
            })
            setInsertMenuAnchorEl(null)
          }}
          disableRipple
        >
          <ListItemIcon>
            <FunctionsIcon />
          </ListItemIcon>
          Equation
        </MenuItem>
        <Divider sx={{my: 0.5}} />
        <MenuItem
          onClick={() => {
            editor.dispatchCommand(INSERT_REFERENCE_COMMAND, {
              reference: 'path',
              inline: true,
            })
            setInsertMenuAnchorEl(null)
          }}
          disableRipple
        >
          <ListItemIcon>
            <ShortcutIcon />
          </ListItemIcon>
          Inline Reference
        </MenuItem>
        <MenuItem
          onClick={() => {
            editor.dispatchCommand(INSERT_REFERENCE_COMMAND, {
              reference: 'path',
              inline: false,
            })
            setInsertMenuAnchorEl(null)
          }}
          disableRipple
        >
          <ListItemIcon>
            <ShortcutIcon />
          </ListItemIcon>
          Reference
        </MenuItem>
        <Divider sx={{my: 0.5}} />
        <DropdownColorPicker
          title={'Font color'}
          color={fontColor}
          onChange={(color) => applyStyleText({color: color})}
          menuItem
        >
          <ListItemIcon>
            <FormatColorTextIcon />
          </ListItemIcon>
          Font color
        </DropdownColorPicker>
        <DropdownColorPicker
          title={'Background color'}
          color={backGroundColor}
          onChange={(color) => applyStyleText({'background-color': color})}
          menuItem
        >
          <ListItemIcon>
            <FormatColorFillIcon />
          </ListItemIcon>
          Background color
        </DropdownColorPicker>
      </Menu>
      <InsertImageDialog
        open={openImageDialog.open}
        activeEditor={editor}
        onClose={() => setOpenImageDialog({open: false})}
        inLine={openImageDialog.inline}
      />
      <InsertTableDialog
        open={openTableDialog}
        activeEditor={editor}
        onClose={() => setOpenTableDialog(false)}
      />
    </React.Fragment>
  )
}

export default InsertNodes
