import {IconButton, Menu, MenuItem, Tooltip} from '@mui/material'
import React, {PropsWithChildren, useState} from 'react'
import {SketchPicker} from 'react-color'

type DropdownColorPickerProps = PropsWithChildren<{
  title: string
  color?: string
  onChange: (color: string) => void
  menuItem?: boolean
}>

function DropdownColorPicker({
  title,
  color,
  children,
  onChange,
  menuItem,
}: DropdownColorPickerProps) {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const openMenu = Boolean(anchorEl)

  return (
    <React.Fragment>
      {menuItem ? (
        <MenuItem onClick={(event) => setAnchorEl(event.currentTarget)}>
          {children}
        </MenuItem>
      ) : (
        <Tooltip title={title}>
          <IconButton
            size={'small'}
            onClick={(event) => setAnchorEl(event.currentTarget)}
            style={{color: color || undefined}}
          >
            {children}
          </IconButton>
        </Tooltip>
      )}
      <Menu
        anchorEl={anchorEl}
        open={openMenu}
        onClose={() => {
          setAnchorEl(null)
        }}
      >
        <SketchPicker
          color={color}
          onChange={(color) =>
            onChange(`rgb(${color.rgb.r}, ${color.rgb.g}, ${color.rgb.b})`)
          }
        />
      </Menu>
    </React.Fragment>
  )
}

export default DropdownColorPicker
