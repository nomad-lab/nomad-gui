import AddIcon from '@mui/icons-material/Add'
import RemoveIcon from '@mui/icons-material/Remove'
import {Box, IconButton, InputBase} from '@mui/material'
import React, {ChangeEvent} from 'react'

function FontSize({
  fontSize,
  onChange,
}: {
  fontSize: number | undefined
  onChange: (fontSize: number | undefined) => void
}) {
  const handleIncrease = () => {
    onChange(Math.min(72, (fontSize || 15) + 1))
  }

  const handleDecrease = () => {
    onChange(Math.max(8, (fontSize || 15) - 1))
  }

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const newValue = parseInt(event.target.value, 10)
    if (!isNaN(newValue)) {
      onChange(newValue)
    } else {
      onChange(undefined)
    }
  }

  return (
    <Box display='flex' alignItems='center'>
      <IconButton size={'small'} onClick={handleDecrease}>
        <RemoveIcon />
      </IconButton>
      <InputBase
        value={fontSize || 15}
        onChange={handleChange}
        inputProps={{
          min: 8,
          max: 72,
          type: 'tel',
        }}
        sx={{width: 20, textAlign: 'center'}}
        endAdornment={null}
      />
      <IconButton size={'small'} onClick={handleIncrease}>
        <AddIcon />
      </IconButton>
    </Box>
  )
}

export default FontSize
