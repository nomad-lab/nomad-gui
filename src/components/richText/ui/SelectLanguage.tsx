import {
  $isCodeNode,
  CODE_LANGUAGE_FRIENDLY_NAME_MAP,
  CODE_LANGUAGE_MAP,
  getLanguageFriendlyName,
} from '@lexical/code'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import {Button, Menu, MenuItem} from '@mui/material'
import {$getNodeByKey, LexicalEditor, NodeKey} from 'lexical'
import React, {useCallback, useState} from 'react'

function getCodeLanguageOptions(): [string, string][] {
  const options: [string, string][] = []
  for (const [lang, friendlyName] of Object.entries(
    CODE_LANGUAGE_FRIENDLY_NAME_MAP,
  )) {
    options.push([lang, friendlyName])
  }
  return options
}

const CODE_LANGUAGE_OPTIONS = getCodeLanguageOptions()

function SelectLanguage({
  editor,
  language,
  elementKey,
}: {
  editor: LexicalEditor
  language: keyof typeof CODE_LANGUAGE_MAP
  elementKey: NodeKey | null
}): JSX.Element {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const openMenu = Boolean(anchorEl)

  const onCodeLanguageSelect = useCallback(
    (value: string) => {
      editor.update(() => {
        if (elementKey !== null) {
          const node = $getNodeByKey(elementKey)
          if ($isCodeNode(node)) {
            node.setLanguage(value)
          }
        }
      })
    },
    [editor, elementKey],
  )

  return (
    <React.Fragment>
      <Button
        disableElevation
        onClick={(event) => setAnchorEl(event.currentTarget)}
        endIcon={<KeyboardArrowDownIcon fontSize={'small'} />}
        color={'inherit'}
        size={'small'}
        sx={{
          paddingTop: 0,
          paddingBottom: 0,
          height: 'inherit',
          '& .css-cstir9-MuiButton-startIcon': {margin: 0},
          '& .css-jcxoq4-MuiButton-endIcon': {margin: 0},
        }}
        aria-label='Code Language'
      >
        {getLanguageFriendlyName(language)}
      </Button>
      <Menu
        anchorEl={anchorEl}
        open={openMenu}
        onClose={() => {
          setAnchorEl(null)
        }}
      >
        {CODE_LANGUAGE_OPTIONS.map(([value, name]) => {
          return (
            <MenuItem
              key={value}
              onClick={() => {
                onCodeLanguageSelect(value)
                setAnchorEl(null)
              }}
              disableRipple
              data-testid={name}
            >
              {name}
            </MenuItem>
          )
        })}
      </Menu>
    </React.Fragment>
  )
}

export default SelectLanguage
