import React, {useContext} from 'react'

export type PageContext = {
  scrollRef: React.RefObject<HTMLElement>
  isScrolled: boolean
}

export const pageContext = React.createContext<PageContext>({
  scrollRef: {current: null},
  isScrolled: false,
})

/**
 * This hook causes a re-render when the page switched from being scrolled to
 * being at the top. It does not re-render on every scroll event.
 */
export default function usePage() {
  return useContext(pageContext)
}
