import MdxPage from './MdxPage'

export default function mdxPageRoute(path: string, mdx: React.ElementType) {
  return {
    path,
    component: () => <MdxPage mdx={mdx} />,
  }
}
