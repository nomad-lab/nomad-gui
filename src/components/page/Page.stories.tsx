import {Box} from '@mui/material'
import type {Meta, StoryObj} from '@storybook/react'
import {PropsWithChildren} from 'react'

import Router from '../routing/Router'
import Page from './Page'

const meta = {
  title: 'components/page/Page',
  component: Page,
  decorators: [
    (Story) => (
      <Box sx={{height: '100vh'}}>
        <Router route={{path: '/*', component: () => <Story />}} />
      </Box>
    ),
  ],
  parameters: {
    layout: 'fullscreen',
  },
  tags: ['autodocs'],
} satisfies Meta<typeof Page>

export default meta

type Story = StoryObj<typeof meta>
export const Plain: Story = {
  args: {
    title: 'Title',
    subtitle: 'sub title',
    navigation: <Box sx={{padding: 2}}>navigation content</Box>,
    sidebar: 'sidebar content',
    children: 'main content',
  },
}

function TallContent({children}: PropsWithChildren) {
  return (
    <Box sx={{padding: 2}}>
      <Box>{children}</Box>
      <Box sx={{height: '110vh', marginTop: 2}}>Scroll down!</Box>
      <Box>The bottom.</Box>
    </Box>
  )
}

function ExamplePage({
  withSidebar,
  withNavigation,
  fullwidth,
  ...pageProps
}: {
  withSidebar?: boolean
  withNavigation?: boolean
  fullwidth?: boolean
  title?: string
  subtitle?: string
}) {
  return (
    <Page
      {...pageProps}
      fullwidth={fullwidth}
      navigation={
        withNavigation && <TallContent>The navigation content.</TallContent>
      }
      sidebar={withSidebar && 'The sidebar content.'}
    >
      <TallContent>The main content</TallContent>
    </Page>
  )
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const examplePageMeta = {
  component: ExamplePage,
} satisfies Meta<typeof ExamplePage>

export const Example: StoryObj<typeof examplePageMeta> = {
  args: {
    withNavigation: true,
    withSidebar: true,
    fullwidth: false,
    title: 'Hello',
    subtitle: 'The subtitle',
  },
  render: (props) => {
    return <ExamplePage {...props} />
  },
}
