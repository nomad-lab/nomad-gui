import React from 'react'

import {components} from '../markdown/components'
import Page from './Page'

export default function MdxPage({mdx}: {mdx: React.ElementType}) {
  return <Page>{React.createElement(mdx, {components: components})}</Page>
}
