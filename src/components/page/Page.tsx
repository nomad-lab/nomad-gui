import {
  Box,
  Container,
  Drawer,
  Fab,
  Fade,
  IconButton,
  Typography,
  generateUtilityClasses,
  useMediaQuery,
} from '@mui/material'
import {styled, useTheme} from '@mui/material/styles'
import {PropsWithChildren, ReactNode, useMemo, useRef, useState} from 'react'
import {useScroll} from 'react-use'

import {SxProps} from '../../utils/types'
import MainMenu from '../app/MainMenu'
import {ArrowUp, Menu} from '../icons'
import Breadcrumbs from '../navigation/Breadcrumbs'
import usePage, {pageContext} from './usePage'

const classes = generateUtilityClasses('Page', [
  'root',
  'navigationColumn',
  'navigationDrawer',
  'sidebar',
  'content',
])

function ScrollTop() {
  const {isScrolled, scrollRef} = usePage()

  const handleClick = () => {
    if (!scrollRef.current) {
      return
    }
    scrollRef.current.scrollTop = 0
  }

  return (
    <Fade in={isScrolled}>
      <Box
        onClick={handleClick}
        role='presentation'
        sx={{position: 'fixed', bottom: 16, right: 16, zIndex: 1000}}
      >
        <Fab size='small' title='Scroll to top'>
          <ArrowUp />
        </Fab>
      </Box>
    </Fade>
  )
}

function PageScrollableContent({
  fullwidth,
  fullheight,
  children,
}: {
  fullwidth?: boolean
  fullheight?: boolean
  disablePadding?: boolean
} & PropsWithChildren) {
  const scrollRef = useRef<HTMLElement>(null)
  const {y} = useScroll(scrollRef)
  const isScrolled = y !== 0

  // This memo is an optimization to prevent re-render on every scroll change.
  // useScroll triggers a re-render on every scroll event on every y-axis change,
  // but we only want to re-render when `y === 0` changes.
  const element = useMemo(
    () => (
      <pageContext.Provider value={{scrollRef, isScrolled}}>
        <Box
          sx={{
            flexDirection: 'column',
            height: '100%',
            flexGrow: 1,
            overflowX: 'auto',
          }}
        >
          <Box
            data-testid='scroll-container'
            ref={scrollRef}
            sx={{
              overflowY: fullheight ? 'hidden' : 'auto',
              height: '100%',
            }}
          >
            <Container
              disableGutters
              maxWidth={fullwidth ? false : 'lg'}
              sx={
                fullheight
                  ? {
                      height: '100%',
                      display: 'flex',
                      flexDirection: 'column',
                    }
                  : undefined
              }
            >
              {children}
            </Container>
          </Box>
          <ScrollTop />
        </Box>
      </pageContext.Provider>
    ),
    [children, fullwidth, fullheight, isScrolled],
  )
  return element
}

const PageNavigationDrawer = styled(Drawer, {
  name: 'Page',
  slot: 'navigationDrawer',
})(() => ({
  '& > .MuiDrawer-paper': {
    padding: 0,
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    borderTop: 'none',
    borderBottom: 'none',
    maxWidth: '90vw',
  },
}))

const PageNavigationColumn = styled('div', {
  name: 'Page',
  slot: 'navigationColumn',
})(({theme}) => ({
  flexDirection: 'column',
  display: 'flex',
  height: '100%',
  overflowY: 'auto',
  borderRight: `1px solid ${theme.palette.divider}`,
  width: (theme.breakpoints.values.lg - theme.breakpoints.values.md) * 1.3,
  flexShrink: 0,
  borderBottom: 'none',
  borderTop: 'none',
  borderLeft: 'none',
}))

const PageSidebar = styled('div', {
  name: 'Page',
  slot: 'sidebar',
})(({theme}) => ({
  [theme.breakpoints.up('xl')]: {
    maxWidth: theme.breakpoints.values.xl - theme.breakpoints.values.lg,
  },
}))

const PageBreadcrumbsWithNavigation = styled('div')(({theme}) => ({
  display: 'flex',
  flexDirection: 'row',
  gap: theme.spacing(2),
  padding: theme.spacing(2),
  alignItems: 'center',
  '&>.MuiIconButton-root:first-of-type': {
    marginLeft: '-6px',
  },
}))

const PageContentWithSidebar = styled('div')(({theme}) => ({
  display: 'flex',
  flexDirection: 'row',
  [theme.breakpoints.down('md')]: {
    flexDirection: 'column',
  },
  gap: theme.spacing(4),
  paddingBottom: theme.spacing(2),
}))

const PageContent = styled('div', {
  name: 'Page',
  slot: 'content',
})(({theme}) => ({
  flexGrow: 1,
  width: '100%',
  paddingLeft: theme.spacing(2),
  paddingRight: theme.spacing(2),
}))

const PageTitleRoot = styled('div', {
  name: 'Page',
  slot: 'titleRoot',
})(({theme}) => ({
  marginTop: theme.spacing(2),
  marginBottom: theme.spacing(2),
}))

const PageTitleHeader = styled('div', {
  name: 'Page',
  slot: 'titleHeader',
})(({theme}) => ({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  gap: theme.spacing(2),
}))

const PageTitleTitle = styled('div', {
  name: 'Page',
  slot: 'titleTitle',
})({flexGrow: 1})

const PageTitleActions = styled('div', {
  name: 'Page',
  slot: 'titleActions',
})({})

const PageActionsRoot = styled('div', {
  name: 'PageActions',
  slot: 'root',
})({
  marginTop: 16,
  marginBottom: 16,
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'left',
  gap: 8,
  flexWrap: 'wrap',
})

const PageRoot = styled('div', {
  name: 'Page',
  slot: 'root',
})(() => ({
  display: 'flex',
  flexDirection: 'row',
  height: '100%',
}))

interface PageTitleProps {
  /** The subtitle of the page or part of the page, shown below the title. */
  subtitle?: string | ReactNode
  /** The title of the page or part of the page. */
  title?: string | ReactNode
  /** Additional actions shown right of the title */
  actions?: ReactNode | ReactNode[]
  /** If this title should be sticke and let the content scroll under it */
  sticky?: boolean
}

/**
 * Component that creates a page title with subtitle. Can be used multiple times
 * on a `Page` to create multiple titles.
 */
export function PageTitle({
  children,
  subtitle,
  title,
  actions,
  sx = [],
}: PropsWithChildren & PageTitleProps & SxProps) {
  return (
    <PageTitleRoot sx={sx}>
      <PageTitleHeader>
        <PageTitleTitle>
          {title ? (
            <Typography variant='h1' color='primary'>
              {title}
            </Typography>
          ) : (
            children && (
              <Typography variant='h1' color='primary'>
                {children}
              </Typography>
            )
          )}
        </PageTitleTitle>
        {actions && <PageTitleActions>{actions}</PageTitleActions>}
      </PageTitleHeader>
      {subtitle && <Typography color='text.secondary'>{subtitle}</Typography>}
    </PageTitleRoot>
  )
}

interface PageActionProps extends PropsWithChildren {
  /** Actions shown on the left, e.g. create */
  leftActions?: ReactNode | ReactNode[]
  /** Actions shown on the right, e.g. download */
  rightActions?: ReactNode | ReactNode[]
}

/**
 * Component that creates a row of actions. Usually placed under the page
 * title and on top of the main page contents.
 */
export function PageActions({
  leftActions,
  rightActions,
  children,
  sx = [],
}: PageActionProps & SxProps) {
  return (
    <PageActionsRoot sx={sx}>
      {leftActions}
      {children}
      <Box sx={{flexGrow: 1}} />
      {rightActions}
    </PageActionsRoot>
  )
}

/**
 * The page props.
 */
export interface PageProps extends PropsWithChildren, PageTitleProps {
  /**
   * Additional *responsive* content that is displayed to the left. The navigation
   * scrolls independently and is visible all the time or hidden in a drawer,
   * depending on screen width.
   */
  navigation?: ReactNode
  /**
   * Additional responsive content that is displayed right to the page content
   * depending on screen width.
   */
  sidebar?: ReactNode
  /**
   * The page content will fill all available horizontal space instead of a
   * centered layout with a max width. */
  fullwidth?: boolean
  /**
   * The page content will fill all available vertical space
   */
  fullheight?: boolean
  /**
   * Disables the padding from the page content.
   */
  disablePadding?: boolean
  /**
   * The variant of the page layout. Can be 'page' or 'dialog'. Dialog pages
   * are used for selection dialogs and will not show the main menu.
   */
  variant?: 'page' | 'dialog'
}

/**
 * Component that creates a page layout based on the passed content (`children`),
 * `navigation`, `sidebar` content, page `title`, `subtitle`, etc. Adds breadcrumbs and scroll aids.
 * Creates consistent layout for all pages in the app.
 *
 * Exposed classes that can be used for styling the component:
 *   Page-navigation: the direct container for the passed `navigation` elements.
 */
export default function Page({
  navigation,
  sidebar,
  fullwidth,
  fullheight,
  disablePadding,
  children,
  title,
  subtitle,
  variant = 'page',
  sx = [],
}: PageProps & SxProps) {
  const theme = useTheme()
  const showNavigationAsDrawer = useMediaQuery(theme.breakpoints.down('lg'))
  const [navigationOpen, setNavigationOpen] = useState(false)
  const handleOpenNavigation = () => setNavigationOpen(true)
  const handleCloseNavigation = () => setNavigationOpen(false)

  return (
    <PageRoot className={classes.root} sx={sx}>
      {showNavigationAsDrawer && variant === 'page' && (
        <PageNavigationDrawer
          className={classes.navigationDrawer}
          anchor='left'
          open={navigationOpen}
          onClose={handleCloseNavigation}
        >
          <MainMenu />
          <Box
            sx={{
              maxWidth: 'calc(100% - 64px)', // this compensates for the collapsed main menu
            }}
          >
            {navigation}
          </Box>
        </PageNavigationDrawer>
      )}
      {!showNavigationAsDrawer && (
        <>
          {variant === 'page' && <MainMenu />}
          {navigation && (
            <PageNavigationColumn className={classes.navigationColumn}>
              {navigation}
            </PageNavigationColumn>
          )}
        </>
      )}
      <PageScrollableContent fullwidth={fullwidth} fullheight={fullheight}>
        <PageBreadcrumbsWithNavigation>
          {showNavigationAsDrawer && variant === 'page' && (
            <IconButton
              data-testid='navigation-button'
              color='primary'
              size='small'
              onClick={handleOpenNavigation}
            >
              <Menu />
            </IconButton>
          )}
          <Breadcrumbs />
        </PageBreadcrumbsWithNavigation>
        <PageContentWithSidebar
          sx={{
            flex: fullheight ? '1 1 auto' : undefined,
            minHeight: 0,
            paddingBottom: disablePadding ? 0 : undefined,
          }}
        >
          <PageContent
            className={classes.content}
            sx={{
              paddingLeft: disablePadding ? 0 : theme.spacing(2),
              paddingRight: disablePadding ? 0 : theme.spacing(2),
            }}
          >
            {title && <PageTitle title={title} subtitle={subtitle} />}
            {children}
          </PageContent>
          {sidebar && <PageSidebar>{sidebar}</PageSidebar>}
        </PageContentWithSidebar>
      </PageScrollableContent>
    </PageRoot>
  )
}
