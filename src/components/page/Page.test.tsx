import {fireEvent, render, screen} from '@testing-library/react'
import React from 'react'
import {describe, expect, it, vi} from 'vitest'
import {LinkProps} from 'wouter'

import {createMatchMedia} from '../../utils/test.helper'
import Page from './Page'

vi.mock('../navigation/Breadcrumbs', () => ({
  default: () => <span>breadcrumbs</span>,
}))

vi.mock('../routing/useRoute', () => ({
  default: () => ({
    matches: () => false,
  }),
}))

vi.mock('../routing/Link', () => ({
  default: React.forwardRef<HTMLAnchorElement, LinkProps>(function Link(
    {children},
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    ref,
  ) {
    return children
  }),
}))

describe('Page component', () => {
  it.each([
    ['plain content', false, false, false],
    ['navigation', true, false, false],
    ['sidebar', false, true, false],
    ['navigation and sidebar', true, true, false],
    ['title', false, false, true],
  ])('should render with %s', (description, navigation, sidebar, title) => {
    const props = {
      ...(navigation && {navigation: <div>navigation</div>}),
      ...(sidebar && {sidebar: <div>sidebar</div>}),
      ...(title && {
        title: 'title',
        subtitle: 'subtitle',
      }),
    }
    render(<Page {...props}>content</Page>)

    expect(screen.getByText('content')).toBeInTheDocument()
    expect(screen.getByText('breadcrumbs')).toBeInTheDocument()

    if (title) {
      expect(screen.getByText('title')).toBeInTheDocument()
      expect(screen.getByText('subtitle')).toBeInTheDocument()
    } else {
      expect(screen.queryByText('title')).not.toBeInTheDocument()
      expect(screen.queryByText('subtitle')).not.toBeInTheDocument()
    }

    if (navigation) {
      expect(screen.getByText('navigation')).toBeInTheDocument()
    } else {
      expect(screen.queryByText('navigation')).not.toBeInTheDocument()
    }

    if (sidebar) {
      expect(screen.getByText('sidebar')).toBeInTheDocument()
    } else {
      expect(screen.queryByText('sidebar')).not.toBeInTheDocument()
    }
  })

  it('should render working scroll up button', async () => {
    // I don't think we can test with actual scrolling due to jsdom not
    // simulating any layout and scroll properties
    vi.mock('react-use', () => ({
      useScroll: () => ({y: 1}),
    }))

    render(<Page>content</Page>)
    const button = screen.getByRole('button', {name: 'Scroll to top'})
    expect(button).toBeInTheDocument()
    const scrollContainer = screen.getByTestId('scroll-container')
    scrollContainer.scrollTop = 1
    fireEvent.click(button)
    expect(scrollContainer.scrollTop).toBe(0)
  })

  it('should render working navigation drawer and button for small screens', async () => {
    window.matchMedia = createMatchMedia(800)
    render(<Page navigation={<div>navigation</div>}>content</Page>)
    expect(screen.queryByText('navigation')).not.toBeInTheDocument()
    const navigationButton = screen.getByTestId('navigation-button')
    fireEvent.click(navigationButton)
    expect(screen.queryByText('navigation')).toBeInTheDocument()
  })
})
