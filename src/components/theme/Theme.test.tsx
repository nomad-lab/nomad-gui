import {Button, Typography, useMediaQuery} from '@mui/material'
import {fireEvent, render, renderHook, screen} from '@testing-library/react'
import {vi} from 'vitest'

import Theme, {Logo} from './Theme'
import lightLogo from './nomad.png'
import darkLogo from './nomad_dark.png'
import {lightTheme} from './themeOptions'
import useThemeSettings from './useThemeSettings'

vi.mock('@mui/material', async (importOriginal) => ({
  ...(await importOriginal<typeof import('@mui/material')>()),
  useMediaQuery: vi.fn().mockReturnValue(true),
}))

vi.mock('../routing/Link', () => ({
  default: ({children}: {children: React.ReactNode}) => <div>{children}</div>,
}))

describe('Theme', async () => {
  it('renders something in the theme', () => {
    vi.mocked(useMediaQuery).mockReturnValueOnce(true)
    render(
      <Theme>
        <Typography variant='h1' color='primary'>
          Content
        </Typography>
      </Theme>,
    )
    const content = screen.getByText('Content')
    expect(content).toBeInTheDocument()
    expect(content).toHaveStyle(
      `font-family: ${lightTheme.typography.h1.fontFamily}`,
    )
    // expect(screen.getByText('Content')).toHaveStyle(
    //   `color: ${lightTheme.palette.primary.main}`,
    // )
  })
  it('provides light color mode', () => {
    vi.mocked(useMediaQuery).mockReturnValueOnce(true)
    const {result} = renderHook(() => useThemeSettings(), {
      wrapper: Theme,
    })
    expect(result.current.colorMode).toBe('light')
  })
  it('provides dark color mode', () => {
    vi.mocked(useMediaQuery).mockReturnValueOnce(false)
    const {result} = renderHook(() => useThemeSettings(), {
      wrapper: Theme,
    })
    expect(result.current.colorMode).toBe('dark')
  })
  it('toggles color mode', () => {
    vi.mocked(useMediaQuery).mockReturnValueOnce(true)

    function ColorModeToggle() {
      const {handleToggleColorMode} = useThemeSettings()
      return <Button onClick={handleToggleColorMode}>Toggle</Button>
    }

    render(
      <Theme>
        <ColorModeToggle />
        <Typography variant='h1' color='primary'>
          Content
        </Typography>
        <Logo respectColorMode />
      </Theme>,
    )
    const button = screen.getByRole('button')
    const content = screen.getByText('Content')
    const logo = screen.getByRole('img')
    expect(button).toBeInTheDocument()
    expect(content).toBeInTheDocument()
    // expect(content).toHaveStyle(`color: ${lightTheme.palette.primary.main}`)
    expect(logo).toHaveAttribute('src', lightLogo)
    fireEvent.click(button)
    // expect(content).toHaveStyle(`color: ${darkTheme.palette.primary.main}`)
    expect(logo).toHaveAttribute('src', darkLogo)
    fireEvent.click(button)
    // expect(content).toHaveStyle(`color: ${lightTheme.palette.primary.main}`)
    expect(logo).toHaveAttribute('src', lightLogo)
  })
})
