import {Box, PaletteMode, useMediaQuery} from '@mui/material'
import {
  ComponentsOverrides,
  ComponentsVariants,
  Theme as MuiTheme,
  ThemeProvider,
} from '@mui/material/styles'
import React, {ReactNode, useMemo, useState} from 'react'

import {SxProps} from '../../utils/types'
import {CardItemProps} from '../layout/CardItem'
import {NavAboutProps} from '../navigation/NavAbout'
import {NavItemProps} from '../navigation/NavItem'
import Link from '../routing/Link'
import lightLogo from './nomad.png'
import darkLogo from './nomad_dark.png'
import {darkTheme, lightTheme} from './themeOptions'
import {themeSettingsContext} from './useThemeSettings'

type Theme = Omit<MuiTheme, 'components'>

declare module '@mui/material/styles' {
  interface TypographyVariants {
    monospace: React.CSSProperties
  }

  interface TypographyVariantsOptions {
    monospace?: React.CSSProperties
  }

  interface ComponentNameToClassKey {
    NavItem: 'root'
    NavAbout: 'root'
    CardItem: 'root' | 'standard' | 'highlighted'
  }

  interface ComponentsPropsList {
    NavItem: Partial<NavItemProps>
    NavAbout: Partial<NavAboutProps>
    CardItem: Partial<CardItemProps>
  }

  interface Components {
    NavItem?: {
      defaultProps?: ComponentsPropsList['NavItem']
      styleOverrides?: ComponentsOverrides<Theme>['NavItem']
      variants?: ComponentsVariants['NavItem']
    }
    NavAbout?: {
      defaultProps?: ComponentsPropsList['NavAbout']
      styleOverrides?: ComponentsOverrides<Theme>['NavAbout']
      variants?: ComponentsVariants['NavAbout']
    }
    CardItem?: {
      defaultProps?: ComponentsPropsList['CardItem']
      styleOverrides?: ComponentsOverrides<Theme>['CardItem']
      variants?: ComponentsVariants['CardItem']
    }
  }

  interface TypeBackground {
    highlight: string
  }
}

declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    monospace: true
    caption: false
    body2: false
    subtitle1: false
  }
}

export function Logo({
  sx = {},
  respectColorMode,
}: {respectColorMode?: boolean} & SxProps) {
  const {colorMode} = React.useContext(themeSettingsContext)
  return (
    <Box
      sx={{
        width: '40px',
        height: '40px',
        cursor: 'pointer',
        padding: '2px',
        borderRadius: '8px',
        bgcolor: 'primary.contrastText',
        ...sx,
      }}
    >
      <Link to='/'>
        {!respectColorMode || colorMode === 'light' ? (
          <img width={36} alt='nomad homepage' src={lightLogo}></img>
        ) : (
          <img width={36} alt='nomad homepage' src={darkLogo}></img>
        )}
      </Link>
    </Box>
  )
}

export default function Theme({children}: {children?: ReactNode}) {
  const systemSettingsColorMode = useMediaQuery('(prefers-color-scheme: light)')
    ? 'light'
    : 'dark'
  const [mode, setMode] = useState<PaletteMode>(systemSettingsColorMode)
  const themeSettings = useMemo(
    () => ({
      colorMode: mode,
      handleToggleColorMode: () => {
        setMode((prevMode) => (prevMode === 'light' ? 'dark' : 'light'))
      },
    }),
    [mode, setMode],
  )
  const theme = useMemo(() => {
    return mode === 'dark' ? darkTheme : lightTheme
  }, [mode])
  return (
    <themeSettingsContext.Provider value={themeSettings}>
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </themeSettingsContext.Provider>
  )
}
