import {AppBarProps, PaletteMode, PaperProps, ThemeOptions} from '@mui/material'
import {createTheme} from '@mui/material/styles'

declare module '@mui/material/styles' {
  interface PaletteColor {
    lighter?: string
  }

  interface SimplePaletteColorOptions {
    lighter?: string
  }
}

const paperProps = {
  variant: 'outlined',
  elevation: undefined,
}

export const sharedThemeOptions: ThemeOptions = {
  typography: {
    fontFamily: ['"Titillium Web"', 'sans-serif'].join(','),
    h1: {
      fontSize: 32,
      lineHeight: '1.5em',
      fontWeight: 'bold',
    },
    h2: {
      fontSize: 24,
      lineHeight: '1.5em',
      fontWeight: 'bold',
    },
    h3: {
      fontSize: 18,
      lineHeight: '1.5em',
      fontWeight: 'bold',
    },
    h4: {
      fontSize: 16,
      lineHeight: '1.5em',
      fontWeight: 'bold',
    },
    h5: {
      fontSize: 14,
      lineHeight: '1.5em',
      fontWeight: 'bold',
    },
    h6: {
      fontSize: 12,
      lineHeight: '1.5em',
      fontWeight: 'bold',
    },
    button: {
      fontWeight: 700,
    },
    // overline: {
    //   fontSize: 14,
    //   textTransform: 'none',
    //   lineHeight: '1em',
    // },
    caption: undefined,
    body2: undefined,
    subtitle1: undefined,
    monospace: {
      fontFamily: ['"Roboto Mono"', 'monospace'].join(','),
    },
  },
  components: {
    MuiButton: {
      defaultProps: {
        disableElevation: true,
      },
      styleOverrides: {
        root: {
          whiteSpace: 'nowrap',
          minWidth: 'max-content',
        },
        contained: {
          borderRadius: 8,
        },
        outlined: {
          borderRadius: 8,
        },
      },
    },
    MuiButtonGroup: {
      defaultProps: {
        disableElevation: true,
        disableRipple: true,
      },
      styleOverrides: {
        root: {
          whiteSpace: 'nowrap',
          minWidth: 'max-content',
        },
        contained: {
          borderRadius: 8,
        },
        outlined: {
          borderRadius: 8,
        },
      },
    },
    MuiAppBar: {
      defaultProps: paperProps as Partial<AppBarProps>,
      styleOverrides: {
        root: {
          zIndex: 998,
        },
      },
    },
    MuiPaper: {
      defaultProps: paperProps as Partial<PaperProps>,
      styleOverrides: {
        rounded: {
          borderRadius: 8,
        },
      },
    },
    MuiChip: {
      styleOverrides: {
        root: {
          borderRadius: 8,
        },
      },
    },
    MuiIconButton: {
      defaultProps: {
        tabIndex: -1,
      },
      styleOverrides: {
        sizeSmall: {
          width: 32,
          height: 32,
          '& .MuiSvgIcon-root': {
            fontSize: '1.25rem',
          },
        },
      },
    },
    MuiToolbar: {
      styleOverrides: {
        root: {
          backgroundColor: 'transparent',
        },
      },
    },
    MuiTableCell: {
      styleOverrides: {
        head: {
          // Titillium Web does not have the default weight 500
          fontWeight: 700,
        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          '& .MuiOutlinedInput-root': {
            borderRadius: 8,
          },
        },
      },
    },
    MuiCardHeader: {
      styleOverrides: {
        title: {
          fontSize: 24,
        },
      },
    },
    MuiDialogTitle: {
      styleOverrides: {
        root: {
          fontSize: 24,
          color: 'primary',
        },
      },
    },
    MuiLink: {
      styleOverrides: {
        root: {
          color: 'primary',
        },
      },
    },
    MuiInput: {
      styleOverrides: {
        root: ({theme}) => ({
          // backgroundColor:
          //   theme.palette.mode === 'light'
          //     ? 'rgba(0,0,0,0.07)'
          //     : 'rgba(255,255,255,0.05)',
          // paddingLeft: 8,
          // paddingRight: 8,
          // marginTop: 4,
          // borderTopLeftRadius: '4px',
          // borderTopRightRadius: '4px',
          '&:before': {
            borderBottomColor: theme.palette.action.focus,
          },
        }),
      },
    },
    MuiGrid2: {
      defaultProps: {
        minWidth: 0,
      },
    },
    NavAbout: {
      styleOverrides: {
        root: ({theme}) => ({
          backgroundColor: theme.palette.background.paper,
          border: `1px solid ${theme.palette.action.focus}`,
        }),
      },
    },
    CardItem: {
      styleOverrides: {
        standard: {},
        highlighted: ({theme}) => ({
          backgroundColor: theme.palette.background.paper,
        }),
      },
    },
  },
}

export const lightTheme = createTheme({
  cssVariables: true,
  ...sharedThemeOptions,
  palette: {
    mode: 'light' as PaletteMode,
    primary: {
      main: '#2A4CDF',
      dark: '#192E86',
      light: '#7F94EC',
      lighter: '#c7d0f5',
    },
    secondary: {
      main: '#008A67',
      light: '#80C583',
      dark: '#00533e',
    },
    background: {
      paper: '#F7F7F9',
      default: '#FEFEFE',
      highlight: '#EFEFF7',
    },
    text: {
      primary: '#1D1D1D',
      secondary: '#666666',
      disabled: 'rgba(102,102,102,0.75)',
    },
  },
})

export const darkTheme = createTheme({
  cssVariables: true,
  ...sharedThemeOptions,
  palette: {
    mode: 'dark' as PaletteMode,
    primary: {
      main: '#8B8CEC',
      contrastText: 'rgba(255,255,255,0.85)',
      dark: '#6162A5',
      light: '#A2A3Ef',
    },
    secondary: {
      main: '#379777',
      contrastText: 'rgba(255,255,255,0.85)',
      light: '#5FAB92',
      dark: '#266953',
    },
    background: {
      paper: '#1c2026',
      default: '#121212',
      highlight: '#25262d',
    },
    text: {
      primary: 'rgba(255,255,255,0.85)',
      secondary: 'rgba(255,255,255,0.7)',
      disabled: 'rgba(255,255,255,0.65)',
    },
  },
})
