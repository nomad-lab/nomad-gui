import {PaletteMode} from '@mui/material'
import React, {useContext} from 'react'

export const themeSettingsContext = React.createContext(
  {} as {
    colorMode: PaletteMode
    handleToggleColorMode: () => void
  },
)

export default function useThemeSettings() {
  return useContext(themeSettingsContext)
}
