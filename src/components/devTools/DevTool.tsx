import {Box} from '@mui/material'
import {PropsWithChildren, useEffect} from 'react'

import {SxProps} from '../../utils/types'
import useDevTools from './useDevTools'

export type DevToolProps = PropsWithChildren<{
  toolKey: string
}> &
  SxProps

export default function DevTool({children, toolKey, sx}: DevToolProps) {
  const {registerTool, unregisterTool} = useDevTools()

  useEffect(() => {
    registerTool(toolKey, <Box sx={sx}>{children}</Box>)
    return () => unregisterTool(toolKey)
  }, [registerTool, unregisterTool, toolKey, children, sx])

  return ''
}
