import {Box} from '@mui/material'
import {JsonViewerProps, defineDataType} from '@textea/json-viewer'

import JsonViewer from '../fileviewer/JsonViewer'

const otherDataType = defineDataType({
  is: (value) => {
    return ['string', 'object', 'array', 'number'].indexOf(typeof value) === -1
  },
  Component: ({value}) => typeof value,
})

export default function DevToolsJsonViewer({
  value,
  defaultInspectDepth = 1,
  valueTypes,
  ...props
}: JsonViewerProps) {
  return (
    <Box sx={{padding: 1}}>
      <JsonViewer
        value={value}
        style={{fontSize: 10}}
        defaultInspectDepth={defaultInspectDepth}
        valueTypes={[...(valueTypes || []), otherDataType]}
        {...props}
      />
    </Box>
  )
}
