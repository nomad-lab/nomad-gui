import {Paper, Typography} from '@mui/material'
import {Meta, StoryObj} from '@storybook/react'

import DevTool from './DevTool'
import DevTools from './DevTools'

const meta = {
  title: 'components/devTools/DevTools',
  component: DevTools,
  tags: ['autodocs'],
} satisfies Meta<typeof DevTools>

export default meta

type Story = StoryObj<typeof meta>
export const Root: Story = {
  args: {
    enabled: true,
    open: true,
    defaultSize: {width: 300, height: 100},
    sx: {position: 'absolute', top: 2, right: 2},
    children: (
      <Paper sx={{padding: 1, width: '100%', height: 200}}>
        <Typography>Some other component</Typography>
        <DevTool toolKey='Tab 1' sx={{padding: 1}}>
          Tab 1 content
        </DevTool>
        <DevTool toolKey='Tab 2' sx={{padding: 1}}>
          Tab 2 content
        </DevTool>
      </Paper>
    ),
  },
}
