import {act, renderHook, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {vi} from 'vitest'

import {createMatchMedia} from '../../utils/test.helper'
import DevTool from './DevTool'
import DevTools from './DevTools'
import DevToolsJsonViewer from './DevToolsJsonViewer'
import useDevTools from './useDevTools'

export async function renderDevTools({
  open = true,
  controlled = false,
  tools,
  tabs = [],
  activeToolKey,
  enabled = true,
}: {
  open?: boolean
  controlled?: boolean
  tools?: React.ReactNode
  tabs?: {toolKey: string; content: React.ReactNode}[]
  activeToolKey?: string
  enabled?: boolean
}) {
  vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia())
  const {result, ...rest} = renderHook(() => useDevTools(), {
    wrapper: ({children, ...props}) => (
      <DevTools
        enabled={enabled}
        open={controlled ? open : undefined}
        {...props}
      >
        {tools}
        {children}
      </DevTools>
    ),
  })
  if (open && !controlled) {
    act(() => result.current.handleOpenClose())
  }
  for (const {toolKey, content} of tabs) {
    act(() => result.current.registerTool(toolKey, content))
  }
  if (activeToolKey) {
    await userEvent.click(screen.getByText(activeToolKey))
  }
  return {result, ...rest}
}

describe('DevTools', () => {
  it('shows registered tools when controlled open', async () => {
    const {result} = await renderDevTools({
      open: true,
      controlled: true,
      tabs: [{toolKey: 'tool-key', content: 'tool-content'}],
    })

    expect(result.current.open).toBe(true)
    expect(screen.getByText('tool-key')).toBeInTheDocument()
    expect(screen.getByText('tool-content')).toBeInTheDocument()

    act(() => result.current.handleOpenClose()) // should have no effect
    expect(result.current.open).toBe(true)
    expect(screen.getByText('tool-key')).toBeInTheDocument()
    expect(screen.getByText('tool-content')).toBeInTheDocument()
  })

  it('shows registered tools when opened', async () => {
    const {result} = await renderDevTools({
      open: false,
      tabs: [{toolKey: 'tool-key', content: 'tool-content'}],
    })

    expect(result.current.open).toBe(false)
    expect(screen.queryByText('tool-key')).not.toBeInTheDocument()
    expect(screen.queryByText('tool-content')).not.toBeInTheDocument()

    act(() => result.current.handleOpenClose())

    expect(screen.getByText('tool-key')).toBeInTheDocument()
    expect(screen.getByText('tool-content')).toBeInTheDocument()
  })

  it('shows registered tools immediately', async () => {
    const {result} = await renderDevTools({})

    expect(result.current.open).toBe(true)
    act(() => result.current.registerTool('tool-key', 'tool-content'))

    expect(screen.getByText('tool-key')).toBeInTheDocument()
    expect(screen.getByText('tool-content')).toBeInTheDocument()
  })

  it('unregistered tools disappear immediately', async () => {
    const {result} = await renderDevTools({
      tabs: [{toolKey: 'tool-key', content: 'tool-content'}],
    })

    expect(screen.getByText('tool-key')).toBeInTheDocument()
    expect(screen.getByText('tool-content')).toBeInTheDocument()

    act(() => result.current.unregisterTool('tool-key'))
    expect(screen.queryByText('tool-key')).not.toBeInTheDocument()
  })

  it('does not show tools when disabled', async () => {
    const {result} = await renderDevTools({enabled: false})
    expect(result.current.open).toBe(false)
    act(() => result.current.handleOpenClose())
    expect(result.current.open).toBe(false)
  })

  it('switches tabs', async () => {
    await renderDevTools({
      tabs: [
        {toolKey: 'tool-key-1', content: 'tool-content-1'},
        {toolKey: 'tool-key-2', content: 'tool-content-2'},
      ],
    })

    expect(screen.getByText('tool-key-1')).toBeInTheDocument()
    expect(screen.getByText('tool-content-1')).toBeInTheDocument()
    expect(screen.queryByText('tool-content-2')).not.toBeInTheDocument()

    await userEvent.click(screen.getByText('tool-key-2'))
    expect(screen.getByText('tool-content-2')).toBeInTheDocument()
    expect(screen.queryByText('tool-content-1')).not.toBeInTheDocument()
  })

  it('shows DevToolsTab tools', async () => {
    await renderDevTools({
      tools: <DevTool toolKey='tool-label'>tool-content</DevTool>,
    })
    expect(screen.getByText('tool-label')).toBeInTheDocument()
    expect(screen.getByText('tool-content')).toBeInTheDocument()
  })

  it('shows DevToolsTab tools with DevToolsJsonViewer', async () => {
    await renderDevTools({
      tools: (
        <DevTool toolKey='tool-label'>
          <DevToolsJsonViewer value={{testKey: 'test-value'}} />
        </DevTool>
      ),
    })

    expect(screen.getByText('tool-label')).toBeInTheDocument()
    expect(screen.getByText(/testKey/)).toBeInTheDocument()
    expect(screen.getByText(/test-value/)).toBeInTheDocument()
  })
})
