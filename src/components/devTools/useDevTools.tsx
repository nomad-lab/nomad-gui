import React, {ReactNode} from 'react'

export type DevToolsContextValue = {
  /**
   * Registers a tool, i.e. adds it as a tab. The toolKey should be unique.
   */
  registerTool: (toolKey: string, content: ReactNode) => void
  /**
   * Unregisters a tool.
   */
  unregisterTool: (toolKey: string) => void
  /**
   * Allows to toggle the open/close state if DevTools is used uncontrolled.
   */
  handleOpenClose: () => void
  /**
   * The open state.
   */
  open: boolean
}

export const devToolsContext = React.createContext<DevToolsContextValue>({
  registerTool: () => {},
  unregisterTool: () => {},
  handleOpenClose: () => {},
  open: false,
})

export default function useDevTools() {
  return React.useContext(devToolsContext)
}
