import {Box, Divider, Paper, Tab, Tabs} from '@mui/material'
import {Resizable} from 're-resizable'
import React, {PropsWithChildren, useCallback, useMemo, useState} from 'react'

import {SxProps} from '../../utils/types'
import {DevToolsContextValue, devToolsContext} from './useDevTools'

type DevToolsContainerProps = PropsWithChildren & {
  defaultSize?: {width: number; height: number}
} & SxProps

function DevToolsContainer({
  children,
  defaultSize,
  ...paperProps
}: DevToolsContainerProps) {
  return (
    <Paper
      elevation={3}
      sx={{
        position: 'fixed',
        bottom: 16,
        left: 81,
        zIndex: 10000,
        ...(paperProps.sx || {}),
      }}
      {...paperProps}
    >
      <Resizable
        defaultSize={
          defaultSize || {
            width: 320,
            height: 500,
          }
        }
        style={{
          position: 'initial',
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        {children}
      </Resizable>
    </Paper>
  )
}

function DevToolsTabs({tools}: {tools: {[key: string]: React.ReactNode}}) {
  const [tabValue, setTabValue] = useState<string | undefined>(undefined)

  let currentTabValue: string | undefined
  if (tabValue && tools[tabValue]) {
    currentTabValue = tabValue
  } else {
    currentTabValue = Object.keys(tools)[0] as string | undefined
  }

  if (!currentTabValue) {
    return ''
  }

  return (
    <>
      <Tabs
        value={currentTabValue}
        onChange={(_, newValue) => setTabValue(newValue)}
      >
        {Object.keys(tools)
          .toSorted()
          .map((value, index) => (
            <Tab key={index} label={value} value={value} />
          ))}
      </Tabs>
      <Divider />
      <Box sx={{overflow: 'auto', flexGrow: 1}}>{tools[currentTabValue]}</Box>
    </>
  )
}

export type DevToolsProps = {
  /**
   * If not enabled, the children will be rendered without the dev tools.
   * The useDevTools hook will return a usable context, but using it will
   * have no effect.
   */
  enabled?: boolean
  /**
   * Optional open to control the open state. This will disable the open/close
   * functionality via useDevTools().handleOpenClose.
   */
  open?: boolean
} & DevToolsContainerProps

export default function DevTools({
  children,
  enabled,
  open: controlledOpen,
  ...devToolsContainerProps
}: DevToolsProps) {
  const isControlled = controlledOpen !== undefined
  const [uncontrolledOpen, setUnControlledOpen] = useState(false)
  const open = isControlled ? controlledOpen : uncontrolledOpen
  const [tools, setTools] = useState<{[key: string]: React.ReactNode}>({})
  const handleOpenClose = useCallback(() => {
    if (isControlled) {
      return
    }
    setUnControlledOpen((open) => !open)
  }, [setUnControlledOpen, isControlled])

  const registerTool = useCallback(
    (toolKey: string, content: React.ReactNode) => {
      setTools((tools) => ({...tools, [toolKey]: content}))
    },
    [setTools],
  )

  const unregisterTool = useCallback(
    (toolKey: string) => {
      setTools((tools) => {
        const newTabs = {...tools}
        delete newTabs[toolKey]
        return newTabs
      })
    },
    [setTools],
  )

  const devToolsContextValue = useMemo<DevToolsContextValue>(() => {
    return {
      registerTool,
      unregisterTool,
      open,
      handleOpenClose,
    }
  }, [open, handleOpenClose, registerTool, unregisterTool])

  if (!enabled) {
    return children
  }

  return (
    <>
      {open && (
        <DevToolsContainer {...devToolsContainerProps}>
          <DevToolsTabs tools={tools} />
        </DevToolsContainer>
      )}
      <devToolsContext.Provider value={devToolsContextValue}>
        {children}
      </devToolsContext.Provider>
    </>
  )
}
