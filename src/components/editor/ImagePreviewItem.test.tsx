import {render, screen} from '@testing-library/react'

import {addDefinitions, mockArchive} from '../archive/archive.helper'
import {Layout, LayoutItem} from '../layout/Layout'

describe('ImagePreview', async () => {
  const layout: LayoutItem = {
    type: 'imagePreview',
    property: 'test_quantity',
  }

  function mockData(withLoading: boolean = false) {
    const data = addDefinitions({
      test_quantity: 'http://some.url/image.jpg',
    })
    return mockArchive({data: withLoading ? undefined : data})
  }

  it('initially renders with loading indicator', async () => {
    mockData(true)
    render(<Layout layout={layout} />)
    const image = screen.getByAltText('test_quantity')
    expect(image).not.toHaveAttribute('src')
  })

  it('renders the image', async () => {
    mockData()
    render(<Layout layout={layout} />)
    const image = screen.getByAltText('test_quantity')
    expect(image).toHaveAttribute('src', 'http://some.url/image.jpg')
  })
})
