import {SubSection} from './SubSectionEditor'
import {
  calculateRequestFromLayout,
  defaultSubSectionRequest,
  mergeRequests,
} from './utils'

describe('calculateRequestFromLayout', async () => {
  it('adds quantities', () => {
    const request = {}
    calculateRequestFromLayout(
      {
        type: 'quantity',
        property: 'test_quantity',
      },
      request,
    )
    expect(request).toEqual({...defaultSubSectionRequest, test_quantity: '*'})
  })
  it('adds nested quantities', () => {
    const request = {}
    calculateRequestFromLayout(
      {
        type: 'container',
        children: [
          {
            type: 'quantity',
            property: 'nested.test_quantity1',
          },
          {
            type: 'quantity',
            property: 'nested.test_quantity2',
          },
        ],
      },
      request,
    )
    expect(request).toEqual({
      ...defaultSubSectionRequest,
      nested: {
        ...defaultSubSectionRequest,
        test_quantity1: '*',
        test_quantity2: '*',
      },
    })
  })
  it.each([[false], [true]])('adds subsections with repeats=%s', (repeats) => {
    const request = {}
    calculateRequestFromLayout(
      {
        type: 'subSection',
        property: 'nested',
        repeats,
        layout: {
          type: 'container',
          children: [
            {
              type: 'quantity',
              property: 'test_quantity',
            },
          ],
        },
      } as SubSection,
      request,
    )
    expect(request).toEqual({
      ...defaultSubSectionRequest,
      nested: {...defaultSubSectionRequest, test_quantity: '*'},
    })
  })
})

describe('mergeRequests', () => {
  it.each([
    {
      description: 'should merge non-conflicting objects successfully',
      current: {a: 1, b: {c: 2}},
      request: {d: 3, b: {e: 4}},
      expected: {a: 1, b: {c: 2, e: 4}, d: 3},
    },
    {
      description: 'should merge when nested values are non-conflicting',
      current: {a: {b: {c: 1}}},
      request: {a: {b: {d: 2}}},
      expected: {a: {b: {c: 1, d: 2}}},
    },
    {
      description: 'should handle empty request objects',
      current: {a: 1, b: {c: 2}},
      request: {},
      expected: {a: 1, b: {c: 2}},
    },
    {
      description: 'should handle empty current objects',
      current: {},
      request: {a: 1, b: {c: 2}},
      expected: {a: 1, b: {c: 2}},
    },
    {
      description: 'should merge deeply nested non-conflicting objects',
      current: {a: {b: {c: {d: 1}}}},
      request: {a: {b: {c: {e: 2, f: {g: 3}}}}},
      expected: {a: {b: {c: {d: 1, e: 2, f: {g: 3}}}}},
    },
    {
      description: 'should throw an error on conflicting primitive values',
      current: {a: 1, b: 2},
      request: {b: 1},
      error:
        'Conflicting layout data requests for the same property. Conflict detected at path: b',
    },
    {
      description: 'should throw an error on conflicting nested values',
      current: {a: {b: {c: 1}}, x: {y: {z: 1}}},
      request: {a: {b: {c: 1}}, x: {y: {z: 2}}},
      error:
        'Conflicting layout data requests for the same property. Conflict detected at path: x.y.z',
    },
    {
      description: 'should throw an error for conflicts in arrays',
      current: {a: {b: [1, 2, 3]}},
      request: {a: {b: [1, 5, 6]}},
      error:
        'Conflicting layout data requests for the same property. Conflict detected at path: a.b.1',
    },
    {
      description:
        'should throw an error when a primitive is replaced with an object',
      current: {a: {b: 1}},
      request: {a: {b: {c: 2}}},
      error:
        'Conflicting layout data requests for the same property. Conflict detected at path: a.b',
    },
    {
      description:
        'should throw an error when an object is replaced with a primitive',
      current: {a: {b: {c: 2}}},
      request: {a: {b: 1}},
      error:
        'Conflicting layout data requests for the same property. Conflict detected at path: a.b',
    },
  ])('$description', ({current, request, expected, error}) => {
    if (error) {
      expect(() => mergeRequests(current, request)).toThrow(new Error(error))
    } else {
      mergeRequests(current, request)
      expect(current).toEqual(expected)
    }
  })
})
