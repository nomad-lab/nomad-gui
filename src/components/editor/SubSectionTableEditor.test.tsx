import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import {addDefinitions, mockArchive} from '../archive/archive.helper'
import {Layout} from '../layout/Layout'
import {cell} from '../richTable/RichTableEditor.helper'
import {SubSectionTable} from './SubSectionTableEditor'

describe('SubSectionTableEditor', async () => {
  const layout = {
    type: 'table',
    property: 'test_subsection',
    columns: [
      {
        property: 'quantity',
      },
      {
        property: 'editable_quantity',
        editable: true,
      },
    ],
  } as SubSectionTable

  function mockData(args?: {withLoading?: boolean}) {
    const {withLoading = false} = args || {}

    const defaultSubSectionData = {
      quantity: 'value',
      editable_quantity: 'editable_value',
    }

    const subSectionData = [defaultSubSectionData]
    const data = addDefinitions({
      ...(subSectionData
        ? {
            test_subsection: subSectionData,
          }
        : {}),
    })
    return mockArchive({
      data: withLoading ? undefined : data,
    })
  }

  it('initially renders with loading indicator', async () => {
    mockData({withLoading: true})
    render(<Layout layout={layout} />)
    expect(screen.queryByText('value')).not.toBeInTheDocument()
  })

  it('renders with title', async () => {
    mockData()
    render(<Layout layout={{title: 'title', ...layout}} />)
    expect(screen.getByText('title')).toBeInTheDocument()
  })

  it('renders with data', async () => {
    mockData()
    render(<Layout layout={layout} />)
    expect(screen.getByText('quantity')).toBeInTheDocument()
    expect(screen.getByText('value')).toBeInTheDocument()
    expect(screen.getByText('editable quantity')).toBeInTheDocument()
    expect(screen.getByDisplayValue('editable_value')).toBeInTheDocument()
  })

  it('allows to edit cells', async () => {
    const archive = mockData()
    render(<Layout layout={layout} />)
    await userEvent.click(cell('editable_value'))
    await userEvent.keyboard('{enter}new_value{enter}')

    expect(archive.changeStack.length).toBe(1)
    expect(archive.changeStack[0]).toMatchObject({
      action: 'upsert',
      new_value: 'new_value',
      path: 'test_subsection/0/editable_quantity',
    })
  })

  it('allows to remove sub section', async () => {
    const archive = mockData()
    render(<Layout layout={layout} />)
    await userEvent.click(screen.getByTestId('DeleteIcon'))

    expect(archive.changeStack.length).toBe(1)
    expect(archive.changeStack[0]).toMatchObject({
      action: 'remove',
      new_value: {},
      path: 'test_subsection/0',
    })
  })

  it('allows to add sub section', async () => {
    const archive = mockData()
    render(<Layout layout={layout} />)
    const addButton = screen.getByRole('button', {name: 'Add test subsection'})
    expect(addButton).toBeInTheDocument()
    await userEvent.click(addButton)

    expect(archive.changeStack.length).toBe(1)
    expect(archive.changeStack[0]).toMatchObject({
      action: 'upsert',
      new_value: expect.objectContaining({}),
      path: 'test_subsection/1',
    })
  })
})
