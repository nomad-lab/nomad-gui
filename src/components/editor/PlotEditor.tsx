import {Skeleton} from '@mui/material'
import {useLayoutEffect, useRef} from 'react'
import {useAsync} from 'react-use'

import {useArchiveProperty} from '../archive/useArchive'
import {LayoutItem} from '../layout/Layout'
import {AbstractProperty} from './PropertyEditor'
import useSubSectionPath from './useSubSectionPath'

export type Plot = AbstractProperty & {
  type: 'plot'
}

export default function PlotEditor({layout}: {layout: LayoutItem}) {
  const plot = layout as Plot
  const {property} = plot
  const path = useSubSectionPath(property)
  const {value, loading} =
    useArchiveProperty<
      {figure: {data: Plotly.Data[]; layout: Plotly.Layout}}[]
    >(path)
  const figureElementRef = useRef<HTMLDivElement | null>(null)

  const Plotly = useAsync(async () => {
    return await import('plotly.js-basic-dist-min')
  }, [])

  const figure = value?.[0].figure

  useLayoutEffect(() => {
    if (figureElementRef.current && figure && Plotly.value) {
      Plotly.value.react(
        figureElementRef.current,
        figure.data,
        {
          ...figure.layout,
          title: undefined,
          margin: {l: 32, r: 16, t: 16, b: 32},
        },
        {
          responsive: true,
        },
      )
    }
  }, [figureElementRef, figure, Plotly.value])

  const figureElement = <div ref={figureElementRef} />

  if (loading) {
    return <Skeleton width={'100%'}>{figureElement}</Skeleton>
  }
  return figureElement
}
