import {Skeleton} from '@mui/material'

import {useArchiveProperty} from '../archive/useArchive'
import {LayoutItem} from '../layout/Layout'
import {AbstractProperty} from './PropertyEditor'
import useSubSectionPath from './useSubSectionPath'

export type ImagePreview = AbstractProperty & {
  type: 'imagePreview'
}

export default function ImagePreviewItem({layout}: {layout: LayoutItem}) {
  const richText = layout as ImagePreview
  const {property} = richText
  const path = useSubSectionPath(property)
  const {value, loading} = useArchiveProperty<string>(path)
  const imageElement = <img src={value} alt={property} width={'100%'} />
  if (loading) {
    return <Skeleton width={'100%'}>{imageElement}</Skeleton>
  }
  return imageElement
}
