import {Skeleton} from '@mui/material'
import {useCallback} from 'react'

import {Quantity as QuantityDefinition} from '../../utils/metainfo'
import {useArchiveProperty} from '../archive/useArchive'
import {LayoutItem} from '../layout/Layout'
import ContainerProps, {
  DynamicContainerProps,
} from '../values/containers/ContainerProps'
import DynamicValue from '../values/utils/DynamicValue'
import {
  DynamicComponentSpec,
  createDynamicComponentSpec,
} from '../values/utils/dynamicComponents'
import {AbstractProperty, getLabel} from './PropertyEditor'
import useSubSectionPath from './useSubSectionPath'

export type Quantity = AbstractProperty & {
  type: 'quantity'

  /**
   * A dynamic value component specification that defines how the value should be
   * displayed and edited. If not provided, a default value specification will be
   * generated from the quantity. The given specification can also be partial and it
   * wil be merged with the default one.
   */
  component?: DynamicComponentSpec
} & Pick<
    DynamicContainerProps,
    | 'label'
    | 'helperText'
    | 'placeholder'
    | 'editable'
    | 'displayUnit'
    | 'displayUnitLocked'
  >

/**
 * Renders a field with respect to the given quantity layout item.
 * It provides the value and handles the on change events in the
 * context of an SectionEditor.
 */
export default function QuantityEditor({layout}: {layout: LayoutItem}) {
  const quantity = layout as Quantity
  const {
    type,
    property,
    label,
    placeholder,
    component: componentOrUndefined,
    ...containerProps
  } = quantity

  const path = useSubSectionPath(property)

  const {value, loading, change, getDefinition} = useArchiveProperty<
    unknown,
    QuantityDefinition
  >(path)

  const handleChange = useCallback(
    (newValue: unknown) => {
      change('upsert', newValue)
    },
    [change],
  )

  // The quantity definition is not available until the data (which holds the
  // m_def) is loaded. This means we cannot create a default component spec here
  // to compute the right skeleton height and have to render with a standard size.
  if (loading) {
    return (
      <Skeleton variant='rounded' component='div' width={'100%'} height={48} />
    )
  }

  const definition = getDefinition()
  const defaultComponent = definition
    ? createDynamicComponentSpec(definition)
    : undefined

  const component = componentOrUndefined || defaultComponent || 'Text'

  const props = {
    label: label || getLabel(quantity),
    placeholder: placeholder || 'no value',
    fullWidth: true,
    ...containerProps,
    value,
    onChange: handleChange,
  } satisfies ContainerProps

  return (
    <DynamicValue
      defaultComponent={defaultComponent}
      component={component}
      {...props}
    />
  )
}
