import React from 'react'

type SubSectionContextValue = {
  path?: string
}

export const subSectionContext = React.createContext<SubSectionContextValue>({})

export default function useSubSectionPath(property: string) {
  const {path} = React.useContext(subSectionContext)
  return path ? `${path}/${property}` : property
}
