import React from 'react'

import {AbstractItem, LayoutItem} from '../layout/Layout'

export type Element = AbstractItem & {
  type: 'element'
  element: React.ReactNode
}

export default function ElementItem({layout}: {layout: LayoutItem}) {
  const {element} = layout as Element

  return element
}
