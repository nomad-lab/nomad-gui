import {Typography} from '@mui/material'
import {render, screen} from '@testing-library/react'

import {Layout, LayoutItem} from '../layout/Layout'

describe('ElementItem', () => {
  it('renders', () => {
    const layout = {
      type: 'element',
      element: <Typography>test</Typography>,
    } as LayoutItem
    render(<Layout layout={layout} />)
    expect(screen.getByText('test')).toBeInTheDocument()
  })
})
