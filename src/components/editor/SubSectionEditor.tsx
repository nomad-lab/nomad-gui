import {Box, Button, IconButton} from '@mui/material'
import Grid from '@mui/material/Grid2'
import {PropsWithChildren, useCallback} from 'react'

import {MSectionResponse} from '../../models/graphResponseModels'
import {SubSection as SubSectionDefinition} from '../../utils/metainfo'
import {assert, splice} from '../../utils/utils'
import {ArchiveProperty, useArchiveProperty} from '../archive/useArchive'
import {Remove} from '../icons'
import {Item, Layout, LayoutItem} from '../layout/Layout'
import {layoutPropsContext} from '../layout/useLayoutProps'
import {AbstractProperty, getLabel} from './PropertyEditor'
import useSubSectionPath, {subSectionContext} from './useSubSectionPath'

function SectionProvider({
  path: path,
  children,
}: {path: string} & PropsWithChildren) {
  return (
    <subSectionContext.Provider value={{path}}>
      {children}
    </subSectionContext.Provider>
  )
}

function SubSectionComponent({
  label,
  layout,
  property,
}: {
  label: string
  layout: LayoutItem
  property: ArchiveProperty<SubSectionData, SubSectionDefinition>
}) {
  const {value, getDefinition, change, path} = property
  const repeats = Array.isArray(value)

  const handleRemove = useCallback(
    (index?: number) => {
      if (repeats) {
        assert(
          index !== undefined,
          'Update operation on repeating sub section without index',
        )
        change(
          'remove',
          splice((value || []) as MSectionResponse[], index, 1),
          index,
        )
      } else {
        change('remove', undefined)
      }
    },
    [repeats, change, value],
  )

  const handleAdd = useCallback(() => {
    // TODO the defintion might not be available. With an improved
    // useArchive implementation, it might still be just a references and
    // we might still need to explicitly fetch to actual definition.
    // I.e., getDefinition() might need to be async.
    const newSubSectionData = {
      m_def: getDefinition()?.sub_section,
    } as MSectionResponse
    if (repeats) {
      change(
        'upsert',
        [...((value || []) as MSectionResponse[]), newSubSectionData],
        (value || []).length,
      )
    } else {
      change('upsert', newSubSectionData)
    }
  }, [repeats, change, value, getDefinition])

  const addButtonLabel = `Add ${label}`

  if (repeats) {
    return (
      <>
        <Grid container rowSpacing={2} columnSpacing={2}>
          {((value || []) as MSectionResponse[]).map((value, index) => (
            <Item key={index} layout={layout}>
              <layoutPropsContext.Provider
                key={index}
                value={{
                  action: (
                    <IconButton
                      size='small'
                      onClick={() => handleRemove(index)}
                      aria-label='delete'
                    >
                      <Remove />
                    </IconButton>
                  ),
                }}
              >
                <SectionProvider path={`${path}/${index}`}>
                  <Layout layout={layout} />
                </SectionProvider>
              </layoutPropsContext.Provider>
            </Item>
          ))}
        </Grid>
        <Box sx={{display: 'flex', flexDirection: 'row'}}>
          <Box sx={{flexGrow: 1}} />
          <Button
            variant='contained'
            sx={{marginTop: 2}}
            onClick={handleAdd}
            aria-label='add'
            title='add'
          >
            {addButtonLabel}
          </Button>
        </Box>
      </>
    )
  }

  if (value === undefined) {
    return (
      <Button variant='contained' onClick={handleAdd} aria-label='add'>
        {addButtonLabel}
      </Button>
    )
  }

  return (
    <layoutPropsContext.Provider
      value={
        path === 'data' // TODO this is a hack to avoid the remove button in the root section
          ? {}
          : {
              action: (
                <IconButton
                  size='small'
                  onClick={() => handleRemove()}
                  aria-label='delete'
                >
                  <Remove />
                </IconButton>
              ),
            }
      }
    >
      <SectionProvider path={path}>
        <Layout layout={layout} />
      </SectionProvider>
    </layoutPropsContext.Provider>
  )
}
export type SubSection = AbstractProperty & {
  type: 'subSection'
  layout: LayoutItem
}

type SubSectionData = MSectionResponse | MSectionResponse[] | undefined

/**
 * Renders all sections in a sub section including actions to modify
 * the sub section. Uses SectionEditor to render each sub section.
 */
export default function SubSectionEditor({layout}: {layout: LayoutItem}) {
  const subSection = layout as SubSection
  const {property: propertyName, layout: subSectionLayout} = subSection
  const label = getLabel(subSection)
  const path = useSubSectionPath(propertyName)

  const property = useArchiveProperty<SubSectionData, SubSectionDefinition>(
    path,
  )
  const {loading} = property

  if (loading) {
    return (
      <SectionProvider path={path}>
        <Layout layout={subSection.layout} />
      </SectionProvider>
    )
  }

  return (
    <SubSectionComponent
      label={label}
      layout={subSectionLayout}
      property={property}
    />
  )
}
