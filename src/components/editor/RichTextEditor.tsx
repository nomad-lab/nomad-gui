import {Skeleton} from '@mui/material'
import {useCallback, useRef} from 'react'

import {useArchiveProperty} from '../archive/useArchive'
import {LayoutItem} from '../layout/Layout'
import Editor, {RichTextEditorProps} from '../richText/RichTextEditor'
import {AbstractProperty, getLabel} from './PropertyEditor'
import useSubSectionPath from './useSubSectionPath'

export type RichText = AbstractProperty & {
  type: 'richText'
  props?: RichTextEditorProps
}

export default function RichTextEditor({layout}: {layout: LayoutItem}) {
  const richText = layout as RichText
  const {property, props = {}} = richText
  const path = useSubSectionPath(property)
  const {value, loading, change} = useArchiveProperty<string>(path)
  const initialValueRef = useRef<string | undefined>(undefined)

  const handleChange = useCallback(
    (newValue: string) => {
      change('upsert', newValue)
    },
    [change],
  )

  if (!initialValueRef.current && value) {
    initialValueRef.current = value
  }

  const richtTextElement = (
    <Editor
      initialValue={initialValueRef.current}
      placeholder={`Enter a ${getLabel(richText)} ...`}
      onChange={handleChange}
      {...props}
    />
  )
  if (loading) {
    return <Skeleton width={'100%'}>{richtTextElement}</Skeleton>
  }
  return richtTextElement
}
