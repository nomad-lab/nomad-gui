import {render, renderHook, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import {addDefinitions, mockArchive} from '../archive/archive.helper'
import {useArchiveProperty} from '../archive/useArchive'
import {Layout, LayoutItem} from '../layout/Layout'

describe('QuantityEditor', async () => {
  const layout: LayoutItem = {
    type: 'quantity',
    property: 'test_quantity',
    editable: true,
  }

  function mockData(withLoading: boolean = false) {
    const data = addDefinitions({
      test_quantity: 'test_value',
    })
    return mockArchive({
      data: withLoading ? undefined : data,
    })
  }

  it('initially renders with loading indicator', async () => {
    mockData(true)
    render(<Layout layout={layout} />)
    expect(screen.queryByDisplayValue('test_value')).not.toBeInTheDocument()
  })

  it('allows to change the value', async () => {
    const archive = mockData()
    expect(archive.archive.test_quantity).toBe('test_value')
    const {result} = renderHook(() => useArchiveProperty('test_quantity'))
    expect(result.current.value).toBe('test_value')
    render(<Layout layout={layout} />)
    const value = screen.getByDisplayValue('test_value')
    expect(value).toBeInTheDocument()
    await userEvent.type(value, 'typed{enter}')
    expect(archive.changeStack.length).toBe(1)
    expect(archive.changeStack[0]).toMatchObject({
      action: 'upsert',
      new_value: 'test_valuetyped',
      path: 'test_quantity',
    })
  })

  it('allows to remove the value', async () => {
    const archive = mockData()
    render(<Layout layout={layout} />)
    const value = screen.getByDisplayValue('test_value')
    expect(value).toBeInTheDocument()
    await userEvent.clear(value)
    await userEvent.type(value, '{enter}')
    expect(archive.changeStack.length).toBe(1)
    expect(archive.changeStack[0]).toMatchObject({
      action: 'upsert',
      new_value: undefined,
      path: 'test_quantity',
    })
  })
})
