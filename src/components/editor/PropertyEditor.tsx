import {AbstractItem} from '../layout/Layout'

export type AbstractProperty = AbstractItem & {
  property: string
  label?: string
}

export function getLabel<T extends AbstractProperty>(layout: T) {
  const {property, label} = layout
  return label || getPropertyLabel(property)
}

export function getPropertyLabel(name: string) {
  return name.replaceAll('_', ' ')
}
