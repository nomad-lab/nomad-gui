import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import {addDefinitions, mockArchive} from '../archive/archive.helper'
import {Layout} from '../layout/Layout'
import {SubSection} from './SubSectionEditor'

describe('SubSectionEditor', async () => {
  const layout = {
    type: 'subSection',
    property: 'test_subsection',
    layout: {
      type: 'card',
      children: [
        {
          type: 'quantity',
          property: 'test_quantity',
        },
      ],
    },
  } as SubSection

  function mockData(args?: {
    repeats?: boolean
    withLoading?: boolean
    empty?: boolean
    definitionsExtra?: Record<string, unknown>
  }) {
    const {
      repeats = false,
      withLoading = false,
      empty = false,
      definitionsExtra,
    } = args || {}
    const defaultSubSectionData = {
      test_quantity: 'test_value',
    }
    let subSectionData
    if (repeats) {
      if (empty) {
        subSectionData = []
      } else {
        subSectionData = [defaultSubSectionData]
      }
    } else {
      if (empty) {
        subSectionData = undefined
      } else {
        subSectionData = defaultSubSectionData
      }
    }
    const data = addDefinitions(
      {
        ...(subSectionData
          ? {
              test_subsection: subSectionData,
            }
          : {}),
      },
      definitionsExtra,
    )
    return mockArchive({
      data: withLoading ? undefined : data,
    })
  }

  it('initially renders with loading indicator', async () => {
    mockData({withLoading: true})
    render(<Layout layout={layout} />)
    expect(screen.queryByText('test_value')).not.toBeInTheDocument()
  })

  it('allows to remove non repeating sub section', async () => {
    const archive = mockData()
    render(<Layout layout={layout} />)
    expect(screen.getByText('test_value')).toBeInTheDocument()
    await userEvent.click(screen.getByRole('button', {name: 'delete'}))

    expect(archive.changeStack.length).toBe(1)
    expect(archive.changeStack[0]).toMatchObject({
      action: 'remove',
      new_value: undefined,
      path: 'test_subsection',
    })
  })

  it('allows to add non repeating sub section', async () => {
    const archive = mockData({
      empty: true,
      definitionsExtra: {test_subsection: {test_quantity: 'test_value'}},
    })
    render(<Layout layout={layout} />)
    const addButton = screen.getByRole('button', {name: 'add'})
    expect(addButton).toBeInTheDocument()
    await userEvent.click(addButton)

    expect(archive.changeStack.length).toBe(1)
    expect(archive.changeStack[0]).toMatchObject({
      action: 'upsert',
      new_value: expect.objectContaining({m_def: expect.objectContaining({})}),
      path: 'test_subsection',
    })
  })

  it('allows to remove repeating sub section', async () => {
    const archive = mockData({repeats: true})
    render(<Layout layout={layout} />)
    expect(screen.getByText('test_value')).toBeInTheDocument()
    await userEvent.click(screen.getByRole('button', {name: 'delete'}))

    expect(archive.changeStack.length).toBe(1)
    expect(archive.changeStack[0]).toMatchObject({
      action: 'remove',
      new_value: {},
      path: 'test_subsection/0',
    })
  })

  it('allows to add repeating sub section', async () => {
    const archive = mockData({
      empty: true,
      repeats: true,
      definitionsExtra: {test_subsection: {test_quantity: 'test_value'}},
    })
    render(<Layout layout={layout} />)
    const addButton = screen.getByRole('button', {name: 'add'})
    expect(addButton).toBeInTheDocument()
    await userEvent.click(addButton)

    expect(archive.changeStack.length).toBe(1)
    expect(archive.changeStack[0]).toMatchObject({
      action: 'upsert',
      new_value: expect.objectContaining({}),
      path: 'test_subsection/0',
    })
  })
})
