import {render, screen, waitFor} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {useDebounce} from 'react-use'
import {vi} from 'vitest'

import * as useEditDebounce from '../../hooks/useEditDebounce'
import {addDefinitions, mockArchive} from '../archive/archive.helper'
import {Layout, LayoutItem} from '../layout/Layout'
import {RichTextEditorProps} from '../richText/RichTextEditor'

describe('RichTextEditor', async () => {
  const createLayout = (props?: RichTextEditorProps) =>
    ({
      type: 'richText',
      property: 'test_quantity',
      props,
    } as LayoutItem)

  function mockData(withLoading: boolean = false) {
    const data = addDefinitions({
      test_quantity: 'test_value',
    })
    return mockArchive({
      data: withLoading ? undefined : data,
    })
  }

  it('initially renders with loading indicator', async () => {
    const layout = createLayout()
    mockData(true)
    await waitFor(() => render(<Layout layout={layout} />))
    expect(screen.queryByText('test_value')).not.toBeInTheDocument()
  })

  it('can be changed', async () => {
    vi.spyOn(useEditDebounce, 'default').mockImplementation((fn, deps) => {
      useDebounce(fn, 0, deps)
    })
    const layout = createLayout()
    const archive = mockData()
    render(<Layout layout={layout} />)
    await waitFor(() =>
      expect(screen.getByText('test_value')).toBeInTheDocument(),
    )
    await userEvent.type(screen.getByRole('textbox'), 'typed')
    await waitFor(() => {
      expect(archive.changeStack.length).toBe(1)
      expect(archive.changeStack[0]).toMatchObject({
        action: 'upsert',
        new_value: expect.stringContaining('typedtest_value'),
        path: 'test_quantity',
      })
    })
  })
})
