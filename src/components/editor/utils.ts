import {has, isEqual, isObject, merge} from 'lodash'

import {MSectionRequest} from '../../models/graphRequestModels'
import {mDefRquest} from '../../pages/entry/entryRoute'
import {assert} from '../../utils/utils'
import {LayoutItem} from '../layout/Layout'

export const defaultSubSectionRequest = {
  m_request: {
    directive: 'plain',
    include_definition: 'both',
    exclude: ['*'],
  },
  m_def: mDefRquest,
} as MSectionRequest

/**
 * Merges an `additionalRequest` object into a `request` object, checking for conflicts in the process.
 *
 * This function ensures that no conflicting values are merged into the `request` object.
 * If a conflict is detected, it throws an error with the specific property path where
 * the conflict occurs.
 *
 * @param request - The current object to be updated.
 * @param additionalRequest - The new object to merge into the current object.
 * @throws {Error} Throws an error if a conflict is detected between `request` and `additionalRequest`.
 */
export function mergeRequests(request: object, additionalRequest: object) {
  const checkConflicts = (
    source: object,
    obj: object,
    path: string[] = [],
  ): void => {
    for (const [key, value] of Object.entries(obj)) {
      const fullPath = [...path, key]
      if (has(source, key)) {
        const existingValue = source[key]
        if (isObject(existingValue) && isObject(value)) {
          checkConflicts(existingValue, value, fullPath)
        } else if (!isEqual(existingValue, value)) {
          throw new Error(
            `Conflicting layout data requests for the same property. Conflict detected at path: ${fullPath.join(
              '.',
            )}`,
          )
        }
      }
    }
  }
  checkConflicts(request, additionalRequest)
  merge(request, additionalRequest)
}

/**
 * Traverses the given layout spec element populates the given request object with
 * required request based on all the quantity and subsSection elements in the layout.
 * @param element The layout spec element
 * @param request The (empty) request object to populate
 */
export function calculateRequestFromLayout(
  element: LayoutItem,
  request: MSectionRequest,
) {
  Object.assign(request, defaultSubSectionRequest)

  function addRequest(
    request: unknown,
    property: string,
    parent: MSectionRequest,
  ) {
    const newRequest: MSectionRequest = {}
    let current = newRequest
    assert(property !== undefined, 'Property layout without "property" key.')
    property.split('.').forEach((item, index, array) => {
      if (index === array.length - 1) {
        current[item] = request
      } else {
        if (current[item] === undefined) {
          current[item] = {...defaultSubSectionRequest} as MSectionRequest
        }
        current = current[item] as MSectionRequest
      }
    })
    mergeRequests(parent, newRequest)
  }

  if ('children' in element) {
    for (const child of element.children) {
      calculateRequestFromLayout(child, request)
    }
  }

  if (
    element.type === 'quantity' ||
    element.type === 'richText' ||
    element.type === 'imagePreview' ||
    element.type === 'plot'
  ) {
    assert(!!element.property, 'Property layout items must define a property')
    addRequest('*', element.property, request)
  }

  if (element.type === 'subSection') {
    const subSectionRequest = {
      ...defaultSubSectionRequest,
    } as MSectionRequest
    calculateRequestFromLayout(element.layout, subSectionRequest)
    addRequest(subSectionRequest, element.property, request)
  }

  if (element.type === 'table') {
    const subSectionRequest = {
      ...defaultSubSectionRequest,
    } as MSectionRequest
    element.columns.forEach((column) => {
      addRequest('*', column.property, subSectionRequest)
    })
    addRequest(subSectionRequest, element.property, request)
  }
}
