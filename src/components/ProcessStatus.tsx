import {EntryResponse, UploadResponse} from '../models/graphResponseModels'
import Status, {StatusProps} from './Status'
import {Failed, Processing, Success, Unknown} from './icons'

export type ProcessStatusProps = {
  entity: Pick<UploadResponse | EntryResponse, 'process_status'>
  variant?: 'default' | 'icon'
}

export default function ProcessStatus({
  entity: {process_status} = {},
  variant = 'default',
}: ProcessStatusProps) {
  const statusProps = {variant} as StatusProps
  if (process_status === 'SUCCESS') {
    statusProps.label = 'processed'
    statusProps.icon = <Success />
    statusProps.color = 'success'
  } else if (process_status === 'READY' || process_status === 'PROCESS') {
    statusProps.label = 'processing'
    statusProps.icon = <Processing />
    statusProps.color = 'warning'
  } else if (process_status === 'FAILED') {
    statusProps.label = 'failed'
    statusProps.icon = <Failed />
    statusProps.color = 'error'
  } else {
    statusProps.label = 'state unknown'
    statusProps.icon = <Unknown />
    statusProps.color = 'error'
  }

  return <Status {...statusProps} />
}
