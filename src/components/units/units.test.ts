import * as math from 'mathjs'

import * as units from './units'
import {dimensionMap, dimensionless, unitMap, unitPrefixes} from './units'

describe('unit', () => {
  it('allows to create all units from full names, aliases, prefixes', async () => {
    // TODO this tests for aliases and prefixes. But none of the unit in our
    // units.data.json has a prefix.
    for (const [name, def] of Object.entries(unitMap)) {
      // Full name + prefixes
      expect(units.unit(name)).not.toBeNaN()
      if (def.prefixes) {
        for (const prefix of Object.keys(
          unitPrefixes[def.prefixes.toUpperCase()],
        )) {
          expect(units.unit(`${prefix}${name}`)).not.toBeNaN()
        }
      }
      // Aliases + prefixes
      if (def.aliases) {
        for (const alias of def.aliases) {
          expect(units.unit(alias)).not.toBeNaN()
          if (def.prefixes) {
            for (const prefix of Object.keys(
              unitPrefixes[def.prefixes.toUpperCase()],
            )) {
              expect(units.unit(`${prefix}${alias}`)).not.toBeNaN()
            }
          }
        }
      }
    }
  })

  describe('toUnit()', () => {
    it('returns a valueless unit', () => {
      expect(units.unit(10, 'meter').toUnit().value).toBeNull()
    })
  })

  describe('to()', () => {
    it.each([
      ['same unit', 'kelvin', 'kelvin', 'K'],
      ['temperature celsius', 'kelvin', 'celsius', '°C'],
      ['temperature fahrenheit', 'kelvin', 'fahrenheit', '°F'],
      ['abbreviated name', 'J', 'eV', 'eV'],
      ['full name', 'joule', 'electron_volt', 'eV'],
      ['division', 'm/s', 'angstrom/femtosecond', 'Å / fs'],
      ['multiplication', 'm*s', 'angstrom*femtosecond', 'Å fs'],
      ['power with hat', 'm^2', 'angstrom^2', 'Å^2'],
      ['power with double asterisk (single)', 'm**2', 'angstrom**2', 'Å^2'],
      [
        'power with double asterisk (multiple)',
        'm**2 / s**2',
        'angstrom**2 / ms**2',
        'Å^2 / ms^2',
      ],
      ['explicit delta (single)', 'delta_celsius', 'delta_K', 'K'],
      [
        'explicit delta (multiple)',
        'delta_celsius / delta_celsius',
        'delta_K / delta_K',
        'K / K',
      ],
      ['explicit delta symbol (single)', 'Δcelsius', 'ΔK', 'K'],
      [
        'explicit delta symbol (multiple)',
        'Δcelsius / Δcelsius',
        'ΔK / ΔK',
        'K / K',
      ],
      ['combined', 'm*m/s^2', 'angstrom^2/femtosecond^2', 'Å^2 / fs^2'],
      ['negative exponent', 's^-2', 'femtosecond^-2', 'fs^-2'],
      ['simple to complex with one unit', 'N', 'kg*m/s^2', '(kg m) / s^2'],
      ['complex to simple with one unit', 'kg*m/s^2', 'N', 'N'],
      ['simple to complex with expression', 'N/m', 'kg/s^2', 'kg / s^2'],
      ['complex to simple with expression', 'kg/s^2', 'N/m', 'N / m'],
      ['unit starting with a number', '1/minute', '1/second', 's^-1'],
    ])('converts for %s', async (name, unitA, unitB, resultLabel) => {
      expect(units.unit(unitA).to(unitB).formatUnits()).toBe(resultLabel)
    })

    it.each([
      ['incompatible dimensions', 'm', 'J'],
      ['wrong power of the correct dimension', 'm', 'm^2'],
      ['unknown unit', 'm', 'beard-second'],
      ['unknown operator', 'm%2', 'Å%2'],
      ['unclosed bracket', '(m^2', '(Å^2'],
    ])('raises for %s', async (name, unitA, unitB) => {
      expect(() => {
        units.unit(unitA).to(unitB)
      }).toThrow()
    })

    it('converts values both ways for each compatible unit', async () => {
      // Create a list of all possible conversions
      const conversions = []
      for (const dimension of Object.values(dimensionMap)) {
        const units = dimension.units
        for (const unitA of units) {
          for (const unitB of units) {
            conversions.push([unitA, unitB])
          }
        }
      }
      for (const [unitA, unitB] of conversions) {
        const a = units.unit(1, unitA)
        const b = a.to(unitB)
        const c = b.to(unitA)
        expect(a.toNumber()).toBeCloseTo(c.toNumber(), 10)
      }
    })

    it.each([
      ['scalar', 'angstrom', 'nanometer', 1, 0.1],
      ['same unit', 'kelvin', 'kelvin', 1, 1],
      ['temperature celsius', 'kelvin', 'celsius', 1, -272.15],
      ['temperature fahrenheit', 'kelvin', 'fahrenheit', 1, -457.87],
      ['abbreviated name', 'J', 'eV', 1, 6241509074460763000],
      ['full name', 'joule', 'electron_volt', 1, 6241509074460763000],
      ['division', 'm/s', 'angstrom/femtosecond', 1, 0.00001],
      [
        'multiplication',
        'm*s',
        'angstrom*femtosecond',
        1,
        9.999999999999999e24,
      ],
      ['power with hat', 'm^2', 'angstrom^2', 1, 99999999999999980000],
      [
        'power with double asterisk (single)',
        'm**2',
        'angstrom**2',
        1,
        99999999999999980000,
      ],
      [
        'power with double asterisk (multiple)',
        'm**2 / s**2',
        'angstrom**2 / ms**2',
        1,
        99999999999999.98,
      ],
      ['explicit delta (single)', 'delta_celsius', 'delta_K', 1, 274.15],
      [
        'explicit delta (multiple)',
        'delta_celsius / delta_celsius',
        'delta_K / delta_K',
        1,
        1,
      ],
      ['explicit delta symbol (single)', 'Δcelsius', 'ΔK', 1, 274.15],
      [
        'explicit delta symbol (multiple)',
        'Δcelsius / Δcelsius',
        'ΔK / ΔK',
        1,
        1,
      ],
      [
        'combined',
        'm*m/s^2',
        'angstrom^2/femtosecond^2',
        1,
        9.999999999999999e-11,
      ],
      ['negative exponent', 's^-2', 'femtosecond^-2', 1, 1e-30],
      ['simple to complex with one unit', 'N', 'kg*m/s^2', 1, 1],
      ['complex to simple with one unit', 'kg*m/s^2', 'N', 1, 1],
      ['simple to complex with expression', 'N/m', 'kg/s^2', 1, 1],
      ['complex to simple with expression', 'kg/s^2', 'N/m', 1, 1],
      [
        'unit starting with a number',
        '1/minute',
        '1/second',
        1,
        0.016666666666666666,
      ],
    ])('converts %s', async (name, unitA, unitB, valueA, valueB) => {
      const a = units.unit(valueA, unitA)
      const b = a.to(unitB)
      expect(b.toNumber()).toBeCloseTo(valueB, 10)
    })

    it.each([
      ['celsius to kelvin', 'celsius', 'kelvin', 5, 278.15],
      ['fahrenheit to kelvin', 'fahrenheit', 'kelvin', 5, 258.15],
      ['celsius to fahrenheit', 'celsius', 'fahrenheit', 5, 41],
      [
        'celsius to kelvin: derived unit (implicit delta)',
        'joule/celsius',
        'joule/kelvin',
        5,
        5,
      ],
      [
        'celsius to kelvin: derived unit (explicit delta)',
        'joule/delta_celsius',
        'joule/kelvin',
        5,
        5,
      ],
      [
        'fahrenheit to kelvin: derived unit (offset not applied)',
        'joule/fahrenheit',
        'joule/kelvin',
        5,
        (9 / 5) * 5,
      ],
      [
        'celsius to fahrenheit: derived unit (offset not applied)',
        'joule/celsius',
        'joule/fahrenheit',
        5,
        (5 / 9) * 5,
      ],
    ])(
      'convers temperatures %s',
      async (name, unitA, unitB, valueA, valueB) => {
        const a = units.unit(valueA, unitA)
        const b = a.to(unitB)
        const c = b.to(unitA)
        expect(b.toNumber()).toBeCloseTo(valueB, 10)
        expect(c.toNumber()).toBeCloseTo(valueA, 10)
      },
    )

    it.each([[1], [2], [3], [4]])(
      'converts %s dimensional value',
      (dimension) => {
        const array = math.ones(Array(dimension).fill(1))
        const result = math.map(array, (v) =>
          units.unit(v, 'angstrom').to('nanometer').toNumber(),
        )
        const index = Array(dimension).fill(0)
        expect(math.matrix(result).get(index)).toBeCloseTo(0.1)
      },
    )
  })

  describe('formatUnits()', () => {
    it.each([
      ['dimensionless', 'dimensionless', 'dimensionless'],
      ['non-abbreviated', 'celsius', '°C'],
      ['abbreviated', '°C', '°C'],
      ['prefix long', 'millikelvin', 'mK'],
      ['prefix short', 'mK', 'mK'],
      ['division', 'angstrom / meter', 'Å / m'],
      ['multiplication', 'angstrom * meter', 'Å m'],
      ['preserve order', 'meter*second', 'm s'],
      ['preserve order', 'second*meter', 's m'],
      ['power', 'meter^2', 'm^2'],
      ['negative power', 'meter^-1', 'm^-1'],
      ['chain', 'meter*meter/second^2', '(m m) / s^2'],
    ])('abbreviates %s', async (name, unitString, format) => {
      const unitObj = units.unit(unitString)
      expect(unitObj.formatUnits()).toBe(format)
    })
  })

  describe('dimension()', () => {
    it.each([
      ['dimensionless', 'dimensionless', 'dimensionless', false],
      ['single unit', 'meter', 'length', false],
      ['fixed order 1', 'meter * second', 'length time', false],
      ['fixed order 2', 'second * meter', 'length time', false],
      ['power', 'meter^3 * second^-1', 'length^3 time^-1', false],
      ['in derived', 'joule', 'energy', false],
      ['in base units', 'joule', 'mass length^2 time^-2', true],
    ])('gets dimension for %s', async (name, unitString, dimension, base) => {
      const unitObj = units.unit(unitString)
      expect(unitObj.dimension(base)).toBe(dimension)
    })
  })

  describe('getUnits()', () => {
    it.each([['m', 'length', ['meter', 'angstrom', 'Å']]])(
      'gets all compatible units for %s',
      (unit, expectedDimension, expectedUnits) => {
        const dimension = units.unit(unit).dimension(true)
        expect(dimension).toBe(expectedDimension)
        const result = units.getUnits(dimension)
        expectedUnits.every((unit) => {
          expect(result).toContain(unit)
        })
      },
    )
  })

  describe('equals', () => {
    it.each([
      ['1 m', '1 m', true],
      ['1 m', '1 cm', false],
      ['1 m', '100 cm', true],
    ])('compares', (a, b, result) => {
      expect(units.unit(a).equals(units.unit(b))).toBe(result)
    })
  })

  describe('unit()', () => {
    it.each([
      ['no number, no unit', '', null, dimensionless, ''],
      ['number only', '100', 100, dimensionless, ''],
      ['unit only', 'joule', null, 'energy', 'J'],
      ['number and unit with dimension', '100 joule', 100, 'energy', 'J'],
      ['mixing number and quantity #1', '1 / joule', 1, 'energy^-1', 'J^-1'],
      [
        'mixing number and quantity #2',
        '100 / joule',
        100,
        'energy^-1',
        'J^-1',
      ],
    ])(
      'can be used to parse %s',
      async (name, input, value, dimension, format) => {
        const valueWithUnit = units.unit(input)
        expect(valueWithUnit.value).toBe(value)
        expect(valueWithUnit.dimension()).toBe(dimension)
        expect(valueWithUnit.formatUnits()).toBe(format)
        expect(valueWithUnit.toUnit().formatUnits()).toBe(format)
        expect(valueWithUnit.toUnit().dimension()).toBe(dimension)
      },
    )
    it.each([
      ['no unit', '', dimensionless, ''],
      ['unit', 'joule', 'energy', 'J'],
    ])(
      'can be used to create unit with number and %s',
      async (name, input, dimension, format) => {
        const valueWithUnit = units.unit(1, input)
        expect(valueWithUnit.value).toBe(1)
        expect(valueWithUnit.dimension()).toBe(dimension)
        expect(valueWithUnit.formatUnits()).toBe(format)
        expect(valueWithUnit.toUnit().formatUnits()).toBe(format)
        expect(valueWithUnit.toUnit().dimension()).toBe(dimension)
      },
    )
  })
})
