import React, {useContext} from 'react'

import {Units} from '../../models/config'

/**
 * React context for interacting with unit configurations.
 */
export type UnitContextValue = {
  units: Units
  setUnits: React.Dispatch<React.SetStateAction<Units>>
}
export const unitContext = React.createContext<UnitContextValue | null>(null)

/**
 * Convenience hook for using the current unit context.
 *
 * @returns Object containing the currently set units for each dimension (e.g.
 * {energy: 'joule'})
 */
export default function useUnitContext() {
  const context = useContext(unitContext)
  if (!context) {
    throw new Error(
      'No unit context. Did you forget to wrap your components in <UnitProvider>.',
    )
  }
  return context
}
