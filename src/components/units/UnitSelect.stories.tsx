import type {Meta, StoryObj} from '@storybook/react'

import UnitSelect from './UnitSelect'
import {unit} from './units'

const meta = {
  title: 'components/units/UnitSelect',
  component: UnitSelect,
  tags: ['autodocs'],
} satisfies Meta<typeof UnitSelect>

export default meta

type Story = StoryObj<typeof meta>
export const SimpleUnitSelect: Story = {
  args: {
    unit: unit('m'),
    autocompleteProps: {
      fullWidth: false,
    },
  },
}
