import {
  Autocomplete,
  AutocompleteProps,
  TextField,
  TextFieldProps,
} from '@mui/material'
import {Unit} from 'mathjs'
import {useCallback, useMemo} from 'react'

import useLocalState from '../../hooks/useLocalState'
import {SxProps} from '../../utils/types'
import {getUnits, unit as mathjsUnit, parseUnitString} from './units'

export type UnitSelectProps = {
  /**
   * The unit to display and edit. The unit is controlled.
   * The unit is also used to determine the compatible units.
   */
  unit: Unit
  /**
   * The change callback to control the unit.
   */
  onChange?: (unit: Unit) => void
  /**
   * Show the unit in error state. Errors are controlled.
   */
  error?: boolean
  /**
   * A callback that is called if there is an error parsing
   * the user input. The callback is called with an error message
   * when the parsing fails or an incompatible unit is entered.
   * The callback is called with undefined when the input is valid.
   */
  onError?: (error: string | undefined) => void
  /**
   * Additional units to provide as choices in the unit selection.
   */
  additionalUnitOptions?: string[]

  autocompleteProps?: Partial<AutocompleteProps<string, false, true, true>>

  textFieldProps?: Partial<TextFieldProps>
} & SxProps

/**
 * A controlled auto complete component to select compatible units based on a given
 * base unit.
 */
export default function UnitSelect({
  unit,
  onChange,
  error,
  onError,
  additionalUnitOptions = [],
  autocompleteProps,
  textFieldProps,
  sx = {},
}: UnitSelectProps) {
  const formatUnit = useCallback((unit: Unit) => unit.formatUnits(), [])
  const [inputValue, setInputValue] = useLocalState(unit, {
    transform: formatUnit,
  })

  const validate = useCallback(
    (inputValue: string) => {
      if (!inputValue) {
        onError?.(undefined)
        return
      }
      const {value: unitValue, error} = parseUnitString(inputValue, unit)
      if (error || unitValue === undefined) {
        onError?.(error)
        return
      }
      onError?.(undefined)
      onChange?.(unitValue)
    },
    [unit, onChange, onError],
  )

  const handleInputChange = useCallback(
    (event: unknown, inputValue: string) => {
      setInputValue(inputValue)
      validate(inputValue)
    },
    [setInputValue, validate],
  )

  const compatibleUnits = useMemo(() => {
    return getUnits(unit.dimension()).map((unit) =>
      mathjsUnit(unit).formatUnits(),
    )
  }, [unit])

  const options = useMemo(() => {
    return [
      ...[unit.formatUnits(), ...additionalUnitOptions].filter(
        (unit) => !compatibleUnits.includes(unit),
      ),
      ...compatibleUnits,
    ]
  }, [unit, compatibleUnits, additionalUnitOptions])

  return (
    <Autocomplete
      sx={sx}
      {...autocompleteProps}
      freeSolo
      disableClearable
      includeInputInList
      openOnFocus
      options={options}
      value={inputValue}
      onChange={handleInputChange}
      componentsProps={{
        popper: {style: {width: 'fit-content'}, placement: 'bottom-start'},
      }}
      popupIcon={null}
      renderInput={(props) => (
        <TextField {...props} {...textFieldProps} error={!!error} />
      )}
    />
  )
}
