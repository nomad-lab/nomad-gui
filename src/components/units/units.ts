import {isNil, startCase, toLower} from 'lodash'
import * as mathjsDefault from 'mathjs'
import {
  Unit,
  UnitComponent,
  UnitDefinition,
  UnitPrefix,
  createUnit,
  unit as mathjsUnit,
} from 'mathjs'

import {unitPrefixes as prefixes, unitList} from './units.data.json'

/**
 * This module provides patches mathjs.Unit with the following features:
 * - replaces all units and prefixes with those in the pint based units (`unit.data.json`)
 * - adds a label method to mathjs.Unit that returns a short string
 * - adds a dimension method to mathjs.Unit that returns the dimension
 *   as a string, e.g. length, time^-1
 * - pre-normalizes unit strings to support some pint syntax
 * - fixes `unit.to()` to handle targets with "values" such as in 1/<unit>
 *
 * To use it import `unit` from this module, instead of mathjs.
 */

export const unit = mathjsUnit
export type UnitSystem = Record<string, {definition: string}>
export type UnitDefinitionWithName = UnitDefinition & {name: string}
export const dimensionless = 'dimensionless'

const prefixAbbreviations: Record<string, string> = {
  // SI
  deca: 'da',
  hecto: 'h',
  kilo: 'k',
  mega: 'M',
  giga: 'G',
  tera: 'T',
  peta: 'P',
  exa: 'E',
  zetta: 'Z',
  yotta: 'Y',
  deci: 'd',
  centi: 'c',
  milli: 'm',
  micro: 'u',
  nano: 'n',
  pico: 'p',
  femto: 'f',
  atto: 'a',
  zepto: 'z',
  yocto: 'y',
  // IEC
  kibi: 'Ki',
  mebi: 'Mi',
  gibi: 'Gi',
  tebi: 'Ti',
  pebi: 'Pi',
  exi: 'Ei',
  zebi: 'Zi',
  yobi: 'Yi',
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const mathjs = mathjsDefault as any

const BASE_DIMENSIONS: string[] = mathjs.Unit.BASE_DIMENSIONS
const BASE_UNITS: Record<string, UnitComponent['unit']['base']> =
  mathjs.Unit.BASE_UNITS
const UNITS: Record<string, UnitComponent['unit']> = mathjs.Unit.UNITS
const PREFIXES: Record<string, Record<string, UnitPrefix>> = mathjs.Unit
  .PREFIXES

export const unitPrefixes = PREFIXES

// Delete all units and prefixes that come by default with Math.js. This way
// they cannot be intermixed with the NOMAD units. Notice that we have to clear
// them in place: they are defined as const.
Object.getOwnPropertyNames(UNITS).forEach((key) => delete UNITS[key])
Object.getOwnPropertyNames(BASE_UNITS).forEach((key) => delete BASE_UNITS[key])
Object.getOwnPropertyNames(PREFIXES).forEach((key) => delete PREFIXES[key])
BASE_DIMENSIONS.splice(0, BASE_DIMENSIONS.length)
PREFIXES.NONE = {'': {name: '', value: 1, scientific: true}}
PREFIXES.PINT = prefixes

// Customize the unit parsing to allow certain special symbols
const isAlphaOriginal = mathjs.Unit.isValidAlpha
const isSpecialChar = function (c: string) {
  const specialChars = new Set(['_', 'Å', 'Å', 'å', '°', 'µ', 'ö', 'é', '∞'])
  return specialChars.has(c)
}
const isGreekLetter = function (c: string) {
  const charCode = c.charCodeAt(0)
  return charCode > 912 && charCode < 970
}
mathjs.Unit.isValidAlpha = function (c: string) {
  return isAlphaOriginal(c) || isSpecialChar(c) || isGreekLetter(c)
}

// Create MathJS unit definitions from the data exported by 'nomad dev units'
export const unitAbbreviations: Record<string, string> = {}
const unitDefinitions: Record<string, UnitDefinition> = {}
for (const defData of unitList) {
  const name = defData.name
  const def = {
    ...defData,
    baseName: defData.dimension,
    prefixes: 'pint',
  } as UnitDefinition
  if (defData.abbreviation) {
    unitAbbreviations[name] = defData.abbreviation
    if (def.aliases) {
      for (const alias of def.aliases) {
        unitAbbreviations[alias] = defData.abbreviation
      }
    }
  }
  unitDefinitions[name] = def
}
createUnit(unitDefinitions, {override: true})

// Swap prefix and unit names for their abbreviations
// to get nice short unit string with `formatUnits()`.
Object.values(PREFIXES['PINT']).forEach(
  (prefix) => (prefix.name = prefixAbbreviations[prefix.name] || prefix.name),
)

Object.values(UNITS).forEach(
  (unit) => (unit.name = unitAbbreviations[unit.name] || unit.name),
)

// Export unit options for each unit and dimension
export const unitMap: Record<string, UnitDefinitionWithName> =
  Object.fromEntries(
    (unitList as unknown as UnitDefinitionWithName[]).map((x) => [x.name, x]),
  )
export const dimensionMap: Record<string, {label: string; units: string[]}> = {}
for (const def of unitList) {
  const name = def.name
  const dimension = def.dimension
  if (isNil(dimension)) {
    continue
  }
  const oldInfo = dimensionMap[dimension] || {
    label: startCase(toLower(dimension.replace('_', ' '))),
  }
  const oldList = oldInfo.units || []
  oldList.push(name)
  oldInfo.units = oldList
  dimensionMap[dimension] = oldInfo
}

/**
 * Convenience function for getting compatible units for a given dimension.
 * Returns all compatible units that have been registered.
 *
 * @param {string} dimension The dimension.
 * @returns Array of compatible units.
 */
export function getUnits(dimension: string): string[] {
  return dimensionMap?.[dimension]?.units || []
}

declare module 'mathjs' {
  interface Unit {
    /**
     * Gets the dimension of this unit as a string. The order of the dimensions is
     * fixed (determined at unit registration time).
     *
     * @param {boolean} base Whether to return dimension in base units. Otherwise
     * the original unit dimensions are used.
     * @returns The dimensionality as a string, e.g. 'time^2 energy mass^-2'
     */
    dimension(base?: boolean): string
    /**
     * Returns the units as a valueless unit.
     */
    toUnit(): Unit

    to(unit: string | Unit): Unit
  }
}

function dimension(unit: Unit, base = false) {
  const dimensions = Object.keys(BASE_UNITS)
  const dimensionMap = Object.fromEntries(dimensions.map((name) => [name, 0]))

  if (base) {
    for (let i = 0; i < BASE_DIMENSIONS.length; ++i) {
      const power = unit.dimensions?.[i]
      if (power) {
        dimensionMap[BASE_DIMENSIONS[i]] += power
      }
    }
  } else {
    for (const unitComponent of unit.units) {
      const power = unitComponent.power
      if (power) {
        dimensionMap[unitComponent.unit.base.key] += power
      }
    }
  }
  const dims = Object.entries(dimensionMap).filter((d) => d[1] !== 0)
  return dims.length > 0
    ? dims
        .map((d) => `${d[0]}${((d[1] < 0 || d[1] > 1) && `^${d[1]}`) || ''}`)
        .join(' ')
    : dimensionless
}
mathjs.Unit.prototype.dimension = function (this, base?: boolean) {
  if (this === undefined) {
    return dimensionless
  }
  return dimension(this, base)
}

const originalUnitParse = mathjs.Unit.parse
mathjs.Unit.parse = function (
  unitString: string,
  options: Record<string, unknown>,
) {
  const normalizedUnitString = unitString
    .replace(/\*\*/g, '^')
    .replace(/delta_/g, '')
    .replace(/Δ/g, '')

  return new originalUnitParse(normalizedUnitString, {
    ...options,
    allowNoUnits: true,
  })
}
const originalUnitTo = mathjs.Unit.prototype.to

/**
 * We cannot directly feed the to unit string into Math.js, because it will
 * parse units like 1/<unit> as Math.js units which have values, and then
 * will raise an exception when converting between valueless and valued
 * unit. The workaround is to explicitly define a valueless unit.
 */
function to(thisUnit: Unit, valuelessUnit: string | Unit) {
  if (typeof valuelessUnit !== 'string') {
    valuelessUnit = valuelessUnit.formatUnits()
  }

  const unitObj = unit(valuelessUnit)
  unitObj.value = null as unknown as number
  return originalUnitTo.apply(thisUnit, [unitObj])
}

mathjs.Unit.prototype.to = function (this, unit: string | Unit) {
  return to(this, unit)
}

mathjs.Unit.prototype.toUnit = function (this) {
  const result = this.clone()
  result.value = null
  return result
}

export function isUnit(value: unknown): value is Unit {
  return value instanceof mathjs.Unit
}

export function parseUnitString(
  value: string,
  compatibleUnit?: Unit | string,
): {value?: Unit; error?: string; containsUnit: boolean} {
  if (typeof compatibleUnit === 'string') {
    compatibleUnit = mathjsUnit(compatibleUnit)
  }
  let dimension = compatibleUnit?.dimension()
  if (!dimension) {
    dimension = dimensionless
  }
  let containsUnit = false
  try {
    let unitValue = mathjsUnit(value)
    let unitValueDimension = unitValue.dimension()
    containsUnit = unitValueDimension !== dimensionless
    if (unitValueDimension === dimensionless && compatibleUnit) {
      unitValue = mathjsUnit(unitValue.toNumber(), compatibleUnit.formatUnits())
      unitValueDimension = unitValue.dimension()
    }
    if (unitValueDimension !== dimension) {
      throw new Error(`Use a ${dimension} unit`)
    }
    return {value: unitValue, containsUnit}
  } catch (e) {
    let error
    if (e instanceof Error) {
      error = e.message
    } else {
      error = 'Invalid unit'
    }
    return {error, containsUnit}
  }
}
