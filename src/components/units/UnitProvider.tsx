import React, {ReactNode, useState} from 'react'

import config from '../../config'
import {UnitSystem, Units} from '../../models/config'
import {assert} from '../../utils/utils'
import {unitContext} from './useUnitContext'

const UnitProvider: React.FC<{children: ReactNode}> = ({children}) => {
  const selected = useState<string>(config.ui.unit_systems?.selected || 'SI')[0]
  // @ts-expect-error Items are added on the fly to the config model by gui_config.py
  const unitSystem = config.ui.unit_systems?.items?.find(
    // @ts-expect-error Items are added on the fly to the config model by gui_config.py
    (system: UnitSystem) => system.id === selected,
  )
  assert(unitSystem, `Unit system "${selected}" is not defined.`)
  const [units, setUnits] = useState<Units>(unitSystem.units)

  return (
    <unitContext.Provider value={{units, setUnits}}>
      {children}
    </unitContext.Provider>
  )
}

export default UnitProvider
