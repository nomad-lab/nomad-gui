import IconButton, {IconButtonProps} from '@mui/material/IconButton'
import {styled} from '@mui/material/styles'

import {ExpandMore as ExpandMoreIcon} from './icons'

export type ExpandMoreProps = {
  expanded: boolean
} & IconButtonProps

const ExpandMore = styled((props: ExpandMoreProps) => {
  const {expanded, ...iconButtonProps} = props
  return (
    <IconButton {...iconButtonProps} aria-label='expand'>
      <ExpandMoreIcon />
    </IconButton>
  )
})(({theme, expanded}) => ({
  transform: !expanded ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}))

export default ExpandMore
