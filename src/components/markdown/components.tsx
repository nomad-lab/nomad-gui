import {
  Box,
  Divider,
  Link,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material'
import {HTMLAttributes, HTMLProps, ReactNode} from 'react'

import RouterLink from '../routing/Link'
import './prism.css'

// mdx-js does not seem to have a proper ts type for this
export type Components = {
  [key: string]: (
    props: HTMLAttributes<HTMLElement> & {components: unknown},
  ) => ReactNode
}

export const components: Components = {
  nav: (props) => (
    <Box
      {...props}
      component='nav'
      sx={{
        '& > h1': {
          fontSize: 16,
        },
        '& ol': {
          listStyle: 'none',
          paddingInlineStart: 4,
        },
        '&>ol': {
          paddingInlineStart: 0,
        },
        '& li': {
          marginY: 1,
        },
        '& a': {
          '&:hover': {
            color: 'primary.main',
          },
          textDecoration: 'none',
          color: 'text.primary',
        },
      }}
    />
  ),
  h1: (props) => (
    <Typography
      {...props}
      variant='h1'
      color='primary'
      sx={{marginTop: 2, marginBottom: 1}}
    />
  ),
  h2: (props) => (
    <Typography
      {...props}
      component='h2'
      variant='h2'
      sx={{marginTop: 3, marginBottom: 1}}
    />
  ),
  h3: (props) => (
    <Typography
      {...props}
      component='h3'
      variant='h3'
      sx={{marginTop: 2, marginBottom: 1}}
    />
  ),
  h4: (props) => <Typography {...props} component='h4' variant='h4' />,
  h5: (props) => <Typography {...props} component='h5' variant='h5' />,
  h6: (props) => <Typography {...props} component='h6' variant='h6' />,
  p: (props) => <Typography {...props} component='p' sx={{marginY: 2}} />,
  strong: (props) => (
    <Typography
      {...props}
      component='strong'
      variant='inherit'
      sx={{fontWeight: 'bold', color: 'primary.main'}}
    />
  ),
  em: (props) => (
    <Typography
      {...props}
      component='em'
      variant='inherit'
      sx={{fontStyle: 'italic'}}
    />
  ),
  ul: (props) => <Typography {...props} component='ul' variant='inherit' />,
  ol: (props) => <Typography {...props} component='ol' variant='inherit' />,
  li: (props) => <Typography {...props} component='li' variant='inherit' />,
  a: (props) => {
    try {
      const url = new URL((props as HTMLProps<HTMLLinkElement>)?.href || '')
      if (
        url.origin === window.location.origin ||
        (url.origin === 'null' &&
          url.protocol === null &&
          url.pathname !== null)
      ) {
        return <Link {...props} {...{component: RouterLink}} />
      }
    } catch {
      // ignore
    }
    return <Link {...props} />
  },
  hr: (props) => <Divider {...props} component='hr' />,
  blockquote: (props) => (
    <Paper
      {...props}
      component='blockquote'
      sx={{
        padding: 1,
      }}
    />
  ),
  pre: (props) => (
    <Box
      {...props}
      component='pre'
      sx={{
        borderRadius: 2,
        margin: 0,
        '& > code': {padding: 0},
      }}
    />
  ),
  code: (props) => (
    <Box
      {...props}
      component='code'
      sx={(theme) => ({
        textWrap: 'nowrap',
        borderRadius: 1,
        paddingY: '1px',
        paddingX: 1,
        ...(theme.palette.mode === 'dark'
          ? {
              bgcolor: 'primary.contrastText',
              color: 'primary.dark',
            }
          : {
              bgcolor: 'primary.main',
              color: 'primary.contrastText',
            }),
      })}
    />
  ),
  table: (props) => (
    <TableContainer component={Paper}>
      <Table {...props} />
    </TableContainer>
  ),
  thead: (props) => <TableHead {...props} />,
  tbody: (props) => <TableBody {...props} />,
  tr: (props) => <TableRow {...props} sx={{'&:last-child td': {border: 0}}} />,
  td: (props) => <TableCell {...props} />,
  th: (props) => <TableCell {...props} />,
  wrapper: ({components, ...rest}) => (
    <Box
      {...rest}
      sx={(theme) => ({
        display: 'flex',
        flexDirection: 'row',
        gap: 4,
        '& .content': {
          flexGrow: 1,
          maxWidth: theme.breakpoints.values.md,
        },
        '& .toc': {
          width: {
            xl: theme.breakpoints.values.xl - theme.breakpoints.values.lg,
          },
          display: {
            xs: 'none',
            xl: 'block',
          },
          order: 1,
          '&:has(ol:empty)': {
            display: 'none',
          },
        },
      })}
    />
  ),
}
