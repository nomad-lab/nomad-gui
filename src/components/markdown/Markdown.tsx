import rehypePrism from '@mapbox/rehype-prism'
import {Box} from '@mui/material'
import {useMemo} from 'react'
import * as prod from 'react/jsx-runtime'
import rehypeReact from 'rehype-react'
import remarkGfm from 'remark-gfm'
import remarkParse from 'remark-parse'
import remarkRehype from 'remark-rehype'
import {Plugin, unified} from 'unified'

import {ignoreSsrWarning} from '../../utils/utils'
import {components} from './components'

const production = {Fragment: prod.Fragment, jsx: prod.jsx, jsxs: prod.jsxs}

export type MarkdownProps = {
  content: string
}

export default function Markdown({content}: MarkdownProps) {
  const element = useMemo(() => {
    return unified()
      .use(remarkParse)
      .use(remarkGfm)
      .use(remarkRehype)
      .use(rehypePrism as unknown as Plugin<[]>)
      .use(rehypeReact, {...production, components})
      .processSync(content).result as JSX.Element
  }, [content])

  return (
    <Box
      sx={{
        [`& > :first-child ${ignoreSsrWarning}`]: {
          marginTop: 0,
        },
        '& > :last-child': {
          marginBottom: 0,
        },
      }}
    >
      {element}
    </Box>
  )
}
