import {render, screen} from '@testing-library/react'

import Markdown from './Markdown'

const exampleContent = `
# h1
## h2
### h3
#### h4
##### h5
###### h6

*italic*, **bold**, \`code\`, _italic_, ~~strikethrough~~.

---

https://example.com, contact@example.com, [here](/local/path).

A note[^1]

[^1]: Big note.

| a   | b   |   c |  d  |
| --- | :-- | --: | :-: |
| 1   | 2   |   3 |  4  |

- [ ] to do
- [x] done

\`\`\`tsx
function Hello() {
  return <Typography>Hello</Typography>
}
\`\`\`

> This
> is a quoted
> block.
`

describe('Markdown', () => {
  it('renders md content', () => {
    render(<Markdown content={exampleContent} />)
    expect(screen.getByRole('heading', {name: 'h1'})).toBeInTheDocument()
  })
})
