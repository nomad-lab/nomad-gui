import {useMemo} from 'react'
import {useLatest} from 'react-use'

import useData, {UseDataParams, UseDataResult} from '../../hooks/useData'
import {GraphResponse} from '../../models/graphResponseModels'
import {DefaultToObject, JSONObject} from '../../utils/types'
import {createRequestForRoute, getResponsesForRoute} from './loader'
import {Route} from './types'
import useRoute from './useRoute'

export type UseDataForRouteParams<
  Request = unknown,
  Response = unknown,
> = UseDataParams<Request, Response> & {
  route?: Route<Request, Response>
  reloadCount?: number
}

/**
 * A hook that fetches data from the API based on a request that is relative
 * to the currently rendered route.
 *
 * @returns The response containing the fetched data or undefined if the data
 *   is not available yet.
 */
export default function useDataForRoute<
  Request = JSONObject,
  Response = JSONObject,
>({
  request,
  route,
  onFetch: onFetchForRoute,
  onError,
  reloadCount,
  ...useDataParams
}: UseDataForRouteParams<Request, Response>): UseDataResult<Response> {
  const {match} = useRoute()
  const latestMatch = useLatest(match)

  const fullRequest = useMemo(() => {
    if (!request) {
      return undefined
    }
    return createRequestForRoute(match, (current) => {
      if (current === match[match.length - 1]) {
        return request as JSONObject
      }
    })
    // The match is not part of the dependency array because it will change
    // but the change should not have an effect on the request.
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [request])

  const onFetch = useMemo(() => {
    if (!onFetchForRoute) {
      return undefined
    }
    return (data: JSONObject, fullResponse: GraphResponse) => {
      const match = latestMatch.current
      const dataForRoute = getResponsesForRoute(match, data)[match.length - 1]
      onFetchForRoute(dataForRoute as DefaultToObject<Response>, fullResponse)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [onFetchForRoute, latestMatch, reloadCount])

  const {data, ...otherUseDataResults} = useData({
    request: fullRequest,
    onFetch,
    onError,
    ...useDataParams,
  })
  const response =
    data && fullRequest
      ? getResponsesForRoute(match, data)[match.length - 1]
      : undefined

  return {
    ...otherUseDataResults,
    data: response as DefaultToObject<Response> | undefined,
  }
}
