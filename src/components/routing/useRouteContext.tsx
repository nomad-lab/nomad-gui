import React, {ReactElement, useContext} from 'react'

import {DefaultSearch, RouteData} from './types'

export type GlobalRouteData = Pick<
  RouteData,
  'location' | 'rawSearch' | 'fullMatch' | 'error' | 'isLoading'
>

export type RouteContextValue<
  Request = unknown,
  Response = unknown,
  Search extends DefaultSearch = DefaultSearch,
> = GlobalRouteData & {
  createRouteData: (
    data: GlobalRouteData,
    index: number,
  ) => RouteData<Request, Response, Search>
  index: number
  element?: ReactElement
}

export const routeContext = React.createContext<RouteContextValue | null>(null)

export default function useRouteContext() {
  const contextValue = useContext(routeContext)
  if (!contextValue) {
    throw new Error(
      'No route context. Did you forget to wrap your components in <Route>.',
    )
  }
  return contextValue
}
