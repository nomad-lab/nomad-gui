import React, {useContext} from 'react'
import {LinkProps} from 'wouter'

export type RouterValue = {
  createLink: React.ForwardRefRenderFunction<HTMLAnchorElement, LinkProps>
  navigate: (url: string) => void
}

export const routerContext = React.createContext<RouterValue | null>(null)

export default function useRouter() {
  const value = useContext(routerContext)
  if (value === null) {
    throw new Error(
      'UseRouter must be used inside a Router. Did you forget to wrap your app in a Router?',
    )
  }
  return value
}
