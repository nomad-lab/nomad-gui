import {JSONObject} from '../../utils/types'
import {
  createRequestForRoute,
  getIndexedKey,
  getResponseForPath,
  getResponsesForRoute,
} from './loader'
import {RouteMatch} from './types'

describe('getIndexKey', () => {
  it('works with indexed key', () => {
    const result = getIndexedKey('key[0]')
    expect(result).toEqual(['key', 0])
  })
  it('works with non indexed key', () => {
    const result = getIndexedKey('key')
    expect(result).toEqual(['key', undefined])
  })
})

describe('getResponseForPath', () => {
  it('works with indexed key', () => {
    const result = getResponseForPath('key[0]', {key: [{value: 'value'}]})
    expect(result).toEqual({value: 'value'})
  })
  it('works with non indexed key', () => {
    const result = getResponseForPath('key', {key: {value: 'value'}})
    expect(result).toEqual({value: 'value'})
  })
  it('works with nested keys', () => {
    const result = getResponseForPath('key/nested', {
      key: {nested: {value: 'value'}},
    })
    expect(result).toEqual({value: 'value'})
  })
  it('throws error if key does not exist', () => {
    expect(() => getResponseForPath('key', {})).toThrow(
      'The entity with id key does not exist.',
    )
  })
})

describe('getResonseForRoute', () => {
  it('works', () => {
    const result = getResponsesForRoute(
      [{route: {requestKey: 'key', request: '*'}} as unknown as RouteMatch],
      {key: {value: 'value'}},
    )
    expect(result).toEqual([{value: 'value'}])
  })
})

describe('createRequestForRoute', () => {
  function functionUnderTest(
    path: string,
    getRequest?: (match: RouteMatch, index: number) => JSONObject | undefined,
  ) {
    return createRequestForRoute(
      path
        .split('/')
        .map((path) => ({path, route: {}} as unknown as RouteMatch)),
      getRequest || (() => ({requestKey: 'value'})),
    )
  }
  it('works with indexed key', () => {
    const result = functionUnderTest('pathKey[0]')
    expect(result).toEqual({'pathKey[0]': {requestKey: 'value'}})
  })
  it('works with non indexed key', () => {
    const result = functionUnderTest('pathKey')
    expect(result).toEqual({pathKey: {requestKey: 'value'}})
  })
  it('works with nested keys', () => {
    const result = functionUnderTest('pathKey/nested')
    expect(result).toEqual({
      pathKey: {nested: {requestKey: 'value'}, requestKey: 'value'},
    })
  })
  it('works with empty key', () => {
    const result = functionUnderTest('pathKey//nested')
    expect(result).toEqual({
      pathKey: {nested: {requestKey: 'value'}, requestKey: 'value'},
    })
  })
  it('works with overlap', () => {
    const result = functionUnderTest('pathKey/nested', (_match, index) => {
      if (index === 0) {
        return {nested: {otherKey: 'value'}} as JSONObject
      }
      return {requestKey: 'value'} as JSONObject
    })
    expect(result).toEqual({
      pathKey: {
        nested: {requestKey: 'value', otherKey: 'value'},
      },
    })
  })
  it('works without request', () => {
    const result = functionUnderTest('pathKey/nested', (_match, index) => {
      if (index === 0) {
        return undefined
      }
      return {requestKey: 'value'} as JSONObject
    })
    expect(result).toEqual({
      pathKey: {
        nested: {requestKey: 'value'},
      },
    })
  })
  it('works with requestKey', () => {
    const result = createRequestForRoute(
      [{route: {requestKey: 'key'}} as unknown as RouteMatch],
      () => ({requestKey: 'value'}),
    )
    expect(result).toEqual({key: {requestKey: 'value'}})
  })
  it('works with requestKey function', () => {
    const result = createRequestForRoute(
      [{route: {requestKey: () => 'key'}} as unknown as RouteMatch],
      () => ({requestKey: 'value'}),
    )
    expect(result).toEqual({key: {requestKey: 'value'}})
  })
  it('works with requestPath', () => {
    const result = createRequestForRoute(
      [{route: {requestPath: 'some/path'}} as unknown as RouteMatch],
      () => ({requestKey: 'value'}),
    )
    expect(result).toEqual({some: {path: {requestKey: 'value'}}})
  })
  it('works with requestPath function', () => {
    const result = createRequestForRoute(
      [{route: {requestPath: () => 'some/path'}} as unknown as RouteMatch],
      () => ({requestKey: 'value'}),
    )
    expect(result).toEqual({some: {path: {requestKey: 'value'}}})
  })
})
