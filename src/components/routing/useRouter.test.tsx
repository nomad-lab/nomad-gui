import {render, renderHook} from '@testing-library/react'
import {it, vi} from 'vitest'

import Router from './Router'
import {Route} from './types'
import useRouter from './useRouter'

describe('useRouter', () => {
  it('returns the router context', () => {
    const result = vi.fn()
    const route = {
      path: '/',
      component: function Component() {
        result(useRouter())
        return null
      },
    } as Route
    render(<Router route={route} />)
    expect(result).toHaveBeenCalledTimes(1)
  })

  it('throws an error when no provider is present', () => {
    vi.spyOn(console, 'error').mockImplementation(() => {})
    expect(() => renderHook(() => useRouter())).toThrowError(
      /UseRouter must be used inside a Router./,
    )
  })
})
