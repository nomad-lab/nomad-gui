import {DefaultSearch, Route} from './types'

const normalizedRoutes = new Map<Route, Route>()

export function routesEqual<Request, Response, Search extends DefaultSearch>(
  a: Route<Request, Response, Search>,
  b: Route<Request, Response, Search>,
  parents: Route<Request, Response, Search>[] = [],
) {
  // Originally, we just wanted to compare routes directly with their
  // cached normalized versions. However, this does not work, as routes
  // seem loose their identity when hot replaced by vite.
  // This is a workaround to compare routes by their path.

  if (a === b) {
    return true
  }

  if (a === undefined || b === undefined) {
    return false
  }

  a = normalizeRoute(a as unknown as Route) as unknown as Route<
    Request,
    Response,
    Search
  >
  b = normalizeRoute(b as unknown as Route) as unknown as Route<
    Request,
    Response,
    Search
  >

  if (parents.includes(a)) {
    return true
  }
  parents = [...parents, a]

  if (a.path !== b.path) {
    return false
  }

  if (a.children?.length !== b.children?.length) {
    return false
  }

  if (a.children && b.children) {
    for (let i = 0; i < a.children?.length; i++) {
      if (!routesEqual(a.children[i], b.children[i], parents)) {
        return false
      }
    }
  }

  return true
}

export default function normalizeRoute(route: Route): Route {
  if (normalizedRoutes.has(route)) {
    return normalizedRoutes.get(route) as Route
  }

  const originalRoute = route
  const path = route.path.split('/')
  route = {
    ...route,
    path: path[path.length - 1],
  }
  for (let index = path.length - 2; index >= 0; index--) {
    route = {
      path: path[index],
      children: [route],
    }
  }

  const normalizedRoute: Route = {...route}
  normalizedRoutes.set(originalRoute, normalizedRoute)
  normalizedRoutes.set(normalizedRoute, normalizedRoute)

  if (route.children !== undefined) {
    normalizedRoute.children = route.children.map(normalizeRoute)
  }

  if (route.onlyRender !== undefined) {
    const onlyRenderRoutes = Array.isArray(route.onlyRender)
      ? route.onlyRender
      : [route.onlyRender]

    normalizedRoute.onlyRender = onlyRenderRoutes.map((onlyRender) => {
      if (typeof onlyRender === 'string') {
        onlyRender = {path: onlyRender}
      }
      return normalizeRoute(onlyRender)
    })
  }

  return normalizedRoute
}
