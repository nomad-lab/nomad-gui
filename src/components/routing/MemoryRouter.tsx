import {Link as MuiLink} from '@mui/material'
import React, {Ref, useCallback, useMemo} from 'react'
import {LinkProps} from 'wouter'

import useLocalState from '../../hooks/useLocalState'
import Routes, {Location, RoutesProps} from './Routes'
import useRouter, {routerContext} from './useRouter'

const Link = React.forwardRef<HTMLAnchorElement, LinkProps>(function Link(
  props,
  ref,
) {
  const {navigate} = useRouter()
  const handleClick = useCallback(() => {
    if (props.to) {
      navigate(props.to)
    }
  }, [navigate, props.to])

  return <MuiLink {...props} href='#' onClick={handleClick} ref={ref} />
})

export type MemoryRouterProps = Omit<RoutesProps, 'location' | 'navigate'> & {
  /**
   * The current url of the router. If this is not provided, the router
   * will manage its own url state.
   */
  url?: string
  /**
   * A callback that is called when the url changes. This is useful when
   * the url is controlled by a parent component.
   */
}

/**
 * A route that stored the current location in a react state variable
 * instead of using the browser location. This is useful to reuse
 * routing dependent functionality in modals and other components that
 * are not suppose to change the main browser location.
 */
export default function MemoryRouter({
  url: controlledUrl,
  ...routesProps
}: MemoryRouterProps) {
  const [url, setUrl] = useLocalState(controlledUrl || '/')

  const location = useMemo(() => {
    const [path, rawSearch] = url.split('?')
    return {
      path,
      rawSearch: Object.fromEntries(
        new URLSearchParams(rawSearch || '').entries(),
      ),
    } as Location
  }, [url])

  const navigate = useCallback(
    (url: string) => {
      setUrl(url)
    },
    [setUrl],
  )

  const routerValue = useMemo(() => {
    return {
      navigate,
      createLink: (props: LinkProps, ref: Ref<HTMLAnchorElement>) => {
        return <Link ref={ref} {...props} />
      },
    }
  }, [navigate])

  return (
    <routerContext.Provider value={routerValue}>
      <Routes {...routesProps} location={location} navigate={navigate} />
    </routerContext.Provider>
  )
}
