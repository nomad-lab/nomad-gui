import {useMemo} from 'react'

import {routesEqual} from './normalizeRoute'
import {DefaultSearch, Route, RouteData} from './types'
import useRouteContext from './useRouteContext'

export default function useRoute<
  Request = unknown,
  Response = unknown,
  Search extends DefaultSearch = DefaultSearch,
>(
  route?: Route<Request, Response, Search>,
): RouteData<Request, Response, Search> {
  const routeContext = useRouteContext()
  return useMemo(() => {
    const {createRouteData, index, ...globalRouteData} = routeContext
    if (route) {
      const {fullMatch} = globalRouteData
      for (let i = index; i >= 0; i--) {
        if (
          routesEqual(
            route,
            fullMatch[i].route as unknown as Route<Request, Response, Search>,
          )
        ) {
          return createRouteData(globalRouteData, i) as unknown as RouteData<
            Request,
            Response,
            Search
          >
        }
      }
    }
    return createRouteData(globalRouteData, index) as unknown as RouteData<
      Request,
      Response,
      Search
    >
  }, [route, routeContext])
}
