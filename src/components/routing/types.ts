import {User} from 'oidc-client-ts'
import {ElementType, ReactNode} from 'react'

import {DefaultToObject, JSONObject} from '../../utils/types'

export type SearchValue = number | string | boolean
export type DefaultSearch = Record<string, SearchValue>
export type RawSearchParams = Record<string, string>

export type PathParams = {
  [name: string]: string | string[] | undefined
}

export type LoaderResult = {
  routeResponses: (JSONObject | undefined)[]
  routeRequests: JSONObject[]
  request: JSONObject
  response: JSONObject
}

export type Loader = (
  fullMatch: RouteMatch[],
  user?: User,
) => (() => Promise<LoaderResult>) | undefined

export type RouteMatch<
  Request = unknown,
  Response = unknown,
  Search extends DefaultSearch = DefaultSearch,
> = {
  /**
   * The route definition for the current route segment.
   */
  route: Route<Request, Response, Search>

  /**
   * The part of the location path that is matched by this route segment.
   */
  path: string

  /**
   * The path params up to the current route segment.
   */
  params: PathParams

  /**
   * True if the current route segment is the last one.
   */
  isLeaf: boolean

  /**
   * The validated search parameters from all route segments up to the current.
   */
  search: Search

  /**
   * The data loaded for the current route segment.
   */
  response?: DefaultToObject<Response>

  /**
   * The user object from the oidc-client-js library representing the logged in user.
   */
  user?: User
}

export type ValidateSearchParams<
  Request = unknown,
  Response = unknown,
  Search extends DefaultSearch = DefaultSearch,
> = Omit<RouteMatch<Request, Response, Search>, 'search'> & {
  location: string
  rawSearch: RawSearchParams
}

export type LocationParams<Search extends DefaultSearch = DefaultSearch> = {
  /**
   * A relative or absolute path. Either a single path segment or a list with multiple
   * path segments. If the first segment starts with `/` the path is absolute.
   */
  path?: string | string[]
  /**
   * The search parameters to use for the new url. This only updates the
   * parameters defined for this route segment all other search parameters
   * in the current location will also be in the new url.
   */
  search?: Search
  /**
   * Only the search parameters that should be updated. The url will contain
   * all remaining search parameters for the current location.
   */
  searchUpdates?: Partial<Search>
  /**
   * The search parameters to use for the new url. This will all
   * search parameters in the existing location.
   */
  rawSearch?: RawSearchParams
}

export type RouteData<
  Request = unknown,
  Response = unknown,
  Search extends DefaultSearch = DefaultSearch,
> = RouteMatch<Request, Response, Search> & {
  /**
   * The array of matches for each route segment matching the current location.
   */
  fullMatch: RouteMatch[]

  /**
   * The current location.
   */
  location: string

  /**
   * The raw string values search parameters.
   */
  rawSearch: RawSearchParams

  /**
   * The index of the current route segment.
   */
  index: number

  /**
   * The array of matches for each route segment up to the current route segment.
   */
  match: RouteMatch[]

  /**
   * The error that occurred while matching the route or loading data.
   */
  error?: Error

  /**
   * Indicates if the route data is still loading. Will only affect the root
   * routes component as all other components only render after loading.
   */
  isLoading: boolean

  /**
   * Returns a URL for the given path (relative to the current route segment) and
   * search parameters.
   */
  url: (params?: LocationParams<Search>) => string

  /**
   * Updates the location in the browser history. Same as `url`, but navigates to the
   * new location.
   */
  navigate: (params: LocationParams<Search>) => void

  /**
   * Reloads the data for the current routs and rerenders.
   */
  reload: () => void

  /**
   * A running counter for reloads. Can be used to update effects.
   */
  reloadCount: number

  /**
   * Potentially turns the given absolute `path` string into a path string
   * that is relative to the current route location or a given `referencePath`.
   */
  relativePath: (path: string, referencePath?: string) => string

  /**
   * Checks if the current route is the same or sub route of the given path.
   */
  matches: (path: string) => boolean
}

export type Route<
  Request = unknown,
  Response = unknown,
  Search extends DefaultSearch = DefaultSearch,
> = {
  /**
   * The path for this route. Can be a single path segment or a path with multiple
   * segments separated by `/`. All routes will be normalized and multi segment
   * path will be broken down into individual `Route` objects. The path
   * can also be a variable path segment starting with `:`. The string after the
   * `:` will be used as the name of the path parameter. Path parameters match
   * any path segment and will be available in the `params` property of the
   * `RouteData` object.
   */
  path: string

  /**
   * The component that is used to render the route. The component can
   * use the `<Outlet>` component to render the child routes. If no component
   * is given, the `<Outlet>` of the parent will render the next child route
   * that has a component. The leaf route segment must have a component.
   */
  component?: ElementType

  /**
   * Instead of a component, a route can define a lazy loaded component. The
   * given function must return a Promise that resolves to a module object
   * with a default export that is the component to render, e.g.
   * `() => import('./MyComponent')`.
   */
  lazyComponent?: () => Promise<{default: ElementType}>

  /**
   * If defined, a given component will only be rendered if the current location
   * matches on of the given routes. Routes can be given as `Route` objects
   * (similar to children) or as a (list of) string(s) that is automatically
   * normalized into a list of routes. On the `children` and `path` properties
   * of the given routes are considered.
   */
  onlyRender?: string | (string | Route)[] | Route

  /**
   * Defines how the route segment is shown in the breadcrumbs navigation.
   * Either a string label, some ReactNode, or a render function that takes the route match
   * as argument. By default the breadcrumbs show the path segment.
   */
  breadcrumb?:
    | string
    | ReactNode
    | ((routeMatch: RouteMatch<Request, Response, Search>) => ReactNode)

  /**
   * An alternative to `component` that is used, if there was an error
   * matching the route, loading lazy components, or loading data. The
   * error component can receive the error via `const {error} = useRoute()`.
   */
  errorComponent?: ElementType

  /**
   * The route will always redirect if `redirect` is defined. The value
   * is a function that returns a URL relative to the current location.
   */
  redirect?: (routeData: RouteData<Request, Response, Search>) => string

  /**
   * The children of the route.
   */
  //
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  children?: Route<any, any, any>[]

  /**
   * An optional function that created the validated search parameters
   * for this route segment. Validated search parameters are merged with
   * the raw search parameters.
   * @param data The route data for this route segment.
   * @returns The validated search parameters satisfying the type `Search`.
   */
  validateSearch?: (
    data: ValidateSearchParams<Request, Response, Search>,
  ) => Search

  /**
   * An optional key or function that produces the request key. The
   * requests of child routes are embedded into the request of the parent
   * via this key. By default the matched path segment is used as the request key.
   */
  requestKey?:
    | string
    | ((locationData: RouteMatch<Request, Response, Search>) => string)

  /**
   * An optional path or function that produces a path. The path will
   * replace the API path usually derived from the `path` property of the route
   * and its parent routes. Similar to request key, but for the whole path.
   *
   * This applies to the whole route including all child routes.
   * TODO Are there cases where this is bad? Should this be configurable?
   */
  requestPath?:
    | string
    | ((locationData: RouteMatch<Request, Response, Search>) => string)

  /**
   * An optional request or function that produces a request. The request
   * has to satisfy the type `Request`. The request is used to create
   * an overall `GraphRequest`. Before any route with requests is rendered,
   * the data will be fetched. The data will be available in the `response`
   * property of the respective `RouteData` object, e.g. obtained via
   * `useRoute`.
   */
  request?:
    | DefaultToObject<Request>
    | ((
        locationData: RouteMatch<Request, Response, Search>,
      ) => DefaultToObject<Request>)

  /**
   * An optional function that is called after the data for the route has
   * been fetched. The function receives the route segment data and the full response.
   */
  onFetch?: (args: {
    fullMatch: RouteMatch[]
    match: RouteMatch<Request, Response, Search>
    request: Request
    response: Response
    fullRequest: JSONObject
    fullResponse: JSONObject
  }) => void

  /**
   * Will be added when used in Routes/Router. Can be used to compare
   * stored route objects with the normalized route objects used in matches.
   */
  routeId?: number
}
