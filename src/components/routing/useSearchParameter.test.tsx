import {renderHook} from '@testing-library/react'
import {it, vi} from 'vitest'

import {SearchValue} from './types'
import useSearchParameter from './useSearchParameter'

const navigate = vi.fn()
const search = vi.fn()
vi.mock('./useRoute', () => ({
  default: () => ({
    navigate,
    search: search(),
  }),
}))

it.each([
  [true, false, true, false],
  [true, false, false, undefined],
  [true, true, true, undefined],
  [true, true, false, true],
  [1, 2, 1, 2],
])(
  'works for %s changed to %s with %s as default',
  (
    value: SearchValue | undefined,
    newVaule: SearchValue,
    defaultValue: SearchValue,
    updatedValue: SearchValue | undefined,
  ) => {
    search.mockReturnValue({name: value})
    const {result} = renderHook(() => useSearchParameter('name', defaultValue))
    const [searchValue, setSearchValue] = result.current
    expect(searchValue).toBe(value)
    setSearchValue(newVaule)
    expect(navigate).toHaveBeenCalledWith({searchUpdates: {name: updatedValue}})
  },
)
