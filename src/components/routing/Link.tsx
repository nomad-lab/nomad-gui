import React from 'react'
import {LinkProps} from 'wouter'

import useRouter from './useRouter'

const Link = React.forwardRef<HTMLAnchorElement, LinkProps>(function Link(
  props,
  ref,
) {
  const {createLink} = useRouter()
  return createLink(props, ref)
})

export default Link
