import {expect, it, vi} from 'vitest'

import matchRoute from './matchRoute'
import {Route} from './types'

describe('matchRoute', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })
  it.each([
    ['/list/testId', true, {id: 'testId'}],
    ['/list/testId/doesNotExist', false, {}],
    ['/rec', true, {}],
    ['/rec/1', true, {id: '1'}],
    ['/rec/1/2', true, {id: ['1', '2']}],
    ['/rec/1/2/end', true, {id: ['1', '2']}],
    ['/more/t/a/i/l', true, {}, 3],
    ['/withEmpty', true, {}, 3],
    ['/doesNotExist', false, {}],
    ['/withEmpty/doesNotExist', false, {}],
  ])(
    'should match %s correctly',
    (path, matches, params, length = undefined) => {
      const recRoute: Route = {
        path: ':id',
      }
      recRoute.children = [
        recRoute,
        {
          path: 'end',
        },
      ]
      const rootRoute = {
        path: '',
        children: [
          {
            path: 'more',
            children: [
              {
                path: '*',
              },
            ],
          },
          {
            path: 'list',
            children: [
              {
                path: ':id',
              },
            ],
          },
          {
            path: 'rec',
            children: [recRoute],
          },
          {
            path: 'withEmpty',
            children: [
              {
                path: '',
              },
            ],
          },
        ],
      }

      const match = matchRoute(path, rootRoute)
      expect(!!match).toBe(matches)
      if (!match) {
        return
      }
      if (length !== undefined) {
        expect(match?.length).toBe(length)
      } else {
        expect(match?.length).toBe(path.split('/').length)
      }
      const matchParams =
        match?.reduce((params, current) => {
          return {...params, ...current.params}
        }, {}) || {}
      expect(matchParams).toStrictEqual(params)
      match
        ?.filter((match, index) => index === 0 || match.route.path !== '')
        ?.forEach((match, index) => {
          expect(match.path).toBe(
            match.route.path === '*'
              ? path.split('/').slice(index).join('/')
              : path.split('/')[index],
          )
        })
    },
  )
})
