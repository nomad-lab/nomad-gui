import React, {useMemo} from 'react'
import {vi} from 'vitest'

import {runUseDataTests} from '../../hooks/useData.shared'
import {GraphResponse} from '../../models/graphResponseModels'
import MemoryRouter from './MemoryRouter'
import {Route} from './types'
import useDataForRoute from './useDataForRoute'

afterEach(() => {
  vi.restoreAllMocks()
})

describe('useDataForRoute', () => {
  const wrapper = function Wrapper({children}: {children: React.ReactNode}) {
    const childredRef = React.useRef<React.ReactNode>()
    childredRef.current = children
    const route = useMemo(
      () =>
        ({
          path: '/path',
          component: () => childredRef.current,
        } as Route),
      [childredRef],
    )
    return <MemoryRouter route={route} url='/path' />
  }

  runUseDataTests(
    useDataForRoute,
    (request) => ({path: request} as GraphResponse),
    wrapper,
  )
})
