import {SearchValue} from './types'
import useRoute from './useRoute'

export default function useSearchParameter<Type extends SearchValue>(
  name: string,
  defaultValue: Type,
): [Type, (value: Type) => void] {
  const {navigate, search} = useRoute<unknown, unknown, {[key: string]: Type}>()

  return [
    search[name] === undefined ? defaultValue : search[name],
    (value: Type) => {
      navigate({
        searchUpdates: {[name]: value === defaultValue ? undefined : value},
      })
    },
  ]
}
