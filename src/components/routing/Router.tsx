import React, {useCallback, useMemo} from 'react'
import {
  LinkProps,
  RouterOptions,
  Router as Wouter,
  Link as WouterLink,
  useLocation,
} from 'wouter'
import {useSearch} from 'wouter/use-location'

import Routes, {Location, RoutesProps} from './Routes'
import {RawSearchParams} from './types'
import {routerContext} from './useRouter'

const RouterLink = WouterLink as React.FunctionComponent<
  LinkProps & React.RefAttributes<HTMLAnchorElement>
>

function useRawSearch() {
  const searchString = useSearch()
  return useMemo(() => {
    const urlSearchParams = new URLSearchParams(
      searchString === '' ? '' : searchString.substring(1),
    )
    return Object.fromEntries(urlSearchParams.entries()) as RawSearchParams
  }, [searchString])
}

function WouterRoutes(props: Omit<RoutesProps, 'location' | 'navigate'>) {
  const [wouterLocation, setWouterLocation] = useLocation()
  const rawSearch = useRawSearch()
  const location = useMemo(() => {
    return {
      path: wouterLocation,
      rawSearch,
    } as Location
  }, [wouterLocation, rawSearch])

  const navigate = useCallback(
    (url: string) => {
      setWouterLocation(url)
    },
    [setWouterLocation],
  )
  return <Routes {...props} location={location} navigate={navigate} />
}

export default function Router({
  route,
  loader,
  ...wouterProps
}: Omit<RoutesProps, 'location' | 'navigate'> & RouterOptions) {
  const routerValue = useMemo(() => {
    return {
      navigate: () =>
        // eslint-disable-next-line no-console
        console.error(
          'Navigate is not necessary and not implemented for Router using browser location.',
        ),
      createLink: (props: LinkProps, ref: React.Ref<HTMLAnchorElement>) => (
        <RouterLink ref={ref} {...props} />
      ),
    }
  }, [])

  return (
    <Wouter {...wouterProps}>
      <routerContext.Provider value={routerValue}>
        <WouterRoutes route={route} loader={loader} />
      </routerContext.Provider>
    </Wouter>
  )
}
