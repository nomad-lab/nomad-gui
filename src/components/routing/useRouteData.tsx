import {DefaultSearch, Route} from './types'
import useRoute from './useRoute'

export function useAvailableRouteData<
  Request = unknown,
  Response = unknown,
  Search extends DefaultSearch = DefaultSearch,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
>(route?: Route<Request, Response, Search>) {
  const {response} = useRoute<Request, Response, Search>()
  return response as Response | undefined
}

export default function useRouteData<
  Request = unknown,
  Response = unknown,
  Search extends DefaultSearch = DefaultSearch,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
>(route?: Route<Request, Response, Search>) {
  const {response} = useRoute<Request, Response, Search>(route)
  if (!response) {
    throw new Error(
      'Route data should be available. This indicates a bug in <Routes/>.',
    )
  }
  return response as Response
}
