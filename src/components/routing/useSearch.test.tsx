import {render, screen} from '@testing-library/react'
import {expect, it} from 'vitest'

import Router from './Router'
import {Route} from './types'
import useRoute from './useRoute'
import useSearch from './useSearch'

it.each([[''], ['p=1'], ['p=1&q=2']])(
  'returns request params for %s',
  async (queryStr) => {
    function Component() {
      const search = useSearch<{[key: string]: string}>()
      const searchStr = Object.keys(search)
        .map((key) => `${key}=${search[key]}`)
        .join('&')
      return <div>{`search: ${searchStr}`.trim()}</div>
    }
    function App() {
      const route: Route = {
        path: '',
        children: [
          {
            path: '',
            component: Component,
          },
        ],
      }
      return <Router route={route} />
    }
    window.history.replaceState(null, '', `/?${queryStr}`)
    render(<App />)
    expect(
      await screen.findByText(`search: ${queryStr}`.trim()),
    ).toBeInTheDocument()
  },
)

it('validates search params', async () => {
  const route: Route<unknown, unknown, {p: number}> = {
    path: '',
    validateSearch: ({rawSearch}) => ({
      p: parseInt(rawSearch.p as string),
    }),
    component: Component,
  }
  function Component() {
    const {p} = useSearch(route)
    return <div>p: {p}</div>
  }
  window.history.replaceState(null, '', `/?p=1`)
  render(<Router route={{path: '', children: [route]}} />)
  expect(screen.getByText('p: 1')).toBeInTheDocument()
})

it('adds search params to relative path', async () => {
  function Component() {
    const {url} = useRoute()
    return <div>path: {url({search: {p: '1'}})}</div>
  }
  function App() {
    const route: Route = {
      path: '',
      children: [
        {
          path: '',
          component: Component,
        },
      ],
    }
    return <Router route={route} />
  }
  render(<App />)
  expect(screen.getByText('path: /?p=1')).toBeInTheDocument()
})
