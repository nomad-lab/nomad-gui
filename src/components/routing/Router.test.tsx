import {act, render, renderHook, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import Link from './Link'
import Router from './Router'
import useRoute from './useRoute'

describe('Router', () => {
  it.each([
    ['/fix', 'fix'],
    ['/variable', 'variable'],
    [encodeURI('/special!"§$%&()=s'), 'special!"§$%&()=s'],
    ['/variable?a=b', 'variable'],
  ])('matches and renders for %s', (location, result) => {
    const route = {
      path: '',
      children: [
        {
          path: 'fix',
          component: () => 'fix',
        },
        {
          path: ':variable',
          component: function MyComponent() {
            const {index, fullMatch} = useRoute()
            return fullMatch[index].params.variable
          },
        },
      ],
    }
    act(() => window.history.pushState(null, '', location))
    render(<Router route={route} />)
    expect(screen.getByText(result)).toBeInTheDocument()
  })

  it.each([
    ['/test', '/test'],
    ['/special!"§$%&()=s', '/' + encodeURIComponent('special!"§$%&()=s')],
    ['/params?a=b', '/' + encodeURIComponent('params?a=b')],
  ])('navigates to %s', (path, targetLocation) => {
    const {result} = renderHook(() => useRoute(), {
      wrapper: ({children}) => (
        <Router
          route={{
            path: '',
            children: [
              {path: 'fix', component: () => children},
              {path: ':variable', component: () => children},
            ],
          }}
        />
      ),
    })

    act(() => window.history.pushState(null, '', '/fix'))
    act(() => result.current.navigate({path}))
    expect(window.location.pathname).toBe(targetLocation)
  })

  it.each([
    ['/test', '/test'],
    ['/special!"§$%&()=s', '/' + encodeURIComponent('special!"§$%&()=s')],
    ['/params?a=b', '/' + encodeURIComponent('params?a=b')],
  ])('links to %s', async (path, targetLocation) => {
    render(
      <Router
        route={{
          path: '',
          children: [
            {
              path: 'start',
              component: function MyComponent() {
                const {url} = useRoute()
                return <Link to={url({path})} />
              },
            },
            {
              path: ':variable',
              component: function MyComponent() {
                return null
              },
            },
          ],
        }}
      />,
    )

    act(() => window.history.pushState(null, '', '/start'))
    await userEvent.click(screen.getByRole('link'))
    expect(window.location.pathname).toBe(targetLocation)
  })
})
