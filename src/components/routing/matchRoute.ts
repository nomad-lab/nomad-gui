import {PathParams, Route, RouteMatch} from './types'

export default function matchRoute(
  path: string,
  route: Route,
  parent?: Omit<RouteMatch, 'search'>,
): Omit<RouteMatch, 'search'>[] | undefined {
  const pattern = route.path
  const matchesAll = pattern === '*'

  const [head, tail] = (function split() {
    if (matchesAll) {
      return [path]
    }
    return path.split(/\/(.*)/, 2)
  })()

  const [matches, params] = (function match() {
    if (matchesAll) {
      return [true, {}]
    }
    if (pattern[0] === ':') {
      return [true, {[pattern.substring(1)]: head}]
    }
    return [pattern === head, {}]
  })() as [boolean, PathParams]

  if (!matches) {
    return undefined
  }

  Object.keys(params).forEach((key) => {
    const current = parent?.params?.[key]
    const newValue = params[key] as string
    if (current !== undefined) {
      if (typeof current === 'string') {
        params[key] = [current, newValue]
      } else {
        params[key] = [...current, newValue]
      }
    } else {
      params[key] = newValue
    }
  })

  let childPath = tail
  if (
    childPath === undefined &&
    route.children?.some((route) => route.path === '')
  ) {
    childPath = ''
  }

  const routeMatch: Omit<RouteMatch, 'search'> = {
    route,
    path: head,
    params,
    isLeaf: childPath === undefined,
  }

  if (childPath === undefined) {
    return [routeMatch]
  }

  if (childPath !== undefined && route.children) {
    const childMatches = route.children
      .map((route) => matchRoute(childPath, route, routeMatch) as RouteMatch[])
      .filter((match) => !!match)
      .sort((a, b) => b.length - a.length)
      .filter(
        (match, index, childMatches) => match.length === childMatches[0].length,
      )

    const nonMatchAllChildMatches = childMatches.filter(
      (match) => match[match.length - 1].route.path !== '*',
    )

    const nonVariableMatchingChildMatches = nonMatchAllChildMatches.filter(
      (match) => match[match.length - 1].route.path[0] !== ':',
    )

    if (nonVariableMatchingChildMatches.length > 0) {
      return [routeMatch, ...nonVariableMatchingChildMatches[0]]
    }
    if (nonMatchAllChildMatches.length > 0) {
      return [routeMatch, ...nonMatchAllChildMatches[0]]
    }
    if (childMatches.length > 0) {
      return [routeMatch, ...childMatches[0]]
    }
  }

  return undefined
}
