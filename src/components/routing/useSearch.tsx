import {DefaultSearch, Route} from './types'
import useRoute from './useRoute'

export default function useSearch<Search extends DefaultSearch = DefaultSearch>(
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  route?: Route<unknown, unknown, Search>,
) {
  const {search} = useRoute()
  return search
}
