import {User} from 'oidc-client-ts'

import {graphApi} from '../../utils/api'
import {resolveAllMDefs} from '../../utils/metainfo'
import {JSONObject} from '../../utils/types'
import {assert} from '../../utils/utils'
import {DefaultSearch, LoaderResult, RouteMatch} from './types'

export class DoesNotExistError extends Error {
  constructor(message: string) {
    super(message)
    this.name = 'DoesNotExist'
  }
}

/**
 * Parses the given key and returns the base key and the index if the key is
 * if the key is indexed (e.g. 'key[0]'). Otherwise, returns the key itself ans
 * `undefined`.
 */
export function getIndexedKey(key: string): [string, number | undefined] {
  const indexKeyMatch = key.match(/(?<key>\w+)\[(?<index>\d+)\]/)
  if (indexKeyMatch) {
    const arrayKey = indexKeyMatch.groups?.key as string
    const arrayIndex = parseInt(indexKeyMatch.groups?.index as string)
    return [arrayKey, arrayIndex]
  } else {
    return [key, undefined]
  }
}

export function getResponseForPath(
  path: string,
  response: JSONObject,
  raiseDoesNotExistError: boolean = true,
): JSONObject | undefined {
  let currentResponse: JSONObject | undefined = response
  let result
  path.split('/').forEach((pathSegment) => {
    const [key, index] = getIndexedKey(pathSegment)
    if (index !== undefined) {
      currentResponse = (currentResponse?.[key] as JSONObject[] | undefined)?.[
        index
      ]
    } else if (key !== '') {
      currentResponse = currentResponse?.[key] as JSONObject | undefined
    }
    if (currentResponse === undefined && raiseDoesNotExistError) {
      throw new DoesNotExistError(`The entity with id ${key} does not exist.`)
    }
    result = currentResponse
  })
  return result
}

export function getResponsesForRoute(
  match: RouteMatch[],
  response: JSONObject,
): (JSONObject | undefined)[] {
  let parentPath: string | undefined
  const requestPaths: (string | undefined)[] = match.map((match) => {
    const path = getRequestPath(match, parentPath)
    if (path !== undefined) {
      parentPath = path
    }
    return path
  })
  return requestPaths.map((path, index) => {
    if (path === undefined) {
      return undefined
    }
    const hasRequest = match[index].route.request !== undefined
    return getResponseForPath(path, response, hasRequest)
  })
}

function getRequestPath(
  match: RouteMatch,
  parentPath?: string,
): string | undefined {
  if (match.route.requestPath) {
    if (typeof match.route.requestPath === 'string') {
      return match.route.requestPath
    } else {
      return match.route.requestPath({
        ...match,
        search: match.search as DefaultSearch,
      })
    }
  }
  const key = getRequestKey(match)
  if (key === '') {
    return parentPath
  }
  if (parentPath === undefined) {
    return key
  } else {
    return `${parentPath}/${key}`
  }
}

function getRequestKey(match: RouteMatch) {
  if (match.route.requestKey === undefined) {
    return match.path
  }
  if (typeof match.route.requestKey === 'string') {
    return match.route.requestKey
  }
  return match.route.requestKey({
    ...match,
    search: match.search as DefaultSearch,
  })
}

export function createRequestForRoute(
  match: RouteMatch[],
  getRequest: (match: RouteMatch, index: number) => JSONObject | undefined,
  routeRequests?: JSONObject[],
): JSONObject {
  const requests: {[path: string]: JSONObject} = {}
  let parentPath: string | undefined
  match.forEach((match, index) => {
    const request = getRequest(match, index)
    const path = getRequestPath(match, parentPath)
    if (request) {
      assert(path !== undefined, 'Request for empty paths are not supported.')
      requests[path] = request
    }
    parentPath = path
  })

  const rootRequest = {} as JSONObject
  Object.entries(requests).forEach(([path, request]) => {
    const segments = path.split('/')
    let currentRequest: JSONObject = rootRequest
    segments.forEach((key) => {
      if (currentRequest[key] === undefined) {
        currentRequest[key] = {}
      }
      currentRequest = currentRequest[key] as JSONObject
    })
    Object.assign(currentRequest, request)
  })

  routeRequests?.push(rootRequest)
  return rootRequest
}

export default function loader(
  match: RouteMatch[],
  user?: User,
): (() => Promise<LoaderResult>) | undefined {
  let isEmpty = true
  function getRequest(routeMatch: RouteMatch) {
    if (!routeMatch.route.request) {
      return
    }
    const request = routeMatch.route.request
    isEmpty = false
    if (typeof request === 'function') {
      return request({
        ...routeMatch,
        search: routeMatch.search as DefaultSearch,
      }) as JSONObject
    }
    return {...request}
  }

  const routeRequests = [] as JSONObject[]
  const request = createRequestForRoute(match, getRequest, routeRequests)

  if (isEmpty) {
    return
  }

  return async () => {
    const response = (await graphApi(request, user)) as JSONObject
    // TODO We should only resolve m_defs in archive data and not in uploads,
    // entries, files, etc. Currently we apply this to all and the complete
    // responses no matter what they are.
    resolveAllMDefs(response)
    return {
      routeResponses: getResponsesForRoute(match, response as JSONObject),
      routeRequests: routeRequests,
      request,
      response,
    }
  }
}
