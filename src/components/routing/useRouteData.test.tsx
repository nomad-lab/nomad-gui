import {act, render, screen} from '@testing-library/react'
import {AuthContextProps} from 'react-oidc-context'
import {MockedFunction, expect, it, vi} from 'vitest'

import * as useAuth from '../../hooks/useAuth'
import {GraphResponse} from '../../models/graphResponseModels'
import * as api from '../../utils/api'
import {renderCount} from '../../utils/test.helper'
import {JSONObject} from '../../utils/types'
import {assert} from '../../utils/utils'
import Outlet from './Outlet'
import Router from './Router'
import loader from './loader'
import {Route} from './types'
import useRouteData from './useRouteData'

let mockedApi: MockedFunction<typeof api.graphApi>
let mockedAuth: MockedFunction<typeof useAuth.default>

beforeEach(() => {
  mockedApi = vi.spyOn(api, 'graphApi') as MockedFunction<typeof api.graphApi>
  mockedAuth = vi.spyOn(useAuth, 'default') as MockedFunction<
    typeof useAuth.default
  >
})

afterEach(() => {
  vi.restoreAllMocks()
})

describe('useRouteData', () => {
  it('should render data with useRouteData', async () => {
    const dataRoute: Route<JSONObject, JSONObject, {key: string}> = {
      path: 'child',
      validateSearch: ({rawSearch, user}) => {
        assert(user?.profile.sub === 'user_id', 'User should be available')
        return {
          key: rawSearch.key || 'q',
        }
      },
      request: ({search, user}) => {
        assert(user?.profile.sub === 'user_id', 'User should be available')
        return {
          [search.key]: '*',
        }
      },
      component: Component,
    }
    const route: Route = {
      path: '',
      children: [
        {
          path: 'parent',
          children: [dataRoute],
        },
      ],
    }

    function Component() {
      renderCount()
      const data = useRouteData(dataRoute)
      return <div>key: {Object.keys(data).join('')}</div>
    }

    window.history.replaceState(null, '', '/parent/child')
    mockedApi.mockResolvedValue({
      parent: {child: {q: 'test_value'}},
    } as GraphResponse)
    mockedAuth.mockReturnValue({
      user: {profile: {sub: 'user_id'}},
    } as AuthContextProps)

    render(<Router route={route} loader={loader} />)
    expect(await screen.findByText('key: q')).toBeInTheDocument()
    expect(renderCount).toBeCalledTimes(1)
    expect(mockedApi).toBeCalledTimes(1)
    expect(mockedApi).toHaveBeenCalledWith(
      {parent: {child: {q: '*'}}},
      useAuth.default().user,
    )

    mockedApi.mockResolvedValue({
      parent: {child: {o: 'test_value'}},
    } as GraphResponse)
    await act(() => window.history.pushState(null, '', '/parent/child?key=o'))
    expect(screen.getByText('key: o')).toBeInTheDocument()
    expect(renderCount).toBeCalledTimes(2)
    expect(mockedApi).toBeCalledTimes(2)
    expect(mockedApi).toHaveBeenCalledWith(
      {parent: {child: {o: '*'}}},
      useAuth.default().user,
    )
  })

  it.each([
    ['whole route', '/data', {data: {dataKey: '*'}}, {dataKey: '*'}],
    [
      'sub route without parent request',
      '/data/more',
      {data: {more: {moreKey: '*'}}},
    ],
    [
      'sub route with parent request',
      '/data/more',
      {data: {dataKey: '*', more: {moreKey: '*'}}},
      {dataKey: '*'},
    ],
    [
      'sub route with parent overlapping request',
      '/data/more',
      {data: {more: {otherMoreKey: '*', moreKey: '*'}}},
      {more: {otherMoreKey: '*'}},
    ],
    [
      'sub route with parent overlapping query request',
      '/data/more',
      {data: {more: {otherMoreKey: {more: '*'}, '*': '*'}}},
      {more: {otherMoreKey: {more: '*'}}},
      {'*': '*'},
    ],
    [
      'sub route with two overlapping query requests',
      '/data/more',
      {
        data: {
          m_request: {optKey: 'value'},
          dataKey: '*',
          more: {
            m_request: {optKey: 'value'},
            moreKey: '*',
            otherMoreKey: '*',
            '*': '*',
          },
          '*': '*',
        },
      },
      {
        m_request: {optKey: 'value'},
        dataKey: '*',
        more: {otherMoreKey: '*'},
        '*': '*',
      },
      {m_request: {optKey: 'value'}, moreKey: '*', '*': '*'},
    ],
  ])(
    'uses the right request for %s',
    async (
      description,
      path,
      resultRequest,
      dataRequest?: JSONObject,
      moreRequest?: JSONObject,
    ) => {
      const route: Route = {
        path: '',
        component: Outlet,
        children: [
          {
            path: 'data',
            request: dataRequest,
            component: Outlet,
            children: [
              {
                path: ':more',
                request: moreRequest || {moreKey: '*'},
                component: 'div',
              },
            ],
          },
        ],
      }
      mockedApi.mockResolvedValue({
        data: {
          dataKey: 'value',
          more: {moreKey: 'value', otherMoreKey: 'value'},
        },
      } as GraphResponse)
      window.history.replaceState(null, '', path)
      await act(() => render(<Router route={route} loader={loader} />))
      expect(mockedApi).toBeCalledTimes(1)
      expect(mockedApi).toBeCalledWith(resultRequest, undefined)
    },
  )

  it('builds the right request for empty nested routes', async () => {
    function Component() {
      useRouteData()
      return <div>hello</div>
    }
    const route: Route = {
      path: '',
      children: [
        {
          path: 'parent',
          children: [
            {
              path: '',
              request: {'*': '*'},
              component: Component,
            },
          ],
        },
      ],
    }
    mockedApi.mockResolvedValue({parent: {}} as GraphResponse)
    window.history.replaceState(null, '', '/parent')
    await act(() => render(<Router route={route} loader={loader} />))
    expect(await screen.findByText('hello')).toBeInTheDocument()
    expect(mockedApi).toHaveBeenCalledWith({parent: {'*': '*'}}, undefined)
  })

  it('calls onFetch', async () => {
    const onFetch = vi.fn()
    const route: Route = {
      path: '',
      component: Outlet,
      children: [
        {
          path: 'data',
          request: {dataKey: '*'},
          onFetch,
          component: () => <div />,
        },
      ],
    }
    mockedApi.mockResolvedValue({
      data: {
        dataKey: 'value',
      },
    } as GraphResponse)
    window.history.replaceState(null, '', '/data')
    await act(() => render(<Router route={route} loader={loader} />))
    expect(mockedApi).toBeCalledTimes(1)
    expect(onFetch).toBeCalledTimes(1)
    expect(onFetch).toBeCalledWith(
      expect.objectContaining({
        response: {dataKey: 'value'},
        fullResponse: {data: {dataKey: 'value'}},
      }),
    )
  })
})
