import {useCallback} from 'react'

import {Pagination1} from '../../models/graphRequestModels'
import {PaginationResponse} from '../../models/graphResponseModels'
import {ENV} from '../../utils/env'
import {PageBasedPagination} from '../../utils/types'
import {ValidateSearchParams} from './types'
import useRoute from './useRoute'
import useRouteData from './useRouteData'

type Pagination = Required<PaginationResponse>
type PaginationUpdate = Pick<
  Pagination1,
  'page' | 'page_size' | 'order' | 'order_by'
>

export const defaultPagination = {
  page: 1,
  page_size: ENV.LOAD_MORE_PAGE_SIZE,
}

export function createPaginationRequest(search: Required<PageBasedPagination>) {
  return {
    page: search.page,
    page_size: search.page_size,
    order: search.order,
    order_by: search.order_by,
  }
}

export function validatePaginationSearch<
  Response,
  Request,
  Search extends Required<PaginationUpdate>,
>({rawSearch}: ValidateSearchParams<Response, Request, Search>) {
  return {
    page: parseInt(rawSearch.page) || defaultPagination.page,
    page_size: parseInt(rawSearch.page_size) || defaultPagination.page_size,
    order: rawSearch.order as 'asc' | 'desc',
    order_by: rawSearch.order_by,
  }
}

export default function usePagination(): [
  Pagination,
  (update: PaginationUpdate) => void,
] {
  const response = useRouteData<
    unknown,
    {m_response: {pagination: PaginationResponse}}
  >()
  const {navigate} = useRoute()
  // TODO if the API works correctly there should always be an `m_response.pagination`.
  // We are still checking for it and use a default response in case it is not there.
  const pagination =
    (response?.m_response
      ?.pagination as unknown as Required<PaginationResponse>) ||
    defaultPagination

  const setPagination = useCallback(
    (update: Partial<PageBasedPagination>) => {
      navigate({searchUpdates: update})
    },
    [navigate],
  )

  return [pagination, setPagination]
}
