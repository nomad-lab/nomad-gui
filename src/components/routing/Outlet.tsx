import useRouteContext from './useRouteContext'

export default function Outlet() {
  const {element} = useRouteContext()
  return element || null
}
