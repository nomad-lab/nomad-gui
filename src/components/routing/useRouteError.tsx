import useRoute from './useRoute'

export default function useRouteError() {
  const {error} = useRoute()
  return error as Error
}
