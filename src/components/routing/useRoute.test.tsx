import {
  act,
  fireEvent,
  render,
  renderHook,
  screen,
  waitFor,
} from '@testing-library/react'
import {PropsWithChildren, useState} from 'react'
import {expect, it, vi} from 'vitest'

import {JSONObject} from '../../utils/types'
import MemoryRouter from './MemoryRouter'
import Outlet from './Outlet'
import Router from './Router'
import Routes from './Routes'
import {ErrorComponent} from './Routes.test'
import {LocationParams, RawSearchParams, Route, RouteMatch} from './types'
import useRoute from './useRoute'

describe('useRoute', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })
  describe('params', () => {
    const countRender = vi.fn()
    const collectIndex = vi.fn()
    function useCollectIndex() {
      const {index} = useRoute()
      collectIndex(index)
    }
    const recursiveRoute: Route = {
      path: ':name',
      component: () => {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        useCollectIndex()
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const {params, isLeaf} = useRoute()
        if (!isLeaf) {
          return <Outlet />
        }
        return <div>{JSON.stringify(params.name)}</div>
      },
    }
    recursiveRoute.children = [recursiveRoute]
    function App() {
      const route: Route = {
        path: '',
        component: () => {
          // eslint-disable-next-line react-hooks/rules-of-hooks
          useCollectIndex()
          countRender()
          return <Outlet />
        },
        children: [
          {
            path: 'list/:id',
            component: function Component() {
              useCollectIndex()
              const {params} = useRoute()
              return <div>{params.id}</div>
            },
          },
          {
            path: 'nestedIndex',
            children: [
              {
                path: ':id',
                component: function Component() {
                  useCollectIndex()
                  const {params} = useRoute()
                  return (
                    <div>
                      <div>{params.id}</div>
                      <Outlet />
                    </div>
                  )
                },
                children: [
                  {
                    path: ':id',
                    component: function Component() {
                      useCollectIndex()
                      const {params} = useRoute()
                      return <div>{params.id}</div>
                    },
                  },
                ],
              },
            ],
          },
          {
            path: 'recursive',
            children: [recursiveRoute],
          },
        ],
      }
      return <Router route={route} />
    }
    it.each([
      ['/list/testId', 'testId', 2],
      ['/nestedIndex/parent/child', 'parent', 3],
      ['/recursive/n1/n2/n3', '["n1","n2","n3"]', 4],
    ])('should provide params for %s', (path, text, index) => {
      window.history.replaceState(null, '', path)
      render(<App />)
      expect(screen.getByText(text)).toBeInTheDocument()
      expect(countRender).toBeCalledTimes(1)
      expect(collectIndex).toBeCalledWith(index)
    })
  })

  describe('data, isLoading', () => {
    beforeEach(() => window.history.replaceState(null, '', '/'))

    const countRootRender = vi.fn()
    const countLeafRender = vi.fn()
    const assertIsLoading = vi.fn()
    function App() {
      function loader(match: RouteMatch[]) {
        if (match[1].path === 'throws') {
          return async () => {
            throw new Error('api error')
          }
        }
        if (match[1].path !== 'list') {
          return undefined
        }
        return async () => {
          return {
            routeResponses: match.map(
              (match) =>
                ({path: match.route.path, id: match.params.id} as JSONObject),
            ),
            routeRequests: [],
            request: {},
            response: {},
          }
        }
      }
      const route: Route = {
        path: '',
        component: function Root() {
          const {isLoading} = useRoute()
          assertIsLoading(isLoading)
          countRootRender()
          return (
            <div>
              <div>Root</div>
              {isLoading && <div>loading</div>}
              <Outlet />
            </div>
          )
        },
        errorComponent: ErrorComponent,
        children: [
          {
            path: '',
            component: () => {
              countLeafRender()
              return (
                <div>
                  <div>Index</div>
                </div>
              )
            },
          },
          {
            path: 'redirect/:id',
            redirect: ({params, url}) =>
              url({path: ['..', '..', 'list', params.id as string]}),
          },
          {
            path: 'list',
            component: function Component() {
              const {response} = useRoute()
              return (
                <div>
                  <div>List</div>
                  <div>{response?.path as string}</div>
                  <Outlet />
                </div>
              )
            },
            children: [
              {
                path: ':id',
                component: function Component() {
                  countLeafRender()
                  const {response} = useRoute()
                  return (
                    <div>
                      <div>Item</div>
                      <div>{response?.path as string}</div>
                      <div>{response?.id as string}</div>
                    </div>
                  )
                },
              },
            ],
          },
          {
            path: 'noLoad',
            component: () => {
              countLeafRender()
              return <div>No load</div>
            },
          },
          {
            path: 'throws',
            component: () => {
              countLeafRender()
              return <div>Never renders</div>
            },
          },
        ],
      }
      return <Router route={route} loader={loader} />
    }

    it.each([
      ['/list/testId', ['list', ':id', 'testId'], 2, 1, true],
      ['/noLoad', ['No load'], 1, 1, false],
    ])(
      'should load and provide data for %s',
      async (path, texts, rootRenderCount, leafRenderCount, isLoading) => {
        window.history.replaceState(null, '', path)
        render(<App />)
        expect(screen.getByText('Root')).toBeInTheDocument()
        expect(assertIsLoading).toBeCalledWith(isLoading)
        for (const text of texts) {
          expect(await screen.findByText(text)).toBeInTheDocument()
        }
        expect(countRootRender).toBeCalledTimes(rootRenderCount)
        expect(countLeafRender).toBeCalledTimes(leafRenderCount)
        expect(assertIsLoading).toBeCalledWith(false)
      },
    )

    it('should render error when load throws', async () => {
      window.history.replaceState(null, '', '/throws')
      render(<App />)
      expect(countRootRender).toBeCalledTimes(1)
      expect(await screen.findByText('Error')).toBeInTheDocument()
      expect(screen.getByText('api error')).toBeInTheDocument()
      expect(countRootRender).toBeCalledTimes(1)
      expect(countLeafRender).toBeCalledTimes(0)
    })

    it('should render on route change after load throws', async () => {
      window.history.replaceState(null, '', '/throws')
      render(<App />)
      expect(await screen.findByText('Error')).toBeInTheDocument()
      act(() => window.history.pushState(null, '', '/noLoad'))
      expect(await screen.findByText('No load')).toBeInTheDocument()
    })

    it('should render on route change with data after load throws', async () => {
      window.history.replaceState(null, '', '/throws')
      render(<App />)
      expect(await screen.findByText('Error')).toBeInTheDocument()

      act(() => window.history.pushState(null, '', '/list/testId2'))
      expect(screen.getByText('loading')).toBeInTheDocument()
      expect(await screen.findByText('testId2')).toBeInTheDocument()
    })

    it('should redirect and render and load correctly', async () => {
      window.history.replaceState(null, '', '/redirect/testId1')
      render(<App />)
      expect(await screen.findByText('testId1')).toBeInTheDocument()
      expect(countRootRender).toBeCalledTimes(2)
      expect(countLeafRender).toBeCalledTimes(1)
    })

    it('should rerender when location changes', async () => {
      global.console.error = vi.fn()
      render(<App />)
      expect(screen.getByText('Index')).toBeInTheDocument()
      expect(countRootRender).toBeCalledTimes(1)
      expect(countLeafRender).toBeCalledTimes(1)

      act(() => window.history.pushState(null, '', '/noLoad'))
      expect(await screen.findByText('No load')).toBeInTheDocument()
      expect(screen.queryByText('loading')).not.toBeInTheDocument()
      expect(countRootRender).toBeCalledTimes(2)
      expect(countLeafRender).toBeCalledTimes(2)

      act(() => window.history.pushState(null, '', '/list/testId1'))
      await waitFor(() =>
        expect(screen.getByText('loading')).toBeInTheDocument(),
      )

      expect(await screen.findByText('testId1')).toBeInTheDocument()
      expect(screen.getByText('Item')).toBeInTheDocument()
      expect(screen.getByText('List')).toBeInTheDocument()
      expect(screen.queryByText('loading')).not.toBeInTheDocument()
      expect(countRootRender).toBeCalledTimes(4)
      expect(countLeafRender).toBeCalledTimes(3)

      act(() => window.history.pushState(null, '', '/list/testId2'))
      await waitFor(() =>
        expect(screen.getByText('loading')).toBeInTheDocument(),
      )

      expect(await screen.findByText('testId2')).toBeInTheDocument()
      expect(screen.getByText('Item')).toBeInTheDocument()
      expect(screen.getByText('List')).toBeInTheDocument()
      expect(screen.queryByText('loading')).not.toBeInTheDocument()
      expect(countRootRender).toBeCalledTimes(6)
      expect(countLeafRender).toBeCalledTimes(4)

      act(() => window.history.pushState(null, '', '/noLoad'))
      expect(await screen.findByText('No load')).toBeInTheDocument()
      expect(screen.queryByText('loading')).not.toBeInTheDocument()
      expect(countRootRender).toBeCalledTimes(7)
      expect(countLeafRender).toBeCalledTimes(5)

      expect(global.console.error).toBeCalledTimes(0)
    })
  })

  describe('url', () => {
    const lastUrl = {
      current: null,
    } as {current: (() => string) | null}

    const App = ({
      path = '/parent/child',
      rawSearch = {},
      urlParams = {},
    }: {
      path?: string
      rawSearch?: RawSearchParams
      urlParams?: LocationParams
    }) => {
      function Component() {
        const setRender = useState(0)[1]
        const {url} = useRoute()
        lastUrl.current = url
        return (
          <>
            <div>{url(urlParams)}</div>
            <div onClick={() => setRender((i) => i + 1)}>rerender</div>
          </>
        )
      }

      return (
        <Routes
          route={{
            path: '',
            children: [
              {
                path: 'parent',
                component: function C() {
                  return (
                    <>
                      <Component />
                      <Outlet />
                    </>
                  )
                },
                children: [
                  {
                    path: 'child',
                    component: Component,
                    validateSearch: ({rawSearch}) => ({
                      p: parseInt(rawSearch.p) || 0,
                    }),
                  },
                ],
              },
            ],
          }}
          location={{path, rawSearch}}
          navigate={() => {}}
        />
      )
    }

    it("should provide path that is relative to component's route", () => {
      render(<App urlParams={{path: 'test'}} />)
      expect(screen.getByText('/parent/child/test')).toBeInTheDocument()
      expect(screen.getByText('/parent/test')).toBeInTheDocument()
    })

    it('should use rawSearch', () => {
      render(<App urlParams={{rawSearch: {p: '2'}}} />)
      expect(screen.getByText('/parent/child?p=2')).toBeInTheDocument()
    })

    it('should use search', () => {
      render(<App urlParams={{search: {p: 2}}} />)
      expect(screen.getByText('/parent/child?p=2')).toBeInTheDocument()
    })

    it('should use search updates', () => {
      render(<App urlParams={{searchUpdates: {p: 2}}} />)
      expect(screen.getByText('/parent/child?p=2')).toBeInTheDocument()
    })

    it('should remove search parameters', () => {
      render(
        <App
          rawSearch={{p: '1'}}
          urlParams={{searchUpdates: {p: undefined}}}
        />,
      )
      expect(screen.getByText('/parent/child')).toBeInTheDocument()
    })

    it('supports absolute paths', () => {
      render(<App urlParams={{path: '/parent/test'}} />)
      expect(screen.getAllByText('/parent/test')).toHaveLength(2)
    })

    it('is memoized when rerendering same rout', () => {
      render(<App urlParams={{path: '/parent/test'}} />)
      const firstUrl = lastUrl.current
      act(() => screen.getAllByText('rerender')[1].click())
      expect(lastUrl.current).toBe(firstUrl)
    })
  })

  describe('navigate', () => {
    const navigate = vi.fn()
    const App = ({
      path = '/parent',
      rawSearch = {},
      navigateParams = {},
    }: {
      path?: string
      rawSearch?: RawSearchParams
      navigateParams?: LocationParams
    }) => {
      function Component() {
        const {navigate} = useRoute()
        return <button onClick={() => navigate(navigateParams)}>press</button>
      }

      return (
        <Routes
          route={{
            path: '',
            children: [
              {
                path: 'parent',
                component: Component,
              },
            ],
          }}
          location={{path, rawSearch}}
          navigate={navigate}
        />
      )
    }

    it('should navigate', () => {
      render(<App navigateParams={{path: 'test'}} />)
      const button = screen.getByRole('button')
      act(() => fireEvent.click(button))
      expect(navigate).toBeCalledWith('/parent/test')
    })
  })

  describe('relativePath', () => {
    const renderOptions = {
      wrapper: ({children}: PropsWithChildren) => {
        const recursiveRoute: Route = {
          path: ':id',
          component: () => children,
        }
        recursiveRoute.children = [recursiveRoute]

        const route = {
          path: '',
          children: [
            {
              path: '1',
              children: [recursiveRoute],
            },
            {
              path: 'a',
              children: [recursiveRoute],
            },
          ],
        }

        return <MemoryRouter route={route} url={'/1/2'} />
      },
    }
    it.each([
      ['/1/a', '../a', undefined],
      ['/1', '..', undefined],
      ['/1/2', '.', undefined],
      ['/1/2/a', './a', undefined],
      ['/a', '/a', undefined],
      ['/a/b', '../b', '/a/c'],
    ])(
      'should return relative path for %s',
      (path, expectedRelativePath, referencePath) => {
        const {result} = renderHook(() => useRoute(), renderOptions)
        expect(result.current.relativePath(path, referencePath)).toBe(
          expectedRelativePath,
        )
      },
    )
  })

  describe('matches', () => {
    it.each([
      ['match parent', '/parent', true],
      ['match full route', '/parent/child', true],
      ['not match alternative route', '/alternative', false],
      ['not match none existing route', '/doesnotexist', false],
    ])('should %s', async (description, path, result) => {
      const {result: renderResult} = renderHook(() => useRoute(), {
        wrapper: (props) => {
          const {children} = props
          const route = {
            path: '',
            children: [
              {
                path: 'parent',
                component: Outlet,
                children: [
                  {
                    path: 'child',
                    component: () => <div>{children}</div>,
                  },
                ],
              },
              {
                path: 'alternative',
                component: () => <div>{children}</div>,
              },
            ],
          } as Route

          return (
            <Routes
              location={{path: '/parent/child', rawSearch: {}}}
              route={route}
              navigate={vi.fn()}
              {...props}
            />
          )
        },
      })
      expect(renderResult.current).not.toBe(null)
      expect(renderResult.current.matches(path)).toBe(result)
    })
  })

  // This would be a nice optional property to have in some cases.
  // Maybe during a refactor of useRoute
  // it('should not re-render parent component on child route change', async () => {
  //   const countParentRender = vi.fn()
  //   const countChildRender = vi.fn()

  //   const route = {
  //     path: '',
  //     component: Parent,
  //     children: [
  //       {
  //         path: ':id',
  //         component: Child,
  //       },
  //     ],
  //   }

  //   function Parent() {
  //     countParentRender()
  //     return <Outlet />
  //   }

  //   function Child() {
  //     countChildRender()
  //     return <div>child</div>
  //   }

  //   window.history.replaceState(null, '', '/child1')
  //   render(<Router route={route} />)

  //   expect(countParentRender).toBeCalledTimes(1)
  //   expect(countChildRender).toBeCalledTimes(1)

  //   await act(() => window.history.pushState(null, '', '/child2'))

  //   expect(countChildRender).toBeCalledTimes(2)
  //   expect(countParentRender).toBeCalledTimes(1)
  // })
})
