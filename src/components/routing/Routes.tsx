import React, {
  ElementType,
  ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useRef,
} from 'react'
import {ErrorBoundary} from 'react-error-boundary'
import {useCounter} from 'react-use'
import {AsyncState} from 'react-use/lib/useAsyncFn'

import useAsyncConditional from '../../hooks/useAsyncConditional'
import useAuth from '../../hooks/useAuth'
import {assert} from '../../utils/utils'
import DevToolsJsonViewer from '../devTools/DevToolsJsonViewer'
import useDevTools from '../devTools/useDevTools'
import matchRoute from './matchRoute'
import normalizeRoute from './normalizeRoute'
import {
  Loader,
  LoaderResult,
  LocationParams,
  RawSearchParams,
  Route,
  RouteData,
  RouteMatch,
} from './types'
import {
  GlobalRouteData,
  RouteContextValue,
  routeContext,
} from './useRouteContext'

class RouteError extends Error {
  constructor(message: string) {
    super(message)
    this.name = 'RouteError'
  }
}

/**
 * Computes the relative path from one path to another path.
 */
function relativePath(
  path: string,
  referencePath: string,
  route: Route,
): string {
  const pathMatch = matchRoute(path, route)
  const referencePathMatch = matchRoute(referencePath, route)
  assert(pathMatch !== undefined, 'Path does not match any route.')
  assert(
    referencePathMatch !== undefined,
    'Reference path does not match any route.',
  )

  // Compute the first index at which path and current route diverge
  // and do not use the same route spec.
  let divergingIndex = referencePathMatch.findIndex((match, index) => {
    return pathMatch[index]?.path !== match.path
  })
  if (divergingIndex === -1) {
    divergingIndex = referencePathMatch.length
  }

  const resultSegments = []
  for (let i = divergingIndex; i < referencePathMatch.length; i++) {
    // We do not allow .. relative paths beyond static segments
    if (!referencePathMatch[i].route.path.startsWith(':')) {
      return path
    }
    resultSegments.push('..')
  }
  if (resultSegments.length === 0) {
    // Path and current route only diverge at the end of the current route
    resultSegments.push('.')
  }

  for (let i = divergingIndex; i < pathMatch.length; i++) {
    resultSegments.push(pathMatch[i].path)
  }

  return resultSegments.join('/')
}

/**
 * A hook that ensures that all lazy components for the given route match are loaded.
 * If there are still unloaded lazy components left, it will return true and
 * rerender the component once all lazy components are loaded.
 *
 * @param fullMatch The full route match.
 * @returns A boolean that indicates whether lazy components are still loading.
 */
function useLoadLazyComponents(fullMatch: RouteMatch[]): AsyncState<void> {
  const loadLazyComponents = useMemo(() => {
    const routesWithLazyComponent = fullMatch
      .map((routeMatch) => routeMatch.route)
      .filter((route) => route.lazyComponent && !route.component)

    if (routesWithLazyComponent.length === 0) {
      return undefined
    }

    return async () => {
      for (const route of routesWithLazyComponent) {
        const lazyComponent = route.lazyComponent as () => Promise<{
          default: ElementType
        }>
        route.component = (await lazyComponent()).default
      }
    }
  }, [fullMatch])

  return useAsyncConditional(loadLazyComponents, [loadLazyComponents])
}

/**
 * A hook that fetches the data for the given route match if necessary and
 * adds it to the route matches. It returns an `AsyncState`. The
 * state will be `isLoading: true` if new data needs to be fetched and has
 * not yet been fetched.
 *
 * @param fullMatch The full route match.
 * @param loader The loader to use for fetching the data.
 * @returns The data fetching state.
 */
function useLoadRouteData(
  fullMatch: RouteMatch[],
  loader: Loader,
  reloadCount?: number,
): AsyncState<LoaderResult | undefined> {
  const {user} = useAuth()
  const fetch = useMemo(
    () => loader(fullMatch, user || undefined),
    [fullMatch, loader, user],
  )
  const state = useAsyncConditional(fetch, [fetch, reloadCount])
  if (state.value) {
    state.value.routeResponses.forEach((response, index) => {
      const match = fullMatch[index]
      if (response && state.value?.response) {
        match.response = response
        try {
          match.route.onFetch?.({
            fullMatch,
            match,
            response,
            request: state.value.routeRequests[index],
            fullRequest: state.value?.request,
            fullResponse: state.value?.response,
          })
        } catch (e) {
          state.error = e as Error
        }
      }
    })
  }
  return state
}

function useRouteMatch(route: Route, {path, rawSearch}: Location) {
  const {user} = useAuth()
  const normalizedRoute = useMemo(() => normalizeRoute(route), [route])
  const fullMatch = useMemo(() => {
    const fullMatch = matchRoute(
      path.split('/').map(decodeURIComponent).join('/'),
      normalizedRoute,
    )
    if (!fullMatch) {
      return
    }
    const result: RouteMatch[] = []
    for (const routeMatch of fullMatch) {
      const route = routeMatch.route
      const props = {
        ...routeMatch,
        location: path,
        rawSearch,
        user: user ?? undefined,
      }
      const search = route.validateSearch?.(props) || {}
      result.push({
        ...routeMatch,
        search: {
          ...(routeMatch === fullMatch[0] ? rawSearch : {}),
          ...(result[result.length - 1]?.search || {}),
          ...search,
        },
        user: user ?? undefined,
      })
    }
    return result
  }, [path, normalizedRoute, rawSearch, user])

  return useMemo(
    () =>
      ({
        fullMatch,
        location: path,
        rawSearch,
      } as Pick<RouteData, 'location' | 'rawSearch' | 'fullMatch'>),
    [fullMatch, path, rawSearch],
  )
}

export type Location = {
  path: string
  rawSearch: RawSearchParams
}

export type RoutesProps = {
  route: Route
  loader?: Loader
  location: Location
  navigate: (url: string) => void
  onNavigate?: (
    data: Pick<RouteData, 'location' | 'rawSearch' | 'fullMatch'>,
  ) => void
}

export default function Routes({
  route,
  loader = () => undefined,
  location,
  navigate,
  onNavigate,
}: RoutesProps) {
  const [reloadCount, {inc: reload}] = useCounter()
  const matchData = useRouteMatch(route, location)
  const {fullMatch} = matchData
  const lastRenderResult = useRef(<></>)

  const isLoadingLazyComponents = useLoadLazyComponents(fullMatch || []).loading
  const routeData = useLoadRouteData(fullMatch || [], loader, reloadCount)
  const isLoadingRouteData = routeData.loading
  const isLoading = isLoadingRouteData || isLoadingLazyComponents

  const devTools = useDevTools()
  useEffect(() => {
    devTools.registerTool(
      'routing',
      <DevToolsJsonViewer
        value={{
          match: fullMatch,
          data: routeData.value?.routeResponses,
          request: routeData.value?.request,
          response: routeData.value?.response,
        }}
      />,
    )
    return () => devTools.unregisterTool('routes-data')
  }, [routeData, fullMatch, devTools])

  const redirect = fullMatch && fullMatch[fullMatch.length - 1].route.redirect

  const renderWithContext = (
    value: RouteContextValue,
    component: ElementType,
  ) => {
    return React.createElement(
      routeContext.Provider,
      {value},
      React.createElement(component),
    )
  }

  const createRouteData = useCallback(
    (data: GlobalRouteData, index: number): RouteData => {
      const {fullMatch} = data
      const routeMatch = fullMatch[index]

      const url = (params: LocationParams = {}) => {
        const {path = '', ...other} = params
        const paths = typeof path === 'string' ? [path] : path
        const isAbsolute = paths[0].startsWith('/')
        const pathSegments: string[] = []
        for (const path of paths) {
          pathSegments.push(
            ...path.split('/').filter((segment) => segment !== ''),
          )
        }

        const newPath: string[] = isAbsolute
          ? []
          : fullMatch
              .slice(0, index + 1)
              .filter((routeMatch) => routeMatch.path !== '')
              .map((routeMatch) => routeMatch.path)

        for (const pathSegment of pathSegments) {
          if (pathSegment === '' || pathSegment === '.') {
            continue
          }
          if (pathSegment === '..') {
            if (newPath.length > 0) {
              newPath.pop()
              continue
            }
          }
          newPath.push(pathSegment)
        }
        const encodedPath = newPath.map(encodeURIComponent)
        const pathString = encodedPath.join('/')

        // we reject all existing search parameters if the path changes
        const currentRawSearch = path === '' ? data.rawSearch || {} : {}
        // we only consider those search parameters that are present in the raw
        // search parameters, i.e. ignore all default validated values
        const filteredSearch = Object.fromEntries(
          Object.entries(routeMatch.search || {}).filter(
            ([key]) => currentRawSearch[key],
          ),
        )

        const rawSearch = {...(other.rawSearch || currentRawSearch)}
        const search = other.search || filteredSearch || {}
        const searchUpdates = other.searchUpdates || {}

        Object.keys(searchUpdates).forEach((key) => {
          if (searchUpdates[key] === undefined) {
            delete search[key]
            delete rawSearch[key]
          } else {
            search[key] = searchUpdates[key] as string | boolean | number
          }
        })

        Object.keys(search).forEach((key) => {
          if (search[key] !== undefined) {
            rawSearch[key] = encodeURIComponent(
              search[key] as string | boolean | number,
            )
          }
        })

        const queryString = Object.keys(rawSearch)
          .filter((key) => rawSearch[key] !== undefined)
          .map(
            (key) =>
              `${encodeURIComponent(key)}=${encodeURIComponent(
                rawSearch[key],
              )}`,
          )
          .join('&')

        if (queryString === '') {
          return `/${pathString}`
        }

        return `/${pathString}?${queryString}`
      }

      const matches = (path: string) => {
        const pathMatch = matchRoute(path, fullMatch[0].route)
        if (!pathMatch) {
          return false
        }
        for (let i = 1; i < pathMatch.length; i++) {
          if (pathMatch[i].route !== fullMatch[i]?.route) {
            return false
          }
        }

        return true
      }

      return {
        ...data,
        index,
        match: fullMatch.slice(0, index + 1),
        ...routeMatch,
        url,
        navigate: (params: LocationParams = {}) => {
          navigate(url(params))
        },
        reload,
        reloadCount,
        relativePath: (path: string, referencePath?: string) => {
          return relativePath(
            path,
            referencePath || data.location,
            fullMatch[0].route,
          )
        },
        matches,
      }
    },
    [navigate, reload, reloadCount],
  )

  const createRouteContextValue = useCallback(
    (data: GlobalRouteData, index: number, element?: ReactElement) => {
      return {
        ...data,
        index,
        createRouteData,
        element,
      } as RouteContextValue
    },
    [createRouteData],
  )

  const renderRootError = useCallback(
    (error: Error) => {
      // eslint-disable-next-line no-console
      console.error(error)
      if (route.errorComponent) {
        const fullMatch = [
          {
            route,
            path: matchData.location,
            params: {},
            isLeaf: true,
            search: {},
          },
        ]
        const contextValue = createRouteContextValue(
          {
            ...matchData,
            fullMatch,
            error,
            isLoading,
          },
          0,
        )

        return renderWithContext(contextValue, route.errorComponent)
      }
      return <div>{error.message}</div>
    },
    [isLoading, matchData, route, createRouteContextValue],
  )

  const renderRouteSegment = useCallback(
    (index: number, element: ReactElement) => {
      const contextData = {
        ...matchData,
        isLoading,
      }
      const route = matchData.fullMatch[index].route
      const {component, errorComponent} = route
      if (route.onlyRender) {
        const onlyRender = route.onlyRender as Route[]
        const remainingPath = matchData.fullMatch
          .slice(index + 1)
          .map((match) => match.path)
          .join('/')
        const matchesOnlyRender = onlyRender.some((route) =>
          matchRoute(remainingPath, route),
        )
        if (!matchesOnlyRender) {
          return element
        }
      }
      if (component) {
        element = renderWithContext(
          createRouteContextValue(contextData, index, element),
          component,
        )
      }
      if (errorComponent) {
        const fallbackRenderer = ({error}: {error: Error}) => {
          return renderWithContext(
            createRouteContextValue({...contextData, error}, index, <></>),
            errorComponent,
          )
        }
        element = (
          <ErrorBoundary fallbackRender={fallbackRenderer}>
            {element}
          </ErrorBoundary>
        )
      }
      return element
    },
    [createRouteContextValue, isLoading, matchData],
  )

  useEffect(() => {
    if (redirect) {
      const globalRouteData = {...matchData, isLoading}
      const index = matchData.fullMatch.length - 1
      navigate(redirect(createRouteData(globalRouteData, index)))
    } else if (!isLoading && onNavigate) {
      onNavigate({...matchData})
    }
  }, [matchData, navigate, redirect, isLoading, createRouteData, onNavigate])

  if (!fullMatch) {
    return renderRootError(
      new RouteError(`There is no matching route for ${location.path}`),
    )
  }

  if (!fullMatch[fullMatch.length - 1].route.component && !isLoading) {
    return renderRootError(
      new RouteError(
        `The matching route for ${location.path} does not provide a component to render.`,
      ),
    )
  }

  if (isLoading || redirect) {
    // Only render the root route segment using the old render results
    // for children
    return renderRouteSegment(0, lastRenderResult.current)
  }

  if (routeData.error) {
    return renderRootError(routeData.error)
  }

  // Rerender whole route
  let element = <></>
  for (let index = fullMatch.length - 1; index > 0; index--) {
    element = renderRouteSegment(index, element)
  }
  lastRenderResult.current = element
  return renderRouteSegment(0, element)
}
