import {render, screen} from '@testing-library/react'
import {useMemo} from 'react'
import {expect, it, vi} from 'vitest'

import Outlet from './Outlet'
import Router from './Router'
import Routes, {RoutesProps} from './Routes'
import {Route} from './types'
import useRouteError from './useRouteError'

export function ErrorComponent() {
  const error = useRouteError()
  return (
    <div>
      <div>{error.message}</div>
      <div>Error</div>
    </div>
  )
}

describe('Routes', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  const countRender = vi.fn()

  const createRoute = () =>
    ({
      path: '',
      component: () => {
        countRender()
        return <Outlet />
      },
      errorComponent: ErrorComponent,
      children: [
        {
          path: '',
          component: () => <div>Index</div>,
        },
        {
          path: 'wildcard',
          children: [
            {
              path: '*',
              component: () => <div>Wildcard</div>,
            },
          ],
        },
        {
          path: 'about',
          component: () => <div>About</div>,
        },
        {
          path: 'noComponent',
        },
        {
          path: 'parent',
          component: () => (
            <div>
              <div>Parent</div>
              <Outlet />
            </div>
          ),
          children: [
            {
              path: 'child',
              component: () => <div>Child</div>,
            },
          ],
        },
        {
          path: 'lazy',
          lazyComponent: () =>
            Promise.resolve({default: () => <div>Lazy</div>}),
        },
        {
          path: 'onlyRender',
          component: () => <div>Only Render</div>,
          onlyRender: ['', 'match/*'],
          children: [
            {
              path: '*',
              component: () => <div>Child</div>,
            },
          ],
        },
      ],
    } as Route)

  const App = ({
    path,
    ...routesProps
  }: {path: string} & Partial<RoutesProps>) => {
    const route = useMemo(() => createRoute(), [])
    return (
      <Routes
        {...routesProps}
        route={route}
        location={{path, rawSearch: {}}}
        navigate={() => {}}
      />
    )
  }

  it.each([
    ['/', 'Index', 0],
    ['/about', 'About', 0],
    ['/wildcard/some/thing', 'Wildcard', 0],
    ['/parent', 'Parent', 0],
    ['/parent/child', 'Child', 0],
    ['/parent/child', 'Parent', 0],
    ['/doesNotExist', [/no matching route/, 'Error'], 1],
    ['/noComponent', [/does not provide a component to render/, 'Error'], 1],
    ['/onlyRender', 'Only Render', 0],
    ['/onlyRender/match/this', 'Only Render', 0],
    ['/onlyRender/not/this', 'Child', 0],
  ])(
    'should render %s',
    (path, texts: (string | RegExp) | (string | RegExp)[], consoleErrors) => {
      if (!Array.isArray(texts)) {
        texts = [texts]
      }
      global.console.error = vi.fn()
      const onNavigate = vi.fn()
      render(<App path={path} onNavigate={onNavigate} />)
      texts.forEach((text) => {
        expect(screen.getByText(text)).toBeInTheDocument()
      })
      expect(global.console.error).toBeCalledTimes(consoleErrors)
      expect(countRender).toBeCalledTimes(consoleErrors === 0 ? 1 : 0)
      expect(onNavigate).toBeCalledTimes(1)
    },
  )

  it('should show errors even without errorComponent', () => {
    global.console.error = vi.fn()
    const route = {
      path: '',
      component: Outlet,
    }
    render(
      <Routes
        route={route}
        location={{path: '/doesNotExist', rawSearch: {}}}
        navigate={() => {}}
      />,
    )
    expect(screen.getByText(/no matching route/)).toBeInTheDocument()
    expect(global.console.error).toBeCalled()
  })

  it('should render error boundary on render errors', () => {
    global.console.error = vi.fn()
    const route = {
      path: '',
      children: [
        {
          path: '*',
          component: () => {
            throw new Error('test error')
          },
        },
      ],
      errorComponent: ErrorComponent,
    }
    render(<Router route={route} />)
    expect(screen.getByText('Error')).toBeInTheDocument()
    expect(screen.getByText('test error')).toBeInTheDocument()
    expect(global.console.error).toBeCalled()
  })

  it('should load lazy components', async () => {
    const {rerender} = render(<App path='/about' />)
    expect(await screen.findByText('About')).toBeInTheDocument()
    expect(countRender).toBeCalledTimes(1)

    rerender(<App path='/lazy' />)
    expect(await screen.findByText('Lazy')).toBeInTheDocument()
    expect(countRender).toBeCalledTimes(3)
  })

  it('should load lazy components initially', async () => {
    render(<App path='/lazy' />)
    expect(await screen.findByText('Lazy')).toBeInTheDocument()
    expect(countRender).toBeCalledTimes(2)
  })

  it('should load lazy components only once', async () => {
    const {rerender} = render(<App path='/lazy' />)
    expect(await screen.findByText('Lazy')).toBeInTheDocument()
    expect(countRender).toBeCalledTimes(2)

    rerender(<App path='/about' />)
    expect(await screen.findByText('About')).toBeInTheDocument()
    expect(countRender).toBeCalledTimes(3)

    rerender(<App path='/lazy' />)
    expect(await screen.findByText('Lazy')).toBeInTheDocument()
    expect(countRender).toBeCalledTimes(4)
  })
})
