import {expect, it, vi} from 'vitest'

import normalizeRoute, {routesEqual} from './normalizeRoute'
import {Route} from './types'

describe('normalizeRoute', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })
  it('normalizes normalized route', () => {
    const component = () => <div>Hello</div>
    const route = {
      path: 'one',
      component,
    }
    const normalizedRoute = normalizeRoute(route)
    expect(normalizedRoute).toMatchObject(route)
    expect(routesEqual(normalizedRoute, {...route} as Route)).toBe(true)
  })
  it('normalizes single route', () => {
    const component = () => <div>Hello</div>
    const route = {
      path: 'one/two/three',
      component,
    }
    const normalizedRoute = normalizeRoute(route)
    expect(normalizedRoute).toMatchObject({
      path: 'one',
      children: [
        {
          path: 'two',
          children: [
            {
              path: 'three',
              component,
            },
          ],
        },
      ],
    })
    expect(routesEqual(normalizedRoute, {...route} as Route)).toBe(true)
  })
  it('normalizes with child routes', () => {
    const component = () => <div>Hello</div>
    const route = {
      path: 'one/two',
      children: [
        {
          path: 'three/four',
          component,
        },
      ],
    }
    const normalizedRoute = normalizeRoute(route)
    expect(normalizedRoute).toMatchObject({
      path: 'one',
      children: [
        {
          path: 'two',
          children: [
            {
              path: 'three',
              children: [
                {
                  path: 'four',
                  component,
                },
              ],
            },
          ],
        },
      ],
    })
    expect(routesEqual(normalizedRoute, {...route} as Route)).toBe(true)
  })

  it('normalizes recursive route', () => {
    const route = {
      path: 'even',
      children: [
        {
          path: 'odd',
        },
      ],
    } as Route

    const childRoute = route?.children?.[0] as Route
    childRoute.children = [route]
    const normalizedRoute = normalizeRoute(route)
    expect(normalizedRoute.children?.[0].children?.[0]).toMatchObject(
      normalizedRoute,
    )
    expect(routesEqual(normalizedRoute, {...route} as Route)).toBe(true)
  })

  it.each([
    ['', [{path: ''}]],
    [
      ['', 'math/this', {path: 'or/this'}],
      [
        {path: ''},
        {path: 'math', children: [{path: 'this'}]},
        {path: 'or', children: [{path: 'this'}]},
      ],
    ],
  ])('normalizes routes with onlyRender=%s', (onlyRender, ref) => {
    const route: Route = {
      path: 'path',
      onlyRender,
    }
    const normalizedRoute = normalizeRoute(route)
    expect(normalizedRoute).toMatchObject({
      path: 'path',
      onlyRender: ref,
    })
    expect(routesEqual(normalizedRoute, {...route} as Route)).toBe(true)
  })
})
