import {Box, useMediaQuery} from '@mui/material'
import React, {PropsWithChildren, useContext, useRef} from 'react'
import {useHoverDirty} from 'react-use'

import {SxProps} from '../utils/types'

export type VisibleOnHoverProps = {
  /**
   * If true, the children are treated as the container that is used to
   * capture the hover state. If false, the children are treated as the
   * visible or not visible content depending on the nearest container.
   * Instances without a container will always be visible.
   */
  container?: boolean
  /**
   * Allows to disable the functionality. If true, the children are always
   * shown
   */
  disabled?: boolean
} & PropsWithChildren &
  SxProps

const context = React.createContext<boolean | undefined>(undefined)

/**
 * A utility component that allows to hide and show content based on
 * the hover state of a parent container.
 */
export default function VisibleOnHover({
  disabled = false,
  container = false,
  children,
  sx,
}: VisibleOnHoverProps) {
  const isHovering = useContext(context)
  const containerRef = useRef<HTMLDivElement>(null)
  const containerIsHovering = useHoverDirty(containerRef)
  const canHover = useMediaQuery('(hover: hover)')

  if (container) {
    return (
      <context.Provider value={containerIsHovering}>
        <Box sx={{width: '100%', ...sx}} ref={containerRef}>
          {children}
        </Box>
      </context.Provider>
    )
  } else {
    return (
      <Box
        sx={{
          ...(!disabled &&
            isHovering === false &&
            canHover && {visibility: 'hidden'}),
          ...sx,
          marginTop: -1,
        }}
      >
        {children}
      </Box>
    )
  }
}
