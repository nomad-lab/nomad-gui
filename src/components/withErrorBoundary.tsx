import ErrorBoundary, {ErrorBoundaryProps} from './ErrorBoundary'

export default function withErrorBoundary<Props>(
  Component: React.ComponentType<Props>,
  errorBoundaryProps?: ErrorBoundaryProps,
) {
  return function ErrorBoundaryComponent(
    props: JSX.IntrinsicAttributes & Props,
  ) {
    return (
      <ErrorBoundary {...errorBoundaryProps}>
        <Component {...props} />
      </ErrorBoundary>
    )
  }
}
