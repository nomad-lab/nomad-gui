/*
 * Copyright The NOMAD Authors.
 *
 * This file is part of NOMAD. See https://nomad-lab.eu for further info.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Box, Button, Dialog, DialogActions, DialogContent} from '@mui/material'
import {isNil} from 'lodash'
import PropTypes from 'prop-types'
import {useEffect, useMemo, useRef} from 'react'
import {useResizeDetector} from 'react-resize-detector'

import {useWindowSize} from '../../hooks'

/**
 * Component that wraps it's children in a container that can be 'floated',
 * i.e. displayed on an html element that is positioned relative to the
 * viewport and is above all other elements.
 */
const actionsHeight = 52.5
const padding = 20
export default function Floatable({
  className,
  float = false,
  children,
  onFloat,
  onResize,
}) {
  // The ratio is calculated dynamically from the final size
  const {height, width, ref} = useResizeDetector()
  const size = useWindowSize()
  const dim = useRef({width: 800, height: 600})

  // When the figure is floated, fix the width/height.
  useEffect(() => {
    if (!float) {
      dim.current = {
        width: width,
        height: height,
      }
    }
    if (onResize && !isNil(width) && !isNil(height)) {
      onResize(width, height)
    }
  }, [float, width, height, onResize])

  // Dialog style that is affected by the current window size
  const dialogSx = useMemo(() => {
    const maxWidth = 1280 // Maximum width for the floating window
    const margin = 0.1 * size.windowHeight + actionsHeight
    const windowHeight = size.windowHeight - margin
    const windowWidth = Math.min(size.windowWidth, maxWidth) - margin
    const windowRatio = windowWidth / windowHeight
    const ratio = dim.current.width / dim.current.height
    let width
    let height
    if (windowRatio > ratio) {
      width = windowHeight * ratio + 2 * padding + 'px'
      height = windowHeight + actionsHeight + 2 * padding
    } else {
      width = windowWidth + 2 * padding
      height = windowWidth / ratio + actionsHeight + 2 * padding + 'px'
    }
    return {
      width: width,
      height: height,
      margin: 0,
      padding: 0,
      maxWidth: 'none',
      maxHeight: 'none',
      boxSizing: 'border-box',
    }
  }, [size])

  return (
    <Box ref={ref} sx={{width: '100%', height: '100%'}} className={className}>
      {float ? (
        <Box sx={{width: dim.current.width, height: dim.current.height}} />
      ) : (
        children
      )}
      <Dialog
        fullWidth={false}
        maxWidth={false}
        open={float}
        PaperProps={{sx: dialogSx}}
      >
        <DialogContent>{float ? children : null}</DialogContent>
        <DialogActions
          sx={{
            height: actionsHeight,
            boxSizing: 'border-box',
          }}
        >
          <Button onClick={() => onFloat(float)}>Close</Button>
        </DialogActions>
      </Dialog>
    </Box>
  )
}

Floatable.propTypes = {
  /**
   * Whether this component should be in floating mode or not.
   */
  float: PropTypes.bool.isRequired,
  /**
   * Callback that is called whenever this component requests a change in the
   * float property. The callback accepts one parameter: 'float' that is a
   * boolean indicating the current float status.
   */
  onFloat: PropTypes.func,
  onResize: PropTypes.func,
  children: PropTypes.node,
  className: PropTypes.string,
}
