/*
 * Copyright The NOMAD Authors.
 *
 * This file is part of NOMAD. See https://nomad-lab.eu for further info.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  Box,
  Checkbox,
  FormControl,
  FormControlLabel,
  IconButton,
  MenuItem,
  Select,
  Tooltip,
} from '@mui/material'
import {isArray} from 'lodash'
import PropTypes from 'prop-types'
import React from 'react'

import {useBoolState} from '../hooks'

export const Actions = React.memo(
  ({justifyContent = 'flex-end', flexDirection, className, children}) => {
    return (
      <Box
        className={className}
        sx={{
          display: 'flex',
          width: '100%',
          boxSizing: 'border-box',
          alignItems: 'center',
          justifyContent: justifyContent || 'flex-end',
          flexDirection: flexDirection || 'row',
        }}
      >
        {children}
      </Box>
    )
  },
)

Actions.propTypes = {
  justifyContent: PropTypes.string, // The flexbox justification of buttons
  flexDirection: PropTypes.string, // The flexbox justification of buttons
  className: PropTypes.string,
  classes: PropTypes.object,
  children: PropTypes.node,
}

export const ActionHeader = React.memo(
  ({disableSpacer, className, children}) => {
    return (
      <Box
        className={className}
        sx={{
          flexGrow: 1,
          height: '100%',
          display: 'flex',
          alignItems: 'center',
          minWidth: 0,
          marginRight: (theme) => theme.spacing(0.5),
        }}
      >
        {children}
        {!disableSpacer && <Box sx={{flexGrow: 1, minWidth: 0}} />}
      </Box>
    )
  },
)

ActionHeader.propTypes = {
  disableSpacer: PropTypes.bool, // Used to disable flexbox spacer
  className: PropTypes.string,
  classes: PropTypes.object,
  children: PropTypes.node,
}

export const Action = React.memo(
  ({
    color,
    size = 'small',
    href,
    disabled,
    onClick,
    onMouseDown,
    onMouseUp,
    tooltip,
    TooltipProps,
    className,
    children,
    disableButton,
    ButtonComponent = IconButton,
    ButtonProps,
    'data-testid': testID,
  }) => {
    return (
      <Tooltip title={tooltip} {...TooltipProps}>
        <Box
          component='span'
          className={className}
          sx={{
            marginRight: (theme) => theme.spacing(1),
            '&:last-child': {
              marginRight: 0,
            },
          }}
        >
          {disableButton ? (
            children
          ) : (
            <ButtonComponent
              {...ButtonProps}
              color={color}
              size={size}
              onClick={onClick}
              onMouseDown={onMouseDown}
              onMouseUp={onMouseUp}
              disabled={disabled}
              href={href}
              aria-label={tooltip}
              data-testid={testID}
            >
              {children}
            </ButtonComponent>
          )}
        </Box>
      </Tooltip>
    )
  },
)

Action.propTypes = {
  variant: PropTypes.string, // The variant of the MUI buttons
  color: PropTypes.string, // The color of the MUI buttons
  size: PropTypes.string, // Size of the MUI buttons
  href: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  onMouseDown: PropTypes.func,
  onMouseUp: PropTypes.func,
  tooltip: PropTypes.string,
  TooltipProps: PropTypes.object,
  ButtonComponent: PropTypes.elementType,
  ButtonProps: PropTypes.object,
  className: PropTypes.string,
  classes: PropTypes.object,
  children: PropTypes.node,
  'data-testid': PropTypes.string,
}

/**
 * Dropdown menu action.
 */
export const ActionSelect = React.memo(
  ({value, options, tooltip, onChange}) => {
    const [isStatsTooltipOpen, openStatsTooltip, closeStatsTooltip] =
      useBoolState(false)
    const items = isArray(options)
      ? Object.fromEntries(options.map((x) => [x, x]))
      : options
    return (
      <Action
        disableButton
        TooltipProps={{
          title: tooltip || '',
          open: isStatsTooltipOpen,
          disableHoverListener: true,
        }}
      >
        <FormControl size='small' variant='standard'>
          <Select
            value={value}
            onMouseEnter={openStatsTooltip}
            onMouseLeave={closeStatsTooltip}
            onOpen={closeStatsTooltip}
            onChange={(event) => onChange && onChange(event.target.value)}
          >
            {Object.entries(items).map(([key, value]) => (
              <MenuItem key={key} value={key}>
                {value}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Action>
    )
  },
)

ActionSelect.propTypes = {
  value: PropTypes.string,
  options: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.arrayOf(PropTypes.string),
  ]),
  tooltip: PropTypes.string,
  onChange: PropTypes.func,
}

/**
 * Checkbox action.
 */
export const ActionCheckbox = React.memo(
  ({value, label, tooltip, onChange}) => {
    return (
      <Tooltip title={tooltip}>
        <FormControlLabel
          control={
            <Checkbox
              checked={value}
              onChange={(event, value) => onChange && onChange(value)}
              size='small'
            />
          }
          label={label}
          sx={{
            marginRight: (theme) => theme.spacing(1),
            '&:last-child': {
              marginRight: 0,
            },
          }}
        />
      </Tooltip>
    )
  },
)

ActionCheckbox.propTypes = {
  value: PropTypes.bool,
  label: PropTypes.string,
  tooltip: PropTypes.string,
  onChange: PropTypes.func,
}
