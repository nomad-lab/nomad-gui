/*
 * Copyright The NOMAD Authors.
 *
 * This file is part of NOMAD. See https://nomad-lab.eu for further info.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Box, Typography} from '@mui/material'
import clsx from 'clsx'
import PropTypes from 'prop-types'
import React from 'react'

import PlotTick from './PlotTick'

/**
 * Label used in plots.
 */

const PlotLabel = React.memo(
  ({
    label,
    size = 6,
    labelPadding = 3,
    tickLength,
    orientation = 'horizontal',
    className,
    classes,
    'data-testid': testID,
  }) => {
    return (
      <Box
        className={clsx(className)}
        data-testid={testID}
        sx={{
          display: 'flex',
          alignItems: 'center',
          ...(orientation === 'horizontal' && {
            justifyContent: 'flex-start',
            flexDirection: 'column-reverse',
            transform: 'translateX(-50%)',
          }),
          ...(orientation === 'vertical' && {
            flexDirection: 'row',
            justifyContent: 'flex-end',
            transform: 'translateY(50%)',
          }),
        }}
      >
        <Typography
          sx={{
            lineHeight: 1,
            flexShrink: 0,
            flexGrow: 0,
            fontSize: size,
          }}
          noWrap
        >
          {label}
        </Typography>
        <Box sx={{flex: `0 0 ${labelPadding}`}} />
        <PlotTick
          sx={{
            flexShrink: 0,
            flexGrow: 0,
            lineHeight: 1,
          }}
          length={tickLength}
          orientation={orientation}
        />
      </Box>
    )
  },
)

PlotLabel.propTypes = {
  label: PropTypes.string,
  size: PropTypes.number,
  labelPadding: PropTypes.number,
  tickLength: PropTypes.number,
  orientation: PropTypes.oneOf(['horizontal', 'vertical']),
  className: PropTypes.string,
  classes: PropTypes.object,
  'data-testid': PropTypes.string,
}

export default PlotLabel
