/*
 * Copyright The NOMAD Authors.
 *
 * This file is part of NOMAD. See https://nomad-lab.eu for further info.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Box, Tooltip} from '@mui/material/'
import {makeStyles, useTheme} from '@mui/styles'
import clsx from 'clsx'
import {clamp} from 'lodash'
import PropTypes from 'prop-types'
import React, {useCallback, useMemo} from 'react'

/**
 * Bar within a plot.
 */
const PlotBar = React.memo(
  ({
    startX,
    endX,
    startY,
    endY,
    selected,
    tooltip,
    onClick,
    'data-testid': testID,
  }) => {
    const theme = useTheme()

    const handleClick = useCallback(
      (event) => {
        onClick?.(event)
      },
      [onClick],
    )

    const highlightStyle = useMemo(() => {
      if (selected === false) {
        return {visibility: 'hidden'}
      }
      if (selected === true) {
        return {
          left: '0%',
          right: '0%',
        }
      }
      return {
        left: `${clamp(selected[0] * 100, 0, 100)}%`,
        right: `${clamp((1 - selected[1]) * 100, 0, 100)}%`,
      }
    }, [selected])

    return (
      <Box
        onClick={handleClick}
        sx={{
          position: 'absolute',
          cursor: 'pointer',
          height: '100%',
          paddingLeft: '1px',
          paddingRight: '1px',
          left: `${startX * 100}%`,
          right: `${(1 - endX) * 100}%`,
        }}
        data-testid={testID}
      >
        <Tooltip
          placement='bottom'
          enterDelay={0}
          leaveDelay={0}
          title={tooltip || ''}
          arrow
        >
          <Box
            sx={{
              position: 'relative',
              width: '100%',
              height: '100%',
            }}
          >
            <Box
              sx={{
                width: '100%',
                height: '100%',
                transition: 'transform 250ms',
                transformOrigin: 'bottom left',
                willChange: 'transform',
                backgroundColor:
                  theme.palette.primary[
                    theme.palette.mode === 'dark' ? 'dark' : 'light'
                  ],
                position: 'relative',
                transform: `scaleY(${endY - startY})`,
              }}
            >
              <Box
                sx={{
                  position: 'absolute',
                  top: 0,
                  bottom: 0,
                  backgroundColor: theme.palette.secondary.light,
                  ...highlightStyle,
                }}
              />
            </Box>
          </Box>
        </Tooltip>
      </Box>
    )
  },
)

PlotBar.propTypes = {
  startX: PropTypes.number,
  endX: PropTypes.number,
  startY: PropTypes.number,
  endY: PropTypes.number,
  /* The selection as a boolean, or as a range. The range is given as two
   * percentages indicate the start and end of the selection. */
  selected: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.arrayOf(PropTypes.number),
  ]),
  tooltip: PropTypes.string,
  onClick: PropTypes.func,
  'data-testid': PropTypes.string,
}

export default PlotBar
