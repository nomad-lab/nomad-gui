/*
 * Copyright The NOMAD Authors.
 *
 * This file is part of NOMAD. See https://nomad-lab.eu for further info.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Box, Tooltip, Typography} from '@mui/material'
import PropTypes from 'prop-types'
import React from 'react'

import Ellipsis from './visualization/Ellipsis'

/**
 * Simple component for displaying titles and corresponding tooltips for
 * metainfo definitions.
 */
export const DefinitionTitle = React.memo(
  ({
    label,
    description,
    variant = 'body2',
    TooltipProps,
    onMouseDown,
    onMouseUp,
    sx,
    rotation = 'right',
    noWrap = true,
  }) => {
    const rootSx = {
      ...(rotation === 'right' && {}),
      ...(rotation === 'down' && {
        writingMode: 'vertical-rl',
        textOrientation: 'mixed',
      }),
      ...(rotation === 'up' && {
        writingMode: 'vertical-rl',
        textOrientation: 'mixed',
        transform: 'rotate(-180deg)',
      }),
      ...(sx || {}),
    }

    return (
      <Tooltip
        title={description || ''}
        enterDelay={400}
        enterNextDelay={400}
        {...(TooltipProps || {})}
      >
        <Box sx={rootSx}>
          <Typography
            noWrap={noWrap}
            variant={variant}
            onMouseDown={onMouseDown}
            onMouseUp={onMouseUp}
          >
            <Ellipsis>{label}</Ellipsis>
          </Typography>
        </Box>
      </Tooltip>
    )
  },
)

DefinitionTitle.propTypes = {
  quantity: PropTypes.string,
  label: PropTypes.string,
  description: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  variant: PropTypes.string,
  className: PropTypes.string,
  classes: PropTypes.object,
  rotation: PropTypes.oneOf(['up', 'right', 'down']),
  TooltipProps: PropTypes.object, // Properties forwarded to the Tooltip
  onMouseDown: PropTypes.func,
  onMouseUp: PropTypes.func,
  placement: PropTypes.string,
  noWrap: PropTypes.bool,
  section: PropTypes.string,
}
