export const searchQuantities = {
  upload_id: {
    name: 'upload_id',
    description:
      'The persistent and globally unique identifier for the upload of the entry',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  upload_name: {
    name: 'upload_name',
    description: 'The user provided upload name',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  upload_create_time: {
    name: 'upload_create_time',
    description: 'The date and time when the upload was created in nomad',
    type: {
      type_kind: 'custom',
      type_data: 'nomad.metainfo.data_type.Datetime',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  entry_id: {
    name: 'entry_id',
    description: 'A persistent and globally unique identifier for the entry',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  entry_name: {
    name: 'entry_name',
    description: 'A brief human readable name for the entry.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'entry_name.prefix': {
    name: 'entry_name',
    description: 'A brief human readable name for the entry.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  entry_type: {
    name: 'entry_type',
    description:
      'The main schema definition. This is the name of the section used for data.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  calc_id: {
    name: 'calc_id',
    description: 'Legacy field name, use `entry_id` instead.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  entry_create_time: {
    name: 'entry_create_time',
    description: 'The date and time when the entry was created in nomad',
    type: {
      type_kind: 'custom',
      type_data: 'nomad.metainfo.data_type.Datetime',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  parser_name: {
    name: 'parser_name',
    description: 'The NOMAD parser used for the last processing',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  mainfile: {
    name: 'mainfile',
    description:
      'The path to the mainfile from the root directory of the uploaded files',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'mainfile.path': {
    name: 'mainfile',
    description:
      'The path to the mainfile from the root directory of the uploaded files',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  mainfile_key: {
    name: 'mainfile_key',
    description:
      'Key used to differentiate between different *child entries* of an entry.\nFor parent entries and entries that do not have any children, the value should\nbe empty.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'mainfile_key.path': {
    name: 'mainfile_key',
    description:
      'Key used to differentiate between different *child entries* of an entry.\nFor parent entries and entries that do not have any children, the value should\nbe empty.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  text_search_contents: {
    name: 'text_search_contents',
    description:
      'Contains text contents that should be considered when\ndoing free text queries for entries.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  files: {
    name: 'files',
    description:
      "The paths to the files within the upload that belong to this entry.\nAll files within the same directory as the entry's mainfile are considered the\nauxiliary files that belong to the entry.",
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'files.path': {
    name: 'files',
    description:
      "The paths to the files within the upload that belong to this entry.\nAll files within the same directory as the entry's mainfile are considered the\nauxiliary files that belong to the entry.",
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  pid: {
    name: 'pid',
    description:
      'The unique, sequentially enumerated, integer PID that was used in the legacy\nNOMAD CoE. It allows to resolve URLs of the old NOMAD CoE Repository.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  raw_id: {
    name: 'raw_id',
    description:
      "The code specific identifier extracted from the entry's raw files by the parser,\nif supported.",
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  external_id: {
    name: 'external_id',
    description:
      'A user provided external id. Usually the id for an entry in an external database\nwhere the data was imported from.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  published: {
    name: 'published',
    description: 'Indicates if the entry is published',
    type: {
      type_kind: 'python',
      type_data: 'bool',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  publish_time: {
    name: 'publish_time',
    description: 'The date and time when the upload was published in nomad',
    type: {
      type_kind: 'custom',
      type_data: 'nomad.metainfo.data_type.Datetime',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  with_embargo: {
    name: 'with_embargo',
    description: 'Indicated if this entry is under an embargo',
    type: {
      type_kind: 'python',
      type_data: 'bool',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  processed: {
    name: 'processed',
    description: 'Indicates that the entry is successfully processed.',
    type: {
      type_kind: 'python',
      type_data: 'bool',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  last_processing_time: {
    name: 'last_processing_time',
    description: 'The date and time of the last processing.',
    type: {
      type_kind: 'custom',
      type_data: 'nomad.metainfo.data_type.Datetime',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  processing_errors: {
    name: 'processing_errors',
    description: 'Errors that occurred during processing',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  nomad_version: {
    name: 'nomad_version',
    description: 'The NOMAD version used for the last processing',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  nomad_commit: {
    name: 'nomad_commit',
    description: 'The NOMAD commit used for the last processing',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  comment: {
    name: 'comment',
    description: 'A user provided comment for this entry',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  references: {
    name: 'references',
    description: 'User provided references (URLs) for this entry',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  external_db: {
    name: 'external_db',
    description:
      'The repository or external database where the original data resides',
    type: {
      type_kind: 'enum',
      type_data: [
        'AFLOW',
        'EELS Data Base',
        'Kyoto Phonopy Database',
        'Materials Project',
        'OQMD',
        'The Perovskite Database Project',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  origin: {
    name: 'origin',
    description:
      'A short human readable description of the entries origin. Usually it is the\nhandle of an external database/repository or the name of the main author.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'main_author.name': {
    name: 'name',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'main_author.name.text': {
    name: 'name',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'main_author.user_id': {
    name: 'user_id',
    description: 'The unique, persistent keycloak UUID',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  main_author: {
    name: 'main_author',
    description: 'The main author of the entry',
    type: {
      type_kind: 'User',
      type_data: 'User',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'authors.name': {
    name: 'name',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'authors.name.text': {
    name: 'name',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  authors: {
    name: 'authors',
    description: 'All authors (main author and co-authors)',
    type: {
      type_kind: 'Author',
      type_data: 'Author',
    },
    shape: ['0..*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'writers.name': {
    name: 'name',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'writers.name.text': {
    name: 'name',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'writers.user_id': {
    name: 'user_id',
    description: 'The unique, persistent keycloak UUID',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  writers: {
    name: 'writers',
    description: 'All writers (main author, upload coauthors)',
    type: {
      type_kind: 'User',
      type_data: 'User',
    },
    shape: ['0..*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  writer_groups: {
    name: 'writer_groups',
    description: 'Groups with write access (= coauthor groups).',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'viewers.name': {
    name: 'name',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'viewers.name.text': {
    name: 'name',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'viewers.user_id': {
    name: 'user_id',
    description: 'The unique, persistent keycloak UUID',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  viewers: {
    name: 'viewers',
    description: 'All viewers (main author, upload coauthors, and reviewers)',
    type: {
      type_kind: 'User',
      type_data: 'User',
    },
    shape: ['0..*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  viewer_groups: {
    name: 'viewer_groups',
    description:
      'Groups with read access (= coauthor groups + reviewer groups).',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'datasets.dataset_id': {
    name: 'dataset_id',
    description:
      'The unique identifier for this dataset as a string. It should be\na randomly generated UUID, similar to other nomad ids.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'datasets.dataset_name': {
    name: 'dataset_name',
    description:
      'The human-readable name of the dataset as string. The dataset name must be\nunique for the user.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'datasets.doi': {
    name: 'doi',
    description:
      'The optional Document Object Identifier (DOI) associated with this dataset.\nNomad can register DOIs that link back to the respective representation of\nthe dataset in the nomad UI. This quantity holds the string representation of\nthis DOI. There is only one per dataset. The DOI is just the DOI name, not its\nfull URL, e.g. "10.17172/nomad/2019.10.29-1".',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'datasets.dataset_create_time': {
    name: 'dataset_create_time',
    description: 'The date when the dataset was first created.',
    type: {
      type_kind: 'custom',
      type_data: 'nomad.metainfo.data_type.Datetime',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'datasets.dataset_modified_time': {
    name: 'dataset_modified_time',
    description:
      'The date when the dataset was last modified. An owned dataset\ncan only be extended after a DOI was assigned. A foreign dataset cannot be changed\nonce a DOI was assigned.',
    type: {
      type_kind: 'custom',
      type_data: 'nomad.metainfo.data_type.Datetime',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'datasets.dataset_type': {
    name: 'dataset_type',
    description:
      'The type determined if a dataset is owned, i.e. was created by\nthe authors of the contained entries; or if a dataset is foreign,\ni.e. it was created by someone not necessarily related to the entries.',
    type: {
      type_kind: 'enum',
      type_data: ['foreign', 'owned'],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  datasets: {
    name: 'datasets',
    description: 'A list of user curated datasets this entry belongs to.',
    type: {
      type_kind: 'reference',
      type_data: '/packages/16/section_definitions/0',
    },
    shape: ['0..*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  domain: {
    name: 'domain',
    description: 'The material science domain',
    type: {
      type_kind: 'enum',
      type_data: ['dft', 'ems', 'nexus'],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  n_quantities: {
    name: 'n_quantities',
    description: 'Number of metainfo quantities parsed from the entry.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  quantities: {
    name: 'quantities',
    description: 'All quantities that are used by this entry.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'quantities.path': {
    name: 'quantities',
    description: 'All quantities that are used by this entry.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  sections: {
    name: 'sections',
    description:
      'All sections that are present in this entry. This field is deprecated and will be removed.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'optimade.elements': {
    name: 'elements',
    description: 'Names of the different elements present in the structure.',
    type: {
      type_kind: 'enum',
      type_data: [
        'Ac',
        'Ag',
        'Al',
        'Am',
        'Ar',
        'As',
        'At',
        'Au',
        'B',
        'Ba',
        'Be',
        'Bh',
        'Bi',
        'Bk',
        'Br',
        'C',
        'Ca',
        'Cd',
        'Ce',
        'Cf',
        'Cl',
        'Cm',
        'Cn',
        'Co',
        'Cr',
        'Cs',
        'Cu',
        'Db',
        'Ds',
        'Dy',
        'Er',
        'Es',
        'Eu',
        'F',
        'Fe',
        'Fl',
        'Fm',
        'Fr',
        'Ga',
        'Gd',
        'Ge',
        'H',
        'He',
        'Hf',
        'Hg',
        'Ho',
        'Hs',
        'I',
        'In',
        'Ir',
        'K',
        'Kr',
        'La',
        'Li',
        'Lr',
        'Lu',
        'Lv',
        'Mc',
        'Md',
        'Mg',
        'Mn',
        'Mo',
        'Mt',
        'N',
        'Na',
        'Nb',
        'Nd',
        'Ne',
        'Nh',
        'Ni',
        'No',
        'Np',
        'O',
        'Og',
        'Os',
        'P',
        'Pa',
        'Pb',
        'Pd',
        'Pm',
        'Po',
        'Pr',
        'Pt',
        'Pu',
        'Ra',
        'Rb',
        'Re',
        'Rf',
        'Rg',
        'Rh',
        'Rn',
        'Ru',
        'S',
        'Sb',
        'Sc',
        'Se',
        'Sg',
        'Si',
        'Sm',
        'Sn',
        'Sr',
        'Ta',
        'Tb',
        'Tc',
        'Te',
        'Th',
        'Ti',
        'Tl',
        'Tm',
        'Ts',
        'U',
        'V',
        'W',
        'X',
        'Xe',
        'Y',
        'Yb',
        'Zn',
        'Zr',
      ],
    },
    shape: ['1..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'optimade.nelements': {
    name: 'nelements',
    description: 'Number of different elements in the structure as an integer.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'optimade.elements_ratios': {
    name: 'elements_ratios',
    description: 'Relative proportions of different elements in the structure.',
    type: {
      type_kind: 'python',
      type_data: 'float',
    },
    shape: ['nelements'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'optimade.chemical_formula_descriptive': {
    name: 'chemical_formula_descriptive',
    description:
      'The chemical formula for a structure as a string in a form chosen by the API\nimplementation.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'optimade.chemical_formula_reduced': {
    name: 'chemical_formula_reduced',
    description:
      'The reduced chemical formula for a structure as a string with element symbols and\ninteger chemical proportion numbers. The proportion number MUST be omitted if it is 1.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'optimade.chemical_formula_hill': {
    name: 'chemical_formula_hill',
    description:
      'The chemical formula for a structure in Hill form with element symbols followed by\ninteger chemical proportion numbers. The proportion number MUST be omitted if it is 1.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'optimade.chemical_formula_anonymous': {
    name: 'chemical_formula_anonymous',
    description:
      'The anonymous formula is the chemical_formula_reduced, but where the elements are\ninstead first ordered by their chemical proportion number, and then, in order left to\nright, replaced by anonymous symbols A, B, C, ..., Z, Aa, Ba, ..., Za, Ab, Bb, ... and\nso on.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'optimade.nperiodic_dimensions': {
    name: 'nperiodic_dimensions',
    description:
      'An integer specifying the number of periodic dimensions in the structure, equivalent\nto the number of non-zero entries in dimension_types.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'optimade.nsites': {
    name: 'nsites',
    description:
      'An integer specifying the length of the cartesian_site_positions property.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'optimade.structure_features': {
    name: 'structure_features',
    description:
      'A list of strings that flag which special features are used by the structure.\n\n- disorder: This flag MUST be present if any one entry in the species list has a\nchemical_symbols list that is longer than 1 element.\n- unknown_positions: This flag MUST be present if at least one component of the\ncartesian_site_positions list of lists has value null.\n- assemblies: This flag MUST be present if the assemblies list is present.',
    type: {
      type_kind: 'enum',
      type_data: ['assemblies', 'disorder', 'unknown_positions'],
    },
    shape: ['1..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'section_defs.definition_qualified_name': {
    name: 'definition_qualified_name',
    description: 'The qualified name of the compatible section.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'section_defs.definition_id': {
    name: 'definition_id',
    description: 'The definition id of the compatible section.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'section_defs.used_directly': {
    name: 'used_directly',
    description: 'If the compatible section is directly used as base section.',
    type: {
      type_kind: 'python',
      type_data: 'bool',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'entry_references.target_reference': {
    name: 'target_reference',
    description: 'The full url like reference of the the target.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'entry_references.target_entry_id': {
    name: 'target_entry_id',
    description: 'The id of the entry containing the target.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'entry_references.target_mainfile': {
    name: 'target_mainfile',
    description: 'The mainfile of the entry containing the target.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'entry_references.target_upload_id': {
    name: 'target_upload_id',
    description: 'The id of the upload containing the target.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'entry_references.target_name': {
    name: 'target_name',
    description: 'The name of the target quantity/section.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'entry_references.target_path': {
    name: 'target_path',
    description: 'The path of the target quantity/section in its archive.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'entry_references.source_name': {
    name: 'source_name',
    description:
      'The name of the source (self) quantity/section in its archive.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'entry_references.source_path': {
    name: 'source_path',
    description:
      'The path of the source (self) quantity/section in its archive.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'entry_references.source_quantity': {
    name: 'source_quantity',
    description:
      'A reference to the quantity definition that defines the reference',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'search_quantities.id': {
    name: 'id',
    description:
      'The full identifier for this quantity that contains the path in the schema +\nschema name.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'search_quantities.definition': {
    name: 'definition',
    description: 'A reference to the quantity definition.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'search_quantities.path_archive': {
    name: 'path_archive',
    description: 'Path of the value within the archive.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'search_quantities.bool_value': {
    name: 'bool_value',
    description: 'The value mapped as an ES boolean field.',
    type: {
      type_kind: 'python',
      type_data: 'bool',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'search_quantities.str_value': {
    name: 'str_value',
    description: 'The value mapped as an ES text and keyword field.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'search_quantities.str_value.keyword': {
    name: 'str_value',
    description: 'The value mapped as an ES text and keyword field.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'search_quantities.int_value': {
    name: 'int_value',
    description: 'The value mapped as an ES long number field.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'search_quantities.float_value': {
    name: 'float_value',
    description: 'The value mapped as an ES double number field.',
    type: {
      type_kind: 'python',
      type_data: 'float',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'search_quantities.datetime_value': {
    name: 'datetime_value',
    description: 'The value mapped as an ES date field.',
    type: {
      type_kind: 'custom',
      type_data: 'nomad.metainfo.data_type.Datetime',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.material_id': {
    name: 'material_id',
    description:
      'A fixed length, unique material identifier in the form of a hash\ndigest.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.material.material_name': {
    name: 'material_name',
    description: 'Meaningful names for this a material if any can be assigned.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.structural_type': {
    name: 'structural_type',
    description: 'Structural class determined from the atomic structure.',
    type: {
      type_kind: 'enum',
      type_data: [
        '1D',
        '2D',
        'atom',
        'bulk',
        'molecule / cluster',
        'not processed',
        'surface',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.dimensionality': {
    name: 'dimensionality',
    description:
      "Dimensionality of the system. For atomistic systems this is\nautomatically evaluated by using the topology-scaling algorithm:\nhttps://doi.org/10.1103/PhysRevLett.118.106101.\n\n| Value | Description |\n| --------- | ----------------------- |\n| `'0D'` | Not connected periodically |\n| `'1D'` | Periodically connected in one dimension |\n| `'2D'` | Periodically connected in two dimensions |\n| `'3D'` | Periodically connected in three dimensions |",
    type: {
      type_kind: 'enum',
      type_data: ['0D', '1D', '2D', '3D'],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.building_block': {
    name: 'building_block',
    description:
      "More exact classification for this system, i.e. the type of \"building\nblock\" it represents.\n\n| Value | Description |\n| --------- | ----------------------- |\n| `'surface'` | Structure built from a unit cell that repeats periodically in two directions and at least twice, but not infinitely in a third direction. |\n| `'2D material'` | Structure built from a unit cell that repeats periodically in two directions and only once in a third direction. |\n| `'molecule'` | Molecule defined in the force-field topology |\n| `'monomer'` | Monomer defined in the force-field topology |",
    type: {
      type_kind: 'enum',
      type_data: ['2D material', 'molecule', 'monomer', 'surface'],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.functional_type': {
    name: 'functional_type',
    description: 'Classification based on the functional properties.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.compound_type': {
    name: 'compound_type',
    description: 'Classification based on the chemical formula.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.elements': {
    name: 'elements',
    description: 'Names of the different elements present in the structure.',
    type: {
      type_kind: 'enum',
      type_data: [
        'Ac',
        'Ag',
        'Al',
        'Am',
        'Ar',
        'As',
        'At',
        'Au',
        'B',
        'Ba',
        'Be',
        'Bh',
        'Bi',
        'Bk',
        'Br',
        'C',
        'Ca',
        'Cd',
        'Ce',
        'Cf',
        'Cl',
        'Cm',
        'Cn',
        'Co',
        'Cr',
        'Cs',
        'Cu',
        'Db',
        'Ds',
        'Dy',
        'Er',
        'Es',
        'Eu',
        'F',
        'Fe',
        'Fl',
        'Fm',
        'Fr',
        'Ga',
        'Gd',
        'Ge',
        'H',
        'He',
        'Hf',
        'Hg',
        'Ho',
        'Hs',
        'I',
        'In',
        'Ir',
        'K',
        'Kr',
        'La',
        'Li',
        'Lr',
        'Lu',
        'Lv',
        'Mc',
        'Md',
        'Mg',
        'Mn',
        'Mo',
        'Mt',
        'N',
        'Na',
        'Nb',
        'Nd',
        'Ne',
        'Nh',
        'Ni',
        'No',
        'Np',
        'O',
        'Og',
        'Os',
        'P',
        'Pa',
        'Pb',
        'Pd',
        'Pm',
        'Po',
        'Pr',
        'Pt',
        'Pu',
        'Ra',
        'Rb',
        'Re',
        'Rf',
        'Rg',
        'Rh',
        'Rn',
        'Ru',
        'S',
        'Sb',
        'Sc',
        'Se',
        'Sg',
        'Si',
        'Sm',
        'Sn',
        'Sr',
        'Ta',
        'Tb',
        'Tc',
        'Te',
        'Th',
        'Ti',
        'Tl',
        'Tm',
        'Ts',
        'U',
        'V',
        'W',
        'X',
        'Xe',
        'Y',
        'Yb',
        'Zn',
        'Zr',
      ],
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.n_elements': {
    name: 'n_elements',
    description: 'Number of different elements in the structure as an integer.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.material.elements_exclusive': {
    name: 'elements_exclusive',
    description:
      'String containing the chemical elements in alphabetical order and\nseparated by a single whitespace. This quantity can be used for\nexclusive element searches where you want to find entries/materials\nwith only certain given elements.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.material.chemical_formula_descriptive': {
    name: 'chemical_formula_descriptive',
    description:
      'The chemical formula for a structure as a string in a form chosen by the API\nimplementation.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.chemical_formula_reduced': {
    name: 'chemical_formula_reduced',
    description:
      'Alphabetically sorted chemical formula with reduced integer chemical\nproportion numbers. The proportion number is omitted if it is 1.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.chemical_formula_hill': {
    name: 'chemical_formula_hill',
    description:
      'The chemical formula for a structure in Hill form with element\nsymbols followed by non-reduced integer chemical proportion numbers.\nThe proportion number is omitted if it is 1.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.chemical_formula_iupac': {
    name: 'chemical_formula_iupac',
    description:
      'Formula where the elements are ordered using a formal list loosely\nbased on electronegativity as defined in the IUPAC nomenclature of\ninorganic chemistry (2005). Contains reduced integer chemical\nproportion numbers where the proportion number is omitted if it is\n1.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.chemical_formula_anonymous': {
    name: 'chemical_formula_anonymous',
    description:
      'Formula with the elements ordered by their reduced integer chemical\nproportion number, and the chemical species replaced by\nalphabetically ordered letters. The proportion number is omitted if\nit is 1. E.g.  H2O becomes A2B and H2O2 becomes AB. The letters are\ndrawn from the english alphabet that may be extended by increasing\nthe number of letters, e.g. A, B, ..., Z, Aa, Ab and so on. This\ndefinition is in line with the similarly named OPTIMADE definition.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.chemical_formula_reduced_fragments': {
    name: 'chemical_formula_reduced_fragments',
    description:
      'Alphabetically sorted chemical formula with reduced integer chemical\nproportion numbers. The proportion number is omitted if it is 1.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.material.elemental_composition.element': {
    name: 'element',
    description: "The symbol of the element, e.g. 'Pb'.",
    type: {
      type_kind: 'enum',
      type_data: [
        'Ac',
        'Ag',
        'Al',
        'Am',
        'Ar',
        'As',
        'At',
        'Au',
        'B',
        'Ba',
        'Be',
        'Bh',
        'Bi',
        'Bk',
        'Br',
        'C',
        'Ca',
        'Cd',
        'Ce',
        'Cf',
        'Cl',
        'Cm',
        'Cn',
        'Co',
        'Cr',
        'Cs',
        'Cu',
        'Db',
        'Ds',
        'Dy',
        'Er',
        'Es',
        'Eu',
        'F',
        'Fe',
        'Fl',
        'Fm',
        'Fr',
        'Ga',
        'Gd',
        'Ge',
        'H',
        'He',
        'Hf',
        'Hg',
        'Ho',
        'Hs',
        'I',
        'In',
        'Ir',
        'K',
        'Kr',
        'La',
        'Li',
        'Lr',
        'Lu',
        'Lv',
        'Mc',
        'Md',
        'Mg',
        'Mn',
        'Mo',
        'Mt',
        'N',
        'Na',
        'Nb',
        'Nd',
        'Ne',
        'Nh',
        'Ni',
        'No',
        'Np',
        'O',
        'Og',
        'Os',
        'P',
        'Pa',
        'Pb',
        'Pd',
        'Pm',
        'Po',
        'Pr',
        'Pt',
        'Pu',
        'Ra',
        'Rb',
        'Re',
        'Rf',
        'Rg',
        'Rh',
        'Rn',
        'Ru',
        'S',
        'Sb',
        'Sc',
        'Se',
        'Sg',
        'Si',
        'Sm',
        'Sn',
        'Sr',
        'Ta',
        'Tb',
        'Tc',
        'Te',
        'Th',
        'Ti',
        'Tl',
        'Tm',
        'Ts',
        'U',
        'V',
        'W',
        'Xe',
        'Y',
        'Yb',
        'Zn',
        'Zr',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.elemental_composition.atomic_fraction': {
    name: 'atomic_fraction',
    description:
      'The atomic fraction of the element in the system it is contained within.\nPer definition a positive value less than or equal to 1.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.elemental_composition.mass_fraction': {
    name: 'mass_fraction',
    description:
      'The mass fraction of the element in the system it is contained within.\nPer definition a positive value less than or equal to 1.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.symmetry.bravais_lattice': {
    name: 'bravais_lattice',
    description:
      'Identifier for the Bravais lattice in Pearson notation. The first lowercase letter\nidentifies the crystal family and can be one of the following: a (triclinic), b\n(monoclinic), o (orthorhombic), t (tetragonal), h (hexagonal) or c (cubic). The\nsecond uppercase letter identifies the centring and can be one of the following: P\n(primitive), S (face centred), I (body centred), R (rhombohedral centring) or F\n(all faces centred).',
    type: {
      type_kind: 'enum',
      type_data: [
        'aP',
        'cF',
        'cI',
        'cP',
        'hP',
        'hR',
        'mP',
        'mS',
        'oF',
        'oI',
        'oP',
        'oS',
        'tI',
        'tP',
      ],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.symmetry.crystal_system': {
    name: 'crystal_system',
    description: 'Name of the crystal system.',
    type: {
      type_kind: 'enum',
      type_data: [
        'cubic',
        'hexagonal',
        'monoclinic',
        'orthorhombic',
        'tetragonal',
        'triclinic',
        'trigonal',
      ],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.symmetry.hall_number': {
    name: 'hall_number',
    description: 'The Hall number for this system.',
    type: {
      type_kind: 'numpy',
      type_data: 'int32',
    },
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.material.symmetry.hall_symbol': {
    name: 'hall_symbol',
    description: 'The Hall symbol for this system.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.symmetry.point_group': {
    name: 'point_group',
    description:
      'Symbol of the crystallographic point group in the Hermann-Mauguin notation.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.symmetry.space_group_number': {
    name: 'space_group_number',
    description:
      'Specifies the International Union of Crystallography (IUC) number of the 3D space\ngroup of this system.',
    type: {
      type_kind: 'numpy',
      type_data: 'int32',
    },
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.material.symmetry.space_group_symbol': {
    name: 'space_group_symbol',
    description:
      'The International Union of Crystallography (IUC) short symbol of the 3D\nspace group of this system.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.symmetry.prototype_formula': {
    name: 'prototype_formula',
    description: 'The formula of the prototypical material for this structure.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.material.symmetry.prototype_aflow_id': {
    name: 'prototype_aflow_id',
    description:
      'The identifier of this structure in the AFLOW encyclopedia of\ncrystallographic prototypes:\nhttp://www.aflowlib.org/prototype-encyclopedia/index.html',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.symmetry.structure_name': {
    name: 'structure_name',
    description: 'A common name for this structure, e.g. fcc, bcc.',
    type: {
      type_kind: 'enum',
      type_data: [
        '4-member ring',
        'Heusler',
        'bcc',
        'bct',
        'bct5',
        'clathrate',
        'cuprite',
        'diamond',
        'fcc',
        'fct',
        'half-Heusler',
        'hcp',
        'perovskite',
        'rock salt',
        'rutile',
        'simple cubic',
        'wurtzite',
        'zincblende',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.symmetry.strukturbericht_designation': {
    name: 'strukturbericht_designation',
    description:
      "Classification of the material according to the historically grown\n'strukturbericht'.",
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.material.topology.system_id': {
    name: 'system_id',
    description:
      'That path of this section within the metainfo that is used as a unique\nidentifier.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.label': {
    name: 'label',
    description: 'Descriptive label that identifies this structural part.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.method': {
    name: 'method',
    description: 'The method used for identifying this system.',
    type: {
      type_kind: 'enum',
      type_data: ['matid', 'parser', 'porosity', 'user'],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.description': {
    name: 'description',
    description: 'A short description about this part of the topology.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.material_id': {
    name: 'material_id',
    description:
      'A fixed length, unique material identifier in the form of a hash\ndigest.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.material_name': {
    name: 'material_name',
    description: 'Meaningful names for this a material if any can be assigned.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.structural_type': {
    name: 'structural_type',
    description: 'Structural class determined from the atomic structure.',
    type: {
      type_kind: 'enum',
      type_data: [
        '1D',
        '2D',
        'active orbitals',
        'atom',
        'bulk',
        'group',
        'molecule',
        'molecule / cluster',
        'monomer',
        'not processed',
        'surface',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.dimensionality': {
    name: 'dimensionality',
    description:
      "Dimensionality of the system. For atomistic systems this is\nautomatically evaluated by using the topology-scaling algorithm:\nhttps://doi.org/10.1103/PhysRevLett.118.106101.\n\n| Value | Description |\n| --------- | ----------------------- |\n| `'0D'` | Not connected periodically |\n| `'1D'` | Periodically connected in one dimension |\n| `'2D'` | Periodically connected in two dimensions |\n| `'3D'` | Periodically connected in three dimensions |",
    type: {
      type_kind: 'enum',
      type_data: ['0D', '1D', '2D', '3D'],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.building_block': {
    name: 'building_block',
    description:
      "More exact classification for this system, i.e. the type of \"building\nblock\" it represents.\n\n| Value | Description |\n| --------- | ----------------------- |\n| `'surface'` | Structure built from a unit cell that repeats periodically in two directions and at least twice, but not infinitely in a third direction. |\n| `'2D material'` | Structure built from a unit cell that repeats periodically in two directions and only once in a third direction. |\n| `'molecule'` | Molecule defined in the force-field topology |\n| `'monomer'` | Monomer defined in the force-field topology |",
    type: {
      type_kind: 'enum',
      type_data: ['2D material', 'molecule', 'monomer', 'surface'],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.functional_type': {
    name: 'functional_type',
    description: 'Classification based on the functional properties.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.compound_type': {
    name: 'compound_type',
    description: 'Classification based on the chemical formula.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.elements': {
    name: 'elements',
    description: 'Names of the different elements present in the structure.',
    type: {
      type_kind: 'enum',
      type_data: [
        'Ac',
        'Ag',
        'Al',
        'Am',
        'Ar',
        'As',
        'At',
        'Au',
        'B',
        'Ba',
        'Be',
        'Bh',
        'Bi',
        'Bk',
        'Br',
        'C',
        'Ca',
        'Cd',
        'Ce',
        'Cf',
        'Cl',
        'Cm',
        'Cn',
        'Co',
        'Cr',
        'Cs',
        'Cu',
        'Db',
        'Ds',
        'Dy',
        'Er',
        'Es',
        'Eu',
        'F',
        'Fe',
        'Fl',
        'Fm',
        'Fr',
        'Ga',
        'Gd',
        'Ge',
        'H',
        'He',
        'Hf',
        'Hg',
        'Ho',
        'Hs',
        'I',
        'In',
        'Ir',
        'K',
        'Kr',
        'La',
        'Li',
        'Lr',
        'Lu',
        'Lv',
        'Mc',
        'Md',
        'Mg',
        'Mn',
        'Mo',
        'Mt',
        'N',
        'Na',
        'Nb',
        'Nd',
        'Ne',
        'Nh',
        'Ni',
        'No',
        'Np',
        'O',
        'Og',
        'Os',
        'P',
        'Pa',
        'Pb',
        'Pd',
        'Pm',
        'Po',
        'Pr',
        'Pt',
        'Pu',
        'Ra',
        'Rb',
        'Re',
        'Rf',
        'Rg',
        'Rh',
        'Rn',
        'Ru',
        'S',
        'Sb',
        'Sc',
        'Se',
        'Sg',
        'Si',
        'Sm',
        'Sn',
        'Sr',
        'Ta',
        'Tb',
        'Tc',
        'Te',
        'Th',
        'Ti',
        'Tl',
        'Tm',
        'Ts',
        'U',
        'V',
        'W',
        'X',
        'Xe',
        'Y',
        'Yb',
        'Zn',
        'Zr',
      ],
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.n_elements': {
    name: 'n_elements',
    description: 'Number of different elements in the structure as an integer.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.elements_exclusive': {
    name: 'elements_exclusive',
    description:
      'String containing the chemical elements in alphabetical order and\nseparated by a single whitespace. This quantity can be used for\nexclusive element searches where you want to find entries/materials\nwith only certain given elements.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.chemical_formula_descriptive': {
    name: 'chemical_formula_descriptive',
    description:
      'The chemical formula for a structure as a string in a form chosen by the API\nimplementation.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.chemical_formula_reduced': {
    name: 'chemical_formula_reduced',
    description:
      'Alphabetically sorted chemical formula with reduced integer chemical\nproportion numbers. The proportion number is omitted if it is 1.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.chemical_formula_hill': {
    name: 'chemical_formula_hill',
    description:
      'The chemical formula for a structure in Hill form with element\nsymbols followed by non-reduced integer chemical proportion numbers.\nThe proportion number is omitted if it is 1.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.chemical_formula_iupac': {
    name: 'chemical_formula_iupac',
    description:
      'Formula where the elements are ordered using a formal list loosely\nbased on electronegativity as defined in the IUPAC nomenclature of\ninorganic chemistry (2005). Contains reduced integer chemical\nproportion numbers where the proportion number is omitted if it is\n1.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.chemical_formula_anonymous': {
    name: 'chemical_formula_anonymous',
    description:
      'Formula with the elements ordered by their reduced integer chemical\nproportion number, and the chemical species replaced by\nalphabetically ordered letters. The proportion number is omitted if\nit is 1. E.g.  H2O becomes A2B and H2O2 becomes AB. The letters are\ndrawn from the english alphabet that may be extended by increasing\nthe number of letters, e.g. A, B, ..., Z, Aa, Ab and so on. This\ndefinition is in line with the similarly named OPTIMADE definition.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.chemical_formula_reduced_fragments': {
    name: 'chemical_formula_reduced_fragments',
    description:
      'Alphabetically sorted chemical formula with reduced integer chemical\nproportion numbers. The proportion number is omitted if it is 1.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.parent_system': {
    name: 'parent_system',
    description: 'Reference to the parent system.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.child_systems': {
    name: 'child_systems',
    description: 'References to the child systems.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.atomic_fraction': {
    name: 'atomic_fraction',
    description:
      'The atomic fraction of this system in the full structure it is contained in.\nPer definition a positive value less than or equal to 1.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.mass_fraction': {
    name: 'mass_fraction',
    description:
      'The mass fraction of this system in the full structure it is contained within.\nPer definition a positive value less than or equal to 1.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.n_atoms': {
    name: 'n_atoms',
    description:
      'The total number of species (atoms, particles) in the system.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.sbu_type': {
    name: 'sbu_type',
    description:
      'The topological representation of the metal secondary building units (sbus).\nThe shape of most metal sbus are well defined and form the basis of most\n popular MOFs. The most common example is the paddlewheel, rodlike mofs,\n irmofs, uio66',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.largest_cavity_diameter': {
    name: 'largest_cavity_diameter',
    description:
      'The largest cavity diameter is the largest sphere that can be inserted in a porous\nsystem without overlapping with any of the atoms in the system.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.pore_limiting_diameter': {
    name: 'pore_limiting_diameter',
    description:
      'The pore limiting diameter is the largest sphere that can freely\ndiffuse through the porous network without overlapping with any of the\natoms in the system.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.largest_included_sphere_along_free_sphere_path': {
    name: 'largest_included_sphere_along_free_sphere_path',
    description:
      'The largest included sphere along free sphere path is\nlargest sphere that can be inserted in the pore.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.accessible_surface_area': {
    name: 'accessible_surface_area',
    description:
      'The surface area accessible is the area that is accessible to guest molecules\nin a porous system. It is generally considered to be the entire surface area\nthat can be spanned by a probe of a specific radius. In NOMAD, by default we use\na probe that has a radius of 1.86 Angstrom, which correspond to the\ncovalent radii of nitrogen gas. For biomolecular system, a radii of\n1.4 Angstrom can be used, which correspond to the covalent radii\nof water.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter ** 2',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.accessible_volume': {
    name: 'accessible_volume',
    description:
      'Volume of unoccupied space in a system that can be accessible to\nguest molecules, like solvents.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter ** 3',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.void_fraction': {
    name: 'void_fraction',
    description:
      'Ratio of the the volume of the unoccupied space in the system\nto the volume of the entire system. It is a good proxy to\ndetermine how porous a system is. Highly porous systems\noften have a larger void fraction, meanwhile compact or dense\nsystems have smaller void fractions.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.n_channels': {
    name: 'n_channels',
    description:
      'Number of channels present in the porous system, which correspond to the number of\npores within the system.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.sbu_coordination_number': {
    name: 'sbu_coordination_number',
    description:
      'The number of connecting point in the secondary building units(sbu), which corresponds to\nthe to the number of point of extension in the secondary building unit. Some common\nterminology include\n1 : monotopic\n2 : ditopic\n3 : tritopic\n4 : tetratopic\n5 : pentatopic',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.elemental_composition.element': {
    name: 'element',
    description: "The symbol of the element, e.g. 'Pb'.",
    type: {
      type_kind: 'enum',
      type_data: [
        'Ac',
        'Ag',
        'Al',
        'Am',
        'Ar',
        'As',
        'At',
        'Au',
        'B',
        'Ba',
        'Be',
        'Bh',
        'Bi',
        'Bk',
        'Br',
        'C',
        'Ca',
        'Cd',
        'Ce',
        'Cf',
        'Cl',
        'Cm',
        'Cn',
        'Co',
        'Cr',
        'Cs',
        'Cu',
        'Db',
        'Ds',
        'Dy',
        'Er',
        'Es',
        'Eu',
        'F',
        'Fe',
        'Fl',
        'Fm',
        'Fr',
        'Ga',
        'Gd',
        'Ge',
        'H',
        'He',
        'Hf',
        'Hg',
        'Ho',
        'Hs',
        'I',
        'In',
        'Ir',
        'K',
        'Kr',
        'La',
        'Li',
        'Lr',
        'Lu',
        'Lv',
        'Mc',
        'Md',
        'Mg',
        'Mn',
        'Mo',
        'Mt',
        'N',
        'Na',
        'Nb',
        'Nd',
        'Ne',
        'Nh',
        'Ni',
        'No',
        'Np',
        'O',
        'Og',
        'Os',
        'P',
        'Pa',
        'Pb',
        'Pd',
        'Pm',
        'Po',
        'Pr',
        'Pt',
        'Pu',
        'Ra',
        'Rb',
        'Re',
        'Rf',
        'Rg',
        'Rh',
        'Rn',
        'Ru',
        'S',
        'Sb',
        'Sc',
        'Se',
        'Sg',
        'Si',
        'Sm',
        'Sn',
        'Sr',
        'Ta',
        'Tb',
        'Tc',
        'Te',
        'Th',
        'Ti',
        'Tl',
        'Tm',
        'Ts',
        'U',
        'V',
        'W',
        'Xe',
        'Y',
        'Yb',
        'Zn',
        'Zr',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.elemental_composition.atomic_fraction': {
    name: 'atomic_fraction',
    description:
      'The atomic fraction of the element in the system it is contained within.\nPer definition a positive value less than or equal to 1.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.elemental_composition.mass_fraction': {
    name: 'mass_fraction',
    description:
      'The mass fraction of the element in the system it is contained within.\nPer definition a positive value less than or equal to 1.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.system_relation.type': {
    name: 'type',
    description:
      "The type of relation between a system and it's parent.\n\n| Value | Description |\n| --------- | ----------------------- |\n| `'root'` | System representing the entire structure, has no parent system. |\n| `'subsystem'` | A single logical entity extracted from the parent system. |\n| `'group'` | A logical group of subsystems within the parent, e.g. a group of molecules in MD. |\n| `'primitive_cell'` | The conventional cell from which the parent is constructed from. |\n| `'conventional_cell'` | The primitive cell from which the parent is constructed from. |",
    type: {
      type_kind: 'enum',
      type_data: [
        'conventional_cell',
        'group',
        'primitive_cell',
        'root',
        'subsystem',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.cell.a': {
    name: 'a',
    description: 'Length of the first basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.cell.b': {
    name: 'b',
    description: 'Length of the second basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.cell.c': {
    name: 'c',
    description: 'Length of the third basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.cell.alpha': {
    name: 'alpha',
    description: 'Angle between second and third basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'radian',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.cell.beta': {
    name: 'beta',
    description: 'Angle between first and third basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'radian',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.cell.gamma': {
    name: 'gamma',
    description: 'Angle between first and second basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'radian',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.cell.volume': {
    name: 'volume',
    description: 'Volume of the cell.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter ** 3',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.cell.atomic_density': {
    name: 'atomic_density',
    description: "Atomic density of the material (atoms/volume).'",
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: '1 / meter ** 3',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.cell.mass_density': {
    name: 'mass_density',
    description: 'Mass density of the material.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'kilogram / meter ** 3',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.symmetry.bravais_lattice': {
    name: 'bravais_lattice',
    description:
      'Identifier for the Bravais lattice in Pearson notation. The first lowercase letter\nidentifies the crystal family and can be one of the following: a (triclinic), b\n(monoclinic), o (orthorhombic), t (tetragonal), h (hexagonal) or c (cubic). The\nsecond uppercase letter identifies the centring and can be one of the following: P\n(primitive), S (face centred), I (body centred), R (rhombohedral centring) or F\n(all faces centred).',
    type: {
      type_kind: 'enum',
      type_data: [
        'aP',
        'cF',
        'cI',
        'cP',
        'hP',
        'hR',
        'mP',
        'mS',
        'oF',
        'oI',
        'oP',
        'oS',
        'tI',
        'tP',
      ],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.symmetry.crystal_system': {
    name: 'crystal_system',
    description: 'Name of the crystal system.',
    type: {
      type_kind: 'enum',
      type_data: [
        'cubic',
        'hexagonal',
        'monoclinic',
        'orthorhombic',
        'tetragonal',
        'triclinic',
        'trigonal',
      ],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.symmetry.hall_number': {
    name: 'hall_number',
    description: 'The Hall number for this system.',
    type: {
      type_kind: 'numpy',
      type_data: 'int32',
    },
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.symmetry.hall_symbol': {
    name: 'hall_symbol',
    description: 'The Hall symbol for this system.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.symmetry.point_group': {
    name: 'point_group',
    description:
      'Symbol of the crystallographic point group in the Hermann-Mauguin notation.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.symmetry.space_group_number': {
    name: 'space_group_number',
    description:
      'Specifies the International Union of Crystallography (IUC) number of the 3D space\ngroup of this system.',
    type: {
      type_kind: 'numpy',
      type_data: 'int32',
    },
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.symmetry.space_group_symbol': {
    name: 'space_group_symbol',
    description:
      'The International Union of Crystallography (IUC) short symbol of the 3D\nspace group of this system.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.symmetry.strukturbericht_designation': {
    name: 'strukturbericht_designation',
    description:
      "Classification of the material according to the historically grown\n'strukturbericht'.",
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.symmetry.prototype_label_aflow': {
    name: 'prototype_label_aflow',
    description:
      'AFLOW label of the prototype (see\nhttp://aflowlib.org/CrystalDatabase/prototype_index.html) identified on the basis\nof the space_group and normalized_wyckoff.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.symmetry.prototype_name': {
    name: 'prototype_name',
    description:
      'A common name for this prototypical structure, e.g. fcc, bcc.',
    type: {
      type_kind: 'enum',
      type_data: [
        '4-member ring',
        'Heusler',
        'bcc',
        'bct',
        'bct5',
        'clathrate',
        'cuprite',
        'diamond',
        'fcc',
        'fct',
        'half-Heusler',
        'hcp',
        'perovskite',
        'rock salt',
        'rutile',
        'simple cubic',
        'wurtzite',
        'zincblende',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.material.topology.active_orbitals.n_quantum_number': {
    name: 'n_quantum_number',
    description: 'Principal quantum number $n$.',
    type: {
      type_kind: 'numpy',
      type_data: 'int32',
    },
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.active_orbitals.j_quantum_number': {
    name: 'j_quantum_number',
    description:
      'Total angular momentum quantum number $j = |l-s| ... l+s$.\n**Necessary with strong L-S coupling or non-collinear spin systems.**',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    shape: ['1..2'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.active_orbitals.mj_quantum_number': {
    name: 'mj_quantum_number',
    description:
      'Azimuthal projection of the $j$ vector.\n**Necessary with strong L-S coupling or non-collinear spin systems.**',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.active_orbitals.degeneracy': {
    name: 'degeneracy',
    description:
      'The number of states under the filling constraints applied to the orbital set.\nThis implicitly assumes that all orbitals in the set are degenerate.',
    type: {
      type_kind: 'numpy',
      type_data: 'int32',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.active_orbitals.n_electrons_excited': {
    name: 'n_electrons_excited',
    description:
      'The electron charge excited for modelling purposes.\nChoices that deviate from 0 or 1 typically leverage Janak composition.\nUnless the `initial` state is chosen, the model corresponds to a single electron being excited in physical reality.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.active_orbitals.occupation': {
    name: 'occupation',
    description:
      'The total number of electrons within the state (as defined by degeneracy)\nafter exciting the model charge.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.active_orbitals.l_quantum_symbol': {
    name: 'l_quantum_symbol',
    description: 'Azimuthal $l$ in symbolic form.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.active_orbitals.ml_quantum_symbol': {
    name: 'ml_quantum_symbol',
    description: 'Magnetic quantum number $m_l$ in symbolic form.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.material.topology.active_orbitals.ms_quantum_symbol': {
    name: 'ms_quantum_symbol',
    description: 'Spin quantum number $m_s$ in symbolic form.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.method.method_id': {
    name: 'method_id',
    description:
      'Identifier for the used method. Only available for a subset of entries\nfor which the methodology has been identified with precision.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.method.method_name': {
    name: 'method_name',
    description: 'Common name for the used method.',
    type: {
      type_kind: 'enum',
      type_data: [
        'BSE',
        'CoreHole',
        'DFT',
        'DMFT',
        'EELS',
        'GW',
        'NMR',
        'TB',
        'XPS',
        'XRD',
        'kMC',
        'quantum cms',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.workflow_name': {
    name: 'workflow_name',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.program_name': {
    name: 'program_name',
    description: 'The name of the used program.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.program_version': {
    name: 'program_version',
    description: 'The version of the used program.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.program_version_internal': {
    name: 'program_version_internal',
    description: 'The version tag used internally by the development team.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.dft.basis_set_type': {
    name: 'basis_set_type',
    description: 'The used basis set functions.',
    type: {
      type_kind: 'enum',
      type_data: [
        '(L)APW+lo',
        'gaussians',
        'not processed',
        'numeric AOs',
        'plane waves',
        'psinc functions',
        'real-space grid',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.dft.core_electron_treatment': {
    name: 'core_electron_treatment',
    description: 'How the core electrons are described.',
    type: {
      type_kind: 'enum',
      type_data: [
        'all electron frozen core',
        'full all electron',
        'pseudopotential',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.dft.spin_polarized': {
    name: 'spin_polarized',
    description: 'Whether the calculation is spin-polarized.',
    type: {
      type_kind: 'python',
      type_data: 'bool',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.dft.scf_threshold_energy_change': {
    name: 'scf_threshold_energy_change',
    description:
      'Specifies the threshold for the total energy change between two subsequent\nself-consistent field (SCF) iterations. The SCF is considered converged when the\ntotal-energy change between two SCF cycles is below the threshold (possibly in\ncombination with other criteria).',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.dft.van_der_Waals_method': {
    name: 'van_der_Waals_method',
    description: 'The used van der Waals method.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.dft.relativity_method': {
    name: 'relativity_method',
    description:
      'Describes the relativistic treatment used for the calculation of the final energy\nand related quantities. If skipped or empty, no relativistic treatment is applied.',
    type: {
      type_kind: 'enum',
      type_data: [
        'pseudo_scalar_relativistic',
        'scalar_relativistic',
        'scalar_relativistic_atomic_ZORA',
      ],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.dft.smearing_kind': {
    name: 'smearing_kind',
    description:
      'Specifies the kind of smearing on the electron occupation used to calculate the\nfree energy (see energy_free)\n\nValid values are:\n\n| Smearing kind             | Description                       |\n\n| ------------------------- | --------------------------------- |\n\n| `"empty"`                 | No smearing is applied            |\n\n| `"gaussian"`              | Gaussian smearing                 |\n\n| `"fermi"`                 | Fermi smearing                    |\n\n| `"marzari-vanderbilt"`    | Marzari-Vanderbilt smearing       |\n\n| `"methfessel-paxton"`     | Methfessel-Paxton smearing        |\n\n| `"tetrahedra"`            | Interpolation of state energies and occupations\n(ignores smearing_width) |',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.dft.smearing_width': {
    name: 'smearing_width',
    description:
      'Specifies the width of the smearing in energy for the electron occupation used to\ncalculate the free energy (see energy_free).\n\n*NOTE:* Not all methods specified in smearing_kind uses this value.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.dft.jacobs_ladder': {
    name: 'jacobs_ladder',
    description:
      "Functional classification in line with Jacob's Ladder.\nFor more information, see https://doi.org/10.1063/1.1390175 (original paper);\nhttps://doi.org/10.1103/PhysRevLett.91.146401 (meta-GGA);\nand https://doi.org/10.1063/1.1904565 (hyper-GGA).",
    type: {
      type_kind: 'enum',
      type_data: [
        'GGA',
        'LDA',
        'hybrid',
        'hyper-GGA',
        'meta-GGA',
        'not processed',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.dft.xc_functional_type': {
    name: 'xc_functional_type',
    description:
      "Functional classification in line with Jacob's Ladder.\nFor more information, see https://doi.org/10.1063/1.1390175 (original paper);\nhttps://doi.org/10.1103/PhysRevLett.91.146401 (meta-GGA);\nand https://doi.org/10.1063/1.1904565 (hyper-GGA).",
    type: {
      type_kind: 'enum',
      type_data: [
        'GGA',
        'LDA',
        'hybrid',
        'hyper-GGA',
        'meta-GGA',
        'not processed',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.dft.xc_functional_names': {
    name: 'xc_functional_names',
    description:
      'The list of libXC functional names that where used in this entry.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.dft.exact_exchange_mixing_factor': {
    name: 'exact_exchange_mixing_factor',
    description:
      'Amount of exact exchange mixed in with the XC functional (value range = [0,1]).',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.dft.hubbard_kanamori_model.u_effective': {
    name: 'u_effective',
    description: 'Value of the effective U parameter (u - j).',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.method.simulation.dft.hubbard_kanamori_model.u': {
    name: 'u',
    description: 'Value of the (intraorbital) Hubbard interaction',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.method.simulation.dft.hubbard_kanamori_model.j': {
    name: 'j',
    description:
      'Value of the exchange interaction. In rotational invariant systems, j = jh.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.method.simulation.tb.type': {
    name: 'type',
    description:
      'Tight-binding model type: Slater Koster fitting, DFTB approximation, xTB perturbation\ntheory, or Wannier projection.',
    type: {
      type_kind: 'enum',
      type_data: ['DFTB', 'Slater-Koster', 'Wannier', 'not processed', 'xTB'],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.tb.localization_type': {
    name: 'localization_type',
    description: 'Localization type of the Wannier orbitals.',
    type: {
      type_kind: 'enum',
      type_data: ['maximally_localized', 'single_shot'],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.gw.type': {
    name: 'type',
    description:
      "GW Hedin's self-consistency cycle:\n\n| Name      | Description                      | Reference             |\n\n| --------- | -------------------------------- | --------------------- |\n\n| `'G0W0'`  | single-shot                      | PRB 74, 035101 (2006) |\n\n| `'scGW'`  | self-consistent G and W               | PRB 75, 235102 (2007) |\n\n| `'scGW0'` | self-consistent G with fixed W0  | PRB 54, 8411 (1996)   |\n\n| `'scG0W'` | self-consistent W with fixed G0  | -                     |\n\n| `'ev-scGW0'`  | eigenvalues self-consistent G with fixed W0   | PRB 34, 5390 (1986)   |\n\n| `'ev-scGW'`  | eigenvalues self-consistent G and W   | PRB 74, 045102 (2006)   |\n\n| `'qp-scGW0'`  | quasiparticle self-consistent G with fixed W0 | PRL 99, 115109 (2007) |\n\n| `'qp-scGW'`  | quasiparticle self-consistent G and W | PRL 96, 226402 (2006) |",
    type: {
      type_kind: 'enum',
      type_data: [
        'G0W0',
        'ev-scGW',
        'ev-scGW0',
        'qp-scGW',
        'qp-scGW0',
        'scG0W',
        'scGW',
        'scGW0',
      ],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.gw.basis_set_type': {
    name: 'basis_set_type',
    description: 'The used basis set functions.',
    type: {
      type_kind: 'enum',
      type_data: [
        '(L)APW+lo',
        'gaussians',
        'not processed',
        'numeric AOs',
        'plane waves',
        'psinc functions',
        'real-space grid',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.gw.starting_point_type': {
    name: 'starting_point_type',
    description:
      'The libXC based xc functional classification used in the starting point DFT simulation.',
    type: {
      type_kind: 'enum',
      type_data: [
        'GGA',
        'HF',
        'LDA',
        'hybrid',
        'hyper-GGA',
        'meta-GGA',
        'not processed',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.gw.starting_point_names': {
    name: 'starting_point_names',
    description:
      'The list of libXC functional names that where used in this entry.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.bse.type': {
    name: 'type',
    description:
      "Type of BSE hamiltonian solved:\n\n    H_BSE = H_diagonal + 2 * gx * Hx - gc * Hc\n\nwhere gx, gc specifies the type.\n\nOnline resources for the theory:\n- http://exciting.wikidot.com/carbon-excited-states-from-bse#toc1\n- https://www.vasp.at/wiki/index.php/Bethe-Salpeter-equations_calculations\n- https://docs.abinit.org/theory/bse/\n- https://www.yambo-code.eu/wiki/index.php/Bethe-Salpeter_kernel\n\n| Name | Description |\n\n| --------- | ----------------------- |\n\n| `'Singlet'` | gx = 1, gc = 1 |\n\n| `'Triplet'` | gx = 0, gc = 1 |\n\n| `'IP'` | Independent-particle approach |\n\n| `'RPA'` | Random Phase Approximation |",
    type: {
      type_kind: 'enum',
      type_data: ['IP', 'RPA', 'Singlet', 'Triplet'],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.bse.basis_set_type': {
    name: 'basis_set_type',
    description: 'The used basis set functions.',
    type: {
      type_kind: 'enum',
      type_data: [
        '(L)APW+lo',
        'gaussians',
        'not processed',
        'numeric AOs',
        'plane waves',
        'psinc functions',
        'real-space grid',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.bse.starting_point_type': {
    name: 'starting_point_type',
    description:
      'The libXC based xc functional classification used in the starting point DFT simulation.',
    type: {
      type_kind: 'enum',
      type_data: [
        'GGA',
        'HF',
        'LDA',
        'hybrid',
        'hyper-GGA',
        'meta-GGA',
        'not processed',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.bse.starting_point_names': {
    name: 'starting_point_names',
    description:
      'The list of libXC functional names that where used in this entry.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.bse.solver': {
    name: 'solver',
    description:
      "Solver algotithm used to diagonalize the BSE Hamiltonian.\n\n| Name | Description | Reference |\n\n| --------- | ----------------------- | ----------- |\n\n| `'Full-diagonalization'` | Full diagonalization of the BSE Hamiltonian | - |\n\n| `'Lanczos-Haydock'` | Subspace iterative Lanczos-Haydock algorithm | https://doi.org/10.1103/PhysRevB.59.5441 |\n\n| `'GMRES'` | Generalized minimal residual method | https://doi.org/10.1137/0907058 |\n\n| `'SLEPc'` | Scalable Library for Eigenvalue Problem Computations | https://slepc.upv.es/ |\n\n| `'TDA'` | Tamm-Dancoff approximation | https://doi.org/10.1016/S0009-2614(99)01149-5 |",
    type: {
      type_kind: 'enum',
      type_data: [
        'Full-diagonalization',
        'GMRES',
        'Lanczos-Haydock',
        'SLEPc',
        'TDA',
      ],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.bse.gw_type': {
    name: 'gw_type',
    description:
      "GW Hedin's self-consistency cycle:\n\n| Name      | Description                      | Reference             |\n\n| --------- | -------------------------------- | --------------------- |\n\n| `'G0W0'`  | single-shot                      | PRB 74, 035101 (2006) |\n\n| `'scGW'`  | self-consistent G and W               | PRB 75, 235102 (2007) |\n\n| `'scGW0'` | self-consistent G with fixed W0  | PRB 54, 8411 (1996)   |\n\n| `'scG0W'` | self-consistent W with fixed G0  | -                     |\n\n| `'ev-scGW0'`  | eigenvalues self-consistent G with fixed W0   | PRB 34, 5390 (1986)   |\n\n| `'ev-scGW'`  | eigenvalues self-consistent G and W   | PRB 74, 045102 (2006)   |\n\n| `'qp-scGW0'`  | quasiparticle self-consistent G with fixed W0 | PRL 99, 115109 (2007) |\n\n| `'qp-scGW'`  | quasiparticle self-consistent G and W | PRL 96, 226402 (2006) |",
    type: {
      type_kind: 'enum',
      type_data: [
        'G0W0',
        'ev-scGW',
        'ev-scGW0',
        'qp-scGW',
        'qp-scGW0',
        'scG0W',
        'scGW',
        'scGW0',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.dmft.impurity_solver_type': {
    name: 'impurity_solver_type',
    description:
      "Impurity solver method used in the DMFT loop:\n\n| Name              | Reference                            |\n\n| ----------------- | ------------------------------------ |\n\n| `'CT-INT'`        | Rubtsov et al., JEPT Lett 80 (2004)  |\n\n| `'CT-HYB'`        | Werner et al., PRL 97 (2006)         |\n\n| `'CT-AUX'`        | Gull et al., EPL 82 (2008)           |\n\n| `'ED'`            | Caffarrel et al, PRL 72 (1994)       |\n\n| `'NRG'`           | Bulla et al., RMP 80 (2008)          |\n\n| `'MPS'`           | Ganahl et al., PRB 90 (2014)         |\n\n| `'IPT'`           | Georges et al., PRB 45 (1992)        |\n\n| `'NCA'`           | Pruschke et al., PRB 47 (1993)       |\n\n| `'OCA'`           | Pruschke et al., PRB 47 (1993)       |\n\n| `'slave_bosons'`  | Kotliar et al., PRL 57 (1986)        |\n\n| `'hubbard_I'`     | -                                    |",
    type: {
      type_kind: 'enum',
      type_data: [
        'CT-AUX',
        'CT-HYB',
        'CT-INT',
        'ED',
        'IPT',
        'MPS',
        'NCA',
        'NRG',
        'OCA',
        'hubbard_I',
        'slave_bosons',
      ],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.dmft.inverse_temperature': {
    name: 'inverse_temperature',
    description: 'Inverse temperature = 1/(kB*T).',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: '1 / joule',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.dmft.magnetic_state': {
    name: 'magnetic_state',
    description: 'Magnetic state in which the DMFT calculation is done.',
    type: {
      type_kind: 'enum',
      type_data: ['antiferromagnetic', 'ferromagnetic', 'paramagnetic'],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.dmft.u': {
    name: 'u',
    description: 'Value of the (intraorbital) Hubbard interaction',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.dmft.jh': {
    name: 'jh',
    description: "Value of the (interorbital) Hund's coupling.",
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.dmft.analytical_continuation': {
    name: 'analytical_continuation',
    description:
      "Analytical continuation used to continuate the imaginary space Green's functions into\nthe real frequencies space.\n\n| Name           | Description         | Reference                        |\n\n| -------------- | ------------------- | -------------------------------- |\n\n| `'Pade'` | Pade's approximant  | https://www.sciencedirect.com/science/article/pii/0021999173901277?via%3Dihub |\n\n| `'MaxEnt'` | Maximum Entropy method | https://journals.aps.org/prb/abstract/10.1103/PhysRevB.41.2380 |\n\n| `'SVD'` | Singular value decomposition | https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.75.517 |\n\n| `'Stochastic'` | Stochastic method | https://journals.aps.org/prb/abstract/10.1103/PhysRevB.57.10287 |",
    type: {
      type_kind: 'enum',
      type_data: ['MaxEnt', 'Pade', 'SVD', 'Stochastic'],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.precision.k_line_density': {
    name: 'k_line_density',
    description:
      'Amount of sampled k-points per unit reciprocal length along each axis.\nContains the least precise density out of all axes.\nShould only be compared between calulations of similar dimensionality.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.precision.native_tier': {
    name: 'native_tier',
    description:
      'The code-specific tag indicating the precision used\nfor the basis set and meshes of numerical routines.\n\nSupported codes (with hyperlinks to the relevant documentation):\n- [`VASP`](https://www.vasp.at/wiki/index.php/PREC)\n- `FHI-aims`\n- [`CASTEP`](http://www.tcm.phy.cam.ac.uk/castep/documentation/WebHelp/CASTEP.html#modules/castep/tskcastepsetelecquality.htm?Highlight=ultra-fine)',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.precision.basis_set': {
    name: 'basis_set',
    description:
      "The type of basis set used by the program.\n\n| Value                          |                       Description |\n| ------------------------------ | --------------------------------- |\n| `'APW'`                        | Augmented plane waves             |\n| `'LAPW'`                       | Linearized augmented plane waves  |\n| `'APW+lo'`             | Augmented plane waves with local orbitals |\n| `'LAPW+lo'` | Linearized augmented plane waves with local orbitals |\n| `'(L)APW'`                     |     A combination of APW and LAPW |\n| `'(L)APW+lo'`  | A combination of APW and LAPW with local orbitals |\n| `'plane waves'`                | Plane waves                       |\n| `'gaussians + plane waves'`    | Basis set of the Quickstep algorithm (DOI: 10.1016/j.cpc.2004.12.014) |\n| `'real-space grid'`            | Real-space grid                   |\n| `'suppport functions'`         | Support functions                 |",
    type: {
      type_kind: 'enum',
      type_data: [
        '(L)APW',
        '(L)APW+lo',
        'APW',
        'APW+lo',
        'LAPW',
        'LAPW+lo',
        'atom-centered orbitals',
        'gaussians + plane waves',
        'not processed',
        'plane waves',
        'real-space grid',
        'support functions',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.method.simulation.precision.planewave_cutoff': {
    name: 'planewave_cutoff',
    description:
      'Spherical cutoff in reciprocal space for a plane-wave basis set. It is the energy\nof the highest plane-wave ($\\frac{\\hbar^2|k+G|^2}{2m_e}$) included in the basis\nset.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.method.simulation.precision.apw_cutoff': {
    name: 'apw_cutoff',
    description:
      'The spherical cutoff parameter for the interstitial plane waves in the LAPW family.\nThis cutoff is unitless, referring to the product of the smallest muffin-tin radius\nand the length of the cutoff reciprocal vector ($r_{MT} * |K_{cut}|$).',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.method.measurement.xrd.diffraction_method_name': {
    name: 'diffraction_method_name',
    description:
      'The diffraction method used to obtain the diffraction pattern.\n| X-Ray Diffraction Method                                   | Description                                                                                                                                                                                                 |\n|------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|\n| **Powder X-Ray Diffraction (PXRD)**                        | The term "powder" refers more to the random orientation of small crystallites than to the physical form of the sample. Can be used with non-powder samples if they present random crystallite orientations. |\n| **Single Crystal X-Ray Diffraction (SCXRD)**               | Used for determining the atomic structure of a single crystal.                                                                                                                                              |\n| **High-Resolution X-Ray Diffraction (HRXRD)**              | A technique typically used for detailed characterization of epitaxial thin films using precise diffraction measurements.                                                                                    |\n| **Small-Angle X-Ray Scattering (SAXS)**                    | Used for studying nanostructures in the size range of 1-100 nm. Provides information on particle size, shape, and distribution.                                                                             |\n| **X-Ray Reflectivity (XRR)**                               | Used to study thin film layers, interfaces, and multilayers. Provides info on film thickness, density, and roughness.                                                                                       |\n| **Grazing Incidence X-Ray Diffraction (GIXRD)**            | Primarily used for the analysis of thin films with the incident beam at a fixed shallow angle.                                                                                                              |\n| **Reciprocal Space Mapping (RSM)**                         | High-resolution XRD method to measure diffracted intensity in a 2-dimensional region of reciprocal space. Provides information about the real-structure (lattice mismatch, domain structure, stress and defects) in single-crystalline and epitaxial samples.|',
    type: {
      type_kind: 'enum',
      type_data: [
        'Grazing Incidence X-Ray Diffraction (GIXRD)',
        'High-Resolution X-Ray Diffraction (HRXRD)',
        'Powder X-Ray Diffraction (PXRD)',
        'Reciprocal Space Mapping (RSM)',
        'Single Crystal X-Ray Diffraction (SCXRD)',
        'Small-Angle X-Ray Scattering (SAXS)',
        'X-Ray Reflectivity (XRR)',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.n_calculations': {
    name: 'n_calculations',
    description: "The number of performed single configuration calculations.'",
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.available_properties': {
    name: 'available_properties',
    description: 'Subset of the property names that are present in this entry.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structural.radial_distribution_function.type': {
    name: 'type',
    description:
      'Describes if the observable is calculated at the molecular or atomic level.',
    type: {
      type_kind: 'enum',
      type_data: ['atomic', 'molecular'],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.structural.radial_distribution_function.label': {
    name: 'label',
    description:
      'Describes the atoms or molecule types involved in determining the property.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.structural.radial_distribution_function.provenance.label':
    {
      name: 'label',
      description:
        'Class or type of the provenance.\nCan be used to add further description to the provenance.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: false,
      repeats: true,
    },
  'results.properties.structural.radial_distribution_function.provenance.molecular_dynamics.time_step':
    {
      name: 'time_step',
      description:
        'The timestep at which the numerical integration is performed.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'second',
      shape: [],
      aggregatable: false,
      dynamic: false,
      repeats: true,
    },
  'results.properties.structural.radial_distribution_function.provenance.molecular_dynamics.ensemble_type':
    {
      name: 'ensemble_type',
      description:
        'The type of thermodynamic ensemble that was simulated.\n\nAllowed values are:\n\n| Thermodynamic Ensemble          | Description                               |\n\n| ---------------------- | ----------------------------------------- |\n\n| `"NVE"`           | Constant number of particles, volume, and energy |\n\n| `"NVT"`           | Constant number of particles, volume, and temperature |\n\n| `"NPT"`           | Constant number of particles, pressure, and temperature |\n\n| `"NPH"`           | Constant number of particles, pressure, and enthalpy |',
      type: {
        type_kind: 'enum',
        type_data: ['NPH', 'NPT', 'NVE', 'NVT'],
      },
      shape: [],
      aggregatable: true,
      dynamic: false,
      repeats: true,
    },
  'results.properties.structural.radius_of_gyration.kind': {
    name: 'kind',
    description: 'Kind of the quantity.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.structural.radius_of_gyration.label': {
    name: 'label',
    description:
      'Describes the atoms or molecule types involved in determining the property.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.structural.radius_of_gyration.provenance.label': {
    name: 'label',
    description:
      'Class or type of the provenance.\nCan be used to add further description to the provenance.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.structural.radius_of_gyration.provenance.molecular_dynamics.time_step':
    {
      name: 'time_step',
      description:
        'The timestep at which the numerical integration is performed.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'second',
      shape: [],
      aggregatable: false,
      dynamic: false,
      repeats: true,
    },
  'results.properties.structural.radius_of_gyration.provenance.molecular_dynamics.ensemble_type':
    {
      name: 'ensemble_type',
      description:
        'The type of thermodynamic ensemble that was simulated.\n\nAllowed values are:\n\n| Thermodynamic Ensemble          | Description                               |\n\n| ---------------------- | ----------------------------------------- |\n\n| `"NVE"`           | Constant number of particles, volume, and energy |\n\n| `"NVT"`           | Constant number of particles, volume, and temperature |\n\n| `"NPT"`           | Constant number of particles, pressure, and temperature |\n\n| `"NPH"`           | Constant number of particles, pressure, and enthalpy |',
      type: {
        type_kind: 'enum',
        type_data: ['NPH', 'NPT', 'NVE', 'NVT'],
      },
      shape: [],
      aggregatable: true,
      dynamic: false,
      repeats: true,
    },
  'results.properties.structural.diffraction_pattern.incident_beam_wavelength':
    {
      name: 'incident_beam_wavelength',
      description: 'The wavelength of the incident beam.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter',
      aggregatable: false,
      dynamic: false,
      repeats: true,
    },
  'results.properties.dynamical.mean_squared_displacement.type': {
    name: 'type',
    description:
      'Describes if the observable is calculated at the molecular or atomic level.',
    type: {
      type_kind: 'enum',
      type_data: ['atomic', 'molecular'],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.dynamical.mean_squared_displacement.label': {
    name: 'label',
    description:
      'Describes the atoms or molecule types involved in determining the property.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.dynamical.mean_squared_displacement.provenance.label': {
    name: 'label',
    description:
      'Class or type of the provenance.\nCan be used to add further description to the provenance.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.dynamical.mean_squared_displacement.provenance.molecular_dynamics.time_step':
    {
      name: 'time_step',
      description:
        'The timestep at which the numerical integration is performed.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'second',
      shape: [],
      aggregatable: false,
      dynamic: false,
      repeats: true,
    },
  'results.properties.dynamical.mean_squared_displacement.provenance.molecular_dynamics.ensemble_type':
    {
      name: 'ensemble_type',
      description:
        'The type of thermodynamic ensemble that was simulated.\n\nAllowed values are:\n\n| Thermodynamic Ensemble          | Description                               |\n\n| ---------------------- | ----------------------------------------- |\n\n| `"NVE"`           | Constant number of particles, volume, and energy |\n\n| `"NVT"`           | Constant number of particles, volume, and temperature |\n\n| `"NPT"`           | Constant number of particles, pressure, and temperature |\n\n| `"NPH"`           | Constant number of particles, pressure, and enthalpy |',
      type: {
        type_kind: 'enum',
        type_data: ['NPH', 'NPT', 'NVE', 'NVT'],
      },
      shape: [],
      aggregatable: true,
      dynamic: false,
      repeats: true,
    },
  'results.properties.structures.structure_original.nperiodic_dimensions': {
    name: 'nperiodic_dimensions',
    description:
      'An integer specifying the number of periodic dimensions in the\nstructure, equivalent to the number of non-zero entries in\ndimension_types.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_original.n_sites': {
    name: 'n_sites',
    description:
      'An integer specifying the length of the cartesian_site_positions property.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_original.cell_volume': {
    name: 'cell_volume',
    description: 'Volume of the cell.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter ** 3',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_original.lattice_parameters.a': {
    name: 'a',
    description: 'Length of the first basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_original.lattice_parameters.b': {
    name: 'b',
    description: 'Length of the second basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_original.lattice_parameters.c': {
    name: 'c',
    description: 'Length of the third basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_original.lattice_parameters.alpha': {
    name: 'alpha',
    description: 'Angle between second and third basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'radian',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_original.lattice_parameters.beta': {
    name: 'beta',
    description: 'Angle between first and third basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'radian',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_original.lattice_parameters.gamma': {
    name: 'gamma',
    description: 'Angle between first and second basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'radian',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_conventional.nperiodic_dimensions': {
    name: 'nperiodic_dimensions',
    description:
      'An integer specifying the number of periodic dimensions in the\nstructure, equivalent to the number of non-zero entries in\ndimension_types.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_conventional.n_sites': {
    name: 'n_sites',
    description:
      'An integer specifying the length of the cartesian_site_positions property.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_conventional.cell_volume': {
    name: 'cell_volume',
    description: 'Volume of the cell.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter ** 3',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_conventional.lattice_parameters.a': {
    name: 'a',
    description: 'Length of the first basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_conventional.lattice_parameters.b': {
    name: 'b',
    description: 'Length of the second basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_conventional.lattice_parameters.c': {
    name: 'c',
    description: 'Length of the third basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_conventional.lattice_parameters.alpha':
    {
      name: 'alpha',
      description: 'Angle between second and third basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'radian',
      aggregatable: false,
      dynamic: false,
      repeats: false,
    },
  'results.properties.structures.structure_conventional.lattice_parameters.beta':
    {
      name: 'beta',
      description: 'Angle between first and third basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'radian',
      aggregatable: false,
      dynamic: false,
      repeats: false,
    },
  'results.properties.structures.structure_conventional.lattice_parameters.gamma':
    {
      name: 'gamma',
      description: 'Angle between first and second basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'radian',
      aggregatable: false,
      dynamic: false,
      repeats: false,
    },
  'results.properties.structures.structure_primitive.nperiodic_dimensions': {
    name: 'nperiodic_dimensions',
    description:
      'An integer specifying the number of periodic dimensions in the\nstructure, equivalent to the number of non-zero entries in\ndimension_types.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_primitive.n_sites': {
    name: 'n_sites',
    description:
      'An integer specifying the length of the cartesian_site_positions property.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_primitive.cell_volume': {
    name: 'cell_volume',
    description: 'Volume of the cell.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter ** 3',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_primitive.lattice_parameters.a': {
    name: 'a',
    description: 'Length of the first basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_primitive.lattice_parameters.b': {
    name: 'b',
    description: 'Length of the second basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_primitive.lattice_parameters.c': {
    name: 'c',
    description: 'Length of the third basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_primitive.lattice_parameters.alpha':
    {
      name: 'alpha',
      description: 'Angle between second and third basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'radian',
      aggregatable: false,
      dynamic: false,
      repeats: false,
    },
  'results.properties.structures.structure_primitive.lattice_parameters.beta': {
    name: 'beta',
    description: 'Angle between first and third basis vector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'radian',
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.structures.structure_primitive.lattice_parameters.gamma':
    {
      name: 'gamma',
      description: 'Angle between first and second basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'radian',
      aggregatable: false,
      dynamic: false,
      repeats: false,
    },
  'results.properties.electronic.band_gap.index': {
    name: 'index',
    description: 'The spin channel index.',
    type: {
      type_kind: 'numpy',
      type_data: 'int32',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.band_gap.value': {
    name: 'value',
    description:
      'The actual value of the band gap. Value of zero indicates a vanishing band gap and\nis distinct from sources lacking any band gap measurement or calculation.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.band_gap.type': {
    name: 'type',
    description: 'Band gap type.',
    type: {
      type_kind: 'enum',
      type_data: ['direct', 'indirect'],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.band_gap.provenance.label': {
    name: 'label',
    description:
      'Class or type of the provenance.\nCan be used to add further description to the provenance.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic.spin_polarized': {
    name: 'spin_polarized',
    description:
      'Whether the DOS is spin-polarized, i.e. is contains channels for both\nspin values.',
    type: {
      type_kind: 'python',
      type_data: 'bool',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic.band_gap.index': {
    name: 'index',
    description: 'The spin channel index.',
    type: {
      type_kind: 'numpy',
      type_data: 'int32',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic.band_gap.value': {
    name: 'value',
    description:
      'The actual value of the band gap. Value of zero indicates a vanishing band gap and\nis distinct from sources lacking any band gap measurement or calculation.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic.band_gap.type': {
    name: 'type',
    description: 'Band gap type.',
    type: {
      type_kind: 'enum',
      type_data: ['direct', 'indirect'],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic.band_gap.provenance.label': {
    name: 'label',
    description:
      'Class or type of the provenance.\nCan be used to add further description to the provenance.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic_new.spin_polarized': {
    name: 'spin_polarized',
    description:
      'Whether the DOS is spin-polarized, i.e. is contains channels for both\nspin values.',
    type: {
      type_kind: 'python',
      type_data: 'bool',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic_new.has_projected': {
    name: 'has_projected',
    description:
      'Whether the DOS has information about projections (species-, atom-, and/or orbital-\nprojected).',
    type: {
      type_kind: 'python',
      type_data: 'bool',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic_new.data.band_gap.index': {
    name: 'index',
    description: 'The spin channel index.',
    type: {
      type_kind: 'numpy',
      type_data: 'int32',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic_new.data.band_gap.value': {
    name: 'value',
    description:
      'The actual value of the band gap. Value of zero indicates a vanishing band gap and\nis distinct from sources lacking any band gap measurement or calculation.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic_new.data.band_gap.type': {
    name: 'type',
    description: 'Band gap type.',
    type: {
      type_kind: 'enum',
      type_data: ['direct', 'indirect'],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic_new.data.band_gap.provenance.label':
    {
      name: 'label',
      description:
        'Class or type of the provenance.\nCan be used to add further description to the provenance.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: false,
      repeats: true,
    },
  'results.properties.electronic.band_structure_electronic.spin_polarized': {
    name: 'spin_polarized',
    description:
      'Whether the band structure is spin-polarized, i.e. is contains channels\nfor both spin values.',
    type: {
      type_kind: 'python',
      type_data: 'bool',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.band_structure_electronic.band_gap.index': {
    name: 'index',
    description: 'The spin channel index.',
    type: {
      type_kind: 'numpy',
      type_data: 'int32',
    },
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.band_structure_electronic.band_gap.value': {
    name: 'value',
    description:
      'The actual value of the band gap. Value of zero indicates a vanishing band gap and\nis distinct from sources lacking any band gap measurement or calculation.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.band_structure_electronic.band_gap.type': {
    name: 'type',
    description: 'Band gap type.',
    type: {
      type_kind: 'enum',
      type_data: ['direct', 'indirect'],
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.electronic.band_structure_electronic.band_gap.provenance.label':
    {
      name: 'label',
      description:
        'Class or type of the provenance.\nCan be used to add further description to the provenance.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: false,
      repeats: true,
    },
  'results.properties.magnetic.spin_spin_coupling.source': {
    name: 'source',
    description:
      "Identifier for the source of the data: 'experiment' or 'simulation'.",
    type: {
      type_kind: 'enum',
      type_data: ['experiment', 'simulation'],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.magnetic.magnetic_susceptibility.source': {
    name: 'source',
    description:
      "Identifier for the source of the data: 'experiment' or 'simulation'.",
    type: {
      type_kind: 'enum',
      type_data: ['experiment', 'simulation'],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.optoelectronic.solar_cell.efficiency': {
    name: 'efficiency',
    description: 'Power conversion effciency of a solar cell in percentage %.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.optoelectronic.solar_cell.fill_factor': {
    name: 'fill_factor',
    description:
      'Fill factor of a solar cell in absolute values (from 0 to 1).',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.optoelectronic.solar_cell.open_circuit_voltage': {
    name: 'open_circuit_voltage',
    description: 'Open circuit voltage of a solar cell.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'volt',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.optoelectronic.solar_cell.short_circuit_current_density':
    {
      name: 'short_circuit_current_density',
      description: 'Short circuit current density of a solar cell.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'ampere / meter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: false,
      repeats: false,
    },
  'results.properties.optoelectronic.solar_cell.illumination_intensity': {
    name: 'illumination_intensity',
    description: 'The light intensity during the IV measurement.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'watt / meter ** 2',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.optoelectronic.solar_cell.device_area': {
    name: 'device_area',
    description:
      'The total area of the cell during IV and stability measurements under illumination.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter ** 2',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.optoelectronic.solar_cell.device_architecture': {
    name: 'device_architecture',
    description:
      'Device architecture of the solar cell. Examples are:\n`pn-Heterojunction`, `pin`, `nip`, ...',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.optoelectronic.solar_cell.device_stack': {
    name: 'device_stack',
    description: 'Layers of the entire device.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.optoelectronic.solar_cell.absorber': {
    name: 'absorber',
    description: 'Absorber layers used in the solar cell.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.optoelectronic.solar_cell.absorber_fabrication': {
    name: 'absorber_fabrication',
    description:
      'Technique describing the fabrication of the absorber layer. Examples are:\n`Spin-coating`, `Evaporation`, `Doctor blading`, ...',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.optoelectronic.solar_cell.electron_transport_layer': {
    name: 'electron_transport_layer',
    description: 'Electron selective contact layers used in the solar cell.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.optoelectronic.solar_cell.hole_transport_layer': {
    name: 'hole_transport_layer',
    description: 'Hole selective contact layers used in the solar cell.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.optoelectronic.solar_cell.substrate': {
    name: 'substrate',
    description: 'Substrate layers used in the solar cell.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.optoelectronic.solar_cell.back_contact': {
    name: 'back_contact',
    description: 'Back contact layers used in the solar cell.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.catalytic.reaction.name': {
    name: 'name',
    description: 'Name of the catalytic test reaction.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.catalytic.reaction.type': {
    name: 'type',
    description:
      'Classification of the catalytic test reaction such as Oxidation, Hydrogenation,\nIsomerization, Coupling...',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.catalytic.reaction.reactants.name': {
    name: 'name',
    description: 'IUPAC name of the reagent.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.catalytic.reaction.reactants.gas_concentration_in': {
    name: 'gas_concentration_in',
    description:
      'Volumetric concentration (fraction) of the reagent in the feed gas.\nShould be a value between 0 and 1.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.catalytic.reaction.reactants.gas_concentration_out': {
    name: 'gas_concentration_out',
    description:
      'Volumetric concentration (fraction) of the reagent after the reactor.\nShould be a value between 0 and 1.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.catalytic.reaction.reactants.conversion': {
    name: 'conversion',
    description: 'Conversion of the reactant, in %.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.catalytic.reaction.products.name': {
    name: 'name',
    description: 'Name of the product, preferably the IUPAC name.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.catalytic.reaction.products.gas_concentration_in': {
    name: 'gas_concentration_in',
    description:
      'Volumetric concentration (fraction) of the reagent in the feed gas.\nShould be a value between 0 and 1.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.catalytic.reaction.products.gas_concentration_out': {
    name: 'gas_concentration_out',
    description:
      'Volumetric concentration (fraction) of the reagent after the reactor.\nShould be a value between 0 and 1.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.catalytic.reaction.products.selectivity': {
    name: 'selectivity',
    description: 'Selectivity of the product, in %.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.catalytic.reaction.products.space_time_yield': {
    name: 'space_time_yield',
    description:
      'Space-time-yield of the product, in mass product per mass catalyst per time.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: '1 / second',
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.catalytic.reaction.rates.name': {
    name: 'name',
    description: 'IUPAC name of the reagent whose rate is captured.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.catalytic.reaction.rates.reaction_rate': {
    name: 'reaction_rate',
    description:
      'The rate of the number of reactant or product molecules converted/produced,\nper mass of total catalyst, per time.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'mole / gram / second',
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.catalytic.reaction.rates.specific_mass_rate': {
    name: 'specific_mass_rate',
    description:
      'The specific rate of the reactant, per mass of active catalyst component\n(e.g. metal).',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'mole / gram / second',
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.catalytic.reaction.rates.specific_surface_area_rate': {
    name: 'specific_surface_area_rate',
    description:
      'The specific rate of the reactant, per surface area of active catalyst.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'mole / meter ** 2 / second',
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.catalytic.reaction.rates.turnover_frequency': {
    name: 'turnover_frequency',
    description:
      'The turnover frequency, calculated from mol of reactant or product per\nnumber of sites over time.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: '1 / second',
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.catalytic.reaction.reaction_conditions.temperature': {
    name: 'temperature',
    description:
      'The reaction temperature(s) in the catalytic reactor during a chemical reaction.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'kelvin',
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.catalytic.reaction.reaction_conditions.pressure': {
    name: 'pressure',
    description: 'The pressure during the catalytic test reaction.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'pascal',
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.catalytic.reaction.reaction_conditions.weight_hourly_space_velocity':
    {
      name: 'weight_hourly_space_velocity',
      description:
        'The weight hourly space velocity in 1/time (gas flow per catalyst mass).',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'milliliter / gram / second',
      shape: ['*'],
      aggregatable: false,
      dynamic: false,
      repeats: false,
    },
  'results.properties.catalytic.reaction.reaction_conditions.gas_hourly_space_velocity':
    {
      name: 'gas_hourly_space_velocity',
      description:
        'The gas hourly space velocity in 1/time (gas flow per catalyst volume).',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: '1 / second',
      shape: ['*'],
      aggregatable: false,
      dynamic: false,
      repeats: false,
    },
  'results.properties.catalytic.reaction.reaction_conditions.flow_rate': {
    name: 'flow_rate',
    description: 'The volumetric gas flow in volume per time.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter ** 3 / second',
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.catalytic.reaction.reaction_conditions.time_on_stream': {
    name: 'time_on_stream',
    description:
      'The time on stream of the catalyst in the catalytic reaction.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'second',
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.catalytic.reaction.reaction_mechanism.initial_states': {
    name: 'initial_states',
    description: 'The names of reactants of the reaction or elementary step.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.catalytic.reaction.reaction_mechanism.final_states': {
    name: 'final_states',
    description: 'The names of products of the reaction or elementary step.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.catalytic.reaction.reaction_mechanism.reaction_enthalpy':
    {
      name: 'reaction_enthalpy',
      description: 'The reaction enthalpy of the reaction or reaction step.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      shape: [],
      aggregatable: false,
      dynamic: false,
      repeats: true,
    },
  'results.properties.catalytic.reaction.reaction_mechanism.activation_energy':
    {
      name: 'activation_energy',
      description:
        'The (apparent) activation energy of the catalyzed reaction or reaction step.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      shape: [],
      aggregatable: false,
      dynamic: false,
      repeats: true,
    },
  'results.properties.catalytic.catalyst.catalyst_name': {
    name: 'catalyst_name',
    description: 'Custom name of catalyst.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.catalytic.catalyst.preparation_method': {
    name: 'preparation_method',
    description: 'The main preparation method of the catalyst sample.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.catalytic.catalyst.catalyst_type': {
    name: 'catalyst_type',
    description:
      'The type of catalyst, wether metal or oxide, model, bulk, supported, ect.\nMultiple values can apply.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.catalytic.catalyst.characterization_methods': {
    name: 'characterization_methods',
    description: 'A list of methods used to characterize the catalyst sample.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'results.properties.catalytic.catalyst.surface_area': {
    name: 'surface_area',
    description: 'The surface area per catalyst mass.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter ** 2 / gram',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.mechanical.energy_volume_curve.type': {
    name: 'type',
    type: {
      type_kind: 'enum',
      type_data: [
        'birch_euler',
        'birch_lagrange',
        'birch_murnaghan',
        'mie_gruneisen',
        'murnaghan',
        'pack_evans_james',
        'pourier_tarantola',
        'raw',
        'tait',
        'vinet',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.mechanical.bulk_modulus.type': {
    name: 'type',
    description: 'Describes the methodology for obtaining the value.',
    type: {
      type_kind: 'enum',
      type_data: [
        'birch_euler',
        'birch_lagrange',
        'birch_murnaghan',
        'mie_gruneisen',
        'murnaghan',
        'pack_evans_james',
        'pourier_tarantola',
        'reuss_average',
        'tait',
        'vinet',
        'voigt_average',
        'voigt_reuss_hill_average',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.mechanical.bulk_modulus.value': {
    name: 'value',
    description: 'Bulk modulus value.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'pascal',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.mechanical.shear_modulus.type': {
    name: 'type',
    description: 'Describes the methodology for obtaining the value.',
    type: {
      type_kind: 'enum',
      type_data: ['reuss_average', 'voigt_average', 'voigt_reuss_hill_average'],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.mechanical.shear_modulus.value': {
    name: 'value',
    description: 'Shear modulus value.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'pascal',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.thermodynamic.trajectory.available_properties': {
    name: 'available_properties',
    description:
      'Subset of the property names that are present in this trajectory.',
    type: {
      type_kind: 'enum',
      type_data: ['energy_potential', 'pressure', 'temperature', 'volume'],
    },
    shape: ['0..*'],
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.thermodynamic.trajectory.provenance.label': {
    name: 'label',
    description:
      'Class or type of the provenance.\nCan be used to add further description to the provenance.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.thermodynamic.trajectory.provenance.molecular_dynamics.time_step':
    {
      name: 'time_step',
      description:
        'The timestep at which the numerical integration is performed.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'second',
      shape: [],
      aggregatable: false,
      dynamic: false,
      repeats: true,
    },
  'results.properties.thermodynamic.trajectory.provenance.molecular_dynamics.ensemble_type':
    {
      name: 'ensemble_type',
      description:
        'The type of thermodynamic ensemble that was simulated.\n\nAllowed values are:\n\n| Thermodynamic Ensemble          | Description                               |\n\n| ---------------------- | ----------------------------------------- |\n\n| `"NVE"`           | Constant number of particles, volume, and energy |\n\n| `"NVT"`           | Constant number of particles, volume, and temperature |\n\n| `"NPT"`           | Constant number of particles, pressure, and temperature |\n\n| `"NPH"`           | Constant number of particles, pressure, and enthalpy |',
      type: {
        type_kind: 'enum',
        type_data: ['NPH', 'NPT', 'NVE', 'NVT'],
      },
      shape: [],
      aggregatable: true,
      dynamic: false,
      repeats: true,
    },
  'results.properties.spectroscopic.spectra.type': {
    name: 'type',
    description:
      'Identifier for the methodology done to obtain the spectra data: EELS, XAS, XPS, etc.',
    type: {
      type_kind: 'enum',
      type_data: [
        'EELS',
        'EXAFS',
        'RXIS',
        'XANES',
        'XAS',
        'XES',
        'XPS',
        'unavailable',
      ],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.spectroscopic.spectra.label': {
    name: 'label',
    description:
      "Identifier for the source of the spectra data, either 'computation' or 'experiment'.",
    type: {
      type_kind: 'enum',
      type_data: ['computation', 'experiment'],
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.spectroscopic.spectra.provenance.label': {
    name: 'label',
    description:
      'Class or type of the provenance.\nCan be used to add further description to the provenance.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: [],
    aggregatable: true,
    dynamic: false,
    repeats: true,
  },
  'results.properties.spectroscopic.spectra.provenance.eels.detector_type': {
    name: 'detector_type',
    description: 'Detector type.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: false,
    repeats: true,
    suggestion: true,
  },
  'results.properties.spectroscopic.spectra.provenance.eels.resolution': {
    name: 'resolution',
    description: 'Energy resolution of the detector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.spectroscopic.spectra.provenance.eels.max_energy': {
    name: 'max_energy',
    description: 'Maximum energy of the detector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.spectroscopic.spectra.provenance.eels.min_energy': {
    name: 'min_energy',
    description: 'Minimum energy of the detector.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    aggregatable: false,
    dynamic: false,
    repeats: true,
  },
  'results.properties.spectroscopic.spectra.provenance.electronic_structure.label':
    {
      name: 'label',
      description:
        'Class or type of the provenance.\nCan be used to add further description to the provenance.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: false,
      repeats: true,
    },
  'results.properties.geometry_optimization.convergence_tolerance_energy_difference':
    {
      name: 'convergence_tolerance_energy_difference',
      description: 'The input energy difference tolerance criterion.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      shape: [],
      aggregatable: false,
      dynamic: false,
      repeats: false,
    },
  'results.properties.geometry_optimization.convergence_tolerance_force_maximum':
    {
      name: 'convergence_tolerance_force_maximum',
      description: 'The input maximum net force tolerance criterion.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'newton',
      shape: [],
      aggregatable: false,
      dynamic: false,
      repeats: false,
    },
  'results.properties.geometry_optimization.final_force_maximum': {
    name: 'final_force_maximum',
    description: 'The maximum net force in the last optimization step.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'newton',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.geometry_optimization.final_energy_difference': {
    name: 'final_energy_difference',
    description:
      'The difference in the energy_total between the last two steps during\noptimization.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'joule',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.properties.geometry_optimization.final_displacement_maximum': {
    name: 'final_displacement_maximum',
    description:
      'The maximum displacement in the last optimization step with respect to previous.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'meter',
    shape: [],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.eln.sections': {
    name: 'sections',
    description:
      'The type of sections used in entries to search for. By default these are the names\nof the used section definitions.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.eln.tags': {
    name: 'tags',
    description:
      'Short tags that are useful to quickly search based on various\nuser defined criteria.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.eln.names': {
    name: 'names',
    description:
      'Short human readable and descriptive names that appear in\nELN entries.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.eln.descriptions': {
    name: 'descriptions',
    description: "'Human descriptions that appear in ELN entries.",
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: false,
    dynamic: false,
    repeats: false,
  },
  'results.eln.instruments': {
    name: 'instruments',
    description:
      'The name or type of instrument used in an activity, e.g. process or\nmeasurement.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.eln.methods': {
    name: 'methods',
    description:
      'The name or the applied method in an activity, e.g. process or measurement',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
  },
  'results.eln.lab_ids': {
    name: 'lab_ids',
    description:
      'The laboratory specific id for any item, e.g. sample, chemical, instrument.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    shape: ['*'],
    aggregatable: true,
    dynamic: false,
    repeats: false,
    suggestion: true,
  },
  'data.name#nomad_simulations.schema_packages.general.Simulation': {
    name: 'name',
    description: 'A short human readable and descriptive name.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.datetime#nomad_simulations.schema_packages.general.Simulation': {
    name: 'datetime',
    description: 'The date and time when this activity was started.',
    type: {
      type_kind: 'custom',
      type_data: 'nomad.metainfo.data_type.Datetime',
    },
    aggregatable: false,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.lab_id#nomad_simulations.schema_packages.general.Simulation': {
    name: 'lab_id',
    description:
      'An ID string that is unique at least for the lab that produced this\ndata.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.description#nomad_simulations.schema_packages.general.Simulation': {
    name: 'description',
    description: 'Any information that cannot be captured in the other fields.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.method#nomad_simulations.schema_packages.general.Simulation': {
    name: 'method',
    description: 'A short consistent handle for the applied method.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.location#nomad_simulations.schema_packages.general.Simulation': {
    name: 'location',
    description: 'location of the experiment.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.datetime_end#nomad_simulations.schema_packages.general.Simulation': {
    name: 'datetime_end',
    description: 'The date and time when this computation ended.',
    type: {
      type_kind: 'custom',
      type_data: 'nomad.metainfo.data_type.Datetime',
    },
    aggregatable: false,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.cpu1_start#nomad_simulations.schema_packages.general.Simulation': {
    name: 'cpu1_start',
    description: 'The starting time of the computation on the (first) CPU 1.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'second',
    aggregatable: false,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.cpu1_end#nomad_simulations.schema_packages.general.Simulation': {
    name: 'cpu1_end',
    description: 'The end time of the computation on the (first) CPU 1.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'second',
    aggregatable: false,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.wall_start#nomad_simulations.schema_packages.general.Simulation': {
    name: 'wall_start',
    description:
      'The internal wall-clock time from the starting of the computation.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'second',
    aggregatable: false,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.wall_end#nomad_simulations.schema_packages.general.Simulation': {
    name: 'wall_end',
    description:
      'The internal wall-clock time from the end of the computation.',
    type: {
      type_kind: 'numpy',
      type_data: 'float64',
    },
    unit: 'second',
    aggregatable: false,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.steps.name#nomad_simulations.schema_packages.general.Simulation': {
    name: 'name',
    description: 'A short and descriptive name for this step.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.steps.start_time#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'start_time',
      description:
        'Optionally, the starting time of the activity step. If omitted, it is assumed to\nfollow directly after the previous step.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.Datetime',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.steps.comment#nomad_simulations.schema_packages.general.Simulation': {
    name: 'comment',
    description:
      'Any additional information about the step not captured by the other fields.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.program.name#nomad_simulations.schema_packages.general.Simulation': {
    name: 'name',
    description: 'The name of the program.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.program.datetime#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'datetime',
      description: 'The date and time associated with this section.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.Datetime',
      },
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.program.lab_id#nomad_simulations.schema_packages.general.Simulation': {
    name: 'lab_id',
    description:
      'An ID string that is unique at least for the lab that produced this\ndata.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.program.description#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'description',
      description:
        'Any information that cannot be captured in the other fields.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.program.version#nomad_simulations.schema_packages.general.Simulation': {
    name: 'version',
    description: 'The version label of the program.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.program.link#nomad_simulations.schema_packages.general.Simulation': {
    name: 'link',
    description: 'Website link to the program in published information.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_simulations.schema_packages.general.Simulation',
  },
  'data.program.version_internal#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'version_internal',
      description:
        'Specifies a program version tag used internally for development purposes.\nAny kind of tagging system is supported, including git commit hashes.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.program.compilation_host#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'compilation_host',
      description: 'Specifies the host on which the program was compiled.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Any verbose naming refering to the ModelSystem. Can be left empty if it is a simple\ncrystal or it can be filled up. For example, an heterostructure of graphene (G) sandwiched\nin between hexagonal boron nitrides (hBN) slabs could be named 'hBN/G/hBN'.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.datetime#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'datetime',
      description: 'The date and time associated with this section.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.Datetime',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.lab_id#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'lab_id',
      description:
        'An ID string that is unique at least for the lab that produced this\ndata.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.description#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'description',
      description:
        'Any information that cannot be captured in the other fields.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        'Type of the system (atom, bulk, surface, etc.) which is determined by the normalizer.',
      type: {
        type_kind: 'enum',
        type_data: [
          '1D',
          '2D',
          'active_atom',
          'atom',
          'bulk',
          'molecule / cluster',
          'surface',
          'unavailable',
        ],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.dimensionality#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'dimensionality',
      description:
        'Dimensionality of the system: 0, 1, 2, or 3 dimensions. For atomistic systems this\nis automatically evaluated by using the topology-scaling algorithm:\n\n    https://doi.org/10.1103/PhysRevLett.118.106101.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.is_representative#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_representative',
      description:
        'If the model system section is the one representative of the computational simulation.\nDefaults to False and set to True by the `Computation.normalize()`. If set to True,\nthe `ModelSystem.normalize()` function is ran (otherwise, it is not).',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.time_step#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'time_step',
      description:
        'Specific time snapshot of the ModelSystem. The time evolution is then encoded\nin a list of ModelSystems under Computation where for each element this quantity defines\nthe time step.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.branch_label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'branch_label',
      description:
        'Label of the specific branch in the hierarchical `ModelSystem` tree.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.branch_depth#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'branch_depth',
      description:
        'Index refering to the depth of a branch in the hierarchical `ModelSystem` tree.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.bond_list#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'bond_list',
      description:
        'List of pairs of atom indices corresponding to bonds (e.g., as defined by a force field)\nwithin this atoms_group.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.composition_formula#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'composition_formula',
      description:
        'The overall composition of the system with respect to its subsystems.\nThe syntax for a system composed of X and Y with x and y components of each,\nrespectively, is X(x)Y(y). At the deepest branch in the hierarchy, the\ncomposition_formula is expressed in terms of the atomic labels.\n\nExample: A system composed of 3 water molecules with the following hierarchy\n\n                        TotalSystem\n                            |\n                        group_H2O\n                        |   |   |\n                       H2O H2O H2O\n\nhas the following compositional formulas at each branch:\n\n    branch 0, index 0: "Total_System" composition_formula = group_H2O(1)\n    branch 1, index 0: "group_H2O"    composition_formula = H2O(3)\n    branch 2, index 0: "H2O"          composition_formula = H(1)O(2)',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.elemental_composition.element#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'element',
      description: "The symbol of the element, e.g. 'Pb'.",
      type: {
        type_kind: 'enum',
        type_data: [
          'Ac',
          'Ag',
          'Al',
          'Am',
          'Ar',
          'As',
          'At',
          'Au',
          'B',
          'Ba',
          'Be',
          'Bh',
          'Bi',
          'Bk',
          'Br',
          'C',
          'Ca',
          'Cd',
          'Ce',
          'Cf',
          'Cl',
          'Cm',
          'Cn',
          'Co',
          'Cr',
          'Cs',
          'Cu',
          'Db',
          'Ds',
          'Dy',
          'Er',
          'Es',
          'Eu',
          'F',
          'Fe',
          'Fl',
          'Fm',
          'Fr',
          'Ga',
          'Gd',
          'Ge',
          'H',
          'He',
          'Hf',
          'Hg',
          'Ho',
          'Hs',
          'I',
          'In',
          'Ir',
          'K',
          'Kr',
          'La',
          'Li',
          'Lr',
          'Lu',
          'Lv',
          'Mc',
          'Md',
          'Mg',
          'Mn',
          'Mo',
          'Mt',
          'N',
          'Na',
          'Nb',
          'Nd',
          'Ne',
          'Nh',
          'Ni',
          'No',
          'Np',
          'O',
          'Og',
          'Os',
          'P',
          'Pa',
          'Pb',
          'Pd',
          'Pm',
          'Po',
          'Pr',
          'Pt',
          'Pu',
          'Ra',
          'Rb',
          'Re',
          'Rf',
          'Rg',
          'Rh',
          'Rn',
          'Ru',
          'S',
          'Sb',
          'Sc',
          'Se',
          'Sg',
          'Si',
          'Sm',
          'Sn',
          'Sr',
          'Ta',
          'Tb',
          'Tc',
          'Te',
          'Th',
          'Ti',
          'Tl',
          'Tm',
          'Ts',
          'U',
          'V',
          'W',
          'Xe',
          'Y',
          'Yb',
          'Zn',
          'Zr',
        ],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.elemental_composition.atomic_fraction#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'atomic_fraction',
      description:
        'The atomic fraction of the element in the system it is contained within.\nPer definition a positive value less than or equal to 1.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.elemental_composition.mass_fraction#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'mass_fraction',
      description:
        'The mass fraction of the element in the system it is contained within.\nPer definition a positive value less than or equal to 1.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        'Name of the specific cell section. This is typically used to easy identification of the\n`Cell` section. Possible values: "AtomicCell".',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.datetime#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'datetime',
      description: 'The date and time associated with this section.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.Datetime',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.lab_id#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'lab_id',
      description:
        'An ID string that is unique at least for the lab that produced this\ndata.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.description#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'description',
      description:
        'Any information that cannot be captured in the other fields.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.length_vector_a#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'length_vector_a',
      description: 'Length of the first basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.length_vector_b#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'length_vector_b',
      description: 'Length of the second basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.length_vector_c#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'length_vector_c',
      description: 'Length of the third basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.angle_vectors_b_c#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'angle_vectors_b_c',
      description: 'Angle between second and third basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'radian',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.angle_vectors_a_c#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'angle_vectors_a_c',
      description: 'Angle between first and third basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'radian',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.angle_vectors_a_b#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'angle_vectors_a_b',
      description: 'Angle between first and second basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'radian',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.volume#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'volume',
      description: 'Volume of a 3D real space entity.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter ** 3',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.surface_area#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'surface_area',
      description: 'Surface area of a 3D real space entity.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter ** 2',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.area#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'area',
      description: 'Area of a 2D real space entity.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter ** 2',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.length#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'length',
      description: 'Total length of a 1D real space entity.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.coordinates_system#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'coordinates_system',
      description:
        "Coordinate system used to determine the geometrical information of a shape in real\nspace. Default to 'cartesian'.",
      type: {
        type_kind: 'enum',
        type_data: ['cartesian', 'cylindrical', 'polar', 'spherical'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Representation type of the cell structure. It might be:\n    - 'original' as in origanally parsed,\n    - 'primitive' as the primitive unit cell,\n    - 'conventional' as the conventional cell used for referencing.",
      type: {
        type_kind: 'enum',
        type_data: ['conventional', 'original', 'primitive'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.cell.n_cell_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_cell_points',
      description: 'Number of cell points.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.symmetry.bravais_lattice#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'bravais_lattice',
      description:
        'Bravais lattice in Pearson notation.\n\nThe first lowercase letter identifies the\ncrystal family: a (triclinic), b (monoclinic), o (orthorhombic), t (tetragonal),\nh (hexagonal), c (cubic).\n\nThe second uppercase letter identifies the centring: P (primitive), S (face centered),\nI (body centred), R (rhombohedral centring), F (all faces centred).',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.symmetry.hall_symbol#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'hall_symbol',
      description:
        'Hall symbol for this system describing the minimum number of symmetry operations\nneeded to uniquely define a space group. See https://cci.lbl.gov/sginfo/hall_symbols.html.\nExamples:\n    - `F -4 2 3`,\n    - `-P 4 2`,\n    - `-F 4 2 3`.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.symmetry.point_group_symbol#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'point_group_symbol',
      description:
        'Symbol of the crystallographic point group in the Hermann-Mauguin notation. See\nhttps://en.wikipedia.org/wiki/Crystallographic_point_group. Examples:\n    - `-43m`,\n    - `4/mmm`,\n    - `m-3m`.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.symmetry.space_group_number#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'space_group_number',
      description:
        'Specifies the International Union of Crystallography (IUC) space group number of the 3D\nspace group of this system. See https://en.wikipedia.org/wiki/List_of_space_groups.\nExamples:\n    - `216`,\n    - `123`,\n    - `225`.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.symmetry.space_group_symbol#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'space_group_symbol',
      description:
        'Specifies the International Union of Crystallography (IUC) space group symbol of the 3D\nspace group of this system. See https://en.wikipedia.org/wiki/List_of_space_groups.\nExamples:\n    - `F-43m`,\n    - `P4/mmm`,\n    - `Fm-3m`.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.symmetry.strukturbericht_designation#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'strukturbericht_designation',
      description:
        "Classification of the material according to the historically grown and similar crystal\nstructures ('strukturbericht'). Useful when using altogether with `space_group_symbol`.\nExamples:\n    - `C1B`, `B3`, `C15b`,\n    - `L10`, `L60`,\n    - `L21`.\n\nExtracted from the AFLOW encyclopedia of crystallographic prototypes.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.symmetry.prototype_formula#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'prototype_formula',
      description:
        'The formula of the prototypical material for this structure as extracted from the\nAFLOW encyclopedia of crystallographic prototypes. It is a string with the chemical\nsymbols:\n    - https://aflowlib.org/prototype-encyclopedia/chemical_symbols.html',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.symmetry.prototype_aflow_id#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'prototype_aflow_id',
      description:
        'The identifier of this structure in the AFLOW encyclopedia of crystallographic prototypes:\n    http://www.aflowlib.org/prototype-encyclopedia/index.html',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.chemical_formula.descriptive#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'descriptive',
      description:
        'The chemical formula of the system as a string to be descriptive of the computation.\nIt is derived from `elemental_composition` if not specified, with non-reduced integer\nnumbers for the proportions of the elements.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.chemical_formula.reduced#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'reduced',
      description:
        'Alphabetically sorted chemical formula with reduced integer chemical proportion\nnumbers. The proportion number is omitted if it is 1.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.chemical_formula.iupac#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iupac',
      description:
        'Chemical formula where the elements are ordered using a formal list based on\nelectronegativity as defined in the IUPAC nomenclature of inorganic chemistry (2005):\n\n    - https://en.wikipedia.org/wiki/List_of_inorganic_compounds\n\nContains reduced integer chemical proportion numbers where the proportion number\nis omitted if it is 1.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.chemical_formula.hill#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'hill',
      description:
        'Chemical formula where Carbon is placed first, then Hydrogen, and then all the other\nelements in alphabetical order. If Carbon is not present, the order is alphabetical.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.chemical_formula.anonymous#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'anonymous',
      description:
        'Formula with the elements ordered by their reduced integer chemical proportion\nnumber, and the chemical species replaced by alphabetically ordered letters. The\nproportion number is omitted if it is 1.\n\nExamples: H2O becomes A2B and H2O2 becomes AB. The letters are drawn from the English\nalphabet that may be extended by increasing the number of letters: A, B, ..., Z, Aa, Ab\nand so on. This definition is in line with the similarly named OPTIMADE definition.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Any verbose naming refering to the ModelSystem. Can be left empty if it is a simple\ncrystal or it can be filled up. For example, an heterostructure of graphene (G) sandwiched\nin between hexagonal boron nitrides (hBN) slabs could be named 'hBN/G/hBN'.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.datetime#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'datetime',
      description: 'The date and time associated with this section.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.Datetime',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.lab_id#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'lab_id',
      description:
        'An ID string that is unique at least for the lab that produced this\ndata.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.description#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'description',
      description:
        'Any information that cannot be captured in the other fields.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        'Type of the system (atom, bulk, surface, etc.) which is determined by the normalizer.',
      type: {
        type_kind: 'enum',
        type_data: [
          '1D',
          '2D',
          'active_atom',
          'atom',
          'bulk',
          'molecule / cluster',
          'surface',
          'unavailable',
        ],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.dimensionality#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'dimensionality',
      description:
        'Dimensionality of the system: 0, 1, 2, or 3 dimensions. For atomistic systems this\nis automatically evaluated by using the topology-scaling algorithm:\n\n    https://doi.org/10.1103/PhysRevLett.118.106101.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.is_representative#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_representative',
      description:
        'If the model system section is the one representative of the computational simulation.\nDefaults to False and set to True by the `Computation.normalize()`. If set to True,\nthe `ModelSystem.normalize()` function is ran (otherwise, it is not).',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.time_step#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'time_step',
      description:
        'Specific time snapshot of the ModelSystem. The time evolution is then encoded\nin a list of ModelSystems under Computation where for each element this quantity defines\nthe time step.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.branch_label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'branch_label',
      description:
        'Label of the specific branch in the hierarchical `ModelSystem` tree.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.branch_depth#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'branch_depth',
      description:
        'Index refering to the depth of a branch in the hierarchical `ModelSystem` tree.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.bond_list#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'bond_list',
      description:
        'List of pairs of atom indices corresponding to bonds (e.g., as defined by a force field)\nwithin this atoms_group.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.composition_formula#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'composition_formula',
      description:
        'The overall composition of the system with respect to its subsystems.\nThe syntax for a system composed of X and Y with x and y components of each,\nrespectively, is X(x)Y(y). At the deepest branch in the hierarchy, the\ncomposition_formula is expressed in terms of the atomic labels.\n\nExample: A system composed of 3 water molecules with the following hierarchy\n\n                        TotalSystem\n                            |\n                        group_H2O\n                        |   |   |\n                       H2O H2O H2O\n\nhas the following compositional formulas at each branch:\n\n    branch 0, index 0: "Total_System" composition_formula = group_H2O(1)\n    branch 1, index 0: "group_H2O"    composition_formula = H2O(3)\n    branch 2, index 0: "H2O"          composition_formula = H(1)O(2)',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.elemental_composition.element#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'element',
      description: "The symbol of the element, e.g. 'Pb'.",
      type: {
        type_kind: 'enum',
        type_data: [
          'Ac',
          'Ag',
          'Al',
          'Am',
          'Ar',
          'As',
          'At',
          'Au',
          'B',
          'Ba',
          'Be',
          'Bh',
          'Bi',
          'Bk',
          'Br',
          'C',
          'Ca',
          'Cd',
          'Ce',
          'Cf',
          'Cl',
          'Cm',
          'Cn',
          'Co',
          'Cr',
          'Cs',
          'Cu',
          'Db',
          'Ds',
          'Dy',
          'Er',
          'Es',
          'Eu',
          'F',
          'Fe',
          'Fl',
          'Fm',
          'Fr',
          'Ga',
          'Gd',
          'Ge',
          'H',
          'He',
          'Hf',
          'Hg',
          'Ho',
          'Hs',
          'I',
          'In',
          'Ir',
          'K',
          'Kr',
          'La',
          'Li',
          'Lr',
          'Lu',
          'Lv',
          'Mc',
          'Md',
          'Mg',
          'Mn',
          'Mo',
          'Mt',
          'N',
          'Na',
          'Nb',
          'Nd',
          'Ne',
          'Nh',
          'Ni',
          'No',
          'Np',
          'O',
          'Og',
          'Os',
          'P',
          'Pa',
          'Pb',
          'Pd',
          'Pm',
          'Po',
          'Pr',
          'Pt',
          'Pu',
          'Ra',
          'Rb',
          'Re',
          'Rf',
          'Rg',
          'Rh',
          'Rn',
          'Ru',
          'S',
          'Sb',
          'Sc',
          'Se',
          'Sg',
          'Si',
          'Sm',
          'Sn',
          'Sr',
          'Ta',
          'Tb',
          'Tc',
          'Te',
          'Th',
          'Ti',
          'Tl',
          'Tm',
          'Ts',
          'U',
          'V',
          'W',
          'Xe',
          'Y',
          'Yb',
          'Zn',
          'Zr',
        ],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.elemental_composition.atomic_fraction#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'atomic_fraction',
      description:
        'The atomic fraction of the element in the system it is contained within.\nPer definition a positive value less than or equal to 1.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.elemental_composition.mass_fraction#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'mass_fraction',
      description:
        'The mass fraction of the element in the system it is contained within.\nPer definition a positive value less than or equal to 1.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        'Name of the specific cell section. This is typically used to easy identification of the\n`Cell` section. Possible values: "AtomicCell".',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.datetime#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'datetime',
      description: 'The date and time associated with this section.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.Datetime',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.lab_id#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'lab_id',
      description:
        'An ID string that is unique at least for the lab that produced this\ndata.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.description#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'description',
      description:
        'Any information that cannot be captured in the other fields.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.length_vector_a#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'length_vector_a',
      description: 'Length of the first basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.length_vector_b#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'length_vector_b',
      description: 'Length of the second basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.length_vector_c#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'length_vector_c',
      description: 'Length of the third basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.angle_vectors_b_c#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'angle_vectors_b_c',
      description: 'Angle between second and third basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'radian',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.angle_vectors_a_c#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'angle_vectors_a_c',
      description: 'Angle between first and third basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'radian',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.angle_vectors_a_b#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'angle_vectors_a_b',
      description: 'Angle between first and second basis vector.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'radian',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.volume#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'volume',
      description: 'Volume of a 3D real space entity.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter ** 3',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.surface_area#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'surface_area',
      description: 'Surface area of a 3D real space entity.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter ** 2',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.area#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'area',
      description: 'Area of a 2D real space entity.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter ** 2',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.length#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'length',
      description: 'Total length of a 1D real space entity.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.coordinates_system#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'coordinates_system',
      description:
        "Coordinate system used to determine the geometrical information of a shape in real\nspace. Default to 'cartesian'.",
      type: {
        type_kind: 'enum',
        type_data: ['cartesian', 'cylindrical', 'polar', 'spherical'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Representation type of the cell structure. It might be:\n    - 'original' as in origanally parsed,\n    - 'primitive' as the primitive unit cell,\n    - 'conventional' as the conventional cell used for referencing.",
      type: {
        type_kind: 'enum',
        type_data: ['conventional', 'original', 'primitive'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.cell.n_cell_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_cell_points',
      description: 'Number of cell points.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.symmetry.bravais_lattice#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'bravais_lattice',
      description:
        'Bravais lattice in Pearson notation.\n\nThe first lowercase letter identifies the\ncrystal family: a (triclinic), b (monoclinic), o (orthorhombic), t (tetragonal),\nh (hexagonal), c (cubic).\n\nThe second uppercase letter identifies the centring: P (primitive), S (face centered),\nI (body centred), R (rhombohedral centring), F (all faces centred).',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.symmetry.hall_symbol#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'hall_symbol',
      description:
        'Hall symbol for this system describing the minimum number of symmetry operations\nneeded to uniquely define a space group. See https://cci.lbl.gov/sginfo/hall_symbols.html.\nExamples:\n    - `F -4 2 3`,\n    - `-P 4 2`,\n    - `-F 4 2 3`.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.symmetry.point_group_symbol#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'point_group_symbol',
      description:
        'Symbol of the crystallographic point group in the Hermann-Mauguin notation. See\nhttps://en.wikipedia.org/wiki/Crystallographic_point_group. Examples:\n    - `-43m`,\n    - `4/mmm`,\n    - `m-3m`.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.symmetry.space_group_number#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'space_group_number',
      description:
        'Specifies the International Union of Crystallography (IUC) space group number of the 3D\nspace group of this system. See https://en.wikipedia.org/wiki/List_of_space_groups.\nExamples:\n    - `216`,\n    - `123`,\n    - `225`.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.symmetry.space_group_symbol#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'space_group_symbol',
      description:
        'Specifies the International Union of Crystallography (IUC) space group symbol of the 3D\nspace group of this system. See https://en.wikipedia.org/wiki/List_of_space_groups.\nExamples:\n    - `F-43m`,\n    - `P4/mmm`,\n    - `Fm-3m`.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.symmetry.strukturbericht_designation#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'strukturbericht_designation',
      description:
        "Classification of the material according to the historically grown and similar crystal\nstructures ('strukturbericht'). Useful when using altogether with `space_group_symbol`.\nExamples:\n    - `C1B`, `B3`, `C15b`,\n    - `L10`, `L60`,\n    - `L21`.\n\nExtracted from the AFLOW encyclopedia of crystallographic prototypes.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.symmetry.prototype_formula#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'prototype_formula',
      description:
        'The formula of the prototypical material for this structure as extracted from the\nAFLOW encyclopedia of crystallographic prototypes. It is a string with the chemical\nsymbols:\n    - https://aflowlib.org/prototype-encyclopedia/chemical_symbols.html',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.symmetry.prototype_aflow_id#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'prototype_aflow_id',
      description:
        'The identifier of this structure in the AFLOW encyclopedia of crystallographic prototypes:\n    http://www.aflowlib.org/prototype-encyclopedia/index.html',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.chemical_formula.descriptive#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'descriptive',
      description:
        'The chemical formula of the system as a string to be descriptive of the computation.\nIt is derived from `elemental_composition` if not specified, with non-reduced integer\nnumbers for the proportions of the elements.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.chemical_formula.reduced#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'reduced',
      description:
        'Alphabetically sorted chemical formula with reduced integer chemical proportion\nnumbers. The proportion number is omitted if it is 1.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.chemical_formula.iupac#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iupac',
      description:
        'Chemical formula where the elements are ordered using a formal list based on\nelectronegativity as defined in the IUPAC nomenclature of inorganic chemistry (2005):\n\n    - https://en.wikipedia.org/wiki/List_of_inorganic_compounds\n\nContains reduced integer chemical proportion numbers where the proportion number\nis omitted if it is 1.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.chemical_formula.hill#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'hill',
      description:
        'Chemical formula where Carbon is placed first, then Hydrogen, and then all the other\nelements in alphabetical order. If Carbon is not present, the order is alphabetical.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_system.model_system.chemical_formula.anonymous#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'anonymous',
      description:
        'Formula with the elements ordered by their reduced integer chemical proportion\nnumber, and the chemical species replaced by alphabetically ordered letters. The\nproportion number is omitted if it is 1.\n\nExamples: H2O becomes A2B and H2O2 becomes AB. The letters are drawn from the English\nalphabet that may be extended by increasing the number of letters: A, B, ..., Z, Aa, Ab\nand so on. This definition is in line with the similarly named OPTIMADE definition.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_method.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the model method. This is typically used to easy identification of the `ModelMethod` section.\n\nSuggested values: 'DFT', 'TB', 'GE', 'BSE', 'DMFT', 'NMR', 'kMC'.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_method.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Identifier used to further specify the type of model Hamiltonian. Example: a TB\nmodel can be 'Wannier', 'DFTB', 'xTB' or 'Slater-Koster'. This quantity should be\nrewritten to a MEnum when inheriting from this class.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_method.external_reference#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'external_reference',
      description: 'External reference to the model e.g. DOI, URL.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.model_method.numerical_settings.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        'Name of the numerical settings section. This is typically used to easy identification of the\n`NumericalSettings` section. Possible values: "KMesh", "FrequencyMesh", "TimeMesh",\n"SelfConsistency", "BasisSet".',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_levels.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_levels.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_levels.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_levels.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_levels.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_levels.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_levels.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_levels.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description: 'The value of the Fermi level.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_levels.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_levels.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_levels.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.chemical_potentials.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.chemical_potentials.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.chemical_potentials.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.chemical_potentials.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.chemical_potentials.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.chemical_potentials.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.chemical_potentials.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.chemical_potentials.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description: 'The value of the chemical potential.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.chemical_potentials.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.chemical_potentials.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.chemical_potentials.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.crystal_field_splittings.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.crystal_field_splittings.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.crystal_field_splittings.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.crystal_field_splittings.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.crystal_field_splittings.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.crystal_field_splittings.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.crystal_field_splittings.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.crystal_field_splittings.n_orbitals#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_orbitals',
      description: 'Number of orbitals in the tight-binding model.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.crystal_field_splittings.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description:
        'Value of the crystal field splittings in joules. This is the intra-orbital local contribution, i.e., the same orbital\nat the same Wigner-Seitz point (0, 0, 0).',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.crystal_field_splittings.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.crystal_field_splittings.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.crystal_field_splittings.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.hopping_matrices.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.hopping_matrices.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.hopping_matrices.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.hopping_matrices.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.hopping_matrices.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.hopping_matrices.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.hopping_matrices.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.hopping_matrices.n_orbitals#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_orbitals',
      description: 'Number of orbitals in the tight-binding model.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.hopping_matrices.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.hopping_matrices.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.hopping_matrices.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.n_bands#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_bands',
      description: 'Number of bands / eigenvalues.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description: 'Value of the electronic eigenvalues.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.spin_channel#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'spin_channel',
      description:
        'Spin channel of the corresponding electronic eigenvalues. It can take values of 0 or 1.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.highest_occupied#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'highest_occupied',
      description:
        'Highest occupied electronic eigenvalue. Together with `lowest_unoccupied`, it defines the\nelectronic band gap.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.lowest_unoccupied#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'lowest_unoccupied',
      description:
        'Lowest unoccupied electronic eigenvalue. Together with `highest_occupied`, it defines the\nelectronic band gap.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.value_contributions.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.value_contributions.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.value_contributions.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.value_contributions.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.value_contributions.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.value_contributions.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.value_contributions.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.value_contributions.n_bands#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_bands',
      description: 'Number of bands / eigenvalues.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.value_contributions.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description: 'Value of the electronic eigenvalues.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.value_contributions.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.value_contributions.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_eigenvalues.value_contributions.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_gaps.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_gaps.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_gaps.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_gaps.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the electronic band gap. This quantity is directly related with `momentum_transfer` as by\ndefinition, the electronic band gap is `'direct'` for zero momentum transfer (or if `momentum_transfer` is `None`) and `'indirect'`\nfor finite momentum transfer.\n\nNote: in the case of finite `variables`, this quantity refers to all of the `value` in the array.",
      type: {
        type_kind: 'enum',
        type_data: ['direct', 'indirect'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_gaps.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_gaps.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_gaps.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_gaps.spin_channel#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'spin_channel',
      description:
        'Spin channel of the corresponding electronic band gap. It can take values of 0 or 1.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_gaps.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description:
        'The value of the electronic band gap. This value has to be positive, otherwise it will\nprop an error and be set to None by the `normalize()` function.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_gaps.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_gaps.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_gaps.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description: 'The value of the electronic DOS.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: '1 / joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.spin_channel#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'spin_channel',
      description:
        'Spin channel of the corresponding electronic DOS. It can take values of 0 or 1.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.energies_origin#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'energies_origin',
      description:
        'Energy level denoting the origin along the energy axis, used for comparison and visualization. It is\ndefined as the `ElectronicEigenvalues.highest_occupied_energy`.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.normalization_factor#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'normalization_factor',
      description:
        'Normalization factor for electronic DOS to get a cell-independent intensive DOS. The cell-independent\nintensive DOS is as the integral from the lowest (most negative) energy to the Fermi level for a neutrally\ncharged system (i.e., the sum of `AtomsState.charge` is zero).',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.projected_dos.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.projected_dos.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.projected_dos.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.projected_dos.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.projected_dos.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.projected_dos.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.projected_dos.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.projected_dos.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description: 'The value of the electronic DOS.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: '1 / joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.projected_dos.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.projected_dos.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_dos.projected_dos.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_surfaces.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_surfaces.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_surfaces.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_surfaces.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_surfaces.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_surfaces.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_surfaces.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_surfaces.n_bands#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_bands',
      description: 'Number of bands / eigenvalues.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_surfaces.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_surfaces.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.fermi_surfaces.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.n_bands#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_bands',
      description: 'Number of bands / eigenvalues.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description: 'Value of the electronic eigenvalues.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.spin_channel#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'spin_channel',
      description:
        'Spin channel of the corresponding electronic eigenvalues. It can take values of 0 or 1.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.highest_occupied#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'highest_occupied',
      description:
        'Highest occupied electronic eigenvalue. Together with `lowest_unoccupied`, it defines the\nelectronic band gap.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.lowest_unoccupied#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'lowest_unoccupied',
      description:
        'Lowest unoccupied electronic eigenvalue. Together with `highest_occupied`, it defines the\nelectronic band gap.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.value_contributions.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.value_contributions.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.value_contributions.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.value_contributions.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.value_contributions.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.value_contributions.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.value_contributions.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.value_contributions.n_bands#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_bands',
      description: 'Number of bands / eigenvalues.',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.value_contributions.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description: 'Value of the electronic eigenvalues.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'joule',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.value_contributions.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.value_contributions.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.electronic_band_structures.value_contributions.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.permittivities.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.permittivities.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.permittivities.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.permittivities.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        'Type of permittivity which allows to identify if the permittivity depends on the frequency or not.',
      type: {
        type_kind: 'enum',
        type_data: ['dynamic', 'static'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.permittivities.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.permittivities.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.permittivities.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.permittivities.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.permittivities.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.permittivities.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.absorption_spectra.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.absorption_spectra.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.absorption_spectra.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.absorption_spectra.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.absorption_spectra.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.absorption_spectra.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.absorption_spectra.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.absorption_spectra.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description:
        'The value of the intensities of a spectral profile in arbitrary units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.absorption_spectra.axis#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'axis',
      description:
        'Axis of the absorption spectrum. This is related with the polarization direction, and can be seen as the\nprincipal term in the tensor `Permittivity.value` (see permittivity.py module).',
      type: {
        type_kind: 'enum',
        type_data: ['xx', 'yy', 'zz'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.absorption_spectra.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.absorption_spectra.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.absorption_spectra.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description:
        'The value of the intensities of a spectral profile in arbitrary units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.axis#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'axis',
      description:
        'Axis of the absorption spectrum. This is related with the polarization direction, and can be seen as the\nprincipal term in the tensor `Permittivity.value` (see permittivity.py module).',
      type: {
        type_kind: 'enum',
        type_data: ['xx', 'yy', 'zz'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.xanes_spectrum.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.xanes_spectrum.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.xanes_spectrum.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.xanes_spectrum.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.xanes_spectrum.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.xanes_spectrum.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.xanes_spectrum.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.xanes_spectrum.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description:
        'The value of the intensities of a spectral profile in arbitrary units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.xanes_spectrum.axis#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'axis',
      description:
        'Axis of the absorption spectrum. This is related with the polarization direction, and can be seen as the\nprincipal term in the tensor `Permittivity.value` (see permittivity.py module).',
      type: {
        type_kind: 'enum',
        type_data: ['xx', 'yy', 'zz'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.xanes_spectrum.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.xanes_spectrum.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.xanes_spectrum.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.exafs_spectrum.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description:
        "Name of the physical property. Example: `'ElectronicBandGap'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.exafs_spectrum.iri#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'iri',
      description:
        'Internationalized Resource Identifier (IRI) of the physical property defined in the FAIRmat\ntaxonomy, https://fairmat-nfdi.github.io/fairmat-taxonomy/.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.exafs_spectrum.source#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'source',
      description:
        "Source of the physical property. This quantity is related with the `Activity` performed to obtain the physical\nproperty. Example: an `ElectronicBandGap` can be obtained from a `'simulation'` or in a `'measurement'`.",
      type: {
        type_kind: 'enum',
        type_data: ['analysis', 'measurement', 'simulation'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.exafs_spectrum.type#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'type',
      description:
        "Type categorization of the physical property. Example: an `ElectronicBandGap` can be `'direct'`\nor `'indirect'`.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.exafs_spectrum.label#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'label',
      description:
        "Label for additional classification of the physical property. Example: an `ElectronicBandGap`\ncan be labeled as `'DFT'` or `'GW'` depending on the methodology used to calculate it.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.exafs_spectrum.is_derived#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_derived',
      description:
        'Flag indicating whether the physical property is derived from other physical properties. We make\nthe distinction between directly parsed and derived physical properties:\n    - Directly parsed: the physical property is directly parsed from the simulation output files.\n    - Derived: the physical property is derived from other physical properties. No extra numerical settings\n        are required to calculate the physical property.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.exafs_spectrum.is_scf_converged#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'is_scf_converged',
      description:
        'Flag indicating whether the physical property is converged or not after a SCF process. This quantity is connected\nwith `SelfConsistency` defined in the `numerical_settings.py` module.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.exafs_spectrum.value#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'value',
      description:
        'The value of the intensities of a spectral profile in arbitrary units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.exafs_spectrum.axis#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'axis',
      description:
        'Axis of the absorption spectrum. This is related with the polarization direction, and can be seen as the\nprincipal term in the tensor `Permittivity.value` (see permittivity.py module).',
      type: {
        type_kind: 'enum',
        type_data: ['xx', 'yy', 'zz'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.exafs_spectrum.variables.name#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'name',
      description: 'Name of the variable.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.exafs_spectrum.variables.n_points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'n_points',
      description: 'Number of points in which the variable is discretized.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.outputs.xas_spectra.exafs_spectrum.variables.points#nomad_simulations.schema_packages.general.Simulation':
    {
      name: 'points',
      description:
        'Points in which the variable is discretized. It might be overwritten with specific units.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_simulations.schema_packages.general.Simulation',
    },
  'data.name#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'name',
    description: 'The short name of the AI Toolkit.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.description#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'description',
    description: 'Short description of the AI Toolkit',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.date#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'date',
    description: 'The date of the last update.',
    type: {
      type_kind: 'custom',
      type_data: 'nomad.metainfo.data_type.Datetime',
    },
    aggregatable: false,
    dynamic: true,
    repeats: false,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.category#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'category',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.platform#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'platform',
    type: {
      type_kind: 'enum',
      type_data: ['Julia', 'Python', 'R', 'other'],
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.authors.first_name#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'first_name',
    description: 'First name of the author',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.authors.last_name#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'last_name',
    description: 'Last name of the author.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.methods.name#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'name',
    description: 'Specifying the name of method.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.systems.name#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'name',
    description: 'Specifying name of the system.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.references.kind#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'kind',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.references.name#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'name',
    description: 'Human readable name for the reference.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.references.description#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'description',
    description: 'Extra details about the reference.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.references.uri#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'uri',
    description: 'External URI for the reference.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.references.version#nomad_aitoolkit.schema.AIToolkitNotebook': {
    name: 'version',
    description: 'Optional field to adding version information.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_aitoolkit.schema.AIToolkitNotebook',
  },
  'data.name#simulationworkflowschema.general.SimulationWorkflow': {
    name: 'name',
    description:
      'A name of the task. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'simulationworkflowschema.general.SimulationWorkflow',
  },
  'data.x_openkim_property#simulationworkflowschema.general.SimulationWorkflow':
    {
      name: 'x_openkim_property',
      description: 'name of the property to be compared to nomad',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'simulationworkflowschema.general.SimulationWorkflow',
    },
  'data.x_openkim_nomad_rms_error#simulationworkflowschema.general.SimulationWorkflow':
    {
      name: 'x_openkim_nomad_rms_error',
      description:
        'root mean square difference of the openkim data with respect to nomad',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'simulationworkflowschema.general.SimulationWorkflow',
    },
  'data.x_openkim_nomad_std#simulationworkflowschema.general.SimulationWorkflow':
    {
      name: 'x_openkim_nomad_std',
      description: 'standard deviation of the nomad data',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'simulationworkflowschema.general.SimulationWorkflow',
    },
  'data.x_openkim_n_nomad_data#simulationworkflowschema.general.SimulationWorkflow':
    {
      name: 'x_openkim_n_nomad_data',
      description:
        'number of nomad entries with property corresponding to x_openkim_property',
      type: {
        type_kind: 'numpy',
        type_data: 'int32',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'simulationworkflowschema.general.SimulationWorkflow',
    },
  'data.inputs.name#simulationworkflowschema.general.SimulationWorkflow': {
    name: 'name',
    description:
      'Name of the link. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.SimulationWorkflow',
  },
  'data.outputs.name#simulationworkflowschema.general.SimulationWorkflow': {
    name: 'name',
    description:
      'Name of the link. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.SimulationWorkflow',
  },
  'data.tasks.name#simulationworkflowschema.general.SimulationWorkflow': {
    name: 'name',
    description:
      'A name of the task. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.SimulationWorkflow',
  },
  'data.tasks.inputs.name#simulationworkflowschema.general.SimulationWorkflow':
    {
      name: 'name',
      description:
        'Name of the link. Will be used as a label for the input or output in workflow representations.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'simulationworkflowschema.general.SimulationWorkflow',
    },
  'data.tasks.outputs.name#simulationworkflowschema.general.SimulationWorkflow':
    {
      name: 'name',
      description:
        'Name of the link. Will be used as a label for the input or output in workflow representations.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'simulationworkflowschema.general.SimulationWorkflow',
    },
  'data.results.n_calculations#simulationworkflowschema.general.SimulationWorkflow':
    {
      name: 'n_calculations',
      description: 'Number of calculations in workflow.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'simulationworkflowschema.general.SimulationWorkflow',
    },
  'data.name#simulationworkflowschema.general.ParallelSimulation': {
    name: 'name',
    description:
      'A name of the task. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'simulationworkflowschema.general.ParallelSimulation',
  },
  'data.inputs.name#simulationworkflowschema.general.ParallelSimulation': {
    name: 'name',
    description:
      'Name of the link. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.ParallelSimulation',
  },
  'data.outputs.name#simulationworkflowschema.general.ParallelSimulation': {
    name: 'name',
    description:
      'Name of the link. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.ParallelSimulation',
  },
  'data.tasks.name#simulationworkflowschema.general.ParallelSimulation': {
    name: 'name',
    description:
      'A name of the task. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.ParallelSimulation',
  },
  'data.tasks.inputs.name#simulationworkflowschema.general.ParallelSimulation':
    {
      name: 'name',
      description:
        'Name of the link. Will be used as a label for the input or output in workflow representations.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'simulationworkflowschema.general.ParallelSimulation',
    },
  'data.tasks.outputs.name#simulationworkflowschema.general.ParallelSimulation':
    {
      name: 'name',
      description:
        'Name of the link. Will be used as a label for the input or output in workflow representations.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'simulationworkflowschema.general.ParallelSimulation',
    },
  'data.results.n_calculations#simulationworkflowschema.general.ParallelSimulation':
    {
      name: 'n_calculations',
      description: 'Number of calculations in workflow.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'simulationworkflowschema.general.ParallelSimulation',
    },
  'data.name#simulationworkflowschema.general.SerialSimulation': {
    name: 'name',
    description:
      'A name of the task. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'simulationworkflowschema.general.SerialSimulation',
  },
  'data.inputs.name#simulationworkflowschema.general.SerialSimulation': {
    name: 'name',
    description:
      'Name of the link. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.SerialSimulation',
  },
  'data.outputs.name#simulationworkflowschema.general.SerialSimulation': {
    name: 'name',
    description:
      'Name of the link. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.SerialSimulation',
  },
  'data.tasks.name#simulationworkflowschema.general.SerialSimulation': {
    name: 'name',
    description:
      'A name of the task. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.SerialSimulation',
  },
  'data.tasks.inputs.name#simulationworkflowschema.general.SerialSimulation': {
    name: 'name',
    description:
      'Name of the link. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.SerialSimulation',
  },
  'data.tasks.outputs.name#simulationworkflowschema.general.SerialSimulation': {
    name: 'name',
    description:
      'Name of the link. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.SerialSimulation',
  },
  'data.results.n_calculations#simulationworkflowschema.general.SerialSimulation':
    {
      name: 'n_calculations',
      description: 'Number of calculations in workflow.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'simulationworkflowschema.general.SerialSimulation',
    },
  'data.name#simulationworkflowschema.general.BeyondDFT': {
    name: 'name',
    description:
      'A name of the task. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'simulationworkflowschema.general.BeyondDFT',
  },
  'data.inputs.name#simulationworkflowschema.general.BeyondDFT': {
    name: 'name',
    description:
      'Name of the link. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.BeyondDFT',
  },
  'data.outputs.name#simulationworkflowschema.general.BeyondDFT': {
    name: 'name',
    description:
      'Name of the link. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.BeyondDFT',
  },
  'data.tasks.name#simulationworkflowschema.general.BeyondDFT': {
    name: 'name',
    description:
      'A name of the task. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.BeyondDFT',
  },
  'data.tasks.inputs.name#simulationworkflowschema.general.BeyondDFT': {
    name: 'name',
    description:
      'Name of the link. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.BeyondDFT',
  },
  'data.tasks.outputs.name#simulationworkflowschema.general.BeyondDFT': {
    name: 'name',
    description:
      'Name of the link. Will be used as a label for the input or output in workflow representations.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'simulationworkflowschema.general.BeyondDFT',
  },
  'data.results.n_calculations#simulationworkflowschema.general.BeyondDFT': {
    name: 'n_calculations',
    description: 'Number of calculations in workflow.',
    type: {
      type_kind: 'python',
      type_data: 'int',
    },
    shape: [],
    aggregatable: false,
    dynamic: true,
    repeats: false,
    schema: 'simulationworkflowschema.general.BeyondDFT',
  },
  'data.ref.internal_sample_id#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'internal_sample_id',
      description:
        'This is your own unique cell identifier. With this text string alone, you should be able to identify this cell in your own internal data management system.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.ref.free_text_comment#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'free_text_comment',
      description:
        'This could be anything given additional description to the cell that is not captured by any other field.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.ref.ID#perovskite_solar_cell_database.schema.PerovskiteSolarCell': {
    name: 'ID',
    description: '',
    type: {
      type_kind: 'numpy',
      type_data: 'int64',
    },
    shape: [],
    aggregatable: false,
    dynamic: true,
    repeats: false,
    schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
  },
  'data.ref.ID_temp#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'ID_temp',
      description: '',
      type: {
        type_kind: 'numpy',
        type_data: 'int64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.ref.name_of_person_entering_the_data#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'name_of_person_entering_the_data',
      description: 'Your name.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.ref.data_entered_by_author#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'data_entered_by_author',
      description:
        'TRUE if you how enter the data also was involved in making the device or if you are a co-author of the paper where the data is presented.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.ref.DOI_number#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'DOI_number',
      description:
        'The DOI number referring to the published paper or dataset where the data can be found. If the data is unpublished, enter \u201cUnpublished\u201d\nExamples:\n10.1021/jp5126624\n10.1016/j.electacta.2017.06.032\nUnpublished',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.ref.lead_author#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lead_author',
      description:
        'The surname of the first author. If several authors, end with et al. If the DOI number is given correctly, this will be extracted automatically from www.crossref.org',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.ref.publication_date#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'publication_date',
      description:
        'Publication date. If the DOI number is given correctly, this will be extracted automatically from www.crossref.org',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.Datetime',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.ref.journal#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'journal',
      description: 'nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.ref.part_of_initial_dataset#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'part_of_initial_dataset',
      description: 'nan',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.ref.original_filename_data_upload#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'original_filename_data_upload',
      description: 'nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.cell.stack_sequence#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'stack_sequence',
      description:
        'The stack sequence describing the cell. Use the following formatting guidelines\n- Start with the substrate to the left and list the materials in each layer of the device\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If two materials, e.g. A and B, are mixed in one layer, list the materials in alphabetic order and separate them with  semicolons, as in (A; B)\n- The perovskite layer is stated as \u201cPerovskite\u201d, regardless of composition, mixtures, dimensionality etc. There are plenty of other fields specifically targeting the perovskite.\n- If a material is doped, or have an additive, state the pure material here and specify the doping in the columns specifically targeting the doping of those layers.\n- There is no sharp well-defined boundary between a when a material is best considered as doped to when it is best considered as a mixture of two materials. When in doubt if your material is doped or a mixture, use the notation that best capture the metaphysical essence of the situation\n- Use common abbreviations when possible but spell it out when there is risk for confusion. For consistency, please pay attention to the abbreviation specified under the headline Abbreviations found earlier in this document.\n- There are several thousand stack sequences described in the literature. Try to find your one in the list of alternatives in the data template. If it is not there (i.e. you may have done something new) define a new stack sequence according to the instructions.\nExampleBelow are the 16 most common device stacks which represent close to half of all reported devices.\nSLG | FTO | TiO2-c | TiO2-mp | Perovskite | Spiro-MeOTAD | Au\nSLG | FTO | TiO2-c | Perovskite | Spiro-MeOTAD | Au\nSLG | FTO | TiO2-c | TiO2-mp | Perovskite | Spiro-MeOTAD | Ag\nSLG | FTO | TiO2-c | Perovskite | Spiro-MeOTAD | Ag\nSLG | ITO | PEDOT:PSS | Perovskite | PCBM-60 | Al\nSLG | ITO | PEDOT:PSS | Perovskite | PCBM-60 | BCP | Ag\nSLG | ITO | PEDOT:PSS | Perovskite | PCBM-60 | Ag\nSLG | FTO | TiO2-c | TiO2-mp | Perovskite | Carbon\nSLG | FTO | TiO2-c | TiO2-mp | ZrO2-mp | Perovskite | Carbon\nSLG | FTO | SnO2-c | Perovskite | Spiro-MeOTAD | Au\nSLG | ITO | SnO2-np | Perovskite | Spiro-MeOTAD | Au\nSLG | ITO | PEDOT:PSS | Perovskite | C60 | BCP | Ag\nSLG | ITO | TiO2-c | Perovskite | Spiro-MeOTAD | Au\nSLG | FTO | TiO2-c | TiO2-mp | Perovskite | PTAA | Au\nSLG | FTO | SnO2-np | Perovskite | Spiro-MeOTAD | Au\nSLG | ITO | NiO-c | Perovskite | PCBM-60 | BCP | Ag',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.cell.area_total#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'area_total',
      description:
        'The total cell area in cm2. The total area is defined as the area that would provide photovoltaic performance when illuminated without any shading, i.e. in practice the geometric overlap between the top and bottom contact.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.cell.area_measured#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'area_measured',
      description:
        'The effective area of the cell during IV and stability measurements under illumination. If measured with a mask, this corresponds to the area of the hole in the mask. Otherwise this area is the same as the total cell area.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.cell.number_of_cells_per_substrate#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'number_of_cells_per_substrate',
      description:
        'The number of individual solar cells, or pixels, on the substrate on which the reported cell is made',
      type: {
        type_kind: 'numpy',
        type_data: 'int64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.cell.architecture#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'architecture',
      description:
        'The cell architecture with respect to the direction of current flow and the order in which layers are deposited. The two most common are nip (also referred to as normal) and pin (also referred to as inverted) but there are also a few others, e.g. Back contacted\n- nip architecture means that the electrons are collected at the substrate side. The typical example is when a TiO2 electron selective contact is deposited between the perovskite and the substrate (e.g. SLG | FTO | TiO2-c |Perovskite | \u2026)\n- pin architecture means that it instead is the holes that are collected at the substrate side. The typical example is when a PEDOT:PSS hole selective contact is deposited between the perovskite and the substrate (e.g. SLG | FTO | PEDOT:PSS |Perovskite | \u2026)',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.cell.flexible#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'flexible',
      description: 'TRUE if the cell flexible and bendable.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.cell.flexible_min_bending_radius#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'flexible_min_bending_radius',
      description:
        'The maximum bending radius possible without degrading the cells performance',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'centimeter',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.cell.semitransparent#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'semitransparent',
      description:
        'TRUE if the cell is semi-transparent, which usually is the case when there are no thick completely covering metal electrodes.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.cell.semitransparent_AVT#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'semitransparent_AVT',
      description:
        'The average visible transmittance in the wavelength range stated in the next field',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.cell.semitransparent_wavelength_range#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'semitransparent_wavelength_range',
      description:
        'the wavelength range under which the average visible transmittance is determined\nExample:\n400; 720\n350; 770',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.cell.semitransparent_raw_data#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'semitransparent_raw_data',
      description:
        'A link to where the data file for the measurement is stored\n- This is a beta feature. The plan is to create a file repository where the raw files for stability data can be stored and disseminated. With the link and associated protocols, it should be possible to programmatically access and analyse the raw data.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.module.Module#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'Module',
      description:
        'TRUE if the cell is a module composed of connected individual sub cells',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.module.number_of_cells_in_module#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'number_of_cells_in_module',
      description: 'The number of cells in the module',
      type: {
        type_kind: 'numpy',
        type_data: 'int64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.module.area_total#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'area_total',
      description:
        'The total area of the module in cm2. This includes scribes, contacts, boundaries, etc. and represent the module\u2019s geometrical footprint.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.module.area_effective#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'area_effective',
      description: 'The active area of the module in cm2.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.module.JV_data_recalculated_per_cell#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'JV_data_recalculated_per_cell',
      description:
        'The preferred way to report IV data for modules is to recalculate the IV data to average data per sub-cells in the module. That simplifies downstream comparisons, and it ensures that there is no erroneous transformation that otherwise may occur when error checking the IV data. Mark this as TRUE if the conversation is done.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.substrate.stack_sequence#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'stack_sequence',
      description:
        'The stack sequence describing the substrate.\n- With the substrate, we refer to any layer below the electron transport layer in a nip-device, and any layer below the hole transport layer in a pin-device.\n- Every layer should be separ   ated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If two materials, e.g. A and B, are mixed in one layer, list the materials in alphabetic order and separate them with semicolons, as in (A; B)\n- Use common abbreviations when appropriate but spell it out if risk for confusion.\n- There are a lot of stack sequences described in the literature. Try to find your one in the list. If it is not there (i.e. you may have done something new) define a new stack sequence according to the instructions.\nExampleBelow are some of the most common substrates\nSLG | FTO\nSLG | ITO\nPET | ITO\nPEN | ITO\nSLG | AZO\nPET | IZO',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.substrate.thickness#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'thickness',
      description:
        'A list of thicknesses of the individual layers in the stack. Use the following guidelines\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- The layers must line up with the previous filed.\n- State thicknesses in nm\n- Every layer in the stack have a thickness. If it is unknown, state this as \u2018nan\u2019\n- If there are uncertainties, state the best estimate, e.g write 100 and not 90-110\n- If you only know the total thickness, e.g. you have a 2 mm thick commercial FTO substrate and you do not know how thick the FTO layer is, state that as \u20182 | nan\u2019\nExample\n2.2 | 0.1\n2 | nan\nnan | nan | nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.substrate.area#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'area',
      description:
        'The total area in cm2 of the substrate over which the perovskite is deposited. This may be significantly larger than the cell area',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.substrate.supplier#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'supplier',
      description:
        '. The supplier of the substrate.\n- Most substrates in the perovskite field are bought commercially, but if it is made in the lab, state this as \u201clab made\u201d\n- If the supplier is unknown, stat that as\u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample:\nLab made\nNGO\nPilkington',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.substrate.brand_name#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'brand_name',
      description:
        '. The specific brand name of the substrate. e.g NGO11, TEC15, etc.\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.substrate.deposition_procedure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_procedure',
      description:
        '. A list of the deposition procedures for the substrate\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- Make sure that you describe as many layers as there are layers in the stack. Otherwise it will be difficult to interpret which layer the deposition procedure is referring to. It should thus be as many vertical bars in this field as when describing the substrate stack.\n- When more than one reaction step, separate them by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the deposition procedure for a layer unknown, state that as\u2018Unknown\u2019\n- If a substrate is bought commercially and you do not know, indicate this by the label \u201cCommercial\u201d\n- This category was included after the initial project release wherefor the list of reported purities are short, so be prepared to expand on the given list of alternatives in the extraction protocol.\nExample\nCommercial | Commercial\nCommercial | Sputtered >> Sputtered\nCommercial | ALD',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.substrate.surface_roughness_rms#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'surface_roughness_rms',
      description:
        'The root mean square value (RMS) of the surface roughness expressed in nm\n- If not known, leave this field blank',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'nanometer',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.substrate.etching_procedure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'etching_procedure',
      description:
        '. For the most common substrates, i.e. FTO and ITO it is common that part of the conductive layer is removed before perovskite deposition. State the method by which it was removed\n- If there is more than one cleaning step involved, separate the steps by a double forward angel bracket (\u2018 >> \u2018)\n- This category was included after the initial project release wherefor the list of reported purities are short, so be prepared to expand on the given list of alternatives in the extraction protocol.\nExample\nZn-powder; HCl >> Mecanical scrubbing\nLaser etching',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.substrate.cleaning_procedure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'cleaning_procedure',
      description:
        '. The schematic cleaning sequence of the substrate. The Extraction protocol does not capture the fine details in the cleaning procedures, e.g. times, temperatures, etc. but state the general sequence. Refers to the cleaning of the entire substrate before the deposition of the rest of the cell stack starts.\n- If there is more than one cleaning step involved, separate the steps by a double forward angel bracket (\u2018 >> \u2018)\n- If more than one procedure is occurring simultaneously, e.g. Soap washing an ultrasonic bath, separate simultaneously occurring steps with a semicolon.\n- This category was included after the initial project release wherefor the list of reported purities are short, so be prepared to expand on the given list of alternatives in the extraction protocol.\nExample\nHelmanex >> Ultrasonic bath >> Ethanol >> Ultrasonic bath >> Acetone >> UV-ozone\nPiranha solutionion\nPiranha solutionion >> UV-ozone\nSoap\nSoap >> Ultrasonic bath\nSoap >> Ultrasonic bath >> Ethanol; Ultrasonic bath >> Acetone >> UV-ozone\nSoap >> Ultrasonic bath >> UV-ozone\nUnknown',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.stack_sequence#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'stack_sequence',
      description:
        'The stack sequence describing the electron transport layer. Use the following formatting guidelines\n- With the ETL, we refer to any layer between the substrate and the perovskite in a nip-device, and any layer between the perovskite and the back contact in a pin-device.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If two materials, e.g. A and B, are mixed in one layer, list the materials in alphabetic order and separate them with semicolons, as in (A; B)\n- If no electron transport layer, state that as \u2018non\u2019\n- Use common abbreviations when appropriate but spell it out if risk for confusion.\n- If a material is doped, or have an additive, state the pure material here and specify the doping in the columns specifically targeting the doping of those layers.\n- There is no sharp well-defined boundary between when a material is best considered as doped or as a mixture of two materials. When in doubt if your material is best described as doped or as a mixture, use the notation that best capture the metaphysical essence of the situation.\n- There are a lot of stack sequences described in the literature. Try to find your one in the list. If it is not there (i.e. you may have done something new) define a new stack sequence according to the instructions.\nExampleBelow are some of the most common electron transport layers\nTiO2-c | TiO2-mp\nTiO2-c\nPCBM-60\nPCBM-60 | BCP\nSnO2-np\nC60 | BCP\nSnO2-c\nTiO2-c | TiO2-mp | ZrO2-mp\nZnO-c\nPCBM-60 | C60 | BCP\nPCBM-60 | LiF',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.thickness#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'thickness',
      description:
        'A list of thicknesses of the individual layers in the stack. Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- The layers must line up with the previous filed.\n- State thicknesses in nm\n- Every layer in the stack have a thickness. If it is unknown, state this as \u2018nan\u2019\n- If there are uncertainties, state the best estimate, e.g write 100 and not 90-110\nExample\n200\nnan |250\n100 | 5 | 8',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.additives_compounds#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'additives_compounds',
      description:
        'List of the dopants and additives that are in each layer of the ETL-stack\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- The layers must line up with the previous fields.\n- If several dopants/additives, e.g. A and B, are present in one layer, list the dopants/additives in alphabetic order and separate them with semicolons, as in (A; B)\n- If no dopants/additives, state that as \u201cUndoped\u201d\n- If the doping situation is unknown, stat that as\u2018Unknown\u2019\nExample\nUndoped | Li-TFSI\nTiCl4\nNb\nUndoped | Undoped | Undoped',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.additives_concentrations#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'additives_concentrations',
      description:
        'The concentration of the dopants/additives.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If more than one dopant/additive in the layer, e.g. A and B, separate the concentration for each dopant/additive with semicolons, as in (A; B)\n- For each dopant/additive in the layer, state the concentration.\n- The order of the dopants/additives must be the same as in the previous filed.\n- For layers with no dopants/additives, state this as \u2018none\u2019\n- When concentrations are unknown, state that as \u2018nan\u2019\n- Concentrations can be stated in different units suited for different situations. Therefore, specify the unit used.\n- The preferred way to state the concentration of a dopant/additive is to refer to the amount in the final product, i.e. the material in the layer. When possible, use on the preferred units\no wt%, mol%, vol%, ppt, ppm, ppb\n- When the concentration of the dopant/additive in the final product is unknown, but where the concentration of the dopant/additive in the solution is known, state that concentration instead. When possible, use on the preferred units\no M, mM, molal; g/ml, mg/ml, \u00b5g/ml\n- For values with uncertainties, state the best estimate, e.g write 4 wt% and not 3-5 wt%.\nExample\n4 wt%\n5 vol%; nan | 10 mg/ml\n0.3 mol% | 2 mol%; 0.2 wt% | 0.3 M',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_procedure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_procedure',
      description:
        'The deposition procedures for the ETL stack.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate them by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- Thermal annealing is generally not considered as an individual reaction step. The philosophy behind this is that every deposition step has a thermal history, which is specified in a separate filed. In exceptional cases with thermal annealing procedures clearly disconnected from other procedures, state \u2018Thermal annealing\u2019 as a separate reaction step.\n- Please read the instructions under \u201cPerovskite. Deposition. Procedure\u201d for descriptions and distinctions between common deposition procedures and how they should be labelled for consistency in the database.\nExample\nSpin-coating\nSpin-coating | Spin-coating\nSpray-pyrolys | Spin-coating\nEvaporation | Evaporation\nSpin-coating | Evaporation\nCBD\nSpray-pyrolys\nSpin-coating | Evaporation | Evaporation\nSpray-pyrolys >> CBD | Spin-coating >> CBD',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_aggregation_state_of_reactants#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_aggregation_state_of_reactants',
      description:
        'The physical state of the reactants\n- The three basic categories are Solid/Liquid/Gas\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the aggregation state associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- Most cases are clear cut, e.g. spin-coating involves species in solution and evaporation involves species in gas phase. For less clear-cut cases, consider where the reaction really is happening as in:\no For a spray-coating procedure, it is droplets of liquid that enters the substrate (thus a liquid phase reaction)\no For sputtering and thermal evaporation, it is species in gas phase that reaches the substrate (thus a gas phase reaction)\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nLiquid\nGas | Liquid\nLiquid | Liquid >> Liquid',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_synthesis_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_synthesis_atmosphere',
      description:
        'The synthesis atmosphere\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the atmospheres associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the synthesis atmosphere is a mixture of different gases, e.g. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nN2\nVacuum | N2\nAir | Ar; H2O >> Ar',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_synthesis_atmosphere_pressure_total#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_synthesis_atmosphere_pressure_total',
      description:
        'The total pressure during each synthesis step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the pressures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- Pressures can be stated in different units suited for different situations. Therefore, specify the unit. The preferred units are:\no atm, bar, mbar, mmHg, Pa, torr, psi\n- If a pressure is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 100 pa and not 80-120 pa.\nExample\n1 atm\n0.002 torr | 10000 Pa\n1 atm >> 1 atm | nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_synthesis_atmosphere_pressure_partial#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_synthesis_atmosphere_pressure_partial',
      description:
        'The partial pressures for the gases present during each reaction step.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the pressures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the synthesis atmosphere is a mixture of different gases, e.g. A and B, list the partial pressures and separate them with semicolons, as in (A; B). The list of partial pressures must line up with the gases they describe.\n- In cases where no gas mixtures are used, this field will be the same as the previous filed.\nExample\n1 atm\n0.002 torr | 10000 Pa\nnan >> 0.99 atm; 0.01 atm | 1 atm',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_synthesis_atmosphere_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_synthesis_atmosphere_relative_humidity',
      description:
        'The relative humidity during each deposition step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the relative humidity associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns\n- If the relative humidity for a step is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 35 and not 30-40.\nExample\n35\n0 | 20\n25 >> 25 | 0',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_solvents#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_solvents',
      description:
        'The solvents used in each deposition procedure for each layer in the stack\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvents associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the solvents in alphabetic order and separate them with semicolons, as in (A; B)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- For non-liquid processes with no solvents, state the solvent as \u2018none\u2019\n- If the solvent is not known, state this as \u2018Unknown\u2019\n- Use common abbreviations when appropriate but spell it out when risk for confusion\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nDMF\nAcetonitil; Ethanol | Ethanol\nNone | Chlorobenzene\nH2O >> H2O | Methanol',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_solvents_mixing_ratios#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_solvents_mixing_ratios',
      description:
        'The mixing ratios for mixed solvents\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent mixing ratios associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- For pure solvents, state the mixing ratio as 1\n- For non-solvent processes, state the mixing ratio as 1\n- For unknown mixing ratios, state the mixing ratio as \u2018nan\u2019\n- For solvent mixtures, i.e. A and B, state the mixing ratios by using semicolons, as in (VA; VB)\n- The preferred metrics is the volume ratios. If that is not available, mass or mol ratios can be used instead, but it the analysis the mixing ratios will be assumed to be based on volumes.\nExample\n9; 0.6; 0.4 | 1\n1 >> 1 | 1\n9; 1 | 3; 2',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_solvents_supplier#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_solvents_supplier',
      description:
        'The suppliers of all the solvents.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent suppliers associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the associated suppliers and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- For non-liquid processes with no solvents, mark the supplier as \u2018none\u2019\n- If the supplier for a solvent is unknown, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nSigma Aldrich\nSigma Aldrich; Fisher | Acros\nnone >> Sigma Aldrich; Sigma Aldrich | Unknown',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_solvents_purity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_solvents_purity',
      description:
        'The purity of the solvents used.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent purities associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the associated purities and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- Use standard nomenclature for purities, e.g. pro analysis, puris, extra dry, etc.\n- For non-liquid processes with no solvents, state the purity as \u2018none\u2019\n- If the purity for a solvent is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nPro analysis\nPuris; Puris| Tecnical\nnone >> Pro analysis; Pro analysis | Unknown',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_reaction_solutions_compounds#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_compounds',
      description:
        'The non-solvent precursor chemicals used in each reaction step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the non-solvent chemicals associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several compounds, e.g. A and B, list the associated compounds in alphabetic order and separate them with semicolons, as in (A; B)\n- Note that also dopants/additives should be included\n- When several precursor solutions are made and mixed before the reaction step, it is the properties of the final mixture used in the reaction we here describe.\n- The number and order of layers and reaction steps must line up with the previous columns.\n- For gas phase reactions, state the reaction gases as if they were in solution.\n- For solid-state reactions, state the compounds as if they were in solution.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019\n- If the compounds for a deposition step is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nTitanium diisopropoxide bis(acetylacetonate) | TiO2-np\nC60 | BCP\nTitanium diisopropoxide bis(acetylacetonate) | TiO2-np >> Li-TFSI',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_reaction_solutions_compounds_supplier#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_compounds_supplier',
      description:
        'The suppliers of the non-solvent chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the non-solvent chemical suppliers associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, e.g. A and B, list the associated suppliers and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- For gas phase reactions, state the suppliers for the gases or the targets/evaporation sources that are evaporated/sputtered/etc.\n- For solid state reactions, state the suppliers for the compounds in the same way.\n- For reaction steps involving only pure solvents, state the supplier as \u2018none\u2019 (as that that is entered in a separate filed)\n- For chemicals that are lab made, state that as \u201cLab made\u201d or \u201cLab made (name of lab)\u201d\n- If the supplier for a compound is unknown, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nDysole; Sigma Aldrich; Dyenamo; Sigma Aldrich\nSigma Aldrich; Fisher | Acros\nLab made (EPFL) | Sigma Aldrich >> none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_reaction_solutions_compounds_purity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_compounds_purity',
      description:
        'The purity of the non-solvent chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the compound purities associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, i.e. A and B, list the associated purities and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- Use standard nomenclature for purities, e.g. pro analysis, puris, extra dry, etc.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019 (as that is stated in another field)\n- If the purity for a compound is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nPro analysis\n99.999; Puris| Tecnical\nUnknown >> Pro analysis; Pro analysis | none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_reaction_solutions_concentrations#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_concentrations',
      description:
        'The concentration of the non-solvent precursor chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the concentrations associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, e.g. A and B, list the associated concentrations and separate them with semicolons, as in (A; B)\n- The order of the compounds must be the same as in the previous filed.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019\n- When concentrations are unknown, state that as \u2018nan\u2019\n- Concentrations can be stated in different units suited for different situations. Therefore, specify the unit used. When possible, use one of the preferred units\no M, mM, molal; g/ml, mg/ml, \u00b5g/ml, wt%, mol%, vol%, ppt, ppm, ppb\n- For values with uncertainties, state the best estimate, e.g write 4 wt% and not 3-5 wt%.\nExample\n4 wt%\n0.2 M; 0.15 M| 10 mg/ml\n0.3 mol% | 2 mol%; 0.2 wt% | nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_reaction_solutions_volumes#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_volumes',
      description:
        'The volume of the reaction solutions\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the volumes associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The volumes refer the volumes used, not the volume of the stock solutions. Thus if 0.15 ml of a solution is spin-coated, the volume is 0.15 ml\n- For reaction steps without solvents, state the volume as \u2018nan\u2019\n- When volumes are unknown, state that as \u2018nan\u2019\nExample\n0.1\n0.1 >> 0.05 | 0.05\nnan | 0.15',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_reaction_solutions_age#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_age',
      description:
        'The age of the solutions\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the age of the solutions associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- As a general guideline, the age refers to the time from the preparation of the final precursor mixture to the reaction procedure.\n- When the age of a solution is not known, state that as \u2018nan\u2019\n- For reaction steps where no solvents are involved, state this as \u2018nan\u2019\n- For solutions that is stored a long time, an order of magnitude estimate is adequate.\nExample\n2\n0.25 |1000 >> 10000\nnan | nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_reaction_solutions_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_temperature',
      description:
        'The temperature of the reaction solutions.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the temperatures of the solutions associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a reaction solution undergoes a temperature program, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons, e.g. 25; 100\n- When the temperature of a solution is unknown, state that as \u2018nan\u2019\n- For reaction steps where no solvents are involved, state the temperature of the gas or the solid if that make sense. Otherwise state this as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- Assume an undetermined room temperature to be 25\nExample\n25\n100; 50 | 25\nnan | 25 >> 25',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_substrate_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_substrate_temperature',
      description:
        'The temperature of the substrate.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the temperatures of the substrates (i.e. the last deposited layer) associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The temperature of the substrate refers to the temperature when the deposition of the layer is occurring.\n- If a substrate undergoes a temperature program before the deposition, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons (e.g. 25; 100)\n- When the temperature of a substrate is not known, state that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- Assume that an undetermined room temperature is 25\nExample\n125; 325; 375; 450 | 25 >> 25\n100\nnan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_thermal_annealing_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_thermal_annealing_temperature',
      description:
        'The temperatures of the thermal annealing program associated with depositing the layers\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the annealing temperatures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the thermal annealing involves a temperature program with multiple temperature stages, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons (e.g. 25; 100)\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- If no thermal annealing is occurring after the deposition of a layer, state that by stating the room temperature (assumed to 25\u00b0C if not further specified)\n- If the thermal annealing program is not known, state that by \u2018nan\u2019\nExample\n450 | 125; 325; 375; 450 >> 125; 325; 375; 450\n50 | 25\nnan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_thermal_annealing_time#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_thermal_annealing_time',
      description:
        'The time program associated to the thermal annealing program.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the annealing times associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the thermal annealing involves a temperature program with multiple temperature stages, list the associated times at those temperatures and separate them with semicolons.\n- The annealing times must align in terms of layers\u00b8 reaction steps and annealing temperatures in the previous filed.\n- If a time is not known, state that by \u2018nan\u2019\n- If no thermal annealing is occurring after the deposition of a layer, state that by \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 20 and not 10-30.\nExample\n30 | 5; 5; 5; 30 >> 5; 5; 5; 30\n60 | 1000\nnan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.deposition_thermal_annealing_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_thermal_annealing_atmosphere',
      description:
        'The atmosphere during thermal annealing\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the atmospheres associated to each annealing step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the atmosphere is a mixture of different gases, i.e. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas.\n- This is often the same as the atmosphere under which the deposition is occurring, but not always.\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nN2\nVacuum | N2\nAir | Ar >> Ar',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.storage_time_until_next_deposition_step#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_time_until_next_deposition_step',
      description:
        'The time between the HTL stack is finalised and the next layer is deposited\n- If there are uncertainties, only state the best estimate, e.g. write 35 and not 20-50.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.storage_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_atmosphere',
      description:
        'The atmosphere in which the sample with the finalised HTL stack is stored until the next deposition step.\nExample\nAir\nN2\nVacuum',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.storage_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_relative_humidity',
      description:
        'The relive humidity under which the sample with the finalised HTL stack is stored until next deposition step\n- If there are uncertainties, only state the best estimate, e.g. write 35 and not 20-50.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.etl.surface_treatment_before_next_deposition_step#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'surface_treatment_before_next_deposition_step',
      description:
        'Description of any type of surface treatment or other treatment the sample with the finalised ETL-stack undergoes before the next deposition step.\n- If more than one treatment, list the treatments and separate them by a double forward angel bracket (\u2018 >> \u2018)\n- If no special treatment, state that as \u2018none\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample:\nnone\nAr plasma',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.single_crystal#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'single_crystal',
      description: 'TRUE if the cell is based on a perovskite single crystal',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.dimension_0D#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'dimension_0D',
      description:
        'TRUE if the cell is based on a perovskite quantum dots. Perovskite nanoparticle architectures can also be counted here unless they more have the characteristics of a standard polycrystalline cell',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.dimension_2D#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'dimension_2D',
      description:
        'TRUE if the cell is based on 2D perovskites, i.e. a layered perovskite with a large A-cation',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.dimension_2D3D_mixture#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'dimension_2D3D_mixture',
      description:
        'TRUE if the cell is based on a mixture of 2D and 3D perovskites. This is sometimes referred to as reduced dimensional perovskites (but not as reduced as to be a pure 2D perovskite)',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.dimension_3D#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'dimension_3D',
      description:
        'TRUE for standard three-dimensional perovskites with ABC3 structures. TRUE also for the case where the bulk of the perovskite is 3D but where there exist a thin 2D-caping layer',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.dimension_3D_with_2D_capping_layer#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'dimension_3D_with_2D_capping_layer',
      description:
        'TRUE if the bulk of the perovskite layer is 3D but there is a top layer with lower dimensionality.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.dimension_list_of_layers#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'dimension_list_of_layers',
      description:
        'A list of the perovskite dimensionalities\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- In most cases, there will be only one layer\n- For a perovskite that is a mixture of a 2D and a 3D phase, mark this is as2.5\nExample\n3\n3 | 2\n0',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_perovskite_ABC3_structure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_perovskite_ABC3_structure',
      description:
        'TRUE if the photo-absorber has a perovskite structure\n- The typical perovskite has an ABC3 structure and that is clearly a TRUE\n- This category is inclusive in the sense that also 2D perovskite analogues should be labelled as TRUE',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_perovskite_inspired_structure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_perovskite_inspired_structure',
      description:
        'TRUE if the photo absorber does not have a perovskite structure. In the literature we sometimes see cells based on non-perovskite photo absorbers, but which claims to be \u201cperovskite inspired\u201d regardless if the crystal structure has any resemblance to the perovskite ABC3 structure or not. This category is for enabling those cells to easily be identified and filtered.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_a_ions#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_a_ions',
      description:
        'List of the A-site ions in the perovskite structure\n- We have experimented with letting users write the perovskite structure and from that extract ions and coefficients. Due to the multitude of formatting variations, that has not worked out very well, wherefor we now define the perovskite ion by ion.\n- List all the A-site ions in alphabetic order and separate them by semicolons\n- For ions which labels are three characters or longer, enclose them in parenthesis. That improves readability and simplifies downstream data treatment.\n- In case of a layered perovskite structure, separate layers by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- Only include ions that go into the perovskite structure. Ions that only are found in secondary phases, or amorphous grain boundaries, or that disappears during synthesis, should instead be added as dopants/additives in the field dedicated to dopants and additives.\no On example is Rb in MAFAPbBrI-perovskites. As far as we know, Rb does not go into the perovskite structure, even if that was believed to be the case in the beginning, but rather form secondary phases. For MAFAPbBrI-perovskites, Rb should thus not be considered as a A-site cation, but as a dopant/additive.\nExample:\nMA\nFA; MA\nCs; FA; MA\n(5-AVA); MA\nCs; FA; MA | (PEA)',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_a_ions_coefficients#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_a_ions_coefficients',
      description:
        'A list of the perovskite coefficients for the A-site ions\n- The list of coefficients must line up with the list of the A-site ions\n- If a coefficient is unknown, state that with an \u2018x\u2019\n- If there are uncertainties in the coefficient, only state the best estimate, e.g. write 0.4 and not 0.3-0.5.\n- A common notation is \u20181-x\u2019. Write that as x\n- If the coefficients are not known precisely, a good guess is worth more than to state that we have absolutely no idea.\nExamples:\n1\n0.83; 0.17\n0.05; 0.79; 0.16\n1.5; 0.5',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_b_ions#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_b_ions',
      description:
        'List of the B-site ions in the perovskite structure\n- We have experimented with letting users write the perovskite structure and from that extract ions and coefficients. Due to the multitude of formatting variations, that has not worked out very well, wherefor we now define the perovskite ion by ion.\n- List all the B-site ions in alphabetic order and separate them by semicolons\n- In case of a layered perovskite structure, separate layers by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- Only include ions that go into the perovskite structure. Ions that only are found in secondary phases, or amorphous grain boundaries, or that disappears during synthesis, should instead be added as dopants/additives in the field dedicated to dopants and additives.\nExample:\nPb\nSn\nPb; Sn\nBi\nPb | Pb',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_b_ions_coefficients#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_b_ions_coefficients',
      description:
        'A list of the perovskite coefficients for the B-site ions\n- The list of coefficients must line up with the list of the B-site ions\n- If a coefficient is unknown, mark that with an \u2018x\u2019\n- If there are uncertainties in the coefficient, only state the best estimate, e.g. write 0.4 and not 0.3-0.5.\n- A common notation is \u20181-x\u2019. Write that as x\n- If the coefficients are not known precisely, a good guess is worth more than to state that we have absolutely no idea.\nExamples:\n1\n0.83; 0.17\nx; x\n0.5; 0.5 | 1',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_c_ions#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_c_ions',
      description:
        'List of the C-site ions in the perovskite structure\n- We have experimented with letting users write the perovskite structure and from that extract ions and coefficients. Due to the multitude of formatting variations, that has not worked out very well, wherefor we now define the perovskite ion by ion.\n- List all the A-site ions in alphabetic order and separate them by semicolons\n- For ions which labels are three characters or longer, enclose them in parenthesis. That improves readability and simplifies downstream data treatment.\n- In case of a layered perovskite structure, separate layers by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- Only include ions that go into the perovskite structure. Ions that only are found in secondary phases, or amorphous grain boundaries, or that disappears during synthesis, should instead be added as dopants/additives in the field dedicated to dopants and additives.\no One example is chloride in MAPbI3. As far as we know, Cl does not go into the perovskite structure even if that was believed to be the case in the beginning. For MAPbI3 Cl should thus not be considered as a C-site cation, but as a dopant/additive.\nExample:\nI\nBr; I\nBr\nBr; I| I',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_c_ions_coefficients#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_c_ions_coefficients',
      description:
        'A list of the perovskite coefficients for the C-site ions\n- The list of coefficients must line up with the list of the C-site ions\n- If a coefficient is unknown, mark that with an \u2018x\u2019\n- If there are uncertainties in the coefficient, only state the best estimate, e.g. write 0.4 and not 0.3-0.5.\n- A common notation is \u20181-x\u2019. Write that as x\n- If the coefficients are not known precisely, a good guess is worth more than to state that we have absolutely no idea.\nExamples:\n3\n0.51; 2.49\n0.51; 2.49 | x',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_none_stoichiometry_components_in_excess#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_none_stoichiometry_components_in_excess',
      description:
        'Components that are in excess in the perovskite synthesis. E.g. to form stoichiometric MAPbI3, PbI2 and MAI are mixed in the proportions 1:1. If one of them are in excess compared to the other, then that component is considered to be in excess. This information can be inferred from data entered on the concentration for all reaction solutions but this gives a convenient shorthand filtering option.\n- If more than one component is in excess, order them in alphabetic order and separate them by semicolons.\n- It there are no components that are in excess, write Stoichiometric\nExamples:\nPbI2\nMAI\nStoichiometric',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_short_form#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_short_form',
      description: 'nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_long_form#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_long_form',
      description: 'nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_assumption#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_assumption',
      description:
        'The knowledge base from which the perovskite composition is inferred. Is the assumed perovskite composition based on the composition of the precursor solutions and the assumption that the final perovskite will have the same composition (i.e. Solution composition), or is it based on literature claims (i.e. Literature) or has it been experimentally verified with some technique, e.g. XRD, EDX, XRF, etc.?',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_inorganic#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_inorganic',
      description: 'TRUE if the perovskite does not contain any organic ions.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.composition_leadfree#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'composition_leadfree',
      description: 'TRUE if the perovskite is completely lead free.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.additives_compounds#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'additives_compounds',
      description:
        'List of the dopants and additives that are in the perovskite\n- If the perovskite is layered (e.g. 3D perovskite with a 2D caping layer), separate the layers by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If several dopants/additives, e.g. A and B, are present in one layer, list the dopants/additives in alphabetic order and separate them with semicolonsas in (A; B)\n- If no dopants/additives, state that as \u201cUndoped\u201d\n- If the doping situation is unknown, stat that as\u2018Unknown\u2019\n- Include any non-solvent that does not go into the perovskite structure. This includes compounds that are found in secondary phases, or amorphous grain boundaries, or that disappears during synthesis.\no One example is Rb in MAFAPbBrI-perovskites. As far as we know, Rb does not go into the perovskite structure, even if that was believed to be the case in the beginning, but rather form secondary phases. For MAFAPbBrI-perovskites, Rb should thus not be considered as a A-site cation, but as a dopant/additive.\no One other example is chloride in MAPbI3. As far as we know, Cl does not go into the perovskite structure even if that was believed to be the case in the beginning. For MAPbI3 Cl should thus not be considered as a C-site cation, but as a dopant/additive.\nExample\nCl\nUndoped\n5-AVAI\nSnF2\nAg; Cl; rGO\nRb',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.additives_concentrations#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'additives_concentrations',
      description:
        'The concentration of the dopants/additives.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If more than one dopant/additive in the layer, e.g. A and B, separate the concentration for each dopant/additive with semicolons, as in (A; B)\n- For each dopant/additive in the layer, state the concentration.\n- The order of the dopants/additives must be the same as in the previous filed.\n- For layers with no dopants/additives, state this as \u2018none\u2019\n- When concentrations are unknown, state that as \u2018nan\u2019\n- Concentrations can be stated in different units suited for different situations. Therefore, specify the unit used.\n- The preferred way to state the concentration of a dopant/additive is to refer to the amount in the final product, i.e. the material in the layer. When possible, use on the preferred units\no wt%, mol%, vol%, ppt, ppm, ppb\n- When the concentration of the dopant/additive in the final product is unknown, but where the concentration of the dopant/additive in the solution is known, state that concentration instead. When possible, use on the preferred units\no M, mM, molal; g/ml, mg/ml, \u00b5g/ml\n- For values with uncertainties, state the best estimate, e.g write 4 wt% and not 3-5 wt%.\nExample\n4 wt%\n5 vol%; nan | 10 mg/ml\n0.3 mol% | 2 mol%; 0.2 wt% | 0.3 M',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.thickness#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'thickness',
      description:
        'The thickness of the perovskite layer\n- If the perovskite contains more than one layer, separate those by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- State thicknesses in nm\n- Every layer in the stack have a thickness. If it is unknown, state this as \u2018nan\u2019\n- If there are uncertainties, state the best estimate, e.g write 100 and not 90-110\n- For cells where the perovskite infiltrates a mesoporous scaffold, state the thickness as starting from the bottom of the infiltrated mesoporous layer to the top of the perovskite layer (i.e. include the thickness of the infiltrated mesoporous layer)\nExample\n200\n500 |20\n600 | nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.band_gap#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'band_gap',
      description:
        'The band gap of the perovskite\n- If the perovskite contains more than one layer, separate the band gaps for the respective layer by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If there are uncertainties, state the best estimate, e.g. write 1.62 and not 1.6-1.64\nExample\n1.62\n1.57 | 2.3',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.band_gap_graded#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'band_gap_graded',
      description:
        'TRUE if the band gap varies as a function of the vertical position in the perovskite layer.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.band_gap_estimation_basis#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'band_gap_estimation_basis',
      description:
        'The method by which the band gap was estimated. The band gap can be estimated from absorption data, EQE-data, UPS-data, or it can be estimated based on literature values for the recipe, or it could be inferred from the composition and what we know of similar but not identical compositions.\nExample\nAbsorption Tauc-plot\nLiterature\nComposition',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.pl_max#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'pl_max',
      description:
        'The maximum from steady-state PL measurements\n- If more than one PL-max, separate those by a semicolon\nExample\n780\n550; 770',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.storage_time_until_next_deposition_step#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_time_until_next_deposition_step',
      description:
        'The time between the perovskite stack is finalised and the next layer is deposited\n- If there are uncertainties, state the best estimate, e.g. write 35 and not 20-50.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.storage_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_atmosphere',
      description:
        'The atmosphere in which the sample with the finalised perovskite stack is stored until the next deposition step.\nExample\nAir\nN2\nVacuum',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.storage_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_relative_humidity',
      description:
        'The time between the perovskite stack is finalised and the next layer is deposited\n- If there are uncertainties, state the best estimate, e.g write 35 and not 20-50.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.surface_treatment_before_next_deposition_step#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'surface_treatment_before_next_deposition_step',
      description:
        'Description of any type of surface treatment or other treatment the sample with the finalised perovskite stack undergoes before the next deposition step.\n- If more than one treatment, list the treatments and separate them by a double forward angel bracket (\u2018 >> \u2018)\n- If no special treatment, state that as \u2018none\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExamples:\nnone\nUV\nOzone',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.name#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'name',
      description: 'A short name for the substance.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.iupac_name#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'iupac_name',
      description: 'IUPAC name.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.molecular_formula#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'molecular_formula',
      description: 'Molecular formula.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.molecular_mass#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'molecular_mass',
      description:
        'The mass of the most likely isotopic composition for a single molecule,\ncorresponding to the most intense ion/molecule peak in a mass spectrum.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'dalton',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.molar_mass#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'molar_mass',
      description:
        'The molar mass is the sum of all atomic masses of the constituent atoms in a\ncompound, measured in g/mol. In the absence of explicit isotope labelling,\naveraged natural abundance is assumed. If an atom bears an explicit isotope label,\n100%% isotopic purity is assumed at this location.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'gram / mole',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.monoisotopic_mass#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'monoisotopic_mass',
      description:
        'The mass of a molecule, calculated using the mass of the most abundant isotope of\neach element.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'dalton',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.inchi#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'inchi',
      description: 'Inchi.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.inchi_key#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'inchi_key',
      description: 'Inchi key.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.smile#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'smile',
      description: 'Smile.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.canonical_smile#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'canonical_smile',
      description: 'Canonical smile.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.cas_number#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'cas_number',
      description: 'CAS number.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.ion_type#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'ion_type',
      description: 'Type of the ion.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.common_name#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'common_name',
      description: 'Common name.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.common_source_compound#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'common_source_compound',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.source_compound_cas#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'source_compound_cas',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.source_compound_formula#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'source_compound_formula',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite.ions.coefficients#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'coefficients',
      description: 'Coefficients for the ion.',
      type: {
        type_kind: 'python',
        type_data: 'float',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.number_of_deposition_steps#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'number_of_deposition_steps',
      description:
        'The number of production steps involved in making the perovskite-stack\n- A spin coating program that are composed of several segments with different spin speed are still counted as one step (1)\n- A spin coating program involving an antisolvent step counts as a 1-step method (1).\n- Depositing PbI2 first and subsequently converting it to a perovskite count as a 2-step procedure (2)\n- Thermal annealing is considered separately. The motivation for this is that every step is considered to have its own thermal history.',
      type: {
        type_kind: 'numpy',
        type_data: 'int64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.procedure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'procedure',
      description:
        'The deposition procedures for the perovskite block.\n- The perovskite stack is considered as one block/layer when we consider the synthesis. Thus, even if the perovskite is layered, consider it as one block, i.e. no vertical bars in this field\n- When more than one reaction step, separate them by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- There should be as many reaction steps described here as indicated in the field \u201cPerovskite. Deposition. Number of deposition steps\u201d\n- Thermal annealing is generally not considered as an individual reaction step. The philosophy behind this is that every deposition step has a thermal history, which is specified in a separate filed. In exceptional cases with thermal annealing procedures clearly disconnected from other procedures, state \u2018Thermal annealing\u2019 as a separate reaction step.\n- Antisolvent treatment is considered in a separate filed. The motivation for that is that it usually is conducted simultaneously as a spin-coating procedure, and thus acts as an additional aspect of reaction step already accounted for. Exception to this is if there is an antisolvent step that is distinctly separated in time, e.g. a film with a spin-coated perovskite solution is immersed in an antisolvent. In that case, this could eb added as a dipp-coating event, while also being reported in the antisolvent field.\n- Even if the most common deposition procedures have been used for 95 % of all reported devise, do not be surprised if you do not find your deposition procedure in the list of reported deposition procedure, as the original dataset tended to use a simplified notation.\n- A few clarifications\n- Air brush spray\n- Deposition with something looking like an old perfume bottle. Classified as a solution technique.\n- Brush painting\no A precursor ink is applied with a brush\n- CBD\n- Chemical bath deposition. Refers to procedures where a film has been immersed in a solution where a reaction occurs. The typical example is when a PbI2 film is immerse in an IPA solution with MAI in which the PbI2 is converted to the perovskite.\n- Co-evaporation\n- Simultaneous evaporation from multiple sources with line of sight deposition.\n- CVD\no Chemical vapour deposition. A gas phase process where a chemical reaction is occurring in the gas phase. If a MA-containing compound is evaporated and reacted with PbI2 where another species is released to the gas phase, it is labeled as CVD. A process where MAI in gas phase react with PbI2 in gas phase is labelled as CVD. A process where MAI or MA gas is reacting with solid PbI2 is instead labelled as a gas reaction as no chemical reaction is occurring the gas phase. Note that all reactions labelled as CVD in the literature may not be CVD even if it is conducted in a CVD reactor, and should instead be labelled as a gas reaction.\n- Diffusion\no Solid state reaction where two solid components are mixed. E.g. solid MAI is bought in direct contact with solid PbI2\n- Diffusion-gas reaction\n- A special case. Where one compound, e.g. MAI is placed on top of another e.g. PbI2 where it is evaporated. It is thus a combination of a gas phase reaction and solid-solid diffusion.\n- Dipp-coating\no The thing that separates dipp-coating from CBD is the occurrence of a reaction. If you have component A in solution, dip your substrate in the solution, take it up and you have component A on your substrate, then you have done a dipp-coating. If you have substance A in solution, dip your substrate in the solution, take it up and have something else than A on your substrate, you have done a CBD (e.g. PbI2 dipped in MAI/IPA which gives MAPbI3 and not MAI on the substrate)\n- Dropcasting\no A drop is applied to a substrate where it is left to dry without any additional procedures.\n- Drop-infiltration\n- A mesoporous scaffold in which a drop of the precursor solution is infiltrated without the aid of spin-coating.\n- Doctor blading\n- There is a family of related techniques, but if it could be described as doctor blading, that is the label to use.\n- Evaporation\n- Refers to thermal evaporation with line-of-sigh deposition. i.e. PVD\n- Flash evaporation\n- Fast evaporation (in a flash) of a perovskite that sublimes on another substrate. Line of sight deposition.\n- Closed space sublimation\n- Evaporation of a well controlled amount of substance (usually in the form of a thin film) in a small container containing the final substrate.\n- Gas reaction\n- A gas phase reaction. Not a line of sight deposition. In the typical case, MAI is evaporated and the MAI gas builds up a pressure in the reaction chamber in which it reacts with a PbI2 film forming the perovskite.\n- Ion exchange\n- One perovskite is dipped into a solution (or exposed to a gas) which leads to an ion exchange, e.g. I is replaced by Br.\n- Lamination\n- A readymade film is transferred directly to the device stack. A rather broad concept. An everyday kitchen related example of lamination would eb to place a thin plastic film over a slice of pie.\n- Recrystallization\n- A perovskite that already have been formed is deformed and then recrystallised. E.g. MAPbI3 is exposed to Methylamine gas for a short while which dissolved the perovskite which then can crystallize again\n- Rinsing\n- Cleaning step with a solvent\n- Sandwiching\n- When a readymade top stack simply is placed on top of the device stack. Could be held together with clams.\n- Ultrasonic spray\n- A bit like air brush spray but with better control of droplet size. Classified as a solution technique.\nExample\nSpin-coating\nSpin-coating >> Spin-coating\nSpin-coating >> CBD\nSpin-coating >> Gas reaction\nDrop-infiltration\nCo-evaporation\nDoctor blading\nEvaporation >> Evaporation\nEvaporation >> Spin-coating\nEvaporation >> Gas reaction\nSlot-die coating\nSpray-coating',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.aggregation_state_of_reactants#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'aggregation_state_of_reactants',
      description:
        'The physical state of the reactants\n- The three basic categories are Solid/Liquid/Gas\n- The perovskite stack is considered as one block/layer when we consider the synthesis. Thus, even if the perovskite is layered, consider it as one block, i.e. no vertical bars in this field\n- When more than one reaction step, separate the aggregation state associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- Most cases are clear cut, e.g. spin-coating involves species in solution and evaporation involves species in gas phase. For less clear-cut cases, consider where the reaction really is happening as in:\no For a spray-coating procedure, it is droplets of liquid that enters the substrate (thus a liquid phase reaction)\no For sputtering and thermal evaporation, it is species in gas phase that reaches the substrate (thus a gas phase reaction)\nExample\nLiquid\nGas >> Liquid\nLiquid >> Liquid >> Liquid',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.synthesis_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'synthesis_atmosphere',
      description:
        'The synthesis atmosphere\n- When more than one reaction step, separate the atmospheres associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order and deposition steps must line up with the previous columns.\n- If the synthesis atmosphere is a mixture of different gases, e.g. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nN2\nAir\nN2 >> N2\nVacuum',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.synthesis_atmosphere_pressure_total#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'synthesis_atmosphere_pressure_total',
      description:
        'The total gas pressure during each reaction step\n- When more than one reaction step, separate the pressures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of deposition steps must line up with the previous columns.\n- Pressures can be stated in different units suited for different situations. Therefore, specify the unit. The preferred units are:\no atm, bar, mbar, mmHg, Pa, torr, psi\n- If a pressure is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 100 pa and not 80-120 pa.\nExample\n1 atm\n0.002 torr\n1 atm >> 1 atm >> nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.synthesis_atmosphere_pressure_partial#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'synthesis_atmosphere_pressure_partial',
      description:
        'The partial pressures for the gases present during each reaction step.\n- When more than one reaction step, separate the pressures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of deposition steps must line up with the previous columns.\n- If the synthesis atmosphere is a mixture of different gases, e.g. A and B, list the partial pressures and separate them with semicolons, as in (A; B). The list of partial pressures must line up with the gases they describe.\n- In cases where no gas mixtures are used, this field will be the same as the previous filed.\nExample\n1 atm\n0.002 torr; 0.03 torr\n0.8 atm; 0.2 atm >> 1 atm >> nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.synthesis_atmosphere_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'synthesis_atmosphere_relative_humidity',
      description:
        'The relative humidity during each deposition step\n- When more than one reaction step, separate the relative humidity associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of deposition steps must line up with the previous columns\n- If the relative humidity for a step is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 35 and not 30-40.\nExample\n35\n0 >> 20\n25 >> 25 >> 0',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.solvents#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'solvents',
      description:
        'The solvents used in each deposition procedure for each layer in the stack\n- When more than one reaction step, separate the solvents associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the solvents in alphabetic order and separate them with semicolons, as in (A; B)\n- The number and order of deposition steps must line up with the previous columns.\n- For non-liquid processes with no solvents, state the solvent as \u2018none\u2019\n- If the solvent is not known, state this as \u2018Unknown\u2019\n- Use common abbreviations when appropriate but spell it out when risk for confusion\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nDMF; DMSO\nGBL\nDMF >> IPA\nDMF >> none\nDMF; DMSO >> IPA',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.solvents_mixing_ratios#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'solvents_mixing_ratios',
      description:
        'The mixing ratios of the solvents used in each deposition procedure for each layer in the stack\n- When more than one reaction step, separate the solvent mixing ratios associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of deposition steps must line up with the previous columns.\n- For pure solvents, state the mixing ratio as 1\n- For non-solvent processes, state the mixing ratio as 1\n- For unknown mixing ratios, state the mixing ratio as \u2018nan\u2019\n- For solvent mixtures, i.e. A and B, state the mixing ratios by using semicolons, as in (VA; VB)\n- The preferred metrics is the volume ratios. If that is not available, mass or mol ratios can be used instead, but it the analysis the mixing ratios will be assumed to be based on volumes.\nExample\n1\n4; 1 >> 1\n1 >> 5; 2; 0.3 >> 2; 1',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.solvents_supplier#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'solvents_supplier',
      description:
        'The suppliers of all the solvents.\n- When more than one reaction step, separate the solvent suppliers associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the associated suppliers and separate them with semicolons, as in (A; B)\n- The number and order of reaction steps and solvents must line up with the previous columns.\n- For non-liquid processes with no solvents, mark the supplier as \u2018none\u2019\n- If the supplier for a solvent is unknown, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nSigma Aldrich\nSigma Aldrich; Fisher >> Acros\nnone >> Sigma Aldrich; Sigma Aldrich >> Unknown',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.solvents_purity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'solvents_purity',
      description:
        'The purity of the solvents used.\n- When more than one reaction step, separate the solvent purities associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the associated purities and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- Use standard nomenclature for purities, e.g. pro analysis, puris, extra dry, etc.\n- For non-liquid processes with no solvents, state the purity as \u2018none\u2019\n- If the purity for a solvent is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nPro analysis\nPuris; Puris>> Tecnical\nnone >> Pro analysis; Pro analysis >> Unknown',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.reaction_solutions_compounds#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reaction_solutions_compounds',
      description:
        'The non-solvent precursor chemicals used in each deposition procedure\n- When more than one reaction step, separate the non-solvent chemicals associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several compounds, e.g. A and B, list the associated compounds in alphabetic order and separate them with semicolons, as in (A; B)\n- Note that also dopants/additives should be included\n- When several precursor solutions are made and mixed before the reaction step, it is the properties of the final mixture used in the reaction we here describe.\n- The number and order of reaction steps must line up with the previous columns.\n- For gas phase reactions, state the reaction gases as if they were in solution.\n- For solid-state reactions, state the compounds as if they were in solution.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019\n- If the compounds for a deposition step is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nCsI; FAI; MAI; PbBr2; PbI2\nPbI2 >> MAI\nPbBr2; PbI2 >> FAI; MAI >> none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.reaction_solutions_compounds_supplier#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reaction_solutions_compounds_supplier',
      description:
        'The suppliers of the non-solvent chemicals.\n- When more than one reaction step, separate the non-solvent chemical suppliers associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, e.g. A and B, list the associated suppliers and separate them with semicolons, as in (A; B)\n- The number and order of reaction steps and solvents must line up with the previous columns.\n- For gas phase reactions, state the suppliers for the gases or the targets/evaporation sources that are evaporated/sputtered/etc.\n- For solid state reactions, state the suppliers for the compounds in the same way.\n- For reaction steps involving only pure solvents, state the supplier as \u2018none\u2019 (as that that is entered in a separate filed)\n- For chemicals that are lab made, state that as \u201cLab made\u201d or \u201cLab made (name of lab)\u201d\n- If the supplier for a compound is unknown, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nDysole; Sigma Aldrich; Dyenamo; Sigma Aldrich\nSigma Aldrich; Fisher | Acros\nLab made (EPFL) | Sigma Aldrich >> none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.reaction_solutions_compounds_purity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reaction_solutions_compounds_purity',
      description:
        'The purity of the chemicals used.\n- When more than one reaction step, separate the compound purities associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, i.e. A and B, list the associated purities and separate them with semicolons, as in (A; B)\n- The number and order of reaction steps and solvents must line up with the previous columns.\n- Use standard nomenclature for purities, e.g. pro analysis, puris, extra dry, etc.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019 (as that is stated in another field)\n- If the purity for a compound is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nPro analysis\n99.999; Puris| Tecnical\nUnknown >> Pro analysis; Pro analysis | none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.reaction_solutions_concentrations#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reaction_solutions_concentrations',
      description:
        'The concentration of the non-solvent precursor chemicals.\n- When more than one reaction step, separate the concentrations associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of deposition steps and chemicals must line up with the previous columns.\n- If a solution contains several dissolved compounds, e.g. A and B, list the associated concentrations and separate them with semicolons, as in (A; B)\n- The order of the chemicals must line up to the chemicals in the previous column.\n- The order of the compounds must be the same as in the previous filed.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019\n- For gas phase reactions, state the concentration as \u2018none\u2019\n- For solid-state reactions, state the concentration as \u2018none\u2019\n- When concentrations are unknown, state that as \u2018nan\u2019\n- Concentrations can be stated in different units suited for different situations. Therefore, specify the unit used. When possible, use one of the preferred units\no M, mM, molal; g/ml, mg/ml, \u00b5g/ml, wt%, mol%, vol%, ppt, ppm, ppb\n- For values with uncertainties, state the best estimate, e.g write 4 wt% and not 3-5 wt%.\nExample\n0.063 M; 1.25 M; 1.25 M; 1.14 M; 1.14 M\n1.25 M; 1.25 M >> 1.14 M; 1.14 M; 10 mg/ml\n1 M; 1 M >> none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.reaction_solutions_volumes#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reaction_solutions_volumes',
      description:
        'The volume of the reaction solutions used. used in each deposition procedure\n- When more than one reaction step, separate the volumes associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The volumes refer the volumes used, not the volume of the stock solutions. Thus if 0.15 ml of a solution is spin-coated, the volume is 0.15 ml\n- For reaction steps without solvents, state the volume as \u2018nan\u2019\n- When volumes are unknown, state that as \u2018nan\u2019\nExample\n0.04\nnan >> 0.1\nnan >> 10',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.reaction_solutions_age#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reaction_solutions_age',
      description:
        'The age of the solutions used in the deposition\n- When more than one reaction step, separate the age of the solutions associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- As a general guideline, the age refers to the time from the preparation of the final precursor mixture to the reaction procedure.\n- When the age of a solution is not known, state that as \u2018nan\u2019\n- For reaction steps where no solvents are involved, state this as \u2018nan\u2019\n- For solutions that is stored a long time, an order of magnitude estimate is adequate.\nExample\n0.5\nnan >> 10\n10000 >> nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.reaction_solutions_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reaction_solutions_temperature',
      description:
        'The temperature of the reaction solutions.\n- If there is more than one reaction step involved, list the solution temperatures and separate the data for each step by a double forward angel bracket (\u2018 >> \u2018)\n- If a reaction solution undergoes a temperature program, list the temperatures (e.g. start, end, and other important points) and separate them with a semicolon (e.g. heated to 80\u00b0C and cooled to room temperature before used would be80; 25)\n- When the temperature of a solution is not known, state that as \u2018nan\u2019\n- For reaction steps where no solvents are involved, state the temperature of the gas or the solid if that make sense. Otherwise mark this with \u2018nan\u2019\n- Assume that an undetermined room temperature is 25\nExample\n25\nnan >> 50\n80; 25',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.substrate_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'substrate_temperature',
      description:
        'The temperature of the substrate on which the perovskite is deposited.\n- When more than one reaction step, separate the temperatures of the solutions associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a reaction solution undergoes a temperature program, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons, e.g. 25; 100\n- When the temperature of a solution is unknown, state that as \u2018nan\u2019\n- For reaction steps where no solvents are involved, state the temperature of the gas or the solid if that make sense. Otherwise state this as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- Assume an undetermined room temperature to be 25\nExample\n25\n70 >> 25\nnan >> 40',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.quenching_induced_crystallisation#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'quenching_induced_crystallisation',
      description:
        'TRUE is measures were taken to discontinuously accelerate the crystallisation process without significantly changing the temperature. i.e. an antisolvent treatment or an analogue process was used.\n- The most common case is the antisolvent treatment where a volume of a solvent in which the perovskite is not soluble is poured on the substrate during spin coating.\n- The same effect can also be achieved by blowing a gas on the sample\n- If the sample quickly after spin coating is subjected to a vacuum, this also counts as quenched induced crystallisation',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.quenching_media#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'quenching_media',
      description:
        'The solvents used in the antisolvent treatment\n- If the antisolvent is a mixture of different solvents, e.g. A and B, list the solvents in alphabetic order and separate them with semicolonsas in (A; B)\n- If gas quenching was used, state the gas used\n- If the sample quickly after spin coating was subjected to a vacuum, state this as \u2018Vacuum\u2019\n- If an antisolvent was used but it is unknown which one, stat this as \u201cAntisolvent\u201d\n- If no antisolvent was used, leave this field blank\nExample\nChlorobenzene\nToluene\nDiethyl ether\nEthyl acetate\nN2\nVacuum\nAnisole',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.quenching_media_mixing_ratios#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'quenching_media_mixing_ratios',
      description:
        'The mixing ratios of the antisolvent\n- The order of the solvent must line up with the previous column\n- For solvent mixtures, i.e. A and B, state the mixing ratios by using semicolons, as in (VA; VB)\n- The preferred metrics is the volume ratios. If that is not available, mass or mol ratios can be used instead, but it the analysis the mixing ratios will be assumed to be based on volumes.\n- For pure solvents, give the mixing ratio as 1\n- For non-solvent processes, give the mixing ratio as 1\nExample\n1\n4; 1',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.quenching_media_volume#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'quenching_media_volume',
      description:
        'The volume of the antisolvent\n- For gas and vacuum assisted quenching, stat the volume as \u2018nan\u2019\n- If the sample is dipped or soaked in the antisolvent, state the volume of the entire solution',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.quenching_media_additives_compounds#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'quenching_media_additives_compounds',
      description:
        'List of the dopants and additives in the antisolvent\n- If several dopants/additives, e.g. A and B, are present, list the dopants/additives in alphabetic order and separate them with semicolonsas in (A; B)\n- If no dopants/additives, leave the field blank',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.quenching_media_additives_concentrations#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'quenching_media_additives_concentrations',
      description:
        'The concentration of the dopants/additives in the antisolvent\n- If more than one dopant/additive in the layer, e.g. A and B, separate the concentration for each dopant/additive with semicolons, as in (A; B)\n- For each dopant/additive, state the concentration.\n- The order of the dopants/additives must be the same as in the previous filed.\n- Concentrations can be stated in different units suited for different situations. Therefore, specify the unit used.\n- The preferred way to state the concentration of a dopant/additive is to refer to the amount in the final product, i.e. the material in the layer. When possible, use on the preferred units\no wt%, mol%, vol%, ppt, ppm, ppb\n- When the concentration of the dopant/additive in the final product is unknown, but where the concentration of the dopant/additive in the solution is known, state that concentration instead. When possible, use on the preferred units\no M, mM, molal; g/ml, mg/ml, \u00b5g/ml\n- For values with uncertainties, state the best estimate, e.g write 4 wt% and not 3-5 wt%.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.thermal_annealing_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'thermal_annealing_temperature',
      description:
        'The temperatures of the thermal annealing program associated with each deposition step\n- When more than one reaction step, separate the annealing temperatures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the thermal annealing involves a temperature program with multiple temperature stages, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons (e.g. 25; 100)\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- If no thermal annealing is occurring after the deposition of a layer, state that by stating the room temperature (assumed to 25\u00b0C if not further specified)\n- If the thermal annealing program is not known, state that by \u2018nan\u2019\nExample\n100\n70; 100 >> 100\n25 >> 90; 150',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.thermal_annealing_time#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'thermal_annealing_time',
      description:
        'The time program associated to the thermal annealing.\n- When more than one reaction step, separate the annealing times associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the thermal annealing involves a temperature program with multiple temperature stages, list the associated times at those temperatures and separate them with semicolons.\n- The annealing times must align in terms of layers\u00b8 reaction steps and annealing temperatures in the previous filed.\n- If a time is not known, state that by \u2018nan\u2019\n- If no thermal annealing is occurring after the deposition of a layer, state that by \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 20 and not 10-30.\nExample\n60\n5; 30 >> 60\n0 >> 5; 5',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.thermal_annealing_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'thermal_annealing_atmosphere',
      description:
        'The atmosphere in which the thermal annealing is conducted.\n- When more than one reaction step, separate the atmospheres associated to each annealing step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of deposition steps must line up with the previous columns.\n- If the atmosphere is a mixture of different gases, i.e. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas.\n- This is often the same as the atmosphere under which the deposition is occurring, but not always.\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nN2\nAir >> N2\nAr',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.thermal_annealing_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'thermal_annealing_relative_humidity',
      description:
        'The relative humidity during the thermal annealing\n- If there is more than one annealing step involved, list the associate relative humidity in the surrounding atmosphere and separate them by a double forward angel bracket (\u2018 >> \u2018)\n- The number and order of annealing steps must line up with the previous column\n- If there are uncertainties, only state the best estimate, e.g. write 35 and not 20-50.\n- If a humidity is not known, stat that as \u2018nan\u2019\nExample\n0\n35 >> 0\nnan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.thermal_annealing_pressure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'thermal_annealing_pressure',
      description:
        'The atmospheric pressure during the thermal annealing\n- If there is more than one annealing step involved, list the associate atmospheric pressures and separate them by a double forward angel bracket (\u2018 >> \u2018)\n- The number and order of annealing steps must line up with the previous column\n- Pressures can be stated in different units suited for different situations. Therefore, specify the unit. The preferred units are:\no atm, bar, mbar, mmHg, Pa, torr, psi\n- If a pressure is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 100 pa and not 80-120 pa.\nExample\n1 atm\n1 atm >> 0.002 torr\n1 atm >> 1 atm >> nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.solvent_annealing#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'solvent_annealing',
      description:
        'TRUE if there has been a separate solvent annealing step, i.e. a step where the perovskite has been annealing in an atmosphere with a significant amount of solvents. This step should also be included deposition procedure sequence but is also stated separately here to simplify downstream filtering.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.solvent_annealing_timing#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'solvent_annealing_timing',
      description:
        'The timing of the solvent annealing with respect to the thermal annealing step under which the perovskite is formed. There are three options.\n- The solvent annealing is conducted before the perovskite is formed.\n- The solvent annealing is conducted under the same annealing step in which the perovskite is formed\n- The solvent annealing is conducted after the perovskite has formed.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.solvent_annealing_solvent_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'solvent_annealing_solvent_atmosphere',
      description:
        'The solvents used in the solvent annealing step\n- If the solvent atmosphere is a mixture of different solvents and gases, e.g. A and B, list them in alphabetic order and separate them with semicolonsas in (A; B)\nExample\nDMSO\nDMF\nDMF; DMSO',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.solvent_annealing_time#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'solvent_annealing_time',
      description:
        'The extend of the solvent annealing step in minutes\n- If the time is not known, state that by \u2018nan\u2019\n- If the solvent annealing involves a temperature program with multiple temperature stages, list the associated times at each temperature and separate them with a semicolon (e.g. 5; 10)',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.solvent_annealing_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'solvent_annealing_temperature',
      description:
        'The temperature during the solvent annealing step\n- The temperature refers to the temperature of the sample\n- If the solvent annealing involves a temperature program with multiple temperature stages, list the associated temperatures and separate them with a semicolon (e.g. 5; 10) and make sure they align with the times in the previous field.\n- If the temperature is not known, state that by \u2018nan\u2019',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.after_treatment_of_formed_perovskite#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'after_treatment_of_formed_perovskite',
      description:
        'Any after treatment of the formed perovskite. Most possible reaction steps should have been entered before this point. This is an extra category for procedures that just does not fit into any of the other categories.\nExamples:\nHot isostatic pressing\nMagnetic field\nUV radiation',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.perovskite_deposition.after_treatment_of_formed_perovskite_met#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'after_treatment_of_formed_perovskite_met',
      description:
        'Connected to the previous field (After treatment of formed perovskite). The label describing the method should be in the previous filed, and the associated metrics in this one. For exampleThe sample is intense gamma radiation at a flux of X under 45 minutes. The \u201cgamma radiation\u201d is the label, and the time and the flux is the metrics. Give the units when you state the metrics\nExamples:\n40kHz; 5W; 4 min\n45 deg. C\n30 min\n50 W/cm2; 2.38 s',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.stack_sequence#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'stack_sequence',
      description:
        'The stack sequence describing the hole transport layer. Use the following formatting guidelines\n- With the HTL, we refer to any layer between the substrate and the perovskite in a pin-device, and any layer between the perovskite and the back contact in a nip-device.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If two materials, e.g. A and B, are mixed in one layer, list the materials in alphabetic order and separate them with semicolons, as in (A; B)\n- If no hole transport layer, state that as \u2018non\u2019\n- Use common abbreviations when appropriate but spell it out if risk for confusion.\n- If a material is doped, or have an additive, state the pure material here and specify the doping in the columns specifically targeting the doping of those layers.\n- There is no sharp well-defined boundary between when a material is best considered as doped or as a mixture of two materials. When in doubt if your material is best described as doped or as a mixture, use the notation that best capture the metaphysical essence of the situation.\n- There are a lot of stack sequences described in the literature. Try to find your one in the list. If it is not there (i.e. you may have done something new) define a new stack sequence according to the instructions.\nExample:\nSpiro-MeOTAD\nPEDOT:PSS\nnone\nNiO-c\nPTAA',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.thickness_list#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'thickness_list',
      description:
        'A list of thicknesses of the individual layers in the stack.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- The layers must line up with the previous filed.\n- State thicknesses in nm\n- Every layer in the stack have a thickness. If it is unknown, state this as \u2018nan\u2019\n- If there are uncertainties, state the best estimate, e.g write 100 and not 90-110\nExample\n200\nnan |250\n100 | 5 | 8',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.additives_compounds#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'additives_compounds',
      description:
        'List of the dopants and additives that are in each layer of the HTL-stack\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- The layers must line up with the previous fields.\n- If several dopants/additives, e.g. A and B, are present in one layer, list the dopants/additives in alphabetic order and separate them with semicolons, as in (A; B)\n- If no dopants/additives, state that as \u201cUndoped\u201d\n- If the doping situation is unknown, stat that as\u2018Unknown\u2019\nExample\nLi-TFSI; TBP\nFK209; Li-TFSI; TBP\nF4-TCNQ\nUndoped\nCu | Ag; Cu',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.additives_concentrations#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'additives_concentrations',
      description:
        'The concentration of the dopants/additives.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If more than one dopant/additive in the layer, e.g. A and B, separate the concentration for each dopant/additive with semicolons, as in (A; B)\n- For each dopant/additive in the layer, state the concentration.\n- The order of the dopants/additives must be the same as in the previous filed.\n- For layers with no dopants/additives, state this as \u2018none\u2019\n- When concentrations are unknown, state that as \u2018nan\u2019\n- Concentrations can be stated in different units suited for different situations. Therefore, specify the unit used.\n- The preferred way to state the concentration of a dopant/additive is to refer to the amount in the final product, i.e. the material in the layer. When possible, use on the preferred units\no wt%, mol%, vol%, ppt, ppm, ppb\n- When the concentration of the dopant/additive in the final product is unknown, but where the concentration of the dopant/additive in the solution is known, state that concentration instead. When possible, use on the preferred units\no M, mM, molal; g/ml, mg/ml, \u00b5g/ml\n- For values with uncertainties, state the best estimate, e.g write 4 wt% and not 3-5 wt%.\nExample\n4 wt%\n5 vol%; nan | 10 mg/ml\n0.3 mol% | 2 mol%; 0.2 wt% | 0.3 M',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_procedure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_procedure',
      description:
        'The deposition procedures for the HTL-stack.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate them by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- Thermal annealing is generally not considered as an individual reaction step. The philosophy behind this is that every deposition step has a thermal history, which is specified in a separate filed. In exceptional cases with thermal annealing procedures clearly disconnected from other procedures, state \u2018Thermal annealing\u2019 as a separate reaction step.\n- Please read the instructions under \u201cPerovskite. Deposition. Procedure\u201d for descriptions and distinctions between common deposition procedures and how they should be labelled for consistency in the database.\nExample\nSpin-coating\nSpin-coating | Spin-coating\nEvaporation\nSpray-pyrolys',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_aggregation_state_of_reactants#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_aggregation_state_of_reactants',
      description:
        'The physical state of the reactants.\n- The three basic categories are Solid/Liquid/Gas\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the aggregation state associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- Most cases are clear cut, e.g. spin-coating involves species in solution and evaporation involves species in gas phase. For less clear-cut cases, consider where the reaction really is happening as in:\no For a spray-coating procedure, it is droplets of liquid that enters the substrate (thus a liquid phase reaction)\no For sputtering and thermal evaporation, it is species in gas phase that reaches the substrate (thus a gas phase reaction)\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nLiquid\nGas | Liquid\nLiquid | Liquid >> Liquid',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_synthesis_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_synthesis_atmosphere',
      description:
        'The synthesis atmosphere.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the atmospheres associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the synthesis atmosphere is a mixture of different gases, e.g. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nN2\nVacuum | N2\nAir | Ar; H2O >> Ar',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_synthesis_atmosphere_pressure_total#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_synthesis_atmosphere_pressure_total',
      description:
        'The total gas pressure during each reaction step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the pressures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- Pressures can be stated in different units suited for different situations. Therefore, specify the unit. The preferred units are:\no atm, bar, mbar, mmHg, Pa, torr, psi\n- If a pressure is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 100 pa and not 80-120 pa.\nExample\n1 atm\n0.002 torr | 10000 Pa\nnan >> 1 atm | 1 atm',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_synthesis_atmosphere_pressure_partial#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_synthesis_atmosphere_pressure_partial',
      description:
        'The partial pressures for the gases present during each reaction step.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the pressures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the synthesis atmosphere is a mixture of different gases, e.g. A and B, list the partial pressures and separate them with semicolons, as in (A; B). The list of partial pressures must line up with the gases they describe.\n- In cases where no gas mixtures are used, this field will be the same as the previous filed.\nExample\n1 atm\n0.002 torr | 10000 Pa\nnan >> 0.99 atm; 0.01 atm | 1 atm',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_synthesis_atmosphere_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_synthesis_atmosphere_relative_humidity',
      description:
        'The relative humidity during each deposition step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the relative humidity associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns\n- If the relative humidity for a step is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 35 and not 30-40.\nExample\n35\n0 | 20\nnan >> 25 | 0',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_solvents#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_solvents',
      description:
        'The solvents used in each deposition procedure for each layer in the stack\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvents associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the solvents in alphabetic order and separate them with semicolons, as in (A; B)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- For non-liquid processes with no solvents, state the solvent as \u2018none\u2019\n- If the solvent is not known, state this as \u2018Unknown\u2019\n- Use common abbreviations when appropriate but spell it out when risk for confusion\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nChlorobenzene\nAcetonitile; Ethanol | Chlorobenzene\nnone >> Ethanol; Methanol; H2O | DMF; DMSO',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_solvents_mixing_ratios#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_solvents_mixing_ratios',
      description:
        'The mixing ratios for mixed solvents\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent mixing ratios associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- For pure solvents, state the mixing ratio as 1\n- For non-solvent processes, state the mixing ratio as 1\n- For unknown mixing ratios, state the mixing ratio as \u2018nan\u2019\n- For solvent mixtures, i.e. A and B, state the mixing ratios by using semicolons, as in (VA; VB)\n- The preferred metrics is the volume ratios. If that is not available, mass or mol ratios can be used instead, but it the analysis the mixing ratios will be assumed to be based on volumes.\nExample\n1\n4; 1 | 1\n1 >> 5; 2; 0.3 | 2; 1',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_solvents_supplier#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_solvents_supplier',
      description:
        'The suppliers of all the solvents.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent suppliers associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the associated suppliers and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- For non-liquid processes with no solvents, mark the supplier as \u2018none\u2019\n- If the supplier for a solvent is unknown, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nSigma Aldrich\nSigma Aldrich; Fisher | Acros\nnone >> Sigma Aldrich; Sigma Aldrich | Unknown',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_solvents_purity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_solvents_purity',
      description:
        'The purity of the solvents used.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent purities associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the associated purities and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- Use standard nomenclature for purities, e.g. pro analysis, puris, extra dry, etc.\n- For non-liquid processes with no solvents, state the purity as \u2018none\u2019\n- If the purity for a solvent is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nPro analysis\nPuris; Puris| Tecnical\nnone >> Pro analysis; Pro analysis | Unknown',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_reaction_solutions_compounds#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_compounds',
      description:
        'The non-solvent precursor chemicals used in each reaction step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the non-solvent chemicals associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several compounds, e.g. A and B, list the associated compounds in alphabetic order and separate them with semicolons, as in (A; B)\n- Note that also dopants/additives should be included\n- When several precursor solutions are made and mixed before the reaction step, it is the properties of the final mixture used in the reaction we here describe.\n- The number and order of layers and reaction steps must line up with the previous columns.\n- For gas phase reactions, state the reaction gases as if they were in solution.\n- For solid-state reactions, state the compounds as if they were in solution.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019\n- If the compounds for a deposition step is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nFK209; Li-TFSI; Spiro-MeOTAD; TBP\nNiO-np\nPTAA | CuSCN',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_reaction_solutions_compounds_supplier#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_compounds_supplier',
      description:
        'The suppliers of the non-solvent chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the non-solvent chemical suppliers associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, e.g. A and B, list the associated suppliers and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- For gas phase reactions, state the suppliers for the gases or the targets/evaporation sources that are evaporated/sputtered/etc.\n- For solid state reactions, state the suppliers for the compounds in the same way.\n- For reaction steps involving only pure solvents, state the supplier as \u2018none\u2019 (as that that is entered in a separate filed)\n- For chemicals that are lab made, state that as \u201cLab made\u201d or \u201cLab made (name of lab)\u201d\n- If the supplier for a compound is unknown, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nDysole; Sigma Aldrich; Dyenamo; Sigma Aldrich\nSigma Aldrich; Fisher | Acros\nLab made (EPFL) | Sigma Aldrich >> none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_reaction_solutions_compounds_purity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_compounds_purity',
      description:
        'The purity of the non-solvent chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the compound purities associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, i.e. A and B, list the associated purities and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- Use standard nomenclature for purities, e.g. pro analysis, puris, extra dry, etc.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019 (as that is stated in another field)\n- If the purity for a compound is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nPro analysis\n99.999; Puris| Tecnical\nUnknown >> Pro analysis; Pro analysis | none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_reaction_solutions_concentrations#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_concentrations',
      description:
        'The concentration of the non-solvent precursor chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the concentrations associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, e.g. A and B, list the associated concentrations and separate them with semicolons, as in (A; B)\n- The order of the compounds must be the same as in the previous filed.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019\n- When concentrations are unknown, state that as \u2018nan\u2019\n- Concentrations can be stated in different units suited for different situations. Therefore, specify the unit used. When possible, use one of the preferred units\no M, mM, molal; g/ml, mg/ml, \u00b5g/ml, wt%, mol%, vol%, ppt, ppm, ppb\n- For values with uncertainties, state the best estimate, e.g write 4 wt% and not 3-5 wt%.\nExample\n4 wt%\n0.2 M; 0.15 M| 10 mg/ml\n0.3 mol% | 2 mol%; 0.2 wt% | nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_reaction_solutions_volumes#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_volumes',
      description:
        'The volume of the reaction solutions\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the volumes associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The volumes refer the volumes used, not the volume of the stock solutions. Thus if 0.15 ml of a solution is spin-coated, the volume is 0.15 ml\n- For reaction steps without solvents, state the volume as \u2018nan\u2019\n- When volumes are unknown, state that as \u2018nan\u2019\nExample\n0.1\n0.1 >> 0.05 | 0.05\nnan | 0.15',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_reaction_solutions_age#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_age',
      description:
        'The age of the solutions\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the age of the solutions associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- As a general guideline, the age refers to the time from the preparation of the final precursor mixture to the reaction procedure.\n- When the age of a solution is not known, state that as \u2018nan\u2019\n- For reaction steps where no solvents are involved, state this as \u2018nan\u2019\n- For solutions that is stored a long time, an order of magnitude estimate is adequate.\nExample\n2\n0.25 |1000 >> 10000\nnan | nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_reaction_solutions_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_temperature',
      description:
        'The temperature of the reaction solutions.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the temperatures of the solutions associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a reaction solution undergoes a temperature program, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons, e.g. 25; 100\n- When the temperature of a solution is unknown, state that as \u2018nan\u2019\n- For reaction steps where no solvents are involved, state the temperature of the gas or the solid if that make sense. Otherwise state this as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- Assume an undetermined room temperature to be 25\nExample\n25\n100; 50 | 25\nnan | 25 >> 25',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_substrate_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_substrate_temperature',
      description:
        'The temperature of the substrate.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the temperatures of the substrates (i.e. the last deposited layer) associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The temperature of the substrate refers to the temperature when the deposition of the layer is occurring.\n- If a substrate undergoes a temperature program before the deposition, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons (e.g. 25; 100)\n- When the temperature of a substrate is not known, state that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- Assume that an undetermined room temperature is 25\nExample\n25\nnan\n125; 325; 375; 450 | 25 >> 25',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_thermal_annealing_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_thermal_annealing_temperature',
      description:
        'The temperatures of the thermal annealing program associated with depositing the layers\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the annealing temperatures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the thermal annealing involves a temperature program with multiple temperature stages, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons (e.g. 25; 100)\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- If no thermal annealing is occurring after the deposition of a layer, state that by stating the room temperature (assumed to 25\u00b0C if not further specified)\n- If the thermal annealing program is not known, state that by \u2018nan\u2019\nExample\n25\n50 | nan\n450 | 125; 325; 375; 450 >> 125; 325; 375; 450',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_thermal_annealing_time#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_thermal_annealing_time',
      description:
        'The time program associated to the thermal annealing program.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the annealing times associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the thermal annealing involves a temperature program with multiple temperature stages, list the associated times at those temperatures and separate them with semicolons.\n- The annealing times must align in terms of layers\u00b8 reaction steps and annealing temperatures in the previous filed.\n- If a time is not known, state that by \u2018nan\u2019\n- If no thermal annealing is occurring after the deposition of a layer, state that by \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 20 and not 10-30.\nExample\nnan\n60 | 1000\n30 | 5; 5; 5; 30 >> 5; 5; 5; 30',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.deposition_thermal_annealing_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_thermal_annealing_atmosphere',
      description:
        'The atmosphere during thermal annealing\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the atmospheres associated to each annealing step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the atmosphere is a mixture of different gases, i.e. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas.\n- This is often the same as the atmosphere under which the deposition is occurring, but not always.\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nN2\nVacuum | N2\nAir | Ar >> Ar',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.storage_time_until_next_deposition_step#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_time_until_next_deposition_step',
      description:
        'The time between the HTL stack is finalised and the next layer is deposited\n- If there are uncertainties, only state the best estimate, e.g. write 35 and not 20-50.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.storage_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_atmosphere',
      description:
        'The atmosphere in which the sample with the finalised HTL stack is stored until the next deposition step.\nExample\nAir\nN2\nVacuum',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.storage_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_relative_humidity',
      description:
        'The relive humidity under which the sample with the finalised HTL stack is stored until next deposition step\n- If there are uncertainties, only state the best estimate, e.g write 35 and not 20-50.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.htl.surface_treatment_before_next_deposition_step#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'surface_treatment_before_next_deposition_step',
      description:
        'Description of any type of surface treatment or other treatment the sample with the finalised HTL stack undergoes before the next deposition step.\n- If more than one treatment, list the treatments and separate them by a double forward angel bracket (\u2018 >> \u2018)\n- If no special treatment, state that as \u2018none\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample:\nnone\nAr plasma\nUV-ozone',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.stack_sequence#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'stack_sequence',
      description:
        'The stack sequence describing the back contact.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If two materials, e.g. A and B, are mixed in one layer, list the materials in alphabetic order and separate them with semicolons, as in (A; B)\n- If no back contact, state that as \u2018non\u2019\n- Use common abbreviations when appropriate but spell it out if risk for confusion.\n- If a material is doped, or have an additive, state the pure material here and specify the doping in the columns specifically targeting the doping of those layers.\n- There is no sharp well-defined boundary between when a material is best considered as doped or as a mixture of two materials. When in doubt if your material is best described as doped or as a mixture, use the notation that best capture the metaphysical essence of the situation.\n- There are a lot of stack sequences described in the literature. Try to find your one in the list. If it is not there (i.e. you may have done something new) define a new stack sequence according to the instructions.\nExample:\nAu\nAg\nAl\nCarbon\nMoO3 | Ag',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.thickness_list#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'thickness_list',
      description:
        'A list of thicknesses of the individual layers in the stack.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- The layers must line up with the previous filed.\n- State thicknesses in nm\n- Every layer in the stack have a thickness. If it is unknown, state this as \u2018nan\u2019\n- If there are uncertainties, state the best estimate, e.g write 100 and not 90-110\nExample\n100\n10 | 80\nnan | 100',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.additives_compounds#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'additives_compounds',
      description:
        'List of the dopants and additives that are in each layer of the HTL-stack\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- The layers must line up with the previous fields.\n- If several dopants/additives, e.g. A and B, are present in one layer, list the dopants/additives in alphabetic order and separate them with semicolons, as in (A; B)\n- If no dopants/additives, state that as \u201cUndoped\u201d\n- If the doping situation is unknown, stat that as\u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template, even if to most common back contacts is undoped metals\nExample\nCuS\nB; P\nAu-np | Undoped',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.additives_concentrations#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'additives_concentrations',
      description:
        'The concentration of the dopants/additives.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If more than one dopant/additive in the layer, e.g. A and B, separate the concentration for each dopant/additive with semicolons, as in (A; B)\n- For each dopant/additive in the layer, state the concentration.\n- The order of the dopants/additives must be the same as in the previous filed.\n- For layers with no dopants/additives, state this as \u2018none\u2019\n- When concentrations are unknown, state that as \u2018nan\u2019\n- Concentrations can be stated in different units suited for different situations. Therefore, specify the unit used.\n- The preferred way to state the concentration of a dopant/additive is to refer to the amount in the final product, i.e. the material in the layer. When possible, use on the preferred units\no wt%, mol%, vol%, ppt, ppm, ppb\n- When the concentration of the dopant/additive in the final product is unknown, but where the concentration of the dopant/additive in the solution is known, state that concentration instead. When possible, use on the preferred units\no M, mM, molal; g/ml, mg/ml, \u00b5g/ml\n- For values with uncertainties, state the best estimate, e.g write 4 wt% and not 3-5 wt%.\nExample\n4 wt%\n5 vol%; nan | 10 mg/ml\n0.3 mol% | 2 mol%; 0.2 wt% | 0.3 M',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_procedure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_procedure',
      description:
        'The deposition procedures for the HTL-stack.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate them by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- Thermal annealing is generally not considered as an individual reaction step. The philosophy behind this is that every deposition step has a thermal history, which is specified in a separate filed. In exceptional cases with thermal annealing procedures clearly disconnected from other procedures, state \u2018Thermal annealing\u2019 as a separate reaction step.\n- Please read the instructions under \u201cPerovskite. Deposition. Procedure\u201d for descriptions and distinctions between common deposition procedures and how they should be labelled for consistency in the database.\n- A few additional clarifications:\n- Lamination\no A readymade film is transferred directly to the device stack. A rather broad concept. An everyday kitchen related example of lamination would eb to place a thin plastic film over a slice of pie.\n- Sandwiching\no When a readymade top stack simply is placed on top of the device stack. Could be held together with clams. The typical example is a when a \u201cCarbon | FTO | SLG\u201d is placed on top of the device stack. Standard procedure in the DSSC filed.\nExample\nEvaporation\nEvaporation | Evaporation\nDoctor blading\nScreen printing\nSputtering\nLamination\nE-beam evaporation\nSandwiching',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_aggregation_state_of_reactants#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_aggregation_state_of_reactants',
      description:
        'The physical state of the reactants.\n- The three basic categories are Solid/Liquid/Gas\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the aggregation state associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- Most cases are clear cut, e.g. spin-coating involves species in solution and evaporation involves species in gas phase. For less clear-cut cases, consider where the reaction really is happening as in:\no For a spray-coating procedure, it is droplets of liquid that enters the substrate (thus a liquid phase reaction)\no For sputtering and thermal evaporation, it is species in gas phase that reaches the substrate (thus a gas phase reaction)\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nLiquid\nGas | Liquid\nLiquid | Liquid >> Liquid',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_synthesis_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_synthesis_atmosphere',
      description:
        'The synthesis atmosphere.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the atmospheres associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the synthesis atmosphere is a mixture of different gases, e.g. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nVacuum\nVacuum | N2\nAir | Ar; H2O >> Ar',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_synthesis_atmosphere_pressure_total#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_synthesis_atmosphere_pressure_total',
      description:
        'The total gas pressure during each reaction step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the pressures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- Pressures can be stated in different units suited for different situations. Therefore, specify the unit. The preferred units are:\no atm, bar, mbar, mmHg, Pa, torr, psi\n- If a pressure is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 100 pa and not 80-120 pa.\nExample\n1 atm\n0.002 torr | 10000 Pa\nnan >> 1 atm | 1 atm',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_synthesis_atmosphere_pressure_partial#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_synthesis_atmosphere_pressure_partial',
      description:
        'The partial pressures for the gases present during each reaction step.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the pressures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the synthesis atmosphere is a mixture of different gases, e.g. A and B, list the partial pressures and separate them with semicolons, as in (A; B). The list of partial pressures must line up with the gases they describe.\n- In cases where no gas mixtures are used, this field will be the same as the previous filed.\nExample\n1 atm\n0.002 torr | 10000 Pa\nnan >> 0.99 atm; 0.01 atm | 1 atm',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_synthesis_atmosphere_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_synthesis_atmosphere_relative_humidity',
      description:
        'The relative humidity during each deposition step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the relative humidity associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns\n- If the relative humidity for a step is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 35 and not 30-40.\nExample\n35\n0 | 20\nnan >> 25 | 0',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_solvents#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_solvents',
      description:
        'The solvents used in each deposition procedure for each layer in the stack\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvents associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the solvents in alphabetic order and separate them with semicolons, as in (A; B)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- For non-liquid processes with no solvents, state the solvent as \u2018none\u2019\n- If the solvent is not known, state this as \u2018Unknown\u2019\n- Use common abbreviations when appropriate but spell it out when risk for confusion\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nnone\nAcetonitile; Ethanol | Chlorobenzene\nnone >> Ethanol; Methanol; H2O | DMF; DMSO',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_solvents_mixing_ratios#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_solvents_mixing_ratios',
      description:
        'The mixing ratios for mixed solvents\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent mixing ratios associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- For pure solvents, state the mixing ratio as 1\n- For non-solvent processes, state the mixing ratio as 1\n- For unknown mixing ratios, state the mixing ratio as \u2018nan\u2019\n- For solvent mixtures, i.e. A and B, state the mixing ratios by using semicolons, as in (VA; VB)\n- The preferred metrics is the volume ratios. If that is not available, mass or mol ratios can be used instead, but it the analysis the mixing ratios will be assumed to be based on volumes.\nExample\n1\n4; 1 | 1\n1 >> 5; 2; 0.3 | 2; 1',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_solvents_supplier#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_solvents_supplier',
      description:
        'The suppliers of all the solvents.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent suppliers associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the associated suppliers and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- For non-liquid processes with no solvents, mark the supplier as \u2018none\u2019\n- If the supplier for a solvent is unknown, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nSigma Aldrich\nSigma Aldrich; Fisher | Acros\nnone >> Sigma Aldrich; Sigma Aldrich | Unknown',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_solvents_purity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_solvents_purity',
      description:
        'The purity of the solvents used.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent purities associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the associated purities and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- Use standard nomenclature for purities, e.g. pro analysis, puris, extra dry, etc.\n- For non-liquid processes with no solvents, state the purity as \u2018none\u2019\n- If the purity for a solvent is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nPro analysis\nPuris; Puris| Tecnical\nnone >> Pro analysis; Pro analysis | Unknown',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_reaction_solutions_compounds#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_compounds',
      description:
        'The non-solvent precursor chemicals used in each reaction step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the non-solvent chemicals associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several compounds, e.g. A and B, list the associated compounds in alphabetic order and separate them with semicolons, as in (A; B)\n- Note that also dopants/additives should be included\n- When several precursor solutions are made and mixed before the reaction step, it is the properties of the final mixture used in the reaction we here describe.\n- The number and order of layers and reaction steps must line up with the previous columns.\n- For gas phase reactions, state the reaction gases as if they were in solution.\n- For solid-state reactions, state the compounds as if they were in solution.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019\n- If the compounds for a deposition step is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nAu\nCuI\nAg',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_reaction_solutions_compounds_supplier#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_compounds_supplier',
      description:
        'The suppliers of the non-solvent chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the non-solvent chemical suppliers associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, e.g. A and B, list the associated suppliers and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- For gas phase reactions, state the suppliers for the gases or the targets/evaporation sources that are evaporated/sputtered/etc.\n- For solid state reactions, state the suppliers for the compounds in the same way.\n- For reaction steps involving only pure solvents, state the supplier as \u2018none\u2019 (as that that is entered in a separate filed)\n- For chemicals that are lab made, state that as \u201cLab made\u201d or \u201cLab made (name of lab)\u201d\n- If the supplier for a compound is unknown, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nDysole; Sigma Aldrich; Dyenamo; Sigma Aldrich\nSigma Aldrich; Fisher | Acros\nLab made (EPFL) | Sigma Aldrich >> none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_reaction_solutions_compounds_purity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_compounds_purity',
      description:
        'The purity of the non-solvent chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the compound purities associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, i.e. A and B, list the associated purities and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- Use standard nomenclature for purities, e.g. pro analysis, puris, extra dry, etc.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019 (as that is stated in another field)\n- If the purity for a compound is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nPro analysis\n99.999; Puris| Tecnical\nUnknown >> Pro analysis; Pro analysis | none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_reaction_solutions_concentrations#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_concentrations',
      description:
        'The concentration of the non-solvent precursor chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the concentrations associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, e.g. A and B, list the associated concentrations and separate them with semicolons, as in (A; B)\n- The order of the compounds must be the same as in the previous filed.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019\n- When concentrations are unknown, state that as \u2018nan\u2019\n- Concentrations can be stated in different units suited for different situations. Therefore, specify the unit used. When possible, use one of the preferred units\no M, mM, molal; g/ml, mg/ml, \u00b5g/ml, wt%, mol%, vol%, ppt, ppm, ppb\n- For values with uncertainties, state the best estimate, e.g write 4 wt% and not 3-5 wt%.\nExample\n4 wt%\n0.2 M; 0.15 M| 10 mg/ml\n0.3 mol% | 2 mol%; 0.2 wt% | nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_reaction_solutions_volumes#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_volumes',
      description:
        'The volume of the reaction solutions\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the volumes associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The volumes refer the volumes used, not the volume of the stock solutions. Thus if 0.15 ml of a solution is spin-coated, the volume is 0.15 ml\n- For reaction steps without solvents, state the volume as \u2018nan\u2019\n- When volumes are unknown, state that as \u2018nan\u2019\nExample\n0.1\n0.1 >> 0.05 | 0.05\nnan | 0.15',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_reaction_solutions_age#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_age',
      description:
        'The age of the solutions\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the age of the solutions associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- As a general guideline, the age refers to the time from the preparation of the final precursor mixture to the reaction procedure.\n- When the age of a solution is not known, state that as \u2018nan\u2019\n- For reaction steps where no solvents are involved, state this as \u2018nan\u2019\n- For solutions that is stored a long time, an order of magnitude estimate is adequate.\nExample\n2\n0.25 |1000 >> 10000\nnan | nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_reaction_solutions_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_reaction_solutions_temperature',
      description:
        'The temperature of the reaction solutions.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the temperatures of the solutions associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a reaction solution undergoes a temperature program, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons, e.g. 25; 100\n- When the temperature of a solution is unknown, state that as \u2018nan\u2019\n- For reaction steps where no solvents are involved, state the temperature of the gas or the solid if that make sense. Otherwise state this as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- Assume an undetermined room temperature to be 25\nExample\n25\n100; 50 | 25\nnan | 25 >> 25',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_substrate_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_substrate_temperature',
      description:
        'The temperature of the substrate.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the temperatures of the substrates (i.e. the last deposited layer) associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The temperature of the substrate refers to the temperature when the deposition of the layer is occurring.\n- If a substrate undergoes a temperature program before the deposition, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons (e.g. 25; 100)\n- When the temperature of a substrate is not known, state that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- Assume that an undetermined room temperature is 25\nExample\n25\nnan\n125; 325; 375; 450 | 25 >> 25',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_thermal_annealing_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_thermal_annealing_temperature',
      description:
        'The temperatures of the thermal annealing program associated with depositing the layers\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the annealing temperatures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the thermal annealing involves a temperature program with multiple temperature stages, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons (e.g. 25; 100)\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- If no thermal annealing is occurring after the deposition of a layer, state that by stating the room temperature (assumed to 25\u00b0C if not further specified)\n- If the thermal annealing program is not known, state that by \u2018nan\u2019\nExample\n25\n50 | nan\n450 | 125; 325; 375; 450 >> 125; 325; 375; 450',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_thermal_annealing_time#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_thermal_annealing_time',
      description:
        'The time program associated to the thermal annealing program.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the annealing times associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the thermal annealing involves a temperature program with multiple temperature stages, list the associated times at those temperatures and separate them with semicolons.\n- The annealing times must align in terms of layers\u00b8 reaction steps and annealing temperatures in the previous filed.\n- If a time is not known, state that by \u2018nan\u2019\n- If no thermal annealing is occurring after the deposition of a layer, state that by \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 20 and not 10-30.\nExample\nnan\n60 | 1000\n30 | 5; 5; 5; 30 >> 5; 5; 5; 30',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.deposition_thermal_annealing_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'deposition_thermal_annealing_atmosphere',
      description:
        'The atmosphere during thermal annealing\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the atmospheres associated to each annelaing step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the atmosphere is a mixture of different gases, i.e. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas.\n- This is often the same as the atmosphere under which the deposition is occurring, but not always.\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nN2\nVacuum | N2\nAir | Ar >> Ar',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.storage_time_until_next_deposition_step#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_time_until_next_deposition_step',
      description:
        'The time between the back contact is finalised and the next layer is deposited\n- If there are uncertainties, only state the best estimate, e.g. write 35 and not 20-50.\n- If this is the last layer in the stack, state this as \u2018nan\u2019',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.storage_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_atmosphere',
      description:
        'The atmosphere in which the sample with the finalised back contact is stored until the next deposition step or device performance measurement\nExample\nAir\nN2\nVacuum',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.storage_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_relative_humidity',
      description:
        'The relive humidity under which the sample with the finalised back contact is stored until the next deposition step or device performance measurement\n- If there are uncertainties, only state the best estimate, e.g write 35 and not 20-50.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.backcontact.surface_treatment_before_next_deposition_step#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'surface_treatment_before_next_deposition_step',
      description:
        'Description of any type of surface treatment or other treatment the sample with the finalised back contact is stored until the next deposition step or device performance measurement\n- If more than one treatment, list the treatments and separate them by a double forward angel bracket (\u2018 >> \u2018)\n- If no special treatment, state that as \u2018none\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample:\nnone\nAr plasma\nUV-ozone',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front',
      description:
        'TRUE if there is a functional layer below the substrate, i.e. on the opposite side of the substrate from with respect to the perovskite.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_function#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_function',
      description:
        'The function of the additional layers on the substrate side\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If a layer has more than one function, e.g. A and B, list the functions in order and separate them with semicolons, as in (A; B)\nExample:\nA.R.C.\nBack reflection\nDown conversion\nEncapsulation\nLight management\nUpconversion',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_stack_sequence#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_stack_sequence',
      description:
        'The stack sequence describing the additional layers on the substrate side\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If two materials, e.g. A and B, are mixed in one layer, list the materials in alphabetic order and separate them with semicolons, as in (A; B)\n- Use common abbreviations when appropriate but spell it out if risk for confusion.\n- There are separate filed for doping. Indicate doping with colons. E.g. wither aluminium doped NiO-np as Al:NiO-np\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample:\nMgF2\nAu-np\nNaYF4:Eu-np',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_thickness_list#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_thickness_list',
      description:
        'A list of thicknesses of the individual layers in the stack.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- The layers must line up with the previous filed.\n- State thicknesses in nm\n- Every layer in the stack have a thickness. If it is unknown, state this as \u2018nan\u2019\n- If there are uncertainties, state the best estimate, e.g write 100 and not 90-110\nExample\n200\nnan |250\n100 | 5 | 8',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_additives_compounds#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_additives_compounds',
      description:
        'List of the dopants and additives that are in each layer of the HTL-stack\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- The layers must line up with the previous fields.\n- If several dopants/additives, e.g. A and B, are present in one layer, list the dopants/additives in alphabetic order and separate them with semicolons, as in (A; B)\n- If no dopants/additives, state that as \u201cUndoped\u201d\n- If the doping situation is unknown, stat that as\u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template, even if to most common back contacts is undoped metals\nExample\nCuS\nB; P\nAu-np | Undoped',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_additives_concentrations#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_additives_concentrations',
      description:
        'The concentration of the dopants/additives.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If more than one dopant/additive in the layer, e.g. A and B, separate the concentration for each dopant/additive with semicolons, as in (A; B)\n- For each dopant/additive in the layer, state the concentration.\n- The order of the dopants/additives must be the same as in the previous filed.\n- For layers with no dopants/additives, state this as \u2018none\u2019\n- When concentrations are unknown, state that as \u2018nan\u2019\n- Concentrations can be stated in different units suited for different situations. Therefore, specify the unit used.\n- The preferred way to state the concentration of a dopant/additive is to refer to the amount in the final product, i.e. the material in the layer. When possible, use on the preferred units\no wt%, mol%, vol%, ppt, ppm, ppb\n- When the concentration of the dopant/additive in the final product is unknown, but where the concentration of the dopant/additive in the solution is known, state that concentration instead. When possible, use on the preferred units\no M, mM, molal; g/ml, mg/ml, \u00b5g/ml\n- For values with uncertainties, state the best estimate, e.g write 4 wt% and not 3-5 wt%.\nExample\n4 wt%\n5 vol%; nan | 10 mg/ml\n0.3 mol% | 2 mol%; 0.2 wt% | 0.3 M',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_procedure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_procedure',
      description:
        'The deposition procedures for the HTL-stack.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate them by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- Thermal annealing is generally not considered as an individual reaction step. The philosophy behind this is that every deposition step has a thermal history, which is specified in a separate filed. In exceptional cases with thermal annealing procedures clearly disconnected from other procedures, state \u2018Thermal annealing\u2019 as a separate reaction step.\n- Please read the instructions under \u201cPerovskite. Deposition. Procedure\u201d for descriptions and distinctions between common deposition procedures and how they should be labelled for consistency in the database.\n- A few additional clarifications:\n- Lamination\no A readymade film is transferred directly to the device stack. A rather broad concept. An everyday kitchen related example of lamination would eb to place a thin plastic film over a slice of pie.\n- Sandwiching\no When a readymade top stack simply is placed on top of the device stack. Could be held together with clams. The typical example is a when a \u201cCarbon | FTO | SLG\u201d is placed on top of the device stack. Standard procedure in the DSSC filed.\nExample\nEvaporation\nEvaporation | Evaporation\nDoctor blading\nScreen printing\nSputtering\nLamination\nE-beam evaporation\nSandwiching',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_aggregation_state_of_reactants#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_aggregation_state_of_reactants',
      description:
        'The physical state of the reactants.\n- The three basic categories are Solid/Liquid/Gas\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the aggregation state associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- Most cases are clear cut, e.g. spin-coating involves species in solution and evaporation involves species in gas phase. For less clear-cut cases, consider where the reaction really is happening as in:\no For a spray-coating procedure, it is droplets of liquid that enters the substrate (thus a liquid phase reaction)\no For sputtering and thermal evaporation, it is species in gas phase that reaches the substrate (thus a gas phase reaction)\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nLiquid\nGas | Liquid\nLiquid | Liquid >> Liquid',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_synthesis_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_synthesis_atmosphere',
      description:
        'The synthesis atmosphere.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the atmospheres associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the synthesis atmosphere is a mixture of different gases, e.g. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nVacuum\nVacuum | N2\nAir | Ar; H2O >> Ar',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_synthesis_atmosphere_pressure_total#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_synthesis_atmosphere_pressure_total',
      description:
        'The total gas pressure during each reaction step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the pressures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- Pressures can be stated in different units suited for different situations. Therefore, specify the unit. The preferred units are:\no atm, bar, mbar, mmHg, Pa, torr, psi\n- If a pressure is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 100 pa and not 80-120 pa.\nExample\n1 atm\n0.002 torr | 10000 Pa\nnan >> 1 atm | 1 atm',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_synthesis_atmosphere_pressure_partial#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_synthesis_atmosphere_pressure_partial',
      description:
        'The partial pressures for the gases present during each reaction step.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the pressures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the synthesis atmosphere is a mixture of different gases, e.g. A and B, list the partial pressures and separate them with semicolons, as in (A; B). The list of partial pressures must line up with the gases they describe.\n- In cases where no gas mixtures are used, this field will be the same as the previous filed.\nExample\n1 atm\n0.002 torr | 10000 Pa\nnan >> 0.99 atm; 0.01 atm | 1 atm',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_synthesis_atmosphere_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_synthesis_atmosphere_relative_humidity',
      description:
        'The relative humidity during each deposition step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the relative humidity associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns\n- If the relative humidity for a step is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 35 and not 30-40.\nExample\n35\n0 | 20\nnan >> 25 | 0',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_solvents#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_solvents',
      description:
        'The solvents used in each deposition procedure for each layer in the stack\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvents associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the solvents in alphabetic order and separate them with semicolons, as in (A; B)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- For non-liquid processes with no solvents, state the solvent as \u2018none\u2019\n- If the solvent is not known, state this as \u2018Unknown\u2019\n- Use common abbreviations when appropriate but spell it out when risk for confusion\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nnone\nAcetonitile; Ethanol | Chlorobenzene\nnone >> Ethanol; Methanol; H2O | DMF; DMSO',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_solvents_mixing_ratios#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_solvents_mixing_ratios',
      description:
        'The mixing ratios for mixed solvents\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent mixing ratios associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- For pure solvents, state the mixing ratio as 1\n- For non-solvent processes, state the mixing ratio as 1\n- For unknown mixing ratios, state the mixing ratio as \u2018nan\u2019\n- For solvent mixtures, i.e. A and B, state the mixing ratios by using semicolons, as in (VA; VB)\n- The preferred metrics is the volume ratios. If that is not available, mass or mol ratios can be used instead, but it the analysis the mixing ratios will be assumed to be based on volumes.\nExample\n1\n4; 1 | 1\n1 >> 5; 2; 0.3 | 2; 1',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_solvents_supplier#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_solvents_supplier',
      description:
        'The suppliers of all the solvents.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent suppliers associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the associated suppliers and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- For non-liquid processes with no solvents, mark the supplier as \u2018none\u2019\n- If the supplier for a solvent is unknown, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nSigma Aldrich\nSigma Aldrich; Fisher | Acros\nnone >> Sigma Aldrich; Sigma Aldrich | Unknown',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_solvents_purity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_solvents_purity',
      description:
        'The purity of the solvents used.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent purities associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the associated purities and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- Use standard nomenclature for purities, e.g. pro analysis, puris, extra dry, etc.\n- For non-liquid processes with no solvents, state the purity as \u2018none\u2019\n- If the purity for a solvent is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nPro analysis\nPuris; Puris| Tecnical\nnone >> Pro analysis; Pro analysis | Unknown',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_reaction_solutions_compounds#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_reaction_solutions_compounds',
      description:
        'The non-solvent precursor chemicals used in each reaction step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the non-solvent chemicals associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several compounds, e.g. A and B, list the associated compounds in alphabetic order and separate them with semicolons, as in (A; B)\n- Note that also dopants/additives should be included\n- When several precursor solutions are made and mixed before the reaction step, it is the properties of the final mixture used in the reaction we here describe.\n- The number and order of layers and reaction steps must line up with the previous columns.\n- For gas phase reactions, state the reaction gases as if they were in solution.\n- For solid-state reactions, state the compounds as if they were in solution.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019\n- If the compounds for a deposition step is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nAu\nCuI\nAg',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_reaction_solutions_compounds_supplier#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_reaction_solutions_compounds_supplier',
      description:
        'The suppliers of the non-solvent chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the non-solvent chemical suppliers associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, e.g. A and B, list the associated suppliers and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- For gas phase reactions, state the suppliers for the gases or the targets/evaporation sources that are evaporated/sputtered/etc.\n- For solid state reactions, state the suppliers for the compounds in the same way.\n- For reaction steps involving only pure solvents, state the supplier as \u2018none\u2019 (as that that is entered in a separate filed)\n- For chemicals that are lab made, state that as \u201cLab made\u201d or \u201cLab made (name of lab)\u201d\n- If the supplier for a compound is unknown, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nDysole; Sigma Aldrich; Dyenamo; Sigma Aldrich\nSigma Aldrich; Fisher | Acros\nLab made (EPFL) | Sigma Aldrich >> none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_reaction_solutions_compounds_purity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_reaction_solutions_compounds_purity',
      description:
        'The purity of the non-solvent chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the compound purities associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, i.e. A and B, list the associated purities and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- Use standard nomenclature for purities, e.g. pro analysis, puris, extra dry, etc.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019 (as that is stated in another field)\n- If the purity for a compound is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nPro analysis\n99.999; Puris| Tecnical\nUnknown >> Pro analysis; Pro analysis | none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_reaction_solutions_concentrations#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_reaction_solutions_concentrations',
      description:
        'The concentration of the non-solvent precursor chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the concentrations associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, e.g. A and B, list the associated concentrations and separate them with semicolons, as in (A; B)\n- The order of the compounds must be the same as in the previous filed.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019\n- When concentrations are unknown, state that as \u2018nan\u2019\n- Concentrations can be stated in different units suited for different situations. Therefore, specify the unit used. When possible, use one of the preferred units\no M, mM, molal; g/ml, mg/ml, \u00b5g/ml, wt%, mol%, vol%, ppt, ppm, ppb\n- For values with uncertainties, state the best estimate, e.g write 4 wt% and not 3-5 wt%.\nExample\n4 wt%\n0.2 M; 0.15 M| 10 mg/ml\n0.3 mol% | 2 mol%; 0.2 wt% | nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_reaction_solutions_volumes#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_reaction_solutions_volumes',
      description:
        'The volume of the reaction solutions\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the volumes associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The volumes refer the volumes used, not the volume of the stock solutions. Thus if 0.15 ml of a solution is spin-coated, the volume is 0.15 ml\n- For reaction steps without solvents, state the volume as \u2018nan\u2019\n- When volumes are unknown, state that as \u2018nan\u2019\nExample\n0.1\n0.1 >> 0.05 | 0.05\nnan | 0.15',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_reaction_solutions_age#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_reaction_solutions_age',
      description:
        'The age of the solutions\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the age of the solutions associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- As a general guideline, the age refers to the time from the preparation of the final precursor mixture to the reaction procedure.\n- When the age of a solution is not known, state that as \u2018nan\u2019\n- For reaction steps where no solvents are involved, state this as \u2018nan\u2019\n- For solutions that is stored a long time, an order of magnitude estimate is adequate.\nExample\n2\n0.25 |1000 >> 10000\nnan | nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_reaction_solutions_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_reaction_solutions_temperature',
      description:
        'The temperature of the reaction solutions.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the temperatures of the solutions associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a reaction solution undergoes a temperature program, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons, e.g. 25; 100\n- When the temperature of a solution is unknown, state that as \u2018nan\u2019\n- For reaction steps where no solvents are involved, state the temperature of the gas or the solid if that make sense. Otherwise state this as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- Assume an undetermined room temperature to be 25\nExample\n25\n100; 50 | 25\nnan | 25 >> 25',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_substrate_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_substrate_temperature',
      description:
        'The temperature of the substrate.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the temperatures of the substrates (i.e. the last deposited layer) associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The temperature of the substrate refers to the temperature when the deposition of the layer is occurring.\n- If a substrate undergoes a temperature program before the deposition, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons (e.g. 25; 100)\n- When the temperature of a substrate is not known, state that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- Assume that an undetermined room temperature is 25\nExample\n25\nnan\n125; 325; 375; 450 | 25 >> 25',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_thermal_annealing_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_thermal_annealing_temperature',
      description:
        'The temperatures of the thermal annealing program associated with depositing the layers\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the annealing temperatures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the thermal annealing involves a temperature program with multiple temperature stages, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons (e.g. 25; 100)\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- If no thermal annealing is occurring after the deposition of a layer, state that by stating the room temperature (assumed to 25\u00b0C if not further specified)\n- If the thermal annealing program is not known, state that by \u2018nan\u2019\nExample\n25\n50 | nan\n450 | 125; 325; 375; 450 >> 125; 325; 375; 450',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_thermal_annealing_time#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_thermal_annealing_time',
      description:
        'The time program associated to the thermal annealing program.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the annealing times associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the thermal annealing involves a temperature program with multiple temperature stages, list the associated times at those temperatures and separate them with semicolons.\n- The annealing times must align in terms of layers\u00b8 reaction steps and annealing temperatures in the previous filed.\n- If a time is not known, state that by \u2018nan\u2019\n- If no thermal annealing is occurring after the deposition of a layer, state that by \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 20 and not 10-30.\nExample\nnan\n60 | 1000\n30 | 5; 5; 5; 30 >> 5; 5; 5; 30',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_deposition_thermal_annealing_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_deposition_thermal_annealing_atmosphere',
      description:
        'The atmosphere during thermal annealing\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the atmospheres associated to each annelaing step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the atmosphere is a mixture of different gases, i.e. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas.\n- This is often the same as the atmosphere under which the deposition is occurring, but not always.\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nN2\nVacuum | N2\nAir | Ar >> Ar',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_storage_time_until_next_deposition_step#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_storage_time_until_next_deposition_step',
      description:
        'The time between the back contact is finalised and the next layer is deposited\n- If there are uncertainties, only state the best estimate, e.g. write 35 and not 20-50.\n- If this is the last layer in the stack, state this as \u2018nan\u2019',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_storage_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_storage_atmosphere',
      description:
        'The atmosphere in which the sample with the finalised back contact is stored until the next deposition step or device performance measurement\nExample\nAir\nN2\nVacuum',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_storage_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_storage_relative_humidity',
      description:
        'The relive humidity under which the sample with the finalised back contact is stored until the next deposition step or device performance measurement\n- If there are uncertainties, only state the best estimate, e.g write 35 and not 20-50.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_front_surface_treatment_before_next_deposition_step#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_front_surface_treatment_before_next_deposition_step',
      description:
        'Description of any type of surface treatment or other treatment the sample with the finalised back contact is stored until the next deposition step or device performance measurement\n- If more than one treatment, list the treatments and separate them by a double forward angel bracket (\u2018 >> \u2018)\n- If no special treatment, state that as \u2018none\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample:\nnone\nAr plasma\nUV-ozone',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back',
      description:
        'TRUE if there is a functional layer above the back contact, i.e. layers deposited after the back contact has been finalised.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_function#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_function',
      description:
        'The function of the additional layers on the backcontact side.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If a layer has more than one function, e.g. A and B, list the functions in order and separate them with semicolons, as in (A; B)\nExample:\nA.R.C.\nBack reflection\nDown conversion\nEncapsulation\nLight management\nUpconversion',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_stack_sequence#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_stack_sequence',
      description:
        'The stack sequence describing the additional layers on the backcontact side.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If two materials, e.g. A and B, are mixed in one layer, list the materials in alphabetic order and separate them with semicolons, as in (A; B)\n- Use common abbreviations when appropriate but spell it out if risk for confusion.\n- There are now separate filed for doping. Indicate doping with colons. E.g. wither aluminium doped NiO-np as Al:NiO-np\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_thickness_list#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_thickness_list',
      description:
        'A list of thicknesses of the individual layers in the stack.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- The layers must line up with the previous filed.\n- State thicknesses in nm\n- Every layer in the stack have a thickness. If it is unknown, state this as \u2018nan\u2019\n- If there are uncertainties, state the best estimate, e.g write 100 and not 90-110',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_additives_compounds#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_additives_compounds',
      description:
        'List of the dopants and additives that are in each layer of the HTL-stack\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- The layers must line up with the previous fields.\n- If several dopants/additives, e.g. A and B, are present in one layer, list the dopants/additives in alphabetic order and separate them with semicolons, as in (A; B)\n- If no dopants/additives, state that as \u201cUndoped\u201d\n- If the doping situation is unknown, stat that as\u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template, even if to most common back contacts is undoped metals\nExample\nCuS\nB; P\nAu-np | Undoped',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_additives_concentrations#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_additives_concentrations',
      description:
        'The concentration of the dopants/additives.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If more than one dopant/additive in the layer, e.g. A and B, separate the concentration for each dopant/additive with semicolons, as in (A; B)\n- For each dopant/additive in the layer, state the concentration.\n- The order of the dopants/additives must be the same as in the previous filed.\n- For layers with no dopants/additives, state this as \u2018none\u2019\n- When concentrations are unknown, state that as \u2018nan\u2019\n- Concentrations can be stated in different units suited for different situations. Therefore, specify the unit used.\n- The preferred way to state the concentration of a dopant/additive is to refer to the amount in the final product, i.e. the material in the layer. When possible, use on the preferred units\no wt%, mol%, vol%, ppt, ppm, ppb\n- When the concentration of the dopant/additive in the final product is unknown, but where the concentration of the dopant/additive in the solution is known, state that concentration instead. When possible, use on the preferred units\no M, mM, molal; g/ml, mg/ml, \u00b5g/ml\n- For values with uncertainties, state the best estimate, e.g write 4 wt% and not 3-5 wt%.\nExample\n4 wt%\n5 vol%; nan | 10 mg/ml\n0.3 mol% | 2 mol%; 0.2 wt% | 0.3 M',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_procedure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_procedure',
      description:
        'The deposition procedures for the HTL-stack.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate them by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- Thermal annealing is generally not considered as an individual reaction step. The philosophy behind this is that every deposition step has a thermal history, which is specified in a separate filed. In exceptional cases with thermal annealing procedures clearly disconnected from other procedures, state \u2018Thermal annealing\u2019 as a separate reaction step.\n- Please read the instructions under \u201cPerovskite. Deposition. Procedure\u201d for descriptions and distinctions between common deposition procedures and how they should be labelled for consistency in the database.\n- A few additional clarifications:\n- Lamination\no A readymade film is transferred directly to the device stack. A rather broad concept. An everyday kitchen related example of lamination would eb to place a thin plastic film over a slice of pie.\n- Sandwiching\no When a readymade top stack simply is placed on top of the device stack. Could be held together with clams. The typical example is a when a \u201cCarbon | FTO | SLG\u201d is placed on top of the device stack. Standard procedure in the DSSC filed.\nExample\nEvaporation\nEvaporation | Evaporation\nDoctor blading\nScreen printing\nSputtering\nLamination\nE-beam evaporation\nSandwiching',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_aggregation_state_of_reactants#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_aggregation_state_of_reactants',
      description:
        'The physical state of the reactants.\n- The three basic categories are Solid/Liquid/Gas\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the aggregation state associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- Most cases are clear cut, e.g. spin-coating involves species in solution and evaporation involves species in gas phase. For less clear-cut cases, consider where the reaction really is happening as in:\no For a spray-coating procedure, it is droplets of liquid that enters the substrate (thus a liquid phase reaction)\no For sputtering and thermal evaporation, it is species in gas phase that reaches the substrate (thus a gas phase reaction)\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nLiquid\nGas | Liquid\nLiquid | Liquid >> Liquid',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_synthesis_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_synthesis_atmosphere',
      description:
        'The synthesis atmosphere.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the atmospheres associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the synthesis atmosphere is a mixture of different gases, e.g. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nVacuum\nVacuum | N2\nAir | Ar; H2O >> Ar',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_synthesis_atmosphere_pressure_total#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_synthesis_atmosphere_pressure_total',
      description:
        'The total gas pressure during each reaction step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the pressures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- Pressures can be stated in different units suited for different situations. Therefore, specify the unit. The preferred units are:\no atm, bar, mbar, mmHg, Pa, torr, psi\n- If a pressure is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 100 pa and not 80-120 pa.\nExample\n1 atm\n0.002 torr | 10000 Pa\nnan >> 1 atm | 1 atm',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_synthesis_atmosphere_pressure_partial#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_synthesis_atmosphere_pressure_partial',
      description:
        'The partial pressures for the gases present during each reaction step.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the pressures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the synthesis atmosphere is a mixture of different gases, e.g. A and B, list the partial pressures and separate them with semicolons, as in (A; B). The list of partial pressures must line up with the gases they describe.\n- In cases where no gas mixtures are used, this field will be the same as the previous filed.\nExample\n1 atm\n0.002 torr | 10000 Pa\nnan >> 0.99 atm; 0.01 atm | 1 atm',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_synthesis_atmosphere_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_synthesis_atmosphere_relative_humidity',
      description:
        'The relative humidity during each deposition step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the relative humidity associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns\n- If the relative humidity for a step is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 35 and not 30-40.\nExample\n35\n0 | 20\nnan >> 25 | 0',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_solvents#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_solvents',
      description:
        'The solvents used in each deposition procedure for each layer in the stack\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvents associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the solvents in alphabetic order and separate them with semicolons, as in (A; B)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- For non-liquid processes with no solvents, state the solvent as \u2018none\u2019\n- If the solvent is not known, state this as \u2018Unknown\u2019\n- Use common abbreviations when appropriate but spell it out when risk for confusion\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nnone\nAcetonitile; Ethanol | Chlorobenzene\nnone >> Ethanol; Methanol; H2O | DMF; DMSO',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_solvents_mixing_ratios#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_solvents_mixing_ratios',
      description:
        'The mixing ratios for mixed solvents\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent mixing ratios associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- For pure solvents, state the mixing ratio as 1\n- For non-solvent processes, state the mixing ratio as 1\n- For unknown mixing ratios, state the mixing ratio as \u2018nan\u2019\n- For solvent mixtures, i.e. A and B, state the mixing ratios by using semicolons, as in (VA; VB)\n- The preferred metrics is the volume ratios. If that is not available, mass or mol ratios can be used instead, but it the analysis the mixing ratios will be assumed to be based on volumes.\nExample\n1\n4; 1 | 1\n1 >> 5; 2; 0.3 | 2; 1',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_solvents_supplier#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_solvents_supplier',
      description:
        'The suppliers of all the solvents.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent suppliers associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the associated suppliers and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- For non-liquid processes with no solvents, mark the supplier as \u2018none\u2019\n- If the supplier for a solvent is unknown, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nSigma Aldrich\nSigma Aldrich; Fisher | Acros\nnone >> Sigma Aldrich; Sigma Aldrich | Unknown',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_solvents_purity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_solvents_purity',
      description:
        'The purity of the solvents used.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the solvent purities associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solvent is a mixture of different solvents, e.g. A and B, list the associated purities and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- Use standard nomenclature for purities, e.g. pro analysis, puris, extra dry, etc.\n- For non-liquid processes with no solvents, state the purity as \u2018none\u2019\n- If the purity for a solvent is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nPro analysis\nPuris; Puris| Tecnical\nnone >> Pro analysis; Pro analysis | Unknown',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_reaction_solutions_compounds#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_reaction_solutions_compounds',
      description:
        'The non-solvent precursor chemicals used in each reaction step\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the non-solvent chemicals associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several compounds, e.g. A and B, list the associated compounds in alphabetic order and separate them with semicolons, as in (A; B)\n- Note that also dopants/additives should be included\n- When several precursor solutions are made and mixed before the reaction step, it is the properties of the final mixture used in the reaction we here describe.\n- The number and order of layers and reaction steps must line up with the previous columns.\n- For gas phase reactions, state the reaction gases as if they were in solution.\n- For solid-state reactions, state the compounds as if they were in solution.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019\n- If the compounds for a deposition step is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nAu\nCuI\nAg',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_reaction_solutions_compounds_supplier#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_reaction_solutions_compounds_supplier',
      description:
        'The suppliers of the non-solvent chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the non-solvent chemical suppliers associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, e.g. A and B, list the associated suppliers and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- For gas phase reactions, state the suppliers for the gases or the targets/evaporation sources that are evaporated/sputtered/etc.\n- For solid state reactions, state the suppliers for the compounds in the same way.\n- For reaction steps involving only pure solvents, state the supplier as \u2018none\u2019 (as that that is entered in a separate filed)\n- For chemicals that are lab made, state that as \u201cLab made\u201d or \u201cLab made (name of lab)\u201d\n- If the supplier for a compound is unknown, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nDysole; Sigma Aldrich; Dyenamo; Sigma Aldrich\nSigma Aldrich; Fisher | Acros\nLab made (EPFL) | Sigma Aldrich >> none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_reaction_solutions_compounds_purity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_reaction_solutions_compounds_purity',
      description:
        'The purity of the non-solvent chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the compound purities associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, i.e. A and B, list the associated purities and separate them with semicolons, as in (A; B)\n- The number and order of layers, reaction steps, and solvents must line up with the previous columns.\n- Use standard nomenclature for purities, e.g. pro analysis, puris, extra dry, etc.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019 (as that is stated in another field)\n- If the purity for a compound is not known, state this as \u2018Unknown\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is short. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nPro analysis\n99.999; Puris| Tecnical\nUnknown >> Pro analysis; Pro analysis | none',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_reaction_solutions_concentrations#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_reaction_solutions_concentrations',
      description:
        'The concentration of the non-solvent precursor chemicals.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the concentrations associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a solution contains several dissolved compounds, e.g. A and B, list the associated concentrations and separate them with semicolons, as in (A; B)\n- The order of the compounds must be the same as in the previous filed.\n- For reaction steps involving only pure solvents, state this as \u2018none\u2019\n- When concentrations are unknown, state that as \u2018nan\u2019\n- Concentrations can be stated in different units suited for different situations. Therefore, specify the unit used. When possible, use one of the preferred units\no M, mM, molal; g/ml, mg/ml, \u00b5g/ml, wt%, mol%, vol%, ppt, ppm, ppb\n- For values with uncertainties, state the best estimate, e.g write 4 wt% and not 3-5 wt%.\nExample\n4 wt%\n0.2 M; 0.15 M| 10 mg/ml\n0.3 mol% | 2 mol%; 0.2 wt% | nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_reaction_solutions_volumes#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_reaction_solutions_volumes',
      description:
        'The volume of the reaction solutions\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the volumes associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The volumes refer the volumes used, not the volume of the stock solutions. Thus if 0.15 ml of a solution is spin-coated, the volume is 0.15 ml\n- For reaction steps without solvents, state the volume as \u2018nan\u2019\n- When volumes are unknown, state that as \u2018nan\u2019\nExample\n0.1\n0.1 >> 0.05 | 0.05\nnan | 0.15',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_reaction_solutions_age#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_reaction_solutions_age',
      description:
        'The age of the solutions\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the age of the solutions associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- As a general guideline, the age refers to the time from the preparation of the final precursor mixture to the reaction procedure.\n- When the age of a solution is not known, state that as \u2018nan\u2019\n- For reaction steps where no solvents are involved, state this as \u2018nan\u2019\n- For solutions that is stored a long time, an order of magnitude estimate is adequate.\nExample\n2\n0.25 |1000 >> 10000\nnan | nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_reaction_solutions_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_reaction_solutions_temperature',
      description:
        'The temperature of the reaction solutions.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the temperatures of the solutions associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If a reaction solution undergoes a temperature program, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons, e.g. 25; 100\n- When the temperature of a solution is unknown, state that as \u2018nan\u2019\n- For reaction steps where no solvents are involved, state the temperature of the gas or the solid if that make sense. Otherwise state this as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- Assume an undetermined room temperature to be 25\nExample\n25\n100; 50 | 25\nnan | 25 >> 25',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_substrate_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_substrate_temperature',
      description:
        'The temperature of the substrate.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the temperatures of the substrates (i.e. the last deposited layer) associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The temperature of the substrate refers to the temperature when the deposition of the layer is occurring.\n- If a substrate undergoes a temperature program before the deposition, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons (e.g. 25; 100)\n- When the temperature of a substrate is not known, state that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- Assume that an undetermined room temperature is 25\nExample\n25\nnan\n125; 325; 375; 450 | 25 >> 25',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_thermal_annealing_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_thermal_annealing_temperature',
      description:
        'The temperatures of the thermal annealing program associated with depositing the layers\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the annealing temperatures associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the thermal annealing involves a temperature program with multiple temperature stages, list the temperatures (e.g. start, end, and other important points) and separate them with semicolons (e.g. 25; 100)\n- For values with uncertainties, state the best estimate, e.g. write 120 and not 110-130.\n- If no thermal annealing is occurring after the deposition of a layer, state that by stating the room temperature (assumed to 25\u00b0C if not further specified)\n- If the thermal annealing program is not known, state that by \u2018nan\u2019\nExample\n25\n50 | nan\n450 | 125; 325; 375; 450 >> 125; 325; 375; 450',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_thermal_annealing_time#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_thermal_annealing_time',
      description:
        'The time program associated to the thermal annealing program.\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the annealing times associated to each reaction step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the thermal annealing involves a temperature program with multiple temperature stages, list the associated times at those temperatures and separate them with semicolons.\n- The annealing times must align in terms of layers\u00b8 reaction steps and annealing temperatures in the previous filed.\n- If a time is not known, state that by \u2018nan\u2019\n- If no thermal annealing is occurring after the deposition of a layer, state that by \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 20 and not 10-30.\nExample\nnan\n60 | 1000\n30 | 5; 5; 5; 30 >> 5; 5; 5; 30',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_deposition_thermal_annealing_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_deposition_thermal_annealing_atmosphere',
      description:
        'The atmosphere during thermal annealing\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- When more than one reaction step, separate the atmospheres associated to each annelaing step by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- The number and order of layers and deposition steps must line up with the previous columns.\n- If the atmosphere is a mixture of different gases, i.e. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas.\n- This is often the same as the atmosphere under which the deposition is occurring, but not always.\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nN2\nVacuum | N2\nAir | Ar >> Ar',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_storage_time_until_next_deposition_step#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_storage_time_until_next_deposition_step',
      description:
        'The time between the back contact is finalised and the next layer is deposited\n- If there are uncertainties, only state the best estimate, e.g. write 35 and not 20-50.\n- If this is the last layer in the stack, state this as \u2018nan\u2019',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_storage_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_storage_atmosphere',
      description:
        'The atmosphere in which the sample with the finalised back contact is stored until the next deposition step or device performance measurement\nExample\nAir\nN2\nVacuum',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_storage_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_storage_relative_humidity',
      description:
        'The relive humidity under which the sample with the finalised back contact is stored until the next deposition step or device performance measurement\n- If there are uncertainties, only state the best estimate, e.g write 35 and not 20-50.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.add.lay_back_surface_treatment_before_next_deposition_step#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lay_back_surface_treatment_before_next_deposition_step',
      description:
        'Description of any type of surface treatment or other treatment the sample with the finalised back contact is stored until the next deposition step or device performance measurement\n- If more than one treatment, list the treatments and separate them by a double forward angel bracket (\u2018 >> \u2018)\n- If no special treatment, state that as \u2018none\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample:\nnone\nAr plasma\nUV-ozone',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.encapsulation.Encapsulation#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'Encapsulation',
      description: 'TRUE if the cell is encapsulated',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.encapsulation.stack_sequence#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'stack_sequence',
      description:
        'The stack sequence of the encapsulation\n- Every layer should be separated by a space, a vertical bar, and a space, i.e. (\u2018 | \u2018)\n- If two materials, e.g. A and B, are mixed in one layer, list the materials in alphabetic order and separate them with semicolons, as in (A; B)\n- Use common abbreviations when appropriate but spell it out if risk for confusion.\n- There are now separate filed for doping. Indicate doping with colons. E.g. wither aluminium doped NiO-np as Al:NiO-np\nExample:\nSLG\nEpoxy\nCover glass\nPMMA',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.encapsulation.edge_sealing_materials#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'edge_sealing_materials',
      description:
        'Edge sealing materials\n- If two materials, e.g. A and Bare used, list the materials in alphabetic order and separate them with semicolons, as in (A; B)\nExample:\nEpoxy\nSurlyn\nUV-glue',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.encapsulation.atmosphere_for_encapsulation#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'atmosphere_for_encapsulation',
      description:
        'The surrounding atmosphere during encapsulation.\n- If the surrounding atmosphere is a mixture of different gases, e.g. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas\nExample\nN2\nVacuum\nAir',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.encapsulation.water_vapour_transmission_rate#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'water_vapour_transmission_rate',
      description:
        'The water vapour transmission rate trough the encapsulation.\n- If there are uncertainties, only state the best estimate, e.g. write 35 and not 20-50.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.encapsulation.oxygen_transmission_rate#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'oxygen_transmission_rate',
      description:
        'The oxygen transmission rate trough the encapsulation.\n- If there are uncertainties, only state the best estimate, e.g. write 35 and not 20-50.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.data_file#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'data_file',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.measured#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'measured',
      description: 'TRUE if IV-data has been measured and is reported.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.average_over_n_number_of_cells#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'average_over_n_number_of_cells',
      description:
        'The number of cells the reported IV data is based on.\n- The preferred way to enter data is to give every individual cell its own entry in the data template/data base. If that is done, the data is an average over 1 cell.\n- If the reported IV data is not the data from one individual cell, but an average over N cells. Give the number of cells.\n- If the reported value is an average, but it is unknown over how many cells the value has been averaged (and no good estimate is available), state the number of cells as 2, which is the smallest number of cells that qualifies for an averaging procedure.',
      type: {
        type_kind: 'numpy',
        type_data: 'int64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.certified_values#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'certified_values',
      description:
        'TRUE if the IV data is measured by an independent and certification institute. If your solar simulator is calibrated by a calibrated reference diode, that does not count as a certified result.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.certification_institute#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'certification_institute',
      description:
        'The name of the certification institute that has measured the certified device.\nExample:\nNewport\nNIM, National Institute of Metrology of China\nKIER, Korea Institute of Energy Research',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.storage_age_of_cell#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_age_of_cell',
      description:
        'The age of the cell with respect to when the last deposition step was finalised.\n- If there are uncertainties, only state the best estimate, e.g. write 3 and not 1-5.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.storage_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_atmosphere',
      description:
        'The atmosphere in which the sample was stored between the device finalisation and the IV measurement.\n- If the atmosphere is a mixture of different gases, e.g. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas\n- If the atmosphere has changed during the storing time, separate the different atmospheres by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nN2\nAir\nN2 >> Air',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.storage_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'storage_relative_humidity',
      description:
        'The relative humidity in the atmosphere in which the sample was stored between the device finalisation and the IV measurement.\n- If the relative humidity has changed during the storing time, separate the different relative humidity by a double forward angel bracket with one blank space on both sides (\u2018 >> \u2018)\n- If the relative humidity is not known, stat that as \u2018nan\u2019\n- For values with uncertainties, state the best estimate, e.g. write 35 and not 30-40.\nExample\n35\n0\n0 >> 25',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.test_atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'test_atmosphere',
      description:
        'The atmosphere in which the IV measurement is conducted\n- If the atmosphere is a mixture of different gases, e.g. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas\nExample\nAir\nN2\nVacuum',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.test_relative_humidity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'test_relative_humidity',
      description:
        'The relive humidity in which the IV measurement is conducted\n- If there are uncertainties, only state the best estimate, e.g write 35 and not 20-50.\n- If the relative humidity is not known, stat that as \u2018nan\u2019',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.test_temperature#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'test_temperature',
      description:
        'The temperature of the device during the IV-measurement\n- If the temperature is not controlled and not is known, assume a standard room temperature of 25\u00b0C.\n- If there are uncertainties, only state the best estimate, e.g write 35 and not 20-50.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'degree_Celsius',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.light_source_type#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_source_type',
      description:
        'The type of light source used during the IV-measurement\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\n- The category Solar simulator should only be used when you do not really know which type of light source you have in your solar simulator.\nExample:\nLaser\nMetal halide\nOutdoor\nSolar simulator\nSulfur plasma\nWhite LED\nXenon plasma',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.light_source_brand_name#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_source_brand_name',
      description:
        'The brand name and model number of the light source/solar simulator used\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample:\nNewport model 91192\nNewport AAA\nAtlas suntest',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.light_source_simulator_class#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_source_simulator_class',
      description:
        'The class of the solar simulator\n- A three-letter code of As, Bs, and Cs. The order of the letters represents the quality ofspectral match, spatial non-uniformity, and temporal instability\nExample\nAAA\nABB\nCAB',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.light_intensity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_intensity',
      description:
        'The light intensity during the IV measurement\n- If there are uncertainties, only state the best estimate, e.g. write 100 and not 90-100.\n- Standard AM 1.5 illumination correspond to 100 mW/cm2\n- If you need to convert from illumination given in lux; at 550 nm, 1 mW/cm2 corresponds to 6830 lux. Be aware that the conversion change with the spectrum used. As a rule of thumb for general fluorescent/LED light sources, around 0.31mW corresponded to 1000 lux. If your light intensity is measured in lux, it probably means that your light spectra deviates quite a lot from AM 1.5, wherefore it is very important that you also specify the light spectra in the next column.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'milliwatt / centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.light_spectra#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_spectra',
      description:
        'The light spectrum used (or simulated as best as possible) during the IV measurement\nExample\nAM 1.0\nAM 1.5\nIndoor light\nMonochromatic\nOutdoor\nUV',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.light_wavelength_range#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_wavelength_range',
      description:
        'The wavelength range of the light source\n- Separate the lower and upper bound by a semicolon.\n- For monochromatic light sources, only give the constant value.\n- If there are uncertainties, only state the best estimate, e.g. write 100 and not 90-100.\n- State unknown values as \u2018nan\u2019\nExample:\n330; 1000\n400; nan\n550',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.light_illumination_direction#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_illumination_direction',
      description:
        'The direction of the illumination with respect to the device stack\n- If the cell is illuminated trough the substrate, state this as \u2018Substrate\u2019\n- If the cell is illuminated trough the top contact, state this as \u2018Superstrate\u2019\n- For back contacted cells illuminated from the non-contacted side, state this as \u2018Superstrate\u2019\nExample\nSubstrate\nSuperstrate',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.light_masked_cell#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_masked_cell',
      description:
        'TRUE if the cell is illuminated trough a mask with an opening that is smaller than the total cell area.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.light_mask_area#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_mask_area',
      description:
        'The area of the opening in the mask trough with the cell is illuminated (if there is a mask)\n- If there are uncertainties, only state the best estimate, e.g. write 100 and not 90-100.\n- If there is no light mask, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.scan_speed#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'scan_speed',
      description: 'The speed of the potential sweep during the IV measurement',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'millivolt / second',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.scan_delay_time#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'scan_delay_time',
      description:
        'The time at each potential value before integration in the potential sweep.\n- For some potentiostats you need to specify this value, whereas for others it is set automatically and is not directly accessible.\n- If there are uncertainties, only state the best estimate, e.g. write 100 and not 90-100.\n- If unknown, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'millisecond',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.scan_integration_time#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'scan_integration_time',
      description:
        'The integration time at each potential value in the potential sweep.\n- For some potentiostats you need to specify this value, whereas for others it is set automatically and is not directly accessible.\n- If there are uncertainties, only state the best estimate, e.g. write 100 and not 90-100.\n- If unknown, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'millisecond',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.scan_voltage_step#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'scan_voltage_step',
      description:
        'The distance between the measurement point in the potential sweep\n- If unknown, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'millivolt',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.preconditioning_protocol#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'preconditioning_protocol',
      description:
        'Any preconditioning protocol done immediately before the IV measurement\n- If no preconditioning was done, state this as \u2018none\u2019\n- If more than one preconditioning protocol was conducted in parallel, separate them with semicolons\n- If more than one preconditioning protocol was conducted in sequence, separate them by a double forward angel bracket (\u2018 >> \u2018)\nExample\nCooling\nHeeting\nLight soaking\nLight soaking; Potential biasing\nPotential biasing',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.preconditioning_time#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'preconditioning_time',
      description:
        'The duration of the preconditioning protocol\n- If there are uncertainties, only state the best estimate, e.g. write 100 and not 90-100.\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'second',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.preconditioning_potential#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'preconditioning_potential',
      description:
        'The potential at any potential biasing step\n- If there are uncertainties, only state the best estimate, e.g. write 100 and not 90-100.\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'volt',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.preconditioning_light_intensity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'preconditioning_light_intensity',
      description:
        'The light intensity at any light soaking step\n- If there are uncertainties, only state the best estimate, e.g. write 100 and not 90-100.\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'milliwatt / centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.reverse_scan_Voc#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reverse_scan_Voc',
      description:
        'The open circuit potential, Voc, at the reverse voltage sweep (when U scanned from Voc to 0)\n- Give Voc in volts [V]\n- If there are uncertainties, only state the best estimate, e.g. write 1.03 and not 1.01-1.05\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'volt',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.reverse_scan_Jsc#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reverse_scan_Jsc',
      description:
        'The short circuit current, Jsc, at the reverse voltage sweep (when U scanned from Voc to 0)\n- Give Jsc in mA/cm2\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'milliampere / centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.reverse_scan_FF#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reverse_scan_FF',
      description:
        'The fill factor, FF, at the reverse voltage sweep (when U scanned from Voc to 0)\n- Give FF as the ratio between Vmp*Jmp/(Voc*Jsc) which gives it a value between 0 and 1\n- If there are uncertainties, only state the best estimate, e.g. write 0.73 and not 0.7-0.76\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.reverse_scan_PCE#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reverse_scan_PCE',
      description:
        'The efficiency, PCE, at the reverse voltage sweep (when U scanned from Voc to 0)\n- Give the efficiency in %\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.reverse_scan_Vmp#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reverse_scan_Vmp',
      description:
        'The potential at the maximum power point, Vmp, at the reverse voltage sweep (when U scanned from Voc to 0)\n- Give Vmp in volts [V]\n- If there are uncertainties, only state the best estimate, e.g. write 1.03 and not 1.01-1.05\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'volt',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.reverse_scan_Jmp#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reverse_scan_Jmp',
      description:
        'The current density at the maximum power point, Jmp, at the reverse voltage sweep (when U scanned from Voc to 0)\n- Give Jmp in mA/cm2\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'milliampere / centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.reverse_scan_series_resistance#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reverse_scan_series_resistance',
      description:
        'The series resistance as extracted from the reverse voltage sweep (when U scanned from Voc to 0)',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'centimeter ** 2 * ohm',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.reverse_scan_shunt_resistance#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'reverse_scan_shunt_resistance',
      description:
        'The shunt resistance as extracted from the reverse voltage sweep (when U scanned from Voc to 0)',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'centimeter ** 2 * ohm',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.forward_scan_Voc#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'forward_scan_Voc',
      description:
        'The open circuit potential, Voc, at the forward voltage sweep (when U scanned from 0 to Voc)\n- Give Voc in volts [V]\n- If there are uncertainties, only state the best estimate, e.g. write 1.03 and not 1.01-1.05\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'volt',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.forward_scan_Jsc#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'forward_scan_Jsc',
      description:
        'The short circuit current, Jsc, at the forward voltage sweep (when U scanned from 0 to Voc)\n- Give Jsc in mA/cm2\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'milliampere / centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.forward_scan_FF#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'forward_scan_FF',
      description:
        'The fill factor, FF, at the forward voltage sweep (when U scanned from 0 to Voc)\n- Give FF as the ratio between Vmp*Jmp/(Voc*Jsc) which gives it a value between 0 and 1\n- If there are uncertainties, only state the best estimate, e.g. write 0.73 and not 0.7-0.76\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.forward_scan_PCE#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'forward_scan_PCE',
      description:
        'The efficiency, PCE, at the forward voltage sweep (when U scanned from 0 to Voc)\n- Give the efficiency in %\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.forward_scan_Vmp#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'forward_scan_Vmp',
      description:
        'The potential at the maximum power point, Vmp, at the forward voltage sweep (when U scanned from 0 to Voc)\n- Give Vmp in volts [V]\n- If there are uncertainties, only state the best estimate, e.g. write 1.03 and not 1.01-1.05\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'volt',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.forward_scan_Jmp#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'forward_scan_Jmp',
      description:
        'The current density at the maximum power point, Jmp, at the forward voltage sweep (when U scanned from 0 to Voc)\n- Give Jmp in mA/cm2\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'milliampere / centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.forward_scan_series_resistance#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'forward_scan_series_resistance',
      description:
        'The series resistance as extracted from the forward voltage sweep (when U scanned from 0 to Voc)',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'centimeter ** 2 * ohm',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.forward_scan_shunt_resistance#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'forward_scan_shunt_resistance',
      description:
        'The shunt resistance as extracted from the forward voltage sweep (when U scanned from 0 to Voc)',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'centimeter ** 2 * ohm',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.link_raw_data#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'link_raw_data',
      description:
        'A link to where the data file for the IV-data is stored\n- This is a beta feature. The plan is to create a file repository where the raw files for IV data can be stored and disseminated. With the link and associated protocols, it should be possible to programmatically access and analyse the raw IV-data.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.default_Voc#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'default_Voc',
      description: 'Open circuit voltage.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'volt',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.default_Jsc#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'default_Jsc',
      description: 'Short circuit current density.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'milliampere / centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.default_FF#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'default_FF',
      description: 'Fill factor.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.default_PCE#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'default_PCE',
      description: 'Power conversion efficiency.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.default_Voc_scan_direction#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'default_Voc_scan_direction',
      description: 'nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.default_Jsc_scan_direction#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'default_Jsc_scan_direction',
      description: 'nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.default_FF_scan_direction#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'default_FF_scan_direction',
      description: 'nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.default_PCE_scan_direction#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'default_PCE_scan_direction',
      description: 'nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.hysteresis_index#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'hysteresis_index',
      description: 'nan',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.jv_curve.n_values#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'n_values',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.jv_curve.cell_name#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'cell_name',
      description: 'Cell identification name.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.jv_curve.figures.label#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'label',
      description: 'Label shown in the plot selection.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.jv_curve.figures.index#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'index',
      description: 'Index of figure in the plot selection.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.jv.jv_curve.figures.open#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'open',
      description: 'Determines whether the figure is initially open or closed.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stabilised.performance_measured#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'performance_measured',
      description:
        'TRUE if a stabilised cell efficiency has been measured\n- A stabilised efficiency requires a continuous measurement. Measuring an IV-curve, storing the cell in the dark for a while, and then measure a new IV-curve does thus not count as a stabilised efficiency measurement.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stabilised.performance_procedure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'performance_procedure',
      description:
        'The Potentiostatic load condition during the stabilised performance measurement\nExamples:\nConstant current\nConstant potential\nMPPT\nPassive resistance\nShort circuit',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stabilised.performance_procedure_metrics#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'performance_procedure_metrics',
      description:
        'The metrics associated to the load condition in the previous filed\n- For measurement under constant current, state the current in mA/cm2\n- For measurement under constant potential. State the potential in V\n- For a measurement under constant resistive load, state the resistance in ohm',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stabilised.performance_measurement_time#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'performance_measurement_time',
      description: 'The duration of the stabilised performance measurement.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'minute',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stabilised.performance_PCE#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'performance_PCE',
      description:
        'The stabilised efficiency, PCE\n- Give the efficiency in %\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stabilised.performance_Vmp#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'performance_Vmp',
      description:
        'The stabilised Vmp\n- Give Vmp in volts [V]\n- If there are uncertainties, only state the best estimate, e.g. write 1.03 and not 1.01-1.05\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'volt',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stabilised.performance_Jmp#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'performance_Jmp',
      description:
        'The stabilised Jmp\n- Give Jmp in mA/cm2\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'milliampere / centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stabilised.performance_link_raw_data#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'performance_link_raw_data',
      description:
        'A link to where the data file for the stability measurement is stored\n- This is a beta feature. The plan is to create a file repository where the raw files for IV data can be stored and disseminated. With the link and associated protocols, it should be possible to programmatically access and analyse the raw IV-data.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.eqe_data_file#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'eqe_data_file',
      description:
        "eqe_array = Quantity(\n    type=np.dtype(np.float64), shape=['n_values'],\n                Drop here your eqe file and click save for processing.",
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.header_lines#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'header_lines',
      description: 'Number of header lines in the file.',
      type: {
        type_kind: 'numpy',
        type_data: 'int64',
      },
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.measured#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'measured',
      description: 'TRUE if the external quantum efficiency has been measured',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.light_bias#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_bias',
      description:
        'The light intensity of any bias light during the EQE measurement\n- If there are uncertainties, only state the best estimate, e.g. write 100 and not 90-100.\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'milliwatt / centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.bandgap_eqe#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'bandgap_eqe',
      description: 'Bandgap derived form the eqe in eV.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'electron_volt',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.integrated_Jsc#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'integrated_Jsc',
      description:
        'The integrated current from the EQE measurement\n- Give Jsc in mA/cm2\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'milliampere / centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.integrated_J0rad#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'integrated_J0rad',
      description:
        'The integrated J<sub>{0, Rad}</sub> from the EQE measurement\n- Give J<sub>{0, Rad}</sub> in mA/cm2\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'milliampere / centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.voc_rad#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'voc_rad',
      description: 'Radiative V<sub>oc</sub> derived from the eqe in V.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'volt',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.urbach_energy#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'urbach_energy',
      description: 'Urbach energy fitted from the eqe in eV.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'electron_volt',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.n_values#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'n_values',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.n_raw_values#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'n_raw_values',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.link_raw_data#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'link_raw_data',
      description:
        'A link to where the data file for the EQE measurement is stored\n- This is a beta feature. The plan is to create a file repository where the raw files for IV data can be stored and disseminated. With the link and associated protocols, it should be possible to programmatically access and analyse the raw IV-data.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.figures.label#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'label',
      description: 'Label shown in the plot selection.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.figures.index#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'index',
      description: 'Index of figure in the plot selection.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.eqe.figures.open#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'open',
      description: 'Determines whether the figure is initially open or closed.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.measured#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'measured',
      description:
        'TRUE if some kind of stability measurement has been done.\n- There is no sharp boundary between a stability measurement and a measurement of stabilised efficiency. Generally, a measurement under a few minutes is considered as a measurement of stabilised efficiency, whereas a stability measurement is sufficiently long for degradation to be seen (unless the device is really good)',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.protocol#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'protocol',
      description:
        'The stability protocol used for the stability measurement.\n- For a more detailed discussion on protocols and standard nomenclature for stability measurements, please see the following paper:\no Consensus statement for stability assessment and reporting for perovskite photovoltaics based on ISOS procedures byM. V. Khenkin et al. Nat. Energ. 2020. DOI10.1038/s41560-019-0529-5\nExample:\nISOS-D-1\nISOS-D-1I\nISOS-L-2\nISOS-T-3\nIEC 61215',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.average_over_n_number_of_cells#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'average_over_n_number_of_cells',
      description:
        'The number of cells the reported stability data is based on.\n- The preferred way to enter data is to give every individual cell its own entry in the data template/data base. If that is done, the data is an average over 1 cell.\n- If the reported stability data is not the data from one individual cell, but an average over N cells. Give the number of cells.\n- If the reported value is an average, but it is unknown over how many cells the value has been averaged (and no good estimate is available), state the number of cells as 2, which is the smallest number of cells that qualifies for an averaging procedure.',
      type: {
        type_kind: 'numpy',
        type_data: 'int64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.light_source_type#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_source_type',
      description:
        'The type of light source used during the stability measurement\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample:\nLaser\nMetal halide\nOutdoor\nSolar simulator\nSulfur plasma\nWhite LED\nXenon plasma',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.light_source_brand_name#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_source_brand_name',
      description:
        'The brand name and model number of the light source/solar simulator used\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample:\nNewport model 91192\nNewport AAA\nAtlas suntest',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.light_source_simulator_class#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_source_simulator_class',
      description:
        'The class of the solar simulator\n- A three-letter code of As, Bs, and Cs. The order of the letters represents the quality ofspectral match, spatial non-uniformity, and temporal instability\nExample\nAAA\nABB\nCAB',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.light_intensity#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_intensity',
      description:
        'The light intensity during the stability measurement\n- If there are uncertainties, only state the best estimate, e.g. write 100 and not 90-100.\n- Standard AM 1.5 illumination correspond to 100 mW/cm2\n- If you need to convert from illumination given in lux; at 550 nm, 1 mW/cm2 corresponds to 6830 lux',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'milliwatt / centimeter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.light_spectra#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_spectra',
      description:
        'The light spectrum used (or simulated as best as possible) during the stability measurement\n- For an unspecified light spectra (that not is dark), state this as \u2018Light\u2019\nExample\nAM 1.0\nAM 1.5\nIndoor light\nMonochromatic\nOutdoor\nUV',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.light_wavelength_range#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_wavelength_range',
      description:
        'The wavelength range of the light source\n- Separate the lower and upper bound by a semicolon.\n- For monochromatic light sources, only give the constant value.\n- If there are uncertainties, only state the best estimate, e.g. write 100 and not 90-100.\n- State unknown values as \u2018nan\u2019\nExample:\n330; 1000\n400; nan\n550',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.light_illumination_direction#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_illumination_direction',
      description:
        'The direction of the illumination with respect to the device stack\n- If the cell is illuminated trough the substrate, state this as \u2018Substrate\u2019\n- If the cell is illuminated trough the top contact, state this as \u2018Superstrate\u2019\n- For back contacted cells illuminated from the non-contacted side, state this as \u2018Superstrate\u2019\nExample\nSubstrate\nSuperstrate',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.light_load_condition#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_load_condition',
      description:
        'The load situation of the illumination during the stability measurement.\n- If the illumination is constant during the entire stability measurement, or if the cell is stored in the dark, state this as \u2018Constant\u2019.\n- If the situation periodically is interrupted by IV-measurements, continue to consider the load condition as constant\n- If there is a cycling between dark and light, state this as \u2018Cycled\u2019\n- If the illumination varies in an uncontrolled way, state this as \u2018Uncontrolled\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample:\nConstant\nCycled\nDay-Night cycle\nUncontrolled',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.light_cycling_times#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_cycling_times',
      description:
        'If the illumination load is cycled during the stability measurement, state the time in low light followed by the time in high light for the cycling period.\n- If not applicable, leave blank\nExample\n12; 12\n6; 10\nnan; nan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.light_UV_filter#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'light_UV_filter',
      description:
        'TRUE if a UV-filter of any kind was placed between the illumination source and the device during the stability measurement.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.potential_bias_load_condition#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'potential_bias_load_condition',
      description:
        'The Potentiostatic load condition during the stability measurement\n- When the cell is not connected to anything, state this as \u2018Open circuit\u2019\nExamples:\nConstant current\nConstant potential\nMPPT\nOpen circuit\nPassive resistance\nShort circuit',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.potential_bias_range#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'potential_bias_range',
      description:
        'The potential range during the stability measurement\n- Separate the lower and upper bound by a semicolon.\n- For constant values, state only that value.\n- For open circuit conditions, state this as \u2018nan\u2019\n- If there are uncertainties, only state the best estimate, e.g. write 1 and not 0.90-1.1\n- State unknown values as \u2018nan\u2019\nExample:\n0.9; 1.02\n1.5\nnan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.potential_bias_passive_resistance#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'potential_bias_passive_resistance',
      description:
        'The passive resistance in the measurement circuit if a resistor was used\n- Give the value in ohm\n- If there are uncertainties, only state the best estimate, e.g. write 1.03 and not 1.01-1.05\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'ohm',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.temperature_load_condition#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'temperature_load_condition',
      description:
        'The load situation of the temperature during the stability measurement.\n- If the temperature is constant during the entire stability measurement, state this as \u2018Constant\u2019.\n- If there is a cycling between colder and hotter conditions, state this as \u2018Cycled\u2019\n- If the temperature varies in an uncontrolled way, state this as \u2018Uncontrolled\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample:\nConstant\nUncontrolled\nCycled',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.temperature_range#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'temperature_range',
      description:
        'The temperature range during the stability measurement\n- Separate the lower and upper bound by a semicolon.\n- For constant values, state only that value.\n- If there are uncertainties, only state the best estimate, e.g. write 1 and not 0.90-1.1\n- State unknown values as \u2018nan\u2019\nExample:\n30\n25; 85\nnan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.temperature_cycling_times#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'temperature_cycling_times',
      description:
        'If the temperature is cycled during the stability measurement, state the time in low temperature followed by the time in high temperature for the cycling period.\n- If not applicable, leave blank\n- Separate the lower and upper bound by a semicolon.\n- If there are uncertainties, only state the best estimate, e.g. write 1 and not 0.90-1.1\n- State unknown values as \u2018nan\u2019\nExample:\n2; 2\n0.5; 10',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.temperature_ramp_speed#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'temperature_ramp_speed',
      description:
        'The temperature ramp speed\n- If there are uncertainties, only state the best estimate, e.g. write 1.03 and not 1.01-1.05\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'delta_degree_Celsius / minute',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.atmosphere#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'atmosphere',
      description:
        'The atmosphere in which the stability measurement is conducted\n- If the atmosphere is a mixture of different gases, e.g. A and B, list the gases in alphabetic order and separate them with semicolons, as in (A; B)\n- \u201cDry air\u201d represent air with low relative humidity but where the relative humidity is not known\n- \u201cAmbient\u201d represent air where the relative humidity is not known. For ambient conditions where the relative humidity is known, state this as \u201cAir\u201d\n- \u201cVacuum\u201d (of unspecified pressure) is for this purpose considered as an atmospheric gas\nExample\nAir\nN2\nVacuum',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.atmosphere_oxygen_concentration#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'atmosphere_oxygen_concentration',
      description:
        'The oxygen concentration in the atmosphere\n- If unknown, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.relative_humidity_load_conditions#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'relative_humidity_load_conditions',
      description:
        'The load situation of the relative humidity during the stability measurement.\n- If the relative humidity is constant during the entire stability measurement, state this as \u2018Constant\u2019.\n- If there is a cycling between dryer and damper conditions, state this as \u2018Cycled\u2019\n- If the relative humidity varies in an uncontrolled way, i.e. the cell is operated under ambient conditions, state this as \u2018Ambient\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample\nAmbient\nControlled\nCycled',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.relative_humidity_range#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'relative_humidity_range',
      description:
        'The relative humidity range during the stability measurement\n- Separate the lower and upper bound by a semicolon.\n- For constant values, state only that value.\n- If there are uncertainties, only state the best estimate, e.g. write 1 and not 0.90-1.1\n- State unknown values as \u2018nan\u2019\nExample:\n45\n35; 65\nnan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.relative_humidity_average_value#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'relative_humidity_average_value',
      description:
        'The average relative humidity during the stability measurement.\n- If there are uncertainties, only state the best estimate, e.g. write 1 and not 0.90-1.1\n- If unknown, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.time_total_exposure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'time_total_exposure',
      description:
        'The total duration of the stability measurement.\n- If there are uncertainties, only state the best estimate, e.g. write 1000 and not 950-1050',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.periodic_JV_measurements#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'periodic_JV_measurements',
      description:
        'TRUE if the stability measurement periodically is interrupted for JV-measurements. A typical example is a cell that is stored in the dark and once in a wile is take out from storage for an IV-measurement.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.periodic_JV_measurements_time_between_jv#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'periodic_JV_measurements_time_between_jv',
      description:
        'The average time between JV-measurement during the stability measurement.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.PCE_initial_value#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_initial_value',
      description:
        'The efficiency, PCE, of the cell before the stability measurement routine starts\n- Give the efficiency in %\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.PCE_burn_in_observed#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_burn_in_observed',
      description:
        'TRUE if the performance has a relatively fast initial decay after which the decay rate stabilises at a lower level.\n- There are no sharp boundary between an initial burn in phase an a catastrophic failure, but if the performance of the cell quickly degrade by more than half, it is stretching it a bit to label this as an initial burn in phase.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.PCE_end_of_experiment#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_end_of_experiment',
      description:
        'The efficiency, PCE, of the cell at the end of the stability routine\n- Give the efficiency in %\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.PCE_T95#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_T95',
      description:
        'The time after which the cell performance has degraded by 5 % with respect to the initial performance.\n- If there are uncertainties, only state the best estimate, e.g. write 1000 and not 950-1050\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.PCE_Ts95#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_Ts95',
      description:
        'The time after which the cell performance has degraded by 5 % with respect to the performance after any initial burn in phase.\n- If there are uncertainties, only state the best estimate, e.g. write 1000 and not 950-1050\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.PCE_T80#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_T80',
      description:
        'The time after which the cell performance has degraded by 20 % with respect to the initial performance.\n- If there are uncertainties, only state the best estimate, e.g. write 1000 and not 950-1050\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.PCE_Ts80#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_Ts80',
      description:
        'The time after which the cell performance has degraded by 20 % with respect to the performance after any initial burn in phase.\n- If there are uncertainties, only state the best estimate, e.g. write 1000 and not 950-1050\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.PCE_Te80#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_Te80',
      description:
        'An estimated T80 for cells that were not measured sufficiently long for them to degrade by 20 %. with respect to the initial performance.\n- This value will by definition have a significant uncertainty to it, as it is not measured but extrapolated under the assumption linearity but without a detailed and stabilised extrapolation protocol. This estimate is, however, not without value as it enables a rough comparison between all cells for with the stability has been measured.\n- If there is an experimental T80, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.PCE_Tse80#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_Tse80',
      description:
        'An estimated T80s for cells that was not measured sufficiently long for them to degrade by 20 %. with respect to the performance after any initial burn in phase.\n- This value will by definition have a significant uncertainty to it, as it is not measured but extrapolated under the assumption linearity but without a detailed and stabilised extrapolation protocol. This estimate is, however, not without value as it enables a ruff comparison between all cells for with the stability has been measured.\n- If there is an experimental T80s, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.PCE_after_1000_h#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_after_1000_h',
      description:
        'The efficiency, PCE, of the cell after 1000 hours\n- Give the efficiency in %\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.lifetime_energy_yield#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'lifetime_energy_yield',
      description:
        'The lifetime energy yield\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour * kilowatt / meter ** 2',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.flexible_cell_number_of_bending_cycles#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'flexible_cell_number_of_bending_cycles',
      description:
        'Number of bending cycles for a flexible cell in a mechanical stability test',
      type: {
        type_kind: 'numpy',
        type_data: 'int64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.flexible_cell_bending_radius#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'flexible_cell_bending_radius',
      description:
        'The bending radius of the flexible cell during the mechanical stability test',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'degree',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.flexible_cell_PCE_initial_value#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'flexible_cell_PCE_initial_value',
      description:
        'The efficiency, PCE, of the cell before the mechanical stability measurement routine starts\n- Give the efficiency in %\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.flexible_cell_PCE_end_of_experiment#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'flexible_cell_PCE_end_of_experiment',
      description:
        'The efficiency, PCE, of the cell after the mechanical stability measurement routine\n- Give the efficiency in %\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.stability.link_raw_data_for_stability_trace#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'link_raw_data_for_stability_trace',
      description:
        'A link to where the data file for the stability data is stored\n- This is a beta feature. The plan is to create a file repository where the raw files for stability data can be stored and disseminated. With the link and associated protocols, it should be possible to programmatically access and analyse the raw stability data.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.tested#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'tested',
      description:
        'TRUE if the performance of the cell has been tested outdoors.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.protocol#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'protocol',
      description:
        'The protocol used for the outdoor testing.\n- For a more detailed discussion on protocols and standard nomenclature for stability measurements, please see the following paper:\no Consensus statement for stability assessment and reporting for perovskite photovoltaics based on ISOS procedures byM. V. Khenkin et al. Nat. Energ. 2020. DOI10.1038/s41560-019-0529-5\nExample:\nIEC 61853-1\nISOS-O-1\nISOS-O-2\nISOS-O-3',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.average_over_n_number_of_cells#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'average_over_n_number_of_cells',
      description:
        'The number of cells the reported outdoor data is based on.\nExample:\n- The preferred way to enter data is to give every individual cell its own entry in the data template/data base. If that is done, the data is an average over 1 cell.\n- If the reported data is not the data from one individual cell, but an average over N cells. Give the number of cells.\n- If the reported value is an average, but it is unknown over how many cells the value has been averaged (and no good estimate is available), state the number of cells as 2, which is the smallest number of cells that qualifies for an averaging procedure.',
      type: {
        type_kind: 'numpy',
        type_data: 'int64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.location_country#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'location_country',
      description:
        'The country where the outdoor testing was occurring\n- For measurements conducted in space, state this as \u2019Space International\u2019\nExample:\nSweden\nSwitzerland\nSpace International',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.location_city#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'location_city',
      description: 'The city where the outdoor testing was occurring',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.location_coordinates#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'location_coordinates',
      description:
        'The coordinates fort the places where the outdoor testing was occurring.\n- Use decimal degrees (DD) as the format.\nExample:\n59.839116; 17.647979\n52.428150; 13.532134',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.location_climate_zone#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'location_climate_zone',
      description:
        'The climate zone for the places where the outdoor testing was occurring.\nExample:\nCold\nDesert\nSubtropical\nTeperate\nTropical',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.installation_tilt#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'installation_tilt',
      description:
        'The tilt of the installed solar cell.\n- A module lying flat on the ground have a tilt of 0\n- A module standing straight up has a tilt of 90',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'degree',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.installation_cardinal_direction#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'installation_cardinal_direction',
      description:
        'The cardinal direction of the installed solar cell.\n- North is 0\n- East is 90\n- South is 180\n- West is 270',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'degree',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.installation_number_of_solar_tracking_axis#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'installation_number_of_solar_tracking_axis',
      description: 'The number of tracking axis in the installation.',
      type: {
        type_kind: 'numpy',
        type_data: 'int64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.time_season#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'time_season',
      description:
        'The time of year the outdoor testing was occurring.\n- Order the seasons in alphabetic order and separate them with semicolons.\n- For time periods longer than a year, state all four seasons once.\nExample:\nAutumn\nAutumn; Summer\nAutumn; Spring, Winter\nAutumn; Spring; Summer; Winter\nSpring; Winter',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.time_start#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'time_start',
      description: 'The starting time for the outdoor measurement.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.time_end#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'time_end',
      description: 'The ending time for the outdoor measurement.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.time_total_exposure#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'time_total_exposure',
      description:
        'The total duration of the outdoor measurement in days.\n- If there are uncertainties, only state the best estimate, e.g. write 1000 and not 950-1050',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.potential_bias_load_condition#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'potential_bias_load_condition',
      description:
        'The Potentiostatic load condition during the outdoor measurement\n- When the cell is not connected to anything, state this as \u2018Open circuit\u2019\nExamples:\nConstant current\nConstant potential\nMPPT\nOpen circuit\nPassive resistance\nShort circuit',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.potential_bias_range#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'potential_bias_range',
      description:
        'The potential range during the outdoor measurement\n- Separate the lower and upper bound by a semicolon.\n- For constant values, state only that value.\n- For open circuit conditions, state this as \u2018nan\u2019\n- If there are uncertainties, only state the best estimate, e.g. write 1 and not 0.90-1.1\n- State unknown values as \u2018nan\u2019\nExample:\n0.9; 1.02\n1.5\nnan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.potential_bias_passive_resistance#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'potential_bias_passive_resistance',
      description:
        'The passive resistance in the measurement circuit if a resistor was used\n- Give the value in ohm\n- If there are uncertainties, only state the best estimate, e.g. write 1.03 and not 1.01-1.05\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'ohm',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.temperature_load_condition#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'temperature_load_condition',
      description:
        'The load situation of the temperature during the outdoor measurement.\n- If the temperature is constant during the entire stability measurement, state this as \u2018Constant\u2019.\n- If there is a cycling between colder and hotter conditions, state this as \u2018Cycled\u2019\n- If the temperature varies in an uncontrolled way, state this as \u2018Uncontrolled\u2019\n- This category was included after the projects initial phase wherefor the list of reported categories is\nshort. Thus, be prepared to expand the given list of alternatives in the data template.\nExample:\nConstant\nUncontrolled\nCycled',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.temperature_range#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'temperature_range',
      description:
        'The temperature range during the outdoor measurement\n- Separate the lower and upper bound by a semicolon.\n- For constant values, state only that value.\n- If there are uncertainties, only state the best estimate, e.g. write 1 and not 0.90-1.1\n- State unknown values as \u2018nan\u2019\nExample:\n30\n-10; 85\nnan',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.temperature_tmodule#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'temperature_tmodule',
      description: 'The effective temperature of the module during peak hours.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'degree_Celsius',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.periodic_JV_measurements#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'periodic_JV_measurements',
      description:
        'TRUE if the outdoor measurement periodically is interrupted for JV-measurements.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.periodic_JV_measurements_time_between_measurements#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'periodic_JV_measurements_time_between_measurements',
      description:
        'The average time between JV-measurement during the outdoor measurement.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.PCE_initial_value#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_initial_value',
      description:
        'The efficiency, PCE, of the cell before the measurement routine starts\n- Give the efficiency in %\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.PCE_burn_in_observed#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_burn_in_observed',
      description:
        'TRUE if the performance has a relatively fast initial decay after which the decay rate stabilises at a lower level.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.PCE_end_of_experiment#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_end_of_experiment',
      description:
        'The efficiency, PCE, of the cell at the end of the experiment\n- Give the efficiency in %\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.PCE_T95#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_T95',
      description:
        'The time after which the cell performance has degraded by 5 % with respect to the initial performance.\n- If there are uncertainties, only state the best estimate, e.g. write 1000 and not 950-1050\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.PCE_Ts95#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_Ts95',
      description:
        'The time after which the cell performance has degraded by 5 % with respect to the performance after any initial burn in phase.\n- If there are uncertainties, only state the best estimate, e.g. write 1000 and not 950-1050\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.PCE_T80#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_T80',
      description:
        'The time after which the cell performance has degraded by 20 % with respect to the initial performance.\n- If there are uncertainties, only state the best estimate, e.g. write 1000 and not 950-1050\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.PCE_Ts80#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_Ts80',
      description:
        'The time after which the cell performance has degraded by 20 % with respect to the performance after any initial burn in phase.\n- If there are uncertainties, only state the best estimate, e.g. write 1000 and not 950-1050\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.PCE_Te80#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_Te80',
      description:
        'An estimated T80 for cells that was not measured sufficiently long for them to degrade by 20 %. with respect to the initial performance.\n- This value will by definition have a significant uncertainty to it, as it is not measured but extrapolated under the assumption linearity but without a detailed and stabilised extrapolation protocol. This estimate is, however, not without value as it enables a ruff comparison between all cells for with the stability has been measured.\n- If there is an experimental T80, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.PCE_Tse80#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_Tse80',
      description:
        'An estimated T80s for cells that was not measured sufficiently long for them to degrade by 20 %. with respect to the performance after any initial burn in phase.\n- This value will by definition have a significant uncertainty to it, as it is not measured but extrapolated under the assumption linearity but without a detailed and stabilised extrapolation protocol. This estimate is, however, not without value as it enables a ruff comparison between all cells for with the stability has been measured.\n- If there is an experimental T80s, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'hour',
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.PCE_after_1000_h#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'PCE_after_1000_h',
      description:
        'The efficiency, PCE, of the cell after 1000 hours\n- Give the efficiency in %\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.power_generated#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'power_generated',
      description:
        'The yearly power generated during the measurement period in kWh/year/m^2.\n- If there are uncertainties, only state the best estimate, e.g. write 20.5 and not 19-20\n- If unknown or not applicable, leave this field empty.',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      shape: [],
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.link_raw_data_for_outdoor_trace#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'link_raw_data_for_outdoor_trace',
      description:
        'A link to where the data file for the measurement is stored\n- This is a beta feature. The plan is to create a file repository where the raw files for stability data can be stored and disseminated. With the link and associated protocols, it should be possible to programmatically access and analyse the raw data.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.detaild_weather_data_available#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'detaild_weather_data_available',
      description:
        'TRUE if detailed weather data is available for the measurement period',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.link_detailed_weather_data#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'link_detailed_weather_data',
      description:
        'A link to where the data file for the measurement is stored\n- This is a beta feature. The plan is to create a file repository where the raw files for stability data can be stored and disseminated. With the link and associated protocols, it should be possible to programmatically access and analyse the raw data.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.spectral_data_available#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'spectral_data_available',
      description:
        'TRUE measured spectral data are available for the measurement period',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.link_spectral_data#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'link_spectral_data',
      description:
        'A link to where the data file for the measurement is stored\n- This is a beta feature. The plan is to create a file repository where the raw files for stability data can be stored and disseminated. With the link and associated protocols, it should be possible to programmatically access and analyse the raw data.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.irradiance_measured#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'irradiance_measured',
      description:
        'TRUE measured irradiance data are available for the measurement period',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.outdoor.link_irradiance_data#perovskite_solar_cell_database.schema.PerovskiteSolarCell':
    {
      name: 'link_irradiance_data',
      description:
        'A link to where the data file for the measurement is stored\n- This is a beta feature. The plan is to create a file repository where the raw files for stability data can be stored and disseminated. With the link and associated protocols, it should be possible to programmatically access and analyse the raw data.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      shape: [],
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'perovskite_solar_cell_database.schema.PerovskiteSolarCell',
    },
  'data.output#pynxtools.nomad.dataconverter.ElnYamlConverter': {
    name: 'output',
    description:
      'Output yaml file to save all the data. Default: eln_data.yaml',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'pynxtools.nomad.dataconverter.ElnYamlConverter',
  },
  'data.reader#pynxtools.nomad.dataconverter.NexusDataConverter': {
    name: 'reader',
    description: 'The reader needed to run the Nexus converter.',
    type: {
      type_kind: 'enum',
      type_data: [
        'apm',
        'ellips',
        'em',
        'example',
        'json_map',
        'json_yml',
        'mpes',
        'multi',
        'raman',
        'sts',
        'xps',
        'xrd',
      ],
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'pynxtools.nomad.dataconverter.NexusDataConverter',
  },
  'data.nxdl#pynxtools.nomad.dataconverter.NexusDataConverter': {
    name: 'nxdl',
    description: 'The nxdl needed for running the Nexus converter.',
    type: {
      type_kind: 'enum',
      type_data: [
        'NXapm',
        'NXapm_compositionspace_config',
        'NXapm_compositionspace_results',
        'NXapm_paraprobe_clusterer_config',
        'NXapm_paraprobe_clusterer_results',
        'NXapm_paraprobe_distancer_config',
        'NXapm_paraprobe_distancer_results',
        'NXapm_paraprobe_intersector_config',
        'NXapm_paraprobe_intersector_results',
        'NXapm_paraprobe_nanochem_config',
        'NXapm_paraprobe_nanochem_results',
        'NXapm_paraprobe_ranger_config',
        'NXapm_paraprobe_ranger_results',
        'NXapm_paraprobe_selector_config',
        'NXapm_paraprobe_selector_results',
        'NXapm_paraprobe_spatstat_config',
        'NXapm_paraprobe_spatstat_results',
        'NXapm_paraprobe_surfacer_config',
        'NXapm_paraprobe_surfacer_results',
        'NXapm_paraprobe_tessellator_config',
        'NXapm_paraprobe_tessellator_results',
        'NXapm_paraprobe_transcoder_config',
        'NXapm_paraprobe_transcoder_results',
        'NXarchive',
        'NXarpes',
        'NXcanSAS',
        'NXcxi_ptycho',
        'NXdirecttof',
        'NXdispersive_material',
        'NXellipsometry',
        'NXem',
        'NXem_calorimetry',
        'NXfluo',
        'NXindirecttof',
        'NXiqproc',
        'NXiv_temp',
        'NXlab_electro_chemo_mechanical_preparation',
        'NXlab_sample_mounting',
        'NXlauetof',
        'NXmicrostructure_gragles_config',
        'NXmicrostructure_gragles_results',
        'NXmicrostructure_imm_config',
        'NXmicrostructure_imm_results',
        'NXmicrostructure_kanapy_results',
        'NXmicrostructure_score_config',
        'NXmicrostructure_score_results',
        'NXmonopd',
        'NXmpes',
        'NXmpes_arpes',
        'NXmx',
        'NXoptical_spectroscopy',
        'NXraman',
        'NXrefscan',
        'NXreftof',
        'NXroot',
        'NXsas',
        'NXsastof',
        'NXscan',
        'NXsensor_scan',
        'NXsnsevent',
        'NXsnshisto',
        'NXspe',
        'NXsqom',
        'NXsts',
        'NXstxm',
        'NXtas',
        'NXtofnpd',
        'NXtofraw',
        'NXtofsingle',
        'NXtomo',
        'NXtomophase',
        'NXtomoproc',
        'NXtransmission',
        'NXxas',
        'NXxasproc',
        'NXxbase',
        'NXxeuler',
        'NXxkappa',
        'NXxlaue',
        'NXxlaueplate',
        'NXxnb',
        'NXxpcs',
        'NXxps',
        'NXxrd_pan',
        'NXxrot',
      ],
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'pynxtools.nomad.dataconverter.NexusDataConverter',
  },
  'data.output#pynxtools.nomad.dataconverter.NexusDataConverter': {
    name: 'output',
    description:
      'Output Nexus filename to save all the data. Default: output.nxs',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'pynxtools.nomad.dataconverter.NexusDataConverter',
  },
  'data.first_name#nomad_plugin_gui.schema_packages.demo_schema.Author': {
    name: 'first_name',
    description: 'First name of the author.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
  },
  'data.last_name#nomad_plugin_gui.schema_packages.demo_schema.Author': {
    name: 'last_name',
    description: 'Last name of the author.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
  },
  'data.email#nomad_plugin_gui.schema_packages.demo_schema.Author': {
    name: 'email',
    description: 'Email address of the author.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
  },
  'data.affiliation#nomad_plugin_gui.schema_packages.demo_schema.Author': {
    name: 'affiliation',
    description: 'Affiliation of the author.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
  },
  'data.orcid#nomad_plugin_gui.schema_packages.demo_schema.Author': {
    name: 'orcid',
    description: 'ORCID of the author.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
  },
  'data.biography#nomad_plugin_gui.schema_packages.demo_schema.Author': {
    name: 'biography',
    description: 'Biography of the author.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
  },
  'data.image#nomad_plugin_gui.schema_packages.demo_schema.Author': {
    name: 'image',
    description: 'URL to an image of the author.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
  },
  'data.cv.from_date#nomad_plugin_gui.schema_packages.demo_schema.Author': {
    name: 'from_date',
    description: 'Start date of the position.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
  },
  'data.cv.to_date#nomad_plugin_gui.schema_packages.demo_schema.Author': {
    name: 'to_date',
    description: 'End date of the position.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
  },
  'data.cv.employer#nomad_plugin_gui.schema_packages.demo_schema.Author': {
    name: 'employer',
    description: 'Employer of the position.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
  },
  'data.cv.occupation#nomad_plugin_gui.schema_packages.demo_schema.Author': {
    name: 'occupation',
    description: 'Occupation of the position.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: true,
    schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
  },
  'data.cv.employer_details.department#nomad_plugin_gui.schema_packages.demo_schema.Author':
    {
      name: 'department',
      description: 'Department within the employer.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
    },
  'data.cv.employer_details.address#nomad_plugin_gui.schema_packages.demo_schema.Author':
    {
      name: 'address',
      description: 'Address of the employer.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
    },
  'data.cv.employer_details.city#nomad_plugin_gui.schema_packages.demo_schema.Author':
    {
      name: 'city',
      description: 'City of the employer.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.demo_schema.Author',
    },
  'data.student_name#nomad_plugin_gui.schema_packages.exercise_schema.Exercise':
    {
      name: 'student_name',
      description: 'Name of the student.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.exercise_schema.Exercise',
    },
  'data.student_id#nomad_plugin_gui.schema_packages.exercise_schema.Exercise': {
    name: 'student_id',
    description: 'ID of the student.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_plugin_gui.schema_packages.exercise_schema.Exercise',
  },
  'data.exercise_date#nomad_plugin_gui.schema_packages.exercise_schema.Exercise':
    {
      name: 'exercise_date',
      description: 'Date of the exercise.',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.Datetime',
      },
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.exercise_schema.Exercise',
    },
  'data.notes#nomad_plugin_gui.schema_packages.exercise_schema.Exercise': {
    name: 'notes',
    description: 'Notes of the experiment.',
    type: {
      type_kind: 'python',
      type_data: 'str',
    },
    aggregatable: true,
    dynamic: true,
    repeats: false,
    schema: 'nomad_plugin_gui.schema_packages.exercise_schema.Exercise',
  },
  'data.figures.label#nomad_plugin_gui.schema_packages.exercise_schema.Exercise':
    {
      name: 'label',
      description: 'Label shown in the plot selection.',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_plugin_gui.schema_packages.exercise_schema.Exercise',
    },
  'data.figures.index#nomad_plugin_gui.schema_packages.exercise_schema.Exercise':
    {
      name: 'index',
      description: 'Index of figure in the plot selection.',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_plugin_gui.schema_packages.exercise_schema.Exercise',
    },
  'data.figures.open#nomad_plugin_gui.schema_packages.exercise_schema.Exercise':
    {
      name: 'open',
      description: 'Determines whether the figure is initially open or closed.',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: true,
      schema: 'nomad_plugin_gui.schema_packages.exercise_schema.Exercise',
    },
  'data.measurements.voltage#nomad_plugin_gui.schema_packages.exercise_schema.Exercise':
    {
      name: 'voltage',
      description: 'Voltage of the measurement.',
      type: {
        type_kind: 'python',
        type_data: 'float',
      },
      unit: 'volt',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_plugin_gui.schema_packages.exercise_schema.Exercise',
    },
  'data.measurements.current#nomad_plugin_gui.schema_packages.exercise_schema.Exercise':
    {
      name: 'current',
      description: 'Current of the measurement.',
      type: {
        type_kind: 'python',
        type_data: 'float',
      },
      unit: 'ampere',
      aggregatable: false,
      dynamic: true,
      repeats: true,
      schema: 'nomad_plugin_gui.schema_packages.exercise_schema.Exercise',
    },
  'data.quantity_types.float_quantity#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'float_quantity',
      type: {
        type_kind: 'python',
        type_data: 'float',
      },
      unit: 'meter',
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'data.quantity_types.np_quantity#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'np_quantity',
      type: {
        type_kind: 'numpy',
        type_data: 'float64',
      },
      unit: 'meter',
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'data.quantity_types.string_quantity#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'string_quantity',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'data.quantity_types.enum_quantity#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'enum_quantity',
      type: {
        type_kind: 'enum',
        type_data: ['one', 'three', 'two'],
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'data.quantity_types.bool_quantity#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'bool_quantity',
      type: {
        type_kind: 'python',
        type_data: 'bool',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'data.quantity_types.int_quantity#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'int_quantity',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'data.quantity_types.datatime_quantity#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'datatime_quantity',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.Datetime',
      },
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'data.quantity_types.float_with_default#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'float_with_default',
      type: {
        type_kind: 'python',
        type_data: 'float',
      },
      unit: 'meter',
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'data.quantity_types.int_with_default#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'int_with_default',
      type: {
        type_kind: 'python',
        type_data: 'int',
      },
      aggregatable: false,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'data.quantity_types.string_with_default#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'string_with_default',
      type: {
        type_kind: 'python',
        type_data: 'str',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'data.quantity_types.url_quantity#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'url_quantity',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.URL',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'data.quantity_types.file_quantity#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'file_quantity',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.File',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'data.quantity_types.capitalized_quantity#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'capitalized_quantity',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.Capitalized',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'data.quantity_types.unit_quantity#nomad_plugin_gui.schema_packages.values_test_schema.Main':
    {
      name: 'unit_quantity',
      type: {
        type_kind: 'custom',
        type_data: 'nomad.metainfo.data_type.Unit',
      },
      aggregatable: true,
      dynamic: true,
      repeats: false,
      schema: 'nomad_plugin_gui.schema_packages.values_test_schema.Main',
    },
  'results.material': {
    name: 'material',
    description:
      '\n        Section containing information on the material composition and structure.\n        ',
    nested: false,
    repeats: false,
  },
  'results.material.elemental_composition': {
    name: 'elemental_composition',
    description:
      '\n        Section containing information about the concentration of an element,\n        given by its atomic and mass fraction within the system or material.\n        ',
    nested: true,
    repeats: true,
  },
  'results.material.symmetry': {
    name: 'symmetry',
    description:
      '\n        Section containing information about the symmetry of the material. All\n        of these properties are derived by running a symmetry analysis on a\n        representative geometry from the original data. This original geometry\n        is stored in results.properties together with the primitive and\n        conventional structures.\n        ',
    nested: false,
    repeats: false,
  },
  'results.material.topology': {
    name: 'topology',
    description:
      '\n        Describes a a structural part that has been identified within the entry.\n        May be related to other systems.\n        ',
    nested: true,
    repeats: true,
  },
  'results.material.topology.atoms': {
    name: 'atoms',
    description:
      'Describes the atomic structure of the physical system. This includes the atom positions, lattice vectors, etc.',
    nested: false,
    repeats: true,
  },
  'results.material.topology.elemental_composition': {
    name: 'elemental_composition',
    description:
      '\n        Section containing information about the concentration of an element,\n        given by its atomic and mass fraction within the system or material.\n        ',
    nested: true,
    repeats: true,
  },
  'results.material.topology.system_relation': {
    name: 'system_relation',
    description:
      'Contains information about the relation between two different systems.',
    nested: false,
    repeats: true,
  },
  'results.material.topology.cell': {
    name: 'cell',
    description: '\n        Properties of a unit cell.\n        ',
    nested: false,
    repeats: true,
  },
  'results.material.topology.symmetry': {
    name: 'symmetry',
    description:
      '\n        Section containing information about the symmetry properties of a\n        conventional cell related to a system.\n        ',
    nested: false,
    repeats: true,
  },
  'results.material.topology.symmetry.wyckoff_sets': {
    name: 'wyckoff_sets',
    description:
      '\n        Section for storing Wyckoff set information. Only available for\n        conventional cells that have undergone symmetry analysis.\n        ',
    nested: false,
    repeats: true,
  },
  'results.material.topology.metal_coordination': {
    name: 'metal_coordination',
    description:
      'Coordination number of an element, which represents the number of atoms directly bonded to the element.',
    nested: false,
    repeats: true,
  },
  'results.material.topology.active_orbitals': {
    name: 'active_orbitals',
    nested: false,
    repeats: true,
  },
  'results.method': {
    name: 'method',
    description:
      '\n        Contains a summary of the methodology that has been used in this entry.\n        This methodology applies to all of the reported properties and\n        determines the result of a single energy evalution. The individual\n        properties may be further methodological details affect e.g. the\n        sampling.\n        ',
    nested: false,
    repeats: false,
  },
  'results.method.simulation': {
    name: 'simulation',
    description:
      '\n        Contains method details for a simulation entry.\n        ',
    nested: false,
    repeats: false,
  },
  'results.method.simulation.dft': {
    name: 'dft',
    description: '\n        Methodology for a DFT calculation.\n        ',
    nested: false,
    repeats: false,
  },
  'results.method.simulation.dft.hubbard_kanamori_model': {
    name: 'hubbard_kanamori_model',
    description: 'Setup of the Hubbard model used in DFT+U',
    nested: true,
    repeats: true,
  },
  'results.method.simulation.tb': {
    name: 'tb',
    description:
      '\n        Methodology for a Tight-Binding calculation.\n        ',
    nested: false,
    repeats: false,
  },
  'results.method.simulation.gw': {
    name: 'gw',
    description: '\n        Methodology for a GW calculation.\n        ',
    nested: false,
    repeats: false,
  },
  'results.method.simulation.bse': {
    name: 'bse',
    description: '\n        Methodology for a BSE calculation.\n        ',
    nested: false,
    repeats: false,
  },
  'results.method.simulation.dmft': {
    name: 'dmft',
    description: '\n        Methodology for a DMFT calculation.\n        ',
    nested: false,
    repeats: false,
  },
  'results.method.simulation.quantum_cms': {
    name: 'quantum_cms',
    nested: false,
    repeats: false,
  },
  'results.method.simulation.quantum_cms.quantum_circuit': {
    name: 'quantum_circuit',
    nested: false,
  },
  'results.method.simulation.precision': {
    name: 'precision',
    description:
      '\n        Contains parameters for controlling or evaluating the convergence of the electronic structure.\n        ',
    nested: false,
    repeats: false,
  },
  'results.method.measurement': {
    name: 'measurement',
    description:
      '\n        Contains method details for a measurement entry.\n        ',
    nested: false,
    repeats: false,
  },
  'results.method.measurement.xrd': {
    name: 'xrd',
    description:
      '\n        Methodology for an X-Ray Diffraction measurement.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties': {
    name: 'properties',
    description:
      '\n        Contains the physical properties that have been calculated or used in\n        this entry.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.structural': {
    name: 'structural',
    description: '\n        Structural properties.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.structural.radial_distribution_function': {
    name: 'radial_distribution_function',
    description: '\n        Radial distribution function.\n        ',
    nested: true,
    repeats: true,
  },
  'results.properties.structural.radial_distribution_function.provenance': {
    name: 'provenance',
    description:
      '\n        Contains provenance information for properties derived from molecular\n        dynamics simulations.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.structural.radial_distribution_function.provenance.molecular_dynamics':
    {
      name: 'molecular_dynamics',
      description: '\n        Methodology for molecular dynamics.\n        ',
      nested: false,
      repeats: true,
    },
  'results.properties.structural.radius_of_gyration': {
    name: 'radius_of_gyration',
    description:
      '\n        Contains Radius of Gyration values as a trajectory.\n        ',
    nested: true,
    repeats: true,
  },
  'results.properties.structural.radius_of_gyration.provenance': {
    name: 'provenance',
    description:
      '\n        Contains provenance information for properties derived from molecular\n        dynamics simulations.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.structural.radius_of_gyration.provenance.molecular_dynamics':
    {
      name: 'molecular_dynamics',
      description: '\n        Methodology for molecular dynamics.\n        ',
      nested: false,
      repeats: true,
    },
  'results.properties.structural.diffraction_pattern': {
    name: 'diffraction_pattern',
    description: '\n        Diffraction pattern.\n        ',
    nested: true,
    repeats: true,
  },
  'results.properties.dynamical': {
    name: 'dynamical',
    description: '\n        Dynamical properties.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.dynamical.mean_squared_displacement': {
    name: 'mean_squared_displacement',
    description: '\n        Mean Squared Displacements.\n        ',
    nested: true,
    repeats: true,
  },
  'results.properties.dynamical.mean_squared_displacement.provenance': {
    name: 'provenance',
    description:
      '\n        Contains provenance information for properties derived from molecular\n        dynamics simulations.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.dynamical.mean_squared_displacement.provenance.molecular_dynamics':
    {
      name: 'molecular_dynamics',
      description: '\n        Methodology for molecular dynamics.\n        ',
      nested: false,
      repeats: true,
    },
  'results.properties.structures': {
    name: 'structures',
    description:
      '\n        Contains full atomistic representations of the material in different\n        forms.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.structures.structure_original': {
    name: 'structure_original',
    description: '\n        Describes an atomistic structure.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.structures.structure_original.species': {
    name: 'species',
    description:
      'Used to describe the species of the sites of this structure. Species can be pure chemical elements, or virtual-crystal atoms representing a statistical occupation of a\ngiven site by multiple chemical elements.',
    nested: false,
    repeats: true,
  },
  'results.properties.structures.structure_original.lattice_parameters': {
    name: 'lattice_parameters',
    description: '\n        Lattice parameters of a cell.\n        ',
    nested: false,
  },
  'results.properties.structures.structure_original.wyckoff_sets': {
    name: 'wyckoff_sets',
    description:
      '\n        Section for storing Wyckoff set information. Only available for\n        conventional cells that have undergone symmetry analysis.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.structures.structure_conventional': {
    name: 'structure_conventional',
    description: '\n        Describes an atomistic structure.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.structures.structure_conventional.species': {
    name: 'species',
    description:
      'Used to describe the species of the sites of this structure. Species can be pure chemical elements, or virtual-crystal atoms representing a statistical occupation of a\ngiven site by multiple chemical elements.',
    nested: false,
    repeats: true,
  },
  'results.properties.structures.structure_conventional.lattice_parameters': {
    name: 'lattice_parameters',
    description: '\n        Lattice parameters of a cell.\n        ',
    nested: false,
  },
  'results.properties.structures.structure_conventional.wyckoff_sets': {
    name: 'wyckoff_sets',
    description:
      '\n        Section for storing Wyckoff set information. Only available for\n        conventional cells that have undergone symmetry analysis.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.structures.structure_primitive': {
    name: 'structure_primitive',
    description: '\n        Describes an atomistic structure.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.structures.structure_primitive.species': {
    name: 'species',
    description:
      'Used to describe the species of the sites of this structure. Species can be pure chemical elements, or virtual-crystal atoms representing a statistical occupation of a\ngiven site by multiple chemical elements.',
    nested: false,
    repeats: true,
  },
  'results.properties.structures.structure_primitive.lattice_parameters': {
    name: 'lattice_parameters',
    description: '\n        Lattice parameters of a cell.\n        ',
    nested: false,
  },
  'results.properties.structures.structure_primitive.wyckoff_sets': {
    name: 'wyckoff_sets',
    description:
      '\n        Section for storing Wyckoff set information. Only available for\n        conventional cells that have undergone symmetry analysis.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.vibrational': {
    name: 'vibrational',
    description: '\n        Vibrational properties.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.vibrational.band_structure_phonon': {
    name: 'band_structure_phonon',
    description:
      '\n        This section stores information on a vibrational band structure\n        evaluation along one-dimensional pathways in the reciprocal space.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.vibrational.dos_phonon': {
    name: 'dos_phonon',
    description: '\n        Contains the phonon density of states.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.vibrational.heat_capacity_constant_volume': {
    name: 'heat_capacity_constant_volume',
    description:
      '\n        Contains the values of the specific (per mass) and isochoric (constant\n        volume) heat capacity at different temperatures.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.vibrational.energy_free_helmholtz': {
    name: 'energy_free_helmholtz',
    description:
      '\n        Contains the values of the Helmholtz free energy per atom at constant\n        volume and at different temperatures.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.electronic': {
    name: 'electronic',
    description: '\n        Electronic properties.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.electronic.band_gap': {
    name: 'band_gap',
    nested: true,
    repeats: true,
  },
  'results.properties.electronic.band_gap.provenance': {
    name: 'provenance',
    description: '\n    ',
    nested: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic': {
    name: 'dos_electronic',
    description:
      '\n        Contains the total electronic density of states.\n\n        OLD VERSION: it will eventually be deprecated.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic.band_gap': {
    name: 'band_gap',
    nested: true,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic.band_gap.provenance': {
    name: 'provenance',
    description:
      '\n        Contains semantically labelled provenance information.\n        To be stored under PropertySection.provenance or children.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic_new': {
    name: 'dos_electronic_new',
    description:
      "\n        Contains the electronic Density of States (DOS). This section can be repeated to refer to\n        different methodologies (e.g., label = 'DFT', 'GW', 'TB', etc.), and it can be spin-polarized\n        or not. The sub-section data points to each (if present) spin channels.\n        ",
    nested: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic_new.data': {
    name: 'data',
    description:
      '\n        Section containign the density of states data.\n\n        It includes the total DOS and the projected DOS values. We differentiate `species_projected` as the\n        projected DOS for same atomic species, `atom_projected` as the projected DOS for different\n        atoms in the cell, and `orbital_projected` as the projected DOS for the orbitals of each\n        atom.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic_new.data.band_gap': {
    name: 'band_gap',
    nested: true,
    repeats: true,
  },
  'results.properties.electronic.dos_electronic_new.data.band_gap.provenance': {
    name: 'provenance',
    description:
      '\n        Contains semantically labelled provenance information.\n        To be stored under PropertySection.provenance or children.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.electronic.band_structure_electronic': {
    name: 'band_structure_electronic',
    description:
      '\n        This section stores information on a electonic band structure\n        evaluation along one-dimensional pathways in the reciprocal space.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.electronic.band_structure_electronic.band_gap': {
    name: 'band_gap',
    nested: true,
    repeats: true,
  },
  'results.properties.electronic.band_structure_electronic.band_gap.provenance':
    {
      name: 'provenance',
      description:
        '\n        Contains semantically labelled provenance information.\n        To be stored under PropertySection.provenance or children.\n        ',
      nested: false,
      repeats: true,
    },
  'results.properties.electronic.greens_functions_electronic': {
    name: 'greens_functions_electronic',
    description:
      "\n        Base class for Green's functions information.\n        ",
    nested: false,
    repeats: true,
  },
  'results.properties.electronic.electric_field_gradient': {
    name: 'electric_field_gradient',
    description:
      '\n        Base class for the electric field gradient information. This section is relevant\n        for NMR and describes the potential generated my the nuclei in the system.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.magnetic': {
    name: 'magnetic',
    description: '\n        Magnetic properties.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.magnetic.magnetic_shielding': {
    name: 'magnetic_shielding',
    description:
      '\n        Base class for the atomic magnetic shielding information.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.magnetic.spin_spin_coupling': {
    name: 'spin_spin_coupling',
    description:
      '\n        Base class for the spin-spin coupling information.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.magnetic.magnetic_susceptibility': {
    name: 'magnetic_susceptibility',
    description:
      '\n        Base class for the magnetic susceptibility information.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.optoelectronic': {
    name: 'optoelectronic',
    description: '\n        Optoelectronic properties.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.optoelectronic.solar_cell': {
    name: 'solar_cell',
    description: '\n        Properties of solar cells.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.catalytic': {
    name: 'catalytic',
    description: '\n        Properties relating to catalysis.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.catalytic.reaction': {
    name: 'reaction',
    description:
      '\n        A collection of specifications and properties of a full catalytic reaction.\n        This may include reaction conditions, results and mechanistic aspects of a reaction.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.catalytic.reaction.reactants': {
    name: 'reactants',
    description:
      '\n        A reactant in a catalytic test reaction. A reactant\n        is identified by having a conversion.\n        ',
    nested: true,
    repeats: true,
  },
  'results.properties.catalytic.reaction.products': {
    name: 'products',
    description:
      '\n        A product of a catalytic reaction. A product here is usually identified by having\n        a selectivity, or a gas_concentration_out but no/zero gas_concentration_in.\n        ',
    nested: true,
    repeats: true,
  },
  'results.properties.catalytic.reaction.rates': {
    name: 'rates',
    description:
      '\n        Section bundling multiple representations of catalytic reaction rates.\n        ',
    nested: true,
    repeats: true,
  },
  'results.properties.catalytic.reaction.reaction_conditions': {
    name: 'reaction_conditions',
    description:
      '\n        Conditions under which a catalytic test reaction was performed.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.catalytic.reaction.reaction_mechanism': {
    name: 'reaction_mechanism',
    description:
      '\n        Properties of single steps of a catalytic reaction mechanism.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.catalytic.catalyst': {
    name: 'catalyst',
    description: '\n        Properties of a heterogeneous catalyst.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.mechanical': {
    name: 'mechanical',
    description: '\n        Mechanical properties.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.mechanical.energy_volume_curve': {
    name: 'energy_volume_curve',
    description: '\n        Energy volume curve.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.mechanical.bulk_modulus': {
    name: 'bulk_modulus',
    description:
      '\n        Contains bulk modulus values calculated with different methodologies.\n        ',
    nested: true,
    repeats: true,
  },
  'results.properties.mechanical.shear_modulus': {
    name: 'shear_modulus',
    description:
      '\n        Contains shear modulus values calculated with different methodologies.\n        ',
    nested: true,
    repeats: true,
  },
  'results.properties.thermodynamic': {
    name: 'thermodynamic',
    description: '\n        Thermodynamic properties.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.thermodynamic.trajectory': {
    name: 'trajectory',
    description:
      '\n        Thermodynamic properties reported for an ensemble evolving in time.\n        ',
    nested: true,
    repeats: true,
  },
  'results.properties.thermodynamic.trajectory.provenance': {
    name: 'provenance',
    description:
      '\n        Contains provenance information for properties derived from molecular\n        dynamics simulations.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.thermodynamic.trajectory.provenance.molecular_dynamics': {
    name: 'molecular_dynamics',
    description: '\n        Methodology for molecular dynamics.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.thermodynamic.trajectory.temperature': {
    name: 'temperature',
    description:
      '\n        Contains temperature values evaluated at different times.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.thermodynamic.trajectory.pressure': {
    name: 'pressure',
    description:
      '\n        Contains pressure values evaluated at different times.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.thermodynamic.trajectory.volume': {
    name: 'volume',
    description:
      '\n        Contains volume values evaluated at different times.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.thermodynamic.trajectory.energy_potential': {
    name: 'energy_potential',
    description:
      '\n        Contains energy values evaluated at different times.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.spectroscopic': {
    name: 'spectroscopic',
    description: '\n        Spectroscopic properties.\n        ',
    nested: false,
    repeats: false,
  },
  'results.properties.spectroscopic.spectra': {
    name: 'spectra',
    description:
      '\n        Base class for Spectra calculation information as obtained from an experiment or a computation.\n        ',
    nested: true,
    repeats: true,
  },
  'results.properties.spectroscopic.spectra.provenance': {
    name: 'provenance',
    description:
      '\n        Contains provenance information (mainly the methodology section) for spectra properties\n        derived from an experiment or a calculation.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.spectroscopic.spectra.provenance.eels': {
    name: 'eels',
    description: '\n        Base class for the EELS methodology.\n        ',
    nested: false,
    repeats: true,
  },
  'results.properties.spectroscopic.spectra.provenance.electronic_structure': {
    name: 'electronic_structure',
    description: '\n    ',
    nested: false,
    repeats: true,
  },
  'results.properties.geometry_optimization': {
    name: 'geometry_optimization',
    description:
      '\n        Geometry optimization results and settings.\n        ',
    nested: false,
    repeats: false,
  },
  'results.eln': {
    name: 'eln',
    nested: false,
    repeats: false,
  },
}
