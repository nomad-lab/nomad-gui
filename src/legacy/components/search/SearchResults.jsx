/*
 * Copyright The NOMAD Authors.
 *
 * This file is part of NOMAD. See https://nomad-lab.eu for further info.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import LaunchIcon from '@mui/icons-material/Launch'
import {Alert, IconButton, Paper, Tooltip, Typography} from '@mui/material'
import jmespath from 'jmespath'
import {isArray, isEmpty} from 'lodash'
import PropTypes from 'prop-types'
import React, {useCallback, useEffect, useMemo, useState} from 'react'

import useRoute from '../../../components/routing/useRoute'
import {
  filterOptions,
  formatInteger,
  parseJMESPath,
  pluralize,
} from '../../utils'
import {
  Datatable,
  DatatableLoadMorePagination,
  DatatableTable,
  DatatableToolbar,
  DatatableToolbarActions,
} from '../datatable/Datatable'
import EntryDownloadButton from '../entry/EntryDownloadButton'
import {useSearchContext} from './SearchContext'

/**
 * Used to retrieve an URL link from the row metadata and display a link icon to
 * that resource.
 */
export const ActionURL = React.memo(({action, data}) => {
  const {path} = parseJMESPath(action.path)
  let href = jmespath.search(data, path)
  href = isArray(href) ? href[0] : href
  return (
    <Tooltip title={action.description || ''}>
      <IconButton href={href} target='_blank'>
        <LaunchIcon />
      </IconButton>
    </Tooltip>
  )
})
ActionURL.propTypes = {
  action: PropTypes.object.isRequired, // Action configuration from app config
  data: PropTypes.object.isRequired, // ES index data
}

/**
 * Displays the list of search results.
 */
export const SearchResults = React.memo(
  ({
    noAction,
    onSelectedChanged,
    defaultUncollapsedEntryID,
    title,
    'data-testid': testID = 'search-results',
    PaperProps,
    ...otherProps
  }) => {
    const {navigate} = useRoute()
    const {columns, resource, rows, useResults, useApiQuery} =
      useSearchContext()
    const {data, pagination, setPagination} = useResults()
    const apiQuery = useApiQuery()
    const [selected, setSelected] = useState(new Set())
    const shownColumns = columns
      ? columns
          .filter((column) => column.selected)
          .map((column) => column.search_quantity)
      : []

    useEffect(() => {
      if (onSelectedChanged) {
        onSelectedChanged(selected)
      }
    }, [onSelectedChanged, selected])

    const query = useMemo(() => {
      if (selected === 'all') {
        return apiQuery
      }
      return {entry_id: [...selected]}
    }, [selected, apiQuery])

    const handleClick = useCallback(
      (row) => {
        navigate({path: row.entry_id})
      },
      [navigate],
    )

    const actionDefinitions = filterOptions(rows.actions)
    const actions = useCallback(
      (data) => {
        const actionComponents = []
        for (const [key, value] of Object.entries(actionDefinitions)) {
          const component = {
            url: <ActionURL key={key} action={value} data={data} />,
          }[value.type]
          if (component) {
            actionComponents.push(component)
          }
        }
      },
      [actionDefinitions],
    )

    if (isEmpty(columns)) {
      return (
        <Alert severity='warning'>
          No search columns defined within this search context. Ensure that all
          GUI artifacts are created.
        </Alert>
      )
    }

    if (pagination.total === 0) {
      return <Typography>no results</Typography>
    }

    if (!pagination.total) {
      return <Typography>searching ...</Typography>
    }

    // Select components based on the targeted resource
    let details
    let buttons
    if (resource === 'entries') {
      details = rows?.details?.render
      if (!noAction)
        buttons = <EntryDownloadButton tooltip='Download files' query={query} />
    }

    return (
      <Paper data-testid={testID} {...PaperProps}>
        <Datatable
          data={data}
          pagination={pagination}
          onPaginationChanged={setPagination}
          columns={columns}
          shownColumns={shownColumns}
          selected={rows?.selection?.enabled ? selected : undefined}
          getId={(option) => option.entry_id}
          onSelectedChanged={rows?.selection?.enabled ? setSelected : undefined}
          {...otherProps}
        >
          <DatatableToolbar
            title={`${formatInteger(data.length)}/${pluralize(
              title || 'result',
              pagination.total,
              true,
              true,
              title ? undefined : 'search',
            )}`}
          >
            {rows?.selection?.enabled && (
              <DatatableToolbarActions selection>
                {buttons}
              </DatatableToolbarActions>
            )}
          </DatatableToolbar>
          <DatatableTable
            onClick={handleClick}
            actions={
              rows?.actions?.enabled && actionDefinitions.size
                ? actions
                : undefined
            }
            details={rows?.details?.enabled ? details : undefined}
            defaultUncollapsedRow={
              defaultUncollapsedEntryID &&
              data.find((row) => row.entry_id === defaultUncollapsedEntryID)
            }
          >
            <DatatableLoadMorePagination color='primary'>
              load more
            </DatatableLoadMorePagination>
          </DatatableTable>
        </Datatable>
      </Paper>
    )
  },
)

SearchResults.propTypes = {
  noAction: PropTypes.bool,
  PaperProps: PropTypes.object,
  onSelectedChanged: PropTypes.func,
  defaultUncollapsedEntryID: PropTypes.string,
  title: PropTypes.string,
  'data-testid': PropTypes.string,
}
