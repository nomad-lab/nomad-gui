/*
 * Copyright The NOMAD Authors.
 *
 * This file is part of NOMAD. See https://nomad-lab.eu for further info.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Box} from '@mui/material'
import PropTypes from 'prop-types'
import React from 'react'

import Query from './Query'
import SearchBar from './SearchBar'
import {useSearchContext} from './SearchContext'
import SearchMenu from './SearchMenu'
import {SearchResults} from './SearchResults'
import Dashboard from './widgets/Dashboard'

/**
 * The primary search interface that is reused throughout the application in
 * different contexts. Displays a menu of filters, a search bar, a list of
 * results and optionally a customizable header above the search bar.
 */
const SearchPage = React.memo(({header}) => {
  const {useSetIsMenuOpen} = useSearchContext()
  const setIsMenuOpen = useSetIsMenuOpen()

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        flexGrow: 1,
        height: '100%',
        overflow: 'hidden',
        width: '100%',
      }}
    >
      <Box
        sx={{
          flexShrink: 0,
          flexGrow: 0,
          height: '100%',
          zIndex: 2,
        }}
      >
        <SearchMenu />
      </Box>
      <Box
        sx={{
          flexGrow: 1,
          height: '100%',
          overflowY: 'scroll',
        }}
        onClick={() => setIsMenuOpen(false)}
      >
        <Box
          sx={{
            marginLeft: 2.5,
            marginRight: 2.5,
            paddingBottom: 3,
          }}
        >
          {header && <Box sx={{marginBottom: 2}}>{header}</Box>}
          <Box sx={{marginBottom: 0}}>
            <SearchBar />
            <Query />
          </Box>
          <Box sx={{marginBottom: 1, zIndex: 0}}>
            <Dashboard />
          </Box>
          <Box sx={{zIndex: 1}}>
            <SearchResults />
          </Box>
        </Box>
      </Box>
    </Box>
  )
})

SearchPage.propTypes = {
  header: PropTypes.node,
}

export default SearchPage
