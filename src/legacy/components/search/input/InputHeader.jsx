/*
 * Copyright The NOMAD Authors.
 *
 * This file is part of NOMAD. See https://nomad-lab.eu for further info.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {makeStyles} from '@mui/styles'
import clsx from 'clsx'
import PropTypes from 'prop-types'
import React, {useCallback, useMemo} from 'react'

import {useBoolState} from '../../../hooks'
import {ActionHeader, ActionSelect, Actions} from '../../Actions'
import {scales} from '../../plotting/common'
import FilterTitle from '../FilterTitle'
import WidgetToggle from '../widgets/WidgetToggle'

/**
 * The quantity label and actions shown by all filter components.
 */
const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(0.5),
    height: '2.5rem',
    width: '100%',
  },
  menuItem: {
    width: '10rem',
    margin: theme.spacing(2),
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  row: {
    display: 'flex',
    height: '100%',
    alignItems: 'center',
    flexGrow: 1,
    minWidth: 0,
  },
  spacer: {
    flexGrow: 1,
    minWidth: 0,
  },
}))

const InputHeader = React.memo(
  ({
    quantity,
    label,
    unit,
    description,
    disableWidget = false,
    disableStatistics = false,
    scale = 'linear',
    onChangeScale,
    actions,
    actionsAlign = 'left',
    className,
    classes,
  }) => {
    const styles = useStyles({classes: classes})
    const [isDragging, setDragging, setNotDragging] = useBoolState(false)
    const [isTooltipOpen, openTooltip, closeTooltip] = useBoolState(false)

    const openMenu = useCallback((event) => {
      setAnchorEl(event.currentTarget)
    }, [])
    const closeMenu = useCallback(() => {
      setAnchorEl(null)
    }, [])
    const handleMouseDown = useCallback(
      (event) => {
        setDragging()
        closeTooltip()
      },
      [closeTooltip, setDragging],
    )
    const handleMouseUp = useCallback(() => {
      setNotDragging()
    }, [setNotDragging])
    const tooltipProps = useMemo(
      () => ({
        open: isTooltipOpen,
        onClose: closeTooltip,
        onOpen: () => !isDragging && openTooltip(),
      }),
      [isTooltipOpen, closeTooltip, isDragging, openTooltip],
    )

    return (
      <Actions className={clsx(styles.root, className)}>
        <ActionHeader disableSpacer>
          <div
            className={clsx(styles.row)}
            onMouseDown={handleMouseDown}
            onMouseUp={handleMouseUp}
          >
            <FilterTitle
              quantity={quantity}
              label={label}
              unit={unit}
              description={description}
              TooltipProps={tooltipProps}
            />
            <div className={styles.spacer} />
          </div>
        </ActionHeader>
        {actionsAlign === 'left' && actions}
        {!disableStatistics && (
          <ActionSelect
            value={scale}
            options={scales}
            tooltip='Statistics scaling'
            onChange={onChangeScale}
          />
        )}
        {!disableWidget && (
          <WidgetToggle quantity={quantity} disabled={disableWidget} />
        )}
        {actionsAlign === 'right' && actions}
      </Actions>
    )
  },
)

InputHeader.propTypes = {
  quantity: PropTypes.string,
  label: PropTypes.string,
  unit: PropTypes.string,
  description: PropTypes.string,
  disableWidget: PropTypes.bool,
  disableStatistics: PropTypes.bool,
  scale: PropTypes.string,
  onChangeScale: PropTypes.func,
  variant: PropTypes.string,
  actions: PropTypes.node,
  actionsAlign: PropTypes.oneOf(['left', 'right']),
  className: PropTypes.string,
  classes: PropTypes.object,
}

export default InputHeader
