/*
 * Copyright The NOMAD Authors.
 *
 * This file is part of NOMAD. See https://nomad-lab.eu for further info.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import ArrowForwardIcon from '@mui/icons-material/ArrowForward'
import NavigateNextIcon from '@mui/icons-material/NavigateNext'
import {
  Box,
  Checkbox,
  FormControlLabel,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  MenuItem as MenuItemMUI,
  Paper,
  Typography,
} from '@mui/material'
import {makeStyles, useTheme} from '@mui/styles'
import clsx from 'clsx'
import PropTypes from 'prop-types'
import React, {Children, isValidElement, useCallback, useContext} from 'react'

import {Action, ActionHeader, Actions} from '../../Actions'
import Scrollable from '../../visualization/Scrollable'
import FilterTitle from '../FilterTitle'
import {useSearchContext} from '../SearchContext'

// The menu animations use a transition on the 'transform' property. Notice that
// animating 'transform' instead of e.g. the 'left' property is much more
// performant. We also hint the browser that the transform property will be
// animated using the 'will-change' property: this will pre-optimize the element
// for animation when possible (the recommendation is to remove/add it when
// needed, but in this case we keep it on constantly).
export const menuContext = React.createContext()
export const paddingHorizontal = 1.5
const collapseWidth = '3rem'

export const Menu = React.memo(
  ({
    size,
    open,
    collapsed,
    onCollapsedChanged,
    subMenuOpen,
    visible,
    onOpenChange,
    selected,
    onSelectedChange,
    subMenus,
    children,
  }) => {
    const width =
      {
        xs: '17rem',
        sm: '21rem',
        md: '25rem',
        lg: '29rem',
        xl: '33rem',
        xxl: '45rem',
      }[size] ||
      size ||
      '21rem'

    return (
      <Box
        sx={{
          height: '100%',
          width: collapsed ? collapseWidth : 'unset',
        }}
      >
        <Box
          elevation={open ? 4 : 0}
          sx={{
            display: 'flex',
            height: '100%',
            flexDirection: 'column',
            width: collapsed ? collapseWidth : width,
            transform: open
              ? 'none'
              : `translateX(calc(-${width} + ${collapsed ? '50px' : '0px'}))`,
            transition: `transform 225ms`,
            willChange: 'transform',
            visibility: visible ? 'visible' : 'hidden',
          }}
        >
          <menuContext.Provider
            value={{
              collapsed,
              selected,
              setSelected: onSelectedChange,
              setCollapsed: onCollapsedChanged,
              subMenuOpen,
              open,
              setOpen: onOpenChange,
              size,
            }}
          >
            <Box sx={{zIndex: 0}}>{subMenus}</Box>
            <Paper
              sx={{
                width: '100%',
                height: '100%',
                display: 'flex',
                flexDirection: 'column',
                zIndex: 1,
              }}
            >
              {children}
            </Paper>
          </menuContext.Provider>
        </Box>
      </Box>
    )
  },
)
Menu.propTypes = {
  size: PropTypes.string,
  open: PropTypes.bool,
  collapsed: PropTypes.bool,
  onCollapsedChanged: PropTypes.func,
  subMenuOpen: PropTypes.bool,
  visible: PropTypes.bool,
  onOpenChange: PropTypes.func,
  selected: PropTypes.number,
  onSelectedChange: PropTypes.func,
  subMenus: PropTypes.node,
  children: PropTypes.node,
}

/**
 * Header for filter menus. Contains panel actions and an overline text.
 */
export const MenuHeader = React.memo(
  ({title, actions, 'data-testid': testID}) => {
    const theme = useTheme()
    const {collapsed, setCollapsed} = useContext(menuContext)

    return (
      <Box
        sx={{
          height: theme.spacing(5),
          paddingTop: theme.spacing(0.5),
          paddingLeft: theme.spacing(paddingHorizontal),
          paddingRight: theme.spacing(paddingHorizontal),
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'flex-start',
        }}
        data-testid={testID}
      >
        <Actions flexDirection={collapsed ? 'column-reverse' : 'row'}>
          <ActionHeader>
            <Typography
              sx={{
                display: 'flex',
                alignItems: 'center',
                fontSize: '0.90rem',
                transform: collapsed ? 'rotate(90deg)' : 'none',
                height: collapsed ? '5rem' : 'none',
              }}
              variant='button'
            >
              {title}
            </Typography>
          </ActionHeader>
          {collapsed ? (
            <Action tooltip={'Show menu'} onClick={() => setCollapsed(false)}>
              <ArrowForwardIcon fontSize='small' />
            </Action>
          ) : (
            actions
          )}
        </Actions>
      </Box>
    )
  },
)

MenuHeader.propTypes = {
  overlineTitle: PropTypes.string,
  title: PropTypes.string,
  topAction: PropTypes.node,
  actions: PropTypes.node,
  'data-testid': PropTypes.string,
}

/**
 * Menu content that is wrapped in a customized scrollable area.
 */
const useMenuContentStyles = makeStyles((theme) => {
  return {
    root: {
      boxSizing: 'border-box',
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      height: '100%',
      position: 'relative',
    },
    headerTextVertical: {
      display: 'flex',
      alignItems: 'center',
      position: 'absolute',
      right: '0.07rem',
      top: '2.5rem',
      transform: 'rotate(90deg)',
    },
    menu: {
      position: 'absolute',
      right: 0,
      top: 0,
      bottom: 0,
      left: 0,
      boxSizing: 'border-box',
    },
    button: {
      marginRight: 0,
      transform: 'none',
      transition: 'transform 250ms',
      willChange: 'transform',
    },
    overflow: {
      overflow: 'visible',
    },
    list: {
      paddingTop: 0,
    },
    container: {
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
    },
    content: {
      flex: 1,
      minHeight: 0,
    },
    hidden: {
      visibility: 'hidden',
    },
  }
})
export const MenuContent = React.memo(({className, children}) => {
  const styles = useMenuContentStyles()
  const {collapsed} = useContext(menuContext)
  // Unfortunately the ClickAwayListener does not play nicely together with
  // Menus/Select/Popper. When using Portals, the clicks are registered wrong.
  // When Portals are disabled (disablePortal), their positioning goes haywire.
  // The clicks outside are thus detected by individual event listeners that
  // toggle the menu state.
  return (
    <div className={clsx(className, styles.root)}>
      <div className={clsx(styles.menu)}>
        <div className={styles.container}>
          <div className={clsx(styles.content, collapsed && styles.hidden)}>
            <Scrollable>
              <List dense className={styles.list}>
                {children}
              </List>
            </Scrollable>
          </div>
        </div>
      </div>
    </div>
  )
})
MenuContent.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
}

/**
 * Menu submenus. Ensures that submenus are correctly placed underneath the
 * menu and their loading is delayed.
 */
export const MenuSubMenus = React.memo(({children}) => {
  return (
    <Box
      sx={{
        position: 'absolute',
        top: '0px',
        bottom: '0',
        left: 'calc(100% - 1px)',
      }}
    >
      {Children.map(children, (child) => {
        if (isValidElement(child)) {
          return (
            <Box
              sx={{
                position: 'absolute',
                top: 0,
                bottom: 0,
                right: 0,
                left: 0,
              }}
            >
              <child.type {...child.props} />
            </Box>
          )
        }
      })}
    </Box>
  )
})
MenuSubMenus.propTypes = {
  children: PropTypes.node,
}

/**
 * Menu item.
 */
const levelIndent = 1.8
const useMenuItemStyles = makeStyles((theme) => {
  return {
    listIcon: {
      fontsize: '1rem',
      minWidth: '1.5rem',
    },
    arrow: {
      marginLeft: theme.spacing(1),
      fontSize: '1.5rem',
    },
    listItem: {
      paddingTop: theme.spacing(0.75),
      paddingBottom: theme.spacing(0.75),
      position: 'relative',
    },
    actions: {
      display: 'flex',
      flexDirection: 'column',
    },
  }
})
export const MenuItem = React.memo(
  ({id, title = '', disableButton, level = 0}) => {
    const styles = useMenuItemStyles()
    const theme = useTheme()
    const {selected, setSelected, subMenuOpen} = useContext(menuContext)
    const opened = subMenuOpen && id === selected

    const handleClick = useCallback(() => setSelected?.(id), [id, setSelected])

    const contentText = (
      <ListItemText
        sx={{marginLeft: theme.spacing(level * levelIndent)}}
        primaryTypographyProps={{color: opened ? 'primary' : 'initial'}}
        primary={<FilterTitle label={title} />}
        data-testid={`menu-item-label-${id}`}
      />
    )

    const contentItem = (
      <Box sx={{display: 'flex', width: '100%', alignItems: 'center'}}>
        {contentText}
        <Box sx={{flexGrow: 1}} />
        {!disableButton && (
          <ListItemIcon className={styles.listIcon}>
            <NavigateNextIcon
              color={opened ? 'primary' : 'action'}
              className={styles.arrow}
            />
          </ListItemIcon>
        )}
      </Box>
    )

    const content = disableButton ? (
      <ListItem className={styles.listItem}>{contentItem}</ListItem>
    ) : (
      <ListItemButton className={styles.listItem} onClick={handleClick}>
        {contentItem}
      </ListItemButton>
    )

    return <Box>{content}</Box>
  },
)

MenuItem.propTypes = {
  id: PropTypes.number,
  title: PropTypes.string,
  disableButton: PropTypes.bool,
  level: PropTypes.number,
}

/**
 * Settings for a menu.
 */
export const MenuSettings = React.memo(() => {
  const {useIsStatisticsEnabled, useSetIsStatisticsEnabled} = useSearchContext()
  const [isStatisticsEnabled, setIsStatisticsEnabled] = [
    useIsStatisticsEnabled(),
    useSetIsStatisticsEnabled(),
  ]

  const handleStatsChange = useCallback(
    (event, value) => {
      setIsStatisticsEnabled(value)
    },
    [setIsStatisticsEnabled],
  )

  return (
    <MenuItemMUI>
      <FormControlLabel
        control={
          <Checkbox
            checked={isStatisticsEnabled}
            onChange={handleStatsChange}
          />
        }
        label='Show advanced statistics'
      />
    </MenuItemMUI>
  )
})
