import {components} from '../../components/markdown/components'
import Page from '../../components/page/Page'
import HomePageContent from './HomePageContent.mdx'

export default function Index() {
  return (
    <Page>
      <HomePageContent components={components} />
    </Page>
  )
}
