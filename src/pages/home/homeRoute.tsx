import {Route} from '../../components/routing/types'
import HomePage from './HomePage'

const homeRoute: Route = {
  path: '',
  component: HomePage,
}

export default homeRoute
