import {TextField} from '@mui/material'
import React, {RefObject, useCallback, useRef, useState} from 'react'

import {useArchiveChanges} from '../../components/archive/useArchive'
import DevTool from '../../components/devTools/DevTool'
import DevToolsJsonViewer from '../../components/devTools/DevToolsJsonViewer'
import {LayoutItem} from '../../components/layout/Layout'

function LayoutEditor({
  layoutRef,
  onLayoutChange,
}: {layoutRef: RefObject<LayoutItem | undefined>} & Pick<
  EntryDataEditorDevToolsProps,
  'onLayoutChange'
>) {
  const [editedLayout, setEditedLayout] = useState<string | undefined>(
    undefined,
  )

  const handleLayoutChange = useCallback(
    (event: React.ChangeEvent<HTMLTextAreaElement>) => {
      setEditedLayout(event.target.value)
      try {
        const newLayout = JSON.parse(event.target.value)
        onLayoutChange?.(newLayout)
        // eslint-disable-next-line no-empty
      } catch {}
    },
    [setEditedLayout, onLayoutChange],
  )

  return (
    <TextField
      hiddenLabel
      variant='filled'
      multiline
      fullWidth
      value={editedLayout || JSON.stringify(layoutRef.current, null, 2)}
      onChange={handleLayoutChange}
    />
  )
}

export type EntryDataEditorDevToolsProps = {
  layout?: LayoutItem
  onLayoutChange?: (layout: LayoutItem) => void
}

export default function EntryDataEditorDevTools({
  onLayoutChange,
  layout,
}: EntryDataEditorDevToolsProps) {
  const {changeStack: archiveChanges} = useArchiveChanges()
  const layoutRef = useRef(layout)
  return (
    <>
      {layout && (
        <DevTool toolKey='layout'>
          <DevToolsJsonViewer value={layout} />
        </DevTool>
      )}
      {layout && (
        <DevTool toolKey='layout-editor'>
          <LayoutEditor layoutRef={layoutRef} onLayoutChange={onLayoutChange} />
        </DevTool>
      )}
      <DevTool toolKey='changes'>
        <DevToolsJsonViewer value={archiveChanges} />
      </DevTool>
    </>
  )
}
