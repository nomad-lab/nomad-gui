import {screen, waitFor} from '@testing-library/react'
import {expect, it, vi} from 'vitest'

import {addDefinitions} from '../../components/archive/archive.helper'
import {getArchive} from '../../components/archive/useArchive'
import {GraphResponse} from '../../models/graphResponseModels'
import * as api from '../../utils/api'
import {
  importLazyComponents,
  renderWithRouteData,
} from '../../utils/test.helper'
import entryRoute from './entryRoute'

await importLazyComponents(entryRoute)

describe('EntryPage', () => {
  it('loads and initially renders', async () => {
    const mockedApi = vi.spyOn(api, 'graphApi')
    window.history.replaceState(null, '', '/entryId')

    const metadata = {
      entry_name: 'entry-name',
      references: ['http://example.com'],
    }

    mockedApi.mockResolvedValue({
      entryId: {
        entry_id: 'entryId',
        mainfile_path: 'entry.archive.json',
        metadata: {
          ...metadata,
          datasets: [],
        },
        archive: addDefinitions({
          metadata,
          material: {
            name: 'gold',
          },
        }),
      },
    } as GraphResponse)
    await renderWithRouteData(entryRoute)
    await waitFor(() =>
      expect(screen.getAllByText(/entry.archive.json/)).toHaveLength(3),
    )
    expect(screen.getByText(/Go to File/)).toBeInTheDocument()
    const archive = getArchive('entryId')
    expect(archive.archive.m_def).toMatchObject({name: 'Unknown'})
  })
})
