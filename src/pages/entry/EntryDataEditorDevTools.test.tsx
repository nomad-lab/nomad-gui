import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {vi} from 'vitest'

import {mockArchive} from '../../components/archive/archive.helper'
import {renderDevTools} from '../../components/devTools/DevTools.test'
import {LayoutItem} from '../../components/layout/Layout'
import EntryDataEditorDevTools from './EntryDataEditorDevTools'

async function renderEditorDevTools({
  activeToolKey = 'changes',
  onLayoutChange,
}: {
  activeToolKey?: string
  onLayoutChange?: (layout: LayoutItem) => void
}) {
  mockArchive({
    changes: [
      {
        action: 'upsert',
        path: 'test-qualified-property',
        new_value: 'test-new-value',
      },
    ],
  })
  return await renderDevTools({
    tools: (
      <EntryDataEditorDevTools
        onLayoutChange={onLayoutChange}
        layout={{type: 'text', content: 'test-content'}}
      />
    ),
    activeToolKey,
  })
}

describe('EntryDataEditorDevTools', () => {
  it('renders changes', async () => {
    await renderEditorDevTools({activeToolKey: 'changes'})
    await userEvent.click(screen.getByText(/0/))
    expect(screen.getByText(/test-qualified-property/)).toBeInTheDocument()
  })
  it('renders layout', async () => {
    await renderEditorDevTools({activeToolKey: 'layout'})
    expect(screen.getByText(/test-content/)).toBeInTheDocument()
  })
  it('handles layout changes', async () => {
    const onLayoutChange = vi.fn()
    await renderEditorDevTools({activeToolKey: 'layout-editor', onLayoutChange})
    const textBox = screen.getByRole('textbox')
    await userEvent.type(
      textBox,
      '{Control>}A{/Control}{{"type":"text","content":"changed-content"}',
    )
    expect(onLayoutChange).toHaveBeenCalledWith({
      type: 'text',
      content: 'changed-content',
    })
  })
})
