import {useCallback, useState} from 'react'

import {Layout, LayoutItem} from '../../components/layout/Layout'
import withErrorBoundary from '../../components/withErrorBoundary'
import EditorDevTools from './EntryDataEditorDevTools'

export type EntryDataEditorProps = {
  layout: LayoutItem
}

/**
 * Provides an editor (this can also be a read only "editor") for the data
 * of the current entry (depends on route) based on a given `layout`.
 */
const EntryDataEditor = withErrorBoundary(
  function Editor({layout}: EntryDataEditorProps) {
    const [editedLayout, setEditedLayout] = useState<LayoutItem | undefined>(
      undefined,
    )

    const handleLayoutChange = useCallback(
      (layout: LayoutItem) => {
        setEditedLayout(layout)
      },
      [setEditedLayout],
    )

    return (
      <>
        <EditorDevTools onLayoutChange={handleLayoutChange} layout={layout} />
        <Layout layout={editedLayout || layout} />
      </>
    )
  },
  {message: 'Error in EntryDataEditor'},
)

export default EntryDataEditor
