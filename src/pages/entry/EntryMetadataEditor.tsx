import {useMemo} from 'react'

import ProcessStatus from '../../components/ProcessStatus'
import {Layout, LayoutItem} from '../../components/layout/Layout'
import useRouteData from '../../components/routing/useRouteData'
import List from '../../components/values/containers/List'
import Text from '../../components/values/primitives/Text'
import {EntryResponse} from '../../models/graphResponseModels'
import {assert} from '../../utils/utils'
import entryRoute from './entryRoute'

function DatasetList({data}: {data: EntryResponse}) {
  // TODO We should remove the "|| []" at some point. This is only there,
  // to support the nomad generted synthetic API data which does not have
  // the datasets.
  const value = (data.metadata?.datasets as {dataset_name: string}[]) || []
  assert(value !== undefined, 'datasets are required')
  // TODO: It might be better to implement this with a special primitive value
  // component instead of a custom component. Especially, if this should
  // editable at some point.
  const datasetNames = value.map((dataset) => dataset.dataset_name)
  return (
    <List
      fullWidth
      label='datasets'
      placeholder='no datasets'
      value={datasetNames}
      renderValue={() => <Text />}
    />
  )
}

export type EntryMetadataEditorProps = {
  expandedByDefault?: boolean
  editable?: boolean
}

export default function EntryMetadataEditor({
  editable = false,
  expandedByDefault = false,
}: EntryMetadataEditorProps) {
  const data = useRouteData(entryRoute) as EntryResponse
  const layout = useMemo(
    () =>
      ({
        type: 'card',
        variant: 'highlighted',
        expandedByDefault,
        collapsedChildren: [
          {
            type: 'container',
            variant: 'flex',
            sx: {
              flexWrap: 'wrap',
              alignItems: 'center',
            },
            children: [
              {
                type: 'value',
                label: 'mainfile',
                grow: true,
                value: data.mainfile_path,
                component: {
                  Text: {
                    actions: 'CopyToClipboard',
                  },
                },
              },
              {
                type: 'value',
                component: {
                  Id: {
                    abbreviated: true,
                    labelActions: 'CopyToClipboard',
                  },
                },
                label: 'entry id',
                value: data.entry_id,
              },
            ],
          },
        ],
        children: [
          {
            type: 'container',
            variant: 'flex',
            sx: {
              flexWrap: 'wrap',
              alignItems: 'center',
            },
            children: [
              {
                type: 'quantity',
                editable,
                grow: true,
                label: 'entry name',
                property: 'metadata/entry_name',
                component: {
                  Text: {
                    variant: 'h1',
                    labelActions: <ProcessStatus key='status' entity={data} />,
                  },
                },
              },
              {
                type: 'value',
                label: 'created at',
                value: data.entry_create_time,
                component: {
                  Datetime: {
                    variant: 'datetime',
                  },
                },
              },
              {
                type: 'value',
                label: 'last change',
                value: data.complete_time,
                component: {
                  Datetime: {
                    variant: 'datetime',
                  },
                },
              },
            ],
          },
          {
            md: 6,
            editable: true,
            type: 'quantity',
            label: 'references',
            placeholder: 'no references',
            property: 'metadata/references',
            component: {
              List: {
                valueComponent: {
                  Link: {
                    actions: 'CopyToClipboard',
                  },
                },
                emptyValue: '',
              },
            },
          },
          {
            md: 6,
            type: 'element',
            element: <DatasetList data={data} />,
          },
        ],
      } as LayoutItem),
    [editable, expandedByDefault, data],
  )

  return <Layout layout={layout} />
}
