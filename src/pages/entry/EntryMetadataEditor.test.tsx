import {render, renderHook, screen} from '@testing-library/react'
import {vi} from 'vitest'

import {
  addDefinitions,
  mockArchive,
} from '../../components/archive/archive.helper'
import {useArchiveProperty} from '../../components/archive/useArchive'
import * as useRouteData from '../../components/routing/useRouteData'
import EntryMetadataEditor from './EntryMetadataEditor'

describe('EntryMetadataEditor', () => {
  it('renders with name and id', () => {
    const metadata = {
      entry_name: 'test-name',
      references: ['test-reference'],
      datasets: [
        {
          dataset_name: 'test-dataset',
        },
      ],
    }

    mockArchive({entryId: 'test-id', data: addDefinitions({metadata})})

    vi.spyOn(useRouteData, 'default').mockReturnValue({
      entry_id: 'test-id',
      mainfile_path: 'test-path',
      metadata,
    })

    render(<EntryMetadataEditor editable expandedByDefault />)

    const {result} = renderHook(() => useArchiveProperty('metadata/entry_name'))
    expect(result.current.value).toBe('test-name')

    expect(screen.getByText('test-id')).toBeInTheDocument()
    expect(screen.getByText('test-path')).toBeInTheDocument()
    expect(screen.getByText('test-dataset')).toBeInTheDocument()
    expect(screen.getByDisplayValue('test-name')).toBeInTheDocument()
    expect(screen.getByDisplayValue('test-reference')).toBeInTheDocument()
  })
})
