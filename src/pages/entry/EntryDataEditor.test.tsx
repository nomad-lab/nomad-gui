import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import {
  addDefinitions,
  mockArchive,
} from '../../components/archive/archive.helper'
import {LayoutItem} from '../../components/layout/Layout'
import EntryDataEditor from './EntryDataEditor'

describe('EntryDataEditor', async () => {
  const layout: LayoutItem = {
    type: 'container',
    children: [
      {
        type: 'quantity',
        property: 'test_quantity1',
        editable: true,
      },
      {
        type: 'subSection',
        property: 'nested',
        layout: {
          type: 'container',
          children: [
            {
              type: 'quantity',
              property: 'test_quantity2',
              editable: true,
            },
          ],
        },
      },
    ],
  }

  function mockData(withLoading: boolean = false) {
    const data = addDefinitions({
      test_quantity1: 'test_value1',
      nested: {
        test_quantity2: 'test_value2',
      },
    })
    return mockArchive({
      data: withLoading ? undefined : data,
    })
  }

  it('initially renders the editor with loading indicator', async () => {
    mockData(true)
    render(<EntryDataEditor layout={layout} />)
    expect(screen.queryByDisplayValue('test_value1')).not.toBeInTheDocument()
    expect(screen.queryByDisplayValue('test_value2')).not.toBeInTheDocument()
  })

  it('accepts changes to one quantity', async () => {
    const archive = mockData()
    render(<EntryDataEditor layout={layout} />)
    const value = screen.getByDisplayValue('test_value1')
    expect(value).toBeInTheDocument()
    await userEvent.type(value, 'typed{enter}')
    expect(archive.changeStack.length).toBe(1)
    expect(archive.changeStack[0]).toMatchObject({
      action: 'upsert',
      path: 'test_quantity1',
      new_value: 'test_value1typed',
    })
  })

  it('accepts changes to multiple quantities', async () => {
    const archive = mockData()
    render(<EntryDataEditor layout={layout} />)
    const value1 = screen.getByDisplayValue('test_value1')
    const value2 = screen.getByDisplayValue('test_value2')
    expect(value1).toBeInTheDocument()
    expect(value2).toBeInTheDocument()

    await userEvent.type(value1, 'a')
    await userEvent.type(value2, 'b')
    await userEvent.type(value1, 'cd{enter}')
    expect(archive.changeStack.length).toBe(3)
    expect(archive.changeStack[0]).toMatchObject({
      new_value: 'test_value1a',
    })
    expect(archive.changeStack[1]).toMatchObject({
      new_value: 'test_value2b',
    })
    expect(archive.changeStack[2]).toMatchObject({
      new_value: 'test_value1acd',
    })
  })
})
