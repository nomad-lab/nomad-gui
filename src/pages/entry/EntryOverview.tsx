import GoToIcon from '@mui/icons-material/ChevronRight'
import {Box, Button} from '@mui/material'
import {useCallback, useMemo, useState} from 'react'

import ErrorMessage from '../../components/app/ErrorMessage'
import EditStatus from '../../components/archive/EditStatus'
import useArchive from '../../components/archive/useArchive'
import {calculateRequestFromLayout} from '../../components/editor/utils'
import {LayoutItem} from '../../components/layout/Layout'
import useSelect from '../../components/navigation/useSelect'
import {PageTitle} from '../../components/page/Page'
import usePage from '../../components/page/usePage'
import Link from '../../components/routing/Link'
import useDataForRoute from '../../components/routing/useDataForRoute'
import useRoute from '../../components/routing/useRoute'
import useRouteData from '../../components/routing/useRouteData'
import {EntryRequest, MSectionRequest} from '../../models/graphRequestModels'
import {
  EntryResponse,
  GraphResponse,
  MSectionResponse,
} from '../../models/graphResponseModels'
import {Section} from '../../utils/metainfo'
import {assert} from '../../utils/utils'
import UploadMetadataEditor from '../upload/UploadMetadataEditor'
import EntryDataEditor from './EntryDataEditor'
import EntryMetadataEditor from './EntryMetadataEditor'
import EntrySubSectionTable from './EntrySubSectionTable'
import entryRoute, {archiveRequest} from './entryRoute'

function EntryOverviewEditor() {
  const {archive: archiveData} = useRouteData(entryRoute)
  const [error, setError] = useState<Error | undefined>()
  const schema = (archiveData?.data as MSectionResponse)?.m_def as Section
  const {isPage} = useSelect()

  const layout = useMemo(() => {
    let layout: LayoutItem
    const layouts = schema?.m_annotations?.layout as unknown as
      | LayoutItem
      | LayoutItem[]
      | undefined
    if (Array.isArray(layouts)) {
      layout = layouts[0]
    } else if (layouts !== undefined) {
      layout = layouts
    } else {
      return undefined
    }

    return {
      type: 'subSection',
      property: 'data',
      layout,
    } satisfies LayoutItem
  }, [schema])

  const request = useMemo(() => {
    const request = {
      archive: {
        ...archiveRequest,
        metadata: {
          entry_name: '*',
          references: '*',
        },
      },
    }
    if (layout) {
      calculateRequestFromLayout(layout, request.archive)
    }
    return request as EntryRequest
  }, [layout])

  const archive = useArchive()

  const onFetch = useCallback(
    (data: EntryResponse, fullResponse: GraphResponse) => {
      archive.updateArchive(
        request.archive as MSectionRequest,
        data?.archive as MSectionResponse,
        fullResponse,
      )
    },
    [archive, request],
  )

  useDataForRoute<EntryRequest, EntryResponse>({
    request,
    onFetch,
    onError: setError,
  })

  let content: React.ReactNode = ''
  if (error) {
    // eslint-disable-next-line no-console
    console.error(error)
    content = <ErrorMessage error={error} />
  } else {
    if (isPage) {
      if (layout) {
        content = <EntryDataEditor layout={layout} />
      } else {
        content = <ErrorMessage error={new Error('No layout defined')} />
      }
    }
  }

  return (
    <>
      <Box sx={{marginBottom: 2}}>
        <UploadMetadataEditor />
      </Box>
      <Box sx={{marginBottom: 4}}>
        <EntryMetadataEditor editable={isPage} expandedByDefault={isPage} />
      </Box>
      {content}
    </>
  )
}

export default function EntryOverview() {
  const {url} = useRoute()
  const {
    upload_id,
    mainfile_path,
    archive: rootSectionData,
  } = useRouteData(entryRoute)
  assert(
    rootSectionData !== undefined,
    'An entry should always have a root section',
  )

  const {isPage, isSelect} = useSelect()
  const {isScrolled} = usePage()

  const actions = (
    <Box display='flex' alignItems='center' flexDirection='row' gap={1}>
      <EditStatus />
      <Button
        variant='contained'
        component={Link}
        to={url({
          path: `/uploads/${upload_id}/files/${mainfile_path}`,
        })}
        endIcon={<GoToIcon />}
      >
        Go to File
      </Button>
    </Box>
  )

  // This memo is an optimization to avoid re-rendering the entire
  // EntryOverviewEditor after usePage causes a srcoll event triggered
  // rerender that only effects the page title.
  const pageContent = useMemo(
    () => (
      <>
        <Box
          sx={{
            marginTop: -1,
          }}
        >
          <EntryOverviewEditor />
        </Box>
        {isSelect && <EntrySubSectionTable data={rootSectionData} />}
      </>
    ),
    [isSelect, rootSectionData],
  )

  return (
    <>
      {isPage && (
        <PageTitle
          sx={{
            position: 'sticky',
            top: 0,
            marginTop: -1,
            marginX: -2,
            paddingX: 2,
            paddingY: 1,
            background: (theme) => theme.palette.background.default,
            zIndex: (theme) => theme.zIndex.appBar,
            ...(isScrolled
              ? {
                  borderBottom: '1px solid',
                  borderColor: (theme) => theme.palette.divider,
                }
              : {}),
          }}
          actions={actions}
        />
      )}
      {pageContent}
    </>
  )
}
