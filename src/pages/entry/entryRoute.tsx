import path from 'path-browserify'

import {getArchive} from '../../components/archive/useArchive'
import {Route} from '../../components/routing/types'
import {
  EntryMetadataRequest,
  EntryRequest,
  MSectionRequest,
} from '../../models/graphRequestModels'
import {
  EntryResponse,
  GraphResponse,
  MSectionResponse,
} from '../../models/graphResponseModels'

export const mDefRquest = {
  m_request: {
    directive: 'plain',
  },
}

export const archiveRequest = {
  m_request: {
    directive: 'plain',
    include_definition: 'both',
  },
  m_def: mDefRquest,
} as MSectionRequest

export const archiveRoute: Route<MSectionRequest, MSectionResponse> = {
  path: ':name',
  request: {...archiveRequest},
  lazyComponent: async () => import('./EntrySection'),
}
archiveRoute.children = [archiveRoute]

const entryRoute: Route<EntryRequest, EntryResponse> = {
  path: ':entryId',
  request: {
    mainfile_path: '*',
    entry_id: '*',
    upload_id: '*',
    process_status: '*',
    entry_create_time: '*',
    complete_time: '*',
    metadata: {
      entry_name: '*',
      references: '*',
      authors: '*',
      datasets: '*',
    } as EntryMetadataRequest,
    archive: {...archiveRequest},
  },
  lazyComponent: async () => import('./EntryPage'),
  breadcrumb: ({response}) => path.basename(response?.mainfile_path as string),
  children: [
    {
      path: '',
      breadcrumb: <b>overview</b>,
      lazyComponent: async () => import('./EntryOverview'),
    },
    {
      ...archiveRoute,
      path: 'archive',
      breadcrumb: <b>archive</b>,
    },
  ],
  onFetch: ({match, request, response, fullResponse}) => {
    if (request?.archive === undefined || response?.archive === undefined) {
      return
    }
    const entryId = match.path
    const archive = getArchive(entryId)

    archive.updateArchive(
      request.archive,
      response.archive,
      fullResponse as GraphResponse,
    )
  },
}

export default entryRoute
