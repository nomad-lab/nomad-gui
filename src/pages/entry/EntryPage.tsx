import {Divider} from '@mui/material'
import {useEffect, useMemo} from 'react'

import useArchive from '../../components/archive/useArchive'
import DevToolsJsonViewer from '../../components/devTools/DevToolsJsonViewer'
import useDevTools from '../../components/devTools/useDevTools'
import Nav from '../../components/navigation/Nav'
import NavItem from '../../components/navigation/NavItem'
import {RecentPage} from '../../components/navigation/RecentNav'
import useSelect from '../../components/navigation/useSelect'
import Page from '../../components/page/Page'
import Link from '../../components/routing/Link'
import Outlet from '../../components/routing/Outlet'
import useRoute from '../../components/routing/useRoute'
import useRouteData from '../../components/routing/useRouteData'
import useRecent from '../../hooks/useRecent'
import UploadNavAbout from '../upload/UploadNavAbout'
import uploadRoute from '../upload/uploadRoute'
import EntryArchiveNav from './EntryArchiveNav'
import EntryNavItem from './EntryNavAbout'
import entryRoute from './entryRoute'

export default function EntryPage() {
  const {url, fullMatch, index} = useRoute()
  const nextPath = fullMatch[index + 1]?.path
  const tab = nextPath === '' ? 'overview' : nextPath
  const {entry_id, mainfile_path} = useRouteData(entryRoute)
  const {upload_id} = useRouteData(uploadRoute)
  const {isPage, pageVariant} = useSelect()

  const devTools = useDevTools()
  const archive = useArchive()

  useEffect(() => {
    devTools.registerTool(
      'archive',
      <DevToolsJsonViewer value={archive.archive} />,
    )
    return () => devTools.unregisterTool('archive')
  }, [devTools, archive])

  useRecent<RecentPage>(
    'upload',
    upload_id as string,
    useMemo(
      () => ({
        [entry_id as string]: {
          url: url(),
          selectUrl: url({path: 'archive'}),
          id: entry_id as string,
          name: mainfile_path as string,
        },
      }),
      [entry_id, mainfile_path, url],
    ),
  )

  return (
    <Page
      variant={pageVariant}
      fullwidth
      navigation={
        <Nav>
          <NavItem
            label='projects'
            component={Link}
            to={url({path: '../../../../uploads'})}
          />
          <Divider />
          <UploadNavAbout />
          <EntryNavItem />
          {isPage && (
            <NavItem
              label='entry overview'
              selected={tab === 'overview'}
              highlighted={tab === 'overview'}
              component={Link}
              to={url({path: ''})}
            />
          )}
          <EntryArchiveNav
            label='data'
            expanded={tab !== 'archive' ? false : undefined}
            selected={tab === 'archive'}
          />
        </Nav>
      }
    >
      <Outlet />
    </Page>
  )
}
