import {useMemo} from 'react'

import useRoute from '../../components/routing/useRoute'
import PlainDataTable from '../../components/table/PlainDataTable'
import {MSectionResponse} from '../../models/graphResponseModels'
import {
  Section as SectionDefinition,
  SubSection as SubSectionDefinition,
} from '../../utils/metainfo'

const subSectionColumns = [
  {
    enableSorting: true,
    accessorKey: 'name',
    header: 'Name',
    grow: 1,
  },
]

export type EntrySubSectionTableProps = {
  data: MSectionResponse
}

export default function EntrySubSectionTable({
  data,
}: EntrySubSectionTableProps) {
  const {navigate} = useRoute()

  const definition = data.m_def as SectionDefinition
  const subSections = useMemo(() => {
    return Object.keys(definition.all_sub_sections)
      .filter((key) => data[key] !== undefined)
      .map((key) => definition.all_sub_sections[key])
      .toSorted((a, b) => a.name.localeCompare(b.name))
  }, [data, definition])

  if (subSections.length === 0) {
    return null
  }

  return (
    <PlainDataTable
      onRowClick={(data: SubSectionDefinition) => {
        navigate({
          path: data.name,
        })
      }}
      columns={subSectionColumns}
      data={subSections}
    />
  )
}
