import {Box, Card, CardContent, CardHeader} from '@mui/material'
import {useMemo} from 'react'

import ErrorBoundary from '../../components/ErrorBoundary'
import QuantityValue from '../../components/archive/QuantityValue'
import JsonViewer from '../../components/fileviewer/JsonViewer'
import {PageTitle} from '../../components/page/Page'
import Outlet from '../../components/routing/Outlet'
import useRoute from '../../components/routing/useRoute'
import useRouteData from '../../components/routing/useRouteData'
import {MSectionRequest} from '../../models/graphRequestModels'
import {MSectionResponse} from '../../models/graphResponseModels'
import {Section as SectionDefinition} from '../../utils/metainfo'
import {JSONObject, JSONValue} from '../../utils/types'
import EntrySubSectionTable from './EntrySubSectionTable'

const sortRawDataKeys = (a: string, b: string) => {
  if (a.startsWith('m_') === b.startsWith('m_')) {
    return a.localeCompare(b)
  }
  if (a.startsWith('m_')) {
    return -1
  }
  return 1
}

function Section() {
  const data = useRouteData<MSectionRequest, MSectionResponse>()

  const definition = data.m_def as SectionDefinition
  const quantities = useMemo(() => {
    return Object.keys(definition.all_quantities)
      .filter((key) => data[key] !== undefined)
      .map((key) => definition.all_quantities[key])
    //.toSorted((a, b) => a.name.localeCompare(b.name))
  }, [data, definition])

  return (
    <>
      <EntrySubSectionTable data={data} />
      <Card>
        <CardHeader title='Quantities' />
        <CardContent sx={{display: 'flex', flexDirection: 'column', gap: 2}}>
          {quantities.map((quantity) => (
            <ErrorBoundary key={quantity.name}>
              <QuantityValue
                quantityDef={quantity}
                value={data[quantity.name] as JSONValue | undefined}
              />
            </ErrorBoundary>
          ))}
        </CardContent>
      </Card>
    </>
  )
}

export default function EntrySection() {
  const {isLeaf, fullMatch} = useRoute()
  const data = useRouteData<MSectionRequest, MSectionResponse>()
  if (!isLeaf) {
    return <Outlet />
  }
  return (
    <>
      <PageTitle title={fullMatch[fullMatch.length - 1].path} />
      <Box sx={{display: 'flex', flexDirection: 'column', gap: 2}}>
        {data.m_def && <Section />}
        <Card>
          <CardHeader title='Raw data' />
          <CardContent>
            <JsonViewer
              value={data as JSONObject}
              defaultInspectDepth={1}
              objectSortKeys={sortRawDataKeys}
            />
          </CardContent>
        </Card>
      </Box>
    </>
  )
}
