import RepeatedSubSectionIcon from '@mui/icons-material/DataArray'
import SectionIcon from '@mui/icons-material/DataObjectOutlined'
import QuantityIcon from '@mui/icons-material/LooksOneOutlined'
import UnknownIcon from '@mui/icons-material/QuestionMark'
import {useMemo} from 'react'

import TreeNav, {TreeNavProps} from '../../components/navigation/TreeNav'
import useRoute from '../../components/routing/useRoute'
import {Section} from '../../utils/metainfo'
import {JSONObject} from '../../utils/types'
import {assert} from '../../utils/utils'
import {archiveRequest} from './entryRoute'

export type EntryArchiveNavProps = Pick<
  TreeNavProps,
  'getChildProps' | 'label' | 'expanded' | 'selected'
>

function filterChildKeys(data: JSONObject, key: string) {
  const sectionDef = data.m_def as Section
  assert(
    sectionDef !== undefined,
    'The section definition is missing in the API data.',
  )
  return sectionDef.all_sub_sections[key] !== undefined
}

function expandable(data: JSONObject) {
  const sectionDef = data?.m_def as Section
  if (sectionDef === undefined) {
    return false
  }
  return (
    Object.keys(data).filter(
      (key) =>
        !key.startsWith('m_') && sectionDef.all_sub_sections[key] !== undefined,
    ).length > 0
  )
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function getChildProps(data: JSONObject, key: string, path: string) {
  const sectionDef = data.m_def as Section
  const property = sectionDef.all_properties[key] || {}

  const isQuantity = property.m_def === 'Quantity'
  const isSubSection = property.m_def === 'SubSection' && !property.repeats
  const isUnknown = property.m_def === undefined
  const isRepeatingSubSection =
    property.m_def === 'SubSection' && property.repeats

  let icon
  if (isQuantity) {
    icon = <QuantityIcon />
  } else if (isSubSection) {
    icon = <SectionIcon />
  } else if (isRepeatingSubSection) {
    icon = <RepeatedSubSectionIcon />
  } else if (isUnknown) {
    icon = <UnknownIcon />
  } else {
    throw new Error('Impossible state.')
  }

  const sharedProps = {
    variant: 'treeNode',
    icon,
    alignWithExpandIcons: true,
  }

  if (isQuantity || isUnknown) {
    return {
      ...sharedProps,
      expandable: false,
      disabled: isUnknown,
    } as TreeNavProps
  }

  if (isSubSection) {
    return {
      ...sharedProps,
      getChildProps,
      filterChildKeys,
      expandable,
    } as TreeNavProps
  }

  if (isRepeatingSubSection) {
    return {
      ...sharedProps,
      expandable: true,
      getChildProps: (data: JSONObject, key: string, path: string) =>
        ({
          ...sharedProps,
          getChildProps,
          filterChildKeys,
          expandable,
          icon: <SectionIcon />,
          label: key,
          path: `${path}[${key}]`,
        } as TreeNavProps),
    } as TreeNavProps
  }

  throw new Error('Impossible state.')
}

export default function EntryArchiveNav({
  ...treeNavProps
}: EntryArchiveNavProps) {
  const {fullMatch, index} = useRoute()
  const highlighted =
    fullMatch.length === index + 2 && fullMatch[index + 1]?.path === 'archive'

  const request = useMemo(
    () =>
      ({
        ...archiveRequest,
      } as JSONObject),
    [],
  )

  return (
    <TreeNav
      highlighted={treeNavProps.selected && highlighted}
      path='archive'
      variant='menuItem'
      icon={undefined}
      request={request}
      expandable={expandable}
      getChildProps={getChildProps}
      filterChildKeys={filterChildKeys}
      {...treeNavProps}
    />
  )
}
