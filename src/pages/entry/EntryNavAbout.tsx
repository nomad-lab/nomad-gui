import ProcessStatus from '../../components/ProcessStatus'
import NavAbout from '../../components/navigation/NavAbout'
import useRouteData from '../../components/routing/useRouteData'
import CopyToClipboard from '../../components/values/actions/CopyToClipboard'
import Value from '../../components/values/containers/Value'
import Id from '../../components/values/primitives/Id'
import {EntryRequest} from '../../models/graphRequestModels'
import {EntryResponse} from '../../models/graphResponseModels'

export default function EntryNavAbout() {
  const entry = useRouteData<EntryRequest, EntryResponse>()
  const {mainfile_path, entry_id} = entry
  return (
    <NavAbout label='entry' title={mainfile_path as string}>
      <Value value={entry_id}>
        <Id abbreviated />
        <CopyToClipboard />
      </Value>
      <div style={{flexGrow: 1}} />
      <ProcessStatus entity={entry} variant='icon' />
    </NavAbout>
  )
}
