import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {vi} from 'vitest'

import {renderWithRouteData} from '../../utils/test.helper'
import appsRoute from './appsRoute'
import * as getApps from './utils'

describe('AppPage', async () => {
  beforeAll(() => {
    window.history.replaceState(null, '', '/explore')
    vi.spyOn(getApps, 'default').mockReturnValue({
      'test-app': {
        id: 'test-app',
        entry_point_type: 'app',
        app: {
          path: 'test',
          category: 'Theory',
          label: 'Test App',
        },
      },
    })
  })
  it('renders apps correctly', async () => {
    await renderWithRouteData(appsRoute)
    screen.getByText('Apps', {selector: 'h1'})
    screen.getByText('Test App')
  }, 30000)
  it('pins/unpins correctly', async () => {
    await renderWithRouteData(appsRoute)
    screen.getByText('Apps', {selector: 'h1'})
    expect(screen.getAllByText('Test App')).toHaveLength(1)
    // Press pin button in the original app card, expect the second one to pop
    // up at the top
    const pinButton = screen.getByRole('button', {name: /pin/i})
    await userEvent.click(pinButton)
    expect(screen.getAllByText('Test App')).toHaveLength(2)

    // Press pin button in new, pinned app card, expect it to disappear
    const pinButtonNew = screen.getAllByRole('button', {name: /pin/i})[0]
    await userEvent.click(pinButtonNew)
    expect(screen.getAllByText('Test App')).toHaveLength(1)
  })
})
