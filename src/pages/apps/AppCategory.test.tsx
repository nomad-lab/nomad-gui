import {render, screen} from '@testing-library/react'

import AppCategory from './AppCategory'

describe('AppCategory', async () => {
  it('renders all information correctly', async () => {
    render(<AppCategory title='Test category'>{'Child'}</AppCategory>)
    screen.getByText('Test category')
  })
})
