import {describe, expect, it, vi} from 'vitest'

import config from '../../config'
import useApps from './utils'

// Mock config
vi.mock('../../config', () => ({
  default: {
    plugins: {
      entry_points: {
        items: [],
      },
    },
  },
}))

describe('getApps', () => {
  it('should return an empty object when there are no apps', () => {
    // @ts-expect-error Items are added on the fly to the config model by gui_config.py
    config.plugins.entry_points.items = []
    const result = useApps()
    expect(result).toEqual({})
  })

  it('should ignore entry points that are not apps', () => {
    // Mock entry points with mixed types
    // @ts-expect-error Items are added on the fly to the config model by gui_config.py
    config.plugins.entry_points.items = [
      {
        id: 'app1',
        entry_point_type: 'app',
        name: 'App 1',
      },
      {
        id: 'nonApp1',
        entry_point_type: 'non-app',
        name: 'Non App 1',
      },
    ]

    const result = useApps()

    expect(result).toEqual({
      app1: {
        id: 'app1',
        entry_point_type: 'app',
        name: 'App 1',
      },
    })
  })
})
