import userEvent from '@testing-library/user-event'
import {PropsWithChildren} from 'react'
import {vi} from 'vitest'

import Router from '../../components/routing/Router'
import {Route} from '../../components/routing/types'
import {createMatchMedia, render, screen} from '../../utils/test.helper'
import AppCard, {AppProps} from './AppCard'

export const renderOptions = {
  wrapper: ({children}: PropsWithChildren) => {
    const recursiveRoute: Route = {
      path: ':id',
      component: () => children,
    }
    recursiveRoute.children = [recursiveRoute]
    const route = {
      path: '',
      children: [recursiveRoute],
    }
    return <Router route={route} />
  },
}

const appProps: AppProps = {
  app: {
    label: 'Test app',
    description: 'Test app description',
    readme: 'This is a readme.',
    path: 'test-app-path',
    category: 'Theory',
  },
  pinned: false,
}

describe('AppCard', async () => {
  it('renders all information correctly', async () => {
    render(<AppCard {...appProps} />, renderOptions)
    screen.getByText('Test app')
    screen.getByText('Test app description')
  })
  it('disables about button if readme not available', async () => {
    render(
      <AppCard {...{...appProps, app: {...appProps.app, readme: undefined}}} />,
      renderOptions,
    )
    expect(screen.getByRole('button', {name: /about/i})).toBeDisabled()
  })
  it('opens/closes readme correctly', async () => {
    render(<AppCard {...appProps} />, renderOptions)
    const text = 'This is a readme.'
    await userEvent.click(screen.getByRole('button', {name: /about/i}))
    expect(screen.getByText(text)).toBeInTheDocument()
    await userEvent.click(screen.getByRole('button', {name: /close/i}))
    expect(screen.queryByText(text)).not.toBeInTheDocument()
  })
  it('opens/close config correctly', async () => {
    render(<AppCard {...appProps} />, renderOptions)
    vi.spyOn(window, 'matchMedia').mockImplementation(createMatchMedia(500))
    const text = '"test-app-path"'
    await userEvent.click(screen.getByRole('button', {name: /config/i}))
    expect(screen.getByText(text)).toBeInTheDocument()
    await userEvent.click(screen.getByRole('button', {name: /close/i}))
    expect(screen.queryByText(text)).not.toBeInTheDocument()
  })
  it.each([
    [true, 'Unpin app'],
    [false, 'Pin app'],
  ])('handles pinning correctly', async (pinned, tooltip) => {
    const onPin = vi.fn()
    render(
      <AppCard {...appProps} pinned={pinned} onPin={onPin} />,
      renderOptions,
    )
    await userEvent.hover(screen.getByRole('button', {name: /pin/i}))
    const tip = await screen.findByRole('tooltip')
    expect(tip.textContent).toBe(tooltip)
    await userEvent.click(screen.getByRole('button', {name: /pin/i}))
    expect(onPin).toHaveBeenCalled()
  })
})
