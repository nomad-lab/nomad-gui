import React from 'react'

import Page from '../../components/page/Page'
import useRoute from '../../components/routing/useRoute'
import {SearchContext} from '../../legacy/components/search/SearchContext'
import SearchPage from '../../legacy/components/search/SearchPage.jsx'
import getApps from './utils'

const apps = getApps()

export default function AppPage() {
  const {match, index} = useRoute()
  const appPath = match[index].path
  const appEntryPoint = Object.values(apps).find(
    (app) => app.app.path === appPath,
  )
  const app = appEntryPoint?.app
  if (!app) {
    return <div>App not found</div>
  }

  return (
    <Page fullwidth fullheight disablePadding>
      <SearchContext
        resource={app.resource}
        initialPagination={app.pagination}
        initialColumns={app.columns}
        initialRows={app.rows}
        initialMenu={app.menu}
        initialSearchQuantities={app?.search_quantities}
        initialFiltersLocked={app.filters_locked}
        initialDashboard={app?.dashboard}
        initialSearchSyntaxes={app?.search_syntaxes}
        id={app?.path}
      >
        <SearchPage />
      </SearchContext>
    </Page>
  )
}
