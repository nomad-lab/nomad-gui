import {Route} from '../../components/routing/types'
import {
  EntryMetadataRequest,
  EntryRequest,
} from '../../models/graphRequestModels'
import {EntryResponse} from '../../models/graphResponseModels'
import {archiveRequest, archiveRoute} from '../entry/entryRoute'
import AppsPage from './AppsPage'

export const appEntryRoute: Route<EntryRequest, EntryResponse> = {
  path: 'explore/:appName/:entryId',
  requestPath: ({path}) => `entries/${path}`,
  request: {
    mainfile_path: '*',
    entry_id: '*',
    upload_id: '*',
    process_status: '*',
    entry_create_time: '*',
    complete_time: '*',
    metadata: {
      entry_name: '*',
      references: '*',
      authors: '*',
      datasets: '*',
    } as EntryMetadataRequest,
    archive: {...archiveRequest},
  },
  lazyComponent: async () => import('./AppEntryPage'),
  breadcrumb: ({path}) => path,
  children: [
    {
      path: '',
      breadcrumb: <b>overview</b>,
      lazyComponent: async () => import('../entry/EntryOverview'),
    },
    {
      ...archiveRoute,
      path: 'archive',
      breadcrumb: <b>archive</b>,
    },
  ],
}

export const appRoute: Route = {
  path: `explore/:appName`,
  lazyComponent: async () => import('./AppPage'),
}

const appsRoute: Route = {
  path: 'explore',
  component: AppsPage,
}

export default appsRoute
