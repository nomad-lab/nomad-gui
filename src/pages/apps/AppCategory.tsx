import {Box, Grid2 as Grid, Stack, Typography} from '@mui/material'
import {Children, PropsWithChildren, ReactNode} from 'react'

export type AppCategoryProps = {
  icon?: ReactNode
  title: string
}

export default function AppCategory({
  icon,
  title,
  children,
}: PropsWithChildren<AppCategoryProps>) {
  const arrayChildren = Children.toArray(children)

  return (
    <Box>
      <Stack direction='row' spacing={1} alignItems='center'>
        {icon}
        <Typography variant='h2' sx={{mb: 1, mt: -1}}>
          {title}
        </Typography>
      </Stack>
      <Box sx={{mb: 1}} />
      <Grid container spacing={2}>
        {Children.map(arrayChildren, (child) => {
          return <Grid size={{xs: 12, sm: 6, md: 4, lg: 3}}>{child}</Grid>
        })}
      </Grid>
    </Box>
  )
}
