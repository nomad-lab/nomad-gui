import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Stack,
  Tooltip,
  Typography,
} from '@mui/material'
import {useCallback, useState} from 'react'

import JsonViewer from '../../components/fileviewer/JsonViewer'
import {Dataset, Dev, Help, Pin} from '../../components/icons'
import Markdown from '../../components/markdown/Markdown'
import useRoute from '../../components/routing/useRoute'
import {App} from '../../models/config'

export type AppProps = {
  app: App
  pinned?: boolean
  onPin?: () => void
}

export default function AppCard({app, pinned, onPin}: AppProps) {
  const {navigate} = useRoute()
  const [isDescriptionOpen, setIsDescriptionOpen] = useState<boolean>(false)
  const [isConfigOpen, setIsConfigOpen] = useState<boolean>(false)
  const toggleDescription = useCallback(
    () => setIsDescriptionOpen((old) => !old),
    [],
  )
  const toggleConfig = useCallback(() => setIsConfigOpen((old) => !old), [])
  const hasReadme = !!app.readme

  return (
    <Card>
      <CardActionArea
        onClick={() => navigate({path: app.path, search: {}})}
        sx={{
          height: 138,
          display: 'flex',
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
        }}
      >
        <CardContent>
          <Stack spacing={1}>
            <Stack alignItems='center' direction='row' spacing={1}>
              <Dataset />
              <Typography
                variant='h3'
                sx={{
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                  display: '-webkit-box',
                  WebkitLineClamp: 2,
                  WebkitBoxOrient: 'vertical',
                }}
              >
                {app.label}
              </Typography>
            </Stack>
            <Typography
              sx={{
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                display: '-webkit-box',
                WebkitLineClamp: 3,
                WebkitBoxOrient: 'vertical',
              }}
              color='text.secondary'
            >
              {app.description}
            </Typography>
          </Stack>
        </CardContent>
      </CardActionArea>
      <CardActions sx={{justifyContent: 'flex-end'}}>
        <Tooltip title='App configuration'>
          <IconButton size='small' aria-label='config' onClick={toggleConfig}>
            <Dev />
          </IconButton>
        </Tooltip>
        <Tooltip title='About this app'>
          <span>
            <IconButton
              size='small'
              aria-label='about'
              disabled={!hasReadme}
              onClick={toggleDescription}
            >
              <Help />
            </IconButton>
          </span>
        </Tooltip>
        <Tooltip title={pinned ? 'Unpin app' : 'Pin app'}>
          <IconButton
            size='small'
            aria-label='pin'
            color={pinned ? 'primary' : 'default'}
            onClick={() => onPin?.()}
          >
            <Pin />
          </IconButton>
        </Tooltip>
      </CardActions>
      {isConfigOpen && (
        <Dialog open onClose={toggleConfig}>
          <DialogTitle>{'App configuration'}</DialogTitle>
          <DialogContent>
            <JsonViewer value={app} defaultInspectDepth={1} />
          </DialogContent>
          <DialogActions>
            <Button aria-label='close' onClick={toggleConfig}>
              Close
            </Button>
          </DialogActions>
        </Dialog>
      )}
      {hasReadme && isDescriptionOpen && (
        <Dialog open onClose={toggleDescription}>
          <DialogTitle>{'About this app'}</DialogTitle>
          <DialogContent>
            <Markdown content={app.readme as string} />
          </DialogContent>
          <DialogActions>
            <Button aria-label='close' onClick={toggleDescription}>
              Close
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </Card>
  )
}
