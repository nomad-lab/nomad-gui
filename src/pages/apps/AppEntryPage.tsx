import {Divider} from '@mui/material'

import Nav from '../../components/navigation/Nav'
import NavItem from '../../components/navigation/NavItem'
import useSelect from '../../components/navigation/useSelect'
import Page from '../../components/page/Page'
import Link from '../../components/routing/Link'
import Outlet from '../../components/routing/Outlet'
import useRoute from '../../components/routing/useRoute'
import EntryArchiveNav from '../entry/EntryArchiveNav'
import EntryNavItem from '../entry/EntryNavAbout'

export default function EntryPage() {
  const {url, fullMatch, index} = useRoute()
  const nextPath = fullMatch[index + 1]?.path
  const tab = nextPath === '' ? 'overview' : nextPath
  const {isPage, pageVariant} = useSelect()

  return (
    <Page
      variant={pageVariant}
      fullwidth
      navigation={
        <Nav>
          <NavItem
            label='projects'
            component={Link}
            to={url({path: '../../../../uploads'})}
          />
          <Divider />
          <EntryNavItem />
          {isPage && (
            <NavItem
              label='entry overview'
              selected={tab === 'overview'}
              highlighted={tab === 'overview'}
              component={Link}
              to={url({path: ''})}
            />
          )}
          <EntryArchiveNav
            label='data'
            expanded={tab !== 'archive' ? false : undefined}
            selected={tab === 'archive'}
          />
        </Nav>
      }
    >
      <Outlet />
    </Page>
  )
}
