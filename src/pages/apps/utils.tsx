import config from '../../config'
import {AppEntryPoint} from '../../models/config'

/**
 * Function for returning the available apps.
 */
export default function getApps(): {[key: string]: AppEntryPoint} {
  const apps: {[key: string]: AppEntryPoint} = {}
  // @ts-expect-error Items are added on the fly to the config model by gui_config.py
  config.plugins.entry_points.items.forEach((value: AppEntryPoint) => {
    if (value?.entry_point_type !== 'app') return
    apps[value.id as string] = value
  })
  return apps
}
