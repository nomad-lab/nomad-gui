import {Box, InputAdornment, Stack, TextField, Typography} from '@mui/material'
import {useCallback, useMemo} from 'react'

import {Pin, Search} from '../../components/icons'
import Page from '../../components/page/Page'
import useStorage from '../../hooks/useStorage'
import {AppEntryPoint} from '../../models/config'
import AppCard from './AppCard'
import AppCategory, {AppCategoryProps} from './AppCategory'
import getApps from './utils'

export default function AppsPage() {
  const [pinned, setPinned] = useStorage<string[]>('global', 'pinned')
  const apps = getApps()

  // Here we pre-construct a map of app categories
  const categories = useMemo<
    (AppCategoryProps & {items: AppEntryPoint[]})[]
  >(() => {
    const categories: {
      [key: string]: AppCategoryProps & {items: AppEntryPoint[]}
    } = {}
    Object.values(apps).forEach((app) => {
      if (!categories[app.app.category]) {
        categories[app.app.category] = {title: app.app.category, items: []}
      }
      categories[app.app.category].items.push(app)
    })
    return Object.values(categories)
  }, [apps])

  const pinnedAvailable = pinned?.filter((id) => !!apps[id])

  const handlePin = useCallback(
    (id: string) => {
      setPinned((prev: string[]) => {
        const newValue = [...(prev || [])]
        const index = newValue.indexOf(id)
        if (index >= 0) {
          newValue.splice(index, 1)
        } else {
          newValue.push(id)
        }
        return newValue
      })
    },
    [setPinned],
  )

  return (
    <Page>
      <Stack direction='row' spacing={2} alignItems='center'>
        <Typography variant='h1'>Apps</Typography>
        <Box sx={{flexGrow: 1}} />
        <TextField
          sx={{flexBasis: 300}}
          size='small'
          placeholder='Search…'
          slotProps={{
            input: {
              startAdornment: (
                <InputAdornment position='start'>
                  <Search />
                </InputAdornment>
              ),
            },
          }}
        />
      </Stack>
      <Box mb={3} />
      <Stack spacing={4}>
        {pinnedAvailable?.length ? (
          <AppCategory title='Pinned' icon={<Pin />}>
            {pinnedAvailable.map((id) => (
              <AppCard
                key={id}
                app={apps[id].app}
                pinned={true}
                onPin={() => handlePin(id)}
              />
            ))}
          </AppCategory>
        ) : null}
        {categories.map((category) => (
          <AppCategory key={category.title} {...category}>
            {category.items.map((app) => (
              <AppCard
                key={app.id}
                app={app.app}
                pinned={pinned?.includes?.(app.id as string)}
                onPin={() => handlePin(app.id as string)}
              />
            ))}
          </AppCategory>
        ))}
      </Stack>
    </Page>
  )
}
