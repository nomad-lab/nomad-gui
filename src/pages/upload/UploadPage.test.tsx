import {screen, waitFor} from '@testing-library/react'
import {expect, it, vi} from 'vitest'

import {GraphResponse} from '../../models/graphResponseModels'
import * as api from '../../utils/api'
import {
  importLazyComponents,
  renderWithRouteData,
} from '../../utils/test.helper'
import uploadRoute from './uploadRoute'

await importLazyComponents(uploadRoute)

describe('UploadPage', () => {
  it('loads and initially renders the overview', async () => {
    const mockedApi = vi.spyOn(api, 'graphApi')
    window.history.replaceState(null, '', '/project')
    mockedApi.mockResolvedValue({
      project: {
        upload_name: 'Project',
        upload_id: 'project',
        main_author: {
          name: 'Main Author',
        },
        coauthors: [],
        files: {
          m_response: {
            pagination: {
              page_size: 10,
              page: 0,
              total: 0,
            },
          },
        },
      },
    } as GraphResponse)
    await renderWithRouteData(uploadRoute)
    await waitFor(() => expect(screen.getAllByText(/Project/)).toHaveLength(2))
  })
})
