import React from 'react'

import {
  SelectDialogButton,
  SelectDialogButtonProps,
} from '../../components/navigation/Select'
import useRoute from '../../components/routing/useRoute'

export type UploadFilesSelectDialogProps<T extends React.ComponentType> = Omit<
  SelectDialogButtonProps<T>,
  'initialPath' | 'selectOptions'
>

export default function UploadFilesSelectDialog<T extends React.ComponentType>(
  props: UploadFilesSelectDialogProps<T>,
) {
  const {
    title = 'move',
    description = 'Select the directory where you want to move the file(s).',
    ...selectDialogButtonProps
  } = props

  const {url, match, index} = useRoute()
  // The overview route needs to be changed to the files route
  const initialPath = match[index].path === '' ? url({path: 'files'}) : url()

  return (
    <SelectDialogButton<T>
      {...(selectDialogButtonProps as SelectDialogButtonProps<T>)}
      title={title}
      description={description}
      selectOptions={{entity: 'directory'}}
      initialPath={initialPath}
    />
  )
}
