import {Divider} from '@mui/material'
import {useMemo} from 'react'

import Nav from '../../components/navigation/Nav'
import NavItem from '../../components/navigation/NavItem'
import RecentNav, {RecentPage} from '../../components/navigation/RecentNav'
import useSelect from '../../components/navigation/useSelect'
import Page from '../../components/page/Page'
import Link from '../../components/routing/Link'
import Outlet from '../../components/routing/Outlet'
import useRoute from '../../components/routing/useRoute'
import useRouteData from '../../components/routing/useRouteData'
import useRecent from '../../hooks/useRecent'
import {JSONObject} from '../../utils/types'
import UploadFilesNav from './UploadFilesNav'
import UploadNavAbout from './UploadNavAbout'
import uploadRoute from './uploadRoute'

export default function UploadPage() {
  const {fullMatch, index, url} = useRoute()
  const leafPath = fullMatch[index + 1]?.path
  const tab = leafPath === '' ? 'overview' : leafPath
  const {upload_id, upload_name} = useRouteData(uploadRoute)
  const {isPage, pageVariant, entity, navigateToEntry} = useSelect()

  useRecent<RecentPage>(
    'global',
    'uploads',
    useMemo(
      () => ({
        [upload_id as string]: {
          url: url(),
          selectUrl: url({path: 'files'}),
          id: upload_id as string,
          name: upload_name as string,
        },
      }),
      [upload_id, upload_name, url],
    ),
  )

  return (
    <Page
      variant={pageVariant}
      fullwidth={tab !== 'overview'}
      navigation={
        <Nav>
          <NavItem
            label='projects'
            component={Link}
            to={url({path: '../../uploads'})}
          />
          <Divider />
          <UploadNavAbout />
          {isPage && (
            <NavItem
              label='project overview'
              selected={tab === 'overview'}
              highlighted={tab === 'overview'}
              component={Link}
              to={url({path: ''})}
            />
          )}
          <UploadFilesNav
            expanded={tab !== 'files' ? false : undefined}
            selected={tab === 'files'}
            getChildProps={(data, key) => {
              if (entity === 'directory') {
                return {disabled: (data[key] as JSONObject).m_is === 'File'}
              }
              return {}
            }}
          />
          {(isPage || navigateToEntry) && (
            <NavItem
              label='entries'
              selected={tab === 'entries'}
              highlighted={tab === 'entries'}
              component={Link}
              to={url({path: 'entries'})}
            />
          )}
          {isPage && (
            <NavItem
              label='settings'
              selected={tab === 'settings'}
              highlighted={tab === 'settings'}
              component={Link}
              to={url({path: 'settings'})}
            />
          )}
          {(isPage || navigateToEntry) && (
            <>
              <Divider />
              <RecentNav
                label='Recently viewed entries'
                scope={'upload'}
                storageKey={upload_id as string}
              />
            </>
          )}
        </Nav>
      }
    >
      <Outlet />
    </Page>
  )
}
