import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {vi} from 'vitest'

import {UploadResponse} from '../../models/graphResponseModels'
import UploadMetadataEditor from './UploadMetadataEditor'

describe('UploadMetadataEditor', () => {
  it('renders with name and id', async () => {
    vi.mock('../../components/routing/useRouteData', () => ({
      default: vi.fn().mockReturnValue({
        upload_id: 'test-id',
        upload_name: 'test-name',
        main_author: {name: 'test-main-author'},
        coauthors: [{name: 'test-coauthor'}],
      } as UploadResponse),
    }))

    render(<UploadMetadataEditor />)

    expect(screen.getByText('test-name')).toBeInTheDocument()
    expect(screen.getByText('test-id')).toBeInTheDocument()
    await userEvent.click(screen.getByRole('button', {name: 'expand'}))
    expect(screen.getByText(/test-main-author/)).toBeInTheDocument()
    expect(screen.getByText(/test-coauthor/)).toBeInTheDocument()
  })
})
