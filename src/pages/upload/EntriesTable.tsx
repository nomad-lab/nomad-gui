import DeleteIcon from '@mui/icons-material/Delete'
import {IconButton, ListItemText, MenuItem} from '@mui/material'
import {useCallback, useMemo} from 'react'

import ProcessStatus from '../../components/ProcessStatus'
import useSelect from '../../components/navigation/useSelect'
import Link from '../../components/routing/Link'
import useRoute from '../../components/routing/useRoute'
import useRouteData from '../../components/routing/useRouteData'
import {ColumnDef} from '../../components/table/ControlledDataTable'
import RoutingTable from '../../components/table/RoutingTable'
import CopyToClipboard from '../../components/values/actions/CopyToClipboard'
import Value from '../../components/values/containers/Value'
import Datetime from '../../components/values/primitives/Datetime'
import Id from '../../components/values/primitives/Id'
import Text from '../../components/values/primitives/Text'
import {EntryResponse} from '../../models/graphResponseModels'
import {assert} from '../../utils/utils'

export default function EntriesTable() {
  const {navigate, url} = useRoute()
  const data = useRouteData<unknown, EntryResponse>()
  const {isPage} = useSelect()

  const tableData = useMemo<EntryResponse[]>(
    () =>
      Object.entries(data)
        .filter(([key]) => key !== 'm_response')
        .map(([, value]) => value) as EntryResponse[],
    [data],
  )

  const columns = useMemo<ColumnDef<EntryResponse>[]>(
    () => [
      {
        accessorKey: 'mainfile_path',
        header: 'Mainfile',
        grow: true,
        Cell: ({cell}) => <Text value={cell.getValue<string>()} fullWidth />,
      },
      {
        accessorKey: 'entry_type',
        header: 'Type',
        enableSorting: false,
        size: 150,
        Cell: ({row}) => (
          <Text
            value={row.original?.metadata?.entry_type as string}
            fullWidth
          />
        ),
      },
      {
        accessorKey: 'entry_id',
        header: 'ID',
        enableSorting: false,
        size: 150,
        Cell: ({cell}) => (
          <Value value={cell.getValue<string>()} fullWidth>
            <Id />
            <CopyToClipboard />
          </Value>
        ),
      },
      {
        accessorKey: 'entry_create_time',
        header: 'Create Time',
        enableSorting: true,
        size: 200,
        Cell: ({cell}) => (
          <Datetime value={cell.getValue<string>()} variant='datetime' />
        ),
      },
      {
        accessorKey: 'process_status',
        header: 'Status',
        size: 120,
        center: true,
        Cell: ({row}) => <ProcessStatus entity={row.original} variant='icon' />,
      },
    ],
    [],
  )

  const rowContextMenuItems = useCallback(
    (row: EntryResponse) => [
      <MenuItem key='open' component={Link} to={url({path: row.entry_id})}>
        <ListItemText>Open entry</ListItemText>
      </MenuItem>,
      <MenuItem
        key='open'
        component={Link}
        to={url({path: ['../files', row.mainfile_path as string]})}
      >
        <ListItemText>Open file</ListItemText>
      </MenuItem>,
    ],
    [url],
  )

  const toolbarSelectActions = useCallback(() => {
    return (
      <>
        <IconButton onClick={() => confirm('Delete')} color='inherit'>
          <DeleteIcon />
        </IconButton>
      </>
    )
  }, [])

  return (
    <RoutingTable
      paginationType='page'
      onRowClick={(data) => {
        assert(
          data.entry_id !== undefined,
          'entry_id for table row must be defined',
        )
        const path = isPage ? data.entry_id : `${data.entry_id}/archive`
        navigate({path})
      }}
      columns={columns}
      data={tableData}
      rowContextMenuItems={rowContextMenuItems}
      toolbarSelectActions={toolbarSelectActions}
    />
  )
}
