import DirectoryIcon from '@mui/icons-material/Folder'
import FileIcon from '@mui/icons-material/InsertDriveFileOutlined'
import {TextField} from '@mui/material'
import {useCallback, useMemo} from 'react'

import TreeNav, {TreeNavProps} from '../../components/navigation/TreeNav'
import {defaultPagination} from '../../components/routing/usePagination'
import useRoute from '../../components/routing/useRoute'
import {
  DirectoryResponse,
  FileResponse,
  PaginationResponse,
} from '../../models/graphResponseModels'
import {JSONObject} from '../../utils/types'

function filesOrderCompareFn(data: JSONObject, a: string, b: string) {
  const dataA = data[a] as JSONObject
  const dataB = data[b] as JSONObject
  if (dataA.m_is === 'Directory' && dataB.m_is === 'File') {
    return -1
  }
  if (dataA.m_is === 'File' && dataB.m_is === 'Directory') {
    return 1
  }
  return a.localeCompare(b)
}

export type UploadFilesNavItemProps = Pick<
  TreeNavProps,
  'getChildProps' | 'label' | 'expanded' | 'selected'
>

export function DirectoryOrFileIcon({type}: {type: 'Directory' | 'File'}) {
  return type === 'Directory' ? <DirectoryIcon /> : <FileIcon />
}

export default function UploadFilesNav({
  getChildProps,
  ...treeNavProps
}: UploadFilesNavItemProps) {
  const {fullMatch, index} = useRoute()
  const highlighted =
    fullMatch.length === index + 2 && fullMatch[index + 1]?.path === 'files'
  const request = useMemo(
    () => ({m_request: {pagination: defaultPagination}, '*': '*'}),
    [],
  )
  const dataIsSufficient = useCallback((data: JSONObject) => {
    const m_response = data.m_response as JSONObject | undefined
    if (!m_response) {
      return false
    }
    const pagination =
      m_response?.pagination as unknown as Required<PaginationResponse>
    return (
      pagination.page === defaultPagination.page &&
      (pagination.total <= pagination.page_size ||
        pagination.page_size >= defaultPagination.page_size)
    )
  }, [])
  return (
    <TreeNav
      highlighted={treeNavProps.selected && highlighted}
      path='files'
      variant='menuItem'
      icon={undefined}
      request={request}
      dataIsSufficient={dataIsSufficient}
      expandable
      getChildProps={(data, key, path) => {
        const child = data[key] as DirectoryResponse | FileResponse
        return {
          variant: 'treeNode',
          icon: child.m_is && <DirectoryOrFileIcon type={child.m_is} />,
          alignWithExpandIcons: true,
          ...(getChildProps?.(data, key, path) || {}),
          expandable: child.m_is === 'Directory',
        }
      }}
      orderCompareFn={filesOrderCompareFn}
      {...treeNavProps}
    >
      <TextField
        sx={{px: 1, '& label.Mui-focused': {display: 'none'}}}
        fullWidth
        size='small'
        label='Go to file'
        InputLabelProps={{shrink: false}}
        variant='outlined'
      />
    </TreeNav>
  )
}
