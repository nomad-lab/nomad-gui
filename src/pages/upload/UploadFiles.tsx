import GoToIcon from '@mui/icons-material/ChevronRight'
import {Box, Button, Paper} from '@mui/material'

import FilePreview from '../../components/fileviewer/FilePreview'
import useSelect from '../../components/navigation/useSelect'
import {PageTitle} from '../../components/page/Page'
import Link from '../../components/routing/Link'
import useRoute from '../../components/routing/useRoute'
import useRouteData from '../../components/routing/useRouteData'
import {
  DirectoryResponse,
  FileResponse,
  MIs1,
  Size1,
} from '../../models/graphResponseModels'
import {ENV} from '../../utils/env'
import {SxProps} from '../../utils/types'
import UploadActions from './UploadActions'
import UploadFilesDownload from './UploadFilesDownload'
import UploadFilesTable from './UploadFilesTable'
import uploadRoute, {filesRoute} from './uploadRoute'

export interface FileResponseWithName extends FileResponse {
  name: string
}

export interface DirectoryResponseWithName {
  name: string
  size: Size1
  m_is?: MIs1
}

export type UploadFilePreviewProps = {
  file?: string
} & SxProps

export function UploadFilePreview({file, sx = {}}: UploadFilePreviewProps) {
  const {fullMatch, index} = useRoute(filesRoute)
  const {upload_id} = useRouteData(uploadRoute)
  const encodedFilePath = [...fullMatch, ...(file ? [{path: file}] : [])]
    .slice(index + 1)
    .map((match) => match.path)
    .map(encodeURIComponent)
    .join('/')

  return (
    <Paper sx={{padding: 2, ...sx}}>
      <FilePreview
        file={`${ENV.API_URL}/uploads/${upload_id}/raw/${encodedFilePath}`}
        size={-1}
      />
    </Paper>
  )
}

export default function UploadFiles() {
  const data = useRouteData<unknown, DirectoryResponse | FileResponse>()
  const {path, url} = useRoute()
  const {upload_id} = useRouteData(uploadRoute)
  const {isPage} = useSelect()

  const actions = (
    <Box sx={{display: 'flex', flexDirection: 'row', gap: 1}}>
      {data.entry && (
        <Button
          variant='contained'
          component={Link}
          to={url({
            path: `/uploads/${upload_id}/entries/${
              (data as FileResponse).entry?.entry_id
            }`,
          })}
          endIcon={<GoToIcon />}
        >
          Go to Entry
        </Button>
      )}
      <UploadFilesDownload />
    </Box>
  )

  if (data.m_is === 'File') {
    return (
      <>
        <PageTitle title={path} actions={isPage && actions} />
        <UploadFilePreview />
      </>
    )
  }
  return (
    <>
      <UploadActions>{actions}</UploadActions>
      <UploadFilesTable data={data} />
    </>
  )
}
