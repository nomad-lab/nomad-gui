import {TextField} from '@mui/material'

import {PageActions} from '../../components/page/Page'
import EntriesTable from './EntriesTable'

export default function Entries() {
  return (
    <>
      <PageActions
        rightActions={
          <TextField
            sx={{width: 400}}
            size='small'
            label='Search or filter entries'
            variant='outlined'
          />
        }
      />
      <EntriesTable />
    </>
  )
}
