import {Box} from '@mui/material'

import {PageTitle} from '../../components/page/Page'
import useRouteData from '../../components/routing/useRouteData'
import UploadActions from './UploadActions'
import {UploadFilePreview} from './UploadFiles'
import UploadFilesDownload from './UploadFilesDownload'
import UploadFilesTable from './UploadFilesTable'
import UploadMetadataEditor from './UploadMetadataEditor'
import {filesRoute} from './uploadRoute'

export default function UploadOverview() {
  const data = useRouteData(filesRoute)
  const hasReadme = Object.keys(data).includes('README.md')

  return (
    <>
      <PageTitle actions={<UploadFilesDownload />} />
      <Box sx={{mb: 4}}>
        <UploadMetadataEditor main editable />
      </Box>
      <UploadActions />
      <UploadFilesTable data={data} navigatePath={(path) => ['files', path]} />
      {hasReadme && <UploadFilePreview file='README.md' sx={{mt: 4}} />}
    </>
  )
}
