import ParentDirectoryIcon from '@mui/icons-material/ArrowUpwardOutlined'
import DownloadIcon from '@mui/icons-material/CloudDownload'
import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import {
  Box,
  Chip,
  Divider,
  IconButton,
  ListItemIcon,
  ListItemText,
  MenuItem,
  Tooltip,
} from '@mui/material'
import {useCallback, useMemo, useState} from 'react'

import useSelect from '../../components/navigation/useSelect'
import Link from '../../components/routing/Link'
import useRoute from '../../components/routing/useRoute'
import useRouteData from '../../components/routing/useRouteData'
import {ColumnDef} from '../../components/table/ControlledDataTable'
import RoutingTable from '../../components/table/RoutingTable'
import {DirectoryResponse, FileResponse} from '../../models/graphResponseModels'
import PlaceholderAction, {
  PlaceholderDialog,
} from '../../utils/PlaceholderAction'
import {DirectoryResponseWithName, FileResponseWithName} from './UploadFiles'
import {DirectoryOrFileIcon} from './UploadFilesNav'
import UploadFilesSelectDialog from './UploadFilesSelectDialog'
import uploadRoute, {pathRoute} from './uploadRoute'

type UploadFilesTableProps = {
  data: DirectoryResponse | FileResponse
  navigatePath?: (path: string) => string | string[]
}

function MoveAction() {
  const [open, setOpen] = useState(false)
  const handleSelect = useCallback(() => setOpen(true), [setOpen])
  const handleClose = useCallback(() => setOpen(false), [setOpen])
  return (
    <>
      <PlaceholderDialog
        open={open}
        onClose={handleClose}
        description={
          <div>
            This will move the file or directory to the selected destination.
            The action will only be performed after confirmation and checking if
            the files can be safely renamed without breaking references.
          </div>
        }
      />
      <UploadFilesSelectDialog
        component={MenuItem}
        title='Move'
        description='Select the directory where you want to move the file(s).'
        selectButtonLabel='Move'
        onSelect={handleSelect}
        key='move'
      >
        <ListItemIcon></ListItemIcon>
        <ListItemText>Move</ListItemText>
      </UploadFilesSelectDialog>
    </>
  )
}

export default function UploadFilesTable({
  data,
  navigatePath = (path) => path,
  ...nomadTableProps
}: UploadFilesTableProps) {
  const {index, fullMatch, url, navigate} = useRoute()
  const {upload_id} = useRouteData(uploadRoute)
  const hasParent = fullMatch[index].route.path === pathRoute.path
  const {isPage, entity, navigateToEntry} = useSelect()

  const tableData = useMemo(() => {
    const tableData = []
    for (const [key, value] of Object.entries(data)) {
      if (typeof value === 'object' && key !== 'm_response') {
        const updatedValue = {...value, ['name']: key} as
          | FileResponseWithName & DirectoryResponseWithName
        tableData.push(updatedValue)
      }
    }
    return tableData
  }, [data])

  const columns = useMemo<
    ColumnDef<FileResponseWithName & DirectoryResponseWithName>[]
  >(
    () => [
      {
        enableSorting: false,
        accessorKey: 'name',
        header: 'Name',
        grow: 1,
        Cell: ({cell}) => (
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              gap: 1,
              flexWrap: 'nowrap',
              '& .MuiSvgIcon-root': {
                color: (theme) => theme.palette.action.active,
              },
            }}
          >
            <DirectoryOrFileIcon
              type={cell.row.original.m_is as unknown as 'Directory' | 'File'}
            />
            {cell.row.original.name}
            {cell.row.original.entry && (
              <Chip color='primary' label='entry' size='small' />
            )}
          </Box>
        ),
      },
      {
        enableSorting: false,
        accessorKey: 'size',
        header: 'Size',
        size: 100,
      },
    ],
    [],
  )

  const rowContextMenuItems = useCallback(
    (row: DirectoryResponseWithName & FileResponseWithName) => [
      <MenuItem
        key='open entry'
        component={Link}
        disabled={!row.entry}
        to={
          row.entry
            ? url({
                path: `/uploads/${upload_id}/entries/${row.entry.entry_id}`,
              })
            : '#'
        }
      >
        <ListItemIcon />
        <ListItemText>Open entry</ListItemText>
      </MenuItem>,
      <MenuItem
        key='open file'
        component={Link}
        to={url({path: navigatePath(row.name)})}
      >
        <ListItemIcon />
        <ListItemText>Open file</ListItemText>
      </MenuItem>,
      <Divider key='devider-1' />,
      <PlaceholderAction
        component={MenuItem}
        key='rename'
        description={
          <div>
            This will open a dialog to rename the file or directory. The action
            will only be performed after confirmation and checking if the files
            can be safely renamed without breaking references.
          </div>
        }
      >
        <ListItemIcon>
          <EditIcon fontSize='small' />
        </ListItemIcon>
        <ListItemText>Rename</ListItemText>
      </PlaceholderAction>,
      <MoveAction key='move' />,
      <PlaceholderAction
        component={MenuItem}
        key='delete'
        description={
          <div>
            This will delete the file or directory and all its contents, after
            confirmation and checking if the files can be safely deleted without
            breaking references.
          </div>
        }
      >
        <ListItemIcon>
          <DeleteIcon fontSize='small' />
        </ListItemIcon>
        <ListItemText>Delete</ListItemText>
      </PlaceholderAction>,
      <Divider key='divider-2' />,
      <PlaceholderAction
        key='download'
        component={MenuItem}
        description={
          <div>
            This will open a browser download dialog to let you download the
            file (or directory as a zip).
          </div>
        }
      >
        <ListItemIcon>
          <DownloadIcon fontSize='small' />
        </ListItemIcon>
        <ListItemText>Download</ListItemText>
      </PlaceholderAction>,
      ...(row.m_is === 'File'
        ? [
            <PlaceholderAction
              key='raw'
              component={MenuItem}
              description='This will open the raw file contents in a new tab.'
            >
              <ListItemIcon></ListItemIcon>
              <ListItemText>View raw</ListItemText>
            </PlaceholderAction>,
          ]
        : []),
    ],
    [url, navigatePath, upload_id],
  )

  const toolbarSelectActions = useCallback(() => {
    return (
      <>
        <IconButton onClick={() => confirm('Delete')} color='inherit'>
          <DownloadIcon />
        </IconButton>
        <IconButton onClick={() => confirm('Delete')} color='inherit'>
          <DeleteIcon />
        </IconButton>
      </>
    )
  }, [])

  const isDisabledRow = useCallback(
    (data: FileResponseWithName & DirectoryResponseWithName) => {
      if (entity === 'directory') {
        return data.m_is === 'File'
      }
      return false
    },
    [entity],
  )

  return (
    <RoutingTable
      paginationType='page'
      onRowClick={(data) => {
        if (data.entry && navigateToEntry) {
          const path = isPage
            ? `/uploads/${upload_id}/entries/${data.entry.entry_id}`
            : `/uploads/${upload_id}/entries/${data.entry.entry_id}/archive`

          navigate({path})
        } else if (typeof data.name === 'string') {
          navigate({path: navigatePath(data.name)})
        }
      }}
      rowContextMenuItems={isPage ? rowContextMenuItems : undefined}
      toolbarCustomActions={() =>
        hasParent && (
          <Tooltip title='Go to parent directory'>
            <IconButton onClick={() => navigate({path: navigatePath('..')})}>
              <ParentDirectoryIcon />
            </IconButton>
          </Tooltip>
        )
      }
      toolbarSelectActions={isPage ? toolbarSelectActions : undefined}
      isDisabledRow={isDisabledRow}
      {...nomadTableProps}
      columns={columns}
      data={tableData}
    />
  )
}
