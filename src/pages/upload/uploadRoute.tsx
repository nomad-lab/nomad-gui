import {Typography} from '@mui/material'

import {components} from '../../components/markdown/components'
import {Route} from '../../components/routing/types'
import {
  createPaginationRequest,
  defaultPagination,
  validatePaginationSearch,
} from '../../components/routing/usePagination'
import {
  DirectoryRequest,
  EntriesRequest,
  UploadRequest,
} from '../../models/graphRequestModels'
import {
  DirectoryResponse,
  EntriesResponse,
  UploadResponse,
} from '../../models/graphResponseModels'
import {PageBasedPagination} from '../../utils/types'
import entryRoute from '../entry/entryRoute'
import UploadSettings from './UploadSettings.mdx'

export const pathRoute: Route<
  DirectoryRequest,
  DirectoryResponse,
  Required<PageBasedPagination>
> = {
  path: ':name',
  request: ({search, isLeaf}) => ({
    m_request: {
      directive: isLeaf ? 'resolved' : 'plain',
      pagination: isLeaf ? createPaginationRequest(search) : defaultPagination,
      depth: 1,
    },
    '*': '*',
  }),
  onlyRender: '',
  lazyComponent: async () => import('./UploadFiles'),
  validateSearch: validatePaginationSearch,
}
pathRoute.children = [pathRoute]

export const filesRoute: Route<
  DirectoryRequest,
  DirectoryResponse,
  Required<PageBasedPagination>
> = {
  ...pathRoute,
  path: 'files',
  breadcrumb: <b>files</b>,
}

const entriesRoute: Route<
  EntriesRequest,
  EntriesResponse,
  Required<PageBasedPagination>
> = {
  path: 'entries',
  breadcrumb: <b>entries</b>,
  request: ({search}) => ({
    m_request: {
      pagination: createPaginationRequest(search),
    },
    '*': {
      entry_id: '*',
      mainfile_path: '*',
      entry_create_time: '*',
      complete_time: '*',
      process_status: '*',
      metadata: {
        entry_type: '*',
      },
    },
  }),
  lazyComponent: async () => import('./Entries'),
  validateSearch: validatePaginationSearch,
  onlyRender: '',
  children: [entryRoute],
}

const uploadRoute: Route<UploadRequest, UploadResponse> = {
  path: ':uploadId',
  request: {
    upload_id: '*',
    upload_name: '*',
    process_status: '*',
    published: '*',
    with_embargo: '*',
    upload_create_time: '*',
    complete_time: '*',
    main_author: {name: '*'},
    coauthors: '*',
  },
  lazyComponent: async () => import('./UploadPage'),
  breadcrumb: ({response}) =>
    response?.upload_name ? (
      <Typography sx={{maxWidth: 180, textOverflow: 'ellipsis'}} noWrap>
        {response?.upload_name}
      </Typography>
    ) : (
      <Typography component='i'>unnamed project</Typography>
    ),
  onlyRender: ['', 'files/*', 'entries', 'settings'],
  children: [
    {
      ...pathRoute,
      path: '',
      breadcrumb: <b>overview</b>,
      lazyComponent: async () => import('./UploadOverview'),
      requestKey: 'files',
    },
    filesRoute,
    entriesRoute,
    {
      path: 'settings',
      breadcrumb: <b>settings</b>,
      component: () => <UploadSettings components={components} />,
    },
  ],
}

export default uploadRoute
