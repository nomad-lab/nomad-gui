import {Box, TextField} from '@mui/material'
import {PropsWithChildren, useCallback, useState} from 'react'

import SplitButton from '../../components/SplitButton'
import useSelect from '../../components/navigation/useSelect'
import {PageActions} from '../../components/page/Page'
import {PlaceholderDialog} from '../../utils/PlaceholderAction'
import {SxProps} from '../../utils/types'

export default function UploadActions({
  sx,
  children,
}: SxProps & PropsWithChildren) {
  const {isPage} = useSelect()
  const [placeholderDescription, setPlaceholderDescription] = useState<
    string | null
  >()

  const handleClickCreateEntry = useCallback(() => {
    setPlaceholderDescription(`
      This will provide several options to create a new entry. It starts
      with a dialog to choose basic metadata, like a file name. By default
      it will create a very basic ELN entry from a simple generic schema
      (title, description, authors, tags, timestamp, etc.). Or, users can
      choose to create an entry from a plugin schema, uploaded schema, or
      existing entry (template).`)
  }, [setPlaceholderDescription])

  const handleClickUploadFiles = useCallback(() => {
    setPlaceholderDescription(`
      This will either open the browser file upload or explain how to
      upload and create files through the API.`)
  }, [setPlaceholderDescription])

  const handleClickLaunch = useCallback(() => {
    setPlaceholderDescription(`
      This opens the current directory in the selected tool. By default
      this will be the basic JupyterLab.`)
  }, [setPlaceholderDescription])

  const handleClosePlaceholderDialog = useCallback(() => {
    setPlaceholderDescription(null)
  }, [setPlaceholderDescription])

  return (
    <PageActions
      sx={sx}
      leftActions={
        isPage && (
          <>
            <SplitButton
              variant='contained'
              color='primary'
              buttons={[
                {children: 'Create entry', onClick: handleClickCreateEntry},
                {
                  children: 'Create entry from template',
                  onClick: handleClickCreateEntry,
                },
                {
                  children: 'Create entry from schema',
                  onClick: handleClickCreateEntry,
                },
              ]}
            />
            <SplitButton
              variant={'contained' as unknown as 'contained'}
              color={'primary' as unknown as 'primary'}
              buttons={[
                {children: 'Upload files', onClick: handleClickUploadFiles},
                {children: 'Upload via API', onClick: handleClickUploadFiles},
                {
                  children: 'Create empty text file',
                  onClick: handleClickUploadFiles,
                },
              ]}
            />
            <SplitButton
              variant={'outlined' as unknown as 'outlined'}
              color={'primary' as unknown as 'primary'}
              buttons={[
                {children: 'Open in Jupyterlab', onClick: handleClickLaunch},
                {children: 'Open in VS Code', onClick: handleClickLaunch},
                {children: 'Open in another tool', disabled: true},
              ]}
            />
          </>
        )
      }
      rightActions={
        <>
          <Box sx={{flexGrow: 1}} />
          <TextField
            sx={{width: 400}}
            size='small'
            label='Go to file'
            variant='outlined'
          />
          {isPage && (
            <>
              {children}
              {placeholderDescription && (
                <PlaceholderDialog
                  description={placeholderDescription}
                  open
                  onClose={handleClosePlaceholderDialog}
                />
              )}
            </>
          )}
        </>
      }
    />
  )
}
