import {Download as DownloadIcon} from '@mui/icons-material'
import {Button} from '@mui/material'

import PlaceholderAction from '../../utils/PlaceholderAction'

export default function UploadFilesDownload() {
  return (
    <PlaceholderAction
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      component={Button}
      description={
        <div>
          This will open a browser download dialog to let you download the file
          (or directory as a zip).
        </div>
      }
      startIcon={<DownloadIcon />}
      variant='contained'
    >
      Download
    </PlaceholderAction>
  )
}
