import {useMemo} from 'react'

import ProcessStatus from '../../components/ProcessStatus'
import Visibility from '../../components/Visibility'
import {Layout, LayoutItem} from '../../components/layout/Layout'
import useRouteData from '../../components/routing/useRouteData'
import List from '../../components/values/containers/List'
import Text from '../../components/values/primitives/Text'
import {UploadResponse} from '../../models/graphResponseModels'
import {assert} from '../../utils/utils'
import uploadRoute from './uploadRoute'

function AuthorList({data}: {data: UploadResponse}) {
  assert(data.main_author !== undefined, 'upload main_author is required')
  assert(data.coauthors !== undefined, 'upload coauthors are required')
  const authors = [data.main_author, ...data.coauthors]
  // TODO: It might be better to use a special comma separated list layout
  // instead of a custom component. Especially, if this should be editable
  // as some point.
  return (
    <List
      variant='inline'
      label='authors'
      value={authors.map((author) => author.name)}
      renderValue={() => <Text />}
    />
  )
}

export type UploadMetadataEditorProps = {
  expandedByDefault?: boolean
  editable?: boolean
  /**
   * If true, the editor will be displayed as the main metadata editor
   * for the page and it will be differently styled and not collapsed
   * by default.
   */
  main?: boolean
}

export default function UploadMetadataEditor({
  expandedByDefault = false,
  editable = false,
  main = false,
}: UploadMetadataEditorProps) {
  expandedByDefault = main ? true : expandedByDefault
  const data = useRouteData(uploadRoute) as UploadResponse

  const layout = useMemo(
    () =>
      ({
        type: 'card',
        variant: 'highlighted',
        expandedByDefault: expandedByDefault,
        collapsedChildren: [
          {
            type: 'container',
            variant: 'flex',
            sx: {
              flexWrap: 'wrap',
              alignItems: 'center',
            },
            children: [
              {
                grow: true,
                type: 'value',
                label: 'project name',
                editable: editable,
                value: data.upload_name,
                component: {
                  Text: {
                    variant: main ? 'h1' : 'standard',
                    labelActions: (
                      <>
                        <ProcessStatus entity={data} />
                        <Visibility upload={data} />
                      </>
                    ),
                  },
                },
              },
              {
                xs: 5,
                type: 'value',
                component: {
                  Id: {
                    abbreviated: true,
                    labelActions: 'CopyToClipboard',
                  },
                },
                label: 'project id',
                value: data.upload_id,
              },
            ],
          },
        ],
        children: [
          {
            xs: 12,
            type: 'container',
            variant: 'flex',
            children: [
              {
                grow: true,
                type: 'element',
                element: <AuthorList data={data} />,
              },
              {
                type: 'value',
                label: 'created at',
                component: {
                  Datetime: {
                    variant: 'datetime',
                  },
                },
                value: data.upload_create_time,
              },
              {
                type: 'value',
                label: 'last change',
                component: {
                  Datetime: {
                    variant: 'datetime',
                  },
                },
                value: data.complete_time,
              },
            ],
          },
        ],
      } as LayoutItem),
    [editable, expandedByDefault, main, data],
  )

  return <Layout layout={layout} />
}
