import ProcessStatus from '../../components/ProcessStatus'
import Visibility from '../../components/Visibility'
import NavAbout from '../../components/navigation/NavAbout'
import {routesEqual} from '../../components/routing/normalizeRoute'
import useRoute from '../../components/routing/useRoute'
import useRouteData from '../../components/routing/useRouteData'
import CopyToClipboard from '../../components/values/actions/CopyToClipboard'
import Value from '../../components/values/containers/Value'
import Id from '../../components/values/primitives/Id'
import entryRoute from '../entry/entryRoute'
import uploadRoute from './uploadRoute'

export default function UploadNavAbout() {
  const upload = useRouteData(uploadRoute)
  const {upload_name, upload_id} = upload
  const {fullMatch, index, url} = useRoute(uploadRoute)
  const isEntryRoute = routesEqual(
    entryRoute,
    fullMatch[index + 2]?.route as typeof entryRoute,
  )
  return (
    <NavAbout
      label='project'
      title={upload_name as string}
      overviewUrl={isEntryRoute ? url({}) : undefined}
      selectUrl={isEntryRoute ? url({path: 'files'}) : undefined}
    >
      <Value value={upload_id}>
        <Id abbreviated />
        <CopyToClipboard />
      </Value>
      <div style={{flexGrow: 1}} />
      <Visibility upload={upload} variant='icon' />
      <ProcessStatus entity={upload} variant='icon' />
    </NavAbout>
  )
}
