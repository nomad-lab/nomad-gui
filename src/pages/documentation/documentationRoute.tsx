import {Route} from '../../components/routing/types'
import {UploadRequest} from '../../models/graphRequestModels'
import {UploadResponse} from '../../models/graphResponseModels'

const documentationRoute: Route<UploadRequest, UploadResponse> = {
  path: 'docs',
  children: [
    {
      path: 'layout',
      lazyComponent: async () => import('./LayoutDocumentation'),
    },
  ],
}

export default documentationRoute
