import {Typography} from '@mui/material'
import Grid from '@mui/material/Grid2/Grid2'
import {useMemo, useState} from 'react'

import {LayoutItem} from '../../components/layout/Layout'
import Page, {PageTitle} from '../../components/page/Page'
import EditToggle from '../../components/values/actions/EditToggle'
import Value from '../../components/values/containers/Value'
import JSONData from '../../components/values/primitives/JsonData'
import EntryDataEditor from '../entry/EntryDataEditor'

export default function LayoutDocumentation() {
  const defaultData = useMemo(
    () => ({
      voltage: 120,
    }),
    [],
  )
  const [data, setData] = useState(defaultData)

  const defaultLayout = useMemo(
    () =>
      ({
        type: 'quantity',
        property: 'voltage',
        label: 'Voltage',
        editable: true,
        unit: 'V',
        component: {
          Numeral: {
            actions: {
              Help: {
                content:
                  '**[Voltage](https://en.wikipedia.org/wiki/Voltage)**, also known as **(electrical) potential difference**, **electric pressure**, or **electric tension** is the difference in electric potential between two points.',
              },
              CopyToClipboard: {},
            },
          },
        },
      } as LayoutItem),
    [],
  )
  const [layout, setLayout] = useState(defaultLayout)

  return (
    <Page>
      <PageTitle>Layout Documentation</PageTitle>
      <Grid container spacing={2}>
        <Grid size={{xs: 4}}>
          <Typography variant='h3'>Final appearance</Typography>
        </Grid>
        <Grid size={{xs: 4}}>
          <Typography variant='h3'>Layout specification</Typography>
        </Grid>
        <Grid size={{xs: 4}}>
          <Typography variant='h3'>Data</Typography>
        </Grid>
        <Grid size={{xs: 4}}>
          <EntryDataEditor layout={layout} />
        </Grid>
        <Grid size={{xs: 4}}>
          <Value
            editable
            edit={false}
            fullWidth
            helperText='press shift+enter'
            value={layout}
            onChange={setLayout}
            actions={<EditToggle />}
          >
            <JSONData />
          </Value>
        </Grid>
        <Grid size={{xs: 4}}>
          <Value
            editable
            edit={false}
            fullWidth
            helperText='press shift+enter'
            value={data}
            onChange={setData}
            actions={<EditToggle />}
          >
            <JSONData />
          </Value>
        </Grid>
      </Grid>
    </Page>
  )
}
