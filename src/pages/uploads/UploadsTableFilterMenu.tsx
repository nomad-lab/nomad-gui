import {Checkbox, FormControlLabel, FormGroup} from '@mui/material'

import {PageActions} from '../../components/page/Page'
import useSearchParameter from '../../components/routing/useSearchParameter'

export default function UploadsTableFilterMenu() {
  const [owned, setOwned] = useSearchParameter<boolean>('owned', true)
  const [shared, setShared] = useSearchParameter<boolean>('shared', true)
  const [editableOnly, setEditableOnly] = useSearchParameter<boolean>(
    'editable_only',
    false,
  )
  const [processing, setProcessing] = useSearchParameter<boolean>(
    'processing',
    false,
  )

  return (
    <PageActions>
      <FormGroup row>
        <FormControlLabel
          control={
            <Checkbox
              checked={owned}
              onChange={(event) => setOwned(event.target.checked)}
            />
          }
          label='created by you'
        />
        <FormControlLabel
          sx={{mr: 4}}
          control={
            <Checkbox
              checked={shared}
              onChange={(event) => setShared(event.target.checked)}
            />
          }
          label='shared with you'
        />
        <FormControlLabel
          control={
            <Checkbox
              checked={editableOnly}
              onChange={(event) => setEditableOnly(event.target.checked)}
            />
          }
          label='only editable'
        />
        <FormControlLabel
          control={
            <Checkbox
              checked={processing}
              onChange={(event) => setProcessing(event.target.checked)}
            />
          }
          label='only failed or processing'
        />
      </FormGroup>
    </PageActions>
  )
}
