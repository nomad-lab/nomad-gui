import {Route} from '../../components/routing/types'
import {
  createPaginationRequest,
  validatePaginationSearch,
} from '../../components/routing/usePagination'
import {UploadsRequest} from '../../models/graphRequestModels'
import {UploadsResponse} from '../../models/graphResponseModels'
import {PageBasedPagination} from '../../utils/types'
import uploadRoute from '../upload/uploadRoute'

type Search = {
  owned: boolean
  shared: boolean
  editable_only: boolean
  processing: boolean
}

const uploadsRoute: Route<
  UploadsRequest,
  UploadsResponse,
  Search & Required<PageBasedPagination>
> = {
  path: 'uploads',
  lazyComponent: async () => import('./UploadsPage'),
  onlyRender: '',
  breadcrumb: <b>projects</b>,
  validateSearch: ({rawSearch, ...params}) => ({
    ...validatePaginationSearch({rawSearch, ...params}),
    owned: rawSearch.owned === undefined ? true : rawSearch.owned === 'true',
    shared: rawSearch.shared === undefined ? true : rawSearch.owned === 'true',
    editable_only: rawSearch.editable_only === 'true',
    processing: rawSearch.processing === 'true',
  }),
  request: ({search, isLeaf}) =>
    isLeaf
      ? {
          m_request: {
            pagination: createPaginationRequest(search),
            query: {
              ...(search.editable_only && {published: false}),
              ...(search.processing && {process_status: 'FAILED'}),
            },
          },
          '*': {
            upload_name: '*',
            upload_id: '*',
            process_status: '*',
            published: '*',
            upload_create_time: '*',
          },
        }
      : ({} as UploadsRequest),
  children: [uploadRoute],
}

export default uploadsRoute
