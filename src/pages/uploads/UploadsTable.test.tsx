import {act, fireEvent, render, screen} from '@testing-library/react'
import {describe, expect, it, vi} from 'vitest'

import UploadsTable from './UploadsTable'

const setPaginate = vi.fn()
const navigate = vi.fn()

vi.mock('../../components/routing/usePagination', () => ({
  validatePaginationSearch: () => {},
  default: () => [
    {
      page: 1,
      per_page: 10,
      page_size: 50,
      total: 100,
    },
    setPaginate,
  ],
}))

vi.mock('../../components/routing/useRoute', () => ({
  default: () => ({
    navigate,
    search: {},
    url: () => '/',
    response: Array.from({length: 12}, (_, index) => ({
      upload_id: `${index + 1}`,
      upload_name: `Upload ${index + 1}`,
    })),
  }),
}))

afterEach(() => {
  vi.restoreAllMocks()
})

describe('UploadsTable Component', () => {
  it('should render the table with data', () => {
    render(<UploadsTable />)

    const table = screen.getByRole('table')
    expect(table).toBeInTheDocument()
  })
  it('go to next page should trigger navigation', () => {
    render(<UploadsTable />)

    const table = screen.getByRole('table')
    expect(table).toBeInTheDocument()

    const nextPageButton = screen.getByText('Load More')
    act(() => fireEvent.click(nextPageButton))
    expect(setPaginate).toHaveBeenCalledWith({page_size: 100})
  })
  it('sorting change should trigger navigation', () => {
    render(<UploadsTable />)

    const table = screen.getByRole('table')
    expect(table).toBeInTheDocument()

    const elements = screen.getAllByRole('button', {
      name: 'Sort by Name ascending',
    })

    fireEvent.click(elements[0])
    expect(setPaginate).toHaveBeenCalledWith({
      order: 'asc',
      order_by: 'upload_name',
    })
  })
})
