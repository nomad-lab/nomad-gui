import {Divider, TextField} from '@mui/material'

import Nav from '../../components/navigation/Nav'
import NavItem from '../../components/navigation/NavItem'
import RecentNav from '../../components/navigation/RecentNav'
import useSelect from '../../components/navigation/useSelect'
import Page, {PageActions, PageTitle} from '../../components/page/Page'
import UploadsActions from './UploadsActions'
import UploadsTable from './UploadsTable'
import UploadsTableFilterMenu from './UploadsTableFilterMenu'

export default function UploadsPage() {
  const {isPage, pageVariant} = useSelect()

  return (
    <Page
      variant={pageVariant}
      navigation={
        <Nav>
          <NavItem label='projects' selected />
          <Divider />
          <RecentNav
            label='Recently viewed projects'
            scope='global'
            storageKey='uploads'
          />
        </Nav>
      }
    >
      {isPage && (
        <PageTitle
          title='Projects'
          subtitle='You can manage your projects here. A project is a container for your files and data. Projects can be shared with other users.'
        />
      )}
      {isPage && (
        <PageActions
          leftActions={<UploadsActions />}
          rightActions={
            <TextField
              sx={{width: 400}}
              size='small'
              label='Search or filter projects'
              variant='outlined'
            />
          }
        />
      )}
      <UploadsTableFilterMenu />
      <UploadsTable />
    </Page>
  )
}
