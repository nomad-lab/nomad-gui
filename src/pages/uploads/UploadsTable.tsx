import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import CopyIcon from '@mui/icons-material/FileCopy'
import {Divider, ListItemIcon, ListItemText, MenuItem} from '@mui/material'
import {useCallback, useMemo} from 'react'

import ProcessStatus from '../../components/ProcessStatus'
import Visibility from '../../components/Visibility'
import useSelect from '../../components/navigation/useSelect'
import Link from '../../components/routing/Link'
import useRoute from '../../components/routing/useRoute'
import useRouteData from '../../components/routing/useRouteData'
import {ColumnDef} from '../../components/table/ControlledDataTable'
import RoutingTable from '../../components/table/RoutingTable'
import CopyToClipboard from '../../components/values/actions/CopyToClipboard'
import Value from '../../components/values/containers/Value'
import Datetime from '../../components/values/primitives/Datetime'
import Id from '../../components/values/primitives/Id'
import Text from '../../components/values/primitives/Text'
import {UploadResponse} from '../../models/graphResponseModels'
import PlaceholderAction from '../../utils/PlaceholderAction'
import UploadsTableToolbarSelectActions from './UploadsTableSelectToolbarActions'
import UploadsTableToolbarActions from './UploadsTableToolbarActions'
import uploadsRoute from './uploadsRoute'

export default function UploadsTable() {
  const {navigate, url} = useRoute()
  const data = useRouteData(uploadsRoute)
  const {isPage} = useSelect()

  const tableData = useMemo(
    () =>
      Object.keys(data)
        .filter((key) => key !== 'm_response')
        .map((key) => data[key] as UploadResponse),
    [data],
  )

  const columns = useMemo<ColumnDef<UploadResponse>[]>(
    () => [
      {
        accessorKey: 'upload_name',
        header: 'Name',
        grow: true,
        Cell: ({cell}) => <Text value={cell.getValue<string>()} fullWidth />,
      },
      {
        accessorKey: 'upload_id',
        header: 'ID',
        enableSorting: false,
        size: 150,
        Cell: ({cell}) => (
          <Value value={cell.getValue<string>()} fullWidth>
            <Id abbreviated />
            <CopyToClipboard />
          </Value>
        ),
      },
      {
        accessorKey: 'upload_create_time',
        header: 'Create Time',
        enableSorting: true,
        size: 190,
        Cell: ({cell}) => (
          <Datetime value={cell.getValue<string>()} variant='datetime' />
        ),
      },

      {
        accessorKey: 'published',
        header: 'Visibility',
        enableSorting: false,
        size: 100,
        center: true,
        Cell: ({row}) => <Visibility upload={row.original} variant='icon' />,
      },
      {
        accessorKey: 'process_status',
        header: 'Status',
        size: 150,
        center: true,
        Cell: ({row}) => <ProcessStatus entity={row.original} variant='icon' />,
      },
    ],
    [],
  )

  const rowContextMenuItems = useCallback(
    (row: UploadResponse) => [
      <MenuItem key='open' component={Link} to={url({path: row.upload_id})}>
        <ListItemIcon />
        <ListItemText>Open</ListItemText>
      </MenuItem>,
      <Divider key='divider' />,
      <PlaceholderAction
        component={MenuItem}
        description={
          <div>
            This will open a dialog and allow to rename a project. It will also
            check if the project can be safely renamed without breaking any
            references.
          </div>
        }
        key='edit'
      >
        <ListItemIcon>
          <EditIcon fontSize='small' />
        </ListItemIcon>
        <ListItemText>Rename</ListItemText>
      </PlaceholderAction>,
      <PlaceholderAction
        component={MenuItem}
        description={
          <div>
            This will open a dialog and allow to copy the project under a new
            title.
          </div>
        }
        key='edit'
      >
        <ListItemIcon>
          <CopyIcon fontSize='small' />
        </ListItemIcon>
        <ListItemText>Copy</ListItemText>
      </PlaceholderAction>,
      <PlaceholderAction
        component={MenuItem}
        description={
          <div>
            This will open a confirm dialog and allow to delete a project. It
            will also check if the project can be safely renamed without
            breaking any references.
          </div>
        }
        key='delete'
      >
        <ListItemIcon>
          <DeleteIcon fontSize='small' />
        </ListItemIcon>
        <ListItemText>Delete</ListItemText>
      </PlaceholderAction>,
    ],
    [url],
  )

  return (
    <RoutingTable
      paginationType='page'
      entityLabel='project'
      getRowId={(data) => data.upload_id as string}
      onRowClick={(data) => {
        if (typeof data.upload_id === 'string') {
          if (isPage) {
            navigate({path: data.upload_id})
          } else {
            navigate({path: `${data.upload_id}/files`})
          }
        }
      }}
      columns={columns}
      data={tableData}
      rowContextMenuItems={isPage ? rowContextMenuItems : undefined}
      toolbarActions={(table) => {
        return <UploadsTableToolbarActions table={table} />
      }}
      toolbarSelectActions={
        isPage
          ? (table) => {
              return <UploadsTableToolbarSelectActions table={table} />
            }
          : undefined
      }
    />
  )
}
