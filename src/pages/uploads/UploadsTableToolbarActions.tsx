import CodeIcon from '@mui/icons-material/Code'
import ReplayIcon from '@mui/icons-material/Replay'
import {IconButton, Tooltip} from '@mui/material'
import {MRT_TableInstance} from 'material-react-table'

import {UploadResponse} from '../../models/graphResponseModels'

interface UploadsTableToolbarActionsProps {
  table: MRT_TableInstance<UploadResponse>
}

export default function UploadsTableToolbarActions({
  table,
}: UploadsTableToolbarActionsProps) {
  const handleApiModal = () => {
    const numRows = table.getSelectedRowModel().rows.length
    confirm(`Num rows: ${numRows}: API model`)
  }
  const handleReload = () => {
    confirm('Reload')
  }

  return (
    <>
      <Tooltip arrow title='API'>
        <IconButton onClick={handleApiModal}>
          <CodeIcon />
        </IconButton>
      </Tooltip>
      <Tooltip arrow title='Reload the projects'>
        <IconButton onClick={handleReload}>
          <ReplayIcon />
        </IconButton>
      </Tooltip>
    </>
  )
}
