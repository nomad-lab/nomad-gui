import {Box, Button} from '@mui/material'

import PlaceholderAction from '../../utils/PlaceholderAction'
import {SxProps} from '../../utils/types'

export default function UploadsActions({sx}: SxProps) {
  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'flex-start',
        flexWrap: 'wrap',
        gap: 1,
        ...sx,
      }}
    >
      <PlaceholderAction
        component={Button}
        variant={'contained' as unknown as 'contained'}
        color={'primary' as unknown as 'primary'}
        description={
          <div>
            This will open a dialog and ask for some key metadata, e.g. like the
            project name, before creating the project.
          </div>
        }
      >
        Create New Project
      </PlaceholderAction>
      <PlaceholderAction
        component={Button}
        variant={'outlined' as unknown as 'outlined'}
        description={
          <div>
            This will open a dialog and ask for some key metadata, e.g. like the
            project name. It will also offer a selection of preconfigured
            example projects to choose from.
          </div>
        }
      >
        Add Example Project
      </PlaceholderAction>
    </Box>
  )
}
