import DeleteIcon from '@mui/icons-material/Delete'
import ShareIcon from '@mui/icons-material/Share'
import {IconButton, Tooltip} from '@mui/material'
import {MRT_TableInstance} from 'material-react-table'

import {UploadResponse} from '../../models/graphResponseModels'

interface UploadsTableToolbarSelectActionsProps {
  table: MRT_TableInstance<UploadResponse>
}

export default function UploadsTableToolbarSelectActions({
  table,
}: UploadsTableToolbarSelectActionsProps) {
  const handleDeleteProjects = () => {
    const numSelected = table.getSelectedRowModel.length
    confirm(`Are you sure you want to delete ${numSelected} projects?`)
  }
  const handleShareProjects = () => {
    confirm('Are you sure you want to share the selected projects?')
  }

  return (
    <>
      <Tooltip arrow title='Delete project'>
        <IconButton onClick={handleDeleteProjects} color='inherit'>
          <DeleteIcon />
        </IconButton>
      </Tooltip>
      <Tooltip arrow title='Share project'>
        <IconButton onClick={handleShareProjects} color='inherit'>
          <ShareIcon />
        </IconButton>
      </Tooltip>
    </>
  )
}
