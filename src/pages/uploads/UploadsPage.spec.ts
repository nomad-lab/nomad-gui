import {APIRequestContext, expect, test} from '@playwright/test'

import {afterFn, beforeFn} from '../../utils/e2e.helper'

let apiContext: APIRequestContext
const testUploadsData = [
  {upload_name: 'Upload One'},
  {upload_name: 'Upload Two'},
  {upload_name: 'Upload Three'},
]

test.beforeEach(async ({playwright, page, baseURL}) => {
  // eslint-disable-next-line
  page.on('console', (m) => console.log(m.text()))
  apiContext = await beforeFn({
    playwright,
    uploads: testUploadsData.map((data) => ({data: data})),
  })
  if (baseURL) await page.goto(`${baseURL}/uploads`)
})

test.afterEach(async () => {
  afterFn(apiContext)
})

test('Uploads page interactions', async ({page}) => {
  await expect(page.getByText('Projects', {exact: true})).toBeVisible()
  await expect(page.getByText('3 of 3')).toBeVisible()
  await page.getByLabel('Reload the projects').first().click()
  await page.getByLabel('API').first().click()
  await page.getByLabel('Show/Hide columns').first().click()
  await page.locator('.MuiBackdrop-root').click()
  await page.getByLabel('Toggle select all').nth(1).check()
  await expect(page.getByText('3 of 3 project(s) selected')).toBeVisible()
  await page.getByLabel('Toggle select all').nth(1).uncheck()
  await expect(page.getByText('project(s) selected')).toBeHidden()
})
