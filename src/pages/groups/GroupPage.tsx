import Page from '../../components/page/Page'
import GroupEditor from './GroupEditor'

export default function GroupPage() {
  return (
    <Page>
      <GroupEditor />
    </Page>
  )
}
