import {useMemo} from 'react'
import {useAuth} from 'react-oidc-context'

import useRoute from '../../components/routing/useRoute'
import useRouteData from '../../components/routing/useRouteData'
import {ColumnDef} from '../../components/table/ControlledDataTable'
import RoutingTable from '../../components/table/RoutingTable'
import Id from '../../components/values/primitives/Id'
import Text from '../../components/values/primitives/Text'
import {GroupResponse} from '../../models/graphResponseModels'
import groupsRoute from './groupsRoute'

function getUserRole(group: GroupResponse, userId: string | null | undefined) {
  if (group?.owner?.user_id === userId) {
    return 'Owner'
  } else if (group?.members?.some((member) => member.user_id === userId)) {
    return 'Member'
  } else {
    return null
  }
}

export default function GroupsTable() {
  const {user} = useAuth()
  const {navigate} = useRoute()
  const data = useRouteData(groupsRoute)

  const tableData = useMemo(
    () =>
      Object.keys(data)
        .filter((key) => key !== 'm_response')
        .map((key) => data[key] as GroupResponse),
    [data],
  )

  const columns = useMemo<ColumnDef<GroupResponse>[]>(
    () => [
      {
        accessorKey: 'group_name',
        header: 'Name',
        Cell: ({cell}) => <Text value={cell.getValue<string>()} fullWidth />,
        grow: true,
      },
      {
        accessorKey: 'owner.name',
        header: 'Owner',
        Cell: ({cell}) => <Text value={cell.getValue<string>()} />,
        grow: true,
      },
      {
        header: 'Role',
        accessorFn: (data) => getUserRole(data, user?.profile.sub),
        Cell: ({cell}) => <Text value={cell.getValue<string>()} />,
        grow: true,
      },
      {
        id: 'members',
        accessorFn: (data) => data?.members?.length || '0',
        header: 'Members',
        Cell: ({cell}) => <Text value={cell.getValue<string>()} />,
        center: true,
        grow: true,
        foo: 'bar',
      },
      {
        accessorKey: 'group_id',
        header: 'ID',
        Cell: ({cell}) => <Id value={cell.getValue<string>()} abbreviated />,
        grow: true,
      },
    ],
    [user?.profile.sub],
  )

  return (
    <RoutingTable
      paginationType='page'
      entityLabel='group'
      getRowId={(data) => data.group_id as string}
      onRowClick={(data) => {
        navigate({path: data.group_id})
      }}
      columns={columns}
      data={tableData}
    />
  )
}
