import {useMemo} from 'react'

import {ColumnDef} from '../../components/table/ControlledDataTable'
import PlainDataTable from '../../components/table/PlainDataTable'
import Id from '../../components/values/primitives/Id'
import {GroupResponse} from '../../models/graphResponseModels'

enum Role {
  Owner = 'Owner',
  Member = 'Member',
}

type MemberData = {
  name: string | undefined
  userId: string | undefined
  affiliation: string | undefined
  role: Role
}

export function GroupMembersTable({data}: {data: GroupResponse}) {
  const membersData = useMemo(
    () =>
      data.members?.map((member) => ({
        name: member.name,
        userId: member.user_id,
        affiliation: member.affiliation,
        role: member.user_id === data.owner?.user_id ? Role.Owner : Role.Member,
      })) || [],
    [data],
  )

  const columns = useMemo<ColumnDef<MemberData>[]>(
    () => [
      {
        accessorKey: 'name',
        header: 'Name',
        grow: 1,
      },
      {
        accessorKey: 'userId',
        header: 'User ID',
        Cell: ({cell}) => <Id value={cell.getValue<string>()} abbreviated />,
        grow: 1,
      },
      {
        accessorKey: 'affiliation',
        header: 'Affiliation',
        grow: 1,
      },
      {
        accessorKey: 'role',
        header: 'Role',
      },
    ],
    [],
  )

  return <PlainDataTable columns={columns} data={membersData} />
}
