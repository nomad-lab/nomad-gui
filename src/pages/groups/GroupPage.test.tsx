import {screen, waitFor, within} from '@testing-library/react'
import {expect, it, vi} from 'vitest'

import {GraphResponse} from '../../models/graphResponseModels'
import * as api from '../../utils/api'
import {
  checkRow,
  importLazyComponents,
  renderWithRouteData,
} from '../../utils/test.helper'
import groupRoute from './groupRoute'

await importLazyComponents(groupRoute)

describe('GroupPage', () => {
  it('loads and initially renders group editor', async () => {
    const mockedApi = vi.spyOn(api, 'graphApi')
    window.history.replaceState(null, '', '/group1')
    mockedApi.mockResolvedValue({
      group1: {
        group_name: 'Group 1',
        group_id: 'group1',
        owner: {user_id: 'user0', name: 'User 0', affiliation: 'Uni 0'},
        members: [
          {user_id: 'user0', name: 'User 0', affiliation: 'Uni 0'},
          {user_id: 'user1', name: 'User 1', affiliation: 'Uni 1'},
          {user_id: 'user2', name: 'User 2', affiliation: 'Uni 2'},
        ],
      },
    } as GraphResponse)
    await renderWithRouteData(groupRoute)
    await waitFor(() =>
      expect(screen.getByLabelText('group name')).toHaveTextContent('Group 1'),
    )

    expect(screen.getByLabelText('owner')).toHaveTextContent('User 0')
    expect(screen.getByLabelText('members')).toHaveTextContent('3')
    expect(screen.getByLabelText('group id')).toHaveTextContent('group1')

    const table = screen.getByRole('table')
    const rows = within(table).getAllByRole('row')
    expect(rows).toHaveLength(4)
    checkRow(
      rows[0],
      ['Name', 'User ID', 'Affiliation', 'Role'],
      'columnheader',
    )
    checkRow(rows[1], ['User 0', 'user0', 'Uni 0', 'Owner'])
    checkRow(rows[2], ['User 1', 'user1', 'Uni 1', 'Member'])
    checkRow(rows[3], ['User 2', 'user2', 'Uni 2', 'Member'])
  })
})
