import {render, screen, within} from '@testing-library/react'
import {describe, expect, it, vi} from 'vitest'

import GroupsTable from './GroupsTable'

const setPaginate = vi.fn()
const navigate = vi.fn()

vi.mock('../../components/routing/usePagination', () => ({
  validatePaginationSearch: () => {},
  default: () => [
    {
      page: 1,
      per_page: 10,
      page_size: 12,
      total: 100,
    },
    setPaginate,
  ],
}))

vi.mock('../../components/routing/useRoute', () => ({
  default: () => ({
    navigate,
    search: {},
    url: () => '/',
    response: Array.from({length: 12}, (_, index) => ({
      group_id: `group${index}`,
      group_name: `Group ${index}`,
      owner: {user_id: `owner${index}`, name: `Owner ${index}`},
      members: [
        {user_id: `owner${index}`, name: `Owner ${index}`},
        {user_id: `member${index}`, name: `Member ${index}`},
      ],
    })),
  }),
}))

afterEach(() => {
  vi.restoreAllMocks()
})

describe('GroupsTable Component', () => {
  it('should render data, sort by name, request more data', async () => {
    render(<GroupsTable />)

    const table = screen.getByRole('table')
    expect(table).toBeVisible()

    const rows = within(table).getAllByRole('row')
    expect(rows).toHaveLength(13)

    const sortNameButton = screen.getByRole('button', {
      name: 'Sort by Name ascending',
    })
    await sortNameButton.click()
    expect(setPaginate).toHaveBeenCalledWith({
      order: 'asc',
      order_by: 'group_name',
    })

    const nextPageButton = screen.getByText('Load More')
    await nextPageButton.click()
    expect(setPaginate).toHaveBeenCalledWith({page_size: 12 + 50})
  })
})
