import {Route} from '../../components/routing/types'
import {
  createPaginationRequest,
  validatePaginationSearch,
} from '../../components/routing/usePagination'
import {GroupsRequest} from '../../models/graphRequestModels'
import {GroupsResponse} from '../../models/graphResponseModels'
import {PageBasedPagination} from '../../utils/types'
import groupRoute from './groupRoute'

type Search = {
  user_id: string
}

const groupsRoute: Route<
  GroupsRequest,
  GroupsResponse,
  Search & Required<PageBasedPagination>
> = {
  path: 'groups',
  lazyComponent: async () => import('./GroupsPage'),
  onlyRender: '',
  breadcrumb: <b>groups</b>,
  validateSearch: ({rawSearch, user, ...params}) => ({
    ...validatePaginationSearch({rawSearch, ...params}),
    user_id: rawSearch.user_id ?? user?.profile.sub ?? '',
  }),
  request: ({search, isLeaf}) =>
    isLeaf
      ? {
          m_request: {
            pagination: createPaginationRequest(search),
            query: {
              ...(search.user_id && {user_id: search.user_id}),
            },
          },
          '*': '*',
        }
      : ({} as GroupsRequest),
  children: [groupRoute],
}

export default groupsRoute
