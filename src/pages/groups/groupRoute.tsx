import {Tooltip, Typography} from '@mui/material'

import {Route} from '../../components/routing/types'
import {GroupRequest} from '../../models/graphRequestModels'
import {GroupResponse} from '../../models/graphResponseModels'

const groupRoute: Route<GroupRequest, GroupResponse> = {
  path: ':groupId',
  request: {},
  lazyComponent: async () => import('./GroupPage'),
  breadcrumb: ({response}) => (
    <Tooltip title={response?.group_id}>
      <Typography sx={{maxWidth: 180, textOverflow: 'ellipsis'}} noWrap>
        {response?.group_name}
      </Typography>
    </Tooltip>
  ),
}

export default groupRoute
