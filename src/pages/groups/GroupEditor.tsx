import {useMemo} from 'react'

import {Layout, LayoutItem} from '../../components/layout/Layout'
import useRouteData from '../../components/routing/useRouteData'
import {GroupResponse} from '../../models/graphResponseModels'
import {GroupMembersTable} from './GroupMembersTable'
import groupRoute from './groupRoute'

export default function GroupEditor({editable = false}) {
  const data = useRouteData(groupRoute) as GroupResponse

  const layout = useMemo(
    () =>
      ({
        type: 'container',
        children: [
          {
            type: 'card',
            variant: 'highlighted',
            children: [
              {
                type: 'container',
                variant: 'flex',
                sx: {
                  flexWrap: 'wrap',
                  alignItems: 'center',
                },
                children: [
                  {
                    grow: true,
                    type: 'value',
                    label: 'group name',
                    editable: editable,
                    value: data.group_name,
                  },
                  {
                    grow: true,
                    xs: 5,
                    type: 'value',
                    label: 'owner',
                    value: data?.owner?.name,
                  },
                  {
                    grow: true,
                    xs: 5,
                    type: 'value',
                    label: 'members',
                    value: data?.members?.length || '0',
                  },
                  {
                    xs: 5,
                    type: 'value',
                    component: {
                      Id: {
                        abbreviated: true,
                        labelActions: 'CopyToClipboard',
                      },
                    },
                    label: 'group id',
                    value: data.group_id,
                  },
                ],
              },
            ],
          },
          {
            grow: true,
            type: 'element',
            element: <GroupMembersTable data={data} />,
          },
        ],
      } as LayoutItem),
    [editable, data],
  )

  return (
    <>
      <Layout layout={layout} />
    </>
  )
}
