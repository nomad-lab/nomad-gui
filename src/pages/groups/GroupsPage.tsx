import useSelect from '../../components/navigation/useSelect'
import Page, {PageActions} from '../../components/page/Page'
import GroupsActions from './GroupsActions'
import GroupsTable from './GroupsTable'
import GroupsTableFilterMenu from './GroupsTableFilterMenu'

export default function GroupsPage() {
  const {pageVariant} = useSelect()

  return (
    <Page
      variant={pageVariant}
      title='Groups'
      subtitle='Manage your user groups here. Projects can also be shared with groups instead of single users.'
    >
      <PageActions leftActions={<GroupsActions />} />
      <GroupsTableFilterMenu />
      <GroupsTable />
    </Page>
  )
}
