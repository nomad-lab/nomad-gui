import {Checkbox, FormControlLabel, FormGroup} from '@mui/material'

import {PageActions} from '../../components/page/Page'
import useSearchParameter from '../../components/routing/useSearchParameter'
import useAuth from '../../hooks/useAuth'

export default function GroupsTableFilterMenu() {
  const {user} = useAuth()
  const defaultUserId = user?.profile.sub ?? ''
  const [userId, setUserId] = useSearchParameter<string>(
    'user_id',
    defaultUserId,
  )

  return (
    <PageActions>
      <FormGroup row>
        <FormControlLabel
          control={
            <Checkbox
              disabled={!user}
              checked={!!user && userId === defaultUserId}
              onChange={(event) => {
                setUserId(event.target.checked ? defaultUserId : '')
              }}
            />
          }
          label='My groups'
        />
      </FormGroup>
    </PageActions>
  )
}
