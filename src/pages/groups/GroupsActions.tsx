import {Box, Button} from '@mui/material'

import PlaceholderAction from '../../utils/PlaceholderAction'
import {SxProps} from '../../utils/types'

export default function GroupsActions({sx}: SxProps) {
  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'flex-start',
        flexWrap: 'wrap',
        gap: 1,
        ...sx,
      }}
    >
      <PlaceholderAction
        component={Button}
        variant={'contained' as unknown as 'contained'}
        color={'primary' as unknown as 'primary'}
        description={
          <div>
            This will open a dialog and ask for some key metadata, e.g. like the
            group name, before creating the group.
          </div>
        }
      >
        Create New Group
      </PlaceholderAction>
    </Box>
  )
}
