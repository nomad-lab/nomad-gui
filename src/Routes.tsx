import {RouterOptions} from 'wouter'

import App from './components/app/App'
import ErrorMessage from './components/app/ErrorMessage'
import Page from './components/page/Page'
import mdxPageRoute from './components/page/mdxPageRoute'
import Router from './components/routing/Router'
import loader from './components/routing/loader'
import useRouteError from './components/routing/useRouteError'
import Analyze from './pages/Analyze.mdx'
import appsRoute, {appEntryRoute, appRoute} from './pages/apps/appsRoute'
import documentationRoute from './pages/documentation/documentationRoute'
import groupsRoute from './pages/groups/groupsRoute'
import homeRoute from './pages/home/homeRoute'
import uploadsRoute from './pages/uploads/uploadsRoute'
import {ENV} from './utils/env'

// import useAuth from './hooks/useAuth'

const rootRoute = {
  path: '',
  component: App,
  errorComponent: function ErrorComponent() {
    const error = useRouteError()
    return (
      <App>
        <Page fullwidth>
          <ErrorMessage error={error} />
        </Page>
      </App>
    )
  },
  children: [
    homeRoute,
    uploadsRoute,
    groupsRoute,
    appsRoute,
    appRoute,
    appEntryRoute,
    mdxPageRoute('analyze', Analyze),
    documentationRoute,
  ],
}

export default function Routes() {
  // If user has not been resolved, do not load any routes. This simplifies the
  // rendering logic and prevents performing API queries without the correct
  // credentials.
  // const {isLoading} = useAuth()
  // if (isLoading) {
  //   return null
  // }

  const props = {} as RouterOptions
  if (ENV.MODE === 'production' && ENV.BASE_URL !== '/') {
    props.base = ENV.BASE_URL
  }
  return <Router route={rootRoute} loader={loader} {...props} />
}
