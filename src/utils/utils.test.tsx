import {vi} from 'vitest'

import {assert, parseFromValuesOrFunc, splice} from './utils'

describe('parseFromValuesOrFunc', () => {
  it('parses a value', () => {
    const result = parseFromValuesOrFunc(1, 2)
    expect(result).toBe(1)
  })
  it('parses a function', () => {
    const result = parseFromValuesOrFunc((arg: number) => arg + 1, 2)
    expect(result).toBe(3)
  })
})

describe('splice', () => {
  it.each([
    ['middle', [2, 1, 6], [1, 2, 6, 4, 5]],
    ['first', [0, 1, 6], [6, 2, 3, 4, 5]],
    ['last', [4, 1, 6], [1, 2, 3, 4, 6]],
    ['insert', [2, 0, 6], [1, 2, 6, 3, 4, 5]],
  ])(
    'produces a new spliced array for %s',
    (descriptions, args, expectedResult) => {
      const arr = [1, 2, 3, 4, 5]
      const result = splice(arr, ...(args as [number, number, number]))
      expect(result).toEqual(expectedResult)
      expect(result).not.toBe(arr)
      expect(arr).toEqual([1, 2, 3, 4, 5])
    },
  )
})

describe('assert', () => {
  const consoleAssert = vi.spyOn(console, 'assert').mockImplementation(() => {})
  it('delegates to console assert and throws', () => {
    expect(() => assert(false, 'This is an error')).toThrow('This is an error')
    expect(consoleAssert).toHaveBeenCalledWith(false, 'This is an error')
  })
})
