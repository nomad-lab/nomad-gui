import {vi} from 'vitest'

import {Metainfo, resolveAllMDefs} from './metainfo'
import {JSONObject} from './types'

describe('resolveSectionDef', () => {
  const apiResponse = (section: JSONObject & {name: string}) => ({
    metainfo: {
      'my.package': {
        section_definitions: {
          [section.name]: section,
          0: section,
        },
      },
    },
  })

  it('gets a section from the API response with name as key', () => {
    const metainfo = new Metainfo(
      apiResponse({
        m_def: {
          m_def: 'metainfo/metainfo/section_definitions/Section',
        },
        name: 'MySection',
      }),
    )
    const section = metainfo.resolveSectionDef(
      'metainfo/my.package/section_definitions/MySection',
    )
    expect(section).toHaveProperty('name', 'MySection')
  })

  it('gets a section from the API response with index as key', () => {
    const metainfo = new Metainfo(
      apiResponse({
        m_def: {
          m_def: 'metainfo/metainfo/section_definitions/0',
        },
        name: 'MySection',
      }),
    )
    const section = metainfo.resolveSectionDef(
      'metainfo/my.package/section_definitions/0',
    )
    expect(section).toHaveProperty('name', 'MySection')
  })

  it('resolves base sections', () => {
    const m_def = 'metainfo/my.package/section_definitions/MySection'
    const metainfo = new Metainfo(
      apiResponse({
        name: 'MySection',
        base_sections: [m_def],
        all_base_sections: [m_def, m_def],
      }),
    )
    const section = metainfo.resolveSectionDef(m_def)
    expect(section.name).toBe('MySection')
    expect(section.base_sections).toEqual([section])
    expect(section.all_base_sections).toEqual([section, section])
  })

  it('resolves references', () => {
    const m_def = 'metainfo/my.package/section_definitions/MySection'
    const quantity = {
      m_def: {
        m_def: 'metainfo/metainfo/section_definitions/Quantity',
      },
      name: 'myReference',
      type: {
        type_kind: 'reference',
        type_data: m_def,
      },
    }
    const metainfo = new Metainfo(
      apiResponse({
        m_def: {
          m_def: 'metainfo/metainfo/section_definitions/Section',
        },
        name: 'MySection',
        quantities: [quantity],
        all_quantities: {myReference: quantity},
        all_properties: {myReference: quantity},
      }),
    )
    const section = metainfo.resolveSectionDef(m_def)
    expect(section.name).toBe('MySection')
    expect(section.quantities[0].type).toHaveProperty('type_kind', 'reference')
    expect(section.quantities[0].type).toHaveProperty('type_data', section)
    expect(section.all_quantities).toHaveProperty(
      'myReference',
      section.quantities[0],
    )
    expect(section.all_properties).toHaveProperty(
      'myReference',
      section.quantities[0],
    )
  })

  it('resolves sub sections', () => {
    const m_def = 'metainfo/my.package/section_definitions/MySection'
    const subSection = {
      m_def: {
        m_def: 'metainfo/metainfo/section_definitions/SubSection',
      },
      name: 'mySubSection',
      sub_section: m_def,
    }
    const metainfo = new Metainfo(
      apiResponse({
        m_def: {
          m_def: 'metainfo/metainfo/section_definitions/Section',
        },
        name: 'MySection',
        sub_sections: [subSection],
        all_sub_sections: {mySubSection: subSection},
        all_properties: {mySubSection: subSection},
      }),
    )
    const section = metainfo.resolveSectionDef(m_def)
    expect(section.name).toBe('MySection')
    expect(section.sub_sections[0].sub_section).toBe(section)
    expect(section.all_sub_sections).toHaveProperty(
      'mySubSection',
      section.sub_sections[0],
    )
    expect(section.all_properties).toHaveProperty(
      'mySubSection',
      section.sub_sections[0],
    )
  })

  it('adds basic derived properties if they are not present in the API', () => {
    const m_def = 'metainfo/my.package/section_definitions/MySection'
    const quantity = {
      m_def: {
        m_def: 'metainfo/metainfo/section_definitions/Quantity',
      },
      name: 'myQuantity',
      type: {
        type_kind: 'Python',
        type_data: 'str',
      },
    }
    const subSection = {
      m_def: {
        m_def: 'metainfo/metainfo/section_definitions/SubSection',
      },
      name: 'mySubSection',
      sub_section: 'metainfo/my.package/section_definitions/MySection',
    }
    const metainfo = new Metainfo(
      apiResponse({
        m_def: {
          m_def: 'metainfo/metainfo/section_definitions/Section',
        },
        base_sections: ['metainfo/my.package/section_definitions/MySection'],
        name: 'MySection',
        quantities: [quantity],
        sub_sections: [subSection],
      }),
    )
    const section = metainfo.resolveSectionDef(m_def)
    expect(section).toHaveProperty('all_quantities')
    expect(section.all_quantities).toHaveProperty('myQuantity')
    expect(section).toHaveProperty('all_sub_sections')
    expect(section.all_sub_sections).toHaveProperty('mySubSection')
    expect(section).toHaveProperty('all_properties')
    expect(section.all_properties).toHaveProperty('myQuantity')
    expect(section.all_properties).toHaveProperty('mySubSection')
    expect(section).toHaveProperty('all_base_sections')
    expect(section.all_base_sections).toHaveLength(1)
  })
})

describe('resolveAllMDefs', () => {
  it('resolves m_defs in a JSON object', () => {
    function createMDef(m_def: string) {
      return {m_def}
    }

    const data: JSONObject = {
      m_def: createMDef('root'),
      child_with_m_def: {
        m_def: createMDef('child_with_m_def'),
        child: {
          m_def: createMDef('child_with_mdef/child'),
        },
      },
      child_without_m_def: {
        child_child: {
          m_def: createMDef('child_without_m_def/child'),
        },
      },
      array: [
        {
          m_def: createMDef('array/0'),
        },
        {
          m_def: createMDef('array/1'),
          child: {
            m_def: createMDef('array/1/child'),
          },
        },
        {
          child: {
            m_def: createMDef('array/2/child'),
          },
        },
      ],
    }
    const resolve = vi.fn().mockImplementation((m_def: string) => m_def)
    resolveAllMDefs(data, {resolveSectionDef: resolve} as unknown as Metainfo)

    expect(resolve).toHaveBeenNthCalledWith(1, 'root')
    expect(resolve).toHaveBeenNthCalledWith(2, 'child_with_m_def')
    expect(resolve).toHaveBeenNthCalledWith(3, 'child_with_mdef/child')
    expect(resolve).toHaveBeenNthCalledWith(4, 'child_without_m_def/child')
    expect(resolve).toHaveBeenNthCalledWith(5, 'array/0')
    expect(resolve).toHaveBeenNthCalledWith(6, 'array/1')
    expect(resolve).toHaveBeenNthCalledWith(7, 'array/1/child')
    expect(resolve).toHaveBeenNthCalledWith(8, 'array/2/child')
  })
})
