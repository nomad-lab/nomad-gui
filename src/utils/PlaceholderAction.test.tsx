import {Button} from '@mui/material'
import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {vi} from 'vitest'

import PlaceholderAction from '../utils/PlaceholderAction'

describe('PlaceholderAction', () => {
  const mockOnClose = vi.fn()

  const placeholderActionProps = {
    description: 'This is a placeholder action',
    component: Button,
    children: 'open',
    onClose: mockOnClose,
  }

  it('renders the component', () => {
    render(<PlaceholderAction {...placeholderActionProps} />)
    expect(
      screen.getByText(placeholderActionProps.children as string),
    ).toBeInTheDocument()
  })

  it('opens the dialog', async () => {
    render(<PlaceholderAction {...placeholderActionProps} />)
    await userEvent.click(screen.getByRole('button'))
    expect(
      screen.getByText(placeholderActionProps.description as string),
    ).toBeVisible()
  })

  it('closes the dialog', async () => {
    render(<PlaceholderAction {...placeholderActionProps} />)
    await userEvent.click(screen.getByRole('button'))
    expect(
      screen.getByText(placeholderActionProps.description as string),
    ).toBeVisible()
    await userEvent.click(screen.getByRole('button', {name: 'Close'}))
    expect(
      screen.getByText(placeholderActionProps.description as string),
    ).not.toBeVisible()
  })
})
