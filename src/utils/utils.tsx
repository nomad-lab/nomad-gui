/**
 * Parses a value or a function that returns a value, based on the provided argument.
 * If the input is a function, it's invoked with the provided argument; otherwise, the input value is returned.
 *
 * @param fn - The value or function to parse.
 * @param arg - The argument to pass to the function (if a function is provided).
 * @returns The parsed value or the result of the function invocation.
 */
export const parseFromValuesOrFunc = <T, U>(
  fn: ((arg: U) => T) | T | undefined,
  arg: U,
): T | undefined => (fn instanceof Function ? fn(arg) : fn)

/**
 * Like Array.splice but produces a new array on an immutable array.
 */
export function splice<T>(
  arr: T[],
  start: number,
  deleteCount: number,
  ...addItem: T[]
) {
  const result = []
  if (start > 0) {
    result.push(...arr.slice(0, start))
  }
  result.push(...addItem)
  const len = result.length - addItem.length
  const count = deleteCount <= 0 ? len : len + deleteCount
  if (arr[count]) {
    result.push(...arr.slice(count))
  }
  return result
}

export function assert(
  condition: unknown,
  message?: string,
): asserts condition {
  // eslint-disable-next-line no-console
  console.assert(condition, message)
  if (!condition) {
    throw new Error(message)
  }
}

export const ignoreSsrWarning =
  '/* emotion-disable-server-rendering-unsafe-selector-warning-please-do-not-use-this-the-warning-exists-for-a-reason */'
