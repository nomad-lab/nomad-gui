import lodash from 'lodash'

import {GraphResponse, MSectionResponse} from '../models/graphResponseModels'
import {JSONObject, JSONValue} from './types'
import {assert} from './utils'

export const metainfoDataTypePackage = 'nomad.metainfo.data_type'

export type Package = {
  section_definitions: {
    [namd: string]: Section
  }
}

export type Section = {
  m_raw: MSectionResponse
  m_resolved: boolean
  m_ref: string
  m_annotations: {
    [name: string]: JSONValue
  }

  name: string
  base_sections: Section[]
  quantities: Quantity[]
  sub_sections: SubSection[]
  all_base_sections: Section[]
  all_quantities: {
    [name: string]: Quantity
  }
  all_sub_sections: {
    [name: string]: SubSection
  }
  all_properties: {
    [name: string]: Property
  }
}

export type Quantity = {
  m_def: 'Quantity'
  name: string
  type: Type
  shape: (string | number | '*')[]
  unit?: string
}

export type SubSection = {
  m_def: 'SubSection'
  name: string
  repeats: boolean
  sub_section: Section | string
}

export type Property = Quantity | SubSection

export type PythonType = {
  type_kind: 'python'
  type_data: 'str' | 'int' | 'float' | 'bool'
}
export type NumpyType = {
  type_kind: 'numpy'
  type_data: string
}
export type CustomType<TypeData = unknown> = {
  type_kind: 'custom'
  type_data: TypeData
}
export type ReferenceType = {
  type_kind: 'reference'
  type_data: Section
}
export type EnumType = {
  type_kind: 'enum'
  type_data: string[]
}
export type Type =
  | PythonType
  | NumpyType
  | CustomType
  | ReferenceType
  | EnumType

/**
 * Resolves a reference to a section within the given graph API response.
 */
export function resolveRef(
  ref: string | JSONObject,
  data: GraphResponse,
): JSONObject {
  if (typeof ref === 'object') {
    return ref
  }

  const refSegments = ref.split('/').filter((segment) => segment !== '')

  return refSegments.reduce((current: JSONObject, key) => {
    return current?.[key] as JSONObject
  }, data as JSONObject)
}

/**
 * Provides functionality to interpret metainfo and archive data retrieved
 * from the API.
 */
export class Metainfo {
  data: GraphResponse
  resolvedSections: {
    [ref: string]: Section
  }

  constructor(data: GraphResponse) {
    this.data = data
    this.resolvedSections = {}
  }

  resolveRef(ref: string): JSONObject {
    return resolveRef(ref, this.data)
  }

  /**
   * A helper function to resolve a section reference and get the section from
   * an API response.
   *
   * @param ref A reference to a section, e.g. '/metainfo/nomad.metainfo/section_definitions/Section'
   * @param data A GraphResponse from the API
   * @returns The resolved section
   */
  resolveSectionDef(ref: string): Section {
    assert(ref && typeof ref === 'string', 'Invalid section reference.')

    if (this.resolvedSections[ref]) {
      return this.resolvedSections[ref]
    }

    const sectionResponse = this.resolveRef(ref)

    if (!sectionResponse) {
      // This happens frequently. Not all metainfo is retrieved recrusively
      // and the API response might miss some sections. We return an empty
      // placeholder. The client must be able to handle this.
      return {
        m_def: 'Section',
      } as unknown as Section
    }

    const toSection = (section: string): Section =>
      this.resolveSectionDef(section)

    const toQuantity = (quantity: JSONObject): Quantity => {
      const result = {
        m_def: 'Quantity',
        name: quantity.name as string,
        type: {
          ...(quantity.type as Type),
        },
        shape: quantity.shape as (string | number | '*')[],
        ...(quantity.unit && {unit: quantity.unit as string}),
      } as Quantity

      if (result.type.type_kind === 'reference') {
        result.type.type_data = this.resolveSectionDef(
          result.type.type_data as unknown as string,
        )
      }

      return result
    }

    const toSubSection = (subSection: JSONObject): SubSection => {
      return {
        m_def: 'SubSection',
        name: subSection.name as string,
        repeats: subSection.repeats as boolean,
        sub_section: this.resolveSectionDef(subSection.sub_section as string),
      }
    }

    const section = {} as Section
    this.resolvedSections[ref] = section

    Object.assign(section, {
      m_raw: sectionResponse,
      m_resolved: false,
      m_ref: ref,
      m_annotations: sectionResponse.m_annotations || {},

      name: sectionResponse.name,

      base_sections:
        (sectionResponse.base_sections as string[])?.map(toSection) || [],
      quantities:
        (sectionResponse.quantities as JSONObject[])?.map(toQuantity) || [],
      sub_sections:
        (sectionResponse.sub_sections as JSONObject[])?.map(toSubSection) || [],
      all_base_sections: (
        (sectionResponse.all_base_sections ||
          sectionResponse.base_sections ||
          []) as string[]
      ).map(toSection),
      all_quantities: sectionResponse.all_quantities
        ? Object.fromEntries(
            Object.entries(sectionResponse.all_quantities as JSONObject).map(
              ([name, quantity]) => [
                name,
                toQuantity(this.resolveRef(quantity as string)),
              ],
            ),
          )
        : Object.fromEntries(
            ((sectionResponse.quantities as JSONObject[]) || []).map(
              (quantity) => [quantity.name, toQuantity(quantity)],
            ),
          ),
      all_sub_sections: sectionResponse.all_sub_sections
        ? Object.fromEntries(
            Object.entries(sectionResponse.all_sub_sections as JSONObject).map(
              ([name, subSection]) => [
                name,
                toSubSection(this.resolveRef(subSection as string)),
              ],
            ),
          )
        : Object.fromEntries(
            ((sectionResponse.sub_sections as JSONObject[]) || []).map(
              (subSection) => [subSection.name, toSubSection(subSection)],
            ),
          ),
      all_properties: sectionResponse.all_properties
        ? Object.fromEntries(
            Object.entries(sectionResponse.all_properties as JSONObject).map(
              ([name, property]) => {
                property = this.resolveRef(property as string)
                if (property.type !== undefined) {
                  return [name, toQuantity(property as JSONObject)]
                } else {
                  return [name, toSubSection(property as JSONObject)]
                }
              },
            ),
          )
        : Object.fromEntries([
            ...((sectionResponse.quantities as JSONObject[]) || []).map(
              (quantity) => [quantity.name, toQuantity(quantity)],
            ),
            ...((sectionResponse.sub_sections as JSONObject[]) || []).map(
              (subSection) => [subSection.name, toSubSection(subSection)],
            ),
          ]),
    } as Section)

    return section
  }
}

/**
 * Recursively traverses through the given data object and
 * resolves all m_def references it can find.
 */
export function resolveAllMDefs(data: JSONObject, metainfo?: Metainfo) {
  if (!metainfo) {
    metainfo = new Metainfo(data)
  }
  const m_def = (data?.m_def as JSONObject)?.m_def
  if (typeof m_def === 'string') {
    data.m_def = metainfo.resolveSectionDef(m_def) as JSONObject
  }

  for (const key in data) {
    if (key.startsWith('m_')) {
      continue
    }
    const value = data[key]
    if (Array.isArray(value)) {
      for (const item of value) {
        if (lodash.isObject(item)) {
          resolveAllMDefs(item as JSONObject, metainfo)
        }
      }
    } else if (typeof value === 'object') {
      resolveAllMDefs(value as JSONObject, metainfo)
    }
  }
}
