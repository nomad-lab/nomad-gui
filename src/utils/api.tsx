import {User} from 'oidc-client-ts'

import {ArchiveChange} from '../models/entryEditRequestModels'
import {GraphRequest} from '../models/graphRequestModels'
import {GraphResponse} from '../models/graphResponseModels'
import {ENV} from './env'

export async function graphApi(
  request: GraphRequest,
  user?: User,
): Promise<GraphResponse> {
  // Define headers for the request
  const headers: HeadersInit = {
    'Content-Type': 'application/json',
  }

  if (user?.access_token) {
    headers['Authorization'] = `Bearer ${user.access_token}`
  }

  // Make an HTTP POST request to the API with the JSON payload
  const response = await fetch(`${ENV.API_URL}/graph/query`, {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(request),
  })

  if (response.ok) {
    try {
      const responseData = await response.json()
      return responseData
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(await response.text())
      // eslint-disable-next-line no-console
      console.error('Error parsing JSON:', error)
      throw new Error('Could not parse API response')
    }
  } else {
    throw new Error(`API request failed with status: ${response.status}`)
  }
}

export async function archiveEditApi(
  entryId: string,
  changes: ArchiveChange[],
  user: User,
): Promise<GraphResponse> {
  // Define headers for the request
  const headers = {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${user.access_token}`,
  }

  // Make an HTTP POST request to the API with the JSON payload
  const response = await fetch(`${ENV.API_URL}/entries/${entryId}/edit`, {
    method: 'POST',
    headers: headers,
    body: JSON.stringify({changes}),
  })

  if (response.ok) {
    try {
      const responseData = await response.json()
      return responseData
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(await response.text())
      // eslint-disable-next-line no-console
      console.error('Error parsing JSON:', error)
      throw new Error('Could not parse API response')
    }
  } else {
    throw new Error(`API request failed with status: ${response.status}`)
  }
}
