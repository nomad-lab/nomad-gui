import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogProps,
  DialogTitle,
} from '@mui/material'
import React, {ComponentType, useCallback, useState} from 'react'

type PlaceholderProps = {
  description: React.ReactNode
}

export type PlaceholderActionType<T extends ComponentType<P>, P> = {
  dialogProps?: Omit<DialogProps, 'open' | 'onClose'>
  component: T
} & PlaceholderProps &
  Partial<P>

export function PlaceholderDialog({
  description,
  onClose,
  ...props
}: PlaceholderProps & Omit<DialogProps, 'onClose'> & {onClose?: () => void}) {
  return (
    <>
      <Dialog {...props} onClose={onClose}>
        <DialogTitle>Not implemented yet</DialogTitle>
        <DialogContent>
          <DialogContentText>{description}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>Close</Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default function PlaceholderAction<T extends ComponentType<P>, P>({
  dialogProps = {},
  description,
  component,
  ...props
}: PlaceholderActionType<T, P>) {
  const Component = component as React.ComponentType<
    Omit<P, 'onClick'> & {onClick: () => void}
  >
  const [open, setOpen] = useState(false)
  const handleClick = useCallback(() => setOpen(true), [setOpen])
  const handleClose = useCallback(() => setOpen(false), [setOpen])
  return (
    <>
      <Component
        onClick={handleClick}
        {...(props as unknown as Omit<P, 'onClick'>)}
      />
      <PlaceholderDialog
        {...dialogProps}
        description={description}
        open={open}
        onClose={handleClose}
      />
    </>
  )
}
