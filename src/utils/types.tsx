import {SxProps as MuiSxProps, Theme} from '@mui/material'

import {Pagination1} from '../models/graphRequestModels'

export interface SxProps {
  sx?: MuiSxProps<Theme>
}

export type JSONValue =
  | string
  | number
  | boolean
  | JSONObject
  | Array<JSONValue>
  | null
export type JSONObject = {[key: string]: JSONValue}
export type Filter = JSONObject
export type Data = JSONObject

export type DefaultToObject<T> = T extends object ? T : JSONObject

export type PageBasedPagination = Pick<
  Pagination1,
  'page' | 'page_size' | 'order' | 'order_by'
>
