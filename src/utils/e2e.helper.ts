import {APIRequestContext, PlaywrightWorkerArgs, expect} from '@playwright/test'

import {UploadProcDataQueryResponse} from '../models/UploadProcDataQueryResponse'

type Playwright = PlaywrightWorkerArgs['playwright']

interface AnyProps {
  [key: string]: unknown
}
interface Params {
  [key: string]: string | number | boolean
}
interface UploadRequest {
  data: AnyProps
}
interface BundleRequest {
  data: AnyProps
  params: Params
}

// importing ENV from .env throws error:
// > SyntaxError: Cannot use 'import.meta' outside a module
const apiUrl = process.env.E2E_API_URL

async function getAccessToken() {
  try {
    const response = await fetch(
      `${apiUrl}/auth/token?username=test&password=password`,
      {
        method: 'GET',
        headers: {
          accept: 'application/json',
        },
      },
    )
    expect(response.status == 200).toBeTruthy()
    const json = await response.json()
    return json.access_token
  } catch (error) {
    // eslint-disable-next-line
    console.log(error)
  }
}

async function getApiContext(playwright: Playwright) {
  const accessToken = await getAccessToken()
  const apiContext = await playwright.request.newContext({
    baseURL: apiUrl,
    extraHTTPHeaders: {
      'Content-Type': 'application/json',
      accept: 'application/json',
      Authorization: `Bearer ${accessToken}`,
    },
  })
  return apiContext
}

async function getUploads(
  apiContext: APIRequestContext,
): Promise<UploadProcDataQueryResponse> {
  const response = await apiContext.get(`${apiUrl}/uploads`)
  expect(response.ok()).toBeTruthy()
  return response.json()
}

async function postUpload(
  apiContext: APIRequestContext,
  upload: UploadRequest,
): Promise<void> {
  const response = await apiContext.post(`${apiUrl}/uploads`, {
    data: upload.data,
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  })
  expect(response.ok()).toBeTruthy()
}

async function postBundle(
  apiContext: APIRequestContext,
  bundle: BundleRequest,
): Promise<void> {
  const response = await apiContext.post(`uploads/bundle`, {
    data: bundle.data,
    params: bundle.params,
  })
  expect(response.ok()).toBeTruthy()
}

async function deleteUpload(
  apiContext: APIRequestContext,
  uploadId: string,
): Promise<void> {
  const response = await apiContext.delete(`${apiUrl}/uploads/${uploadId}`)
  expect.soft(response.ok()).toBeTruthy()
}

async function deleteAllUploads(
  apiContext: APIRequestContext,
): Promise<string[]> {
  const uploads: UploadProcDataQueryResponse = await getUploads(apiContext)
  if (uploads.pagination.total === 0) return []

  expect(uploads.data).toBeDefined()
  const uploadIds = uploads.data!.map((data) => data.upload_id as string)
  await Promise.all(uploadIds.map((id) => deleteUpload(apiContext, id)))

  expect(async () => {
    const uploads = await getUploads(apiContext)
    expect(uploads.pagination.total).toBe(0)
  }).toPass()

  return uploadIds
}

type BeforeFnArgs = {
  playwright: Playwright
  uploads?: UploadRequest[]
  bundles?: BundleRequest[]
}
export async function beforeFn({
  playwright,
  uploads = [],
  bundles = [],
}: BeforeFnArgs): Promise<APIRequestContext> {
  const apiContext = await getApiContext(playwright)
  await deleteAllUploads(apiContext)

  uploads.forEach((upload) => postUpload(apiContext, upload))
  bundles.forEach((bundle) => postBundle(apiContext, bundle))

  return apiContext
}

export async function afterFn(apiContext: APIRequestContext) {
  await deleteAllUploads(apiContext)

  // Dispose all responses.
  await apiContext.dispose()
}
