import config from '../config'

export const ENV = {
  MODE: import.meta.env.MODE,
  BASE_URL: import.meta.env.BASE_URL,
  MOCKED_API_LATENCY: import.meta.env.VITE_USE_MOCKED_API_LATENCY_MS,
  USE_MOCKED_API: import.meta.env.VITE_USE_MOCKED_API === 'true',
  LOAD_MORE_PAGE_SIZE: parseInt(import.meta.env.VITE_LOAD_MORE_PAGE_SIZE),
  API_URL: '', // will be set later
}

if (ENV.USE_MOCKED_API) {
  ENV.API_URL = import.meta.env.VITE_MOCKED_API_URL
} else {
  const host = config.services.api_host
  const basePath = config.services.api_base_path?.replace(/\/+$/, '')
  if (config.services.https) {
    ENV.API_URL = `https://${host}${basePath}/api/v1`
  } else {
    const port = config.services.api_port
    ENV.API_URL = `http://${host}:${port}${basePath}/api/v1`
  }
}
