import {ThemeProvider, createTheme} from '@mui/material/styles'
import {render as rtlRender, within} from '@testing-library/react'
import mediaQuery from 'css-mediaquery'
import React, {ReactElement, ReactNode} from 'react'
import {vi} from 'vitest'

import Router from '../components/routing/Router'
import loader from '../components/routing/loader'
import {DefaultSearch, Route} from '../components/routing/types'

export const renderCount = vi.fn()
export const renderWithData = vi.fn()

export async function renderWithRouteData(
  route: Route<object, object> | Route,
) {
  const rootRoute: Route = {
    path: '',
    children: [route],
  }
  rtlRender(<Router route={rootRoute} loader={loader} />)
}

export function createMatchMedia(width?: number) {
  return (query: string) => ({
    matches: mediaQuery.match(query, {
      width,
    }),
    media: query,
    onchange: null,
    addListener: () => vi.fn(),
    removeListener: () => vi.fn(),
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  })
}

export async function importLazyComponents<
  Request,
  Response,
  Search extends DefaultSearch,
>(route: Route<Request, Response, Search>) {
  function getAllLazyComponents(route: Route, visited: Set<Route> = new Set()) {
    if (visited.has(route)) {
      return []
    }
    visited.add(route)
    const components: (() => Promise<{default: React.ElementType}>)[] = []
    if (route.lazyComponent) {
      components.push(route.lazyComponent)
    }
    if (route.children) {
      for (const child of route.children) {
        components.push(...getAllLazyComponents(child, visited))
      }
    }
    return components
  }

  for (const importComponent of getAllLazyComponents(
    route as unknown as Route,
  )) {
    await importComponent()
  }
}

// This custom theme disables certain transitions. This makes the tests faster
// and can also allows the tests to be simpler, as they might not need to wait
// for element appearance/disappearance.
const theme = createTheme({
  transitions: {create: () => 'none'},
})

// eslint-disable-next-line react-refresh/only-export-components
function AllProviders({children}: {children: ReactNode}) {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>
}

const customRender = (ui: ReactElement, options = {}) =>
  rtlRender(ui, {wrapper: AllProviders, ...options})

// Re-export everything from testing-library
// eslint-disable-next-line react-refresh/only-export-components
export * from '@testing-library/react'
export {customRender as render}

type tableCellRole = 'cell' | 'columnheader' | 'rowheader'

export const checkRow = (
  row: HTMLElement,
  texts: string[],
  role: tableCellRole = 'cell',
) => {
  const cells = within(row).getAllByRole(role)
  expect(cells).toHaveLength(texts.length)
  cells.forEach((cell, i) => expect(cell).toHaveTextContent(texts[i]))
}
