import {vi} from 'vitest'

import {graphApi} from '../utils/api'

describe('api', () => {
  it('should return a successful response', async () => {
    vi.spyOn(global, 'fetch').mockResolvedValueOnce({
      ok: true,
      json: vi.fn().mockResolvedValueOnce({}),
    } as unknown as Response)

    const response = await graphApi({})
    expect(response).toEqual({})
  })

  it('should raise an error when response not ok', async () => {
    vi.spyOn(global, 'fetch').mockResolvedValueOnce({
      ok: false,
      status: 400,
    } as unknown as Response)

    await expect(async () => await graphApi({})).rejects.toThrow(
      'API request failed with status: 400',
    )
  })

  it('should raise an error when json cannot be parsed', async () => {
    vi.spyOn(global, 'fetch').mockResolvedValueOnce({
      ok: true,
      json: vi.fn().mockRejectedValueOnce(new Error('JSON parse error')),
      text: vi.fn().mockResolvedValueOnce('error message'),
    } as unknown as Response)

    const consoleError = vi.spyOn(console, 'error').mockImplementation(() => {})
    const consoleLog = vi.spyOn(console, 'log').mockImplementation(() => {})

    await expect(async () => await graphApi({})).rejects.toThrow(
      'Could not parse API response',
    )
    expect(consoleError).toHaveBeenCalledWith(
      'Error parsing JSON:',
      new Error('JSON parse error'),
    )
    expect(consoleLog).toHaveBeenCalledWith('error message')
  })
})
