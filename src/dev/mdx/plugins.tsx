import {readFileSync} from 'fs'
import {Element} from 'hast'
import {Code, Paragraph, Root} from 'mdast'
import {Plugin} from 'unified'
import visit from 'unist-util-visit'

/**
 * Rehype plugin for post processing the html from .mdx files. It splits the
 * nav/toc element created with rehype-toc and the content into a flex box.
 */
export function rehypePost() {
  const processor = (tree: Element) => {
    const nav = tree.children[0] as Element
    nav.children = [
      {
        type: 'element',
        tagName: 'h1',
        children: [
          {
            type: 'text',
            value: 'Table of contents',
          },
        ],
        properties: {
          className: 'nav',
        },
      },
      ...nav.children,
    ]
    const content = tree.children.slice(1)
    tree.children = [
      nav,
      {
        type: 'element',
        tagName: 'div',
        properties: {
          className: 'content',
        },
        children: content,
      },
    ]
  }
  return processor
}

/**
 * Remark plugin that replaces all `[[code(<some file name>)]]` with
 * a code block that contains the given files content.
 */
export const remarkLoadContent: Plugin<[], Root> = () => {
  return (ast) => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    visit<any>(ast as any, 'paragraph', (node: Paragraph) => {
      if (node.children.length === 1 && node.children[0].type === 'text') {
        const inputMatch = node.children[0].value.match(
          /^\s*\[\[\s*code\s*\(\s*(.*)\s*\)\s*\]\]\s*$/,
        )
        if (inputMatch) {
          const filename = inputMatch[1]
          const lang = filename.substring(filename.lastIndexOf('.') + 1)
          const content = readFileSync(filename).toString().trim()
          const code = node as unknown as Code
          code.type = 'code'
          code.value = content
          code.lang = lang
        }
      }
    })
  }
}
