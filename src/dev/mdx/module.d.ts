declare module '*.mdx' {
  let MDXComponent: (props: Record<string, unknown>) => JSX.Element
  export default MDXComponent
}

declare module 'virtual:sitemap' {
  type SitemapEntry = {
    path: string
    menu?: string
    mdx: boolean
    component: React.FC<object>
  }
  // eslint-disable-next-line
  const sitemap: SitemapEntry[]
  export default sitemap
}
