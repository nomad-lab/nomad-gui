# Getting Started

Follow these steps to set up and work on the NOMAD website project:

## Prerequisites

Before you begin, ensure you have the following software and access:

- **Node.js**: Ensure you have Node.js installed. We recommend using the LTS version.
- **npm**: You'll need a package manager for installing project dependencies.

## Installation

1. Clone this repository to your local machine:

   ```shell
   git clone https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-gui.git
   ```

2. Navigate to the project directory:

   ```shell
   cd nomad-gui
   ```

3. Install project dependencies:

   ```shell
   npm install
   ```

## Run the dev server

To start the development server and work on the website, follow these steps:

1. Launch the development server:

   ```shell
   npm start
   ```

2. Open your web browser and access the development environment at http://localhost:3000. You can start making changes to the website code, and the server will automatically reload for you to see your updates.

## Run storybook

We use storybook to document and demo individual components. To use
[storybook](https://storybook.js.org/docs/react/get-started/install/) follow those steps:

1. Launch the storybook server:

   ```shell
   npm run storybook
   ```

2. Open your web browser and access the development environment at http://localhost:6006. You can start making changes to the website code, and the server will automatically reload for you to see your updates.

## Using NOMAD API mock (Default)

During development, by default, you won't be required to start the API server. Instead, a mock server is run so that you can continue with your development without relying on the actual API server.

## Use real NOMAD API locally

If you need to connect to the API server locally during development, follow these steps:

1. Create an `.env.local` file in the project directory.

2. Add the following line to the `.env.local` file to enable local API server connectivity:

   ```shell
   VITE_USE_MOCKED_API=true
   ```

3. Source the `.env.local` file to make the environment variables available:

   ```shell
   source .env.local
   ```

4. Restart the development server:

   ```shell
   npm start
   ```

Now, all of the requests made by your website will be directed to the API server running locally.

Please note that the `VITE_API_URL` variable, defined in the `.env` file, is used to configure the API server's URL. If your local API server address is different, you can add an entry in the `.env.local` file for the `VITE_API_URL` variable. By default, the `.env.local` file doesn't get committed to Git, so you can change the settings in that file without affecting the `.env` file, which should not contain sensitive or local-specific configuration.

## Create production build

1. When you're ready to build the website for a production environment, use the following command:

```shell
npm run build
```

This will create an optimized production build in the `build` directory, ready for deployment.

2. To run the production version of the website locally, use the following command:

```shell
npm run serve
```

3. Open your web browser and access the production environment at `http://localhost:4173`. Note: any further changes you make to the code would not be updated in the website, to update the website you'll need re-run the `npm run build` as earlier.

## Project structure

Here's an overview of the project structure:

- `src/`: This directory contains the source code for the website.
- `public/`: Publicly accessible assets like images, fonts, and icons.
- `build/`: Production build output.
- `docs/`: Markdown documentation shown in storybook.
- `vite.config.js`: Vite configuration file.

## Run tests

We use `vitest` for unit tests and `Playwright` for end-to-end tests. This section provides guidelines and instructions for running and writing tests.

### Unit Tests with Vitest

To run unit tests using `vitest`, follow these steps:

1. Ensure that you have installed project dependencies:

   ```shell
   npm install
   ```

2. Run the unit tests:

   ```shell
   npm run test:unit
   ```

### End-to-End Tests with Playwright

To run end-to-end tests using `Playwright`, follow these steps:

1. Ensure that you have installed project dependencies (if not already done):

   ```shell
   npm install
   npx playwright install
   ```

2. Run the end-to-end tests. To run them in headless mode, use:

   ```shell
   npm run test:e2e
   ```

   To run the tests in an interactive UI mode, use:

   ```shell
   npm run test:e2e -- --ui
   ```

Read [E2ETesting](/docs/E2ETesting--docs) for more information on setting up and
running end-to-end tests.

## Guidelines

Continnue now to read the [coding guidelines](/docs/Guidelines--docs).

## Additional Resources

For more information on testing frameworks and practices, refer to the documentation of `vitest` and `Playwright`:

- [vitest.dev](https://vitest.dev/)
- [playwright.dev](https://playwright.dev/)
- [storybook.js.org/docs](https://storybook.js.org/docs/react/get-started/install/)
