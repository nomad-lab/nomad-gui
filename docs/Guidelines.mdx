# Guidelines

These are guidelines, not rules. Otherwise, obey them.

**TODO** Add example code for all patterns.

## Files and directories

Generic components and hooks that are used everywhere are placed in the respective
top level directories for `hooks` and `components`.
All page routes and components go into the `pages` directory.

Modules that match thematically can be put into sub-directories. Therefore,
all components and hooks that are are only used in a single parent page or component
go into the respective sub-directory of that parent. Further nesting
is allowed.

Module names should be specific, even if they are in sub-directories that
would otherwise already make them specific.

Good:

- `/pages/uploads/uploadsRoute.tsx`
- `/pages/uploads/UploadsTable.tsx`
- `/pages/uploads/files/FileTable.tsx`
- `/components/table/RoutingTable.tsx`
- `/components/table/useTableNavigation.tsx`
- `/hooks/useRoute.tsx`

Bad:

- `/components/uploads/UploadsTable`
- `/pages/uploads/Table`

## ts and tsx

- Don't use interfaces, use the more flexible type aliases.
- Use [utility types](https://www.typescriptlang.org/docs/handbook/utility-types.html)
  `Partial`, `Omit`, `Pick`, etc. to imply relationships between types if they exist.
- Use `const`, not `let`, but not if it forces nested `?`-operator expressions.
- **TODO**: `for` vs `forEach`, `map`, `filter`, etc. What about `reduce`?

### Comments

- Don't comment code, write code that is self-explanatory.
- If you comment code, describe *why*, not *what*.
- Document components, hooks, and functions, including their props and parameters;
  you do not need to document internal components and functions.

### Modules

Exported components should get their own module of the same name and be exported
as `default`. Other exports, e.g. for interfaces for props, are allowed. Only
the main exported component must be stateful.

You cannot mix exports for components with exports for hooks, variables, other functions
in the same module. This will break "safe refresh" during development. There
is a lint rule to enforce this.

Stories and test modules go into the same directory and modules are named
consistently. For test and story utilities that should not go into the build
or coverage, use the `helper` suffix:

- `Page.tsx`
- `Page.stories.tsx`
- `Page.test.tsx` (unit tests)
- `Page.spec.tsx` (e2e tests)
- `utils.helper.tsx` (helper functions for tests or stories)

### Variables

- Use mostly `const`, but prefer `let` over complex nested `?`-operator expressions.
- Use naming conventions: `handle<Event>`, `has<Condition>`, `props`, `<component>Props`
- Pair names: `<someState>` and `setSomeState`; `onClick` and `handleClick`
- Certain names require a certain type: `onClick: () => void`, `component: React.ElementType`
- Avoid: `other`, `rest` and use more specific names instead.
- Never use capital letters in variable names, no `MAX_WIDTH`.
- Treat abbreviations as words in camel case: use `baseUrl`, not `baseURL`.
- Use specific descriptions when name overlap cannot be avoided: Use `maxWithOrDefault = maxWidth || 0` instead of `maximalWidth = maxWidth || 0`.

Conventional names or name parts:

- `props` for the props of the component.
- `params` for complex function parameters, e.g. `(params: {one: string, two: string}) => void`.
- `component` for props that pass component types, e.g. `component: React.ElementType`.
- `render` for functions that return a React element, e.g. `render: () => React.ReactNode`.
- `is` and `has` for boolean variables, e.g. `isDisabled`, `hasError`.
- `handle` for event handlers, e.g. `handleClick`, `handleKeyDown`.
- `on` for event callback props, e.g. `onChange`.

### Functions or closures

- Use named functions on the top level and for components, there should be no need to
  use closures here.
- Use closures in nested contexts, especially if you need to close variables or use them as arguments.

## Component

### Component names

- Be specific.
- Use reoccurring terminology: `...Table`, `...Actions`, `...Page`
- Use existing terminology when using components from other libraries: `UploadButton`, `UserAutocomplete`
- Avoid denominating a layout quality: `CreateActions` (good), `CreateActionsRow` (bad)
- Prefix components that logically belong together: `UploadsPage`, `UploadsTable`, `UploadsTableActions`

### Basic component pattern

Follow a consistent pattern for component implementations:

- Define the props type alias with `<ComponentName>Props` as a name.
- Reuse `SxProps` and `PropsWithChildren` when necessary.
- Reuse existing prop types, when you drill down props, make use of
typescript's `Omit`, `Pick`, `Partial`, etc.
- Use `<componentName>Props` as var names for drill down props.

```tsx
export type MyComponentProps = {...}
  & Omit<Partial<PaperProps>, 'raised'>
  & PropsWithChildren & SxProps

export default function MyComponent({sx, children, ...paperProps}: MyComponentProps) {
  return (
    <Paper sx={{myCss: 'value', ...sx}} {...paperProps}>
      {children}
    </Paper>
  )
}
```

### Prop names and types

- Consistent names: `to`, `value`, `data`, `on<"Event">`
- Use paired prop names, especially for controlled components, e.g. `value` and `onChange`.
- Always define prop types in an extra type alias right before the component.
- Document all props.

### Controlled vs uncontrolled components

- Make a conscious decision if your component is controlled or not.
- If in doubt, make it a controlled component.
- Only have state in a controlled component, if it is really independent of the props.
- Use consisted names for controlled props: `value`, `onChange`, `<prop>`, `on<Prop>Change`

### useState

- Use an update function wherever sensible, avoid using the variable for the
  current state value: Use `setValue(value => value + 1)` and not `setValue(value + 1)`.
- _Do never_ use `useState` for derived state. Don't store states derived from
  other states, don't store states derived from props. Try to be sensitive for
  these and always question if state is atomic and if you really need to use
  `useState`.

### React.memo

- Do not memo as a reflex.
- Do not memo if the component is always composed in a state-less component. Memo this containing component instead.

### React.forwardRef

- Be familiar with [React.forwardRef](https://react.dev/reference/react/forwardRef) and know when to use them.

### useMemo, useCallback

- There are three reasons to memo:
  - to keep object identity;
  - to derive a new value from a dependency;
  - for performance.
- Know your reasoning and weigh the costs.
- As a rule of thumb: memo object, arrays, functions, but not primitive values.

### useEffect

- Always return a function to unregister the effect.
- Avoid `useEffect` and encapsulate effects in hooks and think about existing
  hooks that might already give you the desired effect.

### Other hooks

- Get familiar with [`react-use`](https://github.com/streamich/react-use) first.
- Prefer custom hooks over direct use of `useEffect`, `useContext`.

### Using contexts

Do you really need a context and should you not use props instead?

Use consistent naming:

```tsx
type MyContextValue = {...}
const myContext = React.createContext<MyContextValue | null>(null)
const myContextValue = {...}
<myContext.Provider value={myContextValue}>...</myContext.Provider>
function useMy() {...}
```

You will need to put `myContext` and `useMy`in a module that is separate from
the component that has the context provider. Otherwise, you will break "safe
refresh" constraints.

### Themes, `styled`, `SX`, etc.

- Consider `styled` for low-level components, follow the MUI guidelines and use
 `generateUtilityClasses` to produce fixed class names.
- For all other components stick to `sx`.
- If you want to expose the root styling of a component based on `sx`, do:

```tsx
import {SxProps} from '.../utils/types'

function MyComponent(props: MyComponentProps & {sx?: SxProps}) {
  const {sx, ...other} = props
  return <div sx={{myCss: 'value', ...sx}} />
}
```

### Test

We use a combination of unit and e2e tests. Generally we test all components and
hooks with unit tests and all pages with e2e tests. The pages may be exempted
from unit tests coverage report (in which case they are ideally covered with e2e
tests). You can control the files included in the tests and in the coverage
report using `vite.config.mts`. The relevant keys are`tests.include/exclude` and
`tests.coverage.include/exclude`.

- Name your test files with a `.test.ts` or `.test.tsx` extension.
- Use `describe` and `it` to structure your test suites and test cases.
  Use `describe('MyComponent', ...)` or `describe('useMyHook', ...)` to mentioned
  the component under test, and use `it` to describe the expected behavior in
  sentences, e.g. `it('should render', ...)`.
- Use [`userEvent`](https://testing-library.com/docs/user-event/intro) to
  emulate user interactions over low level functions like `fireEvent`.
- Make sure your tests are not producing any warning output.
- Aim for 100 % statement, ideally 100 % branch coverage.
  `npx vitest --coverage` will create and show you the coverage report.
- Use TypeScript's type checking for better type safety.
- Parameterize `it.each` when appropriate, otherwise keep tests clean and
  straightforward, _spaghetti_ tests are fine.
- Consider tests that produce warnings or errors as failed tests.

Example unit test file (`Counter.test.ts`):

```typescript
```tsx
describe('Counter', () => {
  it('should increase on click', async () => {
    render(<Counter />)
    await userEvent.click(screen.getByRole('button'))
    expect(screen.getByText('1')).toBeInTheDocument()
  })
})
```

Read [E2ETesting](/docs/E2ETesting--docs) for more information on setting up and
running end-to-end tests.

### Stories

** TODO **

## Hooks

** TODO **

## Using the API

For now the UI app should be developed against a synthetic API first. We still
need to figure out what the UI should be. Using a synthetic API makes it
easier to run and debug the UI quickly. This fake API is build on top of
API models to provide some safety.

## Utilities

** TODO **
