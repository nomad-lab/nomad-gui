# Motivation

We are using a custom routing library to tightly integrate with NOMAD's
graph API, which is not compatible to GraphQL. Goals:

- Use URLs (including search parameters) for all navigation (including filters
  and other view options).
- Remove API calls, navigation-related states, and effects from the page
  implementation.
- Perform only one API request per page load.
- Only render the page once.
- Allow to declare required API data for independent (reusable) route segments
  and the respective independent components used to render them.

# Routes

Routes are defined with objects following the `Route` type:

```typescript
const uploadsRoute: Route<UploadsRequest, UploadsResponse> = {
  path: 'uploads',
  component: UploadsPage,
  request: {
    '*': {
      upload_name: '*',
    },
  },
  children: [uploadRoute]
}
```

This basic example shows the most important properties of a route:

- `path`: The URL path segment of the part that this route is responsible for.
The paths of all parent and children routes are concatenated to form the full path.
This is used to match against the existing browser location to determine which
route to render.
- `component`: The component that should be rendered when this route matches.
Each route object (incl. parent and children) can have a component. A parent
component defines a placeholder for the child components via `<Outlet/>`. This
way things like a nav menu can be defined in a parent route/component and can
be reused by all the children.
- `request`: The data that should be fetched from the API when this route is
matching. The idea is that route objects correspond to API resources. The
parent/child structure of routes corresponds to the parent/child structure within
the graph API. Each route object is only responsible for a specific segment.
The request object has to satisfy the request type (e.g. `UploadsRequest`).
- `children`: An array of child routes. A route object is usually part of
a larger route tree. The children are used to define the next part of the URL.

There are more sophisticated properties that can be used to define routes in
various ways. Please loot at the `Route` type for more information. You can
also have a look at the existing routes. They accompany the respective pages
in the `src/pages` subdirectories.

One route object is usually the root route object. This is then used to
configure the whole application. The root route object is passed to the
`<Router route={rootRoute}/>` component which is the root component of the application.

# Hooks

## useRoute

This hook is used to access the current route and the whole routing
system. It allows to see the current matched route and all the loaded data.
It also shows the route object that corresponds to the current component.

It also provides utility functions to navigate, e.g. create URLs for links or
to implement callbacks for buttons.

```typescript
const {url, navigate} = useRoute()

const handleClick = useCallback((uploadId: string) => {
  navigate({path: uploadId})
}, [navigate])

return (
  <div>
    <a href={url({path: '..'})}>Go back</a>
    <button onClick={() => handleClick('123')}>Go to upload 123</button>
  </div>
)
```

Have a look at the `RouteData` type for more information about the object
returned by `useRoute`.

While you could access API data from the `useRoute` return value, there are
specialized hooks for this purpose below.

## useData

This is not directly related to routing, but is used by the routing library
to implement the `useDataForRoute` and `useRouteData` hooks.

Allows to get data from the API. Issues an API request when the given request
changes (don't forget to memoize). Handles unmounts and new queries while
old queries are still running. Returns `undefined` if the current request is
not yet answered. Accepts two optional type parameters for request and response type.

```typescript
const request = useMemo(
  () => ({
    uploads: {
      '*': {
        upload_name: '*',
        upload_id: '*',
      },
    },
  }),
  [],
)
const data = useData<GraphRequest, GraphResponse>({request})
```

## useDataForRoute

Similar to `useData`, but the request and response are automatically changed
based on the current route match. Query and response type parameters are
inferred from the given route.

```typescript
const request = useMemo(() => ({
  {
    entry_name: "*",
    entry_id: "*"
  },
}), [])
const data = useDataForRoute({request, route: entryRoute})
```

## useRouteData

Returns the data from the current route. This requires that the current route has a
request attached. The request is executed before the render and there is always a
returned value. The optional route parameter allow to infer the response type. You
can alternatively specify the response type as a type argument.

```typescript
const data = useRouteData(entryRoute)
```

Route requests can be simple requests, or functions creating a request. The functions
can take two arguments `params` and `search`. Where `params` is a dictionary with
the path parameters and `search` is a dictionary with the query string/search URL parameters.
Here is an example:

```typescript
const uploadsRoute: Route<UploadsRequest, UploadsResponse> = {
  path: 'uploads',
  component: UploadsPage,
  request: (params, search) => ({
    m_request: {
      pagination: {
        page: parseInt(search.page) || 0,
      },
    },
    '*': {
      upload_name: '*',
    },
  }),
}
```

## useRouteError

Should only be used in a routes `errorComponent` and returns the current error.

## useSearch

Allows you to access the query string/search parameters in the URL. Search
parameters can be validated, i.e. the route can parse and check the string
values. This way you can ensure that the values are of the correct type or
provide proper default values.

```typescript

const route = ({
  validateSearch: (search) => {
    return {
      page = parseInt(search.page) || 0
    }
  }
})

function Component() {
  const {search, url} = useRoute(route)
  console.log(search.page)  // this will be an int and not a string
  return <a href={url({search: {page: search.page + 1}})}>next</a>
}
```
