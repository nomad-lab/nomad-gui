# Values

This is a component library for displaying and editing values of various types
and shapes. There are two levels of components: *containers* and *primitives*.

## Primitives

*Primitives* are components that are responsible for displaying and editing
values of a specific primitive type. Their responsibility is to display the
values as a formatted string or to provide the right form field to edit a value.

All *primitive* components have shared props: `value`, `editable`,
`error`, `placeholder`, `fullWidth`, and the callbacks `onChange`, `onError`,
`onBlur`, `onFocus`.

They are usually used within a *container* component.

## Containers

*Containers* are components that are responsible for displaying and editing
multiple primitive items of a collection or a complex value, like an
array or primitive values. Furthermore, *containers* are responsible to
optionally show labels, helper texts, error tests, additional actions, etc.
Containers also provide a context for primitive values, receive blur, focus,
change, and error events. Even for primitive values, you often use the
simple container component `Value` to wrap a primitive component.

All *container* components inherit shared primitive props and provide
additional props: `label`, `adornments`, `helperText`, and `focused`.

Depending on the specific container, they receive a `renderValue` prop or
children to render the primitives within.

## Examples

```tsx
<Value value='550e8400-e29b-41d4-a716-446655440000' label='uuid'>
  <Id />
  <CopyToClipboard />
</Value>
```

```tsx
<List
  value={['https://example.com', 'https://example.org']}
  onChange={...}
  editable
  renderValue={() => (<Link/>)}
  label='Links'
  placeholder='Enter a valid url ...'
  helperText='Add references to your work'
/>
```

```tsx
<Matrix
  value={[[1, 0 , 0], [0, 1, 0], [0, 0, 1]]}
  renderValue={() => (<Numeral />)}
/>
```

## Dynamic instantiation

We want to use the value library in custom layouts that are defined
in schema annotations (or other JSON data). Therefore, we need to be able to
instantiate value components dynamically based on JSON data. The
`DynamicValue` component allows to do that. Only the JSON-compatible props
are exposed, especially function props are *not* supported. These special
JSON prop types are called `Dynamic<SomeValueComponent>Props`.

```tsx
function TestComponent() {
  return <DynamicValue 
    component={{
      List: {
        valueComponent: {
          Numeral: {
            options: {
              notation: 'scientific'
            },
            actions: 'CopyToClipboard'
          },
        },
        emtpyValue: 0,
        label: 'My numbers',
      }
    }}
    value={[1, 1.23]}
    unit='m'
    displayUnit='cm'
  />
}
```

This allows us to generate elements from layouts like this:

```tsx
{
  // Quantity layout props
  type: 'quantity',
  property: 'voltages',

  // Props that are passed to the container. They could also be defined
  // in `component` with the same effect, but we do not want to force
  // to write a custom `component` just to alter a simple prop like the label.
  editable: true,
  label: 'voltage readings',
  displayUnit: 'mV',

  // An optional custom value compnent specification. There would also be
  // good defaults generated from the `quantity` type and shape. This
  // is merged with the default and default elements do not need to be
  // repeated.
  component: {
    List: {
      valueComponent: {
        Numeral: {
          options: {
            notation: 'scientific',
          },
          actions: 'CopyToClipboard',
        },
      }
      labelActions: {
        Help: {
          content: `
            **[Voltage](https://en.wikipedia.org/wiki/Voltage)**, also known as
            **(electrical) potential difference**, **electric pressure**, or
            **electric tension** is the difference in electric potential between
            two points.`,
        },
      },
    },
  },
}
```
