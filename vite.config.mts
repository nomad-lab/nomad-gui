import rehypePrism from '@mapbox/rehype-prism'
import mdx from '@mdx-js/rollup'
import react from '@vitejs/plugin-react'
import {createRequire} from 'node:module'
import path from 'node:path'
import rehypeSlug from 'rehype-slug'
import rehypeToc from 'rehype-toc'
import remarkFrontmatter from 'remark-frontmatter'
import remarkGfm from 'remark-gfm'
import {Plugin} from 'unified'
import {defineConfig} from 'vite'
import checker from 'vite-plugin-checker'
import eslintPlugin from 'vite-plugin-eslint'
import {viteStaticCopy} from 'vite-plugin-static-copy'
import svgrPlugin from 'vite-plugin-svgr'
import viteTsconfigPaths from 'vite-tsconfig-paths'
import {configDefaults} from 'vitest/config'

import {rehypePost, remarkLoadContent} from './src/dev/mdx/plugins'

const require = createRequire(import.meta.url)
const cMapsDir = path.join(
  path.dirname(require.resolve('pdfjs-dist/package.json')),
  'cmaps',
)
const standardFontsDir = path.join(
  path.dirname(require.resolve('pdfjs-dist/package.json')),
  'standard_fonts',
)

// https://vitejs.dev/config/
export default defineConfig((env) => ({
  plugins: [
    {
      enforce: 'pre',
      ...mdx({
        rehypePlugins: [
          rehypePrism as unknown as Plugin,
          rehypeSlug,
          [rehypeToc, {headings: ['h2', 'h3', 'h4']}],
          rehypePost,
        ],
        remarkPlugins: [remarkFrontmatter, remarkGfm, remarkLoadContent],
      }),
    },
    react(),
    env.mode !== 'test' &&
      eslintPlugin({
        exclude: ['/virtual:/**', 'node_modules/**'],
      }),
    env.mode !== 'test' &&
      checker({
        overlay: {initialIsOpen: false},
        typescript: true,
        eslint: {
          lintCommand: 'eslint "./src/**/*.{ts,tsx}"',
        },
      }),
    viteStaticCopy({
      targets: [
        {src: cMapsDir, dest: ''},
        {src: standardFontsDir, dest: ''},
      ],
    }),
    viteTsconfigPaths(),
    svgrPlugin(),
  ],
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
  },
  server: {
    port: 3000,
  },
  esbuild: {
    target: 'es2022',
  },
  build: {
    target: 'es2022',
    outDir: 'build',
    // This should be lowered once we have a better way to handle large chunks
    chunkSizeWarningLimit: 4096,
    rollupOptions: {
      output: {
        manualChunks: {
          '@mapbox/rehype-prism': ['@mapbox/rehype-prism'],
          'material-react-table': ['material-react-table'],
          'react-pdf': ['react-pdf'],
          msw: ['msw'],
        },
      },
    },
  },
  optimizeDeps: {
    esbuildOptions: {
      target: 'esnext',
    },
  },
  test: {
    globals: true,
    environment: 'jsdom',
    setupFiles: './setup-tests.ts',
    include: ['**/*.test.?(c|m)[jt]s?(x)'],
    exclude: [...configDefaults.exclude, './tests/e2e'],
    testTimeout: 10000,
    hookTimeout: 30000,
    coverage: {
      extension: ['tsx', 'ts'],
      exclude: [
        '**/*.stories.?(c|m)[jt]s?(x)',
        '**/*.helper.?(c|m)[jt]s?(x)',
        '.storybook/**/*',
        'playwright.config.ts',
        'public/**/*',
        'infra/**/*',
        'src/*.*',
        'src/components/app/auth.tsx',
        'src/components/richText/**/*',
        'src/pages/documentation/*',
        'src/pages/entry/*',
        'src/pages/groups/*',
        'src/pages/home/*',
        'src/pages/upload/*',
        'src/pages/uploads/*',
        'src/mocks/**/*.mts',
        'src/mocks/**/*.[jt]s',
        'src/dev/**/*',
        '.pyenv/**/*',
      ],
      all: true,
      provider: 'istanbul',
      reporter: ['cobertura', 'lcov', 'text', 'html'],
    },
  },
}))
